<?php
date_default_timezone_set('UTC');

error_reporting(0);
?>
<style>
a, a:focus, a:hover, a:active {
    cursor: pointer;
     
}
    .modal-dialog {
        width: 450px;

    }
    .ui-autocomplete{
        z-index: 5000;
    }
    #selectedcity,#companyid{
        display: none;
    }
    .table-responsive { overflow: auto; }

    .ui-menu-item{cursor: pointer;background: black;color:white;border-bottom: 1px solid white;width: 200px;}
</style>
<style>
    .col-md-12.col-xlg-6 > p {
        font-size: 18px;
        letter-spacing: 1px;
        font-weight: 800;
        text-align: center;
        color: darkcyan;
    }
    .fs-14 {
        font-size: 20px !important;
        color: #054949!important;
    }
    .col-sm-8.p-r-10 {
        width: 100%;
    }
    .modal .modal-body {
        background: #ffffff;
    }
    p.pull-right.bold {
        color: cadetblue;
        font-size: 14px;
    }
    
</style>
<script>
    $(document).ready(function () {

        $('.exportclick').click(function () {

            if ($('#start').val() != '' || $('#end').val() != '') {

                var dateObject = $("#start").datepicker("getDate"); // get the date object
                var st = dateObject.getFullYear() + '-' + (dateObject.getMonth() + 1) + '-' + dateObject.getDate();// Y-n-j in php date() format
                var dateObject = $("#end").datepicker("getDate"); // get the date object
                var end = dateObject.getFullYear() + '-' + (dateObject.getMonth() + 1) + '-' + dateObject.getDate();// Y-n-j in php date() format
                $('#exportformdate').attr('href', '<?php echo base_url() ?>index.php/masteradmin/callExel/' + st + '/' + end);
                $('#exportformdate')[0].click();
            } else {
                $('#exportformdate').attr('href', '<?php echo base_url() ?>index.php/masteradmin/callExel');
                $('#exportformdate')[0].click();
            }
        });


        $('#searchData').click(function () {
            if ($("#start").val() && $("#end").val())
            {

                var dateObject = $("#start").datepicker("getDate"); // get the date object
                var st = dateObject.getFullYear() + '-' + (dateObject.getMonth() + 1) + '-' + dateObject.getDate();// Y-n-j in php date() format
                var dateObject = $("#end").datepicker("getDate"); // get the date object
                var end = dateObject.getFullYear() + '-' + (dateObject.getMonth() + 1) + '-' + dateObject.getDate();// Y-n-j in php date() format

//           $('#createcontrollerurl').attr('href','<?php // echo base_url()                ?>//index.php/masteradmin/Get_dataformdate/'+st+'/'+end);

                var table = $('#big_table');

                var settings = {
                    "autoWidth": false,
                    "sDom": "<'table-responsive't><'row'<p i>>",
//            "sPaginationType": "bootstrap",
                    "destroy": true,
                    "scrollCollapse": true,
//            "oLanguage": {
//                "sLengthMenu": "_MENU_ ",
//                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
//            },
                    "autoWidth": false,
                            "iDisplayLength": 20,
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": '<?php echo base_url() ?>index.php/masteradmin/transection_data_form_date/' + st + '/' + end,
//                    "bJQueryUI": true,
//                    "sPaginationType": "full_numbers",
                    "iDisplayStart ": 20,
                    "oLanguage": {
                        "sProcessing": "<img src='<?php echo base_url(); ?>theme/assets/img/ajax-loader_dark.gif'>"
                    },
                    "fnInitComplete": function () {
                        //oTable.fnAdjustColumnSizing();
                    },
                    'fnServerData': function (sSource, aoData, fnCallback)
                    {
                        $.ajax
                                ({
                                    'dataType': 'json',
                                    'type': 'POST',
                                    'url': sSource,
                                    'data': aoData,
                                    'success': fnCallback
                                });
                    }
                };

                table.dataTable(settings);

                // search box for table
                $('#search-table').keyup(function () {
                    table.fnFilter($(this).val());
                });


            } else
            {
                 $('#errorfor').text('select start date and end date');
                 
            }

        });

        $('#search_by_select').change(function () {


//          $('#atag').attr('href','<?php //echo base_url()                ?>//index.php/masteradmin/search_by_select/'+$('#search_by_select').val());

            var table = $('#big_table');

            var settings = {
                "autoWidth": false,
                "sDom": "<'table-responsive't><'row'<p i>>",
//            "sPaginationType": "bootstrap",
                "destroy": true,
                "scrollCollapse": true,
//            "oLanguage": {
//                "sLengthMenu": "_MENU_ ",
//                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
//            },
                "iDisplayLength": 20,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": '<?php echo base_url() ?>index.php/masteradmin/search_by_select/' + $('#search_by_select').val(),
//                "bJQueryUI": true,
//                "sPaginationType": "full_numbers",
                "iDisplayStart ": 20,
                "oLanguage": {
                    "sProcessing": "<img src='<?php echo base_url(); ?>theme/assets/img/ajax-loader_dark.gif'>"
                },
                "fnInitComplete": function () {
                    //oTable.fnAdjustColumnSizing();
                },
                'fnServerData': function (sSource, aoData, fnCallback)
                {
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });
                }
            };

            table.dataTable(settings);

            // search box for table
            $('#search-table').keyup(function () {
                table.fnFilter($(this).val());
            });


//            $("#callone").trigger("click");
        });

    });



    function refreshTableOnCityChange() {

        var table = $('#big_table');
        var url = '';

        if ($('#start').val() != '' || $('#end').val() != '') {

            var dateObject = $("#start").datepicker("getDate"); // get the date object
            var st = dateObject.getFullYear() + '-' + (dateObject.getMonth() + 1) + '-' + dateObject.getDate();// Y-n-j in php date() format
            var dateObject = $("#end").datepicker("getDate"); // get the date object
            var end = dateObject.getFullYear() + '-' + (dateObject.getMonth() + 1) + '-' + dateObject.getDate();// Y-n-j in php date() format

            url = '<?php echo base_url() ?>index.php/masteradmin/transection_data_form_date/' + st + '/' + end + '/' + $('#search_by_select').val() + '/' + $('#companyid').val();

        } else {
            url = '<?php echo base_url() ?>index.php/masteradmin/search_by_select/' + $('#search_by_select').val();
        }
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
//            "sPaginationType": "bootstrap",
            "destroy": true,
            "scrollCollapse": true,
//            "oLanguage": {
//                "sLengthMenu": "_MENU_ ",
//                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
//            },
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": url,
//            "bJQueryUI": true,
//            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "oLanguage": {
                "sProcessing": "<img src='<?php echo base_url(); ?>theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {
                //oTable.fnAdjustColumnSizing();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function () {
            table.fnFilter($(this).val());
        });

    }


</script>

<script type="text/javascript">
    $(document).ready(function () {


        $('.transection').addClass('active');
        $('.transection').attr('src', "<?php echo base_url(); ?>/theme/icon/accounting_on.png");
//        $('.transection .icon-thumbnail').addClass("bg-success");

        $('#datepicker-component').on('changeDate', function () {
            $(this).datepicker('hide');
        });



//        $("#datepicker1").datepicker({ minDate: 0});
        var date = new Date();
        $('#datepicker-component').datepicker({
            startDate: date
        });

        var table = $('#big_table');

        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
//            "sPaginationType": "bootstrap",
            "destroy": true,
            "scrollCollapse": true,
//            "oLanguage": {
//                "sLengthMenu": "_MENU_ ",
//                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
//            },
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url(); ?>index.php/masteradmin/transection_data_ajax',
//            "bJQueryUI": true,
//            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "oLanguage": {
                "sProcessing": "<img src='<?php echo base_url(); ?>theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {
                //oTable.fnAdjustColumnSizing();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function () {
            table.fnFilter($(this).val());
        });

    });
    function billed_amount(thisval)
    {
        var B_id = thisval;
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('index.php/masteradmin') ?>/getBilledAmount",
            data: {bid: B_id},
            dataType: 'JSON',
            success: function (result)
            {
                var html='';
                $('#ShowCategoryData').empty();
                 html = '<div class="col-md-12 col-xlg-6">\n\
                         <p>Billed Amount</p>\n\
                                    <div class="widget-17-weather">\n\
                                        <div class="row">\n\
                                            <div class="col-sm-8 p-r-10">\n\
                                            <p style="font-weight: bold; border-top: #d8d6d6 1px solid;padding-top: 10px;">APP BILLING</p>\n\
                                            \n\<div class="row">\n\
                                                    <div class="col-md-12">\n\
                                                        <p class="pull-left">Visit Fee </p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + result.visit_fees + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="row">\n\
                                                    <div class="col-md-12">\n\
                                                        <p class="pull-left">Time Fee </p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + result.comp.time_fees + '</p>\n\
                                                    </div>\n\
                                                </div><p style="font-weight:bold">Services</p>';
                                 $.each(result.services, function (index, res) {

                
                    html += '<div class="row">\n\
                                            <div class="col-md-12">\n\
                                                        <p class="pull-left">' + res.sname + '</p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + res.sprice + '</p>\n\
                                                    </div>\n\
                                                </div>';
                   

                });
                          
                                 html+='<div class="row">\n\
                                                    <div class="col-md-12" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
                                                        <p class="pull-left">App Discount</p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + result.coupon_discount + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="row">\n\
                                                    <div class="col-md-12" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
                                                        <p class="pull-left">Sub Total</p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + result.invoiceData.app_billing + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
\n\                                             <p style="font-weight: bold; border-top: #d8d6d6 1px solid;padding-top: 10px;">PRO BILLING</p>\n\
                                                <div class="row">\n\
                                                    <div class="col-md-12">\n\
                                                        <p class="pull-left">Misc Fee</p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + result.comp.misc_fees + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="row">\n\
                                                    <div class="col-md-12">\n\
                                                        <p class="pull-left">Material Fee</p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' +result.comp.mat_fees + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="row">\n\
                                                    <div class="col-md-12" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
                                                        <p class="pull-left">Pro Discount</p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + result.comp.pro_disc + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                         \n\ <div class="row">\n\
                                                    <div class="col-md-12" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
                                                        <p class="pull-left">Sub Total</p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + result.invoiceData.pro_billing + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                         \n\ <div class="row">\n\
                                                    <div class="col-md-12" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
                                                        <p class="pull-left">Total</p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + result.invoiceData.billed_amount + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                         \n\ <div class="row">\n\
                                                    <div class="col-md-12" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
                                                        <p class="pull-left">Pro Notes</p>\n\
                                                        <p class="pull-right bold"> ' + result.comp.complete_note + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                            <div class="row">\n\
                                                    <div class="col-md-12">\n\
                                                        <p class="pull-left">Provider Sign</p>\n\
                                                        <div class="pull-right bold">\n\
                                        <img src="' + result.comp.sign_url + '" border=3 height=200 width=200></img></div>\n\
                                                    </div>\n\
                                                </div>';
                var services = "";
                services += "<table cellspacing='20'>";
                services += "<tr><td>Visit Fee</td><td>: <?PHP echo CURRENCY ?></td><td>" + result.visit_fees + "</td></tr>";
                services += "<tr><td>Time Fee</td><td>: <?PHP echo CURRENCY ?></td><td>" + result.comp.time_fees + "</td></tr>";
                services += "<tr><td><span style='font-weight:bold;'>Services</td></tr>";

                $.each(result.services, function (index, res) {

                    services += "<tr><td>" + res.sname + "</td><td>:</td>";
                    services += "<td>" + res.sprice + "</td></tr>";

                });
                services += "<tr><td>App Discount</td><td>: <?PHP echo CURRENCY ?></td><td>" + result.coupon_discount + "</td></tr>";
                services += "<tr><td><span style='font-weight:bold;'>Sub Total</td><td>: <?PHP echo CURRENCY ?></td><td>" + result.invoiceData.app_billing + "</td></tr>";
                services += "</table>";
                $('#ShowCategoryData').append(services);
                $('.displayInvoiceData').html(html);

                $('#probill').empty();
                var probill = "";
                probill += "<table cellspacing='15'>";
                probill += "<tr><td>Misc Fee</td><td>: <?PHP echo CURRENCY ?></td><td>" + result.comp.misc_fees + "</td></tr>";
                probill += "<tr><td>Material Fee</td><td>: <?PHP echo CURRENCY ?></td><td>" + result.comp.mat_fees + "</td></tr>";
                probill += "<tr><td>Pro Discount</td><td>: <?PHP echo CURRENCY ?></td><td>" + result.comp.pro_disc + "</td></tr>";
                probill += "<tr><td>Sub Total </td><td>: <?PHP echo CURRENCY ?></td><td>" + result.invoiceData.pro_billing + "</td></tr>";
                probill += "<tr><td><span style='font-weight:bold;'>Total</td><td>: <?PHP echo CURRENCY ?></td><td>" + result.invoiceData.billed_amount + "</td></tr>";
                probill += "<tr><td>Pro Notes</td><td>: <?PHP echo CURRENCY ?></td><td>" + result.comp.complete_note + "</td></tr>";
                probill += "<tr><td>Provider Sign</td><td>:</td><td><img src='" + result.comp.sign_url + "' alt='' border=3 height=200 width=200></img></td></tr>";
                probill += "</table>";
                $('#probill').append(probill);
                $('#billed').modal('show');
            }
        });

    }
    function coupon_discount(thisval)
    {
        var B_id = thisval;
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('index.php/masteradmin') ?>/getBilledAmount",
            data: {bid: B_id},
            dataType: 'JSON',
            success: function (result)
            {   
                var html='';
                var dtype = "";
                if (result.coupon_type == 1)
                    dtype = "Percent";
                else if (result.coupon_type == 2)
                    dtype = "Fixed";
                $('#coupondis').empty();
                var services = "";
                services += "<table>";
                services += "<tr><td>Discount code </td><td>: <?PHP echo CURRENCY ?></td><td>" + result.coupon_code + "</td></tr>";
                services += "<tr><td>Discount type </td><td>: <?PHP echo CURRENCY ?></td><td>" + dtype + "</td></tr>";
                services += "<tr><td>Discount value </td><td>: <?PHP echo CURRENCY ?></td><td>" + result.coupon_discount + "</td></tr>";
                services += "</table>";
                
                 html = '<div class="col-md-12 col-xlg-6">\n\
                         <p>Discount </p>\n\
                                    <div class="widget-17-weather">\n\
                                        <div class="row">\n\
                                            <div class="col-sm-8 p-r-10">\n\
                                                <div class="row">\n\
                                                    <div class="col-md-12">\n\
                                                        <p class="pull-left"> Discount code</p>\n\
                                                        <p class="pull-right bold">' + result.coupon_code + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="row">\n\
                                                    <div class="col-md-12">\n\
                                                        <p class="pull-left">Discount type</p>\n\
                                                        <p class="pull-right bold">' + dtype+ '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="row">\n\
                                                    <div class="col-md-12" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
                                                        <p class="pull-left">App Discount</p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + result.coupon_discount + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                               </div>\n\
                                          </div>\n\
                                    </div>\n\
                               </div>';
                $('.coupondisData').html(html);
                $('#coupondis').append(services);
                $('#coupon_dis').modal('show');
            }

        });

    }
    function pro_earning(thisval)
    {
        var B_id = thisval;
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('index.php/masteradmin') ?>/getBilledAmount",
            data: {bid: B_id},
            dataType: 'JSON',
            success: function (result)
            {
                var html='';
                $('#proearnings').empty();
                var services = "";
                var pro_comm = 100 - result.pro_commission;
                var pro_commision = (parseInt(result.invoiceData.app_billing) * parseInt(pro_comm)) / 100;
                services += "<table>";
                services += "<tr><td>Pro Commission </td><td>: <?PHP echo CURRENCY ?></td><td>" + pro_commision + "</td></tr>";
                services += "<tr><td>Misc Fee </td><td>: <?PHP echo CURRENCY ?></td><td>" + result.comp.misc_fees + "</td></tr>";
                services += "<tr><td>Material Fee </td><td>: <?PHP echo CURRENCY ?></td><td>" + result.comp.mat_fees + "</td></tr>";
                services += "<tr><td>Pro Discount</td><td>: <?PHP echo CURRENCY ?></td><td>" + result.comp.pro_disc + "</td></tr>";
                services += "<tr><td>Subtotal</td><td>: <?PHP echo CURRENCY ?></td><td>" + result.invoiceData.pro_billing + "</td></tr>";
                services += "<tr><td>Pro PG Commission</td><td>: <?PHP echo CURRENCY ?></td><td>" + result.invoiceData.pro_pg_comm + "</td></tr>";
                services += "<tr><td>Net Pro Earnings</td><td>: <?PHP echo CURRENCY ?></td><td>" + result.invoiceData.pro_earning + "</td></tr>";
                services += "</table>";
                 html = '<div class="col-md-12 col-xlg-6">\n\
                         <p>PRO EARNINGS</p>\n\
                                    <div class="widget-17-weather">\n\
                                        <div class="row">\n\
                                            <div class="col-sm-8 p-r-10">\n\
                                                <div class="row">\n\
                                                    <div class="col-md-12">\n\
                                                        <p class="pull-left">Pro Commission</p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + pro_commision + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="row">\n\
                                                    <div class="col-md-12" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
                                                        <p class="pull-left">Misc Fee</p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> '+ result.comp.misc_fees + ' </p>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="row">\n\
                                                    <div class="col-md-12">\n\
                                                        <p class="pull-left">Material Fee</p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + result.comp.mat_fees + '  </p>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="row">\n\
                                                    <div class="col-md-12" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
                                                        <p class="pull-left">Pro Discount</p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + result.comp.pro_disc + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="row">\n\
                                                    <div class="col-md-12" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
                                                        <p class="pull-left">Subtotal</p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + result.invoiceData.pro_billing + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="row">\n\
                                                    <div class="col-md-12" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
                                                        <p class="pull-left">Pro PG Commission</p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + result.invoiceData.pro_pg_comm + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                             <div class="row">\n\
                                                    <div class="col-md-12" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
                                                        <p class="pull-left">Net Pro Earnings</p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + result.invoiceData.pro_earning + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                               </div>\n\
                                          </div>\n\
                                    </div>\n\
                               </div>';
                $('#proearnings').append(services);
                $('.proearningsData').html(html);
                $('#pro_earnings').modal('show')
            }
        });

    }
    function app_earning(thisval)
    {
        var B_id = thisval;
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('index.php/masteradmin') ?>/getBilledAmount",
            data: {bid: B_id},
            dataType: 'JSON',
            success: function (result)
            {
                var html = '';
                $('#appearnings').empty();
                var services = "";
                services += "<table cellspacing='20'>";
                services += "<tr><td>Visit Fee</td><td>: <?PHP echo CURRENCY ?></td><td>" + result.visit_fees + "</td></tr>";
                services += "<tr><td>Time Fee</td><td>: <?PHP echo CURRENCY ?></td><td>" + result.comp.time_fees + "</td></tr>";
                services += "<tr><td><span style='font-weight:bold;'>Services</td></tr>";

                $.each(result.services, function (index, res) {
                    services += "<tr><td>" + res.sname + "</td><td>:</td>";
                    services += "<td>" + res.sprice + "</td></tr>";
                });
                services += "<tr><td>App Discount</td><td>: <?PHP echo CURRENCY ?></td><td>" + result.coupon_discount + "</td></tr>";
                services += "<tr><td><span style='font-weight:bold;'>Sub Total</td><td>: <?PHP echo CURRENCY ?></td><td>" + result.invoiceData.app_billing + "</td></tr>";
                services += "</table>";

                var app_comm = (result.invoiceData.app_billing * result.pro_commission) / 100;
                services += "<table cellspacing='20'>";
                services += "<tr><td>App Commision</td><td>: <?PHP echo CURRENCY ?></td><td>" + app_comm + "</td></tr>";
                services += "<tr><td>App PG Commission</td><td>: <?PHP echo CURRENCY ?></td><td>" + result.invoiceData.app_pg_comm + "</td></tr>";
                services += "<tr><td>App Earning</td><td>: <?PHP echo CURRENCY ?></td><td>" + result.invoiceData.app_earning + "</td></tr>";
                services += "</table>";
                
                html = '<div class="col-md-12 col-xlg-6">\n\
                         <p>App Earning</p>\n\
                                    <div class="widget-17-weather">\n\
                                        <div class="row">\n\
                                            <div class="col-sm-8 p-r-10">\n\
                                             <div class="row">\n\
                                                    <div class="col-md-12">\n\
                                                        <p class="pull-left">Visit Fee</p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + result.visit_fees + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="row">\n\
                                                    <div class="col-md-12">\n\
                                                        <p class="pull-left">Time Fee </p>\n\
                                                        <p class="pull-right bold"> <?PHP echo CURRENCY ?> ' + result.comp.time_fees + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                                 <p style="font-weight:bold">Services</p>';
                                 $.each(result.services, function (index, res) {

                
                    html += '<div class="row">\n\
                                            <div class="col-md-12">\n\
                                                        <p class="pull-left">' + res.sname + '</p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + res.sprice + '</p>\n\
                                                    </div>\n\
                                                </div>';
                   

                });
                                             html+='<div class="row" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
                                                    <div class="col-md-12">\n\
                                                        <p class="pull-left">App Discount</p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + result.coupon_discount + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                        \n\ <div class="row" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
                                                    <div class="col-md-12">\n\
                                                        <p class="pull-left">Sub Total</p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + result.invoiceData.app_billing + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="row" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
                                                    <div class="col-md-12">\n\
                                                        <p class="pull-left">App Commision</p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + app_comm + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="row" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
                                                    <div class="col-md-12">\n\
                                                        <p class="pull-left">App PG Commission</p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + result.invoiceData.app_pg_comm+ '</p>\n\
                                                    </div>\n\
                                                </div>\n\
\n\                                             <div class="row">\n\
                                                    <div class="col-md-12" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
                                                        <p class="pull-left">App Earning </p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + result.invoiceData.app_earning + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                               </div>\n\
                                          </div>\n\
                                    </div>\n\
                                    </div>\n\
                               </div>';
                $('#appearnings').append(services);
                $('.appearningsData').html(html);
                $('#app_earnings').modal('show')
            }
        });

    }
    function pg_Commission(thisval)
    {
        var B_id = thisval;
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('index.php/masteradmin') ?>/getBilledAmount",
            data: {bid: B_id},
            dataType: 'JSON',
            success: function (result)
            {
                var html='';
                $('#pgcommission').empty();
                var total_pg = parseFloat(result.invoiceData.pro_pg_comm + result.invoiceData.app_pg_comm).toFixed(2);
                html = '<div class="col-md-12 col-xlg-6">\n\
                         <p>PAYMENT GATEWAY COMISSION</p>\n\
                                    <div class="widget-17-weather">\n\
                                        <div class="row">\n\
                                            <div class="col-sm-8 p-r-10">\n\
                                                <div class="row">\n\
                                                    <div class="col-md-12">\n\
                                                        <p class="pull-left">Payment Gateway Name</p>\n\
                                                        <p class="pull-right bold"> Stripe </p>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="row">\n\
                                                    <div class="col-md-12">\n\
                                                        <p class="pull-left">Payemnt Gateway Comission</p>\n\
                                                        <p class="pull-right bold">2.9% + <?PHP echo CURRENCY ?> 0.30 </p>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="row">\n\
                                                    <div class="col-md-12">\n\
                                                        <p class="pull-left">App Share of Payment Gateway Comission</p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + result.invoiceData.pro_pg_comm + '  </p>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="row">\n\
                                                    <div class="col-md-12">\n\
                                                        <p class="pull-left">Pro Share of payment gateway commission</p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + result.invoiceData.app_pg_comm + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                             <div class="row">\n\
                                                    <div class="col-md-12" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
                                                        <p class="pull-left">Total Payment Gateway Comission</p>\n\
                                                        <p class="pull-right bold"><?PHP echo CURRENCY ?> ' + total_pg + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                               </div>\n\
                                          </div>\n\
                                    </div>\n\
                               </div>';
        
                var services = "";
                services += "<table>";
                services += "<tr><td>Payment Gateway Name </td><td>:</td><td>Stripe</td></tr>";
                services += "<tr><td>Payemnt Gateway Comission</td><td>: <?PHP echo CURRENCY ?></td><td>2.9% + $0.30</td></tr>";
                services += "<tr><td>App Share of Payment Gateway Comission</td><td>: <?PHP echo CURRENCY ?></td><td>" + result.invoiceData.pro_pg_comm + "</td></tr>";
                services += "<tr><td>Pro Share of payment gateway commission</td><td>: <?PHP echo CURRENCY ?></td><td>" + result.invoiceData.app_pg_comm + "</td></tr>";
                services += "<tr><td>Total Payment Gateway Comission</td><td>: <?PHP echo CURRENCY ?></td><td>" + total_pg + "</td></tr>";
                services += "</table>";
                $('#pgcommission').append(services);
               
                $('.pgcommissionData').html(html);
                $('#pg_commission').modal('show')
            }
        });

    }
</script>

<style>
    .exportOptions{
        display: none;
    }
    .table thead tr th {
    
    font-size: 10px;
    }
</style>
<div class="page-content-wrapper no-padding" style="padding-top: 20px">
    <!-- START PAGE CONTENT -->
    <div class="content" style="padding-top:0px">
<!--        <div class="brand inline" style="  width: auto;
             font-size: 16px;
             color: gray;
             margin-left: 30px;padding-top: 20px;">
                               <img src="<?php //echo base_url();                      ?>theme/assets/img/Rlogo.png" alt="logo" data-src="<?php //echo base_url();                      ?>theme/assets/img/Rlogo.png" data-src-retina="<?php //echo base_url();                      ?>theme/assets/img/logo_2x.png" width="93" height="25">

            <strong style="color:#0090d9;">ACCOUNTING</strong> id="define_page"
        </div>-->
          <div class="inner">
                <ul class="breadcrumb" style="margin-left: 6px;font-size: 16px;color:#0090d9;">
                    <li><a style="color:#0090d9;" href="<?php echo base_url(); ?>index.php/masteradmin/accounting" class="">ACCOUNTING</a>
                    
                </ul>
            </div>
        <!-- START JUMBOTRON -->
        <div class="jumbotron" data-pages="parallax">
              
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">


                <div class="container-fluid container-fixed-lg bg-white no-padding">
                    <!-- START PANEL -->
                    <div class="panel panel-transparent">
                        <ul class="nav nav-tabs nav-tabs-fillup  bg-white">
                        <li id="1" class="active" style="cursor:auto">
                            <a><span style="cursor:auto">ACCOUNTING</span></a>
                        </li>

                    </ul>
                        <div class="panel-heading">



                            <div class="row clearfix">


                                <div class="col-sm-3">
                                    <div class="" aria-required="true">

                                        <div class="input-daterange input-group" id="datepicker-range">
                                            <input type="text" class="input-sm form-control" name="start" id="start" placeholder="Start">
                                            <span class="input-group-addon">to</span>
                                            <input type="text" class="input-sm form-control" name="end"  id="end" placeholder="End">

                                        </div>

                                    </div>

                                </div>
                                <div class="col-sm-1">
                                    <div class="">
                                        <button class="btn btn-primary" type="button" id="searchData">Search</button>
                                    </div>
                                </div>
                                 
                                
                                  <div class="row clearfix">

                                        <div class="">

                                        <div class="pull-right" style="margin-right:32px;"><input type="text" id="search-table" class="form-control pull-right" placeholder="Search"> </div>

                                        <div class="pull-right"> <a href="<?php echo base_url() ?>index.php/masteradmin/callExel<?php echo $stdate; ?>/<?php echo $enddate ?>" id=exportformdate></a>
                                            <button class="btn btn-primary exportclick" style="margin-right:10px;"  type="submit">Export</button></a></div>

                                    </div>
                                    </div>
                     

                            </div>
<div style="margin-left: 1%;
    color: red;" id="errorfor"></div>



                        </div>
                        <div class="panel-body">
                            <div id="tableWithSearch_wrapper" class="dataTables_wrapper form-inline no-footer">
                                <div class="table-responsive">

                                    <?php echo $this->table->generate(); ?>

                                </div><div class="row"></div></div>
                        </div>
                    </div>
                    <!-- END PANEL -->
                </div>



            </div>


        </div>
        <!-- END JUMBOTRON -->

        <!-- START CONTAINER FLUID -->
        <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

            <!-- END PLACE PAGE CONTENT HERE -->
        </div>
        <!-- END CONTAINER FLUID -->

    </div>
    <!-- END PAGE CONTENT -->
    <!-- START FOOTER -->
    <div class="container-fluid container-fixed-lg footer">
        <div class="copyright sm-text-center">
            <p class="small no-margin pull-left sm-pull-reset">
                <span class="hint-text">Copyright @ Goclean Service software technologies, All rights reserved</span>

            </p>

            <div class="clearfix"></div>
        </div>
    </div>
    <!-- END FOOTER -->
</div>

<div class="modal fade stick-up" id="confirmmodels" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                <div class=" clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>

                </div>

            </div>
            <br>
            <div class="modal-body">
                <div class="row">

                    <div class="error-box" id="errorboxdatas" style="font-size: large;text-align:center"><?php echo VEHICLEMODEL_DELETE; ?></div>

                </div>
            </div>

            <br>

            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4" ></div>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4" >
                        <button type="button" class="btn btn-primary pull-right" id="confirmeds" ><?php echo BUTTON_OK; ?></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<div class="modal fade stick-up in" id="billed_amt" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;">
    <div class="modal-dialog " style="width: 500px">
        <div class="modal-content">
            <div class="modal-header clearfix ">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h3> BILLED AMOUNT</h3>

                <p></p>
            </div>
            <div class="modal-body">


            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade stick-up in" id="appdiscount" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header clearfix ">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h3> APP DISCOUNT</h3>

                <p></p>
            </div>
            <div class="modal-body">


                <div class="form-group"  id="discount" class="formex">                            
                </div>   


            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade stick-up in" id="coupon_dis" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;">
    <div class="modal-dialog ">
       <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <div class="container-xs-height full-height">
                    <div class="row-xs-height">
                        <div class="modal-body col-xs-height coupondisData  ">
                            
                        </div>
                    </div>
                </div>
            </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade stick-up in" id="pro_earnings" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;">
    <div class="modal-dialog ">
<!--        <div class="modal-content">
            <div class="modal-header clearfix ">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h3> PRO EARNINGS</h3>

                <p></p>
            </div>
            <div class="modal-body">

                <div class="form-group"  id="proearnings" class="formex">                            
                </div>   


            </div>
        </div>-->
<div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <div class="container-xs-height full-height">
                    <div class="row-xs-height">
                        <div class="modal-body col-xs-height  proearningsData ">
                            
                        </div>
                    </div>
                </div>
            </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade stick-up in" id="app_earnings" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;">
    <div class="modal-dialog ">
<!--        <div class="modal-content">
            <div class="modal-header clearfix ">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h3> APP EARNINGS</h3>

                <p></p>
            </div>
            <div class="modal-body">

                <div class="form-group"  id="appearnings" class="formex">                            
                </div>   


            </div>
        </div>-->
  <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <div class="container-xs-height full-height">
                    <div class="row-xs-height">
                        <div class="modal-body col-xs-height appearningsData  ">
                            
                        </div>
                    </div>
                </div>
            </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade stick-up in" id="pg_commission" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;">
    <div class="modal-dialog ">
<!--        <div class="modal-content">
            <div class="modal-header clearfix ">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h3> PAYMENT GATEWAY COMISSION</h3>

                <p></p>
            </div>
            <div class="modal-body">

                <div class="form-group"  id="pgcommission" class="formex">                            
                </div>   


            </div>
        </div>-->
        <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <div class="container-xs-height full-height">
                    <div class="row-xs-height">
                        <div class="modal-body col-xs-height pgcommissionData  ">
                            
                        </div>
                    </div>
                </div>
            </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade stick-up in" id="billed" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;">
    <div class="modal-dialog ">
<!--        <div class="modal-content">
            <div class="modal-header clearfix ">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h3> BILLED AMOUNT</h3>

                <p></p>
            </div>
            <div class="modal-body">

                <div class="text-left">
                    <h5><span class="semi-bold">APP BILLING</h5>
                </div>

                <div class="form-group"  id="ShowCategoryData" class="formex">                            
                </div>   
                <div class="text-left">
                    <h5><span class="semi-bold">Pro BILLING</h5>
                </div>
                <div class="form-group"  id="probill" class="formex">                            
                </div>

            </div>
        </div>-->
<div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <div class="container-xs-height full-height">
                    <div class="row-xs-height">
                        <div class="modal-body col-xs-height  displayInvoiceData ">
                            
                        </div>
                    </div>
                </div>
            </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>