<?php
date_default_timezone_set('UTC');
$rupee = "$";
error_reporting(0);

if ($stmsg == '1') {
    $Newrequest = 'selected';
} else if ($bookingDetails['status'] == '2')
    $accepted = 'Provider accepted.';
else if ($bookingDetails['status'] == '3')
    $rejected = 'Provider rejected.';
else if ($bookingDetails['status'] == '5')
    $Donw = 'Driver is on the way.';
else if ($bookingDetails['status'] == '6')
    $started = 'Appointment started.';
else if ($bookingDetails['status'] == '7')
    $completed = 'Appointment completed.';
else if ($stmsg == '3') {
    $cancelD = 'selected';
} else if ($stmsg == '9') {
    $cancelP = 'selected';
} else if ($stmsg == '8') {
    $expired = 'selected';
}


$stdate = explode("-", $stdate);

$endate = explode("-", $endate);
?>
<script>
    $(document).ready(function () {

        $('#datepicker').datepicker({
            startDate: '-0m'
                    //endDate: '+2d'
        }).on('changeDate', function (ev) {
            $('#start').text($('#datepicker').data('date'));
            $('#datepicker').datepicker('hide');
        });

        $('.allbooking').addClass('active open');
        $('.allbooking .icon-thumbnail').addClass('bg-success');

        if ('<?php echo $stdate[1] . '/' . $stdate[2] . '/' . $stdate[0]; ?>' != '//') {
            $('#start').datepicker('setDate', '<?php echo $stdate[1] . '/' . $stdate[2] . '/' . $stdate[0]; ?>');
            $('#end').datepicker('setDate', '<?php echo $endate[1] . '/' . $endate[2] . '/' . $endate[0]; ?>');
        }

        $('#searchData').click(function () {
//
//    if($("#start").datepicker("getDate")) ) {
            if ($("#start").val() != '') {
//        alert($("#start").val());
                var dateObject = $("#start").datepicker("getDate"); // get the date object
                var st = dateObject.getFullYear() + '-' + (dateObject.getMonth() + 1) + '-' + dateObject.getDate();// Y-n-j in php date() format
                var dateObject = $("#end").datepicker("getDate"); // get the date object
                var end = dateObject.getFullYear() + '-' + (dateObject.getMonth() + 1) + '-' + dateObject.getDate();// Y-n-j in php date() format

                $('#createcontrollerurl').attr('href', '<?php echo base_url() ?>index.php/masteradmin/Get_dataformdate/1/' + st + '/' + end);
                $('#createcontrollerurl')[0].click();
            } else if ($('#Sortby').val() != '') {
                $('#createcontrollerurl').attr('href', '<?php echo base_url() ?>index.php/masteradmin/Get_TransectionDataBy_selected_option/' + $('#Sortby').val());
                $('#createcontrollerurl')[0].click();
            }
//            else{
//
////                $('#createcontrollerurl').attr("href").removeAttr("href");
//            }

        });

        $('#search_by_select').change(function () {


            $('#atag').attr('href', '<?php echo base_url() ?>index.php/masteradmin/search_by_select/' + $('#search_by_select').val() + '/1');

            $("#callone").trigger("click");
        });

    });

</script>

<style>
    .exportOptions{
        display: none;
    }
</style>
<div class="page-content-wrapper">
    <!-- START PAGE CONTENT -->
    <div class="content" style="padding-top:0px">
        <!-- START JUMBOTRON -->
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <div class="inner">
                    <!-- START BREADCRUMB -->
                    <ul class="breadcrumb">
                        <li>
                            <p>PROVIDER</p>
                        </li>
                        <li><a href="#" class="active">All Bookings</a>
                        </li>
                    </ul>
                    <!-- END BREADCRUMB -->
                </div>





                <div class="container-fluid container-fixed-lg bg-white">
                    <!-- START PANEL -->
                    <div class="panel panel-transparent">
                        <div class="panel-heading">



                            <div class="row clearfix">

                                <!--                                <div class="col-sm-2">-->
                                <!--                                    <div class="">-->
                                <!--                                        <div class="form-group ">-->
                                <!--                                            <select  class="full-width select2-offscreen" id="search_by_select" data-init-plugin="select2" tabindex="-1" title="select" >-->
                                <!--                                                <optgroup label="Payment Type">-->
                                <!--                                                    <option selected>Payment Method...</option>-->
                                <!--                                                    <option value="1">Cash</option>-->
                                <!--                                                    <option value="2">Card</option>-->
                                <!--                                                </optgroup>-->
                                <!---->
                                <!--                                            </select>-->
                                <!--                                            <a href="" id="atag"> <input type="button" id="callone" style="display: none;"/> </a>-->
                                <!--                                        </div>-->
                                <!--                                    </div>-->
                                <!--                                </div>-->

                                <div class="col-sm-3">
                                    <div class="" aria-required="true">

                                        <div class="input-daterange input-group" id="datepicker-range">
                                            <input type="text" class="input-sm form-control" name="start" id="start" placeholder="From">
                                            <span class="input-group-addon">to</span>
                                            <input type="text" class="input-sm form-control" name="end"  id="end" placeholder="To">

                                        </div>

                                    </div>

                                </div>



                                <div class="col-sm-1">

                                    <div class="" style="margin-left: 31px;font-size: 20px;">

                                        or
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="">
                                        <div class="form-group ">
                                            <select  class="full-width select2-offscreen" id="Sortby" data-init-plugin="select2" tabindex="-1" title="select" >
                                                <option value="1" >sort by..</option>
                                                <option value="1" <?php echo $Newrequest ?>>NEW REQUEST</option>
                                                <option value="2" <?php echo $accepted ?>>PROVIDER ACCEPTED</option>
                                                <option value="3" <?php echo $rejected ?>>PROVIDER REJECTED</option>
                                                <option value="5" <?php echo $Donw ?>>PROVIDER ON THE WAY</option>
                                                <option value="6" <?php echo $started ?>>PROVIDER ARRIVED</option>
                                                <option value="7" <?php echo $completed ?>>Completed</option>
                                                <option value="3" <?php echo $cancelD ?>>CANCELLED BY PROVIDER</option>
                                                <option value="9" <?php echo $cancelP ?>>CANCELLED BY Customer</option>
                                                <option value="8" <?php echo $expired ?>>EXPIRED</option>




                                            </select>
                                            <a href="" id="atag"> <input type="button" id="callone" style="display: none;"/> </a>
                                        </div>
                                    </div>
                                </div>



                                <div class="col-sm-2">
                                    <div class="">
                                        <button class="btn btn-primary" type="button" id="searchData">Search</button> <a id="createcontrollerurl" href=""></a>
                                    </div>
                                </div>

                                <div class="col-sm-2">

                                    <div class="">

                                        <div class="pull-right"><input type="text" id="search-table" class="form-control pull-right" placeholder="Search by id"> </div>
                                    </div>
                                </div>



                                <div class="col-sm-2">
                                    <div class="">

<!--                                        <div class="pull-right"> 
                                            <a href="<?php echo base_url() ?>index.php/masteradmin/callExel/1/<?php echo $stdate; ?>/<?php echo $enddate ?>"> 
                                                <button class="btn btn-primary" type="submit">Export</button></a>
                                        </div>-->
                                    </div>
                                </div>
                            </div>




                        </div>
                        <div class="panel-body">
                            <div id="tableWithSearch_wrapper" class="dataTables_wrapper form-inline no-footer"><div class="table-responsive"><table class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info">
                                        <thead>

                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column ascending" style="width: 68px;">SLNO</th>
                                                <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 68px;">Session ID</th>
                                                <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 150px;">Date</th>
                                                <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 80px;">Customer Name</th>
        <!--                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 80px;">Customer Paid</th>-->
        <!--                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 98px;">App Commission(10%)</th>-->
        <!--                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 169px;">Payment Gateway Commission(2.9 % + $0.3)</th>-->

                                                <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 70px;">Appointment Address </th>
                                                <!--                                                                                            <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 149px;">Transection Id </th>-->
                                                <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width:75px;">Session Status</th>
        <!--                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 149px;"> Download </th>-->
                                            </tr>


                                        </thead>
                                        <tbody>












                                            <?php
                                            $slno = 1;

                                            foreach ($transection_data as $result) {
                                                ?>


                                                <tr role="row"  class="gradeA odd">
                                                    <td id = "d_no" class="v-align-middle sorting_1"> <p><?php echo $slno; ?></p></td>
                                                    <td id = "d_no" class="v-align-middle sorting_1"> <p><?php echo $result->appointment_id; ?></p></td>
                                                    <td class="v-align-middle"><?php echo date("M d Y g:i A", (int) (strtotime($result->appointment_dt)));// + ($this->session->userdata("offset") * 60) ?></td>
                                                    <td class="v-align-middle"><?php echo $result->slv_fname; ?></td>


            <!--                                            <td class="v-align-middle">--><?php //echo $rupee. $result->amount;    ?><!--</td>-->
            <!--                                            <td class="v-align-middle">--><?php //echo $rupee. round((float)$result->amount * (10 / 100),2)    ?><!--</td>-->
            <!--                                            <td class="v-align-middle">--><?php //echo $rupee. round((float)($result->amount * (2.9 / 100)) + 0.3,2)    ?><!--</td>-->
            <!--                                            <td class="v-align-middle">--><?php //echo  $rupee. round((float) (($result->amount - ($result->amount * (10 / 100)) - (float)(($result->amount * (2.9 / 100)) + 0.3))),2);    ?><!--</td>-->
            <!--                                                                                            <td class="v-align-middle">--><?php //echo  $result->tr_id;    ?><!--</td>-->
                                                    <td class="v-align-middle"><?php echo $result->address_line1; ?>
                                                        <?php
                                                        if ($result->status == '1')
                                                            $status = 'Appointment requested';
                                                        else if ($result->status == '2')
                                                            $status = 'PROVIDER accepted.';
                                                        else if ($result->status == '3')
                                                            $status = 'PROVIDER rejected.';
                                                        else if ($result->status == '4')
                                                            $status = 'Customer has cancelled.';

                                                        else if ($result->status == '5')
                                                            $status = 'PROVIDER is on the way.';
                                                        else if ($result->status == '6')
                                                            $status = 'Appointment started.';
                                                        else if ($result->status == '7')
                                                            $status = 'Appointment completed.';
                                                        else if ($result->status == '8')
                                                            $status = 'Appointment Timed out.';
                                                        else if ($result->status == '9')
                                                            $status = 'Customer has cancelled with fee.';
                                                        else
                                                            $status = 'Status unavailable.';
                                                        ?>
                                                    <td class="v-align-middle"><?php echo $status; ?></td>
                                                    <!--                                            --><?php // if( $result->inv_id){  ?>
                                                    <!--                                                <td class="v-align-middle"><a href="http://www.privemd.com/invoice/--><?php //echo $result->appointment_id  ?><!--.pdf" target="_blank"><button class="btn btn-success btn-cons" style="min-width: 83px !important;">Download</button></a></td>-->
                                                    <!--                                            --><?php //} else{  ?>
                                                    <!--                                                <td class="v-align-middle">--</td>-->
                                                    <!--                                            --><?php //}  ?>
                                                </tr>
                                                <?php
                                                $slno++;
                                            }
//                                            
                                            ?>
                                        </tbody>
                                    </table></div><div class="row"></div></div>
                        </div>
                    </div>
                    <!-- END PANEL -->
                </div>



            </div>


        </div>
        <!-- END JUMBOTRON -->

        <!-- START CONTAINER FLUID -->
        <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

            <!-- END PLACE PAGE CONTENT HERE -->
        </div>
        <!-- END CONTAINER FLUID -->

    </div>

</div>