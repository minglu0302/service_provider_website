<?php

/*
 * For more details refer->    amazon_s3_php/example.php
 *  */
//echo json_encode(array('msg' => '2'));
require_once 'S3.php';
require_once '../../../../Models/config.php';

$dat = getdate();
$rename_file = "file" . $dat['year'] . $dat['mon'] . $dat['mday'] . $dat['hours'] . $dat['minutes'] . $dat['seconds'] . "." . $ext;

 $emailId = $_REQUEST["emailId"];
 
 $pro_pic = $_FILES["myfile"]["name"];
        $size = $_FILES['myfile']['size'];
        $tmp = $_FILES['myfile']['tmp_name'];
        
        $pro_pic_ext = substr($pro_pic, strrpos($pro_pic, '.') + 1);
        $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg", "PNG", "JPG", "JPEG", "GIF", "BMP");
        $msg = false;
        $bucket = BUCKET_NAME;
        $s3 = new S3(AMAZON_AWS_ACCESS_KEY, AMAZON_AWS_AUTH_SECRET);
        $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);

        if (strlen($pro_pic) > 0) {
            if (in_array($pro_pic_ext, $valid_formats)) {
                if ($size < (1024 * 1024)) {
                    $actual_image_name = "ProfileImages/" . $emailId . "." . $pro_pic_ext;
                    if ($s3->putObjectFile($tmp, $bucket, $actual_image_name, S3::ACL_PUBLIC_READ)) {
                        $msg = true;
                        $s3file = 'http://' . $bucket . '.s3.amazonaws.com/' . $actual_image_name;
                    } else
                        $msg = false;
                } else
                    $msg = false;
            } else
                $msg = false;
        } else
            $msg = false;
        if ($msg == false){
            return false;
        }
       
        
        
        
echo json_encode(array('msg' => $msg, 'fileName' => $s3file));

?>