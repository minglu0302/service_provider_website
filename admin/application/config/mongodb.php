<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// host name or System IP address
$config['default']['mongo_hostbase'] = MONGODB_HOST;

//Database Name
$config['default']['mongo_database'] = MONGODB_DB;

//DB Username - by default, it is empty
$config['default']['mongo_username'] = MONGODB_USER;

//DB Password - by default, it is empty
$config['default']['mongo_password'] = MONGODB_PASS;

