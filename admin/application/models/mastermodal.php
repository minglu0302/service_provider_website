<?php

if (!defined("BASEPATH"))
    exit("Direct access to this page is not allowed");

require_once 'StripeModule.php';
require_once '/var/www/html/Models/config.php';

class Mastermodal extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('mongo_db');
        $this->load->database();
    }

    function getBookingData($status) {
        if ($status != '7')
            $status = "1,2,3,4,5,6,7,8,9";
        else
            $status = "7,9";
        $query = $this->db->query("select ap.pg_commision,ap.app_commision,ap.address_line1,ap.appointment_dt,ap.bid,ap.inv_id,ap.status,ap.discount,ap.amount,d.email as mas_email,d.first_name as mas_fname,d.last_name as mas_lname,p.email as slv_email,p.first_name as slv_fname,p.last_name as slv_lname from appointment ap,doctor d,patient p where ap.doc_id = d.doc_id and ap.cust_id = p.patient_id and ap.doc_id='" . $this->session->userdata('admin_session')['LoginId'] . "' and ap.status IN(" . $status . ") order by ap.appointment_id DESC")->result(); //get_where('slave', array('email' => $email, 'password' => $password));
        return $query;
    }

    function getCatlist() {

        $proid = (int) $this->session->userdata('admin_session')['LoginId'];

        $masData = $this->mongo_db->get_one('location', array('user' => $proid));

        $data = array();
        foreach ($masData['catlist'] as $val) {
            $catname = $this->mongo_db->get_one('Category', array('_id' => new MongoId($val['cid'])));
            $data[] = array('cid' => $val['cid'],'amount' => $val['amount'],'cat_name' => $catname['cat_name'],'price_set_by' => $catname['price_set_by'], 'status' => $val['status']);
        }
        return $data;
    }

    function allbooking_data_ajax() {
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');
        if ($this->input->post('sSearch') != '') {
            $cond = array('name' => array('$regex' => new MongoRegex("/^{$this->input->post('sSearch')}/i")));
        } else {
            $cond = array('status' => array('$in' => array(2, 3, 4, 5, 6, 21, 22, 7)), 'provider_id' => (string) $this->session->userdata('admin_session')['LoginId']);
        }
        $data = array();
        $bookingData = $this->mongo_db->get_where('bookings', $cond)->sort(array('_id' => -1));
        foreach ($bookingData as $row) {
            if ($row['booking_type'] == 1)
                $booking_type = "Now";
            else if ($row['booking_type'] == 2)
                $booking_type = "Later";
            else if ($row['booking_type'] == 3)
                $booking_type = "Now";

            $status = $this->GetBookingStatusMessage($row['status']);


            $data[] = array($row['bid'],
                $row['customer']['id'], $row['customer']['fname'], date('d-m-Y h:i a', strtotime($row['appt_date'])), $row['cat_name'],
                $booking_type, $row['address1'], $status);
        }
        echo $this->datatables->commission_Data($data);
    }

    function Provider_Pay($proid = '') {
        $query1 = "select round(sum(pro_earning),2) as BookingEarning  from bookings where  pro_id = " . $proid . " and payment_status = 0";
        $query2 = "select IFNULL(round((select closing_balance from payroll where mas_id = " . $proid . " order by payroll_id desc limit 1),2),0) as ClosingBal";

        $res1 = $this->db->query($query1)->row();
        $res2 = $this->db->query($query2)->row();
        return round($res1->BookingEarning + $res2->ClosingBal, 2);
    }

    function Driver_pay($masid = '') {
        $query = "select sum(a.doc_amount) as total,m.first_name,"
                . "(select count(settled_flag) from appointment where settled_flag = 0 and doc_id = a.doc_id and doc_amount != 0 and status = 7 and payment_status IN (1,3)) as unsettled_amount_count,"
                . "(select sum(app_commision) from appointment where settled_flag = 0 and payment_type = 2 and doc_id = a.doc_id and doc_amount != 0 and status = 7 ) as driver_hasTo_pay,"
                . "(select sum(doc_amount) from appointment where settled_flag = 0 and  payment_type = 1 and doc_id = a.doc_id and doc_amount != 0 and status = 7) as app_owner_hasTo_pay,"
                . "(select appointment_id from appointment where settled_flag = 0 and doc_id = a.doc_id and status = 7 and payment_status IN (1,3) order by appointment_id DESC limit 0,1) as last_unsettled_appointment_id from appointment a,doctor m where a.doc_id = '" . $masid . "' and a.doc_id = m.doc_id and settled_flag = 0 and a.status = 7 and a.payment_status in (1,3)";
        return $this->db->query($query)->result();
    }

    function get_payrolldata($id = '') {
        $quaery = $this->db->query("SELECT * from payroll WHERE  mas_id = '" . $id . "'")->result();
        return $quaery;
    }

    function Totalamountpaid($id = '') {
        $quaery = $this->db->query("SELECT sum(pay_amount) as totalamt from payroll WHERE  mas_id = '" . $id . "'")->result();
        return $quaery;
    }

    function patientDetails($mas_id = '') {
        $this->load->library('Datatables');
        $this->load->library('table');
// case  when TRUNCATE((select sum(doc_amount) from appointment where doc_id = 
// doc.doc_id and DATE(appointment_dt) = '" . $explodeDateTime[0] . "' and status = 7),2)  
// IS NULL then '--'  else TRUNCATE((select sum(doc_amount) from appointment where doc_id 
        $this->datatables->select('b.bid,b.appointment_dt,b.pro_earning,(case p.status when 1 then "Paid" when 2 then "Partially Paid" else "Not Paid" end) as payment_status', false)
                ->where('p.bid = b.bid and p.payid = "' . $mas_id . '"')
                ->from('bookings b, pay_cycle p', false);
        echo $this->datatables->generate();
    }

    function getBilledAmount($bid) {

        $this->load->library('mongo_db');
        $cond = array('bid' => $bid);

        $bookingData = $this->mongo_db->get_one('bookings', $cond);

        return $bookingData;
    }

    function getTransectionData() {

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->library('mongo_db');
        if ($this->input->post('sSearch') != '') {
            $cond = array('name' => array('$regex' => new MongoRegex("/^{$this->input->post('sSearch')}/i")));
        } else {
            $cond = array('status' => 7, 'provider_id' => (string) $this->session->userdata('admin_session')['LoginId']);
        }
//        echo $this->session->userdata('admin_session')['LoginId'];
        $data = array();
        $bookingData = $this->mongo_db->get_where('bookings', $cond)->sort(array('_id' => -1));
        foreach ($bookingData as $row) {
            if ($row['booking_type'] == 1)
                $booking_type = "Now";
            else if ($row['booking_type'] == 2)
                $booking_type = "Later";
            else if ($row['booking_type'] == 3)
                $booking_type = "Now";
            $pg_Commission = $row['invoiceData']['pro_pg_comm'] + $row['invoiceData']['app_pg_comm'];
            if ($row['payment_type'] == 1)
                $payment_type = "CASH";
            else if ($row['payment_type'] == 2)
                $payment_type = "CARD";
            $status = $this->GetBookingStatusMessage($row['status']);

            $data[] = array($row['bid'],
                $row['customer']['id'], $row['customer']['fname'], date('d-m-Y h:i a', strtotime($row['appt_date'])), $row['cat_name'],
                $booking_type, $row['fee_type'],
                '<a id="dis"  onclick="coupon_discount(' . $row['bid'] . ')">' . $row['coupon_discount'] . '</a>',
                '<a id="billed_amt" " onclick="billed_amount(' . $row['bid'] . ')">' . $row['invoiceData']['billed_amount'] . '</a>',
                '<a id="billed_amt" " onclick="pro_earning(' . $row['bid'] . ')">' . $row['invoiceData']['pro_earning'] . '</a>',
                '<a id="billed_amt" " onclick="app_earning(' . $row['bid'] . ')">' . $row['invoiceData']['app_earning'] . '</a>',
                '<a id="billed_amt" " onclick="pg_Commission(' . $row['bid'] . ')">' . $pg_Commission . '</a>',
                $row['invoiceData']['txn_id'], $status,
                $row['invoiceData']['payment_status'],
                $payment_type, '<a id="download" target="_blank" href="' . base_url() . '../getPDF.php?apntId=' . $row['bid'] . '">Download</a>');
        }
        echo $this->datatables->commission_Data($data);
    }

    function GetBookingStatusMessage($status) {
        $message = "Status Unavailable.";
        if ($status == 1) {
            $message = 'Provider Requested.';
        } else if ($status == 2) {
            $message = 'Provider accepted.';
        } else if ($status == 3) {
            $message = 'Provider rejected.';
        } else if ($status == 4) {
            $message = 'Customer has cancelled with out fee.';
        } else if ($status == 5) {
            $message = 'Provider is on the way.';
        } else if ($status == 21) {
            $message = 'Provider arrived.';
        } else if ($status == 6) {
            $message = 'JOB started.';
        } else if ($status == 22) {
            $message = 'Invoice Raised.';
        } else if ($status == 7) {
            $message = 'JOB completed.';
        } else if ($status == 9) {
            $message = 'Customer has cancelled with fee.';
        } else if ($status == 10) {
            $message = 'Provider Cancelled';
        }
        return $message;
    }

    function validateSuperAdmin($doc = '') {
        $email = $this->input->post("email");
        $password = $this->input->post("password");
        $offset = $this->input->post("offset");
        $this->load->library('nativesession');
        $queryformaster = $this->db->get_where('doctor', array('email' => $email, 'password' => md5($password)));
        $resmas = $queryformaster->row();
//       $newdata = array(
//                   'username'  => 'johndoe',
//                   'email'     => 'johndoe@some-site.com',
//                   'logged_in' => TRUE
//               );
//
//$this->session->set_userdata($newdata);
//        echo 'a';
//       print_r($this->session->userdata('role'));die;

        if ($queryformaster->num_rows > 0 && $email != '' && $password != '') {
            if ($queryformaster->num_rows > 0) {
                if ($resmas->status == '1' || $resmas->status == '2')
                    return array('Message' => 'Your profile is under verification, please wait for our representative to reach you.');
                else if ($resmas->status == '4')
                    return array('Message' => 'Your profile is suspended by admin, please contact your company for further queries');
                $tablename = 'master';
                $LoginId = 'doc_id';
                $role = 'master';
                $sessiondata = $this->setsessiondata($tablename, $LoginId, $resmas, $email, $password, $role, $offset);

                $this->session->set_userdata($sessiondata);
//                 echo 'a';
//        print_r($this->session->userdata('admin_session')['first_name']);die;
                return true;
            }
        }else if ($this->session->userdata('role') == "Super Admin") {
            $queryformaster = $this->db->get_where('doctor', array('doc_id' => $doc));
            $resmas = $queryformaster->row();
            $email = $password = 'super';
            $tablename = 'master';
            $LoginId = 'doc_id';
            $role = 'super';
            $sessiondata = $this->setsessiondata($tablename, $LoginId, $resmas, $email, $password, $role);

            $this->session->set_userdata($sessiondata);

            return true;
        }
        return false;
    }

    function setsessiondata($tablename, $LoginId, $res, $email, $password, $role, $off) {

        $offset = -($off);
        $sessiondata = array(
            'emailid' => $email,
            'password' => $password,
            'LoginId' => $res->$LoginId,
            'profile_pic' => $res->profile_pic,
            'first_name' => $res->first_name,
            'last_name' => $res->last_name,
            'table' => $tablename,
            'Role' => $role,
            'validate' => true,
            'offset' => $offset
        );
//        return $sessiondata;
        $ses_data = array('admin_session' => $sessiondata);
        return $ses_data;
    }

    function get_all_data($stdate, $enddate, $status) {
        if ($status != '7')
            $status = '1,2,3,4,5,6,7,8,9';
        else
            $status = "7,9";

//        if ($stdate || $enddate) {
//            $query = $this->db->query("select ap.pg_commision,ap.doc_amount,ap.app_commision,ap.address_line1,ap.last_modified_dt,ap.appt_long,ap.appt_lat,ap.address_line1,ap.address_line2,ap.appointment_dt as appointment_date,ap.appointment_id as Bookin_Id,ap.inv_id,ap.status,ap.amount,d.email as Driver_email,d.first_name as Driver_First_Name,d.last_name as Driver_Last_Name,p.email as Passenger_email,p.first_name as Passenger_fname,p.last_name as Passenger_lname from appointment ap,doctor d,patient p where ap.doc_id = d.doc_id and ap.cust_id = p.patient_id and DATE(ap.appointment_dt) BETWEEN '" . date('Y-m-d', strtotime($stdate)) . "' AND '" . date('Y-m-d', strtotime($enddate)) . "' and ap.doc_id='" . $this->session->userdata('admin_session')['LoginId'] . "' and ap.status IN(" . $status . ") order by ap.appointment_id DESC");
//        } else {
//            $query = $this->db->query("select ap.pg_commision,ap.doc_amount,ap.app_commision,ap.address_line1,ap.last_modified_dt,ap.appt_long,ap.appt_lat,ap.address_line1,ap.address_line2,ap.appointment_dt as appointment_date,ap.appointment_id as Bookin_Id,ap.inv_id,ap.status,ap.amount,d.email as Driver_email,d.first_name as Driver_First_Name,d.last_name as Driver_Last_Name,p.email as Passenger_email,p.first_name as Passenger_fname,p.last_name as Passenger_lname from appointment ap,doctor d,patient p where ap.doc_id = d.doc_id and ap.cust_id = p.patient_id and  ap.doc_id='" . $this->session->userdata('admin_session')['LoginId'] . "' and ap.status IN(" . $status . ") order by ap.appointment_id DESC");
//        }

        if ($this->input->post('sSearch') != '') {
            $cond = array('name' => array('$regex' => new MongoRegex("/^{$this->input->post('sSearch')}/i")));
        } else {
            $cond = array('status' => 7);
        }

        $data = array();
        $bookingData = $this->mongo_db->get_where('bookings', $cond)->sort(array('_id' => -1));
        foreach ($bookingData as $row) {
            if ($row['booking_type'] == 1)
                $booking_type = "With Dispatcher";
            else if ($row['booking_type'] == 2)
                $booking_type = "Later";
            else if ($row['booking_type'] == 3)
                $booking_type = "WithOut Dispatcher";
            $pg_Commission = $row['invoiceData']['pro_pg_comm'] + $row['invoiceData']['app_pg_comm'];
            if ($row['payment_type'] == 1)
                $payment_type = "CASH";
            else if ($row['payment_type'] == 2)
                $payment_type = "CARD";
            $status = $this->GetBookingStatusMessage($row['status']);

            $data[] = array('JOB ID' => $row['bid'], 'PROVIDER ID' => $row['provider_id'],
                'PROVIDER NAME' => $row['provider']['fname'],
                'CUSTOMER ID' => $row['customer']['id'], 'CUSTOMER NAME' => $row['customer']['fname'],
                'DATE' => $row['appt_date'],
                'CATEGORY NAME' => $row['cat_name'],
                'BOOKING TYPE' => $booking_type,
                'FEE TYPE' => $row['fee_type'],
                'PAYMENT GATEWAY COMMISSION' => $pg_Commission,
                'STATUS' => $status,
                'PAYMENT STATUS' => $row['invoiceData']['payment_status'],
                'PAYMENT TYPE' => $payment_type);
        }
        return $data;
    }

    function getDatafromTransectionstatus($tstatus = '') {

        $query = $this->db->query("select ap.pg_commision,ap.doc_amount,ap.app_commision,ap.address_line1,ap.appointment_dt,ap.appointment_id,ap.inv_id,ap.status,ap.amount,d.email as mas_email,d.first_name as mas_fname,d.last_name as mas_lname,p.email as slv_email,p.first_name as slv_fname,p.last_name as slv_lname from appointment ap,doctor d,patient p where ap.pro_id = d.doc_id and ap.cust_id = p.patient_id and ap.pro_id='" . $this->session->userdata("admin_session")['LoginId'] . "' and ap.status IN(" . $tstatus . ") order by ap.appointment_id DESC")->result(); //get_where('slave', array('email' => $email, 'password' => $password));
        return $query;
    }

    function getDatafromdate($stdate, $enddate, $status) {
        if ($status != '7')
            $status = '1,2,3,4,5,6,7,8,9';
        else
            $status = "7,9";

        $query = $this->db->query("select ap.pg_commision,ap.doc_amount,ap.app_commision,ap.address_line1,ap.appointment_dt,ap.appointment_id,ap.inv_id,ap.status,ap.amount,d.email as mas_email,d.first_name as mas_fname,d.last_name as mas_lname,p.email as slv_email,p.first_name as slv_fname,p.last_name as slv_lname from appointment ap,doctor d,patient p where ap.pro_id = d.doc_id and ap.cust_id = p.patient_id and DATE(ap.appointment_dt) BETWEEN '" . date('Y-m-d', strtotime($stdate)) . "' AND '" . date('Y-m-d', strtotime($enddate)) . "' and ap.pro_id='" . $this->session->userdata("admin_session")['LoginId'] . "' and ap.status IN(" . $status . ") order by ap.appointment_id DESC")->result(); //get_where('slave', array('email' => $email, 'password' => $password));
        return $query;
    }

    function getDataSelected($selectdval, $status) {
        if ($status != '7')
            $status = '1,2,3,4,5,6,7,8,9';
        else
            $status = "7,9";

        $query = $this->db->query("select ap.address_line1,ap.appointment_dt,ap.payment_type,ap.appointment_id,ap.inv_id,ap.status,ap.amount,d.email as mas_email,d.first_name as mas_fname,d.last_name as mas_lname,p.email as slv_email,p.first_name as slv_fname,p.last_name as slv_lname from appointment ap,doctor d,patient p where ap.pro_id = d.doc_id and ap.patient_id = p.patient_id and ap.payment_type = '" . $selectdval . "' and ap.pro_id='" . $this->session->userdata("admin_session")['LoginId'] . "' and ap.status IN(" . $status . ") order by ap.appointment_id DESC")->result(); //get_where('slave', array('email' => $email, 'password' => $password));
        return $query;
    }

    function getuserinfo($param = '') {

//        $query = $this->db->query("SELECT * FROM doctor where doc_id='" . $this->session->userdata('admin_session')['LoginId'] . "' ")->row();
        if ($param == '')
            $query = $this->db->query("select d.amount,d.board_certificate,d.medical_license_num,d.address_line1,d.address_line2,d.zipcode,d.cityName,d.countryName,d.board_certificate,d.board_certification_expiry_dt,d.medical_license_pic, d.doc_id,d.languages,d.expertise,d.first_name,d.profile_pic,d.last_name,d.about,d.mobile,d.country_code,d.tax_num,d.medical_license_num,d.email,d.created_dt,d.profile_pic,d.status,d.num_of_job_images from doctor d where  d.doc_id=" . $this->session->userdata('admin_session')['LoginId'] . "")->row();
        else
            $query = $this->db->query("select d.amount,d.board_certificate,d.medical_license_num,d.address_line1,d.address_line2,d.zipcode,d.cityName,d.countryName,d.board_certificate,d.board_certification_expiry_dt,d.medical_license_pic, d.doc_id,d.languages,d.expertise,d.first_name,d.profile_pic,d.last_name,d.about,d.mobile,d.country_code,d.tax_num,d.medical_license_num,d.email,d.created_dt,d.profile_pic,d.status,d.num_of_job_images from doctor d where  d.doc_id=" . $param . "")->row();


        return $query;
    }

    function GetAllCatForProvider() {
        $proid = (int) $this->session->userdata('admin_session')['LoginId'];
        $cursor = $this->mongo_db->get_where('ProviderCategory', array('prolist.pid' => $proid));
        $arr = array();
        foreach ($cursor as $data) {
            $data['id'] = (string) $data['_id'];
            $arr[] = $data;
        }
        return $arr;
    }

    function GetUserFromMongo() {
        $proid = (int) $this->session->userdata('admin_session')['LoginId'];
        $prodata = $this->mongo_db->get_one('location', array('user' => (int) $proid));
        return $prodata;
    }

    function GetjobImages() {
        $proid = (int) $this->session->userdata('admin_session')['LoginId'];
        $cursor = $this->mongo_db->get_where('location', array('user' => $proid));
        $arr = array();
        foreach ($cursor as $data) {
            $arr = $data['job_images'];
        }
        return $arr;
    }

    function getuseraddress($param = '') {


        if ($param == '')
            $query = $this->db->query("select w.phone,w.address_line1,w.address_line2,w.city,w.country,(select City_Name from city where City_Id = w.city) as cityName,(select Country_Name from country where Country_Id = w.country) as countryName from doctor w where doc_id=" . $this->session->userdata('admin_session')['LoginId'] . ")")->row();
        else
            $query = $this->db->query("select w.phone,w.address_line1,w.address_line2,w.city,w.country,(select City_Name from city where City_Id = w.city) as cityName,(select Country_Name from country where Country_Id = w.country) as countryName from doctor w where doc_id=" . $param . ")")->row();

        return $query;
    }

    function editmasterpassword() {
        $newpass = $this->input->post('newpass');
        $currentpassword = $this->input->post('currentpassword');
        $docId = ($this->session->userdata('admin_session')['LoginId']);
        $pass = $this->db->query("select password from doctor where doc_id=" . $docId)->result();

//        print_r(md5($currentpassword));
//        print_r($pass[0]->password);
        if (md5($currentpassword) != $pass[0]->password) {

            echo json_encode(array('msg' => "you have entered the incorrect current password", 'flag' => 2));
            return;
        } else {

            if ($pass['password'] == md5($newpass)) {
                echo json_encode(array('msg' => "this password already exists. Enter new password", 'flag' => 1));
                return;
            } else {
                $this->db->query("update doctor set password = md5('" . $newpass . "') where doc_id=" . $docId);

                if ($this->db->affected_rows() > 0) {
                    echo json_encode(array('msg' => "your new password updated successfully", 'flag' => 0));
                    return;
                }
            }
        }
    }

    function getuserlocation() {
        $proid = (int) $this->session->userdata('admin_session')['LoginId'];
        $pdata = $this->mongo_db->get_one('location', array('user' => $proid));

        return $query;
    }

    function deletegallerydata() {

        $this->db->where('image', $this->input->post('url'));
        $this->db->delete('images');

        echo json_encode(array('name' => $this->input->post('url')));
    }

    function getgalleryimage($param = '') {
        if ($param == '')
            $query = $this->db->query("SELECT * from images WHERE  doc_id='" . $this->session->userdata('admin_session')['LoginId'] . "' ")->result();
        else {
            $query = $this->db->query("SELECT * from images WHERE  doc_id='" . $param . "' ")->result();
        }
        return $query;
    }

    function getPassangerBooking() {
        $query = $this->db->query("select a.appointment_id,a.last_modified_dt,a.amount,a.inv_id,a.distance,a.appointment_dt,a.drop_addr1,a.drop_addr2,a.doc_id,a.patient_id,d.first_name as doc_firstname,d.profile_pic as doc_profile,d.last_name as doc_lastname,p.first_name as patient_firstname,p.last_name as patient_lastname,a.address_line1,a.address_line2,a.status from appointment a,doctor d,patient p where a.patient_id=p.patient_id and d.doc_id=a.doc_id  and a.patient_id='" . $this->session->userdata("admin_session")['LoginId'] . "' order by a.appointment_id desc")->result(); //get_where('slave', array('email' => $email, 'password' => $password));
        return $query;
    }

    function geteducationdata($param = '') {

        if ($param == '')
            $query = $this->db->query("select * from doctor_education WHERE doc_id='" . $this->session->userdata('admin_session')['LoginId'] . "' ")->result();
        else {
            $query = $this->db->query("select * from doctor_education WHERE doc_id='" . $param . "' ")->result();
        }

        return $query;
    }

    function updateeducation() {


        $education = $this->input->post('education');
        if (!empty($education)) {

            if ($education) {
                for ($i = 0; $i < count($education); $i++) {

                    $data[$i] = array('degree' => $education[$i]['degree'], 'end_year' => $education[$i]['end_year'], 'start_year' => $education[$i]['start_year'], 'institute' => $education[$i]['institute'], 'doc_id' => $this->session->userdata("admin_session")['LoginId']);

                    if ($education[$i]['degree'] != '' || $education[$i]['end_year'] != 'null' || $education[$i]['start_year'] != 'null' || $education[$i]['institute'] != '')
                        $this->db->insert('doctor_education', $data[$i]);
                }
            }
        }

        $educationupdate = $this->input->post('educationupdate');
        if ($educationupdate) {
            for ($i = 0; $i < count($educationupdate); $i++) {

                $data[$i] = array('degree' => $educationupdate[$i]['degree'], 'end_year' => $educationupdate[$i]['end_year'], 'start_year' => $educationupdate[$i]['start_year'], 'institute' => $educationupdate[$i]['institute']);

                $id = $educationupdate[$i]['ed_id'];
                $this->db->update('doctor_education', $data[$i], array('ed_id' => $id));
            }
        }
    }

    function addservices() {
        $data = $this->input->post('servicedata');
        $this->db->insert('services', $data);
    }

    function updateservices($table = '') {
        $formdataarray = $this->input->post('editservicedata');
        $id = $this->input->post('id');
        $this->db->update($table, $formdataarray, array('service_id' => $id));
    }

    function deleteservices($table = '') {
        $id = $this->input->post('id');
        $this->db->where('service_id', $id);
        $this->db->delete($table);
    }

    function getActiveservicedata() {
        $query = $this->db->query("select * from services")->result(); //get_where('slave', array('email' => $email, 'password' => $password));
        return $query;
    }

    function delete_education() {

        $this->db->where('ed_id', $this->input->post('idtodelete'));
        $this->db->delete('doctor_education');

        echo json_encode(array('name' => $this->input->post('idtodelete')));
    }

    function week_start_end_by_date($date, $format = 'Y-m-d') {

        //Is $date timestamp or date?
        if (is_numeric($date) AND strlen($date) == 10) {
            $time = $date;
        } else {
            $time = strtotime($date);
        }

        $week['week'] = date('W', $time);
        $week['year'] = date('o', $time);
        $week['year_week'] = date('oW', $time);
        $first_day_of_week_timestamp = strtotime($week['year'] . "W" . str_pad($week['week'], 2, "0", STR_PAD_LEFT));
        $week['first_day_of_week'] = date($format, $first_day_of_week_timestamp);
        $week['first_day_of_week_timestamp'] = $first_day_of_week_timestamp;
        $last_day_of_week_timestamp = strtotime($week['first_day_of_week'] . " +6 days");
        $week['last_day_of_week'] = date($format, $last_day_of_week_timestamp);
        $week['last_day_of_week_timestamp'] = $last_day_of_week_timestamp;

        return $week;
    }

    function get_schedule_details() {
        $ca = 0;
        if ($this->input->post('slot_id') != '') {
            $ca = 1;
//            $query = $this->db->query("select a.status,a.appt_lat,a.appt_long,a.slot_id,p.email,p.first_name,p.phone,p.profile_pic,p.last_name,a.appointment_dt,a.address_line1,a.address_line2,a.extra_notes from patient p,bookings a,doctor ms WHERE a.slot_id='" . $this->input->post('slot_id') . "'  and a.patient_id = p.patient_id and a.doc_id='" . $this->session->userdata('admin_session')['LoginId'] . "'")->result();
            $bdata = $this->mongo_db->get_one('bookings', array('slot_id' => $this->input->post('slot_id')));
        } else if ($this->input->post('app_id') != '') {
            $ca = 2;
            $query = $this->db->query("select distinct  a.status,a.appt_lat,a.appt_long,a.slot_id,p.email,p.first_name,p.phone,p.profile_pic,p.last_name,a.appointment_dt,a.address_line1,a.address_line2,a.extra_notes from patient p,bookings a,doctor ms WHERE a.appointment_id=" . $this->input->post('app_id') . "  and a.patient_id = p.patient_id")->result();
        }

//        foreach ($query as $result) {
//            $pname = $result->first_name . " " . $result->last_name;
////            $datetime = date("F jS, Y h:i a", (strtotime($result->appointment_dt) - ($this->input->post('offset') * 60)));
//            $datetime = date("F jS, Y h:i a", (strtotime($result->appointment_dt)));
//            $applocation = $result->address_line1 . $result->address_line2;
//            $notes = $result->extra_notes;
//            $profile_pic = $result->profile_pic;
//            $phone = $result->phone;
//            $email = $result->email;
//            $appdate = $result->appointment_dt;
//            $slot_id = $result->slot_id;
//            $lat = $result->appt_lat;
//            $long = $result->appt_long;
//            $status = $result->status;
//            $phone = $result->phone;
//        }

        echo json_encode(array('bdata' => $bdata));
    }

    function get_new_slot_details() {
        if ($this->input->post('slot_id') != '') {
            $slot_id = new MongoId($this->input->post('slot_id'));
            $slot_data = $this->mongo_db->get_one('slotbooking', array('_id' => $slot_id));
            $dated = date('d-m-Y h:i a', strtotime($slot_data['start_dt']));
            $edated = date('d-m-Y h:i a', strtotime($slot_data['end_dt']));


            $loc_data = $this->mongo_db->get_one('masterLocations', array('_id' => new MongoId($slot_data['locId'])));
            $slot_data['address'] = $loc_data['address1'];
            $slot_data['start_dt'] = $dated;
            $slot_data['end_dt'] = $edated;
            $bdata = $this->mongo_db->get_one('bookings', array('slot_id' => $this->input->post('slot_id')));
        }
        echo json_encode(array('slot_data' => $slot_data, 'bdata' => $bdata));
    }

    function updateDataProfile() {

        $jobimge = array();
//        $jobImageUrl = $this->input->post('jobImageUrl');
//        $jobims=array('img'=>$jobImageUrl);
//        array_push($jobimge, $jobims);
        $imgarr = $this->input->post('imgarr');
//        print_r($imgarr);die;
        foreach ($imgarr as $imgf) {
            $jobimge[] = array('url' => $imgf);
        }




        $formdataarray = $this->input->post('fdata');
        $c_code = $formdataarray['country_code'];
        unset($formdataarray['country_code']);
        $formdataarray['country_code'] = '+' . $c_code;
//        unset($formdataarray['radius']);

        $dataarray = $this->input->post('amt');
        if ($formdataarray['board_certification_expiry_dt'] != '') {
            $formdataarray['board_certification_expiry_dt'] = date('Y-m-d', strtotime(str_replace('-', '/', $formdataarray['board_certification_expiry_dt'])));
        }
        $this->db->update('doctor', $formdataarray, array('doc_id' => $this->session->userdata("admin_session")['LoginId']));

        $this->session->set_userdata(array(
            'profile_pic' => $formdataarray['profile_pic'],
            'first_name' => $formdataarray['first_name'],
            'last_name' => $formdataarray['last_name']
        ));

        $formdataarrayaddress = $this->input->post('useraddress');
//        $this->db->query('update workplace set address_line1="' . $formdataarrayaddress['address_line2'] . '",address_line2="' . $formdataarrayaddress['address_line2'] . '" where workplace_id = (select workplace_id from doctor where doc_id ="' . $this->session->userdata("admin_session")['LoginId'] . '" )'); //'doctor', $formdataarrayaddress, array('doc_id' => $this->session->userdata('admin_session')['LoginId']));
//        $con = new Mongo();
//        $PriveMd = $con->iserve_v2;
//        $location = $PriveMd->selectCollection('location');
        //  $location->update(array('user' => (int) $this->session->userdata("admin_session")['LoginId']), array('$set' => array('name' => $formdataarray['first_name'], 'lname' => $formdataarray['last_name'], 'image' => $formdataarray['profile_pic'], 'radius' => $this->input->post('radius'), 'amount' => $formdataarray['amount'])));
//        $condition = array('user' => (int) $this->session->userdata("admin_session")['LoginId']);
//        $data = array('$set' => array('name' => $formdataarray['first_name'], 'lname' => $formdataarray['last_name'], 'image' => $formdataarray['profile_pic'], 'radius' => $this->input->post('radius'), 'amount' => $formdataarray['amount']));
//        $this->mongo_db->where($condition);
//        $this->mongo_db->update('location',$data);
//         $con = new Mongo();
//        $PriveMd = $con->ontheway;
//        $location = $PriveMd->selectCollection('location');
//         $sel_cat = array();
//        foreach ($formdataarray['priceset'] as $type) {
//           
//            print_r($type); die; 
//        }

        $proid = $this->session->userdata('admin_session')['LoginId'];
//       $this->mongo_db->update('location', array('amount' => $formdataarray['amount']), array('catlist' => (array ('cid' => $proid )) ));
        $sel_cat = array();
        foreach ($dataarray['priceset'] as $index => $code) {

            $sel_cat[] = array('cid' => $dataarray['catid'][$index], 'status' => 0, 'amt' => $code);
            $this->mongo_db->update('location', array('catlist.$.amount' => $code), array('user' => (int) $proid, 'catlist.cid' => $dataarray['catid'][$index]));
        }
        $this->mongo_db->update('location', array('job_images' => $jobimge, 'name' => $formdataarray['first_name'], 'lname' => $formdataarray['last_name'], 'image' => $formdataarray['profile_pic'], 'radius' => $this->input->post('radius'), 'amount' => $formdataarray['amount']), array('user' => (int) $proid));
    }

    function deletejobPhoto() {
        $photo = $this->input->post('photo');
        $jobimges = array();
        $proid = (int) $this->session->userdata('admin_session')['LoginId'];
        $cursor = $this->mongo_db->get_where('location', array('user' => $proid));

        foreach ($cursor as $data) {
            foreach ($data['jobImage'] as $imge) {
                foreach ($imge as $imges) {
                    if ($photo != $imges) {
                        $jobims = array('img' => $imges);
                        array_push($jobimges, $jobims);
                    }
                }
            }
        }

        $this->mongo_db->update('location', array('jobImage' => $jobimges), array('user' => (int) $proid));
        return array('flag' => 1, 'message' => "Job Photo Deleted");
    }

    function updategallery() {

        $images = $this->input->post('newimage');
        if ($images)
            $this->db->insert('images', $images);
    }

    function updateMasterBank() {
//        $this->load->library('StripeModule');
        $stripe = new StripeModule();

        $checkStripeId = $this->db->query("SELECT stripe_id from doctor where doc_id = " . $this->session->userdata("admin_session")['LoginId'])->row();

//        if (!is_array($checkStripeId)) {
//            return array('flag' => 2);
//        }

        $userData = $this->input->post('fdata');

        if ($checkStripeId->stripe_id == '') {
            $createRecipientArr = array('name' => $userData['name'], 'type' => 'individual', 'email' => $userData['email'], 'tax_id' => (string) $userData['tax_id'], 'bank_account' => (string) $userData['account_number'], 'routing_number' => (string) $userData['routing_number'], 'description' => 'For ' . $userData['email']);
            $recipient = $stripe->apiStripe('createRecipient', $createRecipientArr);
        } else {
            $updateRecipientArr = array('stripe_id' => $checkStripeId->stripe_id, 'name' => $userData['name'], 'email' => $userData['email'], 'tax_id' => (string) $userData['tax_id'], 'bank_account' => (string) $userData['account_number'], 'routing_number' => (string) $userData['routing_number'], 'description' => 'For ' . $userData['email']);
            $recipient = $stripe->apiStripe('updateRecipient', $updateRecipientArr);
        }
        if (isset($recipient['error'])) {
            return array('flag' => 1, 'message' => $recipient['error']['message'], 'data' => $userData, 'er' => $recipient); //, 'args' => $recipient);"Error, please verify the details once and try again."
        } else if ($recipient['verified'] === FALSE) {
            return array('flag' => 1, 'message' => "Unable to verify, Need your full, legal name, you can check more details with the below link<br>https://support.stripe.com/questions/how-do-i-verify-transfer-recipients", 'link' => 'https://support.stripe.com/questions/how-do-i-verify-transfer-recipients', 'data' => $userData);
        } else if ($recipient['verified'] === TRUE) {
            $this->db->query("update doctor set stripe_id = '" . $recipient['id'] . "' where doc_id = '" . $this->session->userdata("admin_session")['LoginId'] . "'");
            return array('flag' => 0, 'message' => "Updated bank details successfully", 'data' => $userData);
        }

        return array('flag' => 0);
    }

    function Getdashboarddata() {
        $currTime = time();
        // today completed booking count
        $today = date('Y-m-d', $currTime);
        $todayone = $this->db->query("SELECT ap.bid  FROM bookings ap,doctor d ,patient p WHERE ap.cust_id = p.patient_id and ap.pro_id = d.doc_id and ap.pro_id='" . $this->session->userdata('admin_session')['LoginId'] . "' and ap.appointment_dt like '" . date('Y-m-d') . "%' and ap.status = 7 ");

        //$today
        //this week completed booking
        $weekArr = $this->week_start_end_by_date($currTime);
        $week = $this->db->query("SELECT ap.bid  FROM bookings ap,doctor d ,patient p WHERE ap.cust_id = p.patient_id and ap.pro_id = d.doc_id and ap.pro_id='" . $this->session->userdata('admin_session')['LoginId'] . "' and ap.status = 7 and DATE(ap.appointment_dt) >= '" . $weekArr['first_day_of_week'] . "'");
        $currMonth = date('n', $currTime);
        $month = $this->db->query("SELECT ap.bid  FROM bookings ap,doctor d ,patient p WHERE ap.cust_id = p.patient_id and ap.pro_id = d.doc_id and ap.pro_id='" . $this->session->userdata('admin_session')['LoginId'] . "' and ap.status = 7 and  MONTH(ap.appointment_dt) = '" . $currMonth . "' ");
        $lifetime = $this->db->query("SELECT ap.bid  FROM bookings ap,doctor d ,patient p WHERE ap.cust_id = p.patient_id and ap.pro_id = d.doc_id and ap.pro_id='" . $this->session->userdata('admin_session')['LoginId'] . "' and ap.status = 7 ");
        $totaluptodate = $this->db->query("SELECT ap.bid  FROM bookings ap,doctor d ,patient p WHERE ap.cust_id = p.patient_id and ap.pro_id = d.doc_id and ap.pro_id='" . $this->session->userdata('admin_session')['LoginId'] . "' and ap.status = 7");
        $explodeDateTime = explode(' ', date("Y-m-d H:s:i"));
        $todayearning = $this->db->query("select sum(pro_earning) as totamount from bookings,doctor doc where bookings.pro_id = doc.doc_id and DATE(appointment_dt) = '" . $explodeDateTime[0] . "' and bookings.status = 7 and doc.status = 3 and doc.doc_id =" . $this->session->userdata('admin_session')['LoginId']);

        //this week completed booking
        $weekData = $this->week_start_end_by_date(date("Y-m-d H:s:i"));
        $weekearning = $this->db->query("select sum(pro_earning) as totamount from bookings ap, doctor doc where doc.doc_id = ap.pro_id and ap.status = 7 and doc.status = 3 and doc.doc_id ='" . $this->session->userdata('admin_session')['LoginId'] . "' and DATE(appointment_dt) BETWEEN '" . $weekData['first_day_of_week'] . "' and '" . $weekData['last_day_of_week'] . "'");


        // this month completed booking
        $explodeDate = explode('-', $explodeDateTime[0]);
        $monthearning = $this->db->query("select sum(pro_earning) as totamount from bookings ap, doctor doc where ap.pro_id = doc.doc_id and doc.status=3 and ap.status = 7 and doc.doc_id='" . $this->session->userdata('admin_session')['LoginId'] . "' and DATE_FORMAT(appointment_dt,  '%Y-%m') = '" . $explodeDate[0] . '-' . $explodeDate[1] . "'");


        // lifetime completed booking
        $lifetimeearning = $this->db->query("select sum(pro_earning) as totamount from bookings ap, doctor doc where ap.pro_id = doc.doc_id and ap.status = 7 and doc.status=3 and doc.doc_id='" . $this->session->userdata('admin_session')['LoginId'] . "'");

        // total booking uptodate
        $totaluptodateearning = $this->db->query("SELECT  sum(ap.pro_earning)  FROM bookings ap,doctor d ,patient p WHERE ap.cust_id = p.patient_id and ap.pro_id = d.doc_id and ap.pro_id='" . $this->session->userdata('admin_session')['LoginId'] . "' and ap.status = 7");


        $t = $todayearning->row();
        $w = $weekearning->row();
        $m = $monthearning->row();
        $l = $lifetimeearning->row();
        $te = $totaluptodateearning->row();

        $data = array('today' => $todayone->num_rows(), 'week' => $week->num_rows(), 'month' => $month->num_rows(), 'lifetime' => $lifetime->num_rows(), 'total' => $totaluptodate->num_rows(),
            'todayearning' => $t->totamount, 'weekearning' => $w->totamount, 'monthearning' => $m->totamount, 'lifetimeearning' => $l->totamount, 'totalearning' => $te
        );
        return $data;
    }

    function updateData($IdToChange = '', $databasename = '', $db_field_id_name = '') {
        $formdataarray = $this->input->post('fdata');
        $this->db->update($databasename, $formdataarray, array($db_field_id_name => $IdToChange));
    }

    function issessionset() {

        $this->load->library('nativesession');
        if ($this->session->userdata('admin_session')['emailid'] && $this->session->userdata('admin_session')['password']) {

            return true;
        } elseif ($_SESSION['admin_session']['admin'] == "super") {

            return true;
        }
        return false;
    }

    function server_pagination() {

        /* Array of database columns which should be read and sent back to DataTables. Use a space where
         * you want to insert a non-database field (for example a counter or static image)
         */
//    $aColumns = array('address_line1','appointment_dt');
// DB table to use
//    $sTable = 'country';
//

        $iDisplayStart = $this->input->get_post('iDisplayStart', true);
        $iDisplayLength = $this->input->get_post('iDisplayLength', true);
        $iSortCol_0 = $this->input->get_post('iSortCol_0', true);
        $iSortingCols = $this->input->get_post('iSortingCols', true);
        $sSearch = $this->input->get_post('sSearch', true);
        $sEcho = $this->input->get_post('sEcho', true);

// Paging
        if (isset($iDisplayStart) && $iDisplayLength != '-1') {
            $this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
        }

// Ordering
        if (isset($iSortCol_0)) {
            for ($i = 0; $i < intval($iSortingCols); $i++) {
                $iSortCol = $this->input->get_post('iSortCol_' . $i, true);
                $bSortable = $this->input->get_post('bSortable_' . intval($iSortCol), true);
                $sSortDir = $this->input->get_post('sSortDir_' . $i, true);

                if ($bSortable == 'true') {
                    $this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
        }

        /*
         * Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited
         */
        if (isset($sSearch) && !empty($sSearch)) {
            for ($i = 0; $i < count($aColumns); $i++) {
                $bSearchable = $this->input->get_post('bSearchable_' . $i, true);

                // Individual column filtering
                if (isset($bSearchable) && $bSearchable == 'true') {
                    $this->db->or_like($aColumns[$i], $this->db->escape_like_str($sSearch));
                }
            }
        }

// Select Data
//    $this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
//    $rResult = $this->db->get($sTable);
        $rResult = $this->db->query("select ap.address_line1,ap.appointment_dt,ap.bid,ap.inv_id,ap.status,ap.amount,d.email as mas_email,d.first_name as mas_fname,d.last_name as mas_lname,p.email as slv_email,p.first_name as slv_fname,p.last_name as slv_lname from appointment ap,doctor d,patient p where ap.doc_id = d.doc_id and ap.cust_id = p.patient_id and ap.doc_id='" . $this->session->userdata('admin_session')['LoginId'] . "' and ap.status IN(7) order by ap.bid DESC Limit " . $iDisplayLength . " ");

// Data set length after filtering
//    $this->db->select('FOUND_ROWS() AS found_rows');
//    $iFilteredTotal = $this->db->get()->row()->found_rows;

        $iFilteredTotal = $rResult->num_rows();
// Total data set length
//    $iTotal = $this->db->count_all($rResult);

        $iTotal = $rResult->num_rows();

// Output
        $output = array(
            'sEcho' => intval($sEcho),
            'iTotalRecords' => $iTotal,
            'iTotalDisplayRecords' => $iFilteredTotal,
            'aaData' => array()
        );
//
//    if u want spacific data then above a columns array has created please fill that to field
//    foreach($rResult->result_array() as $aRow)
//    {
//        $row = array();
//
//        foreach($aColumns as $col)
//        {
//            $row[] = $aRow[$col];
//        }
//
//        $output['aaData'][] = $row;
//    }

        foreach ($rResult->result_array() as $aRow) {
            $row = array();

            foreach ($aRow as $col) {
                $row[] = $col;
            }

            $output['aaData'][] = $row;
        }
        echo json_encode($output);
    }

    function GetStripeIdForDoctor() {

        $stripe = new StripeModule();
        $stripe_arr = $this->db->query("select * from master_bank where doc_id=" . $this->session->userdata('admin_session')['LoginId'])->result();
//      $stripe_arr = $this->db->query("select * from master_bank where doc_id=36")->result();
        $bank_arr = array();
        $i = 0;
        foreach ($stripe_arr as $strp) {
            $getResp = array('stripe_id' => $strp->stripe_id);
            $rep = $stripe->apiStripe('getRecipient', $getResp);
            $ret = json_decode($rep);
            $bank_arr[$i]['stripe_id'] = $strp->stripe_id;
            $bank_arr[$i]['bank_id'] = $strp->bank_id;
            $bank_arr[$i]['default_stripe'] = $strp->default_stripe;
            $bank_arr[$i]['name'] = $ret->name;
            $bank_arr[$i]['email'] = $ret->email;
            $bank_arr[$i]['bank_name'] = $ret->active_account->bank_name;
            $bank_arr[$i]['routing_number'] = $ret->active_account->routing_number;
            $bank_arr[$i]['country'] = $ret->active_account->country;
            $bank_arr[$i]['created'] = $ret->created;
            $bank_arr[$i]['description'] = $ret->description;
            $i++;
        }
        return $bank_arr;
    }

    function AddRecipient() {
        $stripe = new StripeModule();
        $userData = $this->input->post('fdata');
        //$createRecipientArr = array('name' => $args['ent_first_name'] . ' ' . $args['ent_last_name'], 'type' => 'individual', 'email' => $args['ent_email'], 'tax_id' => '000000000', 'country' => 'US', 'account_number' => '000123456789', 'routing_number' => '110000000', 'description' => 'For ' . $args['ent_email']);
        $createRecipientArr = array('name' => $userData['name'], 'type' => 'individual', 'email' => $userData['email'], 'tax_id' => (string) $userData['tax_id'], 'bank_account' => (string) $userData['account_number'], 'routing_number' => (string) $userData['routing_number'], 'description' => 'For ' . $userData['email']);
        $recipient = $stripe->apiStripe('createRecipient', $createRecipientArr);

        if (isset($recipient['error'])) {
            return array('flag' => 1, 'message' => $recipient['error']['message']);
        } else if ($recipient['verified'] === FALSE) {
            return array('flag' => 1, 'message' => "Unable to verify");
        } else if ($recipient['verified'] === TRUE) {
            $mid = $this->session->userdata('admin_session')['LoginId'];
            $def = 0;
            $stripe_id = $recipient['id'];
            $totacc = $this->db->query("SELECT count(*) as totacc FROM master_bank WHERE doc_id = " . $mid)->result();
            if ($totacc[0]->totacc == 0) {
                $def = 1;
                $this->db->query("update doctor set stripe_id = '" . $stripe_id . "'where doc_id = '" . $mid . "'");
            }
            $this->db->query("insert into master_bank(doc_id,stripe_id,default_stripe) values('" . $mid . "','" . $stripe_id . "','" . $def . "')");
            return array('flag' => 0, 'message' => "Bank Details Added successfully");
        }
    }

    function DeleteRecipient() {
        $stripe = new StripeModule();
        $bid = $this->input->post('bid');
        $stripeid = $this->db->query("select stripe_id from master_bank where bank_id=" . $bid)->result();
        $getRep = array('stripe_id' => $stripeid[0]->stripe_id);
        $rep = $stripe->apiStripe('deleteRecipient', $getRep);
        if (isset($rep['error'])) {
            return array('flag' => 1, 'message' => $rep['error']['message']);
        } else {
            $this->db->query("delete from master_bank where bank_id = '" . $bid . "'");
            return array('flag' => 1, 'message' => "Recipient Deleted");
        }
    }

    function MakeDefaultRecipient() {
        $bid = $this->input->post('bid');
        $mid = $this->session->userdata('admin_session')['LoginId'];
        $this->db->query("update master_bank set default_stripe=0 where doc_id ='" . $mid . "'");
        $this->db->query("update master_bank set default_stripe=1 where bank_id ='" . $bid . "'");
        $stripeid = $this->db->query("select stripe_id from master_bank where default_stripe=1 and bank_id ='" . $bid . "'")->result();
        $this->db->query("update doctor set stripe_id = '" . $stripeid[0]->stripe_id . "'where doc_id = '" . $mid . "'");
        return array('flag' => 0, 'message' => "Default Bank Changed");
    }

}

?>
