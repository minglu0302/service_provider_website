var express = require('express');
var app = express();
var path = require("path");
var mongoose = require('mongoose');
var moment = require('moment');
var async = require('async');

var mysql = require('mysql');
var connection = mysql.createConnection({
    host: '127.0.0.1',
    user: 'root',
    password: '62ubuPZK=a$R7R=e',
    database: 'goclean'
});
connection.connect(function (err) {

    console.log(err);

    if (err != null) {

        // response.write('Error connecting to mysql:' + err+'\n');
    }

});

//connecting mongodb database
mongoose.connect("mongodb://goclean_user:#goclean_user@127.0.0.1:27017/goclean", function (err) {
    if (err) {
        console.log({error: 'error in mongoose connect', err: err});
    } else {
        db = mongoose.connection;
        console.log('connected to database');
    }
});



app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// app.get('/', function (req, res) {
// 	res.sendFile(path.join(__dirname+'/home.html'));
// });

//driverlist
app.get('/drivers', function (req, res) {
    var collection = db.collection('location');

    var xx = moment().unix();
    console.log(xx);


    var date = (xx) - (60 * 60 * 1);
    console.log(date);
    var list = [];
//var list1={};
    collection.find({"status": {$in: [3]}, "lastTsPc": {$gte: date}}).toArray(function (err, result) {
        if (!err) {
            //console.log(result);

            async.each(result, function (item, callback) {
                connection.query('SELECT m.*,ca.City_Id FROM master m,city_available ca,company_info ci where m.company_id = ci.company_id and ci.city = ca.City_Id and m.mas_id= ' + item.user, function (err, rows, fields) {
                    console.log(rows);
                    //console.log("hai",rows[0].mobile);
                    if (!err) {
                        var list1 = {
                            'fname': item.name,
                            'profilePic': rows[0].profile_pic,
                            'phone': rows[0].mobile,
                            'tp': result.type,
                            'cityid': rows[0].City_Id,
                            'driverid': item.user,
                            'a': '4',
                            'lt': item.location.latitude,
                            'lg': item.location.longitude,
                            'e_id': rows[0].mobile,
                            'mid': item.user,
                            'appendflag': true
                        }
                        list.push(list1);
                        callback();
                    }
                })
            }, function (err) {
                if (!err) {
                    res.send({
                        'dlist': list
                    })
                }
            })
        } else {
            res.send(err);
        }

    })
})



//city type data from mysql database
app.get("/citydata", function (req, res) {
    connection.query('SELECT * FROM labaih.city_available ', function (err, rows, fields) {
        if (!err) {
            res.send(rows);
        } else
            console.log('Error while performing Query. citydata');
    });
});

// //vehicle type data from mysql
app.get("/vehicletype", function (req, res) {
    connection.query('SELECT * FROM labaih.workplace_types', function (err, rows, fields) {
        if (!err) {
            res.send(rows);
        } else
            console.log('Error while performing Query.vehicle type');
    });
});

//appointment details for selected driver from mysql
app.get("/appointmentdata/:id/:apid", function (req, res) {
    //console.log("appdata");
    var id = Number(req.params.id);
    var apid = Number(req.params.apid);

//console.log(id);
//console.log(apid);

    connection.query('SELECT * FROM labaih.appointment where mas_id=' + id + ' and appointment_id=' + apid, function (err, approws, fields) {
        if (!err) {
            if (approws[0]) {
                connection.query('SELECT * FROM labaih.slave where slave_id=' + approws[0].slave_id, function (err, slaverows, fields) {
                    res.send({
                        'appointment': approws,
                        'slave': slaverows
                    })
                });
            } else {
                res.send({
                    'appointment': approws
                });
            }
        } else
            console.log('Error while performing Query. appointment data');
    });
});


//vehicle type data based on city id from mysql
app.get("/vehicletype/:id", function (req, res) {
    var id = Number(req.params.id);
    connection.query('SELECT * FROM labaih.workplace_types where city_id=' + id, function (err, rows, fields) {
        if (!err) {
            res.send(rows);
        } else
            console.log('Error while performing Query.');
    });
});


// //session checking
// app.get("/phpsession/:id", function (req, res) {
// 	var id = req.params.id;
// 	console.log(id);
// 	connection.query('SELECT * FROM roadyo_1.ci_sessions where ip_address="'+id+'"', function (err, rows, fields) {
// 		if (!err){
// 			res.send(rows);
// 		}
// 		else
// 			console.log('Error while performing Query.vehicle type');
//   });
// });

//getting available and busy drivers from mongodb


app.get("/", function (req, res) {
    res.send({
        "hai": "hello"
    })
})


app.listen(9009, function () {
    console.log('Example app listening on port 9009!');
});
