<?php

$sess = unserialize($_COOKIE['ci_session']);
if ($sess['table'] == '') {
    header("Location: ../dashboard/");
}
require_once '../Models/config.php';
//require_once 'Models/config.php';
//include("../../Models/config.php");
 
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?= APP_NAME ?> - GodsView</title>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Lato:400,100,300' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,200,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/vis/4.15.0/vis.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <link href='<?= APP_SERVER_HOST ?>Godsview/simple.css' rel='stylesheet' type='text/css'>
        <link rel="shortcut icon" href="<?= APP_SERVER_HOST ?>admin/theme/assets/img/demo/favicon.png" />
        <link href='<?= APP_SERVER_HOST ?>Godsview/default.css' rel='stylesheet' type='text/css'>
        <link rel="shortcut icon" href="<?= LogoURL?>" />
        <title><?php echo Appname; ?></title>
        <link rel="apple-touch-icon" sizes="76x76" href="<?= LogoURL?>">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= LogoURL?>">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= LogoURL?>">
    </head>

    <body id="body">
        <div class="conatiner-fluid">
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <h2><?= APP_NAME ?> GODS VIEW</h2>
                    <ul class="nav navbar-nav navbar-right" style="margin-top:1px;">
                                        <li>
                                            <select class="selectpicker" id="city">
                                            <option value="">Select City</option>
                                        </select>
                                        </li>
               
                </div>
            </nav>
        </div>
        <div id='bs-example'>
            <nav role="navigation" class="navbar navbar-default">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="modal" data-target="#myModal" id="left" style='float: left !important;color: black;'>
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </button>
                    <button type="button" data-target="#collapse1" data-toggle="collapse" class="navbar-toggle">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <div id="collapse1" class="panel-collapse collapse">
                        <div class="col-xs-12">
                            <a href="#" onclick="change_state(5,5);"><img src="http://iserve.ind.in/Godsview/icons/red.png" />
                                <h2><button class="btn btn-default" role="button">On The Way</button></h2>
                            </a>
                        </div>
                        <div class="col-xs-12">
                            <a href="#" onclick="change_state(5,21);"><img src="http://iserve.ind.in/Godsview/icons/blue.png" />
                                <h2><button class="btn btn-default" role="button"> Arrived</button></h2>
                            </a>
                        </div>
                        <div class="col-xs-12">
                            <a href="#" onclick="change_state(5,6);"><img src="http://iserve.ind.in/Godsview/icons/yellow.png" />
                                <h2> <button class="btn btn-default" role="button">Started</button></h2>
                            </a>
                        </div>
                        <div class="col-xs-12">
                            <a href="#" onclick="change_state(3,0);" class="active1"><img src="http://iserve.ind.in/Godsview/icons/green.png" />
                                <h2><button class="btn btn-default " role="button"> Available</button></h2>
                            </a>
                        </div>
                        <div class="col-xs-12">
                            <a href="#" onclick="change_state(0,0);"><img src="" style="opacity:0;" />
                                <h2> <button class="btn btn-default raised" role="button">All</button></h2>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- Collection of nav links and other content for toggling -->
                <div id="navbarCollapse" class="collapse navbar-collapse" style="margin-top: 3px;">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="#" onclick="change_state(5,5);"><img src="http://iserve.ind.in/Godsview/icons/red.png" />
                                <h2><button class="btn btn-default" role="button">On The Way <span id="six"></span></button></h2>
                            </a>
                        </li>
                        <li>
                            <a href="#" onclick="change_state(5,21);"><img src="http://iserve.ind.in/Godsview/icons/blue.png" />
                                <h2><button class="btn btn-default" role="button"> Arrived <span id="seven"></span></button></h2>
                            </a>
                        </li>
                        <li>
                            <a href="#" onclick="change_state(5,6);"><img src="http://iserve.ind.in/Godsview/icons/yellow.png" />
                                <h2> <button class="btn btn-default" role="button">Started <span id="eight"></span></button></h2>
                            </a>
                        </li>
                        <li>
                            <a href="#" onclick="change_state(3,0);" class="active1"><img src="http://iserve.ind.in/Godsview/icons/green.png" />
                                <h2><button class="btn btn-default " role="button"> Available <span id="four"></span></button></h2>
                            </a>
                        </li>
                        <li>
                            <a href="#" onclick="change_state(0,0);"><img src="" style="opacity:0;" />
                                <h2> <button class="btn btn-default raised" role="button">All <span></span></button></h2>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>

        <!-- Modal -->
        <div class="modal left fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document" style="margin-top: 191px;">
                <div class="modal-content">

                    <div class="modal-body" style="padding: 0px;">
                        <ul class="nav nav-tabs noborder aligncenter" role="tablist">
                            <li role="presentation" class="active widht60percent"><a href="#avilable" onclick="show_data(3);" aria-controls="available" role="tab" data-toggle="tab">AVAILABLE</a></li>
                            <li role="presentation" class="widht40percent"><a href="#busy" onclick="show_data(5);" aria-controls="busy" role="tab" data-toggle="tab" class="styletab2">BUSY</a></li>
                        </ul>
                        <table class="table table-hover search-table" id="drivertable">
                            <tbody class="driverlist"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal right fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
            <div class="modal-dialog" role="document" style="height: auto;top: 15vh;">
                <div class="modal-content" style="border-bottom: 10px solid #006873;padding: 0px;">
                    <div class="modal-body" style="padding:0px;">
                        <div id="driver"></div>
                        <div id="jobid"></div>
                        <div id="slave"></div>
                        <div id="route"></div>
                    </div>

                </div>
            </div>
        </div>
        <!-- modal -->

        <div class="row restSpace" id="mainContent">
            <div class="col-sm-3 col-md-2 nopadding restSpace" id="drivercontainer">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs noborder aligncenter" role="tablist">
                    <li role="presentation" class="active widht60percent"><a href="#avilable" onclick="show_data(3);" aria-controls="available" role="tab" data-toggle="tab">AVAILABLE</a></li>
                    <li role="presentation" class="widht40percent"><a href="#busy" onclick="show_data(5);" aria-controls="busy" role="tab" data-toggle="tab" class="styletab2">BUSY</a></li>
                    <li role="presentation" style="text-align: center;width: 100%;border: 1px solid red;">
                                <a href="#location_issue" onclick="show_data(0);" aria-controls="location_issue" role="tab" data-toggle="tab" class="styletab2" aria-expanded="false">LOCATION ISSUE</a>
                            </li>
                </ul>
                <table class="table table-hover search-table" id="drivertableNewOne">
                    <tbody class="drivertableNewOne"></tbody>
                </table>
            </div>
            <div class="col-sm-9 col-md-10 nopadding restSpace">
                <input id="pac-input" class="controls" type="text" placeholder="Search Location" style="width: 15.5em;
    margin-left: 50px;
    z-index: 6;
    position: absolute;
    left: 0px;
    top: 0px;">
            <!--<input id="pac-input" class="controls" type="text" placeholder="Search Location" style="width: 15.5em;margin-left: 50px;">-->
                <div id="map"></div>
            </div>
        </div>
        <div style='position:fixed;bottom:0;height:3vh;width:100vw;background-color:#006873;'>
        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/js/bootstrap.min.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.11.1/jquery.validate.min.js"></script>
        <!--<script src="http://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCHAAF-2jzmx-G06WCapwMZK0VG3pIC3yY&libraries=places,geometry"></script>-->
        <script src='https://api.mapbox.com/mapbox.js/v3.0.1/mapbox.js'></script>
        <link href='https://api.mapbox.com/mapbox.js/v3.0.1/mapbox.css' rel='stylesheet' />
        <script src="http://cdn.pubnub.com/pubnub-3.15.1.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js"></script>
			<script src="//cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.13/moment-timezone-with-data.js"></script>
        <script src="<?= APP_SERVER_HOST ?>Godsview/presence.js"></script>

    </body>

</html>
<style>
    .leaflet-bottom{
        display: none !important;
    }
</style>
<script>
//driver table search
    $(document).ready(function () {

// $.get("http://ipinfo.io", function(response) {
//     alert(response.ip);

//         $.getJSON('http://107.170.65.252:8080/phpsession/'+response.ip, function (response) {
//             console.log(response);
//         });


// }, "jsonp");
                        // $.ajax({
                        //     url:"http://www.roadyo.in/roadyo1.0/superadmin/dashboard/index.php/superadmin/check_session",
                        //     type: "GET",
                        //     dataType: 'json',
                        //     success:function(res){
                        //         console.log("hai");
                        //         console.log(res);
                        //     },
                        //     error:function(res){
                        //         console.log("hai1");
                        //         console.log(res);
                        //     }
                        // });

//  $.getJSON('http://www.roadyo.in/roadyo1.0/superadmin/dashboard/index.php/superadmin/check_session', function (response) {
//      console.log("hai");
//      console.log(response);
//  });

        $('table.search-table').tableSearch({
            searchPlaceHolder: 'Search driver'
        });

    });

    (function ($) {
        $.fn.tableSearch = function (options) {
            if (!$(this).is('table')) {
                return;
            }
            var tableObj = $(this),
                    searchText = (options.searchText) ? options.searchText : '',
                    searchPlaceHolder = (options.searchPlaceHolder) ? options.searchPlaceHolder : '',
                    divObj = $('<div>' + searchText + '</div>'),
                    inputObj = $('<input type="text" id="search" placeholder="' + searchPlaceHolder + '" />'),
                    caseSensitive = (options.caseSensitive === true) ? true : false,
                    searchFieldVal = '',
                    pattern = '';
            inputObj.off('keyup').on('keyup', function () {
                searchFieldVal = $(this).val();
                pattern = (caseSensitive) ? RegExp(searchFieldVal) : RegExp(searchFieldVal, 'i');
                tableObj.find('tbody tr').hide().each(function () {
                    var currentRow = $(this);
                    currentRow.find('td').each(function () {
                        if (pattern.test($(this).html())) {
                            currentRow.show();
                            return false;
                        }
                    });
                });
            });
            tableObj.before(divObj.append(inputObj));
            return tableObj;
        }
    }(jQuery));

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBCGghZXXRx0aDc8zJJdd8Ego6-wnIPQdQ&libraries=places&callback=initMap"
async defer></script>
<script src="http://goclean-service.com:9999/socket.io/socket.io.js"></script>