var UtilityForMysql = require("./UtilityForMysql");
var Utility = require("./UtilityFunc");
var async = require("async");
var moment = require('moment');
var request = require('superagent');

var CronJob = module.exports = {};

CronJob.ExecuteGuaranteeBonus = function (guranteeId, Finalcallback) {


    async.waterfall(
            [
                function (callback) {
                    Utility.ConnectionMongo(function (err, result1) {
                        if (err) {
                            console.log("Error While fetching gaurantee" + err);
                        } else
                            callback(null, callback);
                    });
                },
                function ($arra, callback) {
//                    console.log('111111111');
                    ObjectID = require('mongodb').ObjectID;


                    var guaranteeCondition = {'status': '0'};
                    if (guranteeId != null)
                        guaranteeCondition = {'status': '0', '_id': new ObjectID(guranteeId)};

                    Utility.Select('gaurantee', guaranteeCondition, function (err, result1) {
                        if (err) {
                            console.log("Error While fetching gaurantee" + err);
                        } else
                        {
                            callback(null, result1);
                            console.log(result1);
                        }
                    });

                }
                ,
                function (resultData, callback) {
//                    console.log('1111111112222222222');
//                    console.log(resultData);

                    if (resultData.length > 0) {

                        resultData.forEach(function (item) {

                            var startDate = moment.unix(item.frm_date).add(parseInt(item.currentCount), 'days').format("YYYY-MM-DD");
                            var startDateTime = moment(startDate + ' ' + item.frm_time);

                            var EndDate = moment.unix(item.to_date).format("YYYY-MM-DD");
                            var EndDateTimeToCalculate = moment(EndDate + ' ' + item.to_time);

                            var EndDateTime = moment(startDate + ' ' + item.to_time);
                            console.log("EndDateTimeToCalculate" + EndDateTimeToCalculate.unix());
                            console.log("startTime" + startDateTime.unix());
                            console.log("EndDateTime" + EndDateTime.unix());

                            if (EndDateTimeToCalculate.unix() > startDateTime.unix()) {
                                console.log('Inside*************');

                                Utility.Select('appointments', {"BOOKING": {"$exists": true},
                                    $and: [{timpeStamp_appointment_dt: {$lte: EndDateTime.unix()}}, {timpeStamp_appointment_dt: {$gte: startDateTime.unix()}}]
                                }, function (err, result1) {
                                    if (err) {

                                        console.log("Error While fetching gaurantee" + err);
                                        console.log(err);


                                    } else
                                    if (result1.length > 0) {

                                        // here is the main logic comes

                                        async.waterfall(
                                                [
                                                    function (callback) {
                                                        CaculateGuaranteeAndBounus(result1, item, EndDateTime.unix(), startDateTime.unix(), function (err, masArray) {
                                                            if (err) {
                                                                console.log("Error While fetching gaurantee" + err);
                                                            } else if (Object.keys(masArray).length > 0) {

                                                                var queryToexecute = "insert into DriverTxn(mas_id,amount,paymentCycleId,gauranteeId,Bonusstatus,acceptance_rate,average_rate,active_time,masEarning,startTime,endTime,numoftrips) VALUES";
                                                                for (var key in masArray)
                                                                {
                                                                    var rechargeamt = 0;
                                                                    var message = "";
                                                                    if (item.actionFor == "1") {
                                                                        rechargeamt = parseFloat(item.amount) - parseFloat(masArray[key].masearning);
                                                                        message = 'Congrats You got guarantee of EGP'.rechargeamt;
                                                                    } else if (item.actionFor == "2") {
                                                                        rechargeamt = parseFloat(item.amount);
                                                                        message = 'Congrats You got bonus of EGP'.rechargeamt;
                                                                    }
                                                                    queryToexecute += " ('" + key + "','" + rechargeamt + "','" + item.cycle_id + "','" + item._id.toString() + "','" + item.actionFor + "','" + masArray[key].acceptance_rate + "','" + masArray[key].average_rate + "','" + masArray[key].active_time + "','" + parseFloat(masArray[key].masearning).toFixed(2) + "','" + moment.unix(startDateTime.unix()).format("YYYY-MM-DD HH:mm:ss") + "','" + moment.unix(EndDateTime.unix()).format("YYYY-MM-DD HH:mm:ss") + "','" + masArray[key].tripsCount + "'),";

                                                                    request.post('http://35.156.232.29/LabaihPQ/services.php/PushFromAdminForSpicificnode')
                                                                            .send({usertype: 1, User_id: key, 'message': message})
                                                                            .end(function (err, res) {
//                                                                            console.log(res.body);
                                                                            });

                                                                }

                                                                UtilityForMysql.ExecuteCode(queryToexecute.replace(/,\s*$/, ""), function (err, data) {
                                                                    if (err) {
                                                                        console.log("Error While inserting Bounus and Guarantee" + err);
                                                                    } else {
                                                                        callback(masArray, callback);
                                                                    }

                                                                });

                                                            } else {
                                                                console.log("No driver Who can full fill");
                                                                callback(masArray, callback);
                                                            }

                                                        });
                                                    }
                                                ],
                                                function (masArray, status) {
                                                    console.log('inserted data in mysql');

                                                    ObjectID = require('mongodb').ObjectID;
                                                    Utility.DirectCondition('gaurantee', {'_id': new ObjectID(item._id)}, {'$inc': {'currentCount': 1, "drivers": Object.keys(masArray).length}}, function (err, result1) {
                                                        if (err) {
                                                            console.log("Presence Status Error : ");
                                                        } else if (result1) {
                                                            console.log("inserted Status In Mongo : ");
                                                            callback(null);
                                                        }
                                                    });

                                                }
                                        );

                                    } else {
                                        console.log('No Data Found');

                                        ObjectID = require('mongodb').ObjectID;
                                        Utility.DirectCondition('gaurantee', {'_id': new ObjectID(item._id)}, {'$inc': {'currentCount': 1, "drivers": 0}}, function (err, result1) {
                                            if (err) {
                                                console.log("Presence Status Error : ");
                                            } else if (result1) {
                                                console.log("inserted Status behalf of no appointment : ");
                                                callback(null);
                                            }
                                        });

                                    }
                                });
                            } else {
                                ObjectID = require('mongodb').ObjectID;

                                Utility.Update('gaurantee', {'_id': new ObjectID(item._id)}, {'status': '1'}, function (err, result1) {
                                    if (err) {
                                        console.log("Presence Status Error : ");
                                    } else if (result1) {
                                        console.log("inserted Status In Mongo : ");
                                    }
                                    callback(null);
                                });

                            }


                        });
                    } else {
                        callback(null);
                    }
                }
            ], function ($args) {
//        console.log('1111111113333333333');
        console.log('lastcall');
        return Finalcallback(null, null);
    }
    );


    var CaculateGuaranteeAndBounus = function (appointementData, guarantee, EndDateTime, startDateTime, Newcallback) {
        var masArray = {};
        var FilteredMasterArray = [];
//    var masArray = {};
        async.waterfall(
                [
                    function (callback) { // get the acceptance rate for each driver
//                        console.log("11111111111111");
                        appointementData.forEach(function (item) {
                            item.BOOKING.forEach(function (bookingdata) {
                                if (typeof masArray[bookingdata.DriverId] == "undefined")
                                    masArray[bookingdata.DriverId] = {Accepted: 0, Rejected: 0, recivedButNotRespond: 0, masearning: 0, tripsCount: 0, rating: 0, ratingdevide: 0, acceptance_rate: 0, average_rate: 0, active_time: 0};
                                if (bookingdata.Status == "Accepted") {
                                    if (item.status == 9) {
                                        masArray[bookingdata.DriverId].tripsCount += 1;
                                        masArray[bookingdata.DriverId].masearning += item.invoice[0].mas_earning;
                                        masArray[bookingdata.DriverId].rating += item.rating;
                                        if (item.UpdateReview == 0)
                                            masArray[bookingdata.DriverId].ratingdevide += 5;
                                    }
                                    masArray[bookingdata.DriverId].Accepted += 1;

                                } else if (bookingdata.Status == "Rejected") {
                                    masArray[bookingdata.DriverId].Rejected += 1;
                                } else if (bookingdata.Status == "Received But Didn't Respond") {
                                    masArray[bookingdata.DriverId].recivedButNotRespond += 1;
                                }
                            });

                        });


                        callback(null, callback);
                    }, function (arg1, callback) {
                        console.log(masArray);

                        if (Object.keys(masArray).length > 0) {
                            for (var key in masArray)
                            {
                                //1) step :  calculating acceptanceRate
                                var total = parseInt(masArray[key].Accepted) + parseInt(masArray[key].Rejected) + parseInt(masArray[key].recivedButNotRespond);
                                var acceptancerate = (parseFloat(parseInt(masArray[key].Accepted) / total) * 100).toFixed(2);

                                if (acceptancerate < parseInt(guarantee.acc_rate))
                                    delete masArray[key];
                                else {
                                    masArray[key].acceptance_rate = acceptancerate;
                                    //2 average rating
//                                console.log(masArray);
                                    var calculateRating = (parseInt(masArray[key].rating) / parseInt(masArray[key].ratingdevide) * 100);
                                    if (isNaN(calculateRating) || calculateRating < ((guarantee.averageRating * 100) / 5))
                                        delete masArray[key];
                                    else {
                                        masArray[key].average_rate = calculateRating;
                                        //3 minum trip ocomplish
                                        if (masArray[key].tripsCount < parseInt(guarantee.MinTrips))
                                            delete masArray[key];
                                        else {
                                            if (masArray[key].masearning >= parseFloat(guarantee.amount))
                                                delete masArray[key];
                                            else
                                                FilteredMasterArray.push(parseInt(key));
                                        }
                                    }
                                }

                            }

                        }
                        callback(null, callback);
                    }
                    , function (arr, callback) {
                        console.log(FilteredMasterArray);

                        var Condition = [{"$match": {"master": {"$in": FilteredMasterArray},
                                    $or: [
                                        {
                                            $and: [{startTime: {$gte: startDateTime}, EndTimeGurentee: {$lte: EndDateTime}}]
                                        }, {
                                            $and: [{startTime: {$lt: startDateTime}, EndTimeGurentee: {$gt: startDateTime}}]
                                        },
                                        {
                                            $and: [{startTime: {$gt: startDateTime}, startTime:{$lt: EndDateTime},
                                                    $or: [{EndTimeGurentee: {$eq: EndDateTime}, EndTimeGurentee:{$gt: EndDateTime}}]
                                                }]
                                        }, {
                                            $and: [{startTime: {$lt: startDateTime}, EndTimeGurentee: {$gt: EndDateTime}, EndTimeGurentee:{$gt: startDateTime}}]

                                        }
                                    ]
                                }},
                            {"$group": {
                                    "_id": "$master",
                                    "startTime": {
//                                    "$sum": "$startTime"
                                        "$sum": {
//                                        $cond : [{ if: { $eq: [ "$EndTime", 0 ] }, then: EndDateTime, else: "$EndTime" }]
                                            $cond: [{$lt: ["$startTime", startDateTime]}, startDateTime, "$startTime"]
                                        }
                                    },
                                    "EndTimeGurentee": {
                                        "$sum": {
//                                        $cond : [{ if: { $eq: [ "$EndTime", 0 ] }, then: EndDateTime, else: "$EndTime" }]
                                            $cond: [{$gt: ["$EndTimeGurentee", EndDateTime]}, EndDateTime, "$EndTimeGurentee"]
                                        }
                                    }
                                }
                            }, {
                                "$project": {
                                    total: {$subtract: ["$EndTimeGurentee", "$startTime"]}
                                }
                            },
//                            {"$match": {"total": {"$gte": (parseInt(guarantee.onthejob_time) * 60)}}}
                        ];


                        Utility.aggregate('statusLog', Condition, function (err, result1) {

                            if (err) {
                                console.log("Error While fetching gaurantee" + err);
                            } else
                            if (result1.length > 0) {
                                console.log(result1);
                                result1.forEach(function (master) {
//                                    console.log(":TotalTime:");
//                                    console.log(master.total);
                                    masArray[master._id].active_time = master.total;
                                });

                                callback(null, callback);
                            } else {
                                console.log('No one is applicable.');
//                                console.log(result1);
                                masArray = {};
                                callback(null, callback);
                            }
                        });

                    }
                ],
                function (err) {
                    console.log(masArray);
                    return   Newcallback(null, masArray);
                }
        );

    };

};



CronJob.LogoutDriverIfNotActing = function (logounTimeLimit, Finalcallback) {

    async.waterfall(
            [
                function (callback) {
                    Utility.ConnectionMongo(function (err, result1) {
                        if (err) {
                            console.log("Error While fetching gaurantee" + err);
                        } else {
                            callback(null, callback);
                        }
                    });
                },
                function (args, callback) {
                    var cond = {'lastTs': {'$lt': logounTimeLimit}};
                    Utility.Select('location', cond, function (err, result1) {
                        if (err) {
                            console.log("Update Status Error : " + err);
                        } else if (result1) {
                            var drivers = "";
                            for (var i = 0; i < result1.length; i++) {
                                if ((result1.length - 1) > i)
                                    drivers += result1[i].user + ",";
                                else
                                    drivers += result1[i].user;
                            }
                            callback(null, drivers, callback);
                        } else
                            console.log("no driver who is lessthen 10 hour");
                    });
                },
                function (data, args, callback) {

                    console.log('data');
                    console.log(data);
                    queryToexecute = "select d.workplace_id,us.sid from user_sessions us,master d where us.oid in (" + data + ") and us.user_type = 1 and us.loggedIn = 1 and us.oid = d.mas_id";
//                    queryToexecute = "select *  from user_sessions  where oid in (" + data + ") and user_type = 1 and loggedIn = 1";
                    UtilityForMysql.ExecuteCode(queryToexecute.replace(/,\s*$/, ""), function (err, data) {
                        if (err) {
                            console.log("Error While inserting Bounus and Guarantee" + err);
                        } else {
                            console.log(data);
                            var sids = [];
                            var workplacIds = [];
                            for (var i = 0; i < data.length; i++) {
                                if ((data.length - 1) > i) {
                                    workplacIds += data[i].workplace_id + ",";
                                    sids += data[i].sid + ",";
                                } else {
                                    workplacIds += data[i].workplace_id;
                                    sids += data[i].sid;
                                }
                            }
                            callback(null, sids, workplacIds, callback);
                        }

                    });

                }
            ], function (Org, sids, workplacIds) {
        updatemastertable = "update master set workplace_id = 0 where workplace_id in(" + workplacIds + ")";
        updateworkplaceTable = "update workplace set Status = 2 where workplace_id in(" + workplacIds + ")";
        updateUserSessions = "update user_sessions set loggedIn = 2 where sid in(" + sids + ")";

        UtilityForMysql.ExecuteCode(updateUserSessions.replace(/,\s*$/, ""), function (err, data) {
            if (err) {
                console.log("Error While inserting Bounus and Guarantee" + err);
            } else {
                console.log("Updated usessions");
                UtilityForMysql.ExecuteCode(updatemastertable.replace(/,\s*$/, ""), function (err, data) {
                    console.log("Updated master table");
                    if (err) {
                        console.log("Error While inserting Bounus and Guarantee" + err);
                    } else {

                        UtilityForMysql.ExecuteCode(updateworkplaceTable.replace(/,\s*$/, ""), function (err, data) {
                            if (err) {
                                console.log("Error While inserting Bounus and Guarantee" + err);
                            } else {

                                console.log("UpdatedAll");
                            }

                        });
                    }

                });

            }

        });

    }
    );

};









