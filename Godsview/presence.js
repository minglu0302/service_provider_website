var baseurl = "http://goclean-service.com/Godsview/";
var appurl = 'http://goclean-service.com/';
var baseurl1 = 'http://goclean-service.com/superadmin/index.php/Godsview';
var serverurl = 'http://goclean-service.com:9009';
var scoketurl = 'http://goclean-service.com:9999';
moment.tz.setDefault('America/Los_Angeles');

L.mapbox.accessToken = 'pk.eyJ1IjoiZjRyMmtpdWQiLCJhIjoiY2piNXQ5MWFoMmpzODJxbzZycTltMTltbyJ9.9HrLnO1E7Pu57pNZi8kn3g';//'sk.eyJ1IjoiYXNoaXNoM2VtYmVkIiwiYSI6ImNpd25jcXZyYTAwMTQyc210d2FqZGI5cHYifQ.ybjGWR_zGpVSc8mHGQbR4w';
var map = L.mapbox.map('map', 'mapbox.streets')//'mapbox.light')
        .setView([37.668819, -122.080795], 10);
//L.mapbox.map('map', 'mapbox.streets')
// 
//    .addControl(L.mapbox.geocoderControl('mapbox.places', {
//         autocomplete: true
//    }));
var x_mapmarker = L.marker([37.668819, -122.080795]);
var allMarkers = [];
var allDriver = [];
var mapBound = [];

var status = 0;
var appstatus = 0;
var tab_state = 0;
var timestamp = 0;

//$("#four").html(0);
//$("#six").html(0);
//$("#seven").html(0);
//$("#eight").html(0);

$(document).ready(function () {
    var socket = io(scoketurl);
    socket.on('godzviewChn', function (m) {
        
        if (m) {
            if (m.op == "u") {
                var id = m.o2._id;
                var data = m.o.$set;
                if (typeof data != "undefined") {
                    var updated = [];
                    //            $.each(data.all, function (ind, val) {
                    console.log(data);
                    var adata = {
                        "id": id,
                        "bid": '',
                        "status": data.status,
                        "appstatus": data.apptStatus,
                        "location": data.location,
                        "lastTs": data.lastUpdated,
                    };
                    updated.push(adata);
                    update_data(updated);
                    update_status_no();
                }
            }
        }
        
             
        });
    $.getJSON(baseurl1 + "/get_all_drivers", function (response) {
        if (response.flg == 0) {
            timestamp = response.time;
            var data = response.data;
            $("#four").html(data.four.length);
            $("#six").html(data.six.length);
            $("#seven").html(data.seven.length);
            $("#eight").html(data.eight.length);
            $.each(data.all, function (ind, val) {
                var adata = {
                    "id": val._id.$id,
                    "name": val.name ,
                    "proPic":   val.image,
                    "bid": '',
                    "email": val.email,
                    "mobile": val.mobile,
                    "user": val.user,
                    "booked": val.booked,
                    "status": val.status,
                    "lastTs": val.lastUpdated,
                    "appstatus":parseInt(val.apptStatus),
                    "lat": parseFloat(val.location.latitude),
                    "lng": parseFloat(val.location.longitude)
                };
                allDriver[val.user] = adata;
//                allDriver.push(adata);
            });
            show_data(3, 0);
        }
    });
    $.getJSON(baseurl1 + "/citydata", function (response) {
//        response = $(response).sort(sortCityName);
        citydata = response;
        $.each(citydata, function (ind, val) {
            $('#city').append('<option value="' + val.City_Id + '">' + camelize(val.City_Name) + '</option>');
        });
    });
});
function calculateDistance(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = Math.PI * lat1 / 180
    var radlat2 = Math.PI * lat2 / 180
    var radlon1 = Math.PI * lon1 / 180
    var radlon2 = Math.PI * lon2 / 180
    var theta = lon1 - lon2
    var radtheta = Math.PI * theta / 180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist)
    dist = dist * 180 / Math.PI
    dist = dist * 60 * 1.1515
    if (unit == "K") {
        dist = dist * 1.609344
    }
    if (unit == "N") {
        dist = dist * 0.8684
    }
    return dist
}

function initMap() {

    var input = (document.getElementById('pac-input'));
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.addListener('place_changed', function () {
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
        }
        console.log(place);
        map.removeLayer(x_mapmarker);
        //map pointer in mapbox
        x_mapmarker = L.marker([place.geometry.location.lat(), place.geometry.location.lng()]).addTo(map);
        map.addLayer(x_mapmarker);
        map.setView([place.geometry.location.lat(), place.geometry.location.lng()], 13);



    });
}
function camelize(inStr) {
    return inStr.replace(/\w\S*/g, function (tStr) {
        return tStr.charAt(0).toUpperCase() + tStr.substr(1).toLowerCase();
    });
}
function show_data(flg, flg2 = 1) {
    var html = '';
    tab_state = flg;
    var bound = [];
    for (var ind in allDriver){
        var val = allDriver[ind];
//    $.each(allDriver, function (ind, val) {
        var a = 0;
        var colorcode = "";
        var markericong = '';
         
        switch (val.status) {
            case 3:
                if(val.booked == 0)
                    a = 3;
                    colorcode = "#78ac2c";
                    markericong = baseurl + 'icons/green.png';
                    break;
            case 5:
                switch (val.appstatus) {
                    case 5:
                        a = 5;
                        colorcode = '#be0411';
                        markericong = baseurl + 'icons/red.png';
                        break;
                    case 21:
                        a = 5;
                        colorcode = "#4d93c7";
                        markericong = baseurl + 'icons/blue.png';
                        break;
                    case 6:
                        a = 5;
                        colorcode = "#ffd50b";
                        markericong = baseurl + 'icons/yellow.png';
                        break;
                }
                break;
        }
         var adata = val;
        if(typeof cityobj == undefined || cityobj == "")
             var  d=0;
        else
            d = calculateDistance(adata.lat, adata.lng, cityobj[0].City_Lat, cityobj[0].City_Long, 'K');
        console.log("aaaa" + adata);
        if (d < 150) {
        if (a != 0) {
            var lastTs = (moment().unix() - adata.lastTs);
                if (a == flg || (flg == 0 && lastTs>120 && a == 3)) {
                html += '<tr id="D_' + adata.user + '">\
                            <td> \n\
                                <div onclick="get_driver_data(' + adata.user + ');popupmodel();">\n\
                                    <p> \n\
                                        <span class="col-xs-height col-middle">\n\
                                            <span class="thumbnail-wrapper d32 circular bg-success"> \n\
                                                <img src="' + adata.proPic + '" onerror="this.src = \'' + appurl + 'pics/user.jpg\'">\n\
                                            </span>\n\
                                            <img width="12" height="12" class="position" src="' + markericong + '"/> \n\
                                        </span>\n\
                                    </p>\n\
                                    <p class="p-l-10 col-xs-height col-middle" style="width: 80%">\n\
                                        <span class="text-master" style="padding-right: 30px;">\n\
                                             ' + adata.name + '&nbsp;(ID:' + adata.user + ')<br/>' + adata.mobile + '<br/><b class="sec_D">Last Upd. : ' + (lastTs).toString() + ' sec.</b><font style="float:right;"><font style="float:right;"></font>\n\
                                        </span>\n\
                                    </p>\n\
                                </div>\n\
                            </td>\n\
                        </tr>';
            }
            if (flg2 == 0) {
                allMarkers['D_' + adata.user] = new L.marker(L.latLng(parseFloat(adata.lat), parseFloat(adata.lng)), {
                    icon: L.mapbox.marker.icon({
                        'marker-color': colorcode
                    }), data: adata
                })
                .bindPopup('<b>'+ adata.name +'</b>')
                .addTo(map);
            }
        }
    }
//    });
    }
    $(".drivertableNewOne").html(html);
}

 

function update_status_no(){
    var status_3 = 0;
    var status_6 = 0;
    var status_7 = 0;
    var status_8 = 0;
    
    for (var ind in allDriver){
        var val = allDriver[ind];
        console.log(val);
        var adata = val;
        var d =0;
        if( cityobj[0])
            d = calculateDistance(adata.lat, adata.lng, cityobj[0].City_Lat, cityobj[0].City_Long, 'K');

        if (d < 150) {
//    $.each(allDriver, function (ind, val) {
        switch (val.status) {
            case 3:
                status_3++;
                break;
            case 5:
                switch (val.appstatus) {
                    case 5:
                        status_6++;
                        break;
                    case 21:
                        status_7++;
                        break;
                    case 6:
                        status_8++;
                        break;
                }
                break;
        }
    }
//    });
    }
    $("#four").html(status_3);
    $("#six").html(status_6);
    $("#seven").html(status_7);
    $("#eight").html(status_8);
}

//function get_updated(){
//    $.getJSON(baseurl1 + "/get_updated_drivers/"+timestamp, function (response) {
//        if (response.flg == 0) {
//            timestamp = response.time;
//            var data = response.data;
//            $("#four").html(data.four.length);
//            $("#six").html(data.six.length);
//            $("#seven").html(data.seven.length);
//            $("#eight").html(data.eight.length);
//            var updated = [];
//            $.each(data.all, function (ind, val) {
//                var adata = {
//                    "id": val._id.$id,
//                    "name": val.name + " " + val.lname,
//                    "proPic": appurl + "pics/" + val.image,
//                    "bid": '',
//                    "email": val.email,
//                    "mobile": val.mobile,
//                    "user": val.user,
//                    "status": val.status,
//                    "appstatus": val.apptStatus,
//                    "lat": parseFloat(val.location.latitude),
//                    "lng": parseFloat(val.location.longitude)
//                };
//                updated.push(adata);
//            });
//            update_data(updated);
//        }
//    });
//}

//while loading page after 4 seconds execute function
//setInterval(function () {
//    get_updated();
//}, 7000);
var citymarker, cityobj;
var cityobj = [];
//cityobj.push(
//        {
//            'City_Id': "18761",
//            'City_Lat': "12.971599",
//            'City_Long': "77.594563",
//            'City_Name': "bangalore",
//            'Country_Id': "62"
//        }
//);

//while loading page after 4 seconds execute function
setInterval(function () {
    $('.sec_D').each(function(){
        var sec = $(this).html();
        var sec_arr = sec.split(" ");
        sec_arr[3] = parseInt(sec_arr[3]) + 4;
        
        if (sec_arr[3] >120  && tab_state == 3)
            $(this).closest('tr').remove();
        else {
            sec = sec_arr.join(" ");
            $(this).html(sec);
        }
    });
}, 4000);
function update_data(updated) {
    if (typeof cityobj == 'undefined') {
        cityobj = [];
        cityobj[0].City_Lat = 12.971599;
        cityobj[0].City_Long = 77.594563;
    }
    $.each(updated, function (index, dr_data) {
        var flag = 0;
        var id = dr_data.id;
        
        for (var ind in allDriver){
            var val = allDriver[ind];
//        $.each(allDriver, function (ind, val) {
            if (val.id == id) {
                flag = 1;
                var data = dr_data;
                allDriver[ind].status = (typeof (data.status) != 'undefined') ? data.status : allDriver[ind].status;
                allDriver[ind].appstatus = (typeof (data.appstatus) != 'undefined') ? parseInt(data.appstatus) : allDriver[ind].appstatus;
                allDriver[ind].lat = (typeof (data.location) != 'undefined') ? parseFloat(data.location.latitude) : allDriver[ind].lat;
                allDriver[ind].lng = (typeof (data.location) != 'undefined') ? parseFloat(data.location.longitude) : allDriver[ind].lng;
                allDriver[ind].lastTs = (typeof (data.lastTs) != 'undefined') ? parseFloat(data.lastTs) : allDriver[ind].lastTs;
                var d =0;
                console.log("cityobj[0]",cityobj[0]);
                if(cityobj[0])
                    d = calculateDistance(allDriver[ind].lat, allDriver[ind].lng, cityobj[0].City_Lat, cityobj[0].City_Long, 'K');
                  
                if (d < 150) {
                var a = 0;
                var colorcode = "";
                var markericong = '';
                val = allDriver[ind];
//                console.log(val);

                switch (val.status) {
                    case 3:
                        a = 3;
                        colorcode = "#78ac2c";
                        markericong = baseurl + 'icons/green.png';
                        break;
                    case 5:
                        switch (val.appstatus) {
                            case 5:
                                a = 5;
                                colorcode = '#be0411';
                                markericong = baseurl + 'icons/red.png';
                                break;
                            case 21:
                                a = 5;
                                colorcode = "#4d93c7";
                                markericong = baseurl + 'icons/blue.png';
                                break;
                            case 6:
                                a = 5;
                                colorcode = "#ffd50b";
                                markericong = baseurl + 'icons/yellow.png';
                                break;
                        }
                        break;
                }
                var adata = allDriver[ind];
                var html = '<td> \n\
                            <div onclick="get_driver_data(' + adata.user + ');popupmodel();">\n\
                                <p> \n\
                                    <span class="col-xs-height col-middle">\n\
                                        <span class="thumbnail-wrapper d32 circular bg-success"> \n\
                                            <img src="' + adata.proPic + '" onerror="this.src = \'' + appurl + 'pics/user.jpg\'">\n\
                                        </span>\n\
                                        <img width="12" height="12" class="position" src="' + markericong + '"/> \n\
                                    </span>\n\
                                </p>\n\
                                <p class="p-l-10 col-xs-height col-middle" style="width: 80%">\n\
                                    <span class="text-master" style="padding-right: 30px;">\n\
                                         ' + adata.name + '&nbsp;(ID:' + adata.user + ')<br/>' + adata.mobile + '<br/><b class="sec_D">Last Upd. : ' + (moment().unix() - adata.lastTs).toString() + ' sec.</b><font style="float:right;"></font>\n\
                                    </span>\n\
                                </p>\n\
                            </div>\n\
                        </td>';
                if(a != 0){
                    if (a == tab_state) {
                        if($('#D_' + adata.user).length == 0) {
                            $('#D_' + adata.user).remove();
                            $('.drivertableNewOne').append('<tr id="D_' + adata.user + '">' + html + '</tr>');
                        }else{
                            $('#D_' + adata.user).html(html);
                        }
                    } else {
                        $('#D_' + adata.user).remove();
                    }
                    if(adata.status == 4){
                        if (typeof (allMarkers['D_' + adata.user]) == 'undefined') {

                        } else {
                            map.removeLayer(allMarkers['D_' + adata.user]);
                            delete allMarkers['D_' + adata.user];
                        }
                    } else if ((status == 0 && appstatus == 0) || (adata.status == status && appstatus == 0) || (adata.status == status && adata.appstatus == appstatus)) {
                        if (typeof (allMarkers['D_' + adata.user]) == 'undefined') {
                            allMarkers['D_' + adata.user] = L.marker(L.latLng(parseFloat(adata.lat), parseFloat(adata.lng)), {
                                icon: L.mapbox.marker.icon({
                                    'marker-color': colorcode
                                }), data: adata
                            })
                            .bindPopup('<b>'+ adata.name +'</b>')
                            .addTo(map);
                        } else {
                            map.removeLayer(allMarkers['D_' + adata.user]);
                            allMarkers['D_' + adata.user] = L.marker(L.latLng(parseFloat(adata.lat), parseFloat(adata.lng)), {
                                icon: L.mapbox.marker.icon({
                                    'marker-color': colorcode
                                }), data: adata
                            })
                            .bindPopup('<b>'+ adata.name +'</b>')
                            .addTo(map);
                        }
                    } else {
                        if (typeof (allMarkers['D_' + adata.user]) == 'undefined') {

                        } else {
                            map.removeLayer(allMarkers['D_' + adata.user]);
                            delete allMarkers['D_' + adata.user];
                        }
                    }
                }else{
                    if($('#D_' + adata.user).length != 0) 
                        $('#D_' + adata.user).remove();
                    if (typeof (allMarkers['D_' + adata.user]) != 'undefined'){
                        map.removeLayer(allMarkers['D_' + adata.user]);
                        delete allMarkers['D_' + adata.user];
                    }
                }
                } else {
                    flag = 1;
                    if ($('#D_' + adata.user).length != 0)
                        $('#D_' + adata.user).remove();
                    if (typeof (allMarkers['D_' + adata.user]) != 'undefined') {
                        map.removeLayer(allMarkers['D_' + adata.user]);
                        delete allMarkers['D_' + adata.user];
                    }
                }
            }
//        });
        }
        if (flag == 0) {
            $.getJSON(baseurl1 + "/getSpecificDriver/" + id, function (response) {
                if (response.flg == 0) {
                    var val = response.data;
                    var adata = {
                        "id": val._id.$id,
                        "name": val.name + " " + val.lname,
                        "proPic":   val.image,
                        "bid": '',
                        "email": val.email,
                        "mobile": val.mobile,
                        "user": val.user,
                        "status": val.status,
                        "appstatus": val.apptStatus,
                        "lat": parseFloat(val.location.latitude),
                        "lng": parseFloat(val.location.longitude)
                    };
                    allDriver[val.user] = adata;
//                    allDriver.push(adata);
                    var a = 0;
                    var colorcode = "";
                    var markericong = '';
                    switch (adata.status) {
                        case 3:
                            a = 3;
                            colorcode = "#78AC2C";
                            markericong = baseurl + 'icons/green.png';
                            break;
                        case 5:
                            switch (adata.appstatus) {
                                case 5:
                                    a = 5;
                                    colorcode = "#BE0411";
                                    markericong = baseurl + 'icons/red.png';
                                    break;
                                case 21:
                                    a = 5;
                                    colorcode = "#4D93C7";
                                    markericong = baseurl + 'icons/blue.png';
                                    break;
                                case 6:
                                    a = 5;
                                    colorcode = "#FFD50B";
                                    markericong = baseurl + 'icons/yellow.png';
                                    break;
                            }
                            break;
                    }
//                    var adata = val;
                    var html = '<tr id="D_' + adata.user + '">\
                            <td> \n\
                                <div onclick="get_driver_data(' + adata.user + ');popupmodel();">\n\
                                    <p> \n\
                                        <span class="col-xs-height col-middle">\n\
                                            <span class="thumbnail-wrapper d32 circular bg-success"> \n\
                                                <img src="' + adata.proPic + '" onerror="this.src = \'' + appurl + 'pics/user.jpg\'">\n\
                                            </span>\n\
                                            <img width="12" height="12" class="position" src="' + markericong + '"/> \n\
                                        </span>\n\
                                    </p>\n\
                                    <p class="p-l-10 col-xs-height col-middle" style="width: 80%">\n\
                                        <span class="text-master" style="padding-right: 30px;">\n\
                                             ' + adata.name + '&nbsp;(ID:' + adata.user + ')<br/>' + adata.mobile + '<br/><b class="sec_D">Last Upd. : ' + (moment().unix() - adata.lastTs).toString() + ' sec.</b><font style="float:right;"></font>\n\
                                        </span>\n\
                                    </p>\n\
                                </div>\n\
                            </td>\n\
                        </tr>';
                    if(a != 0){
                        if (a == tab_state || tab_state == 0) {
                            $('#D_' + adata.user).remove();
                            $('.drivertableNewOne').append(html);
                        }
                        if ((status == 0 && appstatus == 0) || (adata.status == status && appstatus == 0) || (adata.status == status && adata.appstatus == appstatus)) {
                            allMarkers['D_' + adata.user] = L.marker([parseFloat(adata.lat), parseFloat(adata.lng)], {
                                icon: L.mapbox.marker.icon({
                                    'marker-color': colorcode
                                }), data: adata
                            })
                            .bindPopup('<b>'+ adata.name +'</b>')
                            .addTo(map);
                        }
                    }
                }
            });
        }
    });
}

function change_state(flg, act) {
    status = flg;
    appstatus = act;

    for (var key in allMarkers)
    {
        var val = allMarkers[key].options;
        if ((flg == 0 && act == 0) || (val.data.status == flg && act == 0) || (val.data.status == flg && val.data.appstatus == act)) {
            allMarkers[key].addTo(map);
        } else {
            map.removeLayer(allMarkers[key]);
        }
    }

}
//getting driver data and appointment data based on driverid 
var popup;
function get_driver_data(id) {
//    clearCircle();
    document.getElementById("driver").innerHTML = "";
    document.getElementById("jobid").innerHTML = "";
    document.getElementById("slave").innerHTML = "";
    document.getElementById("route").innerHTML = "";

    var ddata = '';
    for (var ind in allDriver){
        var val = allDriver[ind];
//    $.each(allDriver, function (ind, val) {
        if (val.user == id) {
            $("#driver").append('<h5 style="padding-left: 10px;color: white;">Driver Details</h5>');
            $("#driver").append('<h5><p style="margin-bottom: 0px;padding: 10px;">\
                                    Driver Name: &nbsp;' + val.name + '<br>Phone No: &nbsp;' + val.mobile + '\
                                </p></h5>');
            map.setView([parseFloat(val.lat), parseFloat(val.lng)], 18);
            allMarkers['D_' + val.user].openPopup();
            ddata = val;
        }
//    });
    }
    if (ddata.status == 5) {
        $.getJSON(baseurl1 + "/getApptDetails/" + id, function (response) {
            if (response.flg == 0) {
                var data = response.data;
                $("#jobid").append('<h5 style="padding-left: 10px;color: white;">Booking Deatils</h5>');
                
                var type = 'Card';
                if (data.payment_type == 2) {
                    type = 'Cash';
                }

                $('#jobid').append('<h5 style="padding-left: 10px;color: white;">Booking ID : ' + data._id + '</h5>');
                $("#jobid").append('<p>Booking Type : ' + type + '</p>')
                $("#jobid").append('<h5><p style="padding: 10px;">Customer Name: ' + data.slave_id.SlaveName + '<br>Phone No: ' + data.slave_id.SlavePone + '</p></h5>');
                $("#jobid").append('<hr><p style="margin-bottom: 0px;"><i class="fa fa-map-marker" style="color: green;font-size: 20px;"></i><font size="3" style="padding-left: 20px;">Pickup Address</font><br/>');
                $("#jobid").append('<span class="col-xs-height col-middle" style="padding-left: 30px;">' + data.appointment_address + '</p><hr>');
                $("#jobid").append('<hr><p style="margin-bottom: 0px;"><i class="fa fa-map-marker" style="color: red;font-size: 20px;"></i><font size="3" style="padding-left: 20px;">Drop Address</font><br/>');
                $("#jobid").append('<span class="col-xs-height col-middle" style="padding-left: 30px;">' + ((data.Drop_address == "")?"----------":data.Drop_address) + '</p><hr>');
//                }
            }
        });
    }

}
$('#city').on('change', function () {
    $('.driverlist').html('');
    var value = $(this).val();
    console.log(value);
    if (value != '') {
        //based on city lat long focusing map to particular location        
        cityobj = $.grep(citydata, function (item) {
            if (item.City_Id == value) {
                return item;
            }
        });
        console.log(cityobj);
//        if (citymarker != undefined) {
//            citymarker.setMap(null);
//        }
        map.setView([parseFloat(cityobj[0].City_Lat), parseFloat(cityobj[0].City_Long)], 12);
        searchflag = 1;
        shownearestDoctor();
    } else {
        city = '';
        searchflag = 0;
    }
    update_status_no();
});

function shownearestDoctor() {

//        console.log(cityobj);
    for (var ind in allDriver) {
        flag = 1;
        var a = 0;
        var colorcode = "";
        var markericong = '';
        var val = allDriver[ind];
        var adata = allDriver[ind];
        var d = calculateDistance(val.lat, val.lng, cityobj[0].City_Lat, cityobj[0].City_Long, 'K');
        if (d < 150) {
            switch (val.status) {
                 case 3:
                        a = 3;
                        colorcode = "#78ac2c";
                        markericong = baseurl + 'icons/green.png';
                        break;
                    case 5:
                        switch (val.appstatus) {
                            case 5:
                                a = 5;
                                colorcode = '#be0411';
                                markericong = baseurl + 'icons/red.png';
                                break;
                            case 21:
                                a = 5;
                                colorcode = "#4d93c7";
                                markericong = baseurl + 'icons/blue.png';
                                break;
                            case 6:
                                a = 5;
                                colorcode = "#ffd50b";
                                markericong = baseurl + 'icons/yellow.png';
                                break;
                        }
                        break;
            }

             var html = '<td> \n\
                            <div onclick="get_driver_data(' + adata.user + ');popupmodel();">\n\
                                <p> \n\
                                    <span class="col-xs-height col-middle">\n\
                                        <span class="thumbnail-wrapper d32 circular bg-success"> \n\
                                            <img src="' + adata.proPic + '" onerror="this.src = \'' + appurl + 'pics/user.jpg\'">\n\
                                        </span>\n\
                                        <img width="12" height="12" class="position" src="' + markericong + '"/> \n\
                                    </span>\n\
                                </p>\n\
                                <p class="p-l-10 col-xs-height col-middle" style="width: 80%">\n\
                                    <span class="text-master" style="padding-right: 30px;">\n\
                                         ' + adata.name + '&nbsp;(ID:' + adata.user + ')<br/>' + adata.mobile + '<br/><b class="sec_D">Last Upd. : ' + (moment().unix() - adata.lastTs).toString() + ' sec.</b><font style="float:right;"></font>\n\
                                    </span>\n\
                                </p>\n\
                            </div>\n\
                        </td>';
                if(a != 0){
                    if (a == tab_state) {
                        if($('#D_' + adata.user).length == 0) {
                            $('#D_' + adata.user).remove();
                            $('.drivertableNewOne').append('<tr id="D_' + adata.user + '">' + html + '</tr>');
                        }else{
                            $('#D_' + adata.user).html(html);
                        }
                    } else {
                        $('#D_' + adata.user).remove();
                    }
                    if(adata.status == 4){
                        if (typeof (allMarkers['D_' + adata.user]) == 'undefined') {

                        } else {
                            map.removeLayer(allMarkers['D_' + adata.user]);
                            delete allMarkers['D_' + adata.user];
                        }
                    } else if ((status == 0 && appstatus == 0) || (adata.status == status && appstatus == 0) || (adata.status == status && adata.appstatus == appstatus)) {
                        if (typeof (allMarkers['D_' + adata.user]) == 'undefined') {
                            allMarkers['D_' + adata.user] = L.marker(L.latLng(parseFloat(adata.lat), parseFloat(adata.lng)), {
                                icon: L.mapbox.marker.icon({
                                    'marker-color': colorcode
                                }), data: adata
                            })
                            .bindPopup('<b>'+ adata.name +'</b>')
                            .addTo(map);
                        } else {
                            map.removeLayer(allMarkers['D_' + adata.user]);
                            allMarkers['D_' + adata.user] = L.marker(L.latLng(parseFloat(adata.lat), parseFloat(adata.lng)), {
                                icon: L.mapbox.marker.icon({
                                    'marker-color': colorcode
                                }), data: adata
                            })
                            .bindPopup('<b>'+ adata.name +'</b>')
                            .addTo(map);
                        }
                    } else {
                        if (typeof (allMarkers['D_' + adata.user]) == 'undefined') {

                        } else {
                            map.removeLayer(allMarkers['D_' + adata.user]);
                            delete allMarkers['D_' + adata.user];
                        }
                    }
                }else{
                    if($('#D_' + adata.user).length != 0) 
                        $('#D_' + adata.user).remove();
                    if (typeof (allMarkers['D_' + adata.user]) != 'undefined'){
                        map.removeLayer(allMarkers['D_' + adata.user]);
                        delete allMarkers['D_' + adata.user];
                    }
                }
        } else {
            if ($('#D_' + adata.user).length != 0)
                $('#D_' + adata.user).remove();
            if (typeof (allMarkers['D_' + adata.user]) != 'undefined') {
                map.removeLayer(allMarkers['D_' + adata.user]);
                delete allMarkers['D_' + adata.user];
            }
        }
    }
}
//right side bar ,its displays driver and jobs details
function popupmodel() {
    $('#myModal2').modal('show');
}
//based on click  adding css class to statusbar enroute,pickup,journey,all 
$(".btn").click(function () {
    $(".btn-default").removeClass("raised");
    $(this).addClass("raised");
});
// to resize the map window based on browser or device size
function changeSize() {
    var elem = (document.compatMode === "CSS1Compat") ?
            document.documentElement :
            document.body;
    var height = elem.clientHeight;
    var restSpace = height - 90;
    document.getElementById("body").style.height = height;
    var y = document.getElementsByClassName("restSpace");
    for (var i = 0, len = y.length; i < len; i++)
        y[i].style.height = restSpace + 'px';
    document.getElementById("map").style.height = restSpace + 'px';
}
window.onresize = changeSize;
//marker moving smoothly  
var x;
var numDeltas = 100;
var delay = 60; //milliseconds
var i = 0;
var deltaLat;
var deltaLng;
function transition(result, ids) {
    x = ids;
    i = 0;
    deltaLat = (result[0] - position[0]) / numDeltas;
    deltaLng = (result[1] - position[1]) / numDeltas;
    moveMarker();
}

function moveMarker() {
    position[0] += deltaLat;
    position[1] += deltaLng;
    var latlng1 = new google.maps.LatLng(position[0], position[1]);
    if (markerStore[x]) {
        markerStore[x].setPosition(latlng1);
    }

    if (i != numDeltas) {
        i++;
        setTimeout(moveMarker, delay);
    }
}