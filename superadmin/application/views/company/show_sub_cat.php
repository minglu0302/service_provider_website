<?php
date_default_timezone_set('UTC');
$rupee = "$";
if ($status == 5) {
    $vehicle_status = 'New';
    $new = "active";
    echo '<style> .searchbtn{float: left;  margin-right: 63px;}.dltbtn{float: right;}</style>';
} else if ($status == 2) {
    $vehicle_status = 'Accepted';
    $accept = "active";
} else if ($status == 4) {
    $vehicle_status = 'Rejected';
    $reject = 'active';
} else if ($status == 2) {
    $vehicle_status = 'Free';
    $free = 'active';
} else if ($status == 1) {

    $active = 'active';
}
?>
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<style>
    .ui-autocomplete{
        z-index: 5000;
    }
    #selectedcity,#companyid{
        display: none;
    }

    .ui-menu-item
    {
        cursor: pointer;background: black;color:white;border-bottom: 1px solid white;width: 200px;
    }
        .exportOptions{
        display: none;
    }
</style>
<script>
function LoadCategory()
{
        var cityid = $("#cityid option:selected").val();
        $.ajax({
            url: "<?php echo base_url('index.php/superadmin') ?>/GetServicesByCity",
            type: "POST",
            data: {cid: cityid},
            dataType: 'JSON',
            beforeSend: function () {
                $('#catloading').show();
            },
            complete: function () {
                $('#catloading').hide();
            },
            success: function (response)
            {
                $('#catlist').html('');
                $('#catlist').append('<option value="0">Select Category</option>');
                $.each(response, function (key, value)
                {
                    $('#catlist').append('<option value="' + value['id'] + '">' + value['cat_name'] + '</option>');
                });
            }
        });
    }
</script>



<script>




    $(document).ready(function () {
        $('.cities').addClass('active');
        $('.cities_thumb').attr('src', "<?php echo base_url(); ?>/theme/icon/cities_on.png");
        $('.error-box-class').keypress(function () {
            $('.error-box').text('');
        });

        $('#editcitypage').click(function () {
            $("#display-data").text("");
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
            if (val.length == 0) {
                $("#display-data").text(<?php echo json_encode(POPUP_SUB_CAT_ANYONE); ?>);
            } else if (val.length > 1) {
                $("#display-data").text(<?php echo json_encode(POPUP_SUB_CAT_ONLYONE); ?>);
            } else {
                var size = $('input[name=stickup_toggler]:checked').val();
                var modalElem = $('#sub_cat_mod');
                $('#latitudeedit').val($('.checkbox:checked').parent().prev().prev().text());
                $('#longitudeedit').val($('.checkbox:checked').parent().prev().text());

                if (size == "mini") {
                    $('#modalStickUpSmall').modal('show')

                } else
                {
                    val = val.toString();
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo base_url('index.php/superadmin') ?>/GetOneSubServices",
                        data: {sid: val},
                        dataType: 'JSON',
                        success: function (result)
                        {
                                $("#close_nos").trigger("click");
                                $('#cityid').val(result[0].city_id);
                                $('#catlist').val(result[0].cat_id);
                                $('#sub_cat_name').val(result[0].cat_name);
                                $('#sub_cat_desc').val(result[0].cat_desc);
                                $('#min_fees').val(result[0].min_fees);
                                $('#base_fees').val(result[0].base_fees);
                                $('#price_per_min').val(result[0].price_min);
                                $('#start_price').val(result[0].start_price);
                                $('#EditServiceId').val(val);
                                $('#sub_cat_mod').modal('show')      
                        }
                    });
                }
            }
        });
        
        
         $("#insertsubcat").click(function () {
            $("#addsubcat").text("");
            var cityid = $("#cityid").val();
            var catlist = $("#catlist").val();
            var sub_cat_name = $("#sub_cat_name").val();
            var sub_cat_desc = $("#sub_cat_desc").val();
            var min_fees = $("#min_fees").val();
            var base_fees = $("#base_fees").val();
            var price_per_min = $("#price_per_min").val();
            var start_price = $("#start_price").val();
            var EditServiceId = $("#EditServiceId").val();
            if (cityid == "0") {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_CITY); ?>);
            } else if (catlist == "0") {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_CATEGORY); ?>);
            } else if (sub_cat_name == "" || sub_cat_name == null) {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_SUB_CATEGORY); ?>);
            } else if (sub_cat_desc == "" || sub_cat_desc == null) {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_SUB_CATEGORY_DESC); ?>);
            } else if (min_fees == "" || min_fees == null) {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_MIN_FEES); ?>);
            } else if (base_fees == "" || base_fees == null) {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_BASE_FEES); ?>);
            } else if (price_per_min == "" || price_per_min == null) {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_PRICE_PER_MIN); ?>);
            } else if (start_price == "" || start_price == null) {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_START_PRICE); ?>);
            } else
            {
                $.ajax({
                    url: "<?php echo base_url('index.php/superadmin') ?>/AddNewSubCategory",
                    type: 'POST',
                    data: {
                        cid: cityid,
                        catid: catlist,
                        scname: sub_cat_name,
                        scdesc: sub_cat_desc,
                        min_fees: min_fees,
                        base_fees: base_fees,
                        pr_min: price_per_min,
                        sp: start_price,
                        SubCategoryId : EditServiceId
                    },
                    dataType: 'JSON',
                    success: function (response)
                    {
                        $(".close").trigger("click");
                         location.reload();
                    }
                });
            }
        });



        $("#chekdel").click(function () {
            $("#display-data").text("");
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
            if (val.length > 0)
            {
                val = val.toString();
                var size = $('input[name=stickup_toggler]:checked').val()
                var modalElem = $('#confirmmodel');
                if (size == "mini") {
                    $('#modalStickUpSmall').modal('show')
                } else {
                    $('#confirmmodel').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }
                $("#errorboxdata").text(<?php echo json_encode(SUB_CAT_DELETE); ?>);
                $("#confirmed").click(function () {
                    $.ajax({
                        url: "<?php echo base_url('index.php/superadmin') ?>/DeleteSubCategory",
                        type: "POST",
                        data: {val: val},
                        dataType: 'json',
                        success: function (result)
                        {
                            $(".close").trigger("click");                            
                            var size = $('input[name=stickup_toggler]:checked').val()
                            var modalElem = $('#confirmmodels');
                            if (size == "mini")
                            {
                                $('#modalStickUpSmall').modal('show')
                            } else
                            {
                                $('#confirmmodels').modal('show')
                                if (size == "default") {
                                    modalElem.children('.modal-dialog').removeClass('modal-lg');
                                } else if (size == "full") {
                                    modalElem.children('.modal-dialog').addClass('modal-lg');
                                }
                            }
                            if (result.flag == '1') {
                                $("#errorboxdatas").text(result.msg);

                            } else if (result.flag == '0') {
                                $("#errorboxdatas").text(result.msg);
                                    $('.checkbox:checked').each(function (i) {
                                        $(this).closest('tr').remove();
                                    });
                            }
                            $("#confirmeds").hide();
                        }
                    });
                });

            } else
            {
                $("#display-data").text(<?php echo json_encode(POPUP_SUB_CAT_ATLEAST); ?>);
            }

        });
    });
</script>

<div class="page-content-wrapper" style="padding-top: 20px">
    <!-- START PAGE CONTENT -->
    <div class="content">
        <!-- START JUMBOTRON -->
        <div class="brand inline" style="  width: auto;
             font-size: 20px;
             color: gray;
             margin-left: 30px;padding-top: 20px;">
            <strong style="color:#0090d9;"> <?php echo $catdata[0]['cat_name']; ?> => SUB CATEGORY</strong>
        </div>
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <div class="panel panel-transparent ">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-tabs-fillup  bg-white">
                        <li class="active" style="cursor:pointer">
                            <a  data = ""><span><?php echo LIST_OF_SUB_CAT; ?></span></a>
                        </li>                    
                    </ul>
                    <!-- Tab panes -->

                    <div class="container-fluid container-fixed-lg bg-white">
                        <!-- START PANEL -->
                        <div class="panel panel-transparent">
                            <div class="panel-heading">
                                <div class="error-box" id="display-data" style="text-align:center"></div>                                     
                                <div class="pull-right m-t-10" class="btn-group" style="margin-left: 5px">
                                    <div class="pull-right m-t-10" class="btn-group" style="margin-left: 5px">
                                        <button type="button" class="btn btn-success" id="chekdel"><i class="fa fa-trash-o"></i>
                                        </button>
                                    </div>
                                    <div class="pull-right m-t-10" class="btn-group" style="margin-left: 5px">
                                        <button type="button" class="btn btn-success" id="editcitypage"><i class="fa fa-pencil"></i>
                                        </button>
                                    </div>
                                    <div class="m-t-10 pull-right" class="btn-group searchbtn" style="margin-left: 5px">
                                        <input type="text" id="search-table" class="form-control pull-right"  placeholder="<?php echo SEARCH; ?>"/> 
                                    </div>
                                </div>
                                &nbsp;
                                <div class="panel-body">
                                    <div class="container-fluid container-fixed-lg bg-white">
                                        <div class="panel panel-transparent">
                                            <div class="panel-heading">
                                                <div class="error-box" id="display-data" style="text-align:center"></div>
                                                <div class="pull-right m-t-10" class="btn-group" style="margin-left: 5px">
                                                </div>                                
                                                &nbsp;
                                                <div class="panel-body">
                                                    <table class="table table-hover demo-table-search dataTable no-footer tableWithSearch_referels" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">
                                                        <thead>
                                                            <tr role="row">
                                                                <th>SUB CATEGORY</th>
                                                                <th>CATEGORY DESC</th>
                                                                <th>MINIMUM FEES</th>
                                                                <th>BASE FEES</th>
                                                                <td>PRICE PER MIN</th>                                                
                                                                <th>STARTING PRICE</th>
                                                                <th>OPTION</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            foreach ($sub_data as $result) {
                                                                ?>
                                                                <tr role="row"  class="gradeA odd">
                                                                    <td id = "d_no" class="v-align-middle sorting_1"> <p><?php echo $result['cat_name']; ?></p></td>
                                                                    <td id = "d_no" class="v-align-middle sorting_1"> <p><?php echo $result['cat_desc']; ?></p></td>
                                                                    <td id = "d_no" class="v-align-middle sorting_1"> <p><?php echo $result['min_fees']; ?></p></td>
                                                                    <td id = "d_no" class="v-align-middle sorting_1"> <p><?php echo $result['base_fees']; ?></p></td>
                                                                    <td id = "d_no" class="v-align-middle sorting_1"> <p><?php echo $result['price_min']; ?></p></td>
                                                                    <td id = "d_no" class="v-align-middle sorting_1"> <p><?php echo $result['start_price']; ?></p></td>
                                                                    <td id = "d_no" class="v-align-middle sorting_1"> <p><input type="checkbox" class="checkbox" value="<?php echo $result['id']; ?>"></p></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>





                </div>

            </div>
            <!-- END PANEL -->
        </div>
    </div>


    <div class="container-fluid container-fixed-lg footer">
        <div class="copyright sm-text-center">
            <p class="small no-margin pull-left sm-pull-reset">
                <span class="hint-text">Copyright @ 3Embed software technologies, All rights reserved</span>

            </p>

            <div class="clearfix"></div>
        </div>
    </div>










    <div class="modal fade stick-up" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">

                    <div class="modal-header">

                        <div class=" clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                            </button>

                        </div>
                        <h3> <?php echo SELECT_COUNTRY_ANDCITY; ?></h3>
                    </div>

                    <br>
                    <br>

                    <div class="modal-body">

                        <div class="form-group" class="formex">
                            <label for="fname" class="col-sm-4 control-label" ><?php echo FIELD_CITIES_COUNTRY; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-6">

                                <select id="countryid" name="country_select"  class="form-control error-box-class" onchange="LoadCities()">
                                    <option value="0">Select Country</option>
                                    <?php
                                    foreach ($country as $result) {

                                        echo "<option value=" . $result->Country_Id . ">" . $result->Country_Name . "</option>";
                                    }
                                    ?>

                                </select>
                            </div>
                        </div>

                        <br>
                        <br>
                        <div class="form-group" class="formex">
                            <div class="frmSearch">
                                <label for="fname" class="col-sm-4 control-label"><?php echo FIELD_VEHICLE_SELECTCITY; ?><span style="color:red;font-size: 18px">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" id="search-box"  placeholder="City Name" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>
                                    <input type="hidden" id="search-box-hidden" class="form-control error-box-class" />
                                    <div id="suggesstion-box"></div>
                                </div>
                            </div>
                        </div>
                        <!--                    <div class="form-group" class="formex">
                                                <label for="fname" class="col-sm-4 control-label">Select city</label>
                                                <div class="col-sm-6">
                        
                                                    <select id="selectedcity" name="city_select"  class="form-control" >
                        
                                                    </select>
                                                </div>
                                            </div>-->

                        <br>
                        <br>

                        <div class="form-group" class="formex">
                            <label for="fname" class="col-sm-4 control-label">    <?php echo FIELD_VEHICLETYPE_LATITUDE; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-6">
                                <input type="text"  id="latitude" name="latitude"  class="form-control error-box-class" placeholder="eg:234.3432">
                            </div>
                        </div>

                        <br>
                        <br>

                        <div class="form-group" class="formex">
                            <label for="fname" class="col-sm-4 control-label"> <?php echo FIELD_VEHICLETYPE_LONGITUDE; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-6">
                                <input type="text"  id="longitude" name="longitude" class="form-control error-box-class" placeholder="eg:3632.465">
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-4" ></div>
                            <div class="col-sm-4 error-box" id="addcity"></div>
                            <div class="col-sm-4" >
                                <button type="button" class="btn btn-primary pull-right" id="insert" ><?php echo BUTTON_ADD; ?></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
        </button>
    </div>


    <div class="modal fade stick-up" id="myanotherModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">

                    <div class="modal-header">

                        <div class=" clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                            </button>

                        </div>
                        <h3> <?php echo LIST_CITY_TABLE_EDITCITIESGEO ?> </h3>
                    </div>
                    <br>
                    <br>


                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-3 control-label"><?php echo FIELD_VEHICLETYPE_LATITUDE; ?></label>
                        <div class="col-sm-6">
                            <input type="text"  id="latitudeedit" name="latitude"  class="form-control error-box-class" placeholder="eg:234.3432">
                        </div>
                    </div>

                    <br>
                    <br>

                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-3 control-label"><?php echo FIELD_VEHICLETYPE_LONGITUDE; ?></label>
                        <div class="col-sm-6">
                            <input type="text"  id="longitudeedit" name="longitude" class="form-control error-box-class" placeholder="eg:3632.465">
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4 error-box" id="longlat" ></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right changeMode" id="insertlonlat" ><?php echo BUTTON_SUBMIT ?></button>
                        </div>
                    </div>
                </div>
            </div>


            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


 <!--delete sub category conformatio model-->
<div class="modal fade stick-up" id="confirmmodel" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>

                    </div>
                </div>
                <br>
                <div class="modal-body">
                    <div class="row">
                        <div class="error-box" id="errorboxdata" style="font-size: large;text-align:center"><?php echo COMPAIGNS_DISPLAY; ?></div>
                    </div>
                </div>
                <br>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="confirmed" ><?php echo BUTTON_YES; ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>



    <div class="modal fade stick-up" id="confirmmodels" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">

                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>

                    </div>

                </div>
                <br>
                <div class="modal-body">
                    <div class="row">

                        <div class="error-box" id="errorboxdatas" style="font-size: large;text-align:center"><?php echo VEHICLEMODEL_DELETE; ?></div>

                    </div>
                </div>

                <br>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="confirmeds" ><?php echo BUTTON_YES; ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    
    
    
    
    
    
    <div class="modal fade stick-up" id="sub_cat_mod" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="modal-header">
                                        <div class=" clearfix text-left">
                                            <button type="button" id="close_nos" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                                            </button>
                                        </div>
                                        <h3> <?php echo EDIT_SUB_CATEGORY_TEXT; ?></h3>
                                    </div>
                                    <br>
                                    <div class="modal-body">
                                        <div class="form-group" class="formex">
                                            <label for="fname" class="col-sm-4 control-label" ><?php echo FIELD_VEHICLE_SELECTCITY; ?><span style="color:red;font-size: 18px">*</span></label>
                                            <div class="col-sm-6">
                                                <select id="cityid" name="city_select"  class="form-control error-box-class" onchange="LoadCategory()">
                                                    <option value="0">Select City</option>
                                                    <?php
                                                    foreach ($citylist as $result) {
                                                        echo "<option value=" . $result->City_Id . ">" . $result->City_Name . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-1" id="catloading" style="display: none;">
                                                <img src="<?php echo base_url() ?>../../pics/loadingimg.gif" /> 
                                            </div>
                                        </div>
                                        <br>
                                        <br>
                                        <div class="form-group" class="formex">
                                            <label for="fname" class="col-sm-4 control-label" ><?php echo FIELD_CATEGORY_NAME; ?><span style="color:red;font-size: 18px">*</span></label>
                                            <div class="col-sm-6">
<!--                                                <select id="catlist" name="fdata[cat]"  class="form-control error-box-class">                                                          
                                                </select>-->
                                                <select id="catlist" name="fdata[cat]"  class="form-control error-box-class">
                                                    <option value="0">Select Category</option>
                                                    <?php
                                                    foreach ($mcat as $result) {
                                                        echo "<option value=" . $result['id'] . ">" . $result['type_name'] . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <br>
                                        <br>

                                        <div class="form-group" class="formex">
                                            <div class="frmSearch">
                                                <label for="fname" class="col-sm-4 control-label"><?php echo FIELD_SUB_CATEGORY_NAME; ?><span style="color:red;font-size: 18px">*</span></label>
                                                <div class="col-sm-8">
                                                    <input name="fdata[subcatname]" type="text" id="sub_cat_name"  placeholder="Sub Category Name" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>
                                                    <div id="suggesstion-box"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <br>
                                        <div class="form-group" class="formex">
                                            <label for="fname" class="col-sm-4 control-label"><?php echo FIELD_SUB_CATEGORY_DESC; ?><span style="color:red;font-size: 18px">*</span></label>
                                            <div class="col-sm-6">
                                                <input type="text"  id="sub_cat_desc" name="fdata['subcatdesc']"  class="form-control error-box-class" placeholder="Sub Category Desc">
                                            </div>
                                        </div>
                                        <br>
                                        <br>
                                        <div class="form-group" class="formex">
                                            <label for="fname" class="col-sm-4 control-label"> <?php echo FIELD_SUB_MIN_FEES; ?><span style="color:red;font-size: 18px">*</span></label>
                                            <div class="col-sm-6">
                                                <input type="text"  id="min_fees" name="fdata[min_fees]" class="form-control error-box-class" placeholder="Minimum Fees">
                                            </div>
                                        </div>
                                        <br>
                                        <br>
                                        <div class="form-group" class="formex">
                                            <label for="fname" class="col-sm-4 control-label"> <?php echo FIELD_SUB_BASE_FEES; ?><span style="color:red;font-size: 18px">*</span></label>
                                            <div class="col-sm-6">
                                                <input type="text"  id="base_fees" name="fdata[base_fees]" class="form-control error-box-class" placeholder="Base Fees">
                                            </div>
                                        </div>   
                                        <br>
                                        <br>
                                        <div class="form-group" class="formex">
                                            <label for="fname" class="col-sm-4 control-label"> <?php echo FIELD_PRICE_PER_MIN; ?><span style="color:red;font-size: 18px">*</span></label>
                                            <div class="col-sm-6">
                                                <input type="text"  id="price_per_min" name="fdata[price_per_min]" class="form-control error-box-class" placeholder="Price Per Minute">
                                            </div>
                                        </div>
                                        <br>
                                        <br>
                                        <div class="form-group" class="formex">
                                            <label for="fname" class="col-sm-4 control-label"> <?php echo FIELD_START_PRICE; ?><span style="color:red;font-size: 18px">*</span></label>
                                            <div class="col-sm-6">
                                                <input type="text"  id="start_price" name="fdata[start_price]" class="form-control error-box-class" placeholder="Starting Price">
                                            </div>
                                        </div>
                                        <input type="hidden" id="EditServiceId" value=""/>
                                        <div class="row">
                                            <div class="col-sm-4" ></div>
                                            <div class="col-sm-4 error-box" id="addsubcat"></div>
                                            <div class="col-sm-4" >
                                                <button type="button" class="btn btn-primary pull-right" id="insertsubcat" ><?php echo BUTTON_SAVE; ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                    </div>
