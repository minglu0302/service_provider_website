<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyCK5bz3K1Ns_BASkAcZFLAI_oivKKo98VE"></script>

<style>
    small, .small {
    font-size: 15px;
}
    .header{
        height:60px !important;
    }
    .header h3{
        margin:10px 0 !important;
    }
    .rating>.rated {
        color: #10cfbd;
    }
    .social-user-profile {
        width: 83px;
    }
    #selectedcity,#companyid{
        display: none;
    }
    .row {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display:         flex;
        flex-wrap: wrap;
    }
    .row > [class*='col-'] {
        display: flex;
        flex-direction: column;
    }
    #map{
        width: 100%; 
        height: 100%;
    }
    @media (max-width: 768px){
        #map{
            width: 100%; 
            height: 100vh;
        }
    }
</style>
<?php
$arrived_route = array();
$arrivedroute = json_decode($data['res']['arrived_route'],True);
foreach ($arrivedroute as $trip_data) {
    if ($trip_data['lat'] != '' || $trip_data['long'] != '' ||
            $trip_data['lat'] != NULL || $trip_data['long'] != NULL ||
            $trip_data['lat'] != null || $trip_data['long'] != null) {
        $trip_data['time'] = date('H:i:s', $trip_data['timestamp']/1000);
        $arrived_route[] = $trip_data;
    }
}

$pick_routes = array();
$picup_route = json_decode($data['res']['pickup_route'],True);
foreach ($picup_route as $trip_data) {
    if ($trip_data['lat'] != '' || $trip_data['long'] != '' ||
            $trip_data['lat'] != NULL || $trip_data['long'] != NULL ||
            $trip_data['lat'] != null || $trip_data['long'] != null) {
        $trip_data['time'] = date('H:i:s', $trip_data['timestamp']/1000);
        $pick_routes[] = $trip_data;
    }
}

$routes = array();
$app_route = json_decode($data['res']['app_route'],True);
foreach ($app_route as $trip_data) {
    if ($trip_data['lat'] != '' || $trip_data['long'] != '' ||
            $trip_data['lat'] != NULL || $trip_data['long'] != NULL ||
            $trip_data['lat'] != null || $trip_data['long'] != null) {
        $trip_data['time'] = date('H:i:s', (int)($trip_data['timestamp']/1000));
        $routes[] = $trip_data;
    }
}

$start = $routes[0];

$startLat = $start[1];
$startLong = $start[2];

$end = end($routes);

$endLat = $end[1];
$endLong = $end[2];

$routes_google[0] = array('long' => (Float) $data['res']['appointment_location']['longitude'], 'lat' => (Float) $data['res']['appointment_location']['latitude']);
$routes_google[1] = array('long' => (Float) $data['res']['Drop_location']['longitude'], 'lat' => (Float) $data['res']['Drop_location']['latitude']);

$apptdata = $data['res'];
$ratings =$data['master_rating_data']->star_rating;
if($apptdata['status'] == 4 ||$apptdata['status'] == 5){
    $apptdata['invoice'][0]['TripDistance'] = "0";
    $apptdata['invoice'][0]['TripTimeFee'] = "0";
    $apptdata['invoice'][0]['waitingTimeFee'] = "0";
    $apptdata['invoice'][0]['airportFee'] = "0";
    $apptdata['invoice'][0]['subtotal'] = $apptdata['amount'];
    $apptdata['invoice'][0]['discountVal'] = "0";
    $apptdata['invoice'][0]['finalAmount'] = $apptdata['amount'];
    $apptdata['invoice'][0]['cashCollected'] = "0";
    $apptdata['invoice'][0]['app_commission'] = $apptdata['app_commission'];
    $apptdata['invoice'][0]['mas_earning'] = $apptdata['mas_earning'];
}
 
$exp = explode(' ', $apptdata['appt_date']);
        $bookindDate = $exp[0];
        $bookindTime = $exp[1];
?>
<div class="content">
    <div class="jumbotron  no-margin" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;">
                <h3 class="">Page Title</h3>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg">
        <ul class="breadcrumb">
            <li>
                <a href="<?= base_url() ?>index.php/superadmin/CompleteJob">Completed Bookings</a>
            </li>
            <li>
                <a href="#" class="active">Booking Details - <?php echo $apptdata['_id']; ?></a>
            </li>
        </ul>
        <div class="container-md-height m-b-20">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Booking Details - <?= ($apptdata['status'] == '7')?"Completed":"Cancelled"?>
                    </div>
                </div>
                <div class="panel-body no-padding">
                    <div class="row m-b-15">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="panel">
                                <ul class="nav nav-tabs nav-tabs-simple" role="tablist" data-init-reponsive-tabs="collapse">
                                    <li class="active">
                                        <a href="#customer_details" data-toggle="tab" role="tab" class="tabChange" aria-expanded="true">Customer Details</a>
                                    </li>
                                    <li class="">
                                        <a href="#driver_details" data-toggle="tab" role="tab" class="tabChange" aria-expanded="false">Provider Details</a>
                                    </li>
                                </ul><div class="panel-group visible-xs" id="LaCQy-accordion"></div>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="customer_details">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Booking ID</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo  $apptdata['bid']; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Booking Type</a>
                                                </p>
                                                <p class="small">
                                                    <?php
                                                    if ($apptdata['booking_type'] == 3)
                                                        echo "Now";
                                                    else
                                                        "Later";
                                                    ?>
                                                </p>
                                            </div>
                                             <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Booking Date</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo $bookindDate; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Booking Time</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo $bookindTime; ?>
                                                </p>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class='row'>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Customer</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo $apptdata['customer']['fname']; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Phone</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo $apptdata['customer']['mobile']; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Email</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo $apptdata['customer']['email']; ?>
                                                </p>
                                            </div>
                                        </div>
                                        <hr/>
<!--                                        <div class='row'>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Vehicle Id</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo $apptdata['VehicleDetails']['vehicleID']; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Plate Number</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo $apptdata['VehicleDetails']['plateNo']; ?>
                                                </p>
                                            </div>
                                        </div>
                                        <hr/>-->
                                        <div class='row'>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Job Start Date</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo $apptdata['job_start_time']; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Job End Date</a>
                                                </p>
                                                <p class="small">
                                                        <?php echo $apptdata['job_end_time']; ?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class='row'>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Job Location</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo ($apptdata['address1'])?$apptdata['address1']:"-------"; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Trip Time</a>
                                                </p>
                                                <p class="small">
                                                    <?php // echo $apptdata['invoice'][0]['TripTime'] . " Minutes"; ?>
                                                    <?php 
                                                        $TripTime = (int) strtotime($apptdata['job_start']) - (int) strtotime($apptdata['job_end']);
                                                        if($apptdata['job_end_time'] == '' || $apptdata['job_end_time'] == ''){
                                                            echo "0 Minutes";
                                                        }else{
                                                            echo (ceil($TripTime/60)) . " Minutes"; 
                                                        }
                                                    ?>
                                                </p>
                                            </div>
                                             
                                        </div>
<!--                                        <div class='row'>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Waiting Time</a>
                                                </p>
                                                <p class="small">
                                                    <?php 
                                                        $wait_time = (int) $apptdata['BookingStartTimeStampServer'] - $apptdata['DriverArrivedTimeStampServer'];
//                                                        if($wait_time < 60)
//                                                            echo $wait_time . " Seconds"; 
//                                                        else
//                                                            echo ((int) ($wait_time/60) ) . " Minutes " . ($wait_time - (((int) ($wait_time/60) ) * 60)) . " Seconds"; 
                                                        if($apptdata['BookingStartTimeStampServer'] == '' || $apptdata['DriverArrivedTimeStampServer'] == ''){
                                                            echo "0 Minutes";
                                                        }else{
                                                            echo (ceil($wait_time/60)) . " Minutes";
                                                        }
                                                    ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Trip Time</a>
                                                </p>
                                                <p class="small">
                                                    <?php // echo $apptdata['invoice'][0]['TripTime'] . " Minutes"; ?>
                                                    <?php 
                                                        $TripTime = (int) strtotime($apptdata['drop_dt']) - $apptdata['BookingStartTimeStampServer'];
                                                        if($apptdata['drop_dt'] == '' || $apptdata['BookingStartTimeStampServer'] == ''){
                                                            echo "0 Minutes";
                                                        }else{
                                                            echo (ceil($TripTime/60)) . " Minutes"; 
                                                        }
                                                    ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Trip Distance</a>
                                                </p>
                                                <p class="small">
                                                    <?php
                                                        echo $apptdata['invoice'][0]['TripDistance'] . ((customerRadiusUnit == 6371)?" KM" : " Miles");
                                                    ?>
                                                </p>
                                            </div>
                                        </div>-->
                                        <hr/>
                                        <div class='row'>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Provider Accept Date</a>
                                                </p>
                                                <p class="small">
                                                    <?php 
                                                        if($apptdata['accepted_dt'] != ''){
                                                            echo $apptdata['accepted_dt']; 
                                                        }else{
                                                            echo "-----";
                                                        }
                                                    ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Provider Arrived Date</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo ($apptdata['proarrived_device'] == "")?"-----":date('Y-d-m H:i:s', $apptdata['proarrived_device']); ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Provider OntheWay Date</a>
                                                </p>
                                                <p class="small">
                                                    <?php  
                                                        if($apptdata['proontheway_device'] != ''){
                                                            echo date('Y-d-m H:i:s', $apptdata['proontheway_device']); 
                                                        }else{
                                                            echo "-----";
                                                        }
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="driver_details">
                                        <div class="row-xs-height">
                                            <div class="social-user-profile col-xs-height text-center col-top">
                                                <div class="thumbnail-wrapper circular bordered b-white">
                                                    <img alt="<?php echo $apptdata['provider']['fname']; ?>" width="55" height="55" style="cursor:pointer;"
                                                         onclick='openimg(this)'
                                                         data-src-retina="<?php echo $apptdata['provider']['pic']; ?>" 
                                                         data-src="<?php echo $apptdata['provider']['pic']; ?>"
                                                         src="<?php echo $apptdata['provider']['pic']; ?>"
                                                         onerror="this.src = 'http://188.166.233.185/pics/user.jpg'">
                                                </div>
                                                <p class="rating text-success">
                                                    <?php
                                                    $rating = $ratings;
                                                    $half = $rating - (int)$rating;
                                                    for ($i = 1; $i <= (int)$rating; $i++) {
                                                        echo '<i class="fa fa-star"></i>';
                                                    }
                                                    if($half >0.25){
                                                        echo '<i class="fa fa-star-half-o"></i>';
                                                        $i++;
                                                    }
                                                    while($i<=5){
                                                        echo '<i class="fa fa-star-o"></i>';
                                                        $i++;
                                                    }
                                                    ?>
                                                </p>
                                            </div>
                                            <div class="col-xs-height p-l-20">
                                                <h3 class="no-margin"><?php echo $apptdata['provider']['fname']; ?></h3>
                                                <p class="no-margin fs-16"><?php echo $apptdata['provider']['email'];; ?></p>
                                                <p class="no-margin fs-16"><?php echo $apptdata['provider']['mobile'];; ?></p>
                                            </div>
                                        </div>      
                                        <hr/>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Visit Fee</a>
                                                </p>
                                                <p class="small">
                                                    <?php 
                                                    if($apptdata['visit_fees'] == '')
                                                    {
                                                        $apptdata['visit_fees'] =0;
                                                    }
                                                    echo CURRENCY. " " . $apptdata['visit_fees']; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Misc Fee</a>
                                                </p>
                                                <p class="small">
                                                    <?php 
                                                    if($apptdata['comp']['misc_fees'] == '')
                                                    {
                                                        $apptdata['comp']['misc_fees'] =0;
                                                    }
                                                    echo CURRENCY. " " . $apptdata['comp']['misc_fees']; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Misc Fee</a>
                                                </p>
                                                <p class="small">
                                                    <?php 
                                                    if($apptdata['comp']['mat_fees'] == '')
                                                    {
                                                        $apptdata['comp']['mat_fees'] =0;
                                                    }
                                                    echo CURRENCY. " " . $apptdata['comp']['mat_fees']; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Time Fee</a>
                                                </p>
                                                <p class="small">
                                                    <?php 
                                                    if($apptdata['comp']['time_fees'] == '')
                                                    {
                                                        $apptdata['comp']['time_fees'] =0;
                                                    }
                                                    echo CURRENCY. " " . $apptdata['comp']['time_fees']; ?>
                                                </p>
                                            </div>
                                            
                                            <?php
                                            foreach ($apptdata['services'] as $service) {?>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#"> <?php echo $service['sname']; ?></a>
                                                </p>
                                                <p class="small">
                                                    <?php echo $service['sprice']; ?>
                                                </p>
                                            </div><?php
            
        }
                                            ?>
                                            
                                             
<!--                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Waiting Time Fee</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo $apptdata['invoice'][0]['waitingTimeFee'] . " " . CURRENCY; ?>
                                                </p>
                                            </div>-->
<!--                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Waiting Time</a>
                                                </p>
                                                <p class="small">
                                                    <?php 
//                                                        $wait_time = (int) $apptdata['BookingStartTimeStampServer'] - $apptdata['DriverArrivedTimeStampServer'];
//                                                        echo gmdate("H:i:s", (ceil($wait_time/60)*60)); 
                                                    ?>
                                                    <?php 
                                                        $wait_time = (int) $apptdata['BookingStartTimeStampServer'] - $apptdata['DriverArrivedTimeStampServer'];
//                                                        if($wait_time < 60)
//                                                            echo $wait_time . " Seconds"; 
//                                                        else
//                                                            echo ((int) ($wait_time/60) ) . " Minutes " . ($wait_time - (((int) ($wait_time/60) ) * 60)) . " Seconds"; 
                                                        if($apptdata['BookingStartTimeStampServer'] == '' || $apptdata['DriverArrivedTimeStampServer'] == ''){
                                                            echo "0 Minutes";
                                                        }else{
                                                            echo (ceil($wait_time/60)) . " Minutes";
                                                        }
                                                    ?>
                                                </p>
                                            </div>-->
<!--                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Airport Fee</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo CURRENCY . " " . $apptdata['invoice'][0]['airportFee'];  ?>
                                                </p>
                                            </div>-->
                                        </div>
                                            <hr>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <p class="no-margin bold">
                                                    <a href="#">Subtotal</a>
                                                </p>
                                                <p class="small bold">
                                                    <?php echo CURRENCY . " " . $apptdata['invoice'][0]['subtotal']; ?>
                                                </p>
                                            </div>
                                             <div class="col-sm-6">
                                               
                                            </div>
                                             <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Coupon Discount</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo CURRENCY. " " . $apptdata['coupon_discount']; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#"> Pro Discounts</a>
                                                </p>
                                                <p class="small">
                                                    <?php 
                                                    if($apptdata['comp']['pro_disc'] == '')
                                                    {
                                                        $apptdata['comp']['pro_disc'] =0;
                                                    }
                                                    echo CURRENCY . " " . $apptdata['comp']['pro_disc']; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin bold">
                                                    <a href="#">Final Fare</a>
                                                </p>
                                                <p class="small bold">
                                                    <?php echo CURRENCY . " " . $apptdata['invoiceData']['billed_amount']; ?>
                                                </p>
                                            </div>
                                             
                                            <div class="col-sm-6">
                                               
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Paid Amount</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo CURRENCY . " " . $apptdata['invoiceData']['billed_amount']; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Payment Method</a>
                                                </p>
                                                <p class="small">
                                                    <?php if ($apptdata['payment_type'] == 2) echo 'Card';
                                                    else echo 'Cash'; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">APP Commission</a>
                                                </p>
                                                <p class="small">
                                                   <?php echo CURRENCY . " " . $apptdata['invoiceData']['app_earning']; ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Provider Earnings</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo CURRENCY . " " . $apptdata['invoiceData']['pro_earning']; ?>
                                                </p>
                                            </div>
                                             <?php if($apptdata['payment_type'] == 2 ){ ?>
                                             <div class="col-sm-6">
                                               <p class="no-margin">
                                                    <a href="#">Payment Gateway Fee </a>
                                                </p>
                                                <p class="small ">
                                                    <?php echo CURRENCY . " " . $apptdata['invoiceData']['app_pg_comm']; ?>
                                                </p>
                                            </div>
                                             <div class="col-sm-6">
                                               <p class="no-margin">
                                                    <a href="#">Payment Gateway Fee</a>
                                                </p>
                                                <p class="small ">
                                                    <?php echo CURRENCY . " " . $apptdata['invoiceData']['pro_pg_comm']; ?>
                                                </p>
                                            </div>
                                               
                                              <?php } ?>
                                        </div>
                                        <hr/>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <p class="no-margin">
                                                    <a href="#">Notes By Customer</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo ($apptdata['comp']['complete_note'])?$apptdata['comp']['complete_note']:"--"; ?>
                                                </p>
                                            </div>
                                        </div>
                                        <?php if($apptdata['status'] == 4 ||$apptdata['status'] == 5){ ?>
                                        <hr/>
                                        <div class='row'>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Trip Cancelled Time</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo date('h:i A', strtotime($apptdata['cancel_dt'])); ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Trip Cancelled By</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo (($apptdata['status'] == 4)?"Passenger":"Driver"); ?>
                                                </p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p class="no-margin">
                                                    <a href="#">Cancelled Reason</a>
                                                </p>
                                                <p class="small">
                                                    <?php echo $apptdata['Reason']; ?>
                                                </p>
                                            </div>
                                        </div>
                                        <?php }?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <div id="map"></div>
                            <!--<img src="<?= $apptdata['routeImg'] . "&size=680x680"?>" style="width:100%;">-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<script type="text/javascript">

    jQuery(function () {
        var stops = jQuery.parseJSON('<?php echo json_encode($routes); ?>');
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: new google.maps.LatLng(<?php echo $data['res']['appointment_location']['latitude']; ?>, <?php echo $data['res']['appointment_location']['longitude']; ?>),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        
        var bounds = new window.google.maps.LatLngBounds();

        // extend bounds for each record
        var ico;
        var msg = "Driver Accepted - ";
        var j = 0;
        var i = 0;
        var markers = [];
        var myLatlng = '';
        var inf = new google.maps.InfoWindow;
        var cust_pick = jQuery.parseJSON('<?php echo json_encode($data['res']['cust_appointment_location']); ?>');
        var cust_drop = jQuery.parseJSON('<?php echo json_encode($data['res']['cust_Drop_location']); ?>');
        var pstops = jQuery.parseJSON('<?php echo json_encode($arrived_route); ?>');
        
        if(cust_pick != null && cust_pick != ''){
            myLatlng = new window.google.maps.LatLng(cust_pick.latitude, cust_pick.longitude);
            markers[i] = new google.maps.Marker({
                icon: ico,
                map: map,
                animation: google.maps.Animation.DROP,
                msg: "Customer Pickup",
                position: myLatlng
            });
            google.maps.event.addListener(markers[i], 'click', function () {
                inf.setContent(this.msg);
                inf.open(map, this);
            });
            i++;
        }
        if(cust_drop != null && cust_drop != ''){
            myLatlng = new window.google.maps.LatLng(cust_drop.latitude, cust_drop.longitude);
            markers[i] = new google.maps.Marker({
                icon: ico,
                map: map,
                animation: google.maps.Animation.DROP,
                msg: "Customer Drop",
                position: myLatlng
            });
            google.maps.event.addListener(markers[i], 'click', function () {
                inf.setContent(this.msg);
                inf.open(map, this);
            });
            i++;
        }
        ico = 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=1|FF00FF';
        jQuery.each(pstops, function (key, val) {
            myLatlng = new window.google.maps.LatLng(val.lat, val.long);
            msg += val.time;
            markers[i] = new google.maps.Marker({
                icon: ico,
                map: map,
                animation: google.maps.Animation.DROP,
                msg: msg,
                position: myLatlng
            });
//            if (j == 0) {
                j++;
                google.maps.event.addListener(markers[i], 'click', function () {
                    inf.setContent(this.msg);
                    inf.open(map, this);
                });
                msg = '';
//            }
            bounds.extend(myLatlng);
            i++;
            ico = 'https://storage.googleapis.com/support-kms-prod/SNP_2752264_en_v0';
        });
        
        pstops = jQuery.parseJSON('<?php echo json_encode($pick_routes); ?>');
        msg = "Driver Arrived - ";
        j = 0;
        ico = 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=2|FF9800';
        jQuery.each(pstops, function (key, val) {
            myLatlng = new window.google.maps.LatLng(val.lat, val.long);
            msg += val.time;
            markers[i] = new google.maps.Marker({
                icon: ico,
                map: map,
                animation: google.maps.Animation.DROP,
                msg: msg,
                position: myLatlng
            });
//            if (j == 0) {
                j++;
                google.maps.event.addListener(markers[i], 'click', function () {
                    inf.setContent(this.msg);
                    inf.open(map, this);
                });
                msg = '';
//            }
            bounds.extend(myLatlng);
            i++;
            ico = 'https://storage.googleapis.com/support-kms-prod/SNP_2752063_en_v0';
        });
        
        if (stops.length > 3)
        {
            console.log("route");
//            var map = new window.google.maps.Map(document.getElementById("map"));
//            var myOptions = {
//                zoom: 13,
//                center: new window.google.maps.LatLng(51.507937, -0.076188), // default to London
//                mapTypeId: window.google.maps.MapTypeId.ROADMAP
//            };
//            map.setOptions(myOptions);
            
            msg = "Appointment Started - ";
            j = 0;
            ico = 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=A|00FF00';
            console.log(stops.length);
            jQuery.each(stops, function (key, val) {
                myLatlng = new window.google.maps.LatLng(val.lat, val.long);
                msg += val.time;
                markers[i] = new google.maps.Marker({
                    icon: ico,
                    map: map,
                    animation: google.maps.Animation.DROP,
                    msg: msg,
                    position: myLatlng
                });
//                if (j == 0) {
                    google.maps.event.addListener(markers[i], 'click', function () {
                        inf.setContent(this.msg);
                        inf.open(map, this);
                    });
                    msg = "";
//                }
                ico = 'https://storage.googleapis.com/support-kms-prod/SNP_2752068_en_v0';
                bounds.extend(myLatlng);
                i++;
                j++;
            });
            markers[i - 1].setIcon('https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=B|FF0000');
            map.fitBounds(bounds);
            var marker, i;
        }
        else
        {
            console.log('google');
            var directionsDisplay;
            var directionsService = new google.maps.DirectionsService();
            directionsDisplay = new google.maps.DirectionsRenderer();

            directionsDisplay.setMap(map);

            var start = new google.maps.LatLng(<?php echo $data['res']['appointment_location']['latitude']; ?>, <?php echo $data['res']['appointment_location']['longitude']; ?>);
            var end = new google.maps.LatLng(<?php echo $data['res']['Drop_location']['latitude']; ?>, <?php echo $data['res']['Drop_location']['longitude']; ?>);

            var request = {
                origin: start,
                destination: end,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            };
            directionsService.route(request, function (response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                }
            });
        }
    });

    function openimg(imgid)
    {
        $('#modal-img').modal('show');
        var src = $(imgid).attr("src");
        console.log(src);
        $("#showimg").attr("src", src);
    }
</script>-->
<div class="modal fade fill-in" id="modal-img" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        <i class="pg-close"></i>
    </button>
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">

            </div>
            <div class="modal-body">
                <div class="row text-center" style="padding:20px;height:80vh;">   
                    <img src="" id="showimg" style="height:100%;">
                </div>  
            </div>
            <div class="modal-footer">
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>