<script src="<?= base_url() ?>theme/assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script>
//    $('.sm-table').find('.header-inner').html('<div class="brand inline" style="  width: auto;\
//                     font-size: 27px;\
//                     color: gray;\
//                     margin-left: 100px;margin-right: 20px;margin-bottom: 12px; margin-top: 10px;">\
//                    <strong><?= Appname ?> Super Admin Console</strong>\
//                </div>');

    $(document).ready(function () {
        var help_tbl = $('#help_table');
        var settings = {
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 20
        };
        help_tbl.DataTable(settings);
        $('#search-table2').keyup(function () {
            help_tbl.fnFilter($(this).val());
        });
        var sabcat_tbl = $('#subcat_table');
        var settings = {
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 20
        };
        help_tbl.DataTable(settings);
        $('#search-table3').keyup(function () {
            sabcat_tbl.fnFilter($(this).val());
        });
    });
    var settings = {
        "sDom": "<'table-responsive't><'row'<p i>>",
        "destroy": true,
        "scrollCollapse": true,
        "oLanguage": {
            "sLengthMenu": "_MENU_ ",
            "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
        },
        "iDisplayLength": 20
    };
    function viewsub_cat(cat_id) {
        $.ajax({
            url: "<?php echo base_url() ?>index.php/utilities/get_subcat_support",
            type: "POST",
            data: {cat_id: cat_id},
            dataType: "JSON",
            success: function (result) {
                var scat_tbl = $('#subcat_table').DataTable(settings);
                scat_tbl.clear().draw();
                var i = 1;
                $.each(result.sub_cat, function (ind, val) {
                    var name = $.map(val.name, function (v) {
                        return v;
                    }).join(', ');
                    var desc = $.map(val.desc, function (v) {
                        return v;
                    }).join(', ');
                    console.log(name);
                    scat_tbl.row.add([
                        i++,
                        name,
                        desc,
                        "<a class='btn btn-default cls111' href='<?= base_url()?>index.php/utilities/support_edit/" + String(result._id.$id) +"/" + String(val.scat_id.$id) +"' data-id='" + val._id + "'>\
                                <i class='fa fa-edit'></i> Edit\
                            </a>\
                            <a class='btn btn-default cls111' onclick='delscat(this)' data-id='" + String(val.scat_id.$id) + "'>\
                                <i class='fa fa-trash'></i> Delete\
                            </a>"
                    ]).draw();
                });
                $('#modal-subcat').modal('show');
            },
            error: function () {
                alert('Problem occurred please try agin.');
            },
            timeout: 30000
        });
    }
    function delcat(rowid){
        if (confirm("Are you sure you want to Delete?")) {
            $.ajax({
                url: "<?php echo base_url() ?>index.php/utilities/support_action/del",
                type: "POST",
                data: {id: $(rowid).attr('data-id')},
                dataType: "JSON",
                success: function (result) {
                    if (result.msg == '1') {
                        var hlp_tbl = $('#help_table').DataTable();
                        hlp_tbl.row($(rowid).closest('tr'))
                                .remove()
                                .draw();
                    } else {
                        alert('Problem in Deleting Group please try agin.');
                    }
                },
                error: function () {
                    alert('Problem in Deleting Group please try agin.');
                },
                timeout: 30000
            });
        }
    }
    function delscat(rowid){
        if (confirm("Are you sure you want to Delete?")) {
            $.ajax({
                url: "<?php echo base_url() ?>index.php/utilities/support_action/del/1",
                type: "POST",
                data: {id: $(rowid).attr('data-id')},
                dataType: "JSON",
                success: function (result) {
                    if (result.msg == '1') {
                        var scat_tbl = $('#subcat_table').DataTable();
                        scat_tbl.row($(rowid).closest('tr'))
                                .remove()
                                .draw();
                    } else {
                        alert('Problem in Deleting Group please try agin.');
                    }
                },
                error: function () {
                    alert('Problem in Deleting Group please try agin.');
                },
                timeout: 30000
            });
        }
    }
</script>

<style>
    .header{
        height:60px !important;
    }
    .header h3{
        margin:10px 0 !important;
    }
    .rating>.rated {
        color: #10cfbd;
    }
    .social-user-profile {
        width: 83px;
    }
    .table > thead > tr > th{
        font-size: 14px;
    }
</style>

<div class="content">
    <div class="jumbotron  no-margin" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;">
                <h3 class="">Page Title</h3>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg">
        <ul class="breadcrumb">
            <li>
                <a href="#" class="active">Support Text</a>
            </li>
            <!--            <li>
                            <a href="#" class="active">Job Details - <?php echo $data['appt_data']->appointment_id; ?></a>
                        </li>-->
        </ul>
        <div class="container-md-height m-b-20">
            <div class="panel panel-transparent">
                <ul class="nav nav-tabs nav-tabs-simple bg-white" role="tablist" data-init-reponsive-tabs="collapse">
                    <li class="active">
                        <a href="#hlp_txt" data-toggle="tab" role="tab" aria-expanded="false">Support Text</a>
                    </li>
                </ul>
                <div class="panel-group visible-xs" id="LaCQy-accordion"></div>
                <div class="tab-content">
                    <div class="tab-pane active" id="hlp_txt">
                        <div class="row panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title">Help Text</div>
                                <div class='pull-right cls110'>
                                    <a href='<?= base_url() ?>index.php/utilities/support_cat'>
                                        <button class='btn btn-success' id='add_new_cat'>
                                            <i class='fa fa-plus text-white'></i> Add New Category
                                        </button>
                                    </a>
                                </div>
                                <div class="panel-body no-padding">
                                    <table class="table table-hover demo-table-search" id="help_table">
                                        <thead>
                                            <tr>
                                                <th style="width:3%;">SL No.</th>
                                                <th style="width:12%;">Category</th>
                                                <th style="width:10%;">Languages</th>
                                                <th style="width:20%;">Description</th>
                                                <!--<th style="width:20%;">Has Form</th>-->
                                                <th style="width:10%;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $l = 1;
                                            foreach ($suppText as $val) {
                                                ?>
                                                <tr>
                                                    <td class="v-align-middle semi-bold">
                                                        <?= $l++ ?>
                                                    </td>
                                                    <td><?= implode(", ", $val['name']) ?></td>
                                                    <td><?php
                                                        $elan = array();
                                                        $elang = array('English');
                                                        foreach($val['name'] as $ind => $val1)
                                                            array_push($elan, $ind);
                                                        foreach ($language as $val2) 
                                                            if(array_search($val2['lan_id'], $elan) != false)
                                                                array_push($elang,$val2['lan_name']);
                                                        echo implode(", ", $elang);
                                                    ?></td>
                                                    <td>
                                                        <?php
                                                        if ($val['has_scat'])
                                                            echo "<button class='btn btn-default' onclick='viewsub_cat(" . $val['cat_id'] . ")'>
                                                                        <i class='fa fa-eye'></i> Sub Categories
                                                                    </button>";
                                                        else
                                                            echo implode(", ", $val['desc']);
                                                        ?>
                                                    </td>
                                                    <!--<td><?= ($val['cat_hform'] == "1") ? "Yes" : "No" ?></td>-->
                                                    <td class="v-align-middle">
                                                        <?php
                                                        if ($val['has_scat'])
                                                            echo "<a href='" . base_url() . "index.php/utilities/support_cat/0/" . $val['cat_id'] . "' class='btn btn-default cls110' id='add_new_cat'>
                                                                        <i class='fa fa-plus'></i> Add Sub Category
                                                                    </a><br/>";
                                                        ?>
                                                        <a class='btn btn-default cls111' href='<?= base_url()?>index.php/utilities/support_edit/<?= (String)$val['_id']?>' data-id='<?= $val['cat_id'] ?>'>
                                                            <i class="fa fa-edit"></i> Edit
                                                        </a>
                                                        <a class='btn btn-default cls111' onclick='delcat(this)' data-id='<?= $val['cat_id'] ?>'>
                                                            <i class="fa fa-trash"></i> Delete
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<div class="modal fade slide-up" id="modal-subcat" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id='grp_form' action=''>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style='margin: 10px;'>
                    <i class="pg-close"></i>
                </button>
                <div class="modal-header">
                    <h4 class="modal-title">Sub Categories</h4>
                </div>
                <div class="modal-body m-t-50">
                    <div class="form-group-attached">
                        <div class="row">
                            <table class="table table-hover demo-table-search" id="subcat_table">
                                <thead>
                                    <tr>
                                        <th style="width:3%;">SL No.</th>
                                        <th style="width:12%;">Sub-Category</th>
                                        <th style="width:20%;">Description</th>
                                        <th style="width:10%;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>