<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined("BASEPATH"))
    exit("Direct access to this page is not allowed");

if (!defined('APP_NAME')) {
    define("APP_NAME","iServe");
}
/*
 * Navigation items
 */
define("LIST_COMPANY_MOBIFY","Only numbers are allowed");


define("SELECT_BY_DRIVER","Select by provider");

define("NAV_DASHBOARD","DASHBOARD");
define("SELECT_PROFESSION","SELECT PROFESSION");
define("SELECT_CAT","SELECT CATEGORY");
define("POPUP_DOCTOR_PROFESSION","Please select the profession");
define("POPUP_DOCTOR_CITY","Please select the city");
define("SELECT_CITY","City");
define("LIST_MAP","PROVIDER ROOT MAP");
define("NAV_START_PAGE","START PAGE");
define("NAV_CITIES","CITIES");
define("NAV_COMPANYS","SERVICE CATEGORIES");
define("NAV_SERVICES","SERVICES");
define("NAV_SERVICES_GROUP","SERVICES GROUP");
define("NAV_SUB_CATS","SERVICE SUB CATEGORIES");
define("NAV_VEHICLETYPES","VEHICLE TYPES");
define("NAV_VEHICLES","VEHICLES");
define("NAV_DRIVERS","PROVIDERS");
define("NAV_DRIVERS_ACCEPTENCE","PROVIDER ACCEPT RATE");
define("NAV_PASSENGERS","CUSTOMERS");
define("NAV_BOOKINGS","LIVE BOOKINGS FEED");
define("NAV_DISPATCHER","DISPATCHER");
define("NAV_CANCLEDBOOKINGS","CANCLED BOOKINGS");
define("NAV_DISPATCHERS","DISPATCHERS");
define("NAV_PAYROLL","PAYROLL");
define("NAV_COMMISSION","COMMISSION");
define("NAV_WALLLET","PRO WALLLET");
define("NAV_ACCOUNTING","ACCOUNTING");
define("NAV_DOCUMENT","DOCUMENTS");
define("NAV_DRIVERREVIEW","PRO REVIEWS");
define("NAV_PASSENGERRATING","CUSTOMER RATINGS");
define("NAV_DISPUTES","DISPUTES");
define("NAV_VEHICLEMODELS","VEHICLE MODELS");
define("NAV_FINANCE","MARKETING");
define("NAV_DELETE","DELETE");
define("NAV_GODSVIEW","GODSVIEW");
define("NAV_COMPAIGNS","CAMPAIGNS");
define("LIST_DRIVER_EDIT","EDIT PROVIDER");

/*
 * headings
 */
define("HEAD_DASHBOARD","Dashboard");
define("HEAD_CITIES","Cities");
define("HEAD_COMPANYS","Company's");
define("HEAD_VEHICLETYPE","Vehicle type");
define("HEAD_VEHICLES","Vehicles");
define("HEAD_DRIVERS","Providers");
define("HEAD_PASSENGERS","Passengers");
define("HEAD_BOOKINGS","Bookings");
define("HEAD_CANCLEDBOOKINGS","Cancelled bookings");
define("HEAD_DISPATCHERS","Dispatchers");
define("HEAD_PAYROLL","Payroll");
define("HEAD_ACCOUNTING","Accounting");

define("HEAD_DOCUMENT","Document");
define("HEAD_DRIVERREVIEW","Provider review");
define("HEAD_PASSENGERRATING","Passenger rating");
define("HEAD_DISPUTES","Disputes");
define("HEAD_VEHICLEMODEL","Vehicle model");
define("HEAD_COMPAIGNS","Campaigns");
define("HEAD_GODSVIEW","GodzView");
define("HEAD_FINANCE","Finance");
define("HEAD_DELETE","Delete");

define("SELECT_COUNTRY_ANDCITY","Select city and country");
define("ADD_SERVICE_TEXT","Add service");
define("ADD_SERVICE_GROUP_TEXT","Add service group");
define("EDIT_CATEGORY_TEXT","Edit category");
define("EDIT_SERVICE_TEXT","Edit service");
define("ADD_CATEGORY_TEXT","Accept category");
define("LIST_CATEGORY_TEXT","List of category");
/*
 * LIST ELEMENTS
 */
define("LIST_RESETPASSWORD_HEAD","Change password");
define("FIELD_COMPAIGNS_TITLE","Title");


//define("VEHICLE_TYPE","New");

define("LIST_SERVICE_GROUP","SERVICE GROUP");
define("LIST_COMMISSION","COMMISSION");
define("LIST_REFFERED_PROMOS","REFERRED PROMOS");
define("LIST_OF_PROMOTION_ANALYTICS","PROMO ANALYTICS");
define("LIST_OF_REFERRAL_ANALYTICS","REFERRAL ANALYTICS");
define("LIST_PRAMOTION_HEAD","Add Promotion");
define("POPUP_COMPAIGN_ONETOEDIT","You required to select atleast a campaign to edit");
define("POPUP_COMPAIGN_ONLYONE","You are allowed to select only one campaign to edit");


define("LIST_NEW","New");
define("LIST_ACCEPTED","Accepted");
define("LIST_SUSPENDED","Suspended");

define("LIST_ACCEPT","ACCEPT");
define("LIST_REJECT","REJECT");
define("LIST_ASSIGNED","ASSIGNED");

define("LIST_NEW_SERVICE_CAT","SERVICE CATEGORIES");
define("LIST_ACCEPTED_PRODUCTS","SERVICES");

define("LIST_UNDERREVIEW","Under review");
define("LIST_ACCEPTEDFREE","Accepted & free");
define("LIST_REJECTED","Rejected");
define("LIST_ACTIVE","Active");
define("LIST_INACTIVE","Inactive");



define("LIST_FREE","Free");
define("LIST_FREEONLINE","Online");
define("LIST_OFFLINE","Offline");
define("LIST_BOOKED","Booked");


define("LIST_DRIVERSLICENSE","Provider's licence");
define("LIST_BANKPASSBOOK","Bank passbook");
define("LIST_CARRIAGEPERMIT","Carriage permit");
define("LIST_CR","Certificate of registration");
define("LIST_INSURENCECERTIFICATE","Insurance certificate");

define("LIST_PASSENGERRATING","Customer rating");

define("LIST_REPORTED","Reported");

define("LIST_VEHICLEMAKE","Vehicle make");
define("LIST_VEHICLEMODELS","Vehicle models");

define("LIST_REFERRALS","Referrals");
define("LIST_PROMOTIONS","Promotions");

define("LIST_CITIES","Cities");
define("LIST_OF_CITIES","List of cities");
define("LIST_OF_SUB_CAT","List of services");
define("LIST_OF_CAT","List of category");
define("LIST_SERVICES","SERVICES");
define("LIST_SUB_CATEGORY","SERVICES");
define("LIST_ADDCITIES","Add City");
define("LIST_ADD_COUNTRY_DETAILS","Add country details");
define("LIST_ADD_CITY_DETAILS","Add city details");

define("LIST_COMPANY","COMPANY");
define("LIST_COMPAIGNS","CAMPAIGNS");
define("LIST_ADD_COMPANY_DETAILS","Add company details");
define("LIST_EDIT_COMPANY_DETAILS","Edit company details");

define("LIST_VEHICLETYPE","Vehicle Type");
define("LIST_ADD_VEHICLETYPE_DETAILS","Please mention the vehicle type");
define("LIST_EDIT_VEHICLETYPE_DETAILS","Please edit the vehicle details");

define("LIST_VEHICLE","Vehicle");
define("LIST_VEHICLE_ADD","Add");
define("LIST_VEHICLE_VEHICLESETUP","Vehicle setup");
define("LIST_VEHICLE_DETAILS","Details");
define("LIST_VEHICLE_DOCUMETS","Documents");

define("LIST_DRIVER","PROVIDER");
define("LIST_DRIVER_ADDNEW","Add");
define("LIST_DRIVER_PESIONALDETAILS","PERSONAL DETAILS");
define("LIST_DRIVER_LOGINDETAILS","LOGIN DETAILS");
define("LIST_DRIVER_DRIVINGLICENCE","DRIVING LICENSE");

define("FIELD_PROFISSION_COUNTRY","SELECT PROFESSION");
define("POPUP_PASSENGERS_ACTIVATE_DOCTORS","Are you sure you wish to activate this provider?");
define("POPUP_COMPANY_DEACTIVATED_DOCTORS","Your selected provider/providers have been de-activated successfully");

define("LIST_DISPATCHERS_ADDMODEL","Select Country And City");
define("LIST_DISPATCHERS_EDITMODEL","Select City");

define("LIST_PASSENGERRATING_EDITMODEL","PASSENGER RATING");

define("LIST_DISPUTES_REPORTED","REPORTED");
define("LIST_DISPUTES_RESOLVED","RESOLVED");
define("LIST_DISPUTES_EDITDISPUTE","Edit dispute");


define("LIST_DISPUTES","ADD VEHICLE TYPE");

define("LIST_REFFERAL_HEAD","Add referral");

define("VEHICLEMAKE_ADDVEHICLE","Add vehicle make");
define("VEHICLEMODEL_ADDVEHICLE","Add vehicle model");
define("SELECT_COMPANY","Select Company");




/*
 * BUTTONS
 */
define("COMPAIGNS_DISPLAY","Are you sure you wish to deactivate this campaign?");
define("VEHICLEMODEL_DELETE","Are your sure you wish to delete the service group?");
define("BUTTON_YES","Yes");
define("BUTTON_NO","No");
define("BUTTON_OK","OK");
define("CITY_DELETE","Are you sure you wish to delete this city?");
define("CAT_DELETE","Are you sure you wish to delete this category?");
define("SUB_CAT_DELETE","Are you sure you wish to delete this service?");

define("SEARCH","Search");

define("SELECT","Select");

define("LIST_RESETPASSWORD_DRIVERDOCUMENTS","Provider Documents");
//define("LIST_RESETPASSWORD_VEHICLEDOCUMENTS","Vehicle Documents");
define("VEHICLEDOCUMENTS","Vehicle Documents");
define("DRIVERS_TABLE_DRIVER_DOCUMENT","DOCUMENT TYPE");
define("DRIVERS_TABLE_DRIVER_EXPIREDATE","EXPIRY DATE");
define("DRIVERS_TABLE_DRIVER_VIEW","VIEW/DOWNLOAD");


define("BUTTON_SAVE","Save");
define("BUTTON_ADD","Add");
define("BUTTON_DOCUMENT","Document");
define("BUTTON_ADD_NEW","Configure & activate city");
define("BUTTON_ADD_SUB_CATEGORY","ADD SERVICE");
define("BUTTON_ADD_SERVICE","ADD SERVICE");
define("BUTTON_ADD_CATEGORY","ADD CATEGORY");
define("POPUP_VEHICLE_MAKE","Are you sure do you wish to delete vehicle make?");
define("BUTTON_ACTIVATE","Activate");
define("BUTTON_ACCEPT","Accept");
define("BUTTON_ACTIVE","Active");
define("BUTTON_REJECT","Reject");
define("BUTTON_DEACTIVATE","Deactivate");
define("BUTTON_INACTIVE","Inactivate");
define("BUTTON_DEACTIVE","Deactive");
define("BUTTON_EDIT","Edit");
define("BUTTON_SUSPEND","Suspend");
define("BUTTON_DELETE","Delete");
define("BUTTON_JOBLOGS","Job logs");
define("BUTTON_CITIES","Create country or city");

define("BUTTON_RESOLVE","Resolve");

define("BUTTON_RESETPASSWORD","Reset password");

define("BUTTON_ADDCOUNTRY","Add country");
define("BUTTON_ADDCITY","Add city");
define("BUTTON_CANCEL","Cancel");
define("BUTTON_CLEAR","Clear");

define("BUTTON_ADD_COMPANY","Add company");
define("BUTTON_EDIT_COMPANY","Edit company");
define("BUTTON_CHANGES_COMPANY","Save changes");

define("BUTTON_ADD_VEHICLETYPE","Add vehicle type");

define("BUTTON_EDIT_VEHICLETYPE","Edit vehicle type");

define("BUTTON_NEXT","Next");
define("BUTTON_PREVIOUS","Previous");
define("BUTTON_FINISH","Finish");

define("BUTTON_SUBMIT","Submit");
define("BUTTON_AUTOMATIC","Automatic");
define("BUTTON_MANUAL","Manual");
define("LIST_ADD_DISPATCHER","Add dispatcher");

/*
 * Popups
 */
define("POPUP_RESETPASSWORD_ANYONE","Please select the profile you wish to reset the password for.");

define("POPUP_SELECT_CATEGORY","Please enter the category name");
define("POPUP_SELECT_CATEGORY_DESC","Please enter the category description");
define("POPUP_SELECT_TYPE","Please select the type");

define("POPUP_BANNER_IMAGE","Banner Image");
define("POPUP_SEL_CAT_IMAGE","Selected state icon");
define("POPUP_UNSEL_CAT_IMAGE","Unselected state icon");

define("POPUP_SELECT_COUNTRY","Please select the country you wish to activate your service");
define("POPUP_SELECT_CITY","Please select the city you wish to activate your service");

define("POPUP_SELECT_PRICE_PLAN","Please select the pricing plan");
define("POPUP_SELECT_COMMISSION_PLAN","Please select the commission plan");

define("POPUP_SELECT_SEL_IMG","Please select selected state image");
define("POPUP_SELECT_UNSEL_IMG","Please select unselected state image");
define("POPUP_SELECT_SUB_CATEGORY","Please enter the service name");
define("POPUP_SELECT_SUB_CATEGORY_DESC","Please enter the service description");
define("POPUP_SELECT_MIN_FEES","Please enter the minimum fees applicable");
define("POPUP_SELECT_BASE_FEES","Please enter the base fees applicable");
define("POPUP_SELECT_PRICE_PER_MILE","Please enter the price per mile");
define("POPUP_SELECT_PRICE_PER_MIN","Please enter the price per minute");
define("POPUP_SELECT_VISIT_FEES","Please mention visiting charges");
define("POPUP_SELECT_FEES_TYPE","Please select the fee type");
define("POPUP_SELECT_CAN_FEES","Please enter the cancellation fees applicable");
define("POPUP_SELECT_FIXED_PRICE","Please enter the fixed price");

define("POPUP_DRIVER_ALERTTAB","The personal details of the provider is incomplete.");
define("POPUP_DRIVER_ALERTLOGINTAB","The login details are incomplete.");
define("POPUP_DRIVER_ALERTLOGINTABDRIVING","Please submit your license details.");

define("POPUP_CITIES_ENTER_COUNTRY_NAME","Please enter the country name");
define("POPUP_CITIES_COUNTRY_ADDED","Country added successfully");

define("POPUP_CITIES_CITY_ENTER","Please enter the city name you wish to activate your service");
define("POPUP_CITIES_CITY_ENTER_COUNTRY","Please select the country you wish to activate your service");
define("POPUP_CITIES_CITY_ENTERALPHA","Please enter the city name as alphabets only");
define("POPUP_CITIES_CITY_CURENCY"," Enter the currency with 3 characters only");
define("POPUP_CITIES_CITY_ADDED","City added successfully");
define("POPUP_CITIES_CITY_EXIST","City already exist");
define("POPUP_LAT_LONG_UPDATED","Your selected city details has been updated successfully");
define("POPUP_CITYTT_UPDATED","Your city updated successfully");
define("POPUP_LAT_LONG_ADDED","Your city added successfully");
define("POPUP_LAT_LONG_DELETED","Your city deleted successfully");
define("POPUP_COUNTRY_ADDED","Your country added successfully");
define("POPUP_CITY_ADDED","Your city added successfully");
define("POPUP_LAT_LONG_UPDATE_FAILED","Your lat long update failed");
define("POPUP_ALPHABET","Please enter alphabet");


define("POPUP_ENTER","Please enter the personal details which are mandatory");
define("POPUP_COMPANY_NAME","Please enter the company name");
define("POPUP_COMPANY_NAMEVALID","Enter the company name as text/number or both but not a special characters");
define("POPUP_COMPANY_PASSWORD","Enter the password");
define("POPUP_COMPANY_EMAIL","Please enter your email number");


define("POPUP_DRIVER_FIRSTNAME","Please enter the provider name");
define("POPUP_DRIVER_LASTNAME","Please enter the last name");
define("POPUP_DRIVER_MOBILE","Please enter The Mobile Number");
define("POPUP_DRIVER_DRIVERPHOTO","Please upload a provider photo");
define("POPUP_DRIVER_VEHICLEPHOTO","Please upload a vehicle photo");

define("POPUP_DRIVER_DRIVER_EMAIL","Please enter the email");
define("POPUP_DRIVER_DRIVER_YOUREMAIL","Please enter a valid email");
define("POPUP_DRIVER_DRIVER_ALLOCATED","Email id is already exist !");
define("POPUP_DRIVER_DRIVER_PASSWORD","Please enter the password");
define("POPUP_DRIVER_DRIVER_PASSWORD_VALID","The password  length must be 6 and at least one number followed by character only");
define("POPUP_DRIVER_DRIVER_ZIPCODE","Please enter the zip code");


define("POPUP_ADDCOMPANY_NAME","Please select the company");
define("POPUP_ADDCITY__NAME","Please select  the city");
define("POPUP_SELECT_VEHICLEID","Please enter  the vehicle id");
define("POPUP_SELECT_VEHICLEMAKE","Please select  the vehicle  make");
define("POPUP_SELECT_VEHICLEMODAL","Please select  the vehicle  modal");
define("POPUP_SELECT_VEHICLEREGNO","Please enter the vehicle registration number");
define("POPUP_SELECT_VEHICLEPLATENO","Please enter the vehicle plate number");
define("POPUP_SELECT_VINSURENCENUMBER","Please enter the vehicle  insurance number");
define("POPUP_SELECT_VEHICLECOLOR","Please enter the vehicle colour");
define("POPUP_SELECT_VEHICLEIMAGE","Please select  vehicle image");
define("POPUP_SELECT_VEHICLEUPLOADREGNO","Please upload the registration certificate");
define("POPUP_SELECT_VEHICLE_DATE","Please select the expire date");
define("POPUP_SELECT_VINSURENCENUMBER_INSURANCE","Please upload  the  motor insurance certificate");
define("POPUP_SELECT_VEHICLECOLOR_CARRIAGE_PERMIT","Please upload the carriage permit certificate");
define("POPUP_COMPANY_EMAILVALID","Please enter a valid email");
define("POPUP_COMPANY_ADDRESS","Enter the address");
define("POPUP_COMPANY_ADDED_D"," Great! Company has been added successfully and is ready for admin approval");
define("POPUP_COMPANY_EXIST","Company already register with this email id");
define("POPUP_COMPANY_EDITED_D","Are you sure you wish to save these changes,  changes once made cannot be recovered?");
define("POPUP_COMPANY_SELECT","Select the city");

define("POPUP_COMPANY_SELECT_FIRSTNAME","Please enter the first name");
define("POPUP_COMPANY_SELECT_LASTNAME","Please enter the last name");
define("POPUP_COMPANY_SELECT_MOBILE","Please enter the mobile number");


define("POPUP_COMPANY_VATNUMBER","Enter the vat number");
define("POPUP_COMPANY_PPCODE","Enter valid pin code number");
define("POPUP_COMPANY_VATNUMBERNUM","Enter the valid vat number");
define("POPUP_COMPANY_DELETEVEHICLE","Please select vehicle type to delete");
define("POPUP_COMPANY_DELETECOMPANY","Please select company to delete");
define("POPUP_COMPANY_DELETECOUNTRY","Please select country to delete");
define("POPUP_COMPANY_DELETECITY","Please select city to delete");
define("POPUP_COMPANY_DELETEDRIVER","Please select provider to delete");
define("POPUP_COMPANY_LOGOUTDRIVER","Please select provider to logout");

define("POPUP_COMPANY_SURELOGOUTDRIVER","Are you sure do you want to logout this provider");



define("POPUP_VEHICLE_TAB_D","Complete vehicle setup tab properly");
define("POPUP_VEHICLE_DETAILTAB_D","Complete details tab properly");


define("POPUP_DRIVER_FILE_POPUP_DRIVING","Please upload the driving licence file");
define("POPUP_DRIVER_FILE_POPUP_EXPERIENCE","Please choose the expiredate of driving licence");
define("POPUP_DRIVER_FILE_POPUP_BANKPASSBOOK","Please upload the bank passbook file");





define("POPUP_COMPANY_ATLEASTONENAME","Please select  at least one company name");
define("POPUP_ACCEPTED","Are you sure you wish to activate this company and have validated all Of Its Documents, etc.?");
define("POPUP_REJECTED","Are you sure you wish to reject this company and have validated all of its documents, etc?");
define("POPUP_SUSPENDED","Are you sure you wish to suspend this company and have validated all of Its documents, etc.?");
define("POPUP_DELETE","Are you sure you wish to delete this Company and have validated all of its documents, etc.?");

define('EDIT_PRO_COMMISSION',"EDIT PROVIDER COMMISSION");
define('EDIT_SERVICE_GROUP',"EDIT SERVICE GROUP");
define("POPUP_DELETE_COMMISSION","Please select any one to delete?");
define("POPUP_ONE_BUSINESSEDIT","Please select only one provider to edit the commission");
define("POPUP_ONE_EDIT_GROUP","Please select only one service group to edit");

define("POPUP_DELETEVEHICLETYPE","Are you sure do you want to delete this vehicle type?");
define("POPUP_DELETECOMPANY","Are you sure do you want to delete this company?");
define("POPUP_DELETECOUNTRY","Are you sure do you want to delete this country?");
define("POPUP_DELETECITY","Are you sure do you want to delete this city?");
define("POPUP_DELETEDRIVER","Are you sure do you want to delete this provider??");
define("POPUP_DELETEDVEHICLEMODAL","Are you sure do you want to delete this vehicle model?");
define("POPUP_DELETEVEHICLEMODAL","Please select vehicle model to delete. ");

define("POPUP_COMPANY_DELETE_DELETE_VEHICLETYPE","Selected vehicle type has been deleted successfully");
define("POPUP_COMPANY_DELETE_DELETE_COMPANY","Selected company has been deleted successfully");
define("POPUP_COMPANY_DELETE_DELETE_COUNTRY","Selected country has been deleted successfully");
define("POPUP_COMPANY_DELETE_DELETE_CITY","Selected city has been deleted successfully");
define("POPUP_COMPANY_DELETE_DELETE_DRIVERDELETE","Selected provider has been deleted successfully");
define("POPUP_COMPANY_DELETE_DELETE_MODELDELETE","Selected model has been deleted sucessfully");





define("POPUP_COMPANY_FIRST_NAME","Please enter first name");
define("POPUP_COMPANY_LAST_NAME","Please enter last name");
define("POPUP_COMPANY_CTATE_NAME","Please enter state name");
define("POPUP_COMPANY_PINCODE_NAME","Please enter pin code");
define("POPUP_COMPANY_ADDED","Your company updated successfully");

define("POPUP_CANCEL","Are you sure to cancel the data");

define("POPUP_COMPANY_MOBILE","Please enter the mobile number");

define("POPUP_CONFIRM","Are you sure you wish to delete companies");
define("POPUP_VEHICLETYPE","Are you sure you wish to delete vehicle model");
define("POPUP_VEHICLES","Are you sure you wish to delete vehicles");
define("POPUP_DRIVERS","Are you sure you wish too delete providers");
define("POPUP_PASSENGERS","Are you sure you wish to inactive customers");
define("POPUP_DISPATCHERS","Are you sure you wish to inactive dispatchers");
define("POPUP_DRIVERREVIEW_INACTIVE","Are you sure you wish to inactive provider reviews");
define("POPUP_DRIVERREVIEW_ACTIVE","Are you sure you wish to active provider reviews");
define("POPUP_VEHICLEMODEL","Are you sure you wish to delete vehicle types");

define("POPUP_COMPANY_ATLEAST","Please select at least one company");
define("POPUP_CITY_ATLEAST","Please select at least one city");
define("POPUP_SUB_CAT_ATLEAST","Please select at least one Service");
define("POPUP_CAT_ATLEAST","Please select at least one category");

define("POPUP_VEHICLETYPE_ATLEAST","Please select at least one vehicle type");

define("POPUP_COMPANY_ACTIVATED","Your selected company/companies activated successfully");
define("POPUP_COMPANY_DEACTIVATED","Your selected company/companies deactivated successfully");
define("POPUP_COMPANY_SUSPENDED","Your selected company/companies suspended successfully");
define("POPUP_COMPANY_DELETED","Your selected company/companies deleted Successfully");

define("POPUP_COMPANY_ANYONE","Please select any one company");
define("POPUP_COMPANY_ONLYONE","Please select only one company to edit");

define("POPUP_CAT_ANYONE","Please select any one category");
define("POPUP_CAT_ONLYONE","Please select only one category to edit");

define("POPUP_SUB_CAT_ONLYONE","Please select only one Service to edit");
define("POPUP_SUB_CAT_ANYONE","Please select any one Service");
define("POPUP_CITY_ANYONE","Please select any one city");
define("POPUP_CITY_ONLYONE","Please select only one city to edit");
define("POPUP_CITY_LAT","Please enter the latitude");
define("POPUP_CITY_LATVAL","Please enter valid data at latitude");
define("POPUP_CITY_LOT","Please enter the longitude");
define("POPUP_CITY_LONVAL","Please enter valid data at longitude");


define("POPUP_VEHICLETYPE_ADDED","Your vehicle type added successfully");
define("POPUP_VEHICLETYPE_DELETED","Vehicle type deleted successfully");
define("POPUP_VEHICLETYPE_UPDATED","Your vehicle type updated successfully");

define("POPUP_VEHICLETYPE_ENTER","Please enter the vehicle type");
define("POPUP_VEHICLETYPE_ENTERTEXT","Please enter the vehicle type as text");
define("POPUP_VEHICLETYPE_NOSEATINGS","Please enter seating capacity");
define("POPUP_VEHICLETYPE_NUMSEATINGS","Please enter the seating as number");
define("POPUP_VEHICLETYPE_MINMUM","Please enter the minimum fare");
define("POPUP_VEHICLETYPE_MINMUM_NUMBER","Please enter the minimum as number only");
define("POPUP_VEHICLETYPE_BASEFARE","Enter the base fare");
define("POPUP_VEHICLETYPE_BASEFARENUM","Enter the base fare as number only");
define("POPUP_VEHICLETYPE_MINUTE","Enter the cost per minute");
define("POPUP_VEHICLETYPE_NUMMINUTE","Enter the price per minute as number only");
define("POPUP_VEHICLETYPE_KM","Enter the price per km/mile");

define("POPUP_VEHICLETYPE_ANYONE","Please select any one vehicle type");
define("POPUP_VEHICLETYPE_ONLYONE","Please select only one vehicle type to edit");
define("POPUP_VEHICLETYPE_ATLEASTONE","Please select at least one vehicle type.");
define("POPUP_VEHICLETYPE_SUREDELETE","Are you sure do you want to delete vehicle type?");



define("POPUP_VEHICLETYPE_KMNUM","Enter the cost per kilometer as number only");
define("POPUP_VEHICLETYPE_SELECTCITY","Please select the City");

define("POPUP_VEHICLES_ATLEAST","Please mark any one option");

define("POPUP_PASSENGERS_ATLEAST","Select at least one customer");
define("POPUP_DRIVERS_ATLEAST","Select at least one provider");
define("POPUP_DRIVERS_ONLY","Select only one Provider to edit");
define("POPUP_PASSENGERS_DEACTIVATE","Are you sure do you want to deactivate provider reviews");
define("POPUP_PASSENGERS_ACTIVATE","Are you sure do you want to activate provider reviews");



define("POPUP_MSG_ACCEPTED","Great! Your company accepted successfully");

define("POPUP_DRIVERS_ACTIVAT","Are you sure do you want to activate providers");
define("POPUP_DRIVERS_DELETE","Are you sure do you want to delete provider/providers");
define("POPUP_DRIVERS_DEACTIVAT","Are you sure do you want to deactivate providers");
define("POPUP_DRIVERS_DEACTIVAT_DATEOFBOOKING","Please select start date and end date");
define("POPUP_DRIVERS_NEWPASSWORD","Your new password updated successfully");
define("POPUP_DRIVERS_NEWPASSWORD_FAILED","Your new password updation failed. Try again once");
define("POPUP_DRIVERS_ERRPASSWORD","Your password already exist! Enter new password");
define("POPUP_DRIVERS_ERRCURRENTPASSWORD","You have entered the incorrect current password!,  Enter the correct currentpassword.");


define("POPUP_PASSENGERS_DELETE","Are you sure you wish to delete customer/customers");
define("POPUP_PASSENGERS_DEACTIVAT","Are you sure you wish to deactivate customers");
define("POPUP_PASSENGERS_ACTIVAT","Are you sure you wish to activate customers");

define("POPUP_PASSENGERS_ANYONEPASS","Please select any one to reset the password");
define("POPUP_PASSENGERS_ONLYONEPASS","Please select only one to reset the password");

define("POPUP_PASSENGERS_PASSVALID","Password  length must be 6 characters and at least one number followed by character only");
define("POPUP_PASSENGERS_PASSCONFIRM","Please confirm the password");
define("POPUP_PASSENGERS_PASSENTER","Please enter the password");
define("POPUP_PASSENGERS_SAMEPASSCONFIRM","Please confirm the same password");
define("POPUP_PASSENGERS_PASSNEW","Please enter the new password");
define("POPUP_PASSENGERS_CURRENTPASSWORD","Please enter the current password");

define("POPUP_PASSENGERS_PASSUPDATED","Password updated successfully");

define("POPUP_DISPATCHERS_CITY","Please select city");

define("POPUP_DISPATCHERS_NAME","Please enter the name");
define("POPUP_DISPATCHERS_NAMETEXT","Please enter the name as text");
define("POPUP_DISPATCHERS_EMAIL","Please enter the email");
define("POPUP_DISPATCHERS_VALIDEMAIL","Please enter valid email");
define("POPUP_DISPATCHERS_PASSWORD","Please enter the password");

define("POPUP_DISPATCHER_DELETE","Please select at least one dispatcher");
define("POPUP_DISPATCHER_EDIT","Please select any one dispatcher");
define("POPUP_DRIVER_EDIT","Please select any one provider");
define("POPUP_VEHICLE_DOCUMENT","Please select any one vehicle");
define("POPUP_DISPATCHER_ONLYEDIT","Please select only one dispatcher to edit");
define("POPUP_DRIVER_ONLYEDIT","Please select only one provider to edit");
define("POPUP_DRIVER_ONLYEDIT_DOCUMENT","Please select only one vehicle to view documents");
define("POPUP_DISPATCHER_INACTIVE"," Select at least one dispatcher");
define("POPUP_DISPATCHER_SUREINACTIVE"," Are you sure you wish to deactivate dispatchers");
define("POPUP_DISPATCHER_SUREACTIVE","Are you sure you wish to activate dispatchers");


define("POPUP_DISPATCHER_SUCCESS","Your dispatcher added successfully");
define("POPUP_DISPATCHER_NOTSUCCESS","Your dispatcher not added!, Please tyr again");

define("POPUP_DRIVERREVIEW_ATLEAST","Please select atleast one Provider review");

define("POPUP_VEHICLE_ATLEAST","Please select one vehicle to edit");
define("POPUP_VEHICLE_ONE","Please select only one vehicle to edit");
define("POPUP_VEHICLE_DELETE","Are you sure you wish to delete these vehicles");
define("POPUP_VEHICLE_DEACTIVATE","Are you sure you wish to reject these vehicle/vehicles");
define("POPUP_VEHICLE_ACTIVATE","Are you sure you wish to activate these vehicle/vehicles");


define("POPUP_MESSAGE","Please enter the message");

define("POPUP_DISPUTES_ANYONE","Please select any one to resolve");
define("POPUP_DISPUTES_ONLYONE","Please select only one to resolve");
define("POPUP_COMPAIGN_ATLEAST","Select atleast one campaign");

define("POPUP_COMPAIGN_AREYOUSURE","Are you sure to deactivate");
define("POPUP_COMPAIGN_REFERRAL","Referral");



define("POPUP_VEHICLEMODEL_TYPENAMES","Please enter the type name");
define("POPUP_VEHICLEMODEL_TEXT","Please enter the type name as text");
define("POPUP_MESSAGE_COMPLETED","Your type name added successfully");
define("POPUP_VEHICLEMODEL_ATLEAST","Select atleast one vehicle type");

define("POPUP_VEHICLEMODEL_TYPENAME","Please select the vehicle type");
define("POPUP_VEHICLEMODEL_MODELNAME","Please enter the model  name");
define("POPUP_VEHICLEMODEL_SUCCESSES","Your  model  name added successfully");


define("POPUP_VEHICLEMODEL_CODE","Please enter the Code");
define("POPUP_VEHICLEMODEL_STARTDATE","Please select the start Date");
define("POPUP_VEHICLEMODEL_EXPIREDATE","Please select the expire Date");

define("POPUP_COMPAIGNS_DISCOUNT","Please enter the new user discount");
define("POPUP_COMPAIGNS_PROMOTION","Please enter the promotion discount");
define("POPUP_COMPAIGNS_TITLE","Please enter the title");
define("POPUP_COMPAIGNS_NUMBERS","Please enter the numbers only");
define("POPUP_COMPAIGNS_REFERALDISCOUNT","Please enter the referral bonus");
define("POPUP_COMPAIGNS_MESSAGE","Please enter the message");
define("POPUP_COMPAIGNS_TEXT","Please enter message or number Only");
define("POPUP_COMPAIGNS_TEXT_PROMOTIONS_S","Great! your promotion has been added sucessfully for this city");

define("POPUP_COMPAIGNS_CODE","Please enter the code");
define("POPUP_COMPAIGNS_TEXTNUMBER","Please enter code as text or number");
define("POPUP_COMPAIGNS_STARTDATE","Please select  the start date");
define("POPUP_COMPAIGNS_EXPIREDATE","Please select  the expire date");



/*
 * Fields
 */

define("FIELD_ENTER_COUNTRY_NAME","ENTER COUNTRY NAME");


define("FIELD_CITIES_COUNTRY","Country");
define("FIELD_CITIES_CITYNAME_NAME","City name");
define("FIELD_CITIES_CURRENCY_NAME","ENTER CURRENCY NAME");

define("FIELD_COMPANY_COMPANYNAME","COMPANY NAME");
define("FIELD_COMPANY_FIRSTNAME","FIRST NAME");
define("FIELD_COMPANY_LASTNAME","LAST NAME");
define("FIELD_COMPANY_USERNAME","USER NAME");
define("FIELD_COMPANY_PASSWORD","PASSWORD");
define("FIELD_COMPANY_EMAIL","EMAIL");
define("FIELD_COMPANY_ADDRESS","ADDRESS");
define("FIELD_COMPANY_MOBILE","MOBILE");
define("FIELD_COMPANY_CITY","City");
define("FIELD_COMPANY_STATE","STATE");
define("FIELD_COMPANY_POSTCODE","POST CODE");
define("FIELD_COMPANY_VATNUMBER","VAT NUMBER");
define("FIELD_COMPANY_COMPANYLOGO","COMPANY LOGO");

define("FIELD_COMPANY_SELECTCOUNTRY","Country");

define("FIELD_VEHICLETYPE_NAME","VEHICLE TYPE NAME");
define("FIELD_VEHICLETYPE_SEATINGCAPACITY","SEATING CAPACITY");
define("FIELD_VEHICLETYPE_MINIMUMFARE","MINIMUM FARE");
define("FIELD_VEHICLETYPE_BASEFARE","BASE FARE   (INR)");
define("FIELD_VEHICLETYPE_PRICEMINUTE","PRICE PER MINUTE ($)");
define("FIELD_VEHICLETYPE_PRICEKM","PRICE PER KM/MILE ($)");
define("FIELD_VEHICLETYPE_CANCILATIONFEE","CANCELATION FEE");
define("FIELD_VEHICLETYPE_DESCRIPTION","VEHICLE TYPE DESCRIPTION");
define("FIELD_VEHICLETYPE_CITY","City");

define("FIELD_VEHICLETYPE_LATITUDE","Latitude");
define("FIELD_VEHICLETYPE_LONGITUDE","Longitude");


define("FIELD_VEHICLE_SELECTCATEGORY","CATEGORY");
define("FIELD_VEHICLE_SELECTCITY","City");
define("FIELD_PAY_COM_BY_NAME","PAYMENT GATEWAY COMMISSION PAID BY");
define("FIELD_PRICE_SET_BY_NAME","PRICE SET BY");
define("FIELD_CATEGORY_NAME","CATEGORY NAME");
define("FIELD_SELECT_GROUP","GROUP");
define("FIELD_GROUP_NAME","GROUP NAME");
define("FIELD_SUB_CATEGORY_NAME","SERVICE NAME");
define("FIELD_SERVICE_NAME","SERVICE NAME");
define("FIELD_SERVICE","SERVICE");

define("FIELD_SERVICE_GROUP","GROUP NAME");
define("FIELD_GROUP_EDIT","EDIT GROUP");
define("FIELD_CATEGORY_DESC","CATEGORY DESCRIPTION");
define("FIELD_SERVICE_DESC","SERVICE DESCRIPTION");
define("FIELD_SUB_CATEGORY_DESC","SERVICE DESCRIPTION");
define("FIELD_SUB_MIN_FEES","MINIMUM FEES");
define("FIELD_SUB_BASE_FEES","BASE FEES");
define("FIELD_VISIT_FEES","VISIT FEES");
define("FIELD_PRICE_PER_MIN","PRICE PER MINUTE");
define("FIELD_FEE_TYPE","FEE TYPE");
define("FIELD_PRICE_PER_MILE","PRICE PER MILE");
define("FIELD_CAN_FEES","CANCELLATION FEES");
define("FIELD_FIXED_PRICE","FIXED PRICE");
define("FIELD_VEHICLE_SELECTCOMPANY","SELECT COMPANY");

define("FIELD_VEHICLE_VEHICLETYPE","SELECT VEHICLE TYPE");
define("FIELD_VEHICLE_VEHICLEMAKE","SELECT VEHICLE MAKE");
define("FIELD_VEHICLE_VEHICLEMODEL","SELECT VEHICLE MODEL");
define("FIELD_VEHICLE_IMAGE","UPLOAD PHOTO OF THE VEHICLE");

define("FIELD_VEHICLE_REGNO","VEHICLE REG.NO");
define("FIELD_VEHICLE_PLATENO","LICENSE PLATE NO");
define("FIELD_VEHICLE_INSURENCE","INSURANCE NUMBER");
define("FIELD_VEHICLE_COLOR","VEHICLE COLOR");

define("FIELD_VEHICLE_UPLOADCR","UPLOAD CERTIFICATE OF REGISTRATION");
define("FIELD_VEHICLE_EXPIREDATE","EXPIRATION DATE");
define("FIELD_VEHICLE_UPLOADMOTOR","UPLOAD MOTOR INSURANCE CERTIFICATE");
define("FIELD_VEHICLE_UPLOADCP","UPLOAD CONTRACT CARRIAGE PERMIT");
define("FIELD_VEHICLE_CHOOSE","CHOOSE FILE");
define("FIELD_VEHICLE_NOCHOOSE","NO FILE CHOOSEN");

define("FIELD_VEHICLE_VEHICLEID","VEHICLE ID");

define("FIELD_DRIVERS_FIRSTNAME","FIRST NAME");
define("FIELD_DRIVERS_LASTNAME","LAST NAME");
define("FIELD_DRIVERS_MOBILE","MOBILE");
define("FIELD_DRIVERS_UPLOADPHOTO","UPLOAD PHOTO");
define("FIELD_DRIVERS_EMAIL","EMAIL");
define("FIELD_DRIVERS_PASSWORD","PASSWORD");
define("FIELD_DRIVERS_ZIPCODE","ZIP CODE");
define("FIELD_DRIVERS_LICENCE_NUM","LICENCE NUMBER");
define("FIELD_DRIVERS_LICENCE_EXPIRY","LICENCE EXPIRY");
define("FIELD_DRIVERS_UPLOADDRIVERLICENSE","UPLOAD DRIVING LICENCE");
define("FIELD_DRIVERS_EXDATE","EXPIRY DATE");
define("FIELD_DRIVERS_UPLOADPASSBOOK","UPLOAD BANK PASSBOOK COPY");

define("FIELD_DISPUTES_MANAGEMENTNOTE","MANAGEMENT NOTE");

define("FIELD_SELECTCITY","City");

define("FIELD_DISPATCHERS_NAME","NAME");
define("FIELD_DISPATCHERS_EMAIL","EMAIL");
define("FIELD_DISPATCHERS_PASSWORD","PASSWORD");

define("FIELD_VEHICLEMODEL_TYPENAME","TYPE NAME");


define("FIELD_VEHICLEMODEL_SELECTTYPE","SELECT TYPE");
define("FIELD_VEHICLEMODEL_MODAL","MODEL");

define("FIELD_COMPAIGNS_PERCENTAGE","PERCENTAGE");
define("FIELD_COMPAIGNS_FIXED","FIXED");
define("FIELD_COMPAIGNS_DISCOUNT","NEW USER DISCOUNT");
define("FIELD_COMPAIGNS_REFERRALDISCOUNTTYPE","REFERRAL DISCOUNT TYPE");
define("FIELD_COMPAIGNS_REFERRALDISCOUNT","REFERRAL BONUS");
define("FIELD_COMPAIGNS_MESSAGE","MESSAGE");




define("FIELD_COMPAIGNS_CODE","CODE");
define("FIELD_COMPAIGNS_STARTDATE","START DATE");
define("FIELD_COMPAIGNS_EXPIREDATE","EXPIRATION DATE");
define("FIELD_COMPAIGNS_DISCOUNTTYPE","DISCOUNT TYPE");
define("FIELD_PROMOTION_DISCOUNT","PROMOTION DISCOUNT");

define("LIST_ADDCOMPANYS","ADD COMPANY");


define("FIELD_COMMISSION","COMMISSION");






/* NEW PASSWORD && CONFIRM PASSWORD */


define("FIELD_NEWPASSWORD","NEW PASSWORD");
define("FIELD_CURRENTPASSWORD","CURRENT PASSWORD");
define("FIELD_CONFIRMPASWORD","CONFIRM PASSWORD");







/*
 * Table headings
 */

define("LIST_CITY_TABLE_SLNO","SL.NO");
define("LIST_CITY_TABLE_COUNTRY","COUNTRY");
define("LIST_CITY_TABLE_CITY","CITY");
define("LIST_CITY_TABLE_LATITUDE","Latitude");
define("LIST_CITY_TABLE_LONGITUDE","Longitude");
define("LIST_CITY_TABLE_SELECT","SELECT");


define("LIST_CITY_TABLE_EDITCITIESGEO","Edit city's geo location");



define("COMPANY_ADDEDIT_PERSIONAL","Personal contact details");
define("COMPANY_TABLE_COMPANYID","COMPANY ID");
define("COMPANY_TABLE_COMPANYNAME","COMPANY NAME");
define("COMPANY_TABLE_ADDRESSLINE","ADDRESS LINE");
define("COMPANY_TABLE_CITY","CITY");
define("COMPANY_TABLE_STATE","STATE");
define("COMPANY_TABLE_POSTALCODE","POSTAL CODE");
define("COMPANY_TABLE_FIRSTNAME","FIRST NAME");
define("COMPANY_TABLE_LASTNAME","LAST NAME");
define("COMPANY_TABLE_EMAIL","EMAIL");
define("COMPANY_TABLE_MOBILE","MOBILE");
define("COMPANY_TABLE_SELECT","SELECT");

define("VTYPE_TABLE_TYPEID","TYPE ID");
define("VTYPE_TABLE_TYPENAME","TYPE NAME");
define("VTYPE_TABLE_MAXSIZE","MAX SIZE");
define("VTYPE_TABLE_BASEFARE","BASE FARE");
define("VTYPE_TABLE_MINFARE","MIN FARE");
define("VTYPE_TABLE_PRICEMINUTE","PRICE PER MINUTE");
define("VTYPE_TABLE_PRICRMILE","PRICE PER MILE");
define("VTYPE_TABLE_TYPEDESCRIPTION","TYPE DESCRIPTION");
define("VTYPE_TABLE_CITY","CITY");
define("VTYPE_TABLE_SELECT","SELECT");

define("VEHICLES_TABLE_VEHICLEID","VEHICLE_ID");
define("VEHICLES_TABLE_TITLE","TITLE");
define("VEHICLES_TABLE_VMODAL","V MODEL");
define("VEHICLES_TABLE_VTYPE","V TYPE");
define("VEHICLES_TABLE_VREGNO","V REG NO");
define("VEHICLES_TABLE_LICENSEPLATNO","LICENSE PLATE NO");
define("VEHICLES_TABLE_INSURENCENUMBER","INSURANCE NUMBER");
define("VEHICLES_TABLE_VCOLOR","V COLOR");
define("VEHICLES_TABLE_OPTION","OPTION");

define("DRIVERS_TABLE_DRIVER_ID","PROVIDER_ID");
define("DRIVERS_TABLE_NAME","NAME");
define("DRIVERS_TABLE_MOBILE","MOBILE");
define("DRIVERS_TABLE_EMAIL","EMAIL");
define("DRIVERS_TABLE_REGDATE","REG DATE");
define("DRIVERS_TABLE_DEVICETYPE","DEVICE TYPE");
define("DRIVERS_TABLE_RESETPASSWORD","RESET PASSWORD");
define("DRIVERS_TABLE_PROFILEPIC","PROFILE PIC");
define("DRIVERS_TABLE_OPTION","OPTION");

define("PASSENGERS_TABLE_PASSENGERID","PASSENGER ID");
define("PASSENGERS_TABLE_FIRSTNAME","FIRST NAME");
define("PASSENGERS_TABLE_LASTNAME","LAST NAME");
define("PASSENGERS_TABLE_MOBILE","MOBILE");
define("PASSENGERS_TABLE_EMAIL","EMAIL");
define("PASSENGERS_TABLE_REGDATE","REG DATE");
define("PASSENGERS_TABLE_DEVICETYPE","DEVICE TYPE");
define("PASSENGERS_TABLE_ZIPCODE","ZIP CODE");
define("PASSENGERS_TABLE_PROFILEPIC","PROFILE PIC");
define("PASSENGERS_TABLE_STATUS","STATUS");
define("PASSENGERS_TABLE_SELECT","SELECT");


define("DISPATCHERS_TABLE_DISPATCHERID","DISPATCHER ID");
define("DISPATCHERS_TABLE_CITY","CITY");
define("DISPATCHERS_TABLE_EMAIL","EMAIL");
define("DISPATCHERS_TABLE_DISPATCHERNAME","DISPATCHER NAME");
define("DISPATCHERS_TABLE_NOOFBOOKINGS","NO OF BOOKINGS");
define("DISPATCHERS_TABLE_OPTION","OPTION");

define("DOCUMENT_TABLE_DOCUMENTID","DOCUMENT ID");
define("DOCUMENT_TABLE_FIRSTNAME","FIRST NAME");
define("DOCUMENT_TABLE_LASTNAME","LAST NAME");
define("DOCUMENT_TABLE_VIEW","VIEW");
define("DOCUMENT_TABLE_EXPIRYDATE","EXPIRY DATE");


define("DOCUMENT_TABLE_VEHICLEID","VEHICLE ID");
define("DOCUMENT_TABLE_COMPANY","COMPANY");


define("DRIVERREVIEW_TABLE_SLNO","SL.NO");
define("DRIVERREVIEW_TABLE_DRIVERID","BOOKING ID");
define("DRIVERREVIEW_TABLE_REVIEW","REVIEW");
define("DRIVERREVIEW_TABLE_RATING","RATING");
define("DRIVERREVIEW_TABLE_REVIEWDATE","BOOKING DATE AND TIME");
define("DRIVERREVIEW_TABLE_DRIVERNAME","PROVIDER NAME");
define("DRIVERREVIEW_TABLE_PASSENGERNAME","PASSENGER ID");
define("DRIVERREVIEW_TABLE_STATUS","STATUS");
define("DRIVERREVIEW_TABLE_SELECT","SELECT");

define("PASSENGRRATING_TABLE_SLNO","SL.NO");
define("PASSENGRRATING_TABLE_PASSENGERID","PASSENGER ID");
define("PASSENGRRATING_TABLE_PASSENGERNAME","PASSENGER NAME");
define("PASSENGRRATING_TABLE_PASSENGEREMAIL","PASSENGER EMAIL");
define("PASSENGRRATING_TABLE_AVGRATING","AVG RATING");

define("DISPUTES_TABLE_DISPUTEID","DISPUTE ID");
define("DISPUTES_TABLE_BOOKINGID","BOOKING ID");
define("DISPUTES_TABLE_PASSENGERID","PASSENGER ID");
define("DISPUTES_TABLE_PASSENGERNAME","PASSENGER NAME");
define("DISPUTES_TABLE_DRIVERID","PROVIDER ID");
define("DISPUTES_TABLE_DRIVERNAME","PROVIDER NAME");
define("DISPUTES_TABLE_DISPUTEMESSAGE","DISPUTE MESSAGE");
define("DISPUTES_TABLE_DISPUTEDATE","DISPUTE DATE");
define("DISPUTES_TABLE_APPOINTMENTID","APPOINTMENT ID");
define("DISPUTES_TABLE_SELECT","SELECT");


define("VEHICLEMAKE_TABLE_ID","ID");
define("VEHICLEMAKE_TABLE_TYPENAME","TYPE NAME");
define("VEHICLEMAKE_TABLE_SELECT","SELECT");

define("VEHICLEMODEL_TABLE_ID","ID");
define("VEHICLEMODEL_TABLE_MODELID","MODEL ID");
define("VEHICLEMODEL_TABLE_MAKE","MAKE");

define("VEHICLEMODEL_TABLE_MODEL","MODEL");
define("VEHICLEMODEL_TABLE_SELECT","SELECT");

define("COMPAIGNS_TABLEL_DISCOUNT","DISCOUNT");
define("COMPAIGNS_TABLE_REFERRALDISCOUNT","REFERRAL DISCOUNT");
define("COMPAIGNS_TABLE_CITY","CITY");
define("COMPAIGNS_TABLE_CURRENCY","CURRENCY");
define("COMPAIGNS_TABLE_SELECT"," SELECT");

define("COMPAIGNS_TABLE_CODE","CODE");
define("COMPAIGNS_TABLE_STARTDATE","START DATE");
define("COMPAIGNS_TABLE_ENDDATE","END DATE");
define("COMPAIGNS_TABLE_DISCOUNT","DISCOUNT");
define("COMPAIGNS_TABLE_MESSAGE","MESSAGE");
define("COMPAIGNS_TABLE_PROMOT_CITY","CITY");
define("COMPAIGNS_TABLE_PROMOT_SELECT","SELECT");



/*
 * Generic
 */

define("GEN_DELETE","Delete");
define("GEN_OPEN","Open");
define("GEN_CREATE","Create");