<?php
date_default_timezone_set('UTC');
$rupee = "$";
?>
<style>
    .ui-autocomplete{
        z-index: 5000;
    }
    #selectedcity,#companyid{
        display: none;
    }
    .ui-menu-item{cursor: pointer;background: black;color:white;border-bottom: 1px solid white;width: 200px;}
</style>
<script>
    function LoadCategory()
    {
        var cityid = $("#cityid option:selected").val();
        $.ajax({
            url: "<?php echo base_url('index.php/superadmin') ?>/GetServicesByCity",
            type: "POST",
            data: {cid: cityid},
            dataType: 'JSON',
            beforeSend: function () {
                $('#catloading').show();
            },
            complete: function () {
                $('#catloading').hide();
            },
            success: function (response)
            {
                $('#catlist').html('');
                $('#catlist').append('<option value="0">Select Category</option>');
                $.each(response, function (key, value)
                {
                    $('#catlist').append('<option value="' + value['id'] + '">' + value['cat_name'] + '</option>');
                });
            }
        });
    }

    function LoadServiceGroup()
    {
        var catlist = $("#catlist option:selected").val();
        $.ajax({
            url: "<?php echo base_url('index.php/superadmin') ?>/GetGroupByCategory",
            type: "POST",
            data: {cid: catlist},
            dataType: 'JSON',
            beforeSend: function () {
                $('#grouploading').show();
            },
            complete: function () {
                $('#grouploading').hide();
            },
            success: function (response)
            {
                $('#grouplist').html('');
                $('#grouplist').append('<option value="0">Select Group</option>');
                $.each(response, function (key, value)
                {
                    $('#grouplist').append('<option value="' + value['id'] + '">' + value['group_name'] + '</option>');
                });
            }
        });
    }
    $(document).ready(function () {
        $('.provisionen').addClass('active');
        $('.provisionen_thumb').attr('src', "<?php echo base_url(); ?>/theme/icon/comission_on copy.png");
        $('.provisionen_thumb').addClass("bg-success");
        $('#search_by_select').change(function () {
            $('#atag').attr('href', '<?php echo base_url() ?>index.php/superadmin/search_by_select/' + $('#search_by_select').val());
            $("#callone").trigger("click");
        });
        var table = $('#tableWithSearch1');
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "sPaginationType": "bootstrap",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 20,
            "order": [[0, "desc"]]
        };
        table.dataTable(settings);
        $('#search-table1').keyup(function () {
            table.fnFilter($(this).val());
        });

      



        $('#btnStickUpSizeToggler').click(function () {
            $("#display-data").text("");
            var size = $('input[name=stickup_toggler]:checked').val();
            var modalElem = $('#sub_cat_mod');
            if (size == "mini") {
                $('#modalStickUpSmall').modal('show')
            } else {
                $('#sub_cat_mod').modal('show')
                if (size == "default") {
                    modalElem.children('.modal-dialog').removeClass('modal-lg');
                } else if (size == "full") {
                    modalElem.children('.modal-dialog').addClass('modal-lg');
                }
            }
        });

        $('#editcitypage').click(function () {
            $("#display-data").text("");
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
            if (val.length == 0) {
                $("#display-data").text(<?php echo json_encode(POPUP_SUB_CAT_ANYONE); ?>);
            } else if (val.length > 1) {
                $("#display-data").text(<?php echo json_encode(POPUP_SUB_CAT_ONLYONE); ?>);
            } 
            else
            {
                    val = val.toString();
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo base_url('index.php/category') ?>/GetOneSubServices",
                        data: {sid: val},
                        dataType: 'JSON',
                        success: function (result)
                        {
                            console.log(result.groupid.$id);
                            $("#close_nos").trigger("click");
                            
                                    $('#s_name').val(result.s_name);
                                    $('#s_desc').val(result.s_desc);
                                    $('#fixed_price').val(result.fixed_price);
                                    $('#EditServiceId').val(result.groupid.$id);
                                    $('#grouplist').val(result.groupid.$id);
                                    $('#sub_cat_mod').modal('show')
                                }
                            });

                        }
        });
        
        $('#addSevicesby').click(function () {
            
            $('#addserviceBY').modal('show')
                                
        });
        
        $("#chekdel").click(function () {
            $("#display-data").text("");
            var gid = '<?php echo $gid ?>';
           
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
            if (val.length > 0)
            {
                val = val.toString();
                var size = $('input[name=stickup_toggler]:checked').val()
                var modalElem = $('#confirmmodel');
                if (size == "mini") {
                    $('#modalStickUpSmall').modal('show')
                } else {
                    $('#confirmmodel').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }
                $("#errorboxdata").text(<?php echo json_encode(SUB_CAT_DELETE); ?>);
                $("#confirmed").click(function () {
                    $.ajax({
                        url: "<?php echo base_url('index.php/category') ?>/DeleteSubCategory",
                        type: "POST",
                        data: {val: val,
                                gid: gid
                                },
                        dataType: 'json',
                        success: function (result)
                        {
//                            $(".close").trigger("click");
//                            var size = $('input[name=stickup_toggler]:checked').val()
//                            var modalElem = $('#confirmmodels');
//                            if (size == "mini")
//                            {
//                                $('#modalStickUpSmall').modal('show')
//                            } else
//                            {
//                                $('#confirmmodels').modal('show')
//                                if (size == "default") {
//                                    modalElem.children('.modal-dialog').removeClass('modal-lg');
//                                } else if (size == "full") {
//                                    modalElem.children('.modal-dialog').addClass('modal-lg');
//                                }
//                            }
//                            if (result.flag == '1') {
//                                $("#errorboxdatas").text(result.msg);
//
//                            } else if (result.flag == '0') {
//                                $("#errorboxdatas").text(result.msg);
//                                $('.checkbox:checked').each(function (i) {
//                                    $(this).closest('tr').remove();
//                                });
//                            }
                            $("#confirmeds").hide();
                            location.reload(true);
                        }
                    });
                });

            } else
            {
                $("#display-data").text(<?php echo json_encode(POPUP_SUB_CAT_ATLEAST); ?>);
            }

        });


        $("#insertsubcat").click(function () {
        var data = $('.checkbox:checked').map(function () {
                    return $(this).attr("data");
                }).get();
                
                var dataarr = data[0];
            $("#addsubcat").text("");
           
            var grouplist = $("#grouplist").val();
            var s_name = $("#s_name").val();
            var s_desc = $("#s_desc").val();
             
            var fixed_price = $("#fixed_price").val();
            var EditServiceId = $("#EditServiceId").val();
           
            if (grouplist == "0" || grouplist == null) {
                $("#addsubcat").text("Please select the group name");
            }else if (s_name == "" || s_name == null) {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_SUB_CATEGORY); ?>);
            }  else if (fixed_price == "" || fixed_price == null) {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_FIXED_PRICE); ?>);
            } else
            {
                 if(EditServiceId != '')
                {
                    $.ajax({
                    url: "<?php echo base_url('index.php/category') ?>/EditServiceby",
                    type: 'POST',
                    data: {
                      
                       grouplist :grouplist,
                       gid :dataarr,
                        s_name: s_name,
                        s_desc: s_desc,
                        fp: fixed_price,
                        edits:EditServiceId
                    },
                    dataType: 'JSON',
                    success: function (response)
                    {
                        $(".close").trigger("click");
                        location.reload();
                    }
                });
                 }else{
                 $.ajax({
                    url: "<?php echo base_url('index.php/category') ?>/AddService",
                    type: 'POST',
                    data: {
                        
                       gid :grouplist,
                        s_name: s_name,
                        s_desc: s_desc,
                        fp: fixed_price,
                    },
                    dataType: 'JSON',
                    success: function (response)
                    {
                        $(".close").trigger("click");
                        location.reload();
                    }
                });
                 }
                
            }
        });
        $("#addser").click(function () {
          
             
             var groupid1 = $("#grouplist1").val();
             
            var s_name1 = $("#s_name1").val();
            var s_desc1 = $("#s_desc1").val();
            var fixed_price1 = $("#fixed_price1").val();
            if(groupid1 == '0')
            {
                $('#addsubcatd33').text("Please select service group ");
            }else if(s_name1 == '')
            {
                $('#addsubcatd33').text("please enter service name");
            }else if(fixed_price1 == '')
            {
                $('#addsubcatd33').text("please enter fixed price");
            }else{
         
                $.ajax({
                    url: "<?php echo base_url('index.php/category') ?>/AddService",
                    type: 'POST',
                    data: {
                        gid :groupid1,
                        s_name: s_name1,
                        s_desc: s_desc1,
                        fp: fixed_price1
                    },
                    dataType: 'JSON',
                    success: function (response)
                    {
                        $(".close").trigger("click");
                        location.reload();
                    }
                });
                }

        });




    });

</script>
<style>
    #active{
        display:none;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {        
        $('#big_table_processing').show();
        var table = $('#big_table');
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "aoColumns": [
                {"sWidth": "2%","sClass" : "text-center"},
                {"sWidth": "7%"},
                {"sWidth": "10%","sClass" : "text-center"},
                {"sWidth": "5%","sClass" : "text-center"},
                {"sWidth": "5%","sClass" : "text-center"}
            ],
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url() ?>index.php/category/datatable_show_services/<?php echo $gid ?>',
//                        "bJQueryUI": true,
//                        "sPaginationType": "full_numbers",
                        "iDisplayStart ": 20,
                        "oLanguage": {
                            "sProcessing": "<img src='http://107.170.66.211/roadyo_live/sadmin/theme/assets/img/ajax-loader_dark.gif'>"
                        },
                        "fnInitComplete": function () {

                            $('#big_table_processing').hide();
                        },
                        'fnServerData': function (sSource, aoData, fnCallback)
                        {
                            $.ajax
                                    ({
                                        'dataType': 'json',
                                        'type': 'POST',
                                        'url': sSource,
                                        'data': aoData,
                                        'success': fnCallback
                                    });
                        }
                    };
                    table.dataTable(settings);
                    // search box for table
                    $('#search-table').keyup(function () {
                        table.fnFilter($(this).val());
                    });
                });
</script>


<style>
/*   .dataTables_wrapper .dataTables_info{
        padding: 0;
    }*/
    .exportOptions{
        display: none;
    }
      .header{
        height:60px !important;
    }
    .header h3{
        margin:10px 0 !important;
    }
</style>
<div class="content">
 <div class="jumbotron  no-margin" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;">
                <h3 class="">Page Title</h3>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg">
        <ul class="breadcrumb">
           <li><a href="<?php echo base_url(); ?>index.php/category/categories_services/" class="">CATEGORY</a>
                    </li>
                    <?php
                                foreach ($citylist as $result) {
                                    if($result->City_Id == $cityIdss)
                                    {
                                        $cityname = $result->City_Name;
                                    }
                                }
                                ;
                                ?>
                    <li><a href="<?php echo base_url(); ?>index.php/category/show_cat/<?php echo $cityIdss; ?>" class=""> <?php echo $cityname; ?></a>
                    </li>
                    <li><a href="<?php echo base_url(); ?>index.php/category/sgroup/<?php echo $catId; ?>" class=""><?php echo $cat_name; ?></a>
                    </li>
                    <li><a href="" class="active"><?php echo $group_name ?></a>
                    </li>
        </ul>
        <div class="container-md-height m-b-20">
            <div class="panel panel-transparent">
                <ul class="nav nav-tabs nav-tabs-simple bg-white" role="tablist" data-init-reponsive-tabs="collapse">
                     <ul class="nav nav-tabs nav-tabs-simple bg-white" role="tablist" data-init-reponsive-tabs="collapse">
                        <li class="active">
                            <a href="#" data-toggle="tab" role="tab" aria-expanded="false"><?php echo LIST_SERVICES; ?></a>
                        </li>    
                        <div class="pull-right m-t-10"> 
                            <button class="btn btn-success btn-cons" id="editcitypage"><i class="fa fa-edit text-white"></i> <?php echo BUTTON_EDIT; ?></button>
                        </div>
                        <div class="pull-right m-t-10"> 
                            <button class="btn btn-danger btn-cons" id="chekdel"><span><i class="fa fa-trash text-white"></i> <?php echo BUTTON_DELETE; ?></button>
                        </div>
                        <div class="pull-right m-t-10"> <button class="btn btn-primary btn-cons" id="addSevicesby"><span><i class="fa fa-plus text-white"></i> <?php echo BUTTON_ADD_SERVICE; ?></button></a></div>    
                </ul>
                <div class="panel-group visible-xs" id="LaCQy-accordion"></div>
                <div class="tab-content">
                    <div class="tab-pane active" id="hlp_txt">
                        <div class="row panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title"></div>
                                <div class="error-box" id="display-data" style="text-align:center"></div>
                                <div class='pull-right cls110'>
                                    
                                    <div class="pull-right m-t-10 btn-group ">
                                        <input type="text" id="search-table" class="form-control pull-right"  placeholder="<?php echo SEARCH; ?>"/> 
                                    </div>
                                </div>
                                <br>
                                <div class="panel-body no-padding">
                                    <?php echo $this->table->generate(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

 
       

<div class="modal fade stick-up" id="sub_cat_mod" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <div class=" clearfix text-left">
                        <button type="button" id="close_nos" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                    </div>
                    <h3> <?php echo EDIT_SERVICE_TEXT; ?></h3>
                </div>
                <br>
                <div class="modal-body">
                    
                      <div class="form-group" class="formex">
                        <label for="grouplist" class="col-sm-4 control-label" ><?php echo FIELD_SELECT_GROUP; ?><span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <select id="grouplist" name="fdata[grouplist]"  class="form-control error-box-class">   
                                <option value="0">Select Service Group</option>
                                 <?php
                                 $this->load->library('mongo_db');
                                 $cursor = $this->mongo_db->get_where('Category', array('_id' => new MongoId($catId)));    
                                 
                                foreach ($cursor as $result) {
                                   
                                    foreach ($result['groups'] as $groups){
                                        $cur = (string) $groups['gid'];
                                    
                                        echo "<option value=" . $cur . ">" . $groups['group_name'] . "</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="form-group" class="formex">
                        <div class="frmSearch">
                            <label for="s_name" class="col-sm-4 control-label"><?php echo FIELD_SUB_CATEGORY_NAME; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-8">
                                <input name="fdata[s_name]" type="text" id="s_name"  placeholder="Service Name" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>
                                <div id="suggesstion-box"></div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="form-group" class="formex">
                        <label for="s_desc" class="col-sm-4 control-label"><?php echo FIELD_SUB_CATEGORY_DESC; ?></label>
                        <div class="col-sm-6">
                            <input type="text"  id="s_desc" name="fdata['s_desc']"  class="form-control error-box-class" placeholder="Service Desc">
                        </div>
                    </div>
                    <br/>
                    <br/>
                    <div class="form-group" class="formex">
                        <label for="fixed_price" class="col-sm-4 control-label"> <?php echo FIELD_FIXED_PRICE; ?><span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <input type="number" min="0"  id="fixed_price" name="fdata[fixed_price]" class="form-control error-box-class" placeholder="Fixed Price">
                        </div>
                    </div>
                    <input type="hidden" id="EditServiceId" value=""/>
                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4 error-box" id="addsubcat"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="insertsubcat" ><?php echo BUTTON_SAVE; ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
    </button>
</div>

<div class="modal fade stick-up" id="addserviceBY" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <div class=" clearfix text-left">
                        <button type="button" id="close_nos" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                    </div>
                    <h3> Add Service</h3>
                </div>
                <br>
                <div class="modal-body">
                    
                      <div class="form-group" class="formex">
                        <label for="grouplist" class="col-sm-4 control-label" ><?php echo FIELD_SELECT_GROUP; ?><span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <select id="grouplist1" name="fdata[grouplist]"  class="form-control error-box-class">   
                                <option value="0">Select Service Group</option>
                                 <?php
                                 $this->load->library('mongo_db');
                                 $cursor = $this->mongo_db->get_where('Category', array('_id' => new MongoId($catId)));    
                                 
                                foreach ($cursor as $result) {
                                   
                                    foreach ($result['groups'] as $groups){
                                        $cur = (string) $groups['gid'];
                                    
                                        echo "<option value=" . $cur . ">" . $groups['group_name'] . "</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="form-group" class="formex">
                        <div class="frmSearch">
                            <label for="s_name" class="col-sm-4 control-label"><?php echo FIELD_SUB_CATEGORY_NAME; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-8">
                                <input name="fdata[s_name]" type="text" id="s_name1"  placeholder="Service Name" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>
                                <div id="suggesstion-box"></div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="form-group" class="formex">
                        <label for="s_desc" class="col-sm-4 control-label"><?php echo FIELD_SUB_CATEGORY_DESC; ?></label>
                        <div class="col-sm-6">
                            <input type="text"  id="s_desc1" name="fdata['s_desc']"  class="form-control error-box-class" placeholder="Service Desc">
                        </div>
                    </div>
                    <br/>
                    <br/>
                    <div class="form-group" class="formex">
                        <label for="fixed_price" class="col-sm-4 control-label"> <?php echo FIELD_FIXED_PRICE; ?><span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <input type="number" min="0"  id="fixed_price1" name="fdata[fixed_price]" class="form-control error-box-class" placeholder="Fixed Price">
                        </div>
                    </div>
                    <input type="hidden" id="" value=""/>
                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4 error-box" id="addsubcatd33"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="addser" ><?php echo BUTTON_SAVE; ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
    </button>
</div>


<!--delete sub category conformatio model-->
<div class="modal fade stick-up" id="confirmmodel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class=" clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>

                </div>
            </div>
            <br>
            <div class="modal-body">
                <div class="row">
                    <div class="error-box" id="errorboxdata" style="font-size: large;text-align:center"><?php echo COMPAIGNS_DISPLAY; ?></div>
                </div>
            </div>
            <br>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4" ></div>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4" >
                        <button type="button" class="btn btn-primary pull-right" id="confirmed" ><?php echo BUTTON_YES; ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
