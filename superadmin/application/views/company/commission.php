<?php
date_default_timezone_set('UTC');
$rupee = "$";
//error_reporting(0);
?>

<style>
   
    .ui-autocomplete{
        z-index: 5000;
    }
    #selectedcity,#companyid{
        display: none;
    }

    .ui-menu-item{cursor: pointer;background: black;color:white;border-bottom: 1px solid white;width: 200px;}
</style>
<script>
    $(document).ready(function () {
//          $("#define_page").html("Driver Review");
        $('.provisionen').addClass('active');
        $('.provisionen_thumb').attr('src', "<?php echo base_url(); ?>/theme/icon/comission_on copy.png");
        $('.provisionen_thumb').addClass("bg-success");
        $('#search_by_select').change(function () {


            $('#atag').attr('href', '<?php echo base_url() ?>index.php/superadmin/search_by_select/' + $('#search_by_select').val());

            $("#callone").trigger("click");
        });

        var table = $('#tableWithSearch1');

        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "sPaginationType": "bootstrap",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 20,
            "order": [[0, "desc"]]
        };

        table.dataTable(settings);

        $('#search-table1').keyup(function () {
            table.fnFilter($(this).val());
        });
        $('#insert').click(function () {
            $('.clearerror').text("");
            var providername = $("#businessname").val();

            var commission = $("#commission").val();

            if (providername == "0")
            {

                $("#clearerror").text(<?php //echo json_encode(POPUP_BUSINESS_NAME); ?>);


            } else if (commission == "" || commission == null)
            {

                $("#clearerror").text(<?php //echo json_encode(POPUP_COMMISSION_NAME); ?>);

            } else {

                $.ajax({
                    url: "<?php echo base_url('index.php/superadmin') ?>/insertcommission",
                    type: 'POST',
                    data: {
                        providername: providername,
                        commission: commission,
                    },
                    dataType: 'JSON',
                    success: function (response)
                    {
                        $('.close').trigger('click');
                        window.location = "<?php echo base_url(); ?>index.php/superadmin/commission/1";
                    }
                });


            }
        });


        $('#editbusiness').click(function () {

            var val = $('.checkbox:checked').val();

            $('.clearerror').text("");

            var commission = $('#editedcommission').val();

            if (commission == "" || commission == null) {

              //  $("#clearerror").text(<?php //echo json_encode(POPUP_COMMISSIONEDIT_NAME); ?>);
            } else {

                $.ajax({
                    url: "<?php echo base_url('index.php/superadmin') ?>/editcommission",
                    type: 'POST',
                    data: {
                        id: val,
                        commission: commission,
                    },
                    dataType: 'JSON',
                    success: function (response)
                    {
                        $('.close').trigger('click');
                         $('#editModal').modal('hide');
                        window.location = "<?php echo base_url(); ?>index.php/superadmin/commission/1";
                    }
                });

            }

        });



        $('#btnStickUpSizeToggler').click(function () {
            $("#display-data").text("");

             
            var size = $('input[name=stickup_toggler]:checked').val()
            var modalElem = $('#myModal');
            if (size == "mini") {
                $('#modalStickUpSmall').modal('show')
            } else {
                $('#myModal').modal('show')
                if (size == "default") {
                    modalElem.children('.modal-dialog').removeClass('modal-lg');
                } else if (size == "full") {
                    modalElem.children('.modal-dialog').addClass('modal-lg');
                }
            }
        });
        $('#edit').click(function () {
            
            $('#modalHeading').html(<?php echo json_encode(EDIT_PRO_COMMISSION); ?>);
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
          
            if (val.length < 0 || val.length=='') {
               
                $("#display-data").text(<?php echo json_encode(POPUP_ONE_BUSINESSEDIT); ?>);
            } else if (val.length == 1)
            {
                var data = $('.checkbox:checked').map(function () {
                    return $(this).attr("data");
                }).get();
                $('#editedcommission').val(data);
                var size = $('input[name=stickup_toggler]:checked').val()
                var modalElem = $('#editModal');
                if (size == "mini")
                {
                    $('#modalStickUpSmall').modal('show')
                } else
                {
                    $('#editModal').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }
                $("#display-data").text(<?php echo json_encode(POPUP_PASSENGERS_ACTIVATE); ?>);

                $("#confirmeds").click(function () {
                    {
                        $.ajax({
                            url: "<?php echo base_url('index.php/superadmin') ?>/editcommission",
                            type: "POST",
                            data: {val: val},
                            dataType: 'json',
                            success: function (result)
                            {

                                $('.checkbox:checked').each(function (i) {
                                    $(this).closest('tr').remove();
                                });
                                $(".close").trigger('click');
                            }
                        });
                    }

                });

            } else if (val.length > 1)
            {
                //      alert("select atleast one passenger");
                $("#display-data").text(<?php echo json_encode(POPUP_ONE_BUSINESSEDIT); ?>);
            }



        });

        $('#fdelete').click(function () {
        
//            $("#display-data").text("");
       
        var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
           
    
             if (val.length < 0||val.length == 0) {
              
                $("#display-data").text(<?php echo json_encode(POPUP_DELETE_COMMISSION); ?>);

            }
            else if (val.length == 1 || val.length > 1)
            {
                
                $("#display-data").text("");
                       var BusinessId =  val;
//                    alert(BusinessId);
                      
                 var size = $('input[name=stickup_toggler]:checked').val()
            var modalElem = $('#confirmmodel');
            if (size == "mini") {
                $('#modalStickUpSmall').modal('show')
            } else {
                $('#confirmmodel').modal('show')
                if (size == "default") {
                    modalElem.children('.modal-dialog').removeClass('modal-lg');
                } else if (size == "full") {
                    modalElem.children('.modal-dialog').addClass('modal-lg');
                }
            }
                      
                      
                
                $("#confirmed").click(function(){

                          $.ajax({
                               url: "<?php echo base_url('index.php/superadmin') ?>/deleteCommission",

                             type: "POST",
                                    data: {
                        val: val,
                    },
                           
//                                data: {val: val},
                                dataType: 'json',
                               success: function (response)
                               {
                                $(".close").trigger("click");
                                location.reload();
                               }
                           });
                           
                       });             
                 }
                           
                      
           

            });
     

    });

</script>
<style>
    #active{
        display:none;
    }
</style>
<style>
    .exportOptions{
        display: none;
    }
      .header{
        height:60px !important;
    }
    .header h3{
        margin:10px 0 !important;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {


        $('#big_table_processing').show();

        var table = $('#big_table');
  $('#big_table_processing').show();
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url() ?>index.php/superadmin/datatable_commission',
//            "bJQueryUI": true,
//            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "order": [[0, "desc"]],
            "oLanguage": {
                "sProcessing": "<img src='http://107.170.66.211/roadyo_live/sadmin/theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {

                $('#big_table_processing').hide();
            },
                       "drawCallback": function () {
                $('#big_table_processing').hide();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };




        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function () {
          $('#big_table_processing').show();
            table.fnFilter($(this).val());
        });


        $('.whenclicked li').click(function () {
            // alert($(this).attr('id'));\
            if ($(this).attr('id') == 1) {
                $('#inactive').show();
                $('#active').hide();
                $('#btnStickUpSizeToggler').show();
//                 $('#big_table').find('td:eq(6),th:eq(6)').hide();
            } else if ($(this).attr('id') == 2) {
                $('#active').show();
                $('#inactive').hide();
                $('#btnStickUpSizeToggler').hide();
//             $('#big_table').find('td:eq(6),th:eq(6)').show();


            }
        });

        $('.changeMode').click(function () {

            var table = $('#big_table');
            $('#big_table_processing').show();
  $('#big_table_processing').show();
            var settings = {
                "autoWidth": false,
                "sDom": "<'table-responsive't><'row'<p i>>",
                "destroy": true,
                "scrollCollapse": true,
                "iDisplayLength": 20,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": $(this).attr('data'),
//                "bJQueryUI": true,
//                "sPaginationType": "full_numbers",
                "iDisplayStart ": 20,
                "oLanguage": {
                    "sProcessing": "<img src='http://107.170.66.211/roadyo_live/sadmin/theme/assets/img/ajax-loader_dark.gif'>"
                },
                "fnInitComplete": function () {
                    //oTable.fnAdjustColumnSizing();
                    $('#big_table_processing').hide();

                },
                           "drawCallback": function () {
                $('#big_table_processing').hide();
            },
                'fnServerData': function (sSource, aoData, fnCallback)
                {
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });
                }
            };

            $('.tabs_active').removeClass('active');

            $(this).parent().addClass('active');



            table.dataTable(settings);

            // search box for table
            $('#search-table').keyup(function () {
              $('#big_table_processing').show();
                table.fnFilter($(this).val());
            });

        });

    });

    function refreshTableOnCityChange() {

        var table = $('#big_table');
        $('#big_table_processing').show();
  $('#big_table_processing').show();
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": $(".whenclicked li.active").children('a').attr('data'),
//            "bJQueryUI": true,
//            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "oLanguage": {
                "sProcessing": "<img src='http://107.170.66.211/roadyo_live/sadmin/theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {
                //oTable.fnAdjustColumnSizing();
                $('#big_table_processing').hide();

            },
                       "drawCallback": function () {
                $('#big_table_processing').hide();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function () {
  $('#big_table_processing').show();
            table.fnFilter($(this).val());
        });
    }
</script>


<style>
    .exportOptions{
        display: none;
    }
</style>

<div class="content">
 <div class="jumbotron  no-margin" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;">
                <h3 class="">Page Title</h3>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg">
        <ul class="breadcrumb">
<!--            <li>
                <a href="#" class="active"><?php echo LIST_COMMISSION; ?></a>
            </li>-->
            
        </ul>
        <div class="container-md-height m-b-20">
            <div class="panel panel-transparent">
                <ul class="nav nav-tabs nav-tabs-simple bg-white whenclicked li" role="tablist" data-init-reponsive-tabs="collapse">
                    <li id="1" class="active" style="cursor:auto">
                            <a><span style="cursor:auto"><?php echo LIST_COMMISSION; ?></span></a>
                        </li>
                        <div class="pull-right m-t-10"> <button class="btn btn-info btn-cons" id="edit"><i class="fa fa-edit text-white"></i> <?php echo BUTTON_EDIT; ?></button></div>
                        <!--<div class="pull-right m-t-10"> <button class="btn btn-primary btn-cons" id="btnStickUpSizeToggler"><span><?php echo BUTTON_ADD; ?></button></a></div>-->
                        <!--<div class="pull-right m-t-10"> <button class="btn btn-danger btn-cons" id="fdelete"><span><i class="fa fa-trash text-white"></i> <?php echo BUTTON_DELETE; ?></button></a></div>-->

                </ul>
                <div class="panel-group visible-xs" id="LaCQy-accordion"></div>
                <div class="tab-content">
                    <div class="tab-pane active" id="hlp_txt">
                        <div class="row panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title"></div>
                                 <div class="error-box" id="display-data" style="text-align:center"></div>
                                 <div id="big_table_processing" class="dataTables_processing" style=""><img src="<?php echo APP_SERVER_HOST ?>pics/ajax-loader_dark.gif"></div>
                                <div class='pull-right cls110'>
                                     
                                    <div class="pull-right">
                                            <input type="text" id="search-table" class="form-control pull-right" placeholder="<?php echo SEARCH; ?>"> </div>
                                    </div>
                                    
                                </div>
                                <br>
                                <div class="panel-body no-padding">
                                    <?php echo $this->table->generate(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
 
     



<div class="modal fade stick-up" id="confirmmodel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                <div class=" clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>

                </div>

            </div>
            <br>
            <div class="modal-body">
                <div class="row">

                    <div class="error-box" id="errorboxdata" style="font-size: large;text-align:center"><?php echo VEHICLEMODEL_DELETE; ?></div>

                </div>
            </div>

            <br>

            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4" ></div>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4" >
                        <button type="button" class="btn btn-primary pull-right" id="confirmed" ><?php echo BUTTON_YES; ?></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>



<div class="modal fade stick-up" id="confirmmodels" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                <div class=" clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>

                </div>

            </div>
            <br>
            <div class="modal-body">
                <div class="row">

                    <div class="error-box" id="errorboxdatas" style="font-size: large;text-align:center"><?php echo VEHICLEMODEL_DELETE; ?></div>

                </div>
            </div>

            <br>

            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4" ></div>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4" >
                        <button type="button" class="btn btn-primary pull-right" id="confirmeds" ><?php echo BUTTON_YES; ?></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<div class="modal fade stick-up" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">

                <div class="modal-header">

                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>

                    </div>
                    <h3> <?php echo SELECT_COUNTRY_ANDBUSINESS_COMMISSION; ?></h3>
                </div>

                <div class="modal-body">

                    <BR>


                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-4 control-label" ><?php echo FIELD_BUSINESSNAME; ?><span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">

                            <select id="businessname" name="country_select"  class="form-control error-box-class">
                                <option value="0">Select Business</option>


                                <?php
                                foreach ($business as $result) {
                                    echo "<option value=" . $result['masterid'] . ">" . $result['businessname'] . "</option>";
                                }
                                ?>

                            </select>
                        </div>
                    </div>

                    <br>
                    <br>
                    <div class="form-group" class="formex">

                        <div class="frmSearch">
                            <label for="fname" class="col-sm-4 control-label"><?php echo FIELD_COMMISSION; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" id="commission" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>

                                <div id="suggesstion-box"></div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>

                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4 error-box" id="clearerror"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="insert" ><?php echo BUTTON_ADD; ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
    </button>
</div>


<div class="modal fade stick-up" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">

                <div class="modal-header">

                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>

                    </div>
                    <h3 id='modalHeading'> <?php echo SELECT_COUNTRY_EDIT_BUSINESS_COMMISSION; ?></h3>
                </div>

                <div class="modal-body">


                    <br>
                    <br>
                    <div class="form-group" class="formex">

                        <div class="frmSearch">
                            <label for="fname" class="col-sm-4 control-label"><?php echo FIELD_COMMISSION; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" id="editedcommission" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>

                                <div id="suggesstion-box"></div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>

                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4 error-box" id="clearerror"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="editbusiness" ><?php echo BUTTON_ADD; ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
    </button>
</div>
