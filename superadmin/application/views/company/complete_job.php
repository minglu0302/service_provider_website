<?php
date_default_timezone_set('UTC');
$rupee = "$";
error_reporting(0);
?>
<style>
    .modal .modal-content {
    border: 7px solid !important;
    }
    a, a:focus, a:hover, a:active {
        cursor: pointer;

    }
    .modal-dialog {
        width: 450px;

    }
    .ui-autocomplete{
        z-index: 5000;
    }
    #selectedcity,#companyid{
        display: none;
    }
      .header{
        height:60px !important;
    }
    .header h3{
        margin:10px 0 !important;
    }
    .table-responsive { overflow-x: auto; }

    .ui-menu-item{cursor: pointer;background: black;color:white;border-bottom: 1px solid white;width: 200px;}
</style>
<style>
/*    #bill_style{
            border-radius: 25px;
    border: 1px solid #ccc;
    padding: 20px;
    }*/
</style>
<style>
    .col-md-12.col-xlg-6 > p {
        font-size: 18px;
        letter-spacing: 1px;
        font-weight: 800;
        text-align: center;
        color: #00b3f4;
    }
    .fs-14 {
        font-size: 20px !important;
        color: #054949!important;
    }
    .col-sm-8.p-r-10 {
        width: 100%;
    }
    .modal .modal-body {
        background: #ffffff;
    }
    p.pull-right.bold {
        color: #00b3f4;
        font-size: 14px;
    }

</style>
<script>

    $(document).ready(function () {
        var socket = io('<?php echo socketpath ?>');
        socket.on('LiveBookingResponce', function (data) {
            $('#big_table').find('tr:first').find('th:first').trigger('click');
        });
    });
    
    $(document).ready(function () {
        $('.exportclick').click(function () {
            if ($('#start').val() != '' || $('#end').val() != '') {

                var dateObject = $("#start").datepicker("getDate"); // get the date object
                var st = dateObject.getFullYear() + '-' + (dateObject.getMonth() + 1) + '-' + dateObject.getDate();// Y-n-j in php date() format
                var dateObject = $("#end").datepicker("getDate"); // get the date object
                var end = dateObject.getFullYear() + '-' + (dateObject.getMonth() + 1) + '-' + dateObject.getDate();// Y-n-j in php date() format
                $('#exportformdate').attr('href', '<?php echo base_url() ?>index.php/superadmin/callExel/' + st + '/' + end);
                $('#exportformdate')[0].click();
            } else {
                $('#exportformdate').attr('href', '<?php echo base_url() ?>index.php/superadmin/callExel');
                $('#exportformdate')[0].click();
            }
        });


        $('#searchData').click(function () {
            if ($("#start").val() && $("#end").val())
            {

                var dateObject = $("#start").datepicker("getDate"); // get the date object
                var st = dateObject.getFullYear() + '-' + (dateObject.getMonth() + 1) + '-' + dateObject.getDate();// Y-n-j in php date() format
                var dateObject = $("#end").datepicker("getDate"); // get the date object
                var end = dateObject.getFullYear() + '-' + (dateObject.getMonth() + 1) + '-' + dateObject.getDate();// Y-n-j in php date() format

//           $('#createcontrollerurl').attr('href','<?php // echo base_url()                 ?>//index.php/superadmin/Get_dataformdate/'+st+'/'+end);

                var table = $('#big_table');

                var settings = {
                    "autoWidth": false,
                    "sDom": "<'table-responsive't><'row'<p i>>",
//            "sPaginationType": "bootstrap",
                    "destroy": true,
                    "scrollCollapse": true,
//            "oLanguage": {
//                "sLengthMenu": "_MENU_ ",
//                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
//            },
                    "autoWidth": false,
                            "iDisplayLength": 20,
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": '<?php echo base_url() ?>index.php/superadmin/transection_data_form_date/' + st + '/' + end,
//                    "bJQueryUI": true,
//                    "sPaginationType": "full_numbers",
                    "iDisplayStart ": 20,
                    "oLanguage": {
                        "sProcessing": "<img src='<?php echo base_url(); ?>theme/assets/img/ajax-loader_dark.gif'>"
                    },
                    "fnInitComplete": function () {
                        //oTable.fnAdjustColumnSizing();
                    },
                    'fnServerData': function (sSource, aoData, fnCallback)
                    {
                        $.ajax
                                ({
                                    'dataType': 'json',
                                    'type': 'POST',
                                    'url': sSource,
                                    'data': aoData,
                                    'success': fnCallback
                                });
                    }
                };

                table.dataTable(settings);

                // search box for table
                $('#search-table').keyup(function () {
                    table.fnFilter($(this).val());
                });
                 $('#payment_search').change(function () {
                        table.fnFilter($(this).val());
                    });

            } else
            {
                var size = $('input[name=stickup_toggler]:checked').val()
                var modalElem = $('#confirmmodels');
                if (size == "mini")
                {
                    $('#modalStickUpSmall').modal('show')
                } else
                {
                    $('#confirmmodels').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }
                $("#errorboxdatas").text(<?php echo json_encode(POPUP_DRIVERS_DEACTIVAT_DATEOFBOOKING); ?>);

                $("#confirmeds").click(function () {
                    $('.close').trigger('click');
                });
            }

        });

        $('#search_by_select').change(function () {


//          $('#atag').attr('href','<?php //echo base_url()                 ?>//index.php/superadmin/search_by_select/'+$('#search_by_select').val());

            var table = $('#big_table');

            var settings = {
                "autoWidth": false,
                "sDom": "<'table-responsive't><'row'<p i>>",
//            "sPaginationType": "bootstrap",
                "destroy": true,
                "scrollCollapse": true,
//            "oLanguage": {
//                "sLengthMenu": "_MENU_ ",
//                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
//            },
                "iDisplayLength": 20,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": '<?php echo base_url() ?>index.php/superadmin/search_by_select/' + $('#search_by_select').val(),
//                "bJQueryUI": true,
//                "sPaginationType": "full_numbers",
                "iDisplayStart ": 20,
                "oLanguage": {
                    "sProcessing": "<img src='<?php echo base_url(); ?>theme/assets/img/ajax-loader_dark.gif'>"
                },
                "fnInitComplete": function () {
                    //oTable.fnAdjustColumnSizing();
                },
                'fnServerData': function (sSource, aoData, fnCallback)
                {
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });
                }
            };

            table.dataTable(settings);

            // search box for table
            $('#search-table').keyup(function () {
                table.fnFilter($(this).val());
            });

             $('#payment_search').change(function () {
                        table.fnFilter($(this).val());
                    });
//            $("#callone").trigger("click");
        });

    });



    function refreshTableOnCityChange() {

        var table = $('#big_table');
        var url = '';

        if ($('#start').val() != '' || $('#end').val() != '') {

            var dateObject = $("#start").datepicker("getDate"); // get the date object
            var st = dateObject.getFullYear() + '-' + (dateObject.getMonth() + 1) + '-' + dateObject.getDate();// Y-n-j in php date() format
            var dateObject = $("#end").datepicker("getDate"); // get the date object
            var end = dateObject.getFullYear() + '-' + (dateObject.getMonth() + 1) + '-' + dateObject.getDate();// Y-n-j in php date() format

            url = '<?php echo base_url() ?>index.php/superadmin/transection_data_form_date/' + st + '/' + end + '/' + $('#search_by_select').val() + '/' + $('#companyid').val();

        } else {
            url = '<?php echo base_url() ?>index.php/superadmin/search_by_select/' + $('#search_by_select').val();
        }
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
//            "sPaginationType": "bootstrap",
            "destroy": true,
            "scrollCollapse": true,
//            "oLanguage": {
//                "sLengthMenu": "_MENU_ ",
//                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
//            },
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": url,
//            "bJQueryUI": true,
//            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "oLanguage": {
                "sProcessing": "<img src='<?php echo base_url(); ?>theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {
                //oTable.fnAdjustColumnSizing();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function () {
            table.fnFilter($(this).val());
        });
         $('#payment_search').change(function () {
                        table.fnFilter($(this).val());
                    });
    }


</script>

<script type="text/javascript">
    $(document).ready(function () {


        $('.transection').addClass('active');
        $('.transection').attr('src', "<?php echo base_url(); ?>/theme/icon/accounting_on.png");
//        $('.transection .icon-thumbnail').addClass("bg-success");

        $('#datepicker-component').on('changeDate', function () {
            $(this).datepicker('hide');
        });



//        $("#datepicker1").datepicker({ minDate: 0});
        var date = new Date();
        $('#datepicker-component').datepicker({
            startDate: date
        });

        var table = $('#big_table');

        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
//            "sPaginationType": "bootstrap",
            "destroy": true,
            "scrollCollapse": true,
//            "oLanguage": {
//                "sLengthMenu": "_MENU_ ",
//                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
//            },
            "iDisplayLength": 20,
                     "aoColumns": [
                {"sWidth": "2%" ,"sClass" : "text-center"},
                {"sWidth": "2%" ,"sClass" : "text-center"},
                {"sWidth": "4%"},
                {"sWidth": "2%" ,"sClass" : "text-center"},
                {"sWidth": "4%"},
                {"sWidth": "6%"},
                {"sWidth": "4%"  ,"sClass" : "text-center"},
                {"sWidth": "5%"},
                
                {"sWidth": "5%"},
                {"sWidth": "2%" ,"sClass" : "text-center"},
                {"sWidth": "2%" ,"sClass" : "text-center"}
            ],
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url(); ?>index.php/superadmin/complete_getTransectionData',
//            "bJQueryUI": true,
//            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "oLanguage": {
                "sProcessing": "<img src='<?php echo base_url(); ?>theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {
                //oTable.fnAdjustColumnSizing();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function () {
            table.fnFilter($(this).val());
        });
         $('#payment_search').change(function () {
                        table.fnFilter($(this).val());
                    });

    }); 
</script>

<style>
    .exportOptions{
        display: none;
    }
    .table thead tr th {

        font-size: 10px;
    }
</style>

<div class="content">
    <div class="jumbotron  no-margin" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;">
                <h3 class="">Page Title</h3>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg">
        <ul class="breadcrumb">
            <li>
                <a href="#" class="active">COMPLETED JOBS</a>
            </li>
             
        </ul>
        <div class="container-md-height m-b-20">
            <div class="panel panel-transparent">
                <ul class="nav nav-tabs nav-tabs-simple bg-white" role="tablist" data-init-reponsive-tabs="collapse">
                    <li class="active">
                        <a href="#hlp_txt" data-toggle="tab" role="tab" aria-expanded="false">COMPLETED JOBS</a>
                    </li>
                </ul>
                <div class="panel-group visible-xs" id="LaCQy-accordion"></div>
                <div class="tab-content">
                    <div class="tab-pane active" id="hlp_txt">
                        <div class="row panel panel-default">
                            <div class="panel-heading">
                                 <div class="col-sm-3 no-padding">
                                <div class="" aria-required="true">

<!--                                    <div class="input-daterange input-group" id="datepicker-range">
                                        <input type="text" class="input-sm form-control" name="start" id="start" placeholder="From">
                                        <span class="input-group-addon">to</span>
                                        <input type="text" class="input-sm form-control" name="end"  id="end" placeholder="To">

                                    </div>-->
                                    
                                </div>

                            </div>
                            

                           
                            
<!--                            <div class="col-sm-1">
                                <div class="">
                                    <button class="btn btn-primary" type="button" id="searchData"><i class="fa fa-search text-white"></i> Search</button>
                                </div>
                            </div>-->

<!--                             <div class="col-sm-2">
                                <div class="" aria-required="true">

                                    <select id="payment_search" class="form-control">
                                            <option value>Payment Type</option>
                                            <option value="CASH">CASH</option>
                                            <option value="CARD">CARD</option>     
                                        </select>
                                    
                                </div>

                            </div>-->
                            <div class="row clearfix">

                                <div class="">
                                    <div class="pull-right" style="margin-right:18px;"><input type="text" id="search-table" class="form-control pull-right" placeholder="Search"> </div>

<!--                                    <div class="pull-right"> <a href="<?php echo base_url() ?>index.php/superadmin/callExel<?php echo $stdate; ?>/<?php echo $enddate ?>" id=exportformdate></a>
                                        <button class="btn btn-primary exportclick" style="margin-right:10px;"  type="submit">Export</button></a></div>-->

                                </div>
                            </div>
                               
                                    
                                </div>
                               
                                <div class="panel-body no-padding">
                                    <?php echo $this->table->generate(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 

 


<div class="modal fade stick-up" id="confirmmodels" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                <div class=" clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>

                </div>

            </div>
            <br>
            <div class="modal-body">
                <div class="row">

                    <div class="error-box" id="errorboxdatas" style="font-size: large;text-align:center"><?php echo VEHICLEMODEL_DELETE; ?></div>

                </div>
            </div>

            <br>

            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4" ></div>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4" >
                        <button type="button" class="btn btn-primary pull-right" id="confirmeds" ><?php echo BUTTON_OK; ?></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<div class="modal fade slide-up disable-scroll in" id="billed_amt" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;">
    <div class="modal-dialog " style="width: 500px">
        <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix ">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h3> BILLED AMOUNT</h3>

                <p></p>
            </div>
            <div class="modal-body">


            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade slide-up disable-scroll in" id="appdiscount" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;">
    <div class="modal-dialog ">
        <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix ">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h3> APP DISCOUNT</h3>

                <p></p>
            </div>
            <div class="modal-body">


                <div class="form-group"  id="discount" class="formex">                            
                </div>   


            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade slide-up disable-scroll in" id="coupon_dis" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;">
    <div class="modal-dialog ">
        <div class="modal-content-wrapper">
            <div class="modal-content-wrapper">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
            </button>
            <div class="container-xs-height full-height">
                <div class="row-xs-height">
                    <div class="modal-body col-xs-height coupondisData  ">

                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade slide-up disable-scroll in" id="pro_earnings" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;">
    <div class="modal-dialog ">
        <!--        <div class="modal-content">
                    <div class="modal-header clearfix ">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                        <h3> PRO EARNINGS</h3>
        
                        <p></p>
                    </div>
                    <div class="modal-body">
        
                        <div class="form-group"  id="proearnings" class="formex">                            
                        </div>   
        
        
                    </div>
                </div>-->
        <div class="modal-content-wrapper">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
            </button>
            <div class="container-xs-height full-height">
                <div class="row-xs-height">
                    <div class="modal-body col-xs-height  proearningsData ">

                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade slide-up disable-scroll in" id="app_earnings" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;">
    <div class="modal-dialog ">
        <!--        <div class="modal-content">
                    <div class="modal-header clearfix ">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                        <h3> APP EARNINGS</h3>
        
                        <p></p>
                    </div>
                    <div class="modal-body">
        
                        <div class="form-group"  id="appearnings" class="formex">                            
                        </div>   
        
        
                    </div>
                </div>-->
        <div class="modal-content-wrapper">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
            </button>
            <div class="container-xs-height full-height">
                <div class="row-xs-height">
                    <div class="modal-body col-xs-height appearningsData  ">

                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade slide-up disable-scroll in" id="pg_commission" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;">
    <div class="modal-dialog ">
        <!--        <div class="modal-content">
                    <div class="modal-header clearfix ">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                        <h3> PAYMENT GATEWAY COMISSION</h3>
        
                        <p></p>
                    </div>
                    <div class="modal-body">
        
                        <div class="form-group"  id="pgcommission" class="formex">                            
                        </div>   
        
        
                    </div>
                </div>-->
        <div class="modal-content-wrapper">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
            </button>
            <div class="container-xs-height full-height">
                <div class="row-xs-height">
                    <div class="modal-body col-xs-height pgcommissionData  ">

                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade slide-up disable-scroll in" id="billed" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;">
    <div class="modal-dialog ">
        <!--        <div class="modal-content">
                    <div class="modal-header clearfix ">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                        <h3> BILLED AMOUNT</h3>
        
                        <p></p>
                    </div>
                    <div class="modal-body">
        
                        <div class="text-left">
                            <h5><span class="semi-bold">APP BILLING</h5>
                        </div>
        
                        <div class="form-group"  id="ShowCategoryData" class="formex">                            
                        </div>   
                        <div class="text-left">
                            <h5><span class="semi-bold">Pro BILLING</h5>
                        </div>
                        <div class="form-group"  id="probill" class="formex">                            
                        </div>
        
                    </div>
                </div>-->
        <div class="modal-content-wrapper">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
            </button>
            <div class="container-xs-height full-height">
                <div class="row-xs-height">
                    <div class="modal-body col-xs-height  displayInvoiceData ">

                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    </div>
    <!-- /.modal-dialog -->
</div>