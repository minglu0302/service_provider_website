<?PHP error_reporting(e_all);
?>
<style>
    .form-horizontal .form-group 
    {
        margin-left: 13px;
    }    
</style>
<style>
    .ui-autocomplete{
        z-index: 5000;
    }
    #selectedcity,#companyid{
        display: none;
    }
    
    .ui-menu-item{cursor: pointer;background: black;color:white;border-bottom: 1px solid white;width: 200px;}
</style>
<script>
    $("#HeaderMenu").addClass("active");
    $("#HeaderMenu4").addClass("active");
    $(document).ready(function () {
//        alert('Loading');
        $('#callM').click(function () {
            $('#EditporId').val("");
            $('#Comment').val("");
            $('#Price').val("");
            $('#From').val("");
            $('#To').val("");
            $('#NewCat').modal('show');
        });

        $('#CategoryId').val('<?PHP echo $ProductDetails['CategoryId']; ?>');
        var catid = '<?PHP echo $ProductDetails['CategoryId']; ?>';
        var SubCategoryId = '<?PHP echo $ProductDetails['SubCategoryId']; ?>';

        if (SubCategoryId != '') {

            $('#SubCategory').load('<?PHP echo AjaxUrl; ?>GetSubCatfromCat', {catId: catid, SubCat: SubCategoryId});
        }
        $('#CategoryId').change(function () {


            var catId = $("#CategoryId option:selected").attr('value');
//            alert(catId);
            $('#SubCategory').load('<?PHP echo AjaxUrl; ?>GetSubCatfromCat', {catId: catId});
        });
    });
    function editMe(data) {
        var id = data.value;
        var porComment = $('#porComment' + id).val();
        var porPric = $('#porPric' + id).val();
        var porFrom = $('#porFrom' + id).val();
        var porTo = $('#porTo' + id).val();

        $('#Comment').val(porComment);
        $('#Price').val(porPric);
        $('#From').val(porFrom);
        $('#To').val(porTo);
        $('#EditporId').val(id);


        $('#NewCat').modal('show');
    }

    //submit form data from forth tab
    function submitform()
    {
        if (signatorytab('fourthlitab', 'tab4'))
        {
            $("#addentity").submit();
        }
    }

    //load mobile prefix as country code

    function fillcountrycode()
    {
        var country = $("#entitycountry").val();
        if (country !== "null")
        {
            var n = country.indexOf(",");
            $("#mobileprefix").val(country.substring((n + 1), country.length));
            $("#countrycode").val(country.substring((n + 1), country.length));
        }
    }

    //validations for each previous tab before proceeding to the next tab
    function managebuttonstate()
    {
        $("#prevbutton").addClass("hidden");
    }

    function profiletab(litabtoremove, divtabtoremove)
    {
        var reason = '';
        var pstatus = true;
        if ($("#entityname").val()=='')
        {
            pstatus = false;
        }
        if ((($("#entityVat").val()=='')))
        {
            pstatus = false;
        }
<?PHP
if ($ProfileData['ApplicableTax'] == '2') {
    ?>
            if ((($("#entityVat").val()=='')))
            {
                reason = 'Enter Applicable Vat On This Product.';
                pstatus = false;
            }
    <?PHP
}
?>
<?PHP
if ($ProfileData['ImageFlag'] == '1') {
    ?>
            if ((($(".Masterimageurl").val()=='')))
            {
                reason = 'Upload Image';
                pstatus = false;
            }
    <?PHP
}
?>
//        if (isBlank($("#entityemail").val()))
//        {
//            pstatus = false;
//        }
        if (($("#CategoryId").val()==''))
        {
            pstatus = false;
        }

//        if (isBlank($("#SubCategoryId").val()))
//        {
//            pstatus = false;
//        }


        if (pstatus === false)
        {
            setTimeout(function ()
            {
                proceed(litabtoremove, divtabtoremove, 'firstlitab', 'tab1');
            }, 300);
            if (reason != '') {
                alert(reason);
            } else {

                alert("Mandatory Fields Missing")
            }
            $("#tab1icon").removeClass("fs-14 fa fa-check");
            return false;
        }
//        alert();
        $("#tab1icon").addClass("fs-14 fa fa-check");
        $("#prevbutton").removeClass("hidden");
        $("#nextbutton").addClass("hidden");
        $("#finishbutton").removeClass("hidden");
        return true;
    }

    function addresstab(litabtoremove, divtabtoremove)
    {

        var astatus = true;
        var totl1 = $('#PortionTable11 tr').length;
//        alert(totl1);
        if (totl1 == 1) {
//            alert('Add Portion.');
            astatus = false;
        }

        //alert(profiletab());
        if (profiletab(litabtoremove, divtabtoremove))
        {
//            if ($("#entitytown").val() === "null")
//            {
//                astatus = false;
//            }
//
//            if (isBlank($("#entitypobox").val()) || isBlank($("#entityzipcode").val()))
//            {
//                astatus = false;
//            }

            if (astatus === false)
            {
                setTimeout(function ()
                {
                    proceed(litabtoremove, divtabtoremove, 'secondlitab', 'tab2');
                }, 100);
                alert("Add Portion.");
                $("#tab2icon").removeClass("fs-14 fa fa-check");
                return false;
            }
            $("#tab2icon").addClass("fs-14 fa fa-check");
            $("#nextbutton").addClass("hidden");
            $("#finishbutton").removeClass("hidden");
            return astatus;
        }
    }

    function bonafidetab(litabtoremove, divtabtoremove)
    {
        var bstatus = true;
        if (addresstab(litabtoremove, divtabtoremove))
        {
//            if (isBlank($("#entitydocname").val()) || isBlank($("#entitydocfile").val()) || isBlank($("#entityexpirydate").val()))
//            {
//                bstatus = false;
//            }

            if (bstatus === false)
            {
                setTimeout(function ()
                {
                    proceed(litabtoremove, divtabtoremove, 'thirdlitab', 'tab3');
                }, 100);
                alert("Mandatory Fields Missing");
                $("#tab3icon").removeClass("fs-14 fa fa-check");
                return false;
            }

            $("#tab3icon").addClass("fs-14 fa fa-check");
            $("#nextbutton").addClass("hidden");
            $("#finishbutton").removeClass("hidden");
            return bstatus;
        }
    }

    function signatorytab(litabtoremove, divtabtoremove)
    {
        var bstatus = true;
        if (bonafidetab(litabtoremove, divtabtoremove))
        {
//            if (isBlank($("#entitypersonname").val()) || isBlank($("#entitysignatorymobileno").val()) || isBlank($("#entitysignatoryfile").val()) || $("#entitydegination").val() === "null")
//            {
//                bstatus = false;
//            }
//
//            if (validateEmail($("#entitysignatoryemail").val()) !== 2)
//            {
//                bstatus = false;
//            }

            if (bstatus === false)
            {
                setTimeout(function ()
                {
                    proceed(litabtoremove, divtabtoremove, 'fourthlitab', 'tab4');
                }, 100);
                alert("Mandatory Fields Missing");
                $("#tab4icon").removeClass("fs-14 fa fa-check");
                return false;
            }

            $("#tab4icon").addClass("fs-14 fa fa-check");
            $("#nextbutton").addClass("hidden");
            $("#finishbutton").removeClass("hidden");
            return bstatus;
        }

    }


    function proceed(litabtoremove, divtabtoremove, litabtoadd, divtabtoadd)
    {
        $("#" + litabtoremove).removeClass("active");
        $("#" + divtabtoremove).removeClass("active");
        $("#" + litabtoadd).addClass("active");
        $("#" + divtabtoadd).addClass("active");
    }

    /*-----managing direct click on tab is over -----*/

    //manage next next and finish button
    function movetonext()
    {
//        var currenttabstatus = $("li.active").attr('id');
        if ($("#firstlitab").attr('class') === "active")
        {
            profiletab('secondlitab', 'tab2');
            proceed('firstlitab', 'tab1', 'secondlitab', 'tab2');
        }
        else if ($("#secondlitab").attr('class') === "active")
        {
            addresstab('thirdlitab', 'tab3');
            proceed('secondlitab', 'tab2', 'thirdlitab', 'tab3');
        }

        else if ($("#thirdlitab").attr('class') === "active")
        {
            bonafidetab('fourthlitab', 'tab4');
            proceed('thirdlitab', 'tab3', 'fourthlitab', 'tab4');
            $("#finishbutton").removeClass("hidden");
            $("#nextbutton").addClass("hidden");
        }
    }

    function movetoprevious()
    {
        var currenttabstatus = $("li.active").attr('id');
        if ($("#secondlitab").attr('class') === "active")
        {
            profiletab('secondlitab', 'tab2');
            proceed('secondlitab', 'tab2', 'firstlitab', 'tab1');
            $("#prevbutton").addClass("hidden");
        }
        else if ($("#thirdlitab").attr('class') === "active")
        {
            addresstab('thirdlitab', 'tab3');
            proceed('thirdlitab', 'tab3', 'secondlitab', 'tab2');
        }
        else if ($("#fourthlitab").attr('class') === "active")
        {
            bonafidetab('fourthlitab', 'tab4');
            proceed('fourthlitab', 'tab4', 'thirdlitab', 'tab3');
            $("#nextbutton").removeClass("hidden");
            $("#finishbutton").addClass("hidden");
        }
    }
    function DelRows(thisval) {
        $('#deletemodal').modal('show');
        var entityidid = thisval.id;
        $('.deletoption').val(entityidid);
    }

    function Delete() {
        var Delid = $('.deletoption').val();
        $('#Row' + Delid).remove();
        $('#deletemodal').modal('hide');
    }

</script>




<div class="page-content-wrapper">
    <!-- START PAGE CONTENT -->
    <div class="content">
        <!-- START JUMBOTRON -->
        <div class="jumbotron bg-white" data-pages="parallax">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb" style="margin-left: 20px;">

                    <li><a href="<?php echo base_url() ?>index.php/superadmin/catogiries_services/3">SERVICES</a>
                    </li>

                    <li style="width: 100px"><a href="#" class="active">Add New</a>
                    </li>
                </ul>
                <!-- END BREADCRUMB -->
            </div>



            <div class="container-fluid container-fixed-lg bg-white">

                <div id="rootwizard" class="m-t-50">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-tabs-linetriangle nav-tabs-separator nav-stack-sm" id="mytabs">
                        <li class="active" id="firstlitab" onclick="managebuttonstate()">
                            <a data-toggle="tab" href="#tab1" id="tb1"><i id="tab1icon" class=""></i> <span>Details</span></a>
                        </li>
                        <li class="" id="secondlitab">
                            <a data-toggle="tab" href="#tab2" onclick="profiletab('secondlitab', 'tab2')" id="mtab2"><i id="tab2icon" class=""></i> <span>Pricing</span></a>
                        </li>

                    </ul>
                    <!-- Tab panes -->
                    <form id="addentity" class="form-horizontal" role="form" action="<?php echo base_url() ?>index.php/superadmin/AddnewProduct" method="post" enctype="multipart/form-data">
                        <input type='hidden' value='<?PHP echo $ProductId; ?>' name='ProductId'>

                        <?PHP
                        if ((string) $ProductDetails['_id'] == '') {
                            echo " <input type='hidden' value='" . $count . "' name='FData[count]'>";
                        }
                        ?>

                        <div class="tab-content">
                            <div class="tab-pane padding-20 slide-left active" id="tab1">
                                <div class="row row-same-height">

                                    <div class="form-group">
                                        <label for="fname" class="control-label">Product Image</label>
                                        <div class="" >
                                            <a onclick="openFileUpload(this)" id='1' style="cursor: pointer;">
                                                <div class="portfolio-group">
                                                    <?PHP
                                                    if ($ProductDetails['Masterimageurl']['Url'] == '') {
                                                        echo '  <img src="' . UrlBase . 'Business/addnew.png" id="MainImageUrl" style="width: 20%;height: 160px;">';
                                                    } else {
                                                        echo '  <img src="' . $ProductDetails['Masterimageurl']['Url'] . '" id="MainImageUrl" style="width: 20%;height: 160px;">';
                                                    }
                                                    ?>

                                                </div>
                                            </a>                                                                    
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="fname" class="col-sm-3 control-label">Name</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="entityname" placeholder="Product Name" name="FData[Name]"  aria-required="true" value='<?PHP echo $ProductDetails['Name']; ?>'>
                                        </div>
                                        <div class="col-sm-1">
                                            <span style="color: red; font-size: 20px">*</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="fname" class="col-sm-3 control-label">Description</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="entityemail" placeholder="Product Description" name="FData[Description]"  aria-required="true" value='<?PHP echo $ProductDetails['Description']; ?>'>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label for="fname" class="col-sm-3 control-label">Category</label>
                                        <div class="col-sm-6">
                                            <select class="form-control" id="CategoryId" name="FData[CategoryId]" required>
                                                <option value="">Select Category</option>
                                                <?PHP
                                                foreach ($AllCats as $Cats) {
                                                    echo '<option value = "' . (string) $Cats['_id'] . '">' . $Cats['Category'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-1">
                                            <span style="color: red; font-size: 20px">*</span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="tab-pane slide-left padding-20" id="tab2">
                                <div class="row row-same-height">
                                    <button type="button" class="btn btn-default" id="callM">Add new</button>
                                    <div id="tableWithSearch_wrapper" class="dataTables_wrapper form-inline no-footer">
                                        <div class="table-responsive">
                                            <table class="table table-hover demo-table-search dataTable no-footer" id="PortionTable111" role="grid" aria-describedby="tableWithSearch_info">
                                                <thead>

                                                    <tr role="row">

                                                        <th class="sorting_asc" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Title: activate to sort column ascending" style="width: 247px;">
                                                            From </th>
                                                        <th class="sorting_asc" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Title: activate to sort column ascending" style="width: 247px;">
                                                            To </th>
                                                        <th class="sorting_asc" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Title: activate to sort column ascending" style="width: 247px;">
                                                           Comments</th>
                                                        <th class="sorting_asc" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Title: activate to sort column ascending" style="width: 247px;">
                                                             Price(<?PHP echo $this->session->userdata('Currency'); ?>) </th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Last Update: activate to sort column ascending" style="width: 232px;">
                                                            Option</th>
                                                    </tr>

                                                </thead>
                                                <tbody>

                                                    <?PHP
                                                    $Poid = 0;
                                                    if (is_array($ProductDetails['Portion'])) {

                                                        foreach ($ProductDetails['Portion'] as $portion) {
                                                            ?>
                                                            <tr id='Row<?PHP echo $Poid; ?>'>
                                                                <td> <label id="LabelFrom<?PHP echo $Poid; ?>"><?PHP echo $portion['From']; ?></label></td>
                                                                <td> <label id="LabelTo<?PHP echo $Poid; ?>"><?PHP echo $portion['To']; ?></label></td>
                                                                <td> <label id="LabelPric<?PHP echo $Poid; ?>"><?PHP echo $portion['price']; ?></label></td>
                                                                <td> <label id="LabelComment<?PHP echo $Poid; ?>"><?PHP echo $portion['Comment']; ?></label></td>
                                                        <input id="porTo<?PHP echo $Poid; ?>" type="hidden" name="FData[Portion][<?PHP echo $Poid; ?>][To]" value="<?PHP echo $portion['To']; ?>">
                                                        <input id="porPric<?PHP echo $Poid; ?>" type="hidden" name="FData[Portion][<?PHP echo $Poid; ?>][price]" value="<?PHP echo $portion['price']; ?>">
                                                        <input id="porFrom<?PHP echo $Poid; ?>" type="hidden" name="FData[Portion][<?PHP echo $Poid; ?>][From]" value="<?PHP echo $portion['From']; ?>">
                                                        <input id="porComment<?PHP echo $Poid; ?>" type="hidden" name="FData[Portion][<?PHP echo $Poid; ?>][Comment]" value="<?PHP echo $portion['Comment']; ?>">
                                                        <input id="porId<?PHP echo $Poid; ?>" type="hidden" name="FData[Portion][<?PHP echo $Poid; ?>][id]" value="<?PHP echo $portion['id']; ?>">

                                                        <td  class="v-align-middle">
                                                            <div class="btn-group">
                                                                <a><button type="button" onclick="editMe(this);"   value="<?PHP echo $Poid; ?>" type="button" style="color: #ffffff !important;background-color: #10cfbd;" class="btn btn-success"><i class="fa fa-pencil"></i>
                                                                    </button></a>
                                                                <a><button type="button" onclick="DelRows(this)" id="<?PHP echo $Poid; ?>" class="btn btn-success" style="color: #ffffff !important;background-color: #10cfbd;"><i class="fa fa-trash-o"></i>
                                                                    </button></a>
                                                            </div>
                                                        </td></tr>
                                                        <?PHP
                                                        $Poid++;
                                                    }
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <script>
                                var UrlId = 0;
                                $(document).ready(function () {
                                    $('#uploadFile').change(function () {

                                        var imageid = $('#imageId').val();
                                        if (imageid == '0') {

                                        } else {
                                            UrlId = imageid;
                                        }
//                                        alert(UrlId);
                                        var formElement = document.getElementById("addentity");
                                        var formData = new FormData(formElement);
                                        var imgUrl = '';
                                        $.ajax({
                                            url: "<?PHP echo ImageAjaxUrl; ?>/upload_file.php",
                                            type: "POST",
                                            data: formData,
                                            dataType: "JSON",
                                            async: false,
                                            success: function (result) {
                                                if (result.msg == '1') {
                                                    imgUrl = "https://s3.amazonaws.com/" + result.fileName;
                                                    $("#MultipleImages").append('<div class="col-md-2" id="Img' + UrlId + '"><input type = "hidden" id = "image" name = "FData[Images][' + UrlId + '][Url]" value = "' + imgUrl + '"><img src="' + imgUrl + '" alt="image 1" style="width: 100%;height: 160px;"><div style="position:absolute;top:0;right:0px;"><a onclick="delete1(this)" id="' + UrlId + '"  value="" ><img  class="thumb_image" src="http://108.166.190.172:81/SteinGlass/assets/newui/dialog_close_button.png" height="20px" width="20px" /></a></div></div>');
                                                    UrlId++;
                                                    $('#imageId').val(UrlId);
                                                } else {
                                                    alert('Problem In Uploading Image-' + result.folder);
                                                    $('#iimg').hide();
                                                }
                                            },
                                            cache: false,
                                            contentType: false,
                                            processData: false
                                        });
                                    });
                                    $('#uploadMain').change(function () {
//                                        alert();
                                        var formElement = document.getElementById("addentity");
                                        var formData = new FormData(formElement);
                                        var imgUrl = '';
                                        $.ajax({
                                            url: "<?PHP echo ImageAjaxUrl; ?>/upload_main_file.php",
                                            type: "POST",
                                            data: formData,
                                            dataType: "JSON",
                                            async: false,
                                            success: function (result) {
//                                                alert();
                                                if (result.msg == '1') {
                                                    imgUrl = "https://s3.amazonaws.com/" + result.fileName;
//                                                    $("#MainImageUrl").append('<div class="col-md-2" id="Img' + UrlId + '"><input type = "hidden" id = "image" name = "FData[Images][' + UrlId + '][Url]" value = "' + imgUrl + '"><img src="' + imgUrl + '" alt="image 1" style="width: 100%;height: 160px;"><div style="position:absolute;top:0;right:0px;"><a onclick="delete1(this)" id="' + UrlId + '"  value="" ><img  class="thumb_image" src="http://108.166.190.172:81/SteinGlass/assets/newui/dialog_close_button.png" height="20px" width="20px" /></a></div></div>');
                                                    $('.Masterimageurl').val(imgUrl);
                                                    $('.masterImageHeight').val(result.Height);
                                                    $('.masterImageWidth').val(result.Width);
                                                    $('#MainImageUrl').attr('src', imgUrl);
                                                } else {
                                                    alert('Problem In Uploading Image-' + result.folder);
                                                    $('#iimg').hide();
                                                }
                                            },
                                            cache: false,
                                            contentType: false,
                                            processData: false
                                        });
                                    });
                                });
                                function openFileUpload(check)
                                {
                                    if (check.id == '1') {
                                        $('#uploadMain').trigger('click');
                                    } else {
                                        $('#uploadFile').trigger('click');
                                    }
//                                    $('#iimg').show();

//                                    $('#iimg').hide();


                                }

                                function delete1(val) {
                                    var ii = val.id;
                                    $('#Img' + ii).remove();
//                                    $('#SaveImages').load('<?PHP echo AjaxUrl; ?>DeleteImage.php', {ImageId: ii});
                                }

                            </script>
                            <input id="uploadMain" type="file" name="myMainfile"  style="visibility: hidden;"/>
                            <input type='hidden' class='Masterimageurl' name='FData[Masterimageurl][Url]' value="<?PHP echo $ProductDetails['Masterimageurl'][Url]; ?>">
                            <input type='hidden' class='masterImageHeight' name='FData[Masterimageurl][Height]' value="<?PHP echo $ProductDetails['Masterimageurl'][Height]; ?>">
                            <input type='hidden' class='masterImageWidth' name='FData[Masterimageurl][Width]' value="<?PHP echo $ProductDetails['Masterimageurl'][Width]; ?>">



                        </div>
                        <div class="padding-20 bg-white">
                            <ul class="pager wizard">
                                <li class="next" id="nextbutton">
                                    <button class="btn btn-primary btn-cons pull-right" type="button" onclick="movetonext()" >
                                        <span>Next</span>
                                    </button>
                                </li>
                                <li class="hidden" id="finishbutton">
                                    <button class="btn btn-primary btn-cons pull-right" type="button" onclick="submitform()" >
                                        <span>Finish</span>
                                    </button>
                                </li>

                                <li class="previous hidden" id="prevbutton">
                                    <button class="btn btn-default btn-cons pull-right" type="button" onclick="movetoprevious()">
                                        <span>Previous</span>
                                    </button>
                                </li>
                            </ul>
                        </div>


                    </form>
                </div>    

            </div>


        </div>
        <!-- END PANEL -->
    </div>

</div>
<!-- END JUMBOTRON -->

<!-- START CONTAINER FLUID -->
<div class="container-fluid container-fixed-lg">
    <!-- BEGIN PlACE PAGE CONTENT HERE -->

    <!-- END PLACE PAGE CONTENT HERE -->
</div>
<!-- END CONTAINER FLUID -->

</div>
<!-- END PAGE CONTENT -->
<!-- START FOOTER -->
<!--<div class="container-fluid container-fixed-lg footer">
    <div class="copyright sm-text-center">
        <p class="small no-margin pull-left sm-pull-reset">
            <span class="hint-text">Copyright © 2014</span>
            <span class="font-montserrat">REVOX</span>.
            <span class="hint-text">All rights reserved.</span>
            <span class="sm-block"><a href="#" class="m-l-10 m-r-10">Terms of use</a> | <a href="#" class="m-l-10">Privacy Policy</a>
            </span>
        </p>
        <p class="small no-margin pull-right sm-pull-reset">
            <a href="#">Hand-crafted</a>
            <span class="hint-text">&amp; Made with Love ®</span>
        </p>
        <div class="clearfix"></div>
    </div>
</div>-->
<!-- END FOOTER -->
<?PHP
$con = new Mongo();
?>
<script>
    var count = 0;
    function AddPortion()
    {

        var Comment = $('#Comment').val();
        var price = $('#Price').val();
        var To = $('#To').val();
        var From = $('#From').val();
        if (From == '') {
            alert('Please Enter From.');
        }
        if (To == '') {
            alert('Please Enter To.');
        }
        if (Comment == '') {
            alert('Please Enter Comment.');
        }
        if (price == '') {
            alert('Please Enter Price.');
        }
        if ($('#EditporId').val() != '') {
            $('#porComment' + $('#EditporId').val()).val(Comment);
            $('#porPric' + $('#EditporId').val()).val(price);
            $('#porFrom' + $('#EditporId').val()).val(From);
            $('#porTo' + $('#EditporId').val()).val(To);


            $('#LabelComment' + $('#EditporId').val()).text(Comment);
            $('#LabelPric' + $('#EditporId').val()).text(price);
            $('#LabelFrom' + $('#EditporId').val()).text(From);
            $('#LabelTo' + $('#EditporId').val()).text(To);


        } else {
//            alert();
            var id = Math.floor((Math.random() * 10000) + 1);


            var totl = '<?PHP echo $Poid; ?>';
            if (count > totl) {

            } else {
                count = totl;
            }
            $('#PortionTable111').append('<tr id="Row' + count + '">' +
                    '<td> <label id="LabelFrom' + count + '">' + From + '</label></td>' +
                    '<td> <label id="LabelTo' + count + '">' + To + '</label></td>' +
                    '<td> <label id="LabelComment' + count + '">' + Comment + '</label></td>' +
                    '<td> <label id="LabelPric' + count + '">' + price + '</label></td>' +
                    '<input id="porComment ' + count + '" type="hidden" name="FData[Portion][' + count + '][Comment]" value="' + Comment + '">' +
                    '<input id="porFrom' + count + '" type="hidden" name="FData[Portion][' + count + '][From]" value="' + From + '">' +
                    '<input id="porTo' + count + '" type="hidden" name="FData[Portion][' + count + '][To]" value="' + To + '">' +
                    '<input id="porPric' + count + '" type="hidden" name="FData[Portion][' + count + '][price]" value="' + price + '">' +
                    '<input id="porId' + count + '" type="hidden" name="FData[Portion][' + count + '][id]" value="' + id + '">' +
                    ' <td  class="v-align-middle">' +
                    ' <div class="btn-group">' +
                    '<a><button onclick="editMe(this);" value="' + count + '" type="button" style="color: #ffffff !important;background-color: #10cfbd;" class="btn btn-success"><i class="fa fa-pencil"></i>' +
                    '  </button></a>' +
                    '  <a><button type="button" onclick="DelRows(this)" id="' + count + '" class="btn btn-success" style="color: #ffffff !important;background-color: #10cfbd;"><i class="fa fa-trash-o"></i>' +
                    '   </button></a>' +
                    ' </div>' +
                    '</td></tr>');
            count++;
        }
        $('#NewCat').modal('hide');

    }
    function validate(key) {
        //getting key code of pressed key
        var keycode = (key.which) ? key.which : key.keyCode;
        // var tex = document.getElementById('TextBox2');
        //comparing pressed keycodes
        if (!(keycode == 8 || keycode == 46) && (keycode < 48 || keycode > 57)) {
            return false;
        }
        else {

        }
    }

</script>

<div class="modal fade in" id="NewCat" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="">
    <div class="modal-dialog">

        <form action = "<?php echo base_url(); ?>index.php/Admin/AddNewSubCategory" method= "post" onsubmit="return validateForm
                        ();">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add New Portion</h4>
                </div>

                <div class="modal-body">

                    <div class="form-group" >
                        <label>From</label>
                        <input type="text" class="form-control" placeholder="From" id="From" name="From">
                    </div>
                    <div class="form-group" >
                        <label>To</label>
                        <input type="text" class="form-control" placeholder="To" id="To" name="To">
                    </div>
                    <div class="form-group" >
                        <label>Price</label>
                        <div class="input-group transparent">
                            <span class="input-group-addon">
                                <i><?PHP echo $this->session->userdata('Currency'); ?></i>
                            </span>
                            <input type="text" class="form-control" onkeypress="return validate(event)" placeholder="Price" id="Price" name="Price">
                        </div>

                    </div>
                    <div class="form-group" >
                        <label>Comment</label>
                        <input type="text" class="form-control" placeholder="Comment" id="Comment" name="Comment">
                    </div>
                    <input id="EditporId" type="hidden" value="">


                    <label id = "errorbox" style="color: red; font-size: 15px;"></label>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="button" class="btn btn-primary" value="Add" onclick="AddPortion();">

                </div>

        </form>
    </div>
</div>

</div>
<div class="modal fade stick-up in" id="deletemodal" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Are You Sure To Delete This Portion ? </h5>
                </div>
                <div class="modal-body">

                </div>
                <input type='hidden' class='deletoption'>
                <div class="modal-footer">


                    <a id="deletelink"><button onclick='Delete(this);' type="button" class="btn btn-primary btn-cons  pull-left inline">Continue</button></a>
                    <button type="button" class="btn btn-default btn-cons no-margin pull-left inline" data-dismiss="modal">Cancel</button>

                </div>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
