<?php
date_default_timezone_set('UTC');
$rupee = "$";
?>
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<style>
    .ui-autocomplete{
        z-index: 5000;
    }
    #selectedcity,#companyid{
        display: none;
    }
    th { font-size: 12px; }
    .ui-menu-item{cursor: pointer;background: black;color:white;border-bottom: 1px solid white;width: 200px;}
</style>
<script>
    $(document).ready(function () {
        $('.cities').addClass('active');
        $('.cities_thumb').attr('src', "<?php echo base_url(); ?>/theme/icon/cities_on.png");

        $('.error-box-class').keypress(function () {
            $('.error-box').text('');
        });
        $('#btnStickUpSizeToggler').click(function () {
            $("#display-data").text("");
            var size = $('input[name=stickup_toggler]:checked').val()
            var modalElem = $('#myModal');
            if (size == "mini") {
                $('#modalStickUpSmall').modal('show')
            } else {
                $('#myModal').modal('show')
                if (size == "default") {
                    modalElem.children('.modal-dialog').removeClass('modal-lg');
                } else if (size == "full") {
                    modalElem.children('.modal-dialog').addClass('modal-lg');
                }
            }
        });

        $('.changeMode').click(function () {
            var table = $('#big_table');
            var settings = {
                "autoWidth": false,
                "sDom": "<'table-responsive't><'row'<p i>>",
                "destroy": true,
                "scrollCollapse": true,
                "iDisplayLength": 20,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "<?php echo base_url() ?>index.php/superadmin/datatable_oprdetails/<?php echo $oprid ?>",
                "oLanguage": {
                    "sProcessing": "<img src='http://107.170.66.211/roadyo_live/sadmin/theme/assets/img/ajax-loader_dark.gif'>"
                },
                "fnInitComplete": function () {
                },
                'fnServerData': function (sSource, aoData, fnCallback)
                {
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });
                },
            };
            table.dataTable(settings);

            // search box for table
            $('#search-table').keyup(function () {
                table.fnFilter($(this).val());
            });

        });
    });

</script>

<script type="text/javascript">
    $(document).ready(function () {

        var table = $('#big_table');

        $('#big_table_processing').show();
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url() ?>index.php/superadmin/datatable_oprdetails/<?php echo $oprid ?>',
//            "bJQueryUI": true,
//            "sPaginationType": "full_numbers",
//            "iDisplayStart ": 20,
            "oLanguage": {
                "sProcessing": "<img src='http://107.170.66.211/roadyo_live/sadmin/theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {
                //oTable.fnAdjustColumnSizing();
                $('#big_table_processing').hide();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };



        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function () {
            table.fnFilter($(this).val());
        });

    });
</script>

<style>
    .exportOptions{
        display: none;
    }
</style>
<div class="page-content-wrapper" style="padding-top: 20px">
    <!-- START PAGE CONTENT -->
    <div class="content">
        <!-- START JUMBOTRON -->
        <div class="inner">
            <ul class="breadcrumb" style="margin-left: 20px;font-size: 16px;color:#0090d9;">
                <li><a style="color:#0090d9;" href="<?php echo base_url(); ?>index.php/superadmin/operator/" class="">OPERATOR</a>
                </li><li><a style="color:#0090d9;" href="#" class=""><?php echo $operator_name ?></a></li>
            </ul>
        </div>

        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <div class="panel panel-transparent ">
                    <ul class="nav nav-tabs nav-tabs-fillup  bg-white">
                        <li class="active" style="cursor:auto">
                            <a><span style="cursor:auto">LIST OF PROVIDERS</span></a>
                        </li>
                        
                    </ul>

                    <div class="container-fluid container-fixed-lg bg-white no-padding">
                        <!-- START PANEL -->
                        <div class="panel panel-transparent no-padding">
                            <div class="panel-heading no-padding">
                                <div class="error-box" id="display-data" style="text-align:center"></div>
                                <div id="big_table_processing" class="dataTables_processing" style=""><img src="http://www.ahmed-samy.com/demos/datatables_2/assets/images/ajax-loader_dark.gif"></div>
                                <div class="pull-right m-t-10" class="btn-group" style="margin-right: 4px">
                                    <div class="m-t-10 pull-right" class="btn-group searchbtn" style="margin-left: 5px">
                                        <input type="text" id="search-table" class="form-control pull-right"  placeholder="<?php echo SEARCH; ?>"/> 
                                    </div>
                                </div>
                                &nbsp;
                                <div class="panel-body p-r-20  p-l-20  p-t-30">
                                    <?php echo $this->table->generate(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>