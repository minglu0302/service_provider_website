<?php
$this->load->database();
?>
<style>
    .form-horizontal .form-group
    {
        margin-left: 13px;
    }
    #selectedcity,#companyid{
        display: none;
    }

</style>




<script>
function showprice(ob) {
            if (ob.checked) {
                $("#later_booking_direct_accepted").val(1);
            } else {
                $("#later_booking_direct_accepted").val(0);
            }
        }

    $(document).ready(function () {
        if("<?php echo $dispute['later_booking_direct_accepted']; ?>" == 1){
            $("#shipment").trigger("click");
        }

        $('.cities').addClass('active');
//        $('.cities').attr('<?php echo base_url();?>/theme/icon/cities_on.png"');
        $('.cities_thumb').attr('src',"<?php echo base_url();?>/theme/icon/cities_on.png");

        $("#ex").click(function () {
             var booking_second = $("#booking_second").val();
             var booking_seconds_later = $("#booking_seconds_later").val();
             var expire_now = $("#expire_now").val();
             var expire_later = $("#expire_later").val();
             var apikey1 = $("#apikey1").val();
             var apikey2 = $("#apikey2").val();
             var apikey3 = $("#apikey3").val();
             var apikey4 = $("#apikey4").val();
             var apikey5 = $("#apikey5").val();
             var apikey6 = $("#apikey6").val();
             var apikey7 = $("#apikey7").val();
             var apikey8 = $("#apikey8").val();
             var apikey9 = $("#apikey9").val();
             var apikey10 = $("#apikey10").val();
             var apikey11 = $("#apikey11").val();
             var apikey12 = $("#apikey12").val();
             var pt = $("#pt").val();
             var pul = $("#pul").val();
             
             var ios_pro_app_version = $("#ios_pro_app_version").val();
             var ios_cust_app_version = $("#ios_cust_app_version").val();
             var and_pro_app_version = $("#and_pro_app_version").val();
             var and_cust_app_version = $("#and_cust_app_version").val();
             var later_booking_direct_accepted = $("#later_booking_direct_accepted").val();
            
            
             if(booking_second == '' || booking_second == null)
             {
                 $("#email1_error").text("Please enter dispute email address");
             }
             
             else {
            $.ajax({
                    type: 'post',
                    url: "<?php echo base_url('index.php/superadmin') ?>/savedispute_confing",
                    data: {
                        ios_pro_app_version:ios_pro_app_version,
                        ios_cust_app_version:ios_cust_app_version,
                        and_pro_app_version:and_pro_app_version,
                        and_cust_app_version:and_cust_app_version,
                
                        booking_second:booking_second,
                        booking_seconds_later:booking_seconds_later,
                        expire_now:expire_now,
                        expire_later:expire_later,
                        later_booking_direct_accepted:later_booking_direct_accepted,
                       apikey1:apikey1,
                       apikey2:apikey2,
                       apikey3:apikey3,
                       apikey4:apikey4,
                       apikey5:apikey5,
                       apikey6:apikey6,
                       apikey7:apikey7,
                       apikey8:apikey8,
                       apikey9:apikey9,
                       apikey10:apikey10,
                       apikey11:apikey11,
                       apikey12:apikey12,
                       pt:pt,
                       pul:pul
                    },
                    dataType: "json",
                    success: function (response) {
                        var size = $('input[name=stickup_toggler]:checked').val()
                        var modalElem = $('#confirmmodels');
                        if (size == "mini")
                        {
                            $('#modalStickUpSmall').modal('show')
                        }
                        else
                        {
                            $('#confirmmodels').modal('show')
                            if (size == "default") {
                                modalElem.children('.modal-dialog').removeClass('modal-lg');
                            }
                            else if (size == "full") {
                                modalElem.children('.modal-dialog').addClass('modal-lg');
                            }
                        }

                        if (response.flag == 1)
                            $("#errorboxdatas").text(response.msg);
                        else
                            $("#errorboxdatas").text(<?php echo json_encode(POPUP_COUNTRY_ADDED); ?>);

                        $("#confirmeds").hide();

               
                        
                    },
                });

            }

        });


        $('.error-box-class').keypress(function () {
            $('.error-box').text('');
        });



      


         

    });

 



</script>




<div class="page-content-wrapper">
    <!-- START PAGE CONTENT -->
    <div class="content">
        <div class="inner">
            <!-- START BREADCRUMB -->
            <ul class="breadcrumb" style="margin-top: 20px;margin-left: 20px;font-size: 16px;color:#0090d9;">
                <li><a href="cities" class="">Configuration Setting</a>
                </li>


            </ul>


            <!-- END BREADCRUMB -->
        </div>
        <!-- START JUMBOTRON -->
        <div class="jumbotron bg-white" data-pages="parallax">
 
            <div class="container-fluid container-fixed-lg bg-white">


                <div class="panel-body">
                    <form id="addentity" class="form-horizontal" role="form"   enctype="multipart/form-data">

                        <div class="tab-content">
                            <div class="tab-pane padding-20 slide-left active" id="tab1">
                                <div class="row">
                                <div class="col-sm-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                Google Server Keys
                                            </div>
                                            <div class="panel-body">
                                                <!--<table>-->
                                                <?php
                                                for ($i = 0; $i < 12; $i++) {
                                                    $j =$i+1;
                                                        if(($i-1) % 3 == 0){
//                                                            echo '<tr>';
                                                        }
                                                    ?>
                                                    <td>
                                                    <div class="col-sm-4 form-group-attached m-b-15">
                                                        <div class="form-group input-group form-group-default">
                                                            <input <?= ($i < 2) ? "required" : "" ?> type="text" name='<?php echo $dispute['apikey'.$j]; ?>' id="apikey<?php echo $j; ?>" class="form-control" value="<?php echo $dispute['apikey'.$j]; ?>" style="height: calc(50px);">
                                                            <span class="input-group-addon"><?= $i + 1 ?></span>
                                                        </div>
                                                    </div>
                                                    </td>
                                                    <?php if($i % 3 == 0){
//                                                        echo '</tr>';
                                                    }
                                                }
                                                ?>
                                                <!--</table>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <div class="row">
                                <div class="col-sm-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                Required App Version
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="address" class="col-sm-5 control-label">Android Customer Ver.<span style="color:red;font-size: 18px">*</span></label>
                                                        <div class="col-sm-4">
                                                            <input type="text" name='and_cust_app_version' id="and_cust_app_version" class="form-control" value="<?php echo $dispute['and_cust_app_version']; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="address" class="col-sm-5 control-label">Android Provider Ver.<span style="color:red;font-size: 18px">*</span></label>
                                                        <div class="col-sm-4">
                                                            <input type="text" name='and_pro_app_version' id="and_pro_app_version" class="form-control" value="<?php echo $dispute['and_pro_app_version']; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="address" class="col-sm-5 control-label">iOS Customer Ver.<span style="color:red;font-size: 18px">*</span></label>
                                                        <div class="col-sm-4">
                                                            <input type="text" name='ios_cust_app_version' id="ios_cust_app_version" class="form-control" value="<?php echo $dispute['ios_cust_app_version']; ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="address" class="col-sm-5 control-label">iOS Provider Ver.<span style="color:red;font-size: 18px">*</span></label>
                                                        <div class="col-sm-4">
                                                            <input type="text" name='ios_pro_app_version' id="ios_pro_app_version" class="form-control" value="<?php echo $dispute['ios_pro_app_version']; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <div class="row">
                                <div class="col-sm-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                Booking Display Time
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="address" class="col-sm-5 control-label">Now Booking Second<span style="color:red;font-size: 18px">*</span></label>
                                                        <div class="col-sm-4">
                                                            <input type="text"   id="booking_second" name="booking_second" class="form-control error-box-class" value="<?php echo $dispute['booking_seconds']; ?>">
                                                        </div>
                                                        <div class="col-sm-3 error-box" id="email1_error"></div>
                                                    </div>

                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="address" class="col-sm-5 control-label">Later Booking Second<span style="color:red;font-size: 18px">*</span></label>
                                                        <div class="col-sm-4">
                                                            <input type="text"   id="booking_seconds_later" name="booking_seconds_later" class="form-control error-box-class" value="<?php echo $dispute['booking_seconds_later']; ?>">
                                                        </div>
                                                        <div class="col-sm-3 error-box" id="email1_error"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="address" class="col-sm-5 control-label">Now Booking Expire Before Minutes<span style="color:red;font-size: 18px">*</span></label>
                                                        <div class="col-sm-4">
                                                            <input type="text"   id="expire_now" name="expire_now" class="form-control error-box-class" value="<?php echo $dispute['expire_now']; ?>">
                                                        </div>
                                                        <div class="col-sm-3 error-box" id="email1_error"></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="address" class="col-sm-5 control-label">Later Booking  Expire Before Minutes <span style="color:red;font-size: 18px">*</span></label>
                                                        <div class="col-sm-4">
                                                            <input type="text"  id="expire_later" name="expire_later" class="form-control error-box-class" value="<?php echo $dispute['expire_later']; ?>">
                                                        </div>
                                                        <div class="col-sm-3 error-box" id="email1_error"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" id="later_booking_direct_accepted" value="<?php echo $dispute['later_booking_direct_accepted']; ?>" name="later_booking_direct_accepted">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="address" class="col-sm-5 control-label">Later Booking Direct Accept <span style="color:red;font-size: 18px">*</span></label>
                                                        <!--<label for="shipment" class="control-label">SHIPMENT</label>-->
                                                        <div class="col-sm-1">
                                                            <span class="input-group-addon bg-transparent">
                                                                <input value="<?php echo $dispute['later_booking_direct_accepted']; ?>" onchange="showprice(this)" type="checkbox" data-init-plugin="switchery" data-size="small" data-color="primary" name="shipmentcb" id="shipment" data-switchery="true">
                                                            </span>
                                                        </div>
                                                        <div class="col-sm-3 error-box" id="email1_error"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                
                                 <div class="row">
                                <div class="col-sm-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
<!--                                                Required App Version-->
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="address" class="col-sm-5 control-label">Provider Timeout <span style="color:red;font-size: 18px">*</span></label>
                                                        <div class="col-sm-4">
                                                            <input type="text" name='pt' id="pt" class="form-control" value="<?php echo $dispute['provider_timeout']; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="address" class="col-sm-5 control-label">Provider update location<span style="color:red;font-size: 18px">*</span></label>
                                                        <div class="col-sm-4">
                                                            <input type="text" name='pul' id="pul" class="form-control" value="<?php echo $dispute['provider_update_location']; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <div class="row row-same-height">
                                    

                                                                        
                                    
                                    
                                      
                                    


                                    <div>

                                        <div class="pull-right m-t-10"> <button id="ex" type="button" class="btn btn-primary btn-cons">SAVE</button></div>
                                    </div>



                                </div>
                            </div>

                        </div>

                        <div>


                        </div>
                </div>

            </div>

        </div>


    </div>





<!-- END PANEL -->





 



<div class="modal fade stick-up" id="confirmmodels" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                <div class=" clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>

                </div>

            </div>
            <br>
            <div class="modal-body">
                <div class="row">

                    <div class="error-box" id="errorboxdatas" style="font-size: large;text-align:center"><?php echo VEHICLEMODEL_DELETE; ?></div>

                </div>
            </div>

            <br>

            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4" ></div>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4" >
                        <button type="button" class="btn btn-primary pull-right" id="confirmeds" ><?php echo BUTTON_YES; ?></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>



<div class="modal fade stick-up" id="addcitymodal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                <div class=" clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>

                </div>

            </div>
            <br>
            <div class="modal-body">
                <div class="row">

                    <div class="error-box" id="cityerror" style="font-size: large;text-align:center"><?php echo VEHICLEMODEL_DELETE; ?></div>

                </div>
            </div>

            <br>

            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4" ></div>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4" >
                        <button type="button" class="btn btn-primary pull-right" id="cityok" ><?php echo BUTTON_YES; ?></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>



<!--<script src="http://107.170.66.211/apps/RylandInsurence/RylandInsurence/javascript/RylandInsurence.js" type="text/javascript"></script>-->




