<style>

    .div-table{
        display:table;         
        width:auto;         
        background-color:#eee;         
        border:1px solid  #666666;         
        border-spacing:5px;/*cellspacing:poor IE support for  this*/
    }
    .div-table-row{
        display:table-row;
        width:auto;
        clear:both;
    }
    .div-table-col{
        float:left;/*fix for  buggy browsers*/
        display:table-column;         
        width:200px;         
        background-color:#ccc;  
    }

    .select2-results {
        max-height: 192px;
    }
    td.details-control {
        background: url('<?php echo base_url() ?>theme/pages/img/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.details td.details-control {
        background: url('<?php echo base_url() ?>theme/pages/img/details_close.png') no-repeat center center;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        var socket = io('<?php echo socketpath; ?>');
//        var socket = io('http://iserve.ind.in:9999');
        socket.on('LiveBookingResponce', function (data) {
            $('#big_table').find('tr:first').find('th:first').trigger('click');
        });
    });
    function getdetail(bid) {
        $.ajax({
            type: 'post',
            url: '<?php echo base_url('index.php/superadmin/get_appointmentDetials') ?>',
            data: {app_id: bid},
            dataType: 'json',
            success: function (row1) {
                var now = new Date();
                var datestr = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate() + ' ' + now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
                var then = row1.appointment_dt;
                var dirr = moment.utc(moment(datestr, "YYYY-MM-DD HH:mm:ss").diff(moment(then, "YYYY-MM-DD HH:mm:ss"))).format("HH:mm:ss")
                $('.competeBooking').attr('id', row1.appointment_id);
                $('.competeBooking').attr('alt', row1.chn);
                $('.bookingid').html("Booking Id : " + row1.appointment_id);
                $('.competeBooking').attr('email', row1.email);
                $('.competeBooking').attr('apdate', row1.appointment_dt);
                $('#provider_id').val(row1.provider_id);
                $('.pickupaddress').html(row1.address_line1);
                $('.BookingTime').html(dirr);
                $('.vehicleType').html(row1.typename);
                $('#amounttocharge').val(row1.apprxAmt);
                $('.approxAmount').html(row1.apprxAmt);
                $('.paymentstatus').html(row1.paymentstatus);

                $('.pricepermin').html(row1.price_min);
                $('.visitfees').html(row1.visit_fees);
                $('.services').html(row1.services);
                $('.booking_type').html(row1.booking_type);
                $('.coupon_discount').html(row1.coupon_discount);

                $('.bookingstatus').html(row1.status_result);
                $('.driverName').html(row1.first_name);
                $('.driverPhone').html(row1.mobile);
                $('.passengerName').html(row1.sname);
                $('.passengerPhone').html(row1.phone);
                $('#modal-container-186699441').modal('show');
            }
        });
    }
    $(document).ready(function () {
        $('.idonclick').click(function () {
            var bid = $(this).attr('data');
            alert(bid);
            $.ajax({
                type: 'post',
                url: '<?php echo base_url('index.php/superadmin/get_appointmentDetials') ?>',
                data: {app_id: bid},
                dataType: 'json',
                success: function (row1) {
                    var now = new Date();
                    var datestr = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate() + ' ' + now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
                    var then = row1.appointment_dt;
                    var dirr = moment.utc(moment(datestr, "YYYY-MM-DD HH:mm:ss").diff(moment(then, "YYYY-MM-DD HH:mm:ss"))).format("HH:mm:ss")
                    $('.competeBooking').attr('id', row1.appointment_id);
                    $('.competeBooking').attr('alt', row1.chn);
                    $('.bookingid').html("Booking Id : " + row1.appointment_id);
                    $('.pickupaddress').html(row1.address_line1);
                    $('.BookingTime').html(dirr);
                    $('.vehicleType').html(row1.typename);
                    $('#amounttocharge').val(row1.apprxAmt);
                    $('.approxAmount').html(row1.apprxAmt);
                    $('.paymentstatus').html(row1.paymentstatus);
                    $('.bookingstatus').html(row1.status_result);
                    $('.driverName').html(row1.first_name);
                    $('.driverPhone').html(row1.mobile);
                    $('.passengerName').html(row1.sname);
                    $('.passengerPhone').html(row1.phone);
                    $('#modal-container-186699441').modal('show');
                }
            });
        });


//        $('#Docomplete').click(function () {
//            var chn = $('.competeBooking').attr('alt');
//            var bid = $('.competeBooking').attr('id');
//            var data = $('.competeBooking').attr('data');
//            var amount = $('#amounttobecharge').val();
//            var d = new Date;
//            var duration = [d.getFullYear(),
//                d.getMonth() + 1,
//                d.getDate()].join('-') + ' ' +
//                    [d.getHours(),
//                        d.getMinutes(),
//                        d.getSeconds()].join(':');
//            if(amount != ''){
//            if (confirm('Are you sure.!')) {
//                $.ajax({
//                    type: 'post',
//                    url: '<?php echo base_url('index.php/superadmin/CompleteBooking') ?>',
//                    data: {app_id: bid, data: data, amount: amount, duration: duration},
//                    dataType: 'json',
//                    success: function (row1) {
//                        if (row1.flag == 1)
//                            alert(row1.msg);
//                        else {
//                            var t = $('#tableWithSearch').DataTable();
//                            t.row($('.app_id_' + bid).closest("tr"))
//                                    .remove()
//                                    .draw();
//                            $('.close_confirm').trigger('click');
//                        }
//                    }
//                });
//            }
//            }else{
//                $("#errorbox").text('Please enter amount');
//            }
//        });
//        $('.competeBooking').click(function () {
//            if ($(this).attr('data') == '1') {
//
//                var bid = $('.competeBooking').attr('id');
//                var chn = $('.competeBooking').attr('alt');
//                var data = $('.competeBooking').attr('data');
//                var amount = $('#amounttobecharge').val();
//                var d = new Date;
//                var duration = [d.getFullYear(),
//                    d.getMonth() + 1,
//                    d.getDate()].join('-') + ' ' +
//                        [d.getHours(),
//                            d.getMinutes(),
//                            d.getSeconds()].join(':');
//
//                if (confirm('Are you sure.!')) {
//                    $.ajax({
//                        type: 'post',
//                        url: '<?php echo base_url('index.php/superadmin/CompleteBooking') ?>',
//                        data: {app_id: bid, data: data, amount: amount, duration: duration},
//                        dataType: 'json',
//                        success: function (row1) {
//                            if (row1.flag == 1)
//                                alert(row1.msg);
//                            else {
//
//                                var t = $('#tableWithSearch').DataTable();
//                                t.row($('.app_id_' + bid).closest("tr"))
//                                        .remove()
//                                        .draw();
//                                $('#modal-container-186699441').modal('hide');
//                                $('.close_confirm').trigger('click');
//                            }
//                        }
//
//
//
//                    });
//                }
//
//            } else {
//                $('#modal-container-186699441').modal('hide');
//                $('#ManualAmt').modal('show');
//            }
//        });
        $('.competeBooking').click(function () {
            var bid = $(this).attr('id');
            var data = $(this).attr('data');
            var email = $('.competeBooking').attr('email');
            var apdate = $('.competeBooking').attr('apdate');
            var proid = $('#provider_id').val();
            var amount = $('#charged_amount').val();
            var actionType = $('input[name=radio]:checked').val();
            var d = new Date;
            var duration = [d.getFullYear(),
                d.getMonth() + 1,
                d.getDate()].join('-') + ' ' +
                    [d.getHours(),
                        d.getMinutes(),
                        d.getSeconds()].join(':');
            if (actionType == 3)
            {
                if (amount == '' || amount == null)
                {
                    alert('please enter chargable amount');
                } else if (confirm('Are you sure.!')) {
                    $.ajax({
                        type: 'post',
                        url: '<?php echo base_url('index.php/superadmin/CompleteBooking') ?>',
                        data: {actionType: actionType, app_id: bid, data: data, proid: proid, amount: amount, apdate: apdate, response: 7, email: email, duration: duration},
                        dataType: 'json',
                        success: function (row1) {
                            if (row1.flag == 1)
                                alert(row1.msg);
                            else {



                                $('#modal-container-186699441').modal('hide');
                                var t = $('#big_table').DataTable();
                                t.row($('.app_id_' + bid).closest("tr")).remove().draw();


                            }
                        }

                    });
                    $('#modal-container-186699441').modal('hide');
                    $('.close_confirm').trigger('click');
                    var t = $('#big_table').DataTable();
                    t.row($('.app_id_' + bid).closest("tr")).remove().draw();
                }


            } else if (actionType == 2)
            {
                if (confirm('Are you sure.!')) {
                    $.ajax({
                        type: 'post',
                        url: '<?php echo base_url('index.php/superadmin/CompleteBooking') ?>',
                        data: {actionType: actionType, app_id: bid, proid: proid, apdate: apdate, response: 7, email: email, data: data, amount: 0, duration: duration},
                        dataType: 'json',
                        success: function (row1) {
                            if (row1.flag == 1)
                                alert(row1.msg);
                            else {


                                $('#modal-container-186699441').modal('hide');
                                var t = $('#big_table').DataTable();
                                t.row($('.app_id_' + bid).closest("tr")).remove().draw();


                            }
                        }

                    });
                    $('#modal-container-186699441').modal('hide');
                    $('.close_confirm').trigger('click');
                    var t = $('#big_table').DataTable();
                    t.row($('.app_id_' + bid).closest("tr")).remove().draw();
                }


            } else if (confirm('Are you sure.!')) {
                $.ajax({
                    type: 'post',
                    url: '<?php echo base_url('index.php/superadmin/cancelBooking'); ?>',
                    data: {actionType: actionType, app_id: bid, proid: proid, apdate: apdate},
                    dataType: 'json',
                    success: function (row1) {
                        if (row1.flag == 1)
                            alert(row1.msg);
                        else {



                            $('#modal-container-186699441').modal('hide');
                            var t = $('#big_table').DataTable();
                            t.row($('.app_id_' + bid).closest("tr")).remove().draw();


                        }
                    }

                });
                $('#modal-container-186699441').modal('hide');
                $('.close_confirm').trigger('click');
                var t = $('#big_table').DataTable();
                t.row($('.app_id_' + bid).closest("tr")).remove().draw();
            }
        });


    });


    function oneFunction() {
        $.ajax({
            type: 'post',
            url: '<?php echo base_url('index.php/superadmin/filter_AllOnGoing_jobs') ?>',
            dataType: 'json',
            success: function (result) {
                var t = $('#tableWithSearch').DataTable();
                t.clear().draw();
                $.each(result.aaData, function (index, row1) {
                    var status = 'Status unavailable.';
                    t.row.add([
                        '<a class="idonclick" data="' + row1.appointment_id + '" style="cursor: pointer"> <p>' + row1.appointment_id + '</p></a>',
                        row1.mas_id,
                        row1.first_name,
                        row1.dphone,
                        row1.pessanger_fname,
                        row1.phone,
                        row1.appointment_dt,
                        row1.address_line1,
                        row1.drop_addr1 + row1.drop_addr2,
                        '<span class="app_id_' + row1.appointment_id + '">' + status + '</span>'
                    ]).order([[6, 'desc']])
                            .draw();
                });
            }
        });
    }


    function refreshTableOnCityChange() {
        oneFunction();
    }

    function refreshTableOnActualcitychagne() {
        oneFunction();
    }
    function isNumberKey(evt)
    {
        $("#errMsg").text("");
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 41 || charCode > 57)) {
            //    alert("Only numbers are allowed");
            $("#errMsg").text('Please enter amount in number');
            return false;
        }
        return true;
    }
</script>
<div id="message"></div>
<!--<script src="http://iserve.ind.in:9999/socket.io/socket.io.js"></script>-->
<script src="<?php echo socketpath; ?>/socket.io/socket.io.js"></script>
<script src="http://momentjs.com/downloads/moment.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('.radio').click(function ()
        {

            if ($(this).attr('id') == "radio3")
            {
                $('#charge_input_price').show();
            } else
            {
                $('#charge_input_price').hide();

            }
        });
        var table = $('#big_table');
        $('#big_table_processing').show();
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url(); ?>index.php/superadmin/datatable_onGoing_jobs/',
//            "bJQueryUI": true,
//            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "oLanguage": {
                "sProcessing": "<img src='<?php echo base_url(); ?>theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {
                $('#big_table_processing').hide();
            },
            "drawCallback": function () {
                $('#big_table_processing').hide();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                console.log("data");
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function () {
            $('#big_table_processing').show();
            table.fnFilter($(this).val());
        });


        var detailRows = [];
        // On each draw, loop over the `detailRows` array and show any child rows
        table.on('draw', function () {
            $.each(detailRows, function (i, id) {
                $('#' + id + ' td.details-control').trigger('click');
            });
        });
    });


    function refreshTableOnCityChange() {

        var table = $('#big_table');
        $('#big_table_processing').show();
        var url = '<?php echo base_url(); ?>index.php/superadmin/bookings_data_ajax/' + $('#Sortby').val() + '/' + $('#companyid').val();
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": url,
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "oLanguage": {
                "sProcessing": "<img src='<?php echo base_url(); ?>theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {
                $('#big_table_processing').hide();
            },
            "drawCallback": function () {
                $('#big_table_processing').hide();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };
        table.dataTable(settings);
        $('#search-table').keyup(function () {
            $('#big_table_processing').show();
            table.fnFilter($(this).val());
        });

    }
</script>
<style>
    .exportOptions{
        display: none;
    }
    .header{
        height:60px !important;
    }
    .header h3{
        margin:10px 0 !important;
    }
</style>


<div class="content">
    <div class="jumbotron  no-margin" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;">
                <h3 class="">Page Title</h3>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg">
        <ul class="breadcrumb">
            <li>
                <a href="#" class="active">ON GOING JOBS</a>
            </li>

        </ul>
        <div class="container-md-height m-b-20">
            <div class="panel panel-transparent">
                <ul class="nav nav-tabs nav-tabs-simple bg-white" role="tablist" data-init-reponsive-tabs="collapse">
                    <!--                    <li class="active">
                                            <a href="#hlp_txt" data-toggle="tab" role="tab" aria-expanded="false">ON GOING JOBS</a>
                                        </li>-->
                </ul>
                <div class="panel-group visible-xs" id="LaCQy-accordion"></div>
                <div class="tab-content">
                    <div class="tab-pane active" id="hlp_txt">
                        <div class="row panel panel-default">
                            <div class="panel-heading">
                                <div id="big_table_processing" class="dataTables_processing" style=""><img src="<?php echo APP_SERVER_HOST ?>pics/ajax-loader_dark.gif"></div>
                                <div class='pull-right cls110'>

                                    <div class="pull-right">
                                        <input type="text" id="search-table" class="form-control pull-right" placeholder="<?php echo SEARCH; ?>"> </div>
                                </div>
                                <br>
                            </div>

                            <div class="panel-body no-padding">
                                <?php echo $this->table->generate(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 




<div class="modal in" id="modal-container-186699441" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="widget-15 panel  no-border no-margin widget-loader-circle">
                <div class="panel-heading">

                    <div class="panel-title bookingid">
                        Booking Id : 101
                    </div>
                    <div class="panel-controls">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="p-l-3">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="b-b b-t b-grey m-b-10">
                                    <!--pickpu address-->
                                    <div class="row m-t-10">
                                        <div class="col-md-5">
                                            <div class="panel-title">
                                                <i class="pg-map"></i>JOB Address
                                                <span class="caret"></span>
                                            </div>
                                            <p class="small hint-text pickupaddress">9th August 2014</p>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="pull-left">
                                                <p class="small hint-text no-margin">Customer</p>

                                                <span class="small hint-text passengerName">Ashish</span>
                                                <span class="small hint-text passengerPhone"> / 8892656768</span>

                                            </div>
                                            <div class="pull-right">
                                                <canvas height="64" width="64" class="clear-day"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                    <!--dropp address--> 
                                    <div class="row m-t-1">
                                        <div class="col-md-5">                                            
                                        </div>
                                        <div class="col-md-7" style="margin-bottom: -25px;">
                                            <p class="small hint-text no-margin">Provider</p>

                                            <span class="small hint-text driverName">Laxman</span>
                                            <span class="small hint-text driverPhone"> / 8892656768</span>

                                            <div class="pull-right">
                                                <canvas height="64" width="64" class="clear-day"></canvas>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <p class="bold">Booking Details</p>
                                <div class="widget-17-weather b-b b-grey">
                                    <div class="row">
                                        <div class="col-sm-6 p-r-10">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="pull-left ">Currunt Status</p>
                                                    <p class="pull-right bookingstatus"> On the way</p>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="pull-left">Payment Method</p>
                                                    <p class="pull-right paymentstatus">Cash</p>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="pull-left">Price Per Minute</p>
                                                    <p class="pull-right pricepermin">10</p>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="pull-left">Visit Fees</p>
                                                    <p class="pull-right visitfees">10</p>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="pull-left">App Discount</p>
                                                    <p class="pull-right coupon_discount">10</p>
                                                </div>
                                            </div>




                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="pull-left bold">Approx Amount</p>
                                                    <p class="pull-right text-danger approxAmount"> <?php echo CURRENCY; ?> 500 </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 p-l-10">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="pull-left">Booking Time</p>
                                                    <p class="pull-right BookingTime">1 hour 30 m </p>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="pull-left">Booking Type</p>
                                                    <p class="pull-right booking_type">NOW </p>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="pull-left">Category</p>
                                                    <p class="pull-right vehicleType">Silver</p>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="pull-left">Services</p>
                                                    <p class="pull-right services">services</p>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-1">
                                        <input type="radio" class="radio" id="radio1" name="radio" value="1" checked></div>
                                    <div class="col-sm-10" style="margin-left:-4%;">
                                        <p class="pull-left no-margin"><label>Cancel Booking Without Fee</label></p>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-sm-1">
                                        <input type="radio" class="radio" id="radio2" name="radio" value="2" ></div>
                                    <div class="col-sm-10" style="margin-left:-4%;">
                                        <p class="pull-left no-margin"><label>Complete Booking Without Fee </label></p>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-sm-1">
                                        <input type="radio" class="radio" id="radio3" name="radio" value="3" ></div>
                                    <div class="col-sm-10" style="margin-left:-4%;">
                                        <p class="pull-left no-margin"><label>Complete Booking With Fee </label></p>
                                    </div> 
                                </div>




                                <div class="row" id="charge_input_price" style="display: none;">
                                    <div class="form-group">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-3" style="margin-left:-4%;">
                                            <p class="pull-left no-margin">Charge Amount (<?php echo CURRENCY; ?>)</p></div> 
                                        <div class="col-sm-3"><input type="text" class="form-control" placeholder="Enter an amount" id="charged_amount" name="charged_amount" onkeypress="return isNumberKey(event)"></div>
                                        <div class="col-sm-3"><span id="errMsg" style="color: red"></span></div>
                                    </div>

                                </div>

                                <div class="widget-17-weather b-b b-grey"></div>



                                <div class="pull-right m-t-10"> 
                                    <button class="btn btn-success  competeBooking"  data="1">Submit</button>

                                </div>
                                <div class="pull-right m-t-10"> <button type="button" data-dismiss="modal" class="btn btn-default btn-cons" id="cancel">Cancel</button></div>
                                <!--                                <div class="row m-t-10 m-l-5">
                                                                    <div class="col-sm-6" style="float: right">
                                                                        <div class="row">
                                                                            <button class="btn btn-success btn-cons competeBooking" data="1">Complete (Don't Charge)</button>
                                                                        </div>
                                                                    </div>
                                                                    <input type="hidden" id="amounttocharge"> 
                                                                    <div class="col-sm-6 " style="float: left">
                                                                        <div class="row">
                                                                            <button class="btn btn-danger btn-cons  competeBooking" data="2">Complete (Charge) </button>
                                                                        </div>
                                                                    </div>
                                                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <img src="pages/img/progress/progress-circle-master.svg" style="display:none"></div>
        </div>
    </div>
</div>

<div id="ManualAmt" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6"><input type="text" id="amounttobecharge" placeholder="Enter Amount" style="padding: 6px;"></div>
                    <div class="col-sm-3"> <span id="errorbox" style="color: red; font-size: 12px;"></span></div>
                    <div class="col-sm-3"></div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id='Docomplete'>Trigger</button>
                <button type="button" class="btn btn-default close_confirm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script src="http://momentjs.com/downloads/moment.js"/>