<?php
    $mainAdmin = $this->session->userdata("mainAdmin");

    if($mainAdmin != true){
        redirect(base_url().'index.php/superadmin/access_denied');
    }
    $City_Nm = array();
    foreach ($cities as $val) {
        $City_Nm[$val->City_Id] =  $val->City_Name;
    }
    $Role_Nm = array();
    foreach ($roles as $val) {
        $Role_Nm[(string) $val['_id']] = $val['role_name'];
    }
    $manageAccess = array(
        ['Cities','city'],
        ['Service Category','category'],
        ['Provider','driver'],
        ['Customer','passenger'],
        ['On Going Page','onGoing'],
        ['Live Booking Feed','booking'],
        ['Provider Accept Rate','acceptance'],
        ['Payroll','payroll'],
         ['Commission','comission'],
        ['Accounting','transection'],
        ['Provider Review','driver_review'],
        ['Disputes','dispute'],
        ['Campaigns','campaigns'],
        ['Launguage','lang'],
        ['Support Text','suptext'],
        ['Cancellation Reason','can_reason'],
        ['Payment Gateway','paymentGateway'],
        ['Godsview','godsview']
    );
?>
<script src="<?= base_url() ?>theme/assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script>
//    $('.sm-table').find('.header-inner').html('<div class="brand inline" style="  width: auto;\
//                     font-size: 27px;\
//                     color: gray;\
//                     margin-left: 100px;margin-right: 20px;margin-bottom: 12px; margin-top: 10px;">\
//                    <strong><?= Appname ?> Super Admin Console</strong>\
//                </div>');
    var settings = {
        "sDom": "<'table-responsive't><'row'<p i>>",
        "destroy": true,
        "scrollCollapse": true,
        "bJQueryUI": true,
        "oLanguage": {
            "sLengthMenu": "_MENU_ ",
            "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
        },
        "iDisplayLength": 20,
        "aoColumns": [
            {"sWidth": "2%", "sClass": "text-center"},
            {"sWidth": "10%"},
            {"sWidth": "5%", "sClass": "text-center"}
        ]
    };
    var settings_user = {
        "sDom": "<'table-responsive't><'row'<p i>>",
        "destroy": true,
        "scrollCollapse": true,
        "bJQueryUI": true,
        "oLanguage": {
            "sLengthMenu": "_MENU_ ",
            "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
        },
        "iDisplayLength": 20,
        "aoColumns": [
            {"sWidth": "2%", "sClass": "text-center"},
            {"sWidth": "7%"},
            {"sWidth": "5%", "sClass": "text-center"},
            {"sWidth": "7%", "sClass": "text-center"},
            {"sWidth": "5%", "sClass": "text-center"},
            {"sWidth": "5%", "sClass": "text-center"}
        ]
    };
    $(document).ready(function () {
        var role_tbl = $('#role_table');
        role_tbl.DataTable(settings);
        $('#search-table1').keyup(function () {
            role_tbl.fnFilter($(this).val());
        });
        $('#add_new_role').click(function () {
            $('#role_title').html('Add new Role');
            $('#add_role').html('Add');
            $('#role_edit').val('');
            $('#role_name').val('');
            $('.checkrole_role').prop('checked',false);
            $('.checkrole_role').prop('disabled',false);
            $('.checkrole_role').closest('.checkbox').removeClass('disabled');
            $('#modal-role').modal('show');
        });

        var user_tbl = $('#user_table');
        user_tbl.DataTable(settings_user);

        $('#add_new_user').click(function () {
            $('#user_title').html('Add new User');
            $('#add_user').html('Add');
            $('#user_edit').val('');
            $('#user_name').val('');
            $('#user_role').val('');
            $('#user_email').val('');
            $('#user_pass').val('');
            $('#user_pass').prop('disabled',false);
            $('#user_pass').closest('.form-group').removeClass('disabled');
            $('#user_city').val('');
            $('.checkrole').prop('checked',false);
            $('.checkrole').prop('disabled',false);
            $('.checkrole').closest('.checkbox').removeClass('disabled');
            $('#modal-user').modal('show');
        });
        $('.checkall_role').change(function(){
           if(this.checked){
               $(this).closest('.row').find('.check3_role').prop('checked',true);
               $(this).closest('.row').find('.check3_role').prop('disabled',true);
               $(this).closest('.row').find('.check3_role').closest('.checkbox').addClass('disabled');
           }else{
               $(this).closest('.row').find('.check3_role').prop('disabled',false);
               $(this).closest('.row').find('.check3_role').closest('.checkbox').removeClass('disabled');
               $('.checkadd_role').trigger('change');
           }
        });
        $('.checkadd_role').change(function(){
           if(this.checked){
               $(this).closest('.row').find('.check2_role').prop('checked',true);
               $(this).closest('.row').find('.check2_role').prop('disabled',true);
               $(this).closest('.row').find('.check2_role').closest('.checkbox').addClass('disabled');
           }else{
               $(this).closest('.row').find('.check2_role').prop('disabled',false);
               $(this).closest('.row').find('.check2_role').closest('.checkbox').removeClass('disabled');
           } 
        });
        $('.checkall').change(function(){
           if(this.checked){
               $(this).closest('.row').find('.check3').prop('checked',true);
               $(this).closest('.row').find('.check3').prop('disabled',true);
               $(this).closest('.row').find('.check3').closest('.checkbox').addClass('disabled');
           }else{
               $(this).closest('.row').find('.check3').prop('disabled',false);
               $(this).closest('.row').find('.check3').closest('.checkbox').removeClass('disabled');
               $('.checkadd').trigger('change');
           }
        });
        $('.checkadd').change(function(){
           if(this.checked){
               $(this).closest('.row').find('.check2').prop('checked',true);
               $(this).closest('.row').find('.check2').prop('disabled',true);
               $(this).closest('.row').find('.check2').closest('.checkbox').addClass('disabled');
           }else{
               $(this).closest('.row').find('.check2').prop('disabled',false);
               $(this).closest('.row').find('.check2').closest('.checkbox').removeClass('disabled');
           } 
        });
        $('#user_role').change(function(){
            $('.checkrole').prop('checked',false);
            $('.checkrole').prop('disabled',false);
            $('.checkrole').closest('.checkbox').removeClass('disabled');

            var accarr = JSON.parse($('option:selected', this).attr('data-val'));
            $.each(accarr, function(ind,val){
                $("."+ind+"acc[value="+val+"]").prop("checked",true);
            });
            $('.checkadd').trigger('change');
            $('.checkall').trigger('change');
        });
    });

    var erid;
    function add_edit_role() {
        if ($('#role_name').val() == '') {
            $('#role_name').closest('.form-group').addClass('has-error');
            return;
        }
        $('#role_name').closest('.form-group').removeClass('has-error');
        $.ajax({
            url: "<?php echo base_url() ?>index.php/accessctrl/role_action",
            type: "POST",
            data: $('#role_form').serialize(),
            dataType: "JSON",
            success: function (result) {
                if (result.msg == '1') {
                    if (result.insert != 0) {
                        var role_tbl = $('#role_table').DataTable(settings);
                        var data = role_tbl.rows().data();
                        role_tbl.row.add([
                            parseInt(data.length) + 1,
                            $('#role_name').val(),
                            "<a class='btn btn-default' onclick='editrole(this)' data-id='" + result.insert + "' data-val='" + JSON.stringify(result.access) + "'>\
                                    <i class='fa fa-edit'></i> Edit\
                                </a>\
                                <a class='btn btn-default' onclick='delrole(this)' data-id='" + result.insert + "'>\
                                    <i class='fa fa-trash'></i> Delete\
                                </a>"
                        ]).draw();
                        var optionhtml="<option value='" + result.insert + "' data-val='" + JSON.stringify(result.access) + "'>"+$('#role_name').val()+"</option>";
                        $('#user_role').append(optionhtml);
                    } else {
                        $(erid).closest('tr').find("td:nth-child(2)").html($('#role_name').val());
                        $(erid).closest('tr').find("td:nth-child(3)").html("<a class='btn btn-default' onclick='editrole(this)' data-id='" + $('#role_edit').val() + "' data-val='" + JSON.stringify(result.access) + "'>\
                                                                                <i class='fa fa-edit'></i> Edit\
                                                                            </a>\
                                                                            <a class='btn btn-default' onclick='delrole(this)' data-id='" + $('#role_edit').val() + "'>\
                                                                                <i class='fa fa-trash'></i> Delete\
                                                                            </a>");
                        $("#user_role option[value="+$('#role_edit').val()+"]").attr("data-val", JSON.stringify(result.access));
                        $("#user_role option[value="+$('#role_edit').val()+"]").html($('#role_name').val());
                        erid = '';
                    }
                    $('#modal-role').modal('hide');
                } else {
                    alert('Problem occurred please try agin.');
                }
            },
            error: function () {
                alert('Problem occurred please try agin.');
            },
            timeout: 30000
        });
    }
    function editrole(rowid) {
        erid = rowid;
        $('#role_title').html('Edit Role');
        $('#add_role').html('Update');
        $('#role_edit').val($(rowid).attr('data-id'));
        $('#role_name').val($(rowid).closest('tr').find("td:nth-child(2)").html());
        $('.checkrole_role').prop('checked',false);
        $('.checkrole_role').prop('disabled',false);
        $('.checkrole_role').closest('.checkbox').removeClass('disabled');
        
        var accarr = JSON.parse($(rowid).attr('data-val'));
        $.each(accarr, function(ind,val){
            $("."+ind+"acc_role[value="+val+"]").prop("checked",true);
        });
        $('.checkadd_role').trigger('change');
        $('.checkall_role').trigger('change');
        $('#modal-role').modal('show');
    }
    function delrole(rowid) {
        if (confirm("Are you sure want to Delete?")) {
            $.ajax({
                url: "<?php echo base_url() ?>index.php/accessctrl/role_action/del",
                type: "POST",
                data: {id: $(rowid).attr('data-id')},
                dataType: "JSON",
                success: function (result) {
                    if (result.msg == '1') {
                        $("#user_role option[value="+$(rowid).attr('data-id')+"]").remove();
                        var role_tbl = $('#role_table').DataTable(settings);
                        role_tbl.row($(rowid).closest('tr'))
                                .remove()
                                .draw();
                    } else {
                        alert('Problem in Deleting Group please try agin.');
                    }
                },
                error: function () {
                    alert('Problem in Deleting Group please try agin.');
                },
                timeout: 30000
            });
        }
    }

    var euid;
    function add_edit_user() {
        if ($('#user_name').val() == '') {
            $('#user_name').closest('.form-group').addClass('has-error');
            return;
        }
        $('#user_name').closest('.form-group').removeClass('has-error');
        $.ajax({
            url: "<?php echo base_url() ?>index.php/accessctrl/user_action",
            type: "POST",
            data: $('#user_form').serialize(),
            dataType: "JSON",
            success: function (result) {
                if (result.msg == '1') {
                    if (result.insert != 0) {
                        var user_tbl = $('#user_table').DataTable(settings_user);
                        var data = user_tbl.rows().data();
                        user_tbl.row.add([
                            parseInt(data.length) + 1,
                            $('#user_name').val(),
                            "<a class='btn btn-default' onclick='editgrp(this)' data-id='" + result.msg + "'>\
                                    <i class='fa fa-edit'></i> Edit\
                                </a>\
                                <a class='btn btn-default' onclick='delgrp(this)' data-id='" + result.msg + "'>\
                                    <i class='fa fa-trash'></i> Delete\
                                </a>"
                        ]).draw();
                    } else {
                        $(euid).closest('tr').find("td:nth-child(2)").html($('#user_name').val());
                        euid = '';
                    }
                    $('#modal-user').modal('hide');
                } else {
                    alert('Problem occurred please try agin.');
                }
            },
            error: function () {
                alert('Problem occurred please try agin.');
            },
            timeout: 30000
        });
    }
    function edituser(rowid) {
        euid = rowid;
        $('#user_title').html('Edit User Details');
        $('#add_user').html('Update');
        $('#user_edit').val($(rowid).attr('data-id'));
        $('#user_name').val($(rowid).closest('tr').find("td:nth-child(2)").html());
        $('#user_role').val($(rowid).closest('tr').find("td:nth-child(3)").attr('data-id'));
        $('#user_email').val($(rowid).closest('tr').find("td:nth-child(4)").html());
        $('#user_city').val($(rowid).closest('tr').find("td:nth-child(5)").attr('data-id'));
        $('#user_pass').val('');
        $('#user_pass').prop('disabled',true);
        $('#user_pass').closest('.form-group').addClass('disabled');
        $('.checkrole').prop('checked',false);
        $('.checkrole').prop('disabled',false);
        $('.checkrole').closest('.checkbox').removeClass('disabled');
        
        var accarr = JSON.parse($(rowid).attr('data-val'));
        $.each(accarr, function(ind,val){
//            console.log(ind+"->"+val);
            $("."+ind+"acc[value="+val+"]").prop("checked",true);
        });
        $('.checkadd').trigger('change');
        $('.checkall').trigger('change');
        $('#modal-user').modal('show');
    }
    function deluser(rowid) {
        if (confirm("Are you sure want to Delete?")) {
            $.ajax({
                url: "<?php echo base_url() ?>index.php/accessctrl/user_action/del",
                type: "POST",
                data: {id: $(rowid).attr('data-id')},
                dataType: "JSON",
                success: function (result) {
                    if (result.msg == '1') {
                        var user_tbl = $('#user_table').DataTable(settings_user);
                        user_tbl.row($(rowid).closest('tr'))
                                .remove()
                                .draw();
                    } else {
                        alert('Problem in Deleting Group please try agin.');
                    }
                },
                error: function () {
                    alert('Problem in Deleting Group please try agin.');
                },
                timeout: 30000
            });
        }
    }
</script>

<style>
    .header{
        height:60px !important;
    }
    .header h3{
        margin:10px 0 !important;
    }
    .rating>.rated {
        color: #10cfbd;
    }
    .social-user-profile {
        width: 83px;
    }
    .table > thead > tr > th{
        font-size: 14px;
    }
    .form-group-default.disabled input {
        opacity: 0.23;
    }
    #selectedcity,#companyid{
        display: none;
    }
</style>

<div class="content">
    <div class="jumbotron  no-margin" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;">
                <h3 class="">Page Title</h3>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg">
        <ul class="breadcrumb">
            <li>
                <a href="#" class="active">Access Control</a>
            </li>
            <!--                        <li>
                                        <a href="#" class="active">Job Details - <?php echo $data['appt_data']->appointment_id; ?></a>
                                    </li>-->
        </ul>
        <div class="container-md-height m-b-20">
            <div class="panel panel-transparent">
                <ul class="nav nav-tabs nav-tabs-simple bg-white" role="tablist" data-init-reponsive-tabs="collapse">
                    <li class="active">
                        <a href="#tab_user" data-toggle="tab" role="tab" aria-expanded="false">Users</a>
                    </li>
                    <li class="">
                        <a href="#tab_role" data-toggle="tab" role="tab" aria-expanded="true">Role</a>
                    </li>
                </ul>
                <div class="panel-group visible-xs" id="LaCQy-accordion"></div>
                <div class="tab-content">
                    <div class="tab-pane slide-right active" id="tab_user">
                        <div class="row">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        Users
                                    </div>
                                    <div class='pull-right'>
                                        <button class='btn btn-success' id='add_new_user'>
                                            <i class="fa fa-plus text-white"></i> Add User
                                        </button>
                                    </div>
                                </div>
                                <div class="panel-body no-padding">
                                    <table class="table table-hover" id="user_table">
                                        <thead>
                                            <tr>
                                                <th>SL No.</th>
                                                <th>User Name</th>
                                                <th>Role</th>
                                                <th>Email</th>
                                                <th>City</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $l = 1;
                                            foreach ($users as $val) {
                                                ?>
                                                <tr>
                                                    <td class="v-align-middle semi-bold">
                                                        <?= $l++ ?>
                                                    </td>
                                                    <td><?= $val['name'] ?></td>
                                                    <td data-id='<?= $val['role']?>'><?= $Role_Nm[$val['role']] ?></td>
                                                    <td><?= $val['email'] ?></td>
                                                    <td data-id='<?= $val['city']?>'><?= $City_Nm[$val['city']] ?></td>
                                                    <td class="v-align-middle">
                                                        <a class='btn btn-default' onclick='edituser(this)' data-id='<?= (string) $val['_id'] ?>' data-val='<?= json_encode($val['access']) ?>'>
                                                            <i class="fa fa-edit"></i> Edit
                                                        </a>
                                                        <a class='btn btn-default' onclick='deluser(this)' data-id='<?= (string) $val['_id'] ?>'>
                                                            <i class="fa fa-trash"></i> Delete
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane slide-left" id="tab_role">
                        <div class="row">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        Role
                                    </div>
                                    <div class='pull-right'>
                                        <button class='btn btn-success' id='add_new_role'>
                                            <i class="fa fa-plus text-white"></i> Add Role
                                        </button>
                                    </div>
                                </div>
                                <div class="panel-body no-padding">
                                    <table class="table table-hover" id="role_table">
                                        <thead>
                                            <tr>
                                                <th>SL No.</th>
                                                <th>Role Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $l = 1;
                                            foreach ($roles as $val) {
                                                ?>
                                                <tr>
                                                    <td class="v-align-middle semi-bold">
                                                        <?= $l++ ?>
                                                    </td>
                                                    <td><?= $val['role_name'] ?></td>
                                                    <td class="v-align-middle">
                                                        <a class='btn btn-default' onclick='editrole(this)' data-id='<?= (string) $val['_id'] ?>' data-val='<?= json_encode($val['access']) ?>'>
                                                            <i class="fa fa-edit"></i> Edit
                                                        </a>
                                                        <a class='btn btn-default' onclick='delrole(this)' data-id='<?= (string) $val['_id'] ?>'>
                                                            <i class="fa fa-trash"></i> Delete
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade slide-up disable-scroll" id="modal-role" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog ">
        <div class="modal-content">
            <form id='role_form' onsubmit='return false;'>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style='margin: 10px;'>
                    <i class="pg-close"></i>
                </button>
                <div class="modal-header">
                    <h4 class="modal-title" id='role_title'></h4>
                </div>
                <div class="modal-body m-t-50">
                    <div class="form-group-attached">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default required">
                                    <label>Role</label>
                                    <input type="text" required name='fdata[role_name]' id="role_name" class="form-control">
                                    <input type="hidden" name='edit_id' id='role_edit'>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid bold m-t-10 m-b-10">
                        Default Access Rights
                        <?php
                            foreach($manageAccess as $val){
                                echo '<input type="hidden" value="000" name="fdata[access]['.$val[1].']">';
                            }
                        ?>
                    </div>
                    <div class='row bold p-l-20'>
                        <div class="col-sm-6">Page</div>
                        <div class="col-sm-2">View</div>
                        <div class="col-sm-2">Add</div>
                        <div class="col-sm-2">Edit</div>
                    </div>
                    <div class='row' style="height: 40vh;overflow: scroll;">
                        <?php
                            $i=0;
                            foreach($manageAccess as $val){
                        ?>
                        <div class='row bordered'>
                            <div class="col-sm-6 checkbox p-l-15"><?= $val[0]?></div>
                            <div class="col-sm-2 p-l-25">
                                <div class="checkbox check-success">
                                    <input type="checkbox" value="100" id="checkbox<?= $i?>r" name="fdata[access][<?= $val[1]?>]" class="check2_role check3_role checkrole_role <?= $val[1]?>acc_role">
                                    <label for="checkbox<?= $i++?>r" style='position: initial;'></label>
                                </div>
                            </div>
                            <div class="col-sm-2 p-l-25">
                                <div class="checkbox check-success">
                                    <input type="checkbox" value="110" id="checkbox<?= $i?>r" name="fdata[access][<?= $val[1]?>]" class="checkadd_role check3_role checkrole_role <?= $val[1]?>acc_role">
                                    <label for="checkbox<?= $i++?>r" style='position: initial;'></label>
                                </div>
                            </div>
                            <div class="col-sm-2 p-l-25">
                                <div class="checkbox check-success">
                                    <input type="checkbox" value="111" id="checkbox<?= $i?>r" name="fdata[access][<?= $val[1]?>]" class="checkall_role checkrole_role <?= $val[1]?>acc_role">
                                    <label for="checkbox<?= $i++?>r" style='position: initial;'></label>
                                </div>
                            </div>
                        </div>
                        <?php
                            }
                        ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-sm-4 m-t-10 sm-m-t-10 pull-right">
                            <button type='button' class="btn btn-primary btn-block m-t-5" id='add_role' onclick='add_edit_role()'>Add</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade slide-up disable-scroll" id="modal-user" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog ">
        <div class="modal-content">
            <form id='user_form' action="<?php echo base_url() ?>index.php/accessctrl/user_action/" autocomplete="off" method="post">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style='margin: 10px;'>
                    <i class="pg-close"></i>
                </button>
                <div class="modal-header">
                    <h4 class="modal-title" id='user_title'></h4>
                </div>
                <div class="modal-body m-t-20">
                    <div class="form-group-attached">
                        <div class="form-group form-group-default required">
                            <label>User Name</label>
                            <input type="text" required name='fdata[name]' id="user_name" class="form-control" autocomplete="off">
                            <input type="hidden" name='edit_id' id='user_edit'>
                        </div>
                        <div class="form-group form-group-default required">
                            <label>Role</label>
                            <select required name='fdata[role]' id="user_role" class="form-control">
                                <option value=''>Select Role</option>
                                <?php
                                foreach ($roles as $val) {
                                    ?>
                                    <option value='<?= (string) $val['_id'] ?>' data-val='<?= json_encode($val['access']) ?>'><?= $val['role_name'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group form-group-default required">
                            <label>Email</label>
                            <input type="email" required name='fdata[email]' id="user_email" class="form-control" value="" autocomplete="off">
                        </div>
                        <div class="form-group form-group-default required">
                            <label>Password</label>
                            <input type="password" required name='fdata[pass]' id="user_pass" class="form-control" value="" autocomplete="off">
                        </div>
                        <div class="form-group form-group-default required">
                            <label>City</label>
                            <select required name='fdata[city]' id="user_city" class="form-control">
                                <option value=''>Select City</option>
                                <option value='0'>All City</option>
                                <?php
                                foreach ($cities as $val) {
                                    ?>
                                    <option value='<?= $val->City_Id ?>'><?= $val->City_Name ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid bold m-t-10 m-b-10">
                        Access Rights
                        <input type="hidden" value="111" name="fdata[access][dashboard]">
                        <?php
                            foreach($manageAccess as $val){
                                echo '<input type="hidden" value="000" name="fdata[access]['.$val[1].']">';
                            }
                        ?>
                    </div>
                    <div class='row bold p-l-20'>
                        <div class="col-sm-6">Page</div>
                        <div class="col-sm-2">View</div>
                        <div class="col-sm-2">Add</div>
                        <div class="col-sm-2">Edit</div>
                    </div>
                    <div class='row' style="height: 25vh;overflow: scroll;">
                        <?php
                            $i=0;
                            foreach($manageAccess as $val){
                        ?>
                        <div class='row bordered'>
                            <div class="col-sm-6 checkbox p-l-15"><?= $val[0]?></div>
                            <div class="col-sm-2 p-l-25">
                                <div class="checkbox check-success">
                                    <input type="checkbox" value="100" id="checkbox<?= $i?>" name="fdata[access][<?= $val[1]?>]" class="check2 check3 checkrole <?= $val[1]?>acc">
                                    <label for="checkbox<?= $i++?>" style='position: initial;'></label>
                                </div>
                            </div>
                            <div class="col-sm-2 p-l-25">
                                <div class="checkbox check-success">
                                    <input type="checkbox" value="110" id="checkbox<?= $i?>" name="fdata[access][<?= $val[1]?>]" class="checkadd check3 checkrole <?= $val[1]?>acc">
                                    <label for="checkbox<?= $i++?>" style='position: initial;'></label>
                                </div>
                            </div>
                            <div class="col-sm-2 p-l-25">
                                <div class="checkbox check-success">
                                    <input type="checkbox" value="111" id="checkbox<?= $i?>" name="fdata[access][<?= $val[1]?>]" class="checkall checkrole <?= $val[1]?>acc">
                                    <label for="checkbox<?= $i++?>" style='position: initial;'></label>
                                </div>
                            </div>
                        </div>
                        <?php
                            }
                        ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-sm-4 m-t-10 sm-m-t-10 pull-right">
                            <button type='submit' class="btn btn-primary btn-block m-t-5" id='add_user'>Add</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>