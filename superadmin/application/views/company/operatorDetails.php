<?php
date_default_timezone_set('UTC');
$rupee = "$";
error_reporting(0);
$duebalance = $closingBal;
?>

<style>
    .ui-autocomplete{
        z-index: 5000;
    }
    #selectedcity,#companyid{
        display: none;
    }

    .ui-menu-item{cursor: pointer;background: black;color:white;border-bottom: 1px solid white;width: 200px;}
</style>
<script>
    function get_other_earning(thisval)
    {
        var B_id = thisval;
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url('index.php/superadmin') ?>/getBilledAmount",
            data: {bid: B_id},
            dataType: 'JSON',
            success: function (result)
            {
                var html = '';
                $('#ShowCategoryData').empty();
                html = '<div class="col-md-12 col-xlg-6">\n\
                                    <div class="widget-17-weather">\n\
                                        <div class="row">\n\
                                            <div class="col-sm-8 p-r-10">';
                html += '<div class="row"><p style="font-weight: bold; border-top: #d8d6d6 1px solid;padding-top: 10px;">PRO BILLING</p>\n\
                                                <div class="row">\n\
                                                    <div class="col-md-12">\n\
                                                        <p class="pull-left">Misc Fee</p>\n\
                                                        <p class="pull-right bold">$ ' + result.comp.misc_fees + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="row">\n\
                                                    <div class="col-md-12">\n\
                                                        <p class="pull-left">Material Fee</p>\n\
                                                        <p class="pull-right bold">$ ' + result.comp.mat_fees + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="row">\n\
                                                    <div class="col-md-12" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
                                                        <p class="pull-left">Pro Discount</p>\n\
                                                        <p class="pull-right bold">$ ' + result.comp.pro_disc + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                         \n\ <div class="row">\n\
                                                    <div class="col-md-12" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
                                                        <p class="pull-left">Sub Total</p>\n\
                                                        <p class="pull-right bold">$ ' + result.invoiceData.pro_billing + '</p>\n\
                                                    </div>\n\
                                                </div>\n\
                                         \n\ <div class="row">\n\
                                                    <div class="col-md-12" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
                                                        <p class="pull-left">Pro Notes</p>\n\
                                                        <p class="pull-right bold"> ' + result.comp.complete_note + '</p>\n\
                                                    </div>\n\
                                                </div>';
                $('.displayInvoiceData').html(html);
                $('#billed').modal('show');
            }
        });

    }
    $(document).ready(function () {
        $('.payroll').addClass('active');
        $('.payroll').attr('src', "<?php echo base_url(); ?>/theme/icon/payroll_on.png");
        $('#payamount').click(function () {
            var colonneSelected = $('#due_amt').html();
            if (colonneSelected == 0)
                alert("Sorry  there is no due amount for this Provider");
            else if (colonneSelected < 0) {
                $('#collectModal').modal('show');
            } else if (colonneSelected > 0) {
                $('#myModal').modal('show');
//                if (colonneSelected == 0) {
//                    $('.r_amount,.cr_amount,.p_amount').hide();
//                } else {
//                    $('.r_amount,.cr_amount,.p_amount').show();
//                }
            }
        });
        $("#releasedamt").blur(function () {
            var firstamt = parseFloat($('#due_amt').html());
            var secondamt = parseFloat($('#releasedamt').val());
            if (firstamt >= secondamt) {
                $('#error_msg').html("");
            } else {
                $('#error_msg').html("Released Amount Is grater then Due Amount ! ");
                $('#releasedamt').focus();
            }
        });
        
        $('#submit_form').click(function () {
            $('#error_msg_collect').html('');
            $('#error_msg_chk').html('');

            var firstamt = parseInt($('#releasedamt').val());
            var secondamt = parseInt($('#creleasedamt').val());


            if (!$('input[name=payment_mode]:checked').length <= 0) {
                if (firstamt == secondamt) {
                    if (firstamt > 0 || secondamt > 0) {
                        $('#error_msg_chk').html("");
                        var currentdate = new Date();
                        var datetime = currentdate.getFullYear() + "-"
                                + (currentdate.getMonth() + 1) + "-"
                                + currentdate.getDate() + " "
                                + currentdate.getHours() + ":"
                                + currentdate.getMinutes() + ":"
                                + currentdate.getSeconds();
                        $('#hdate').val(datetime);
                        var colonneSelected = $("#tableWithSearch tr:last").find('.close_bal').text();
                        $('#currunEarnigs').val(colonneSelected);
                        $('#form-work').submit();
                    } else {
                        $('#error_msg_chk').html("Amount Must be Greater then 0!");
                    }

                } else {

                    $('#error_msg_chk').html("Amount does't match !");
                }
            } else {
                $('#error_msg_pay').html("Selecte payment mode ");
            }

        });
        $('#submit_collect').click(function () {
            $('#error_msg_pay').html('');
            $('#error_msg_coll').html('');

            var firstamt = parseInt($('#collectamt').val());
            var secondamt = parseInt($('#ccollectamt').val());
            if (!$('input[name=payment_mode]:checked').length <= 0) {
                if (firstamt == secondamt) {
                    if (firstamt > 0 || secondamt > 0) {
                        $('#error_msg_coll').html("");
                        var currentdate = new Date();
                        var datetime = currentdate.getFullYear() + "-"
                                + (currentdate.getMonth() + 1) + "-"
                                + currentdate.getDate() + " "
                                + currentdate.getHours() + ":"
                                + currentdate.getMinutes() + ":"
                                + currentdate.getSeconds();
                        $('#colldate').val(datetime);
                        var colonneSelected = $("#tableWithSearch tr:last").find('.close_bal').text();
                        $('#currunEarnigs').val(colonneSelected);
                        $('#form-collect').submit();
                    } else {
                        $('#error_msg_coll').html("Amount Must be Greater then 0!");
                    }
                } else {
                    $('#error_msg_coll').html("Amount does't match !");
                }
            } else {
                $('#error_msg_coll').html("Selecte payment mode ");
            }

        });
    });
function validate(evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
        var regex = /[0-9]|\./;
        if (!regex.test(key)) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault)
                theEvent.preventDefault();
        }
    }
    function refreshTableOnCityChange() {
        var table = $('#big_table');
        var url = '';
        url = '<?php echo base_url(); ?>index.php/superadmin/operatorDetails_ajax/<?php echo $op_id ?>';
        var settings = {
            "sDom": "<'table-responsive't><'row'<p i>>",
//            "sPaginationType": "bootstrap",
            "destroy": true,
            "scrollCollapse": true,
//            "oLanguage": {
//                "sLengthMenu": "_MENU_ ",
//                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
//            },
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": url,
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "oLanguage": {
                "sProcessing": "<img src='<?php echo base_url(); ?>theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {
                //oTable.fnAdjustColumnSizing();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function () {
            table.fnFilter($(this).val());
        });

    }

</script>


<script type="text/javascript">
    $(document).ready(function () {


        $('#datepicker-component').on('changeDate', function () {
            $(this).datepicker('hide');
        });



//        $("#datepicker1").datepicker({ minDate: 0});
        var date = new Date();
        $('#datepicker-component').datepicker({
            startDate: date
        });
        var table = $('#big_table');
        var settings = {
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url(); ?>index.php/superadmin/operatorDetails_ajax/<?php echo $op_id ?>',
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "oLanguage": {
                "sProcessing": "<img src='<?php echo base_url(); ?>theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {
                //oTable.fnAdjustColumnSizing();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };
        table.dataTable(settings);
        $('#search-table').keyup(function () {
            table.fnFilter($(this).val());
        });
    });
</script>
<style>
    .exportOptions{
        display: none;
    }
</style>
<div class="page-content-wrapper">
    <div class="content">
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <br/>
                <div class="inner">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url('index.php/superadmin/operatorPayroll') ?>" class="">Payroll</a>
                        </li>
                        <li><a href="#" class="">Operator(<?php echo $operatorName ?>)</a>
                        </li>
                         
                    </ul>
                </div>

                <!--start box-->
                <div class="row">
                    <div class="col-md-3">
                        <div class="widget-9 panel no-border bg-primary no-margin widget-loader-bar">
                            <div class="container-xs-height full-height">
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="panel-heading  top-left top-right">
                                            <div class="panel-title text-black">
                                            </div>
                                            <div class="panel-controls">
                                                <ul>
                                                    <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="p-l-20">
                                            <a href=" ">
                                                <h4 class="no-margin p-b-5 text-white">OPENING BALANCE</h4>                                           
                                                <div style="font-size: 42px;margin-left: 10%;">
                                                    <?php echo  CURRENCY . ' ' . $openingBal ?>
                                                </div>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-bottom">
                                        <div class="progress progress-small m-b-20">
                                            <div class="progress-bar progress-bar-white" data-percentage="0%" style="width: 0%;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <img src="pages/img/progress/progress-bar-master.svg" style="display: none;">
                        </div>                        
                    </div>
                    <div class="col-md-3">
                        <div class="widget-9 panel no-border bg-success no-margin widget-loader-bar">
                            <div class="container-xs-height full-height">
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="panel-heading  top-left top-right">
                                            <div class="panel-title text-black">
                                            </div>
                                            <div class="panel-controls">
                                                <ul>
                                                    <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="p-l-20">
                                            <a href="">
                                                <h4 class="no-margin p-b-5 text-white">BOOKING THIS CYCLE</h4>                                           
                                                <div style="font-size: 42px;margin-left: 10%;">
                                                    <?php echo    $bookingCycle ?>
                                                </div>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-bottom">
                                        <div class="progress progress-small m-b-20">
                                            <div class="progress-bar progress-bar-white" data-percentage="0%" style="width: 0%;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <img src="pages/img/progress/progress-bar-master.svg" style="display: none;">
                        </div>                        
                    </div>
                    <div class="col-md-3">
                        <div class="widget-9 panel no-border bg-primary no-margin widget-loader-bar">
                            <div class="container-xs-height full-height">
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="panel-heading  top-left top-right">
                                            <div class="panel-title text-black">
                                            </div>
                                            <div class="panel-controls">
                                                <ul>
                                                    <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="p-l-20">
                                            <a href="">
                                                <h4 class="no-margin p-b-5 text-white">EARNING THIS CYCLE</h4>                                           
                                                <div style="font-size: 42px;margin-left: 10%;">
                                                    <?php echo  CURRENCY . ' ' . $earningCycle ?>
                                                </div>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-bottom">
                                        <div class="progress progress-small m-b-20">
                                            <div class="progress-bar progress-bar-white" data-percentage="0%" style="width: 0%;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <img src="pages/img/progress/progress-bar-master.svg" style="display: none;">
                        </div>                        
                    </div>
                    <div class="col-md-3">
                        <div class="widget-9 panel no-border bg-success no-margin widget-loader-bar">
                            <div class="container-xs-height full-height">
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="panel-heading  top-left top-right">
                                            <div class="panel-title text-black">
                                            </div>
                                            <div class="panel-controls">
                                                <ul>
                                                    <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="p-l-20">
                                            <a href="">
                                                <h4 class="no-margin p-b-5 text-white">CLOSING BALANCE</h4>                          
                                                <div style="font-size: 42px;margin-left: 10%;">
                                                    <?php echo  CURRENCY . ' ' .$closingBal ?> 
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-bottom">
                                        <div class="progress progress-small m-b-20">
                                            <div class="progress-bar progress-bar-white" data-percentage="0%" style="width: 95.4545%;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <img src="pages/img/progress/progress-bar-master.svg" style="display: none;"></div>
                    </div>                    
                </div>
                <!--end box-->
                <div class="container-fluid container-fixed-lg bg-white">
                    <div class="panel panel-transparent">
                        <div class="panel-heading">                            
                            <div class="row clearfix">
                                 <div class="pull-left">
                                            <div class="col-md-6">
                                                <button id="payamount" class="btn btn-primary btn-cons m-b-10 m-l-10 pull-left" type="button" ><i class="pg-form"></i> <span class="bold">PAY</span>
                                                </button>
                                            </div>
                                        </div>
                                <div class="">
                                    <div class="pull-right"><input type="text" id="search-table" class="form-control pull-right" placeholder="Search"> </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                                    <div id="tableWithSearch_wrapper" class="dataTables_wrapper form-inline no-footer"><div class="table-responsive"><table class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info">
                                                <thead>
                                                    <tr role="row">
                                                        <th class="sorting_asc" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column ascending" style="width: 68px;">SLNO</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">CYCLE START DATE</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">BOOKINGS THIS CYCLE</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">OPENING BALANCE (<?php echo $rupee; ?>)</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">OPERATOR RECEIVABLE (<?php echo $rupee; ?>)</th>
                                                        
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">PAID AMOUNT(<?php echo $rupee; ?>)</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 50px;">PAY DATE</th>
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">CLOSING BALANCE</th>                                         
                                                        
                                                        <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 88px;">PAYMENT STATUS</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                       <?php
                                                    $slno = 1;
                                                    ?>
                                                    <tr role="row"  class="gradeA odd">
                                                <td class="v-align-middle sorting_1"> <p><?php echo   $slno; ?></p></td>
                                                <td class="v-align-middle sorting_1"> <p><?php echo $currentcycle['start_date'] ?></p></td>
                                                <td class="v-align-middle sorting_1"> <p><?php echo $currentcycle['booking_cycle'] ?></p></td>
                                                <td class="v-align-middle sorting_1"> <p><?php echo CURRENCY . ' ' . $currentcycle['opening_balance'] ?></p></td>
                                                <td class="v-align-middle sorting_1"> <p><?php echo CURRENCY . ' ' . $currentcycle['booking_earn'] ?></p></td>
                                                <td class="v-align-middle sorting_1"> <p><?php echo CURRENCY . ' ' . $currentcycle['pay_amount'] ?></p></td>
                                                <td class="v-align-middle sorting_1"> <p><?php echo $currentcycle['pay_date'] ?></p></td>
                                                <td class="v-align-middle sorting_1"> <p><?php echo CURRENCY . ' ' . $currentcycle['closing_balance'] ?></p></td>
                                                <td class="v-align-middle sorting_1"> <p>--</p></td>
                                                
                                                    </tr>
                                                <?php
                                                  $slno += 1;
                                                foreach ($pastcycle as $result) {
                                                    
                                                    ?>
                                                    <tr role="row"  class="gradeA odd">
                                                        <td class="v-align-middle sorting_1"> <p><?php echo $slno; ?></p></td>
                                                        <td class="v-align-middle sorting_1"> <p><?php echo $result['start_date'] ?></p></td>
                                                        <td class="v-align-middle sorting_1"> <p><?php echo $result['no_of_bookings'] ?></p></td>
                                                        <td class="v-align-middle sorting_1"> <p><?php echo CURRENCY . ' ' .$result['opening_balance'] ?></p></td>
                                                        <td class="v-align-middle sorting_1"> <p><?php echo CURRENCY . ' ' .$result['operator_recevible'] ?></p></td>
                                                        <td class="v-align-middle sorting_1"> <p><?php echo CURRENCY . ' ' .$result['pay_amount'] ?></p></td>
                                                        <td class="v-align-middle sorting_1"> <p><?php echo $result['pay_date'] ?></p></td>
                                                        <td class="v-align-middle sorting_1"> <p><?php echo CURRENCY . ' ' .$result['closing_balance'] ?></p></td>
                                                        <td class="v-align-middle sorting_1"> <p>Success</p></td>
                                                    </tr>
                                                    <?php
                                                    $slno++;
                                                }
                                                   
                                                ?>
                                                </tbody>
                                            </table></div><div class="row">
                                        </div></div>
                                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade stick-up" id="confirmmodels" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class=" clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
            </div>
            <br>
            <div class="modal-body">
                <div class="row">
                    <div class="error-box" id="errorboxdatas" style="font-size: large;text-align:center"><?php echo VEHICLEMODEL_DELETE; ?></div>
                </div>
            </div>
            <br>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4" ></div>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4" >
                        <button type="button" class="btn btn-primary pull-right" id="confirmeds" ><?php echo BUTTON_OK; ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade slide-up disable-scroll in" id="billed" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;">
    <div class="modal-dialog ">       
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <div class="container-xs-height full-height">
                    <div class="row-xs-height">
                        <div class="modal-body col-xs-height  displayInvoiceData ">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade stick-up in" id="myModal" tabindex="-1" role="dialog" aria-hidden="false" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <h5>Payment <span class="semi-bold">Information</span></h5>
            </div>
            <div class="modal-body">
                <form id="form-work" class="form-horizontal" role="form" autocomplete="off" novalidate="novalidate" action="<?php echo base_url() ?>index.php/superadmin/pay_operator_amount/<?php echo $op_id ?>" method="post">
                    <div class="form-group-attached">
                        <input type='hidden' name='ctime' id="hdate" value==''>
                        <input type='hidden' id="dueamount" name='dueamount' value='<?php echo $duebalance; ?>'>
                        <div class="form-group">
                            <label for="position" class="col-sm-3 control-label">Due Amount</label>
                            <div class="col-sm-9">
                                <?php echo $rupee ?> <span id="due_amt"><?php echo $duebalance; ?></span>
                            </div>
                        </div>                      
                        <div class="form-group">
                            <label for="position" class="col-sm-3 control-label">Payment Mode</label>                           
                            <div class="col-sm-1">
                                <input type="radio" id="cash_pay" value="1" name="payment_mode" class="">
                            </div>
                            <label for="can_after_pro_on" class="col-sm-3 control-label">
                                CASH
                            </label>                          
<!--                            <div class="col-sm-1">
                                <input type="radio" id="card_pay" value="2" name="payment_mode" class="">
                            </div>
                            <label for="can_after_pro_on" class="col-sm-3 control-label">
                                CARD
                            </label>-->
                            <br>
                            <span id="error_msg_pay" style="color: red"></span>
                        </div>
                        <div class="form-group r_amount">
                            <label for = "position" class = "col-sm-3 control-label">Enter Amount To Be Paid </label>
                            <div class = "col-sm-9">
                                <input type = "text" min = "1" class = "form-control " id = "releasedamt" placeholder = "Collect Amount" onkeypress = 'validate(event)' required = "" aria-required = "true" aria-invalid = "true" >
                                <span id = "error_msg" style = "color: red"></span>
                            </div>
                        </div>
                        <div class = "form-group cr_amount">
                            <label for = "position" class = "col-sm-3 control-label">Re-Confirm Amount</label>
                            <div class = "col-sm-9">
                                <input type = "text" min = "1" class = "form-control " id = "creleasedamt" placeholder = "Confirm Amount" onkeypress = 'validate(event)' required = "" aria-required = "true" aria-invalid = "true" name = "paid_amount">
                                <span id = "error_msg_chk" style = "color: red"></span>
                            </div>
                        </div>
                    </div>
                </form>
                <div class = "row">
                    <div class = "col-sm-8">
                        <div class = "p-t-20 clearfix p-l-10 p-r-10">
                            <div class = "pull-left">
                            </div>
                        </div>
                    </div>
                    <div class = "col-sm-4 m-t-10 sm-m-t-10 p_amount">
                        <button type = "button" id = "submit_form" class = "btn btn-primary btn-block m-t-5">Pay Now</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>