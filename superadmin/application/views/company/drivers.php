<?php
date_default_timezone_set('UTC');
$rupee = "$";
//error_reporting(0);

$link = explode('/', $this->session->userdata('lastDoctor'));

$db = $link[0];
$accept = "";
$reject = '';
$free = "";
$new = "";
$offline = '';
$booked = '';
$status = 1;
if ($status == 1) {
    $vehicle_status = 'New';
    $new = "active";
} else if ($status == 3 && $db == 'my') {
    $vehicle_status = 'Accepted';
    $accept = "active";
} else if ($status == 3 && $db == 'mo') {
    $vehicle_status = 'Online&Free';
    $free = "active";
} else if ($status == 4) {
    $vehicle_status = 'Rejected';
    $reject = 'active';
} else if ($status == 30) {
    $$vehicle_status = 'Offile';
    $offline = 'active';
} else if ($status == 567) {
    $$vehicle_status = 'Booked';
    $booked = 'active';
}
?>

<style>
    .panel-body {
        overflow-x: auto;
    }
    .imageborder{
        border-radius: 50%;
    }
    .catcb{
        width: 3.5em;
        height: 1.5em;
        margin: 0.5em;
    }
</style>

<style>
    .col-md-12.col-xlg-6 > p {
        font-size: 18px;
        letter-spacing: 1px;
        font-weight: 800;
        text-align: center;
        color: darkcyan;
    }
    .fs-14 {
        font-size: 20px !important;
        color: #054949!important;
    }
    .col-sm-8.p-r-10 {
        width: 100%;
    }
    .modal .modal-body {
        background: #ffffff;
    }
    p.pull-left.bold {
        color: cadetblue;
        font-size: 14px;
    }

</style>
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
    function ShowCategory(pid)
    {

        $('#ShowCategoryData').empty();
        $('#coupon_dis').modal('show');
        $.ajax({
            url: "<?php echo base_url('index.php/superadmin') ?>/GetAllCatForProvider",
            type: "POST",
            data: {val: pid},
            dataType: 'json',
            beforeSend: function () {
                $('#ShowCategoryDataLoading').show();
            },
            complete: function () {
                $('#ShowCategoryDataLoading').hide();
            },
            success: function (result)
            {
                var count = 1;

                var newCatcount = 1;
                var newCat = '';
                newCat += '<div class="row"><div class="col-md-12 col-xlg-6">';
                newCat += '<p>City : ' + result.cityName + ' </p></div></div>';
                newCat += '<hr/>';
                $.each(result.newCat, function (index, res) {
                    if (newCatcount == 1) {
                        newCat += '<div class="row"><div class="col-md-12" style="margin-left: 25%;"><p class="pull-left bold">New Category </p></div></div><hr/>';
                    }
                    newCat += '<div class="row">';
                    newCat += '<div class="col-md-12" style="margin-left: 25%;">';
                    newCat += '<p class="pull-left bold">' + newCatcount + '</p>';
                    newCat += '<p class="pull-left bold"> . ' + res.cat_name + '</p>';
                    newCat += '</div></div>';
                    newCatcount++;
                });

                var rejCatcount = 1;
                var rejCat = '';
                $.each(result.rejCat, function (index, res) {
                    if (rejCatcount == 1) {
                        rejCat += '<br/><div class="row"><div class="col-md-12" style="margin-left: 25%;"><p class="pull-left bold">Rejected Category </p></div></div><hr/>';
                    }
                    rejCat += '<div class="row">';
                    rejCat += '<div class="col-md-12" style="margin-left: 25%;">';
                    rejCat += '<p class="pull-left bold">' + rejCatcount + '</p>';
                    rejCat += '<p class="pull-left bold"> . ' + res.cat_name + '</p>';
                    rejCat += '</div></div>';
                    rejCatcount++;
                });

                var accCatcount = 1;
                var accCat = '';
                $.each(result.accCat, function (index, res) {
                    if (accCatcount == 1) {
                        accCat += '<div class="row"><div class="col-md-12" style="margin-left: 25%;"><p class="pull-left bold">Accepted Category </p></div></div><hr/>';
                    }
                    accCat += '<div class="row">';
                    accCat += '<div class="col-md-12" style="margin-left: 25%;">';
                    accCat += '<p class="pull-left bold">' + accCatcount + '</p>';
                    accCat += '<p class="pull-left bold"> . ' + res.cat_name + '</p>';
                    accCat += '</div></div>';
                    accCatcount++;
                });


//                var html = '';
//                html = '<br/>\n\<div class="col-md-12 col-xlg-6">\n\
//                                         <p>City : ' + result.cityName + ' </p>\n\
//        <div class="row" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
//</div>\n\
//                         <p>Accepted Category </p>\n\
//                                    <div class="widget-17-weather">\n\
//                                        <div class="row" style="border-top: #d8d6d6 1px solid;padding-top: 10px;">\n\
//                                            <div class="col-sm-8 p-r-10">';
//                $.each(result.catarr, function (index, res) {
//                    html += '<div class="row">\n\
//                             <div class="col-md-12" style="margin-left: 25%;">\n\
//                             <p class="pull-left bold">' + count + '</p>\n\
//                             <p class="pull-left bold"> . ' + res.cat_name + '</p>\n\
//                             </div>\n\
//                             </div>';
//                    count++;
//                });
//                html += '</div>\n\
//                                          </div>\n\
//                                    </div>\n\
//                               </div>';

//                $('.coupondisData').html(newCat + accCat + rejCat);
                $('.coupondisData').html(newCat + accCat + rejCat);
            }
        });
    }
    $(document).ready(function () {
        $('#big_table1').on('init.dt', function () {

            var urlChunks = $("li.active").find('.changeMode').attr('data').split('/');
            var status = urlChunks[urlChunks.length - 1];
            var forwhat = urlChunks[urlChunks.length - 2];
            if (forwhat == 'my') {
                if (status == 1) {
                    $('#big_table1').dataTable().fnSetColumnVis([6], false);
                }
                if (status == 1 || status == 3 || status == 4) {
                    $('#big_table1').dataTable().fnSetColumnVis([10], false);
                    $('#big_table1').dataTable().fnSetColumnVis([9], false);
                }
            }
            if (forwhat == 'mo') {
                if (status == 567) {
                    $('#big_table1').dataTable().fnSetColumnVis([9], false);
                    $('#big_table1').dataTable().fnSetColumnVis([10], false);
                    $('#big_table1').dataTable().fnSetColumnVis([9], false);
                }
                if (status == 3 || status == 30) {
                    $('#big_table1').dataTable().fnSetColumnVis([4], false);
                    $('#big_table1').dataTable().fnSetColumnVis([7], false);
                    $('#big_table1').dataTable().fnSetColumnVis([11], false);
                    $('#big_table1').dataTable().fnSetColumnVis([9], false);
                }
            }

        });
        $("#define_page").html("Drivers");
        $('.drivers').addClass('active');
        $('.Drivers').attr('src', "<?php echo base_url(); ?>/theme/icon/drivers_on.png");
//        $('.driver_thumb').addClass("bg-success");

        $("#document_data").click(function () {


            $("#display-data").text("");
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
            if (val.length == 0) {
                //         alert("please select any one dispatcher");
                $("#display-data").text(<?php echo json_encode(POPUP_DRIVER_EDIT); ?>);
            } else if (val.length > 1)
            {

                //     alert("please select only one to edit");
                $("#display-data").text(<?php echo json_encode(POPUP_DRIVER_ONLYEDIT); ?>);
            } else
            {
                var size = $('input[name=stickup_toggler]:checked').val()
                var modalElem = $('#myModaldocument');
                if (size == "mini") {
                    $('#modalStickUpSmall').modal('show')
                } else {
                    $('#myModaldocument').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }

            }
            $('#doc_body').html('');
            $.ajax({
                url: "<?php echo base_url('index.php/superadmin') ?>/documentgetdata",
                type: "POST",
                data: {val: val},
                dataType: 'json',
                success: function (result)
                {
                    if (result == '')
                    {
                        $('#doc_body').text("No Data Available...");
                    } else {
                        console.log(result);
                        $.each(result, function (index, doc) {

                            var html = "<tr><td>";
                            if (doc.doctype == '1')
                                html += "License</td><td>" + doc.expirydate + "</td>";
                            else
                                html += "PRACTICE CERTIFICATE</td><td>-</td>";
                            html += "<td>" + "<a target=__blank href=" + '<?php echo base_url() ?>' + "../../pics/" + doc.url + "><button>view</button></a>\n\
                            <a target=__blank href=" + '<?php echo base_url() ?>' + "../../pics/" + doc.url + " download= " + doc.url + "><button>download</button></a>" + "</td>";
                            html += "</tr>";
                            $('#doc_body').append(html);
                        });
                    }
                    $("#documentok").click(function () {
                        $('.close').trigger('click');
                    });
                }
            });
            $("#editdriver").click(function () {


                $("#display-data").text("");
                var val = $('.checkbox:checked').map(function () {
                    return this.value;
                }).
                        get();
                if (val.length == 0) {
                    //         alert("please select any one dispatcher");
                    $("#display-data").text(<?php echo json_encode(POPUP_DRIVER_EDIT); ?>);
                } else if (val.length > 1)
                {

                    //     alert("please select only one to edit");
                    $("#display-data").text(<?php echo json_encode(POPUP_DRIVER_ONLYEDIT); ?>);
                } else
                {

//               window.locaton = "<?php echo base_url() ?>index.php/superadmin/editdriver" + val;
                    window.location = "<?php echo base_url('index.php/superadmin') ?>/editdriver/" + val;
                }
            });
        });
        $("#chekdel").click(function () {

            $("#display-data").text("");
            var val = [];
            $('.checkbox:checked').each(function (i) {
                val[i] = $(this).val();
            });
            if (val.length > 0) {

                var size = $('input[name=stickup_toggler]:checked').val()
                var modalElem = $('#deletedriver');
                if (size == "mini")
                {
                    $('#modalStickUpSmall').modal('show')
                } else
                {
                    $('#deletedriver').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }
                $("#errorbox").text(<?php echo json_encode(POPUP_DRIVERS_DELETE); ?>);
                $("#yesdelete").click(function () {

//            if(confirm("Are you sure to Delete " +val.length + " Drivers")){
                    $.ajax({
                        url: "<?php echo base_url('index.php/superadmin') ?>/deleteDrivers",
                        type: "POST",
                        data: {masterid: val},
                        success: function (result) {

                            $('.checkbox:checked').each(function (i) {
                                $(this).closest('tr').remove();
                            });
                            $(".close").trigger('click');
                        }
                    });
                });
            } else {
//                alert("Please mark any one of options");
                $("#display-data").text(<?php echo json_encode(POPUP_DRIVERS_ATLEAST); ?>);
            }
        });
        $("#makeon").click(function () {
            $("#display-data").text("");
            var val = [];
            $('.checkbox:checked').each(function (i) {
                val[i] = $(this).val();
            });
            if (val.length > 0) {
                $.ajax({
                    url: "<?php echo base_url('index.php/superadmin') ?>/makeonline",
                    type: "POST",
                    data: {masterid: val},
                    success: function (result) {
                        $('.checkbox:checked').each(function (i) {
                            $(this).closest('tr').remove();
                        });
                        var res = JSON.parse(result);
                        alert(res.msg);
                    }
                });
            } else {
                $("#display-data").text(<?php echo json_encode(POPUP_DRIVERS_ATLEAST); ?>);
            }
        });
        $("#makeoff").click(function () {
            $("#display-data").text("");
            var val = [];
            $('.checkbox:checked').each(function (i) {
                val[i] = $(this).val();
            });
            if (val.length > 0 && confirm('Are you sure you want to offline?')) {
                $.ajax({
                    url: "<?php echo base_url('index.php/superadmin') ?>/makeOffline",
                    type: "POST",
                    data: {masterid: val},
                    success: function (result) {
                        $('.checkbox:checked').each(function (i) {
                            $(this).closest('tr').remove();
                        });
                        var res = JSON.parse(result);
                    }
                });
            } else {
                $("#display-data").text(<?php echo json_encode(POPUP_DRIVERS_ATLEAST); ?>);
            }
        });
        $("#makelog").click(function () {
            $("#display-data").text("");
            var val = [];
            $('.checkbox:checked').each(function (i) {
                val[i] = $(this).val();
            });
            if (val.length > 0 && confirm('Are you sure you want to logout?')) {
                $.ajax({
                    url: "<?php echo base_url('index.php/superadmin') ?>/makeLogout",
                    type: "POST",
                    data: {masterid: val},
                    success: function (result) {
                        $('.checkbox:checked').each(function (i) {
                            $(this).closest('tr').remove();
                        });
                        var res = JSON.parse(result);
                    }
                });
            } else {
                $("#display-data").text(<?php echo json_encode(POPUP_DRIVERS_ATLEAST); ?>);
            }
        });
        $('#acceptcat').click(function () {

            var isOperator = $("input[name=opoption]:checked").val();
            var opr_id = $('#opr_id').val();
//            if (isOperator == 'undefined' || opr_id == 'undefined')
//            {
//                isOperator = 2;
//                opr_id = 0;
//            }
            var catcb = $('.catcb:checked').map(function () {
                return this.value;
            }).get();
            var pid = $('#proid').val();
            if (catcb.length > 0)
            {
                if (isOperator == 1)
                {
                    if (opr_id != 0)
                    {
                        $.ajax({
                            url: "<?php echo base_url('index.php/superadmin') ?>/AcceptCategory",
                            data: {catcb: catcb, pid: pid, isOperator: isOperator, opr_id: opr_id},
                            type: "POST",
                            dataType: 'json',
                            success: function (data) {
                                console.log(data);
                                window.location.reload(true);
                            }
                        });
                    } else
                    {
                        alert("select atleast one operator");
                    }
                } else
                {
                    $.ajax({
                        url: "<?php echo base_url('index.php/superadmin') ?>/AcceptCategory",
                        data: {catcb: catcb, pid: pid, isOperator: isOperator, opr_id: opr_id},
                        type: "POST",
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);
                            window.location.reload(true);
                        }
                    });
                }
            } else {
                alert("select atleast one category");
            }
        });


//        $('#acceptcat').click(function () {
//
//
//            var catcb = $('.catcb:checked').map(function () {
//                return this.value;
//            }).get();
//            var pid = $('#proid').val();
//            if (catcb.length > 0)
//            {
//                $.ajax({
//                    url: "<?php echo base_url('index.php/superadmin') ?>/AcceptCategory",
//                    data: {catcb: catcb, pid: pid},
//                    type: "POST",
//                    dataType: 'json',
//                    success: function (data) {
//                        console.log(data);
//                        window.location.reload(true);
//                    }
//                });
//            } else {
//                alert("select atleast one category");
//            }
//        });
//        $("#accept").click(function () {
//            $("#display-data").text("");
//            var val = $('.checkbox:checked').map(function () {
//                return this.value;
//            }).get();
//            if (val.length > 0) {
//                val = val.toString();
//                $.ajax({
//                    url: "<?php echo base_url('index.php/superadmin') ?>/GetAllCatForProvider",
//                    type: "POST",
//                    data: {val: val},
//                    dataType: 'json',
//                    success: function (result)
//                    {
//
//                        $('#catdata').empty();
//                        var data = "<table>";
//                        $.each(result.newCat, function (index, res) {
//                            data += "<tr>";
//                            data += "<td><div class='checkbox check-primary'><input class='catcb'id='" + res.id + "' type='checkbox' value='" + res.id + "' name='fdata[id]'/><label for='" + res.id + "'> " + res.cat_name + "</label></div></td>";
//                        });
//                        $.each(result.rejCat, function (index, res) {
//                            data += "<tr>";
//                            data += "<td><div class='checkbox check-primary'><input class='catcb'id='" + res.id + "' type='checkbox' value='" + res.id + "' name='fdata[id]'/><label for='" + res.id + "'> " + res.cat_name + "</label></div></td>";
//                        });
//
//                        data += "</table>";
//                        $('#catdata').append(data);
//                        $('#proid').val(val);
//                        $('#catlist').modal('show');
//                    }
//                });
//            } else
//            {
//                $("#display-data").text(<?php echo json_encode(POPUP_DRIVERS_ATLEAST); ?>);
//            }
//        });
        $("#accept").click(function () {

            $("#display-data").text("");
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
            if (val.length > 0) {
                val = val.toString();

                $.ajax({
                    url: "<?php echo base_url('index.php/superadmin') ?>/GetAllCatForProvider",
                    type: "POST",
                    data: {val: val},
                    dataType: 'json',
                    success: function (result)
                    {
                        $('#catdata').empty();
                        var data = "<table>";
                        var isNeedOpr = 0;
                        $.each(result.newCat, function (index, res) {
                            data += "<tr>";
                            data += "<td><div class='checkbox check-primary'><input class='catcb'id='" + res.id + "' type='checkbox' value='" + res.id + "' name='fdata[id]'/><label for='" + res.id + "'> " + res.cat_name + "</label></div></td>";
                            isNeedOpr = 1;
                        });

                        $.each(result.rejCat, function (index, res) {
                            data += "<tr>";
                            data += "<td><div class='checkbox check-primary'><input class='catcb'id='" + res.id + "' type='checkbox' value='" + res.id + "' name='fdata[id]'/><label for='" + res.id + "'> " + res.cat_name + "</label></div></td>";
                             isNeedOpr = 1;
                        });
                        data += "</table>";
                        var nextData = "";

                        if (isNeedOpr == 1) {
                            nextData += '<br/>';
                            nextData += '<label class="btn btn-default active">';
                            nextData += '<input type="radio" name="opoption" id="opoption1" value="0" checked> <span class="fs-16">Freelancer</span>';
                            nextData += '</label>';
                            nextData += '<label class="btn btn-default">';
                            nextData += '<input type="radio" name="opoption" id="opoption2" value="1"> <span class="fs-16 click_opr">Operator</span>';
                            nextData += '</label>';
                            nextData += '</br></br>';
                            nextData += '<select id="opr_id" class="form-control error-box-class hide_pro" name="opr_id" style="display:none">';
                            nextData += '<option value="0">select Operator</option>';

                            $.each(result.oparr, function (index, res) {
                                nextData += '<option value="' + res.id + '"> ' + res.operator_name + '</option>';
                            });
                            nextData += '</select>';
                        }

                        $('#catdata').append(data);
                        $('#catdata').append(nextData);
                        $('#proid').val(val);
                        $('#catlist').modal('show');
                    }
                });
            } else
            {
                $("#display-data").text(<?php echo json_encode(POPUP_DRIVERS_ATLEAST); ?>);
            }
        });


        $('#driverresetpassword').click(function () {
            $("#display-data").text("");
            $("#errorpass_driversmsg").text("");
            $("#newpass").val("");
            $("#confirmpass").val("");
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
            if (val.length == 0) {
                //         alert("please select any one dispatcher");
                $("#display-data").text(<?php echo json_encode(POPUP_DRIVER_EDIT); ?>);
            } else if (val.length > 1)
            {

                //     alert("please select only one to edit");
                $("#display-data").text(<?php echo json_encode(POPUP_DRIVER_ONLYEDIT); ?>);
            } else
            {
                var size = $('input[name=stickup_toggler]:checked').val()
                var modalElem = $('#myModal1_driverpass');
                if (size == "mini") {
                    $('#modalStickUpSmall').modal('show')
                } else {
                    $('#myModal1_driverpass').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }

            }
        });
        $("#editpass_msg").click(function () {
            $("errorpass").text("");
            $("#errorpass_driversmsg").text("");
            var newpass = $(".newpass").val();
            var confirmpass = $(".confirmpass").val();
            var reg = /^\S*(?=\S*[a-zA-Z])(?=\S*[0-9])\S*$/; //^[-]?(?:[.]\d+|\d+(?:[.]\d*)?)$/;



            if (newpass == "" || newpass == null)
            {
//                alert("please enter the new password");
                $("#errorpass_driversmsg").text(<?php echo json_encode(POPUP_PASSENGERS_PASSNEW); ?>);
            } else if (!reg.test(newpass))
            {
//                alert("please enter the password with atleast one chareacter and one letter");

                $("#errorpass_driversmsg").text(<?php echo json_encode(POPUP_PASSENGERS_PASSVALID); ?>);
            } else if (confirmpass == "" || confirmpass == null)
            {
//                alert("please confirm the password");
                $("#errorpass_driversmsg").text(<?php echo json_encode(POPUP_PASSENGERS_PASSCONFIRM); ?>);
            } else if (confirmpass != newpass)
            {
//                alert("please confirm the same password");
                $("#errorpass_driversmsg").text(<?php echo json_encode(POPUP_PASSENGERS_SAMEPASSCONFIRM); ?>);
            } else
            {
                $.ajax({
                    url: "<?php echo base_url('index.php/superadmin') ?>/editdriverpassword",
                    type: 'POST',
                    data: {
                        newpass: newpass,
                        val: $('.checkbox:checked').val()
                    },
                    dataType: 'JSON',
                    success: function (response)
                    {
                        if (response.flag != 1) {
                            $(".close").trigger('click');
                            var size = $('input[name=stickup_toggler]:checked').val()
                            var modalElem = $('#confirmmodelss');
                            if (size == "mini")
                            {
                                $('#modalStickUpSmall').modal('show')
                            } else
                            {
                                $('#confirmmodelss').modal('show')
                                if (size == "default") {
                                    modalElem.children('.modal-dialog').removeClass('modal-lg');
                                } else if (size == "full") {
                                    modalElem.children('.modal-dialog').addClass('modal-lg');
                                }
                            }
                            $("#errorboxdatass").text(<?php echo json_encode(POPUP_DRIVERS_NEWPASSWORD); ?>);
                            $("#confirmedss").hide();
                            $(".newpass").val('');
                            $(".confirmpass").val('');
                        } else if (response.flag == 1)
                        {
                            alert("this password already exists. Enter new password");
                            $("#errorboxdatass").text(<?php echo json_encode(POPUP_DRIVERS_NEWPASSWORD_FAILED); ?>);
                        }
                        $("#confirmedss").hide();
//                        location.reload();

                    }

                });
            }

        });
        $("#reject").click(function () {
            $("#display-data").text("");
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).
                    get();
            if (val.length > 0) {

                //      if (confirm("Are you sure to inactive " + val.length + " passengers"))

                var size = $('input[name=stickup_toggler]:checked').val()
                var modalElem = $('#confirmmodels');
                if (size == "mini")
                {
                    $('#modalStickUpSmall').modal('show')
                } else
                {
                    $('#confirmmodels').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }
                $("#errorboxdatas_reject").text(<?php echo json_encode(POPUP_DRIVERS_DEACTIVAT); ?>);
                $("#confirmeds").click(function () {

                    $.ajax({
                        url: "<?php echo base_url('index.php/superadmin') ?>/rejectdrivers",
                        type: "POST",
                        data: {val: val},
                        dataType: 'json',
                        success: function (result)
                        {
                            $('#confirmmodels').modal('show')
                            $("#errorboxdatas_reject").text(<?php echo json_encode(POPUP_COMPANY_DEACTIVATED_DOCTORS); ?>);
                            $('#confirmeds').hide();
                            $('.checkbox:checked').each(function (i) {
                                $(this).closest('tr').remove();
                            });
                        }
                    });
                });
            } else
            {
                //      alert("select atleast one passenger");
                $("#display-data").text(<?php echo json_encode(POPUP_DRIVERS_ATLEAST); ?>);
            }

        });
        $("#editdriver").click(function () {
            $("#display-data").text("");
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
            if (val.length == 0) {
                //         alert("please select any one dispatcher");
                $("#display-data").text(<?php echo json_encode(POPUP_DRIVER_EDIT); ?>);
            } else if (val.length > 1)
            {

                //     alert("please select only one to edit");
                $("#display-data").text(<?php echo json_encode(POPUP_DRIVER_ONLYEDIT); ?>);
            } else
            {
                window.location = "<?php echo base_url() ?>index.php/superadmin/editdriver/" + val;
                $.ajax({
                    url: "<?php echo base_url('index.php/superadmin') ?>/editdriver",
                    type: "POST",
                    data: {val: val},
                    dataType: 'json',
                    success: function (result)
                    {
                    }
                });
            }
        });
        $("#joblogs").click(function () {

            $("#display-data").text("");
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
            if (val.length == 0) {
                //         alert("please select any one dispatcher");
                $("#display-data").text(<?php echo json_encode(POPUP_DRIVER_EDIT); ?>);
            } else if (val.length > 1)
            {

                //     alert("please select only one to edit");
                $("#display-data").text(<?php echo json_encode(POPUP_DRIVER_ONLYEDIT); ?>);
            } else
            {
                window.location = "<?php echo base_url() ?>index.php/superadmin/joblogs/" + val;
            }
        });
    });</script>


<script type="text/javascript">
    $(document).ready(function () {



        var status = '<?php echo $status; ?>';
        var table = $('#big_table1');
        $('#big_table1').hide();
        $('#big_table_processing').show();
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url() ?>index.php/superadmin/datatable_drivers/my/' + status,
//            "bJQueryUI": true,
//            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "aoColumns": [
                {"sWidth": "2%", "sClass": "text-center"},
                {"sWidth": "10%"},
                {"sWidth": "8%"},
                {"sWidth": "10%"},
                {"sWidth": "10%"},
                {"sWidth": "2%"},
                {"sWidth": "10%", "sClass": "text-center"},
                {"sWidth": "7%", "sClass": "text-center"},
                {"sWidth": "5%", "sClass": "text-center"},
                {"sWidth": "5%", "sClass": "text-center"},
                {"sWidth": "10%"},
                {"sWidth": "2%", "sClass": "text-center"},
                {"sWidth": "2%", "sClass": "text-center"}
            ],
            "order": [[0, "desc"]],
            "oLanguage": {
                "sProcessing": "<img src='<?php echo base_url() ?>theme/assets/img/ajax-loader_dark.gif'>"
            },
            "drawCallback": function () {
                $('#big_table_processing').hide();
            },
            "fnInitComplete": function () {
                //oTable.fnAdjustColumnSizing();
                $('#big_table1').show();
                $('#big_table_processing').hide();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };
        table.dataTable(settings);
        $('#pricepln').change(function () {
            $('#big_table_processing').show();
            table.fnFilter($(this).val());
        });
//         search box for table
        $('#search-table').keyup(function () {
            $('#big_table_processing').show();
            table.fnFilter($(this).val());
        });
        table.dataTable(settings);
        if (status == 1) {

            $("#display-data").text("");
//            alert('asdf');
            $('#driverresetpassword').show();
            $('#chekdel').show();
            $('#reject').show();
            $('#accept').show();
            $('#add').show();
            $('#editdriver').show();
            $('#selectedcity').hide();
            $('#companyid').hide();
            $('#makeon').hide();
            $('#makelog').hide();
            $('#makeoff').hide();
        }

        $('#my1').click(function () {
            $("#display-data").text("");
            $('#driverresetpassword').show();
            $('#chekdel').show();
            $('#reject').show();
            $('#accept').show();
            $('#accept').show();
            $('#add').show();
            $('#editdriver').show();
            $('#selectedcity').hide();
            $('#companyid').hide();
            $('#document_data').show();
            $('#makeon').hide();
            $('#makelog').hide();
            $('#makeoff').hide();
        });

        $('#my3').click(function () {
            $("#display-data").text("");
            $('#driverresetpassword').show();
            $('#chekdel').show();
            $('#reject').show();
            $('#accept').hide();
            $('#add').hide();
            $('#editdriver').show();
            $('#selectedcity').hide();
            $('#companyid').hide();
            $('#document_data').show();
            $('#makeoff').hide();
            $('#makelog').hide();
            $('#makeon').hide();
        });
        $('#my4').click(function () {
            $("#display-data").text("");
            $('#driverresetpassword').show();
            $('#chekdel').show();
            $('#reject').hide();
            $('#accept').show();
            $('#add').hide();
            $('#editdriver').show();
            $('#selectedcity').hide();
            $('#companyid').hide();
            $('#document_data').show();
            $('#makeon').hide();
            $('#makelog').hide();
            $('#makeoff').hide();
        });
        $('#mo3').click(function () {

            $("#display-data").text("");
            $('#driverresetpassword').hide();
            $('#chekdel').hide();
            $('#reject').hide();
            $('#accept').hide();
            $('#editdriver').hide();
            $('#joblogs').hide();
            $('#driverresetpassword').hide();
            $('#document_data').hide();
            $('#add').hide();
            $('#selectedcity').hide();
            $('#companyid').hide();
            $('#makeoff').show();
            $('#makelog').show();
            $('#makeon').hide();
        });
        $('#mo30').click(function () {
            $("#display-data").text("");
            $('#driverresetpassword').hide();
            $('#chekdel').hide();
            $('#reject').hide();
            $('#accept').hide();
            $('#editdriver').hide();
            $('#joblogs').hide();
            $('#driverresetpassword').hide();
            $('#document_data').hide();
            $('#add').hide();
            $('#selectedcity').hide();
            $('#companyid').hide();
            $('#makeon').show();
            $('#makelog').show();
            $('#makeoff').hide();
        });
        $('#mo567').click(function () {
            $("#display-data").text("");
            $('#driverresetpassword').hide();
            $('#chekdel').hide();
            $('#editdriver').hide();
            $('#joblogs').hide();
            $('#driverresetpassword').hide();
            $('#document_data').hide();
            $('#reject').hide();
            $('#accept').hide();
            $('#add').hide();
            $('#selectedcity').hide();
            $('#companyid').hide();
            $('#makeon').hide();
            $('#makelog').hide();
            $('#makeoff').hide();
        });
        $('#my6').click(function () {
            $("#display-data").text("");
            $('#driverresetpassword').hide();
            $('#chekdel').hide();
            $('#editdriver').hide();
            $('#joblogs').hide();
            $('#driverresetpassword').hide();
            $('#document_data').hide();
            $('#reject').hide();
            $('#accept').hide();
            $('#add').hide();
            $('#selectedcity').hide();
            $('#companyid').hide();
            $('#makeon').hide();
            $('#makelog').hide();
            $('#makeoff').hide();
        });

    
$(".hide_pro").hide();
$("#opoption1").attr('checked',true);

        $(document).on('change','input[name=opoption]',function(){
           $(".hide_pro").toggle();
        });

//change(function () {
//            // do some stuff
//            console.log("sdsd");
//            $(".hide_pro").show()
//        });


        $('.changeMode').click(function () {

            $('#search-table').val('');
            $("#display-data").text("");
            var status = '<?php echo $status; ?>';
//
            if (status == "my1") {
                $('#selectedcity').hide();
                $('#companyid').hide();
            } else if (status == "my3") {
                $('#selectedcity').hide();
                $('#companyid').hide();
            }



            var table = $('#big_table1');
            $('#big_table1').hide();
            $('#big_table_processing').show();
            var settings = {
//                "autoWidth": false,
                "sDom": "<'table-responsive't><'row'<p i>>",
                "destroy": true,
                "scrollCollapse": true,
                "iDisplayLength": 20,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": $(this).attr('data'),
//                "bJQueryUI": true,
//                "sPaginationType": "full_numbers",
                "iDisplayStart ": 20,
                "aoColumns": [
                    {"sWidth": "2%", "sClass": "text-center"},
                    {"sWidth": "10%"},
                    {"sWidth": "8%"},
                    {"sWidth": "10%"},
                    {"sWidth": "10%"},
                    {"sWidth": "2%", "sClass": "text-center"},
                    {"sWidth": "2%", "sClass": "text-center"},
                    {"sWidth": "7%", "sClass": "text-center"},
                    {"sWidth": "5%", "sClass": "text-center"},
                    {"sWidth": "5%", "sClass": "text-center"},
                    {"sWidth": "10%"},
                    {"sWidth": "2%", "sClass": "text-center"},
                    {"sWidth": "2%", "sClass": "text-center"}
                ],
                "order": [[0, "desc"]],
                "oLanguage": {
                    "sProcessing": "<img src='<?php echo APP_SERVER_HOST ?>pics/ajax-loader_dark.gif'>"
                },
                "fnInitComplete": function () {
                    //oTable.fnAdjustColumnSizing();
                    $('#big_table1').show();
                    $('#big_table_processing').hide();
                },
                "drawCallback": function () {
                    $('#big_table_processing').hide();
                },
                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                },
                'fnServerData': function (sSource, aoData, fnCallback)
                {
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });
                }
            };
            $('.tabs_active').removeClass('active');
            $(this).parent().addClass('active');
            table.dataTable(settings);
            $('#search-table').keyup(function () {
                $('#big_table_processing').show();
                table.fnFilter($(this).val());

            });
            $('#pricepln').change(function () {
                $('#big_table_processing').show();
                table.fnFilter($(this).val());
            });
        });
    });
    function refreshTableOnCityChange() {

//        var table = $('#big_table_processing');
//        $("#display-data").text("");


        $('#big_table_processing').toggle();
        var table = $('#big_table1');
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": $(".whenclicked li.active").children('a').attr('data'),
//            "bJQueryUI": true,
//            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "oLanguage": {
                "sProcessing": "<img src='<?php echo APP_SERVER_HOST ?>pics/ajax-loader_dark.gif'>"
            },
            "drawCallback": function () {
                $('#big_table_processing').hide();
            },
            "fnInitComplete": function () {
                //oTable.fnAdjustColumnSizing();


            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };
        table.dataTable(settings);
        // search box for table
        $('#search-table').keyup(function () {
            $('#big_table_processing').show();
            table.fnFilter($(this).val());
        });
        $('#pricepln').change(function () {
            $('#big_table_processing').show();
            table.fnFilter($(this).val());
        });
    }
    function deleteDriver(ids)
    {
        var val = [];
        val.push(ids);
        $('#deletedriver').modal('show')


        $("#yesdelete").click(function () {

            $.ajax({
                url: "<?php echo base_url('index.php/superadmin') ?>/deleteDrivers",
                type: "POST",
                data: {masterid: val},
                success: function (result) {

                    console.log(result);
                }
            });
        });
    }
</script>


<style>
    /*    .dataTables_wrapper .dataTables_info{
            padding: 0;
        }*/
    .exportOptions{
        display: none;
    }
    .btn-cons {
        margin-right: 5px;
        min-width: 102px;
    }
    .btn{
        font-size: 11px;
    }
    .header{
        height:60px !important;
    }
    .header h3{
        margin:10px 0 !important;
    }
</style>

<div class="content">
    <div class="jumbotron  no-margin" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;">
                <h3 class="">Page Title</h3>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg">
        <ul class="breadcrumb">
            <li>
                <a href="#" class="active">PROVIDERS</a>
            </li>

        </ul>
        <div class="container-md-height m-b-20">
            <div class="panel panel-transparent">
                <ul class="nav nav-tabs nav-tabs-simple bg-white" role="tablist" data-init-reponsive-tabs="collapse">
                    <li id= "my1" class="tabs_active <?php echo $new ?>" style="cursor:pointer">
                        <a  class="changeMode New_" data="<?php echo base_url(); ?>index.php/superadmin/datatable_drivers/my/1"><span><?php echo LIST_NEW; ?></span></a>
                    </li>
                    <li id= "my3" class="tabs_active <?php echo $accept ?>" style="cursor:pointer">
                        <a  class="changeMode accepted_" data="<?php echo base_url(); ?>index.php/superadmin/datatable_drivers/my/3"><span><?php echo LIST_ACCEPTED; ?></span></a>
                    </li>
                    <li id= "my4" class="tabs_active <?php echo $reject ?>" style="cursor:pointer">
                        <a  class="changeMode rejected_" data="<?php echo base_url(); ?>index.php/superadmin/datatable_drivers/my/4"><span><?php echo LIST_REJECTED; ?></span></a>
                    </li>


                    <li id= "mo3" class="tabs_active <?php echo $free ?>" style="cursor:pointer">
                        <a class="changeMode" data="<?php echo base_url(); ?>index.php/superadmin/datatable_drivers/mo/3"><span><?php echo LIST_FREEONLINE; ?></span></a>
                    </li>
                    <li id= "mo30" class="tabs_active <?php echo $offline ?>" style="cursor:pointer">
                        <a class="changeMode" data="<?php echo base_url(); ?>index.php/superadmin/datatable_drivers/mo/30"><span><?php echo LIST_OFFLINE; ?></span></a>
                    </li>
                    <li id= "mo567" class="tabs_active <?php echo $offline ?>" style="cursor:pointer">
                        <a class="changeMode" data="<?php echo base_url(); ?>index.php/superadmin/datatable_drivers/mo/567"><span>BUSY</span></a>
                    </li>
                    <li id= "my6" class="tabs_active <?php echo $offline ?>" style="cursor:pointer">
                        <a class="changeMode" data="<?php echo base_url(); ?>index.php/superadmin/datatable_drivers/my/6"><span>LOGGED OUT</span></a>
                    </li>

                    <div class="pull-right m-t-10"> <button class="btn btn-info btn-cons" id="driverresetpassword"><?php echo BUTTON_RESETPASSWORD; ?></button></a></div>
                    <div class="pull-right m-t-10"> <button class="btn btn-danger btn-cons " id="chekdel"  ><?php echo BUTTON_DELETE; ?></button></a></div>
                    <!--<div class=""><button class="btn btn-primary pull-right m-t-10 " id="editdriver" style="margin-left:10px;margin-top: 5px"><?php echo BUTTON_EDIT; ?></button></div>-->
                    <div class="pull-right m-t-10"> <button class="btn btn-warning btn-cons  " id="reject"  ><?php echo BUTTON_REJECT; ?></button></a></div>
                    <div class="pull-right m-t-10"> <button class="btn btn-success btn-cons  " id="accept"  ><?php echo BUTTON_ACCEPT; ?></button></a></div>
                    <div class="pull-right m-t-10"> <button class="btn btn-primary btn-cons  " id="makeon"  >MAKE ONLINE</button></a></div>
                    <div class="pull-right m-t-10"> <button class="btn btn-primary btn-cons  " id="makeoff"  >MAKE OFFLINE</button></a></div>
                    <div class="pull-right m-t-10"> <button class="btn btn-primary btn-cons  " id="makelog"  >MANUALLY LOGOUT</button></a></div>
                    <div class="pull-right m-t-10"><a href="<?php echo base_url() ?>index.php/superadmin/addnewdriver"> <button class="btn btn-primary btn-cons  " id="add" ><?php echo BUTTON_ADD; ?></button></a></div>
                    <div class="pull-right m-t-10"><button class="btn btn-info btn-cons   " id="document_data" ><?php echo BUTTON_DOCUMENT ?></button></div>
                </ul>
                <div class="panel-group visible-xs" id="LaCQy-accordion"></div>
                <div class="tab-content">
                    <div class="tab-pane active" id="hlp_txt">
                        <div class="row panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title"></div>
                                <div class="error-box" id="display-data" style="text-align:center"></div>
                                <div id="big_table_processing" class="dataTables_processing" style=""><img src="<?php echo APP_SERVER_HOST ?>pics/ajax-loader_dark.gif"></div>
                                <div class='pull-right cls110'>

                                    <div class="pull-right">
                                        <input type="text" id="search-table" class="form-control pull-right" placeholder="<?php echo SEARCH; ?>"> </div>
                                </div>

                            </div>
                            <br>
                            <div class="panel-body no-padding">
                                <?php echo $this->table->generate(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>






    <div class="modal fade stick-up" id="confirmmodel" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">

                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>

                    </div>

                </div>
                <br>
                <div class="modal-body">

                    <div class="form-group">
                        <div class="row">

                            <div class="error-box" id="errorboxdatas" style="font-size: large;text-align:center"><?php echo VEHICLEMODEL_DELETE; ?></div>

                        </div>
                            <!--<label for="fname" class="col-sm-4 control-label"><?php echo FIELD_VEHICLE_SELECTCOMPANY; ?><span style="color:red;font-size: 18px">*</span></label>-->
                        <!--                    <div class="col-sm-6 error-box" >
                                                <select class="form-control" id="company_select">
                                                    <option value="0">Select Company</option>
                        <?php
//                            $this->load->database();
//                            $query = $this->db->query('select * from  company_info WHERE  status = 3')->result();
//                            foreach ($query as $result) {
//
//
//                                echo '<option value="' . $result->company_id . '">' . $result->companyname . '</option>';
//                            }
                        ?>
                                                </select>
                                            </div>-->

                    </div>
                    <br>

                </div>

                <br>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-2" ></div>
                        <div class="col-sm-6 " id="ve_compan"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="confirmed" ><?php echo BUTTON_YES; ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>







    <div class="modal fade stick-up" id="confirmmodels" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">

                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>

                    </div>

                </div>
                <br>
                <div class="modal-body">
                    <div class="row">

                        <div class="error-box" id="errorboxdatas_reject" style="font-size: large;text-align:center"><?php echo VEHICLEMODEL_DELETE; ?></div>

                    </div>
                </div>

                <br>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="confirmeds" ><?php echo BUTTON_OK; ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



    <div class="modal fade stick-up" id="myModal1_driverpass" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">

                    <div class="modal-header">

                        <div class=" clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                            </button>

                        </div>
                        <h3> <?php echo LIST_RESETPASSWORD_HEAD; ?></h3>
                    </div>


                    <br>
                    <br>

                    <div class="modal-body">




                        <div class="form-group" class="formex">
                            <label for="fname" class="col-sm-4 control-label"> <?php echo FIELD_NEWPASSWORD; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-6">
                                <input type="text"  id="newpass" name="latitude"  class="newpass form-control" placeholder="eg:g3Ehadd">
                            </div>
                        </div>

                        <br>
                        <br>

                        <div class="form-group" class="formex">
                            <label for="fname" class="col-sm-4 control-label"><?php echo FIELD_CONFIRMPASWORD; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-6">
                                <input type="text"  id="confirmpass" name="longitude" class="confirmpass form-control" placeholder="H3dgsk">
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-4" ></div>
                            <div class="col-sm-4 error-box" id="errorpass_driversmsg"></div>
                            <div class="col-sm-4" >
                                <button type="button" class="btn btn-primary pull-right" id="editpass_msg" ><?php echo BUTTON_SUBMIT; ?></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

    </div>




    <div class="modal fade stick-up" id="confirmmodelss" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">

                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>

                    </div>

                </div>
                <br>
                <div class="modal-body">
                    <div class="row">

                        <div class="error-box" id="errorboxdatass" style="font-size: large;text-align:center"><?php echo VEHICLEMODEL_DELETE; ?></div>

                    </div>
                </div>

                <br>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="confirmedss" ><?php echo BUTTON_OK; ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade stick-up" id="deletedriver" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">

                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>

                    </div>

                </div>
                <br>
                <div class="modal-body">

                    <div class="row">

                        <div class="error-box" id="errorbox" style="font-size: large;text-align:center"><?php echo VEHICLEMODEL_DELETE; ?></div>

                    </div>
                </div>

                <br>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="yesdelete" ><?php echo BUTTON_YES; ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade stick-up" id="acceptdrivermsg" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">

                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>

                    </div>

                </div>
                <br>
                <div class="modal-body">

                    <div class="row">

                        <div class="error-box" id="errorbox_accept" style="font-size: large;text-align:center"><?php echo VEHICLEMODEL_DELETE; ?></div>

                    </div>
                </div>

                <br>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="accepted_msg" ><?php echo BUTTON_OK; ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade stick-up" id="myModaldocument" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">

                    <div class="modal-header">

                        <div class=" clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                            </button>

                        </div>
                        <h3> <?php echo LIST_RESETPASSWORD_DRIVERDOCUMENTS; ?></h3>
                    </div>


                    <br>
                    <br>

                    <div class="modal-body">

                        <div id="tableWithSearch_wrapper" class="dataTables_wrapper form-inline no-footer"><div class="table-responsive"><table class="table table-hover demo-table-search dataTable no-footer" id="big_table" role="grid" aria-describedby="tableWithSearch_info">


                                    <thead>

                                        <tr role="row">
                                            <th  rowspan="1" colspan="1" aria-sort="ascending"  style="width: 100PX;font-size: 14px"><?php echo DRIVERS_TABLE_DRIVER_DOCUMENT; ?></th>
                                            <th  rowspan="1" colspan="1" aria-sort="ascending"  style="width: 100PX;font-size: 14px"><?php echo DRIVERS_TABLE_DRIVER_EXPIREDATE; ?></th>
                                            <th  rowspan="1" colspan="1" aria-sort="ascending"  style="width: 100PX;font-size: 14px"><?php echo DRIVERS_TABLE_DRIVER_VIEW; ?></th>

                                        </tr>


                                    </thead>
                                    <tbody id="doc_body">

                                    </tbody>
                                </table>

                                <div class="row">
                                    <div class="col-sm-4" ></div>
                                    <div class="col-sm-4 error-box" id="errorpass"></div>
                                    <div class="col-sm-4" >
                                        <button type="button" class="btn btn-primary pull-right" id="documentok" ><?php echo BUTTON_OK; ?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>


            </div>
        </div> </div>


    <div class="modal fade stick-up" id="catlist" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" style="width: 350px">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="modal-header">
                        <div class=" clearfix text-left">
                            <button type="button" id="close_nos" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                            </button>
                        </div>
                        <h3> <?php echo ADD_CATEGORY_TEXT; ?></h3>
                    </div>
                    <br>
                    <form id="callistform" action="" method= "post" enctype="multipart/form-data" onsubmit="return validateForm();"> 
                        <div class="modal-body">
                            <div class="form-group"  id="catdata" class="formex">                            
                            </div>                                    
                            <input type="hidden" name="proid" value="" id="proid"/>
                        </div>
                        <div class="row">
                            <div class="col-sm-4" ></div>
                            <div class="col-sm-4 error-box" id="addsubcat"></div>
                            <div class="col-sm-4" >
                                <button type="button" class="btn btn-primary pull-right" id="acceptcat" ><?php echo BUTTON_SAVE; ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>






    <div class="modal fade stick-up" id="ShowCategory" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" style="width: 350px">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="modal-header">
                        <div class=" clearfix text-left">
                            <button type="button" id="close_nos" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                            </button>
                        </div>
                        <h3> <?php echo LIST_CATEGORY_TEXT; ?></h3>
                    </div>
                    <br>
                    <!--<form id="callistform" action="" method= "post" enctype="multipart/form-data" onsubmit="return validateForm();">--> 
                    <div class="modal-body">
                        <div class="col-sm-1" id="ShowCategoryDataLoading" style="display: none;">

                        </div>
                        <div class="form-group"  id="ShowCategoryData" class="formex">                            
                        </div>                                 
                        <!--<input type="hidden" name="proid" value="" id="proid"/>-->
                    </div>
                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <!--<div class="col-sm-4 error-box" id="addsubcat"></div>-->
                        <!--                        <div class="col-sm-4" >
                                                    <button type="button" class="btn btn-primary pull-right" id="acceptcat" ><?php echo BUTTON_SAVE; ?></button>
                                                </div>-->
                    </div>
                    <!--</form>-->
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade stick-up in" id="coupon_dis" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;">
        <div class="modal-dialog " style="width:305px;">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                </button>
                <div class="container-xs-height full-height">
                    <div class="row-xs-height">
                        <div class="modal-body col-xs-height coupondisData  ">

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <script>

       

    </script>