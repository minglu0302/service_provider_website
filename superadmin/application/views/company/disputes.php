<?php
date_default_timezone_set('UTC');
$rupee = "$";
//error_reporting(0);

if ($status == 1) {
    $passenger_status = 'active';
    $active = "active";
    echo '<style> .searchbtn{float: right;  margin-right: 0px;}.dltbtn{float: right;}</style>';
} else if ($status == 2) {
    $passenger_status = 'deactive';
    $deactive = "active";
}
?>

<script type="text/javascript">
    $(document).ready(function () {



        var status = '<?php echo $status; ?>';

        var table = $('#big_table');


        $('.whenclicked li').click(function () {

            if ($(this).attr('id') == 1) {
                $('#btnStickUpSizeToggler').show();
                 $("#error").text("");
               
            }
            else if ($(this).attr('id') == 2) {

                $('#btnStickUpSizeToggler').hide();
                 $("#error").text("");
                 $('#big_table').dataTable().fnSetColumnVis([8], false);
            }


        });
         $('#big_table_processing').show();

        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url() ?>index.php/superadmin/datatable_disputes/' + status,
//            "bJQueryUI": true,
//            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "oLanguage": {
                "sProcessing": "<img src='http://107.170.66.211/roadyo_live/sadmin/theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {
                //oTable.fnAdjustColumnSizing();
                 $('#big_table_processing').hide();
            },
                      "drawCallback": function () {
                $('#big_table_processing').hide();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };




        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function () {
          $('#big_table_processing').show();
            table.fnFilter($(this).val());
        });



        $('.changeMode').click(function () {
             $("#error").text("");
            var table = $('#big_table');
             $('#big_table_processing').show();

            var settings = {
                "autoWidth": false,
                "sDom": "<'table-responsive't><'row'<p i>>",
                "destroy": true,
                "scrollCollapse": true,
                "iDisplayLength": 20,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": $(this).attr('data'),
//                "bJQueryUI": true,
//                "sPaginationType": "full_numbers",
                "iDisplayStart ": 20,
                "oLanguage": {
                    "sProcessing": "<img src='http://107.170.66.211/roadyo_live/sadmin/theme/assets/img/ajax-loader_dark.gif'>"
                },
                "fnInitComplete": function () {
                    //oTable.fnAdjustColumnSizing();
                     $('#big_table_processing').hide();

                },
                          "drawCallback": function () {
                $('#big_table_processing').hide();
            },
                'fnServerData': function (sSource, aoData, fnCallback)
                {
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });
                },
            };

            $('.tabs_active').removeClass('active');

            $(this).parent().addClass('active');



            table.dataTable(settings);

            // search box for table
            $('#search-table').keyup(function () {
              $('#big_table_processing').show();
                table.fnFilter($(this).val());
            });

        });
        
//        $('#companyid').change(function () {


     function refreshTableOnCityChange(){
      $("#error").text("");
            var table = $('#big_table');
             $('#big_table_processing').show();

            var settings = {
                "autoWidth": false,
                "sDom": "<'table-responsive't><'row'<p i>>",
                "destroy": true,
                "scrollCollapse": true,
                "iDisplayLength": 20,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": $(".whenclicked li.active").children('a').attr('data'),
//                "bJQueryUI": true,
//                "sPaginationType": "full_numbers",
                "iDisplayStart ": 20,
                "oLanguage": {
                    "sProcessing": "<img src='<?php echo base_url(); ?>theme/assets/img/ajax-loader_dark.gif'>"
                },
                "fnInitComplete": function () {
                    //oTable.fnAdjustColumnSizing();
                     $('#big_table_processing').hide();
                },
                          "drawCallback": function () {
                $('#big_table_processing').hide();
            },
                'fnServerData': function (sSource, aoData, fnCallback)
                {
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });
                },
            };

            table.dataTable(settings);

            // search box for table
            $('#search-table').keyup(function () {
              $('#big_table_processing').show();
                table.fnFilter($(this).val());
            });

        }
    });
</script>

<script>

    $(document).ready(function () {
        $("#define_page").html("Disputes");
        $('.disputes').addClass('active');
        $('.disputes').attr('src',"<?php echo base_url();?>/theme/icon/dispuite_on.png");
//        $('.disputes_thumb').addClass("bg-success");



        $('.error-box-class').keypress(function () {
            $('.error-box').text('');
        });



        $("#btnStickUpSizeToggler").click(function () {

            $("#display-data").text("");

            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();


            if (val.length == 0) {
                //      alert("please select any one to resolve");
                $("#display-data").text(<?php echo json_encode(POPUP_DISPUTES_ANYONE); ?>);
            } else if (val.length > 1) {

                //       alert("please select only one to resolve");
                $("#display-data").text(<?php echo json_encode(POPUP_DISPUTES_ONLYONE); ?>);
            }
            else {



                var size = $('input[name=stickup_toggler]:checked').val()
                var modalElem = $('#myModal');
                if (size == "mini") {
                    $('#modalStickUpSmall').modal('show')
                } else {
                    $('#myModal').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }
            }
        });








        $("#insert").click(function () {

            $("#displayerror").text("");

            var val = $('.checkbox:checked').val();

            var message = $("#message").val();
            var test = /[^a-zA-Z0-9\s]/;

            if (message == "" || message == null)
            {
                //     alert("please enter the message");
                $("#displayerror").text(<?php echo json_encode(POPUP_MESSAGE); ?>);
            }
//            else if (test.test(message))
//            {
//                //    alert("please enter the valid data");
//                $("#displayerror").text(<?php echo json_encode(POPUP_COMPAIGNS_TEXT); ?>);
//            }

            else {

                $.ajax({
                    type: 'POST',
                    url: "<?php echo base_url('index.php/superadmin') ?>/resolvedisputes",
                    data: {val: val,
                        message: message},
                    dataType: 'JSON',
                    success: function (response)
                    {


                        $('.checkbox:checked').each(function (i) {
                            $(this).closest('tr').remove();
                        });

                        $(".close").trigger('click');

                    }

                });
            }
        });
    });

</script>

<style>
    .table-responsive{
        overflow-x: auto;
    }
    .exportOptions{
        display: none;
    }
         .header{
        height:60px !important;
    }
    .header h3{
        margin:10px 0 !important;
    }
</style>

<div class="content">
    <div class="jumbotron  no-margin" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;">
                <h3 class="">Page Title</h3>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg">
        <ul class="breadcrumb">
            <li>
                <a href="#" class="active">DISPUTES</a>
            </li>
             
        </ul>
        <div class="container-md-height m-b-20">
            <div class="panel panel-transparent">
                <ul class="nav nav-tabs nav-tabs-simple bg-white whenclicked li" role="tablist" data-init-reponsive-tabs="collapse">
                    <li  id="1" class="tabs_active <?php echo ($status == 1 ? "active" : ""); ?>">
                            <a  class="changeMode"  data="<?php echo base_url(); ?>index.php/superadmin/datatable_disputes/1"><span><?php echo LIST_DISPUTES_REPORTED; ?></span></a>
                        </li>
                        <li  id="2" class="tabs_active <?php echo ($status == 2 ? "active" : ""); ?>">
                            <a  class="changeMode"  data="<?php echo base_url(); ?>index.php/superadmin/datatable_disputes/2"><span><?php echo LIST_DISPUTES_RESOLVED; ?> </span></a>
                        </li>

                        <div class="pull-right m-t-10"> <button class="btn btn-primary btn-cons" id="btnStickUpSizeToggler" ><?php echo BUTTON_RESOLVE; ?></button></div>


                        <div class="pull-right m-t-10" style="margin-right: 10px; width: 150px;">

 
                        </div>

                </ul>
                <div class="panel-group visible-xs" id="LaCQy-accordion"></div>
                <div class="tab-content">
                    <div class="tab-pane active" id="hlp_txt">
                        <div class="row panel panel-default">
                            <div class="panel-heading">
                                 <div class="error-box" id="display-data" style="text-align:center"></div>
                                        <div id="big_table_processing" class="dataTables_processing" style=""><img src="<?php echo APP_SERVER_HOST ?>pics/ajax-loader_dark.gif"></div>

                                
                                            <div class="searchbtn row clearfix pull-right" >

                                                <div class="pull-right" style="margin-right: 15px;" ><input type="text" id="search-table" class="form-control pull-right" placeholder="<?php echo SEARCH;?>"> </div>
                                            </div>
                                       
                                            <div class="dltbtn">
                               
                                    
                                </div>
                                <br>
                                <div class="panel-body no-padding">
                                    <?php echo $this->table->generate(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 

  
 
   
<div class="modal fade stick-up" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">

                <div class="modal-header">

                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>

                    </div>
                    <h3> <?php echo LIST_DISPUTES_EDITDISPUTE; ?></h3>
                </div>



                <br>



                <div class="form-group" class="formex">
                    <label for="fname" class="col-sm-4 control-label">    <?php echo FIELD_DISPUTES_MANAGEMENTNOTE; ?><span style="color:red;font-size: 18px">*</span></label>
                    <div class="col-sm-6">
                        <textarea  id="message" name="latitude"  class="form-control error-box-class" placeholder=""></textarea>
                    </div>
                </div>

                <br>


                <div class="row ">
                    <div class="col-sm-4" ></div>
                    <div class="col-sm-4 error-box" id="displayerror" ></div>
                    <div class="col-sm-4" >
                        <button type="button" class="btn btn-primary pull-right" id="insert" ><?php echo BUTTON_SUBMIT; ?></button>
                    </div>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
    </button>
</div>

<div class="modal fade stick-up" id="confirmmodel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                <div class=" clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>

                </div>

            </div>
            <br>
            <div class="modal-body">
                <div class="row">

                    <div class="error-box" id="errorboxdata" style="font-size: large;text-align:center"></div>

                </div>
            </div>

            <br>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

