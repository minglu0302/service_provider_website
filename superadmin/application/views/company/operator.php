<?php
date_default_timezone_set('UTC');
$rupee = "$";
?>
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<style>
    .ui-autocomplete{
        z-index: 5000;
    }
    #selectedcity,#companyid{
        display: none;
    }
    th { font-size: 12px; }
    .ui-menu-item{cursor: pointer;background: black;color:white;border-bottom: 1px solid white;width: 200px;}
</style>
<script>
    $(document).ready(function () {
        $('.cities').addClass('active');
        $('.cities_thumb').attr('src', "<?php echo base_url(); ?>/theme/icon/cities_on.png");

        $('.error-box-class').keypress(function () {
            $('.error-box').text('');
        });

        $('#btnStickUpSizeToggler').click(function () {
            $("#display-data").text("");
            var size = $('input[name=stickup_toggler]:checked').val()
            var modalElem = $('#myModal');
            if (size == "mini") {
                $('#modalStickUpSmall').modal('show')
            } else {
                $('#myModal').modal('show')
                if (size == "default") {
                    modalElem.children('.modal-dialog').removeClass('modal-lg');
                } else if (size == "full") {
                    modalElem.children('.modal-dialog').addClass('modal-lg');
                }
            }
        });



        $('#editoperatorbtn').click(function () {
            $("#display-data").text("");
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();

            if (val.length == 0 || val.length > 1) {
                $("#display-data").text("select one operator to edit");
            } else {
                var size = $('input[name=stickup_toggler]:checked').val();
                var modalElem = $('#myanotherModal');
                $('#onameedit').val($('.checkbox:checked').parent().prev().prev().prev().prev().prev().prev().text());
                $('#oaddressedit').val($('.checkbox:checked').parent().prev().prev().prev().prev().prev().text());
                $('#mnameedit').val($('.checkbox:checked').parent().prev().prev().prev().prev().text());
                $('#ophoneedit').val($('.checkbox:checked').parent().prev().prev().prev().text());
                $('#oemailedit').val($('.checkbox:checked').parent().prev().prev().text());
                if (size == "mini") {
                    $('#modalStickUpSmall').modal('show')
                } else
                {
                    $('#myanotherModal').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }
            }
        });

        //var opassword = $("#opasswordedit").val();
//            var ocpassword = $("#ocpasswordedit").val();
// else if (opassword == "" || opassword == null)
//            {
//                $("#opassworderr").text("Enter Operator password");
//            } else if (ocpassword == "" || ocpassword == null)
//            {
//                $("#ocpassworderr").text("Enter Operator Confirm passworrd");
//            }
// else if (opassword != ocpassword)
//            {
//                $("#opassworderr").text("Password Mismatched");
//            }


        $('#editpassbtn').click(function () {
            $("#display-data").text("");
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
            if (val.length == 0 || val.length > 1) {
                $("#display-data").text("select one operator to edit password");
            } else {

                var size = $('input[name=stickup_toggler]:checked').val();
                var modalElem = $('#myanotherModal');
                if (size == "mini") {
                    $('#modalStickUpSmall').modal('show')
                } else
                {
                    $('#editpassmodel').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }
            }
        });





        $("#insert").click(function () {

            var oname = $("#oname").val();
            var oaddress = $("#oaddress").val();
            var mname = $("#mname").val();
            var ophone = $("#ophone").val();
            var oemail = $("#oemail").val();
            var opassword = $("#opassword").val();
            var ocpassword = $("#ocpassword").val();
            var vemail = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/; //^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            var vphone = /^(1\s|1|)?((\(\d{3}\))|\d{3})(\-|\s)?(\d{3})(\-|\s)?(\d{4})$/;

            if (oname == "" || oname == null)
            {
                $("#addoperatorerr").text("Enter Operator Name");
            } else if (mname == "" || mname == null)
            {
                $("#addoperatorerr").text("Enter Manager Name");
            } else if (oemail == "" || oemail == null)
            {
                $("#addoperatorerr").text("Enter Operator email");
            } else if (!(vemail.test(oemail)))
            {
                $("#addoperatorerr").text("Enter Valid Email");

            } else if (!(vphone.test(ophone)))
            {
                $("#addoperatorerr").text("Enter Valid Phone");

            } else if (opassword == "" || opassword == null)
            {
                $("#addoperatorerr").text("Enter Operator password");
            } else if (ocpassword == "" || ocpassword == null)
            {
                $("#addoperatorerr").text("Enter Operator Confirm passworrd");
            } else if (opassword != ocpassword)
            {
                $("#addoperatorerr").text("Password Mismatched");
            } else
            {
                $.ajax({
                    url: "<?php echo base_url('index.php/superadmin') ?>/insertoperator",
                    type: 'POST',
                    data: {
                        oname: oname,
                        oaddress: oaddress,
                        mname: mname,
                        ophone: ophone,
                        oemail: oemail,
                        opassword: opassword
                    },
                    dataType: 'JSON',
                    success: function (response)
                    {
                        $(".close").trigger("click");
                        var size = $('input[name=stickup_toggler]:checked').val()
                        var modalElem = $('#confirmmodels');
                        if (size == "mini")
                        {
                            $('#modalStickUpSmall').modal('show')
                        } else
                        {
                            $('#confirmmodels').modal('show')
                            if (size == "default") {
                                modalElem.children('.modal-dialog').removeClass('modal-lg');
                            } else if (size == "full") {
                                modalElem.children('.modal-dialog').addClass('modal-lg');
                            }
                        }
                        $("#confirmeds").hide();
                        if (response.flag == '0') {
                            $('#errorboxdatas').text(response.msg);
                            $("#search-box-hidden").val("");
                            $("#oname").val("");
                            $("#oaddress").val("");
                            $("#ophone").val("");
                            $("#oemail").val("");
                            $("#mname").val("");
                            $("#opassword").val("");
                        } else if (response.flag == '1') {
                            $('#errorboxdatas').text(response.msg);
                        }
                        var table = $('#big_table');
                        var settings = {
                            "autoWidth": false,
                            "sDom": "<'table-responsive't><'row'<p i>>",
                            "destroy": true,
                            "scrollCollapse": true,
                            "iDisplayLength": 20,
                            "bProcessing": true,
                            "bServerSide": true,
                            "sAjaxSource": "<?php echo base_url() ?>index.php/superadmin/datatable_operators",
                            "oLanguage": {
                                "sProcessing": "<img src='<?php echo base_url() ?>theme/assets/img/ajax-loader_dark.gif'>"
                            },
                            "fnInitComplete": function () {
                            },
                            'fnServerData': function (sSource, aoData, fnCallback)
                            {
                                $.ajax
                                        ({
                                            'dataType': 'json',
                                            'type': 'POST',
                                            'url': sSource,
                                            'data': aoData,
                                            'success': fnCallback
                                        });
                            }
                        };

                        table.dataTable(settings);

                        // search box for table
                        $('#search-table').keyup(function () {
                            table.fnFilter($(this).val());
                        });

                    }

                });
            }

        });




        $("#editoperator").click(function () {


            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
            if (val.length != 1) {
                alert("select one provider to edit");
                return;
            }
            var oid = val.toString();
            var oname = $("#onameedit").val();
            var oaddress = $("#oaddressedit").val();
            var mname = $("#mnameedit").val();
            var ophone = $("#ophoneedit").val();
            var oemail = $("#oemailedit").val();
            if (oname == "" || oname == null)
            {
                $("#onameerr").text("Enter Operator Name");
            } else if (mname == "" || mname == null)
            {
                $("#oaddresserr").text("Enter Operator Address");
            } else if (oemail == "" || oemail == null)
            {
                $("#oemailerr").text("Enter Operator email");
            } else
            {
                $.ajax({
                    url: "<?php echo base_url('index.php/superadmin') ?>/editoperator",
                    type: 'POST',
                    data: {
                        oid: oid,
                        oname: oname,
                        oaddress: oaddress,
                        mname: mname,
                        ophone: ophone,
                        oemail: oemail
                    },
                    dataType: 'JSON',
                    success: function (response)
                    {
                        $(".close").trigger("click");
                        var size = $('input[name=stickup_toggler]:checked').val()
                        var modalElem = $('#confirmmodels');
                        if (size == "mini")
                        {
                            $('#modalStickUpSmall').modal('show')
                        } else
                        {
                            $('#confirmmodels').modal('show')
                            if (size == "default") {
                                modalElem.children('.modal-dialog').removeClass('modal-lg');
                            } else if (size == "full") {
                                modalElem.children('.modal-dialog').addClass('modal-lg');
                            }
                        }
                        $("#errorboxdatas").text("Operator Updated");
                        $("#confirmeds").hide();
                    }
                });
            }
        });



        $("#editoperatorpass").click(function () {
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
            if (val.length != 1) {
                alert("select one provider to edit");
                return;
            }
            var oid = val.toString();
            var opassword = $("#opasswordedit").val();
            var ocpassword = $("#ocpasswordedit").val();

            if (opassword == "" || opassword == null)
            {
                $("#passupdateerr").text("Enter Operator password");
                return;
            } else if (ocpassword == "" || ocpassword == null)
            {
                $("#passupdateerr").text("Enter Operator Confirm passworrd");
                return;
            } else if (opassword != ocpassword)
            {
                $("#passupdateerr").text("Password Mismatched");
                return;
            } else
            {
                $.ajax({
                    url: "<?php echo base_url('index.php/superadmin') ?>/editoperatorpass",
                    type: 'POST',
                    data: {
                        oid: oid,
                        opassword: opassword
                    },
                    dataType: 'JSON',
                    success: function (response)
                    {
                        $(".close").trigger("click");
                        var size = $('input[name=stickup_toggler]:checked').val()
                        var modalElem = $('#confirmmodels');
                        if (size == "mini")
                        {
                            $('#modalStickUpSmall').modal('show')
                        } else
                        {
                            $('#confirmmodels').modal('show')
                            if (size == "default") {
                                modalElem.children('.modal-dialog').removeClass('modal-lg');
                            } else if (size == "full") {
                                modalElem.children('.modal-dialog').addClass('modal-lg');
                            }
                        }
                        $("#errorboxdatas").text("Operator Password Updated");
                        $("#confirmeds").hide();
                    }
                });
            }
        });


        $('.changeMode').click(function () {

            var table = $('#big_table');


            var settings = {
                "autoWidth": false,
                "sDom": "<'table-responsive't><'row'<p i>>",
                "destroy": true,
                "scrollCollapse": true,
                "iDisplayLength": 20,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "<?php echo base_url() ?>index.php/superadmin/datatable_operators",
                "oLanguage": {
                    "sProcessing": "<img src='http://107.170.66.211/roadyo_live/sadmin/theme/assets/img/ajax-loader_dark.gif'>"
                },
                "fnInitComplete": function () {
                    //oTable.fnAdjustColumnSizing();

                },
                'fnServerData': function (sSource, aoData, fnCallback)
                {
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });
                },
            };
            table.dataTable(settings);

            // search box for table
            $('#search-table').keyup(function () {
                table.fnFilter($(this).val());
            });

        });

        $("#resetpassbtn").click(function () {
            $("#display-data").text("");
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
            if (val.length > 0)
            {
                var size = $('input[name=stickup_toggler]:checked').val()
                var modalElem = $('#confirmmodel');
                if (size == "mini") {
                    $('#modalStickUpSmall').modal('show')
                } else {
                    $('#confirmmodel').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }
                $("#errorboxdata").text("Are you sure you wish to Reset password for this operators?");

                $("#confirmed").click(function () {

                    $.ajax({
                        url: "<?php echo base_url('index.php/superadmin') ?>/oprresetpassword",
                        type: "POST",
                        data: {val: val},
                        dataType: 'json',
                        success: function (result)
                        {
                            $(".close").trigger("click");
                            var size = $('input[name=stickup_toggler]:checked').val()
                            var modalElem = $('#confirmmodels');
                            if (size == "mini")
                            {
                                $('#modalStickUpSmall').modal('show')
                            } else
                            {
                                $('#confirmmodels').modal('show')
                                if (size == "default") {
                                    modalElem.children('.modal-dialog').removeClass('modal-lg');
                                } else if (size == "full") {
                                    modalElem.children('.modal-dialog').addClass('modal-lg');
                                }
                            }
                            if (result.flag == '1') {
                                $("#errorboxdatas").text(result.msg);

                            } else if (result.flag == '0') {
                                $("#errorboxdatas").text(result.msg);
                            }
                            $("#confirmeds").hide();
                        }
                    });
                });

            } else
            {
                $("#display-data").text("please select at least one operator");
            }
        });


        $("#chekdel").click(function () {
            $("#display-data").text("");
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
            if (val.length > 0)
            {
                var size = $('input[name=stickup_toggler]:checked').val()
                var modalElem = $('#confirmmodel');
                if (size == "mini") {
                    $('#modalStickUpSmall').modal('show')
                } else {
                    $('#confirmmodel').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }
                $("#errorboxdata").text("Are you sure you wish to delete this operator?");

                $("#confirmed").click(function () {

                    $.ajax({
                        url: "<?php echo base_url('index.php/superadmin') ?>/deleteoperator",
                        type: "POST",
                        data: {val: val},
                        dataType: 'json',
                        success: function (result)
                        {
                            $(".close").trigger("click");
                            $('.checkbox:checked').each(function (i) {
                                $(this).closest('tr').remove();
                            });
                            var size = $('input[name=stickup_toggler]:checked').val()
                            var modalElem = $('#confirmmodels');
                            if (size == "mini")
                            {
                                $('#modalStickUpSmall').modal('show')
                            } else
                            {
                                $('#confirmmodels').modal('show')
                                if (size == "default") {
                                    modalElem.children('.modal-dialog').removeClass('modal-lg');
                                } else if (size == "full") {
                                    modalElem.children('.modal-dialog').addClass('modal-lg');
                                }
                            }
                            if (result.flag == '1') {
                                $("#errorboxdatas").text(result.msg);

                            } else if (result.flag == '0') {
                                $("#errorboxdatas").text(result.msg);
                            }
                            $("#confirmeds").hide();
                        }
                    });
                });

            } else
            {
                $("#display-data").text("please select at least one operator");
            }

        });


    });










</script>

<script type="text/javascript">
    $(document).ready(function () {

        var table = $('#big_table');

        $('#big_table_processing').show();
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url() ?>index.php/superadmin/datatable_operators',
//            "bJQueryUI": true,
//            "sPaginationType": "full_numbers",
//            "iDisplayStart ": 20,
            "oLanguage": {
                "sProcessing": "<img src='http://107.170.66.211/roadyo_live/sadmin/theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {
                //oTable.fnAdjustColumnSizing();
                $('#big_table_processing').hide();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };



        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function () {
            table.fnFilter($(this).val());
        });

    });
</script>

<style>
    .exportOptions{
        display: none;
    }
</style>
<div class="page-content-wrapper" style="padding-top: 20px">
    <!-- START PAGE CONTENT -->
    <div class="content">
        <!-- START JUMBOTRON -->
        <div class="inner">
            <ul class="breadcrumb" style="margin-left: 20px;font-size: 16px;color:#0090d9;">
                <li><a style="color:#0090d9;" href="<?php echo base_url(); ?>index.php/superadmin/operator/" class="">OPERATORS</a>

            </ul>
        </div>
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <div class="panel panel-transparent ">
                    <ul class="nav nav-tabs nav-tabs-fillup  bg-white">
                        <li class="active" style="cursor:auto">
                            <a><span style="cursor:auto">LIST OF OPERATORS</span></a>
                        </li>
                        <div class="pull-right m-t-10 cls111"> <button class="btn btn-primary btn-cons" id="btnStickUpSizeToggler"><span>ADD OPERATOR</button></a></div>
                    </ul>

                    <div class="container-fluid container-fixed-lg bg-white no-padding">
                        <!-- START PANEL -->
                        <div class="panel panel-transparent no-padding">
                            <div class="panel-heading no-padding">
                                <div class="error-box" id="display-data" style="text-align:center"></div>
                                <div id="big_table_processing" class="dataTables_processing" style=""><img src="http://www.ahmed-samy.com/demos/datatables_2/assets/images/ajax-loader_dark.gif"></div>


                                <div class="pull-right m-t-10" class="btn-group" style="margin-right: 4px">



                                    <div class="pull-right m-t-10" class="btn-group" style="margin-left: 5px">
                                        <button type="button" class="btn btn-success" id="chekdel"><i class="fa fa-trash-o"></i>
                                        </button>
                                    </div>
                                    <div class="pull-right m-t-10" class="btn-group" style="margin-left: 5px">
                                        <button class="btn btn-green btn-lg pull-right" id="editoperatorbtn"   style="line-height: 14px;color: #ffffff !important;margin-right: 2px;background-color: #10cfbd;" class="btn btn-success"  >
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                    </div>
                                    <div class="pull-right m-t-10" class="btn-group" style="margin-left: 5px">
                                        <button type="button" class="btn btn-success" id="editpassbtn">Edit Password
                                        </button>
                                    </div>
                                    <div class="pull-right m-t-10" class="btn-group" style="margin-left: 5px">
                                        <button type="button" class="btn btn-success" id="resetpassbtn">Reset Password
                                        </button>
                                    </div>

                                    <div class="m-t-10 pull-right" class="btn-group searchbtn" style="margin-left: 5px">


                                        <input type="text" id="search-table" class="form-control pull-right"  placeholder="<?php echo SEARCH; ?>"/> 


                                    </div>
                                    <!--<div class=" m-t-10 error-box"  style="margin-left: 5px;text-align:center" id="display-data"></div>-->




                                </div>
                                &nbsp;
                                <div class="panel-body p-r-20  p-l-20  p-t-30">

                                    <?php echo $this->table->generate(); ?>

                                </div>
                            </div>



                        </div>
                    </div>





                </div>

            </div>
            <!-- END PANEL -->
        </div>
    </div>



    <div class="modal fade slide-up disable-scroll in" id="myModal" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;">
        <div class="modal-dialog ">
            <div class="modal-content-wrapper">
                <div class="modal-content">
                    <div class="modal-header clearfix ">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                        <h3>ADD OPERATOR</h3>
                        <p></p>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">                            
                            <br>                            
                            <br>
                            <div class="form-group" class="formex">
                                <label for="fname" class="col-sm-4 control-label">OPERATOR NAME<span style="color:red;font-size: 18px">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text"  id="oname" name="oname"  class="form-control error-box-class" placeholder="operator name">
                                </div>
                            </div>

                            <br>                            
                            <br>
                            <div class="form-group" class="formex">
                                <label for="fname" class="col-sm-4 control-label">OPERATOR ADDRESS<span style="color:red;font-size: 18px"></span></label>
                                <div class="col-sm-6">
                                    <input type="text"  id="oaddress" name="oaddress"  class="form-control error-box-class" placeholder="operator address">
                                </div>
                            </div>

                            <br>                            
                            <br>
                            <div class="form-group" class="formex">
                                <label for="fname" class="col-sm-4 control-label">MANAGER NAME<span style="color:red;font-size: 18px">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text"  id="mname" name="mname"  class="form-control error-box-class" placeholder="manager name">
                                </div>
                            </div>


                            <br>
                            <br>
                            <div class="form-group" class="formex">
                                <label for="fname" class="col-sm-4 control-label">OPERATOR PHONE<span style="color:red;font-size: 18px"></span></label>
                                <div class="col-sm-6">
                                    <input type="text"  id="ophone" name="ophone" class="form-control error-box-class" placeholder="operator phone">
                                </div>
                            </div>


                            <br>
                            <br>
                            <div class="form-group" class="formex">
                                <label for="fname" class="col-sm-4 control-label">OPERATOR EMAIL<span style="color:red;font-size: 18px">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text"  id="oemail" name="oemail" class="form-control error-box-class" placeholder="operator email">
                                </div>
                            </div>

                            <br>
                            <br>
                            <div class="form-group" class="formex">
                                <label for="fname" class="col-sm-4 control-label">PASSWORD<span style="color:red;font-size: 18px">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text"  id="opassword" name="opassword" class="form-control error-box-class" placeholder="password">
                                </div>
                            </div>

                            <br>
                            <br>
                            <div class="form-group" class="formex">
                                <label for="fname" class="col-sm-4 control-label">CONFIRM PASSWORD<span style="color:red;font-size: 18px">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text"  id="ocpassword" name="ocpassword" class="form-control error-box-class" placeholder="confirm password">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4" ></div>
                                <div class="col-sm-4 error-box" id="addoperatorerr"></div>
                                <div class="col-sm-4" >
                                    <button type="button" class="btn btn-primary pull-right" id="insert" ><?php echo BUTTON_ADD; ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
        </button>
        <!-- /.modal-dialog -->
    </div>




    <div class="modal fade stick-up" id="editpassmodel" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="modal-header">
                        <div class=" clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                            </button>
                        </div>
                        <h3> EDIT PASSWORD</h3>
                    </div>

                    <br>                            
                    <br>
                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-4 control-label">PASSWORD<span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <input type="password"  id="opasswordedit" name="opasswordedit"  class="form-control error-box-class" placeholder="operator name">
                        </div>
                    </div>
                    <br>                            
                    <br>
                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-4 control-label">CONFIRM PASSWORD<span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <input type="password"  id="ocpasswordedit" name="ocpasswordedit"  class="form-control error-box-class" placeholder="operator address">
                        </div>
                    </div>                    
                </div>
                <div class="row">
                    <div class="col-sm-4" ></div>
                    <div class="col-sm-4 error-box" id="passupdateerr" ></div>
                    <div class="col-sm-4" >
                        <button type="button" class="btn btn-primary pull-right changeMode" id="editoperatorpass" ><?php echo BUTTON_SUBMIT ?></button>
                    </div>
                </div>
                <br/>
            </div>
        </div>
    </div>


    <div class="modal fade stick-up" id="myanotherModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="modal-header">
                        <div class=" clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                            </button>
                        </div>
                        <h3> EDIT OPERATOR</h3>
                    </div>

                    <br>                            
                    <br>
                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-4 control-label">OPERATOR NAME<span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <input type="text"  id="onameedit" name="onameedit"  class="form-control error-box-class" placeholder="operator name">
                        </div>
                    </div>

                    <br>                            
                    <br>
                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-4 control-label">OPERATOR ADDRESS<span style="color:red;font-size: 18px"></span></label>
                        <div class="col-sm-6">
                            <input type="text"  id="oaddressedit" name="oaddressedit"  class="form-control error-box-class" placeholder="operator address">
                        </div>
                    </div>

                    <br>                            
                    <br>
                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-4 control-label">MANAGER NAME<span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <input type="text"  id="mnameedit" name="mnameedit"  class="form-control error-box-class" placeholder="manager name">
                        </div>
                    </div>


                    <br>
                    <br>
                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-4 control-label">OPERATOR PHONE<span style="color:red;font-size: 18px"></span></label>
                        <div class="col-sm-6">
                            <input type="text"  id="ophoneedit" name="ophoneedit" class="form-control error-box-class" placeholder="operator phone">
                        </div>
                    </div>


                    <br>
                    <br>
                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-4 control-label">OPERATOR EMAIL<span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <input type="text"  id="oemailedit" name="oemailedit" class="form-control error-box-class" placeholder="operator email">
                        </div>
                    </div>                    
                </div>
                <div class="row">
                    <div class="col-sm-4" ></div>
                    <div class="col-sm-4 error-box" id="longlat" ></div>
                    <div class="col-sm-4" >
                        <button type="button" class="btn btn-primary pull-right changeMode" id="editoperator" ><?php echo BUTTON_SUBMIT ?></button>
                    </div>
                </div>
                <br/>
            </div>
        </div>
    </div>
</div>



<div class="modal fade stick-up" id="confirmmodel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                <div class=" clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>

                </div>

            </div>
            <br>
            <div class="modal-body">
                <div class="row">

                    <div class="error-box" id="errorboxdata" style="font-size: large;text-align:center"><?php echo COMPAIGNS_DISPLAY; ?></div>

                </div>
            </div>

            <br>

            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4" ></div>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4" >
                        <button type="button" class="btn btn-primary pull-right" id="confirmed" ><?php echo BUTTON_YES; ?></button>
                    </div>
                </div>
            </div>
        </div>

        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>



<div class="modal fade stick-up" id="confirmmodels" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                <div class=" clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>

                </div>

            </div>
            <br>
            <div class="modal-body">
                <div class="row">

                    <div class="error-box" id="errorboxdatas" style="font-size: large;text-align:center"><?php echo VEHICLEMODEL_DELETE; ?></div>

                </div>
            </div>

            <br>

            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4" ></div>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4" >
                        <button type="button" class="btn btn-primary pull-right" id="confirmeds" ><?php echo BUTTON_YES; ?></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
