<?php
date_default_timezone_set('UTC');
$rupee = "$";
?>
<style>
        .header{
        height:60px !important;
    }
    .header h3{
        margin:10px 0 !important;
    }
    .ui-autocomplete{
        z-index: 5000;
    }
    #selectedcity,#companyid{
        display: none;
    }

    .ui-menu-item{cursor: pointer;background: black;color:white;border-bottom: 1px solid white;width: 200px;}
</style>
<script>
    function LoadCategory()
    {
        var cityid = $("#cityid option:selected").val();
        $.ajax({
            url: "<?php echo base_url('index.php/superadmin') ?>/GetCategoryByCity",
            type: "POST",
            data: {cid: cityid},
            dataType: 'JSON',
            beforeSend: function () {
                $('#catloading').show();
            },
            complete: function () {
                $('#catloading').hide();
            },
            success: function (response)
            {
                $('#catlist').html('');
                $('#catlist').append('<option value="0">Select Category</option>');
                $.each(response, function (key, value)
                {
                    $('#catlist').append('<option value="' + value['id'] + '">' + value['cat_name'] + '</option>');
                });
            }
        });
    }
    function EditLoadCategory()
    {
        var ecityid = $("#ecityid option:selected").val();
        $.ajax({
            url: "<?php echo base_url('index.php/superadmin') ?>/GetCategoryByCity",
            type: "POST",
            data: {cid: ecityid},
            dataType: 'JSON',
            beforeSend: function () {
                $('#ecatloading').show();
            },
            complete: function () {
                $('#ecatloading').hide();
            },
            success: function (response)
            {
                console.log(response);
                $('#ecatlist').html('');
                $('#ecatlist').append('<option value="0">Select Category</option>');
                $.each(response, function (key, value)
                {
                    $('#ecatlist').append('<option value="' + value['id'] + '">' + value['cat_name'] + '</option>');
                });
            }
        });
    }
    $(document).ready(function () {
        $('.provisionen').addClass('active');
        $('.provisionen_thumb').attr('src', "<?php echo base_url(); ?>/theme/icon/comission_on copy.png");
        $('.provisionen_thumb').addClass("bg-success");
        $('#search_by_select').change(function () {
            $('#atag').attr('href', '<?php echo base_url() ?>index.php/superadmin/search_by_select/' + $('#search_by_select').val());
            $("#callone").trigger("click");
        });
        var table = $('#big_table');
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "sPaginationType": "bootstrap",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 20,
            "order": [[0, "desc"]]
        };
        table.dataTable(settings);
        $('#search-table').keyup(function () {
            table.fnFilter($(this).val());
        });

        $('#editbusiness').click(function () {
            var egid = $('#egid').val();
            var ecatlist = $('#ecatlists').val();
           

            var cmand = 0;
            var cmult = 0;
//            if (document.getElementById('ecmand').checked)
//                cmand = 1;
            if (document.getElementById('ecmult').checked)
                cmult = 1;


            var group_name = $('#egroup_name').val();
           if (group_name == "" || group_name == null) {
                $("#clearerror").text("Please enter the group name");
            } else {
                $.ajax({
                    url: "<?php echo base_url('index.php/category') ?>/EditServiceGroup",
                    type: 'POST',
                    data: {
                        
                        gid: egid,
                        ecatlist: ecatlist,
                        group_name: group_name,
                        cmult: cmult,
                        cmand: cmand
                    },
                    dataType: 'JSON',
                    success: function (response)
                    {
                        $('.close').trigger('click');
                        location.reload(true);
                    }
                });
            }
        });


 $('#insertsgroup').click(function () {
            $('.clearerror').text("");
        
                
            var group_name = $('#group_name1').val();
            var category = '<?php echo $catId ?>';
           
            var cmand = 0;
            var cmult = 0;
//            if (document.getElementById('cmand1').checked)
//                cmand = 1;
            if (document.getElementById('cmult1').checked)
                cmult = 1;
            if (category == "" || category == null) {
                $("#addsubcat22").text('Please select the category');
            }else if (group_name == "" || group_name == null) {
                $("#addsubcat22").text('Please enter the group name');
            }
            else {
                $.ajax({
                    url: "<?php echo base_url('index.php/category') ?>/AddServiceGroup",
                    type: 'POST',
                    data: {
                        cid:category,
                        group_name: group_name,
                        cmult: cmult,
                        cmand: cmand
                    },
                    dataType: 'JSON',
                    success: function (response)
                    {
                        $('.close').trigger('click');
                        location.reload(true);
                        
                    }
                });
            }
        });
            

        $('#add_group').click(function () {
            $('#add_model').modal('show');
        });

        $('#edit').click(function () {
            $('#modalHeading').html(<?php echo json_encode(EDIT_SERVICE_GROUP); ?>);
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
            if (val.length == 1)
            {
                var data = $('.checkbox:checked').map(function () {
                    return $(this).attr("data");
                }).get();
                
                var dataarr = data[0].split(",");
                var catid = "<?php echo $catId ?>";
                
                $('#egid').val(dataarr[0]);
               
                $('#ecityid').val(dataarr[1]);
                $('#ecatlists').val(catid);
                $('#egroup_name').val(dataarr[4]);
                $('#editModal').modal('show');
                
                 $.ajax({
                    url: "<?php echo base_url('index.php/category') ?>/EditGetServiceGroup",
                    type: 'POST',
                    data: {
                        egid: (dataarr[0]),
                         
                    },
                    dataType: 'JSON',
                    success: function (response)
                    {
                        console.log(response.group_name);
                        $('#egroup_name').val(response.group_name);
                        if(response.cmand == "1")
                        {
                            $("#ecmand").prop("checked", true);
                        }
                        else
                        {
                            $("#ecmand").prop("checked", false);
                        }
                        
                         if(response.cmult == "1")
                        {
                            $("#ecmult").prop("checked", true);
                        }
                        else
                        {
                            $("#ecmult").prop("checked", false);
                        }
                          
                    }
                });
            } else {
                $("#display-data").text(<?php echo json_encode(POPUP_ONE_EDIT_GROUP); ?>);
            }
        });

        $('#fdelete').click(function () {
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
            if (val.length < 0 || val.length == 0) {

                $("#display-data").text(<?php echo json_encode(POPUP_DELETE_COMMISSION); ?>);

            } else if (val.length == 1 || val.length > 1)
            {
                $("#display-data").text("");
                var BusinessId = val;
                var size = $('input[name=stickup_toggler]:checked').val()
                var modalElem = $('#confirmmodel');
                if (size == "mini") {
                    $('#modalStickUpSmall').modal('show')
                } else {
                    $('#confirmmodel').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }

                $("#confirmed").click(function () {
                    $.ajax({
                        url: "<?php echo base_url('index.php/category') ?>/DeleteServiceGroup",
                        type: "POST",
                        data: {
                            val: val
                        },
                        dataType: 'json',
                        success: function (response)
                        {
                            $(".close").trigger("click");
                            location.reload(true);

                        }
                    });

                });
            }
        });


    });

</script>
<style>
    #active{
        display:none;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        
        $('#btnStickUpSizeToggler').click(function () {
            
                $('#sub_cat_mod').modal('show');
           
              
        });
         $("#insertsubcat").click(function () {
           var data = $('.checkbox:checked').map(function () {
                    return $(this).attr("data");
                }).get();
                
                var dataarr = data[0];//[0].split(",");
             var groupid = $("#grouplist").val();
            var s_name = $("#s_name").val();
            var s_desc = $("#s_desc").val();
            var fixed_price = $("#fixed_price").val();
            if(groupid == '0')
            {
                $('#addsubcatd').text("Please select service group ");
            }else if(s_name == '')
            {
                $('#addsubcatd').text("please enter service name");
            }else if(fixed_price == '')
            {
                $('#addsubcatd').text("please enter fixed price");
            }else{
          
                $.ajax({
                    url: "<?php echo base_url('index.php/category') ?>/AddService",
                    type: 'POST',
                    data: {
                        gid :groupid,
                        s_name: s_name,
                        s_desc: s_desc,
                        fp: fixed_price
                    },
                    dataType: 'JSON',
                    success: function (response)
                    {
                        $(".close").trigger("click");
                        location.reload();
                    }
                });
                }

        });

        $('#big_table_processing').show();
        var table = $('#big_table');
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url() ?>index.php/category/datatable_sgroup/<?= $catId ?>',
//            "bJQueryUI": true,
//            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
                    "aoColumns": [
                {"sWidth": "2%","sClass" : "text-center"},
                {"sWidth": "10%"},
                {"sWidth": "5%","sClass" : "text-center"},
                {"sWidth": "5%","sClass" : "text-center"}
            ],
            "oLanguage": {
                "sProcessing": "<img src='http://107.170.66.211/roadyo_live/sadmin/theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {

                $('#big_table_processing').hide();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };


//      
        

        table.dataTable(settings);
        // search box for table
        $('#search-table').keyup(function () {
            table.fnFilter($(this).val());
        });
        
        $('.changeMode').click(function () {

            $('#big_table_processing').show();
        var table = $('#big_table');
             var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url() ?>index.php/superadmin/datatable_sgroup/<?= $catId ?>',
//            "bJQueryUI": true,
//            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "oLanguage": {
                "sProcessing": "<img src='http://107.170.66.211/roadyo_live/sadmin/theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {

                $('#big_table_processing').hide();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };

            $('.tabs_active').removeClass('active');

            $(this).parent().addClass('active');



            table.dataTable(settings);

            // search box for table
            $('#search-table').keyup(function () {
                table.fnFilter($(this).val());
            });

        });
//        $('#ecityid').on('change', function () {
//        
//            $('#big_table_processing').show();
//        var table = $('#big_table');
//             var settings = {
//            "autoWidth": false,
//            "sDom": "<'table-responsive't><'row'<p i>>",
//            "destroy": true,
//            "scrollCollapse": true,
//            "iDisplayLength": 20,
//            "bProcessing": true,
//            "bServerSide": true,
//            "sAjaxSource": '<?php echo base_url() ?>index.php/superadmin/datatable_sgroup',
//            "bJQueryUI": true,
//            "sPaginationType": "full_numbers",
//            "iDisplayStart ": 20,
//            "oLanguage": {
//                "sProcessing": "<img src='http://107.170.66.211/roadyo_live/sadmin/theme/assets/img/ajax-loader_dark.gif'>"
//            },
//            "fnInitComplete": function () {
//
//                $('#big_table_processing').hide();
//            },
//            'fnServerData': function (sSource, aoData, fnCallback)
//            {
//                $.ajax
//                        ({
//                            'dataType': 'json',
//                            'type': 'POST',
//                            'url': sSource,
//                            'data': aoData,
//                            'success': fnCallback
//                        });
//            }
//        };

            $('.tabs_active').removeClass('active');

            $(this).parent().addClass('active');



            table.dataTable(settings);

            // search box for table
//            $('#ecityid').keyup(function () {
//                table.fnFilter($(this).val());
//            });
            $('#ecityiddd').change(function () {
                table.fnFilter($(this).val());
            });
    });
</script>


<style>
/*    .dataTables_wrapper .dataTables_info{
        padding: 0;
    }*/
    .exportOptions{
        display: none;
    }
</style>
<div class="content">
 <div class="jumbotron  no-margin" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;">
                <h3 class="">Page Title</h3>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg">
        <ul class="breadcrumb">
            <li>
                <a href="<?php echo base_url(); ?>index.php/category/categories_services/" class="">CATEGORY</a>
            </li>
           
                    <?php
                                foreach ($citylist as $result) {
                                    if($result->City_Id == $cityIdss)
                                    {
                                        $cityname = $result->City_Name;
                                    }
                                }
                                ;
                                ?>
                    <li><a href="<?php echo base_url(); ?>index.php/category/show_cat/<?php echo $cityIdss; ?>" class=""> <?php echo $cityname; ?></a>
                    </li>
                    <li><a href="" class="active"><?php echo $cat_name; ?></a>
                    </li>
        </ul>
        <div class="container-md-height m-b-20">
            <div class="panel panel-transparent">
                <ul class="nav nav-tabs nav-tabs-simple bg-white" role="tablist" data-init-reponsive-tabs="collapse">
                    <li class="active">
                        <a href="#" data-toggle="tab" role="tab" aria-expanded="false">Service Groups</a>
                    </li>
                    <div class="pull-right m-t-10"> <button class="btn btn-success btn-cons" id="edit"><i class="fa fa-edit text-white"></i> <?php echo BUTTON_EDIT; ?></button></div>
                        <div class="pull-right m-t-10"> <button class="btn btn-danger btn-cons" id="fdelete"><span><i class="fa fa-trash text-white"></i> <?php echo BUTTON_DELETE; ?></button></a></div>
                        <div class="pull-right m-t-10"> <button class="btn btn-primary btn-cons" id="btnStickUpSizeToggler"><span><i class="fa fa-plus text-white"></i> <?php echo BUTTON_ADD_SERVICE; ?></button></a></div>    
                        <div class="pull-right m-t-10"> <button class="btn btn-primary btn-cons" id="add_group"><i class="fa fa-plus text-white"></i> Add Group</button></div>
                </ul>
                <div class="panel-group visible-xs" id="LaCQy-accordion"></div>
                <div class="tab-content">
                    <div class="tab-pane active" id="hlp_txt">
                        <div class="row panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title"></div>
                                 <div class="error-box" id="display-data" style="text-align:center"></div>
                                <div class='pull-right cls110'>
                                    
                                    <div class="pull-right m-t-10 btn-group ">
                                        <input type="text" id="search-table" class="form-control pull-right"  placeholder="<?php echo SEARCH; ?>"/> 
                                    </div>
                                </div>
                                <br>
                                <div class="panel-body no-padding">
                                    <?php echo $this->table->generate(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

 
               

 
 
<div class="modal fade stick-up" id="confirmmodel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class=" clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
            </div>
            <br>
            <div class="modal-body">
                <div class="row">
                    <div class="error-box" id="errorboxdata" style="font-size: large;text-align:center">
                        <?php echo VEHICLEMODEL_DELETE; ?></div>
                </div>
            </div>
            <br>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4" ></div>
                    <div class="col-sm-4">
                       
                    </div>
                    <div class="col-sm-4" >
                        
                        <button type="button" class="btn btn-primary pull-right" id="confirmed" >
                            <?php echo BUTTON_YES; ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade stick-up" id="confirmmodels" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class=" clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
            </div>
            <br>
            <div class="modal-body">
                <div class="row">
                    <div class="error-box" id="errorboxdatas" style="font-size: large;text-align:center"><?php echo VEHICLEMODEL_DELETE; ?></div>
                </div>
            </div>
            <br>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4" ></div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo NO?></button>
                    </div>
                    <div class="col-sm-4" >
                        <button type="button" class="btn btn-primary pull-right" id="confirmeds" ><?php echo BUTTON_YES; ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--<div class="modal fade stick-up" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                    </div>
                    <h3> <?php echo SELECT_COUNTRY_ANDBUSINESS_COMMISSION; ?></h3>
                </div>
                <div class="modal-body">
                    <BR>
                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-4 control-label" ><?php echo FIELD_BUSINESSNAME; ?><span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">

                            <select id="businessname" name="country_select"  class="form-control error-box-class">
                                <option value="0">Select Business</option>


<?php
foreach ($business as $result) {
    echo "<option value=" . $result['masterid'] . ">" . $result['businessname'] . "</option>";
}
?>

                            </select>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="form-group" class="formex">
                        <div class="frmSearch">
                            <label for="fname" class="col-sm-4 control-label"><?php echo FIELD_COMMISSION; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" id="commission" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>

                                <div id="suggesstion-box"></div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4 error-box" id="clearerror"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="insert" ><?php echo BUTTON_ADD; ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
    </button>
</div>-->


<div class="modal fade stick-up" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                    </div>
                    <h3 style="font-size: 12px; font-weight:bold;color: #0090d9; " id='modalHeading'> <?php echo FIELD_GROUP_EDIT; ?></h3>
                </div>
               
                <div class="modal-body">
                    <input name="fdata[group_id]" type="hidden" id="egid"/>                                
<!--                    <div class="form-group" class="formex">
                        <label for="city_select" class="col-sm-4 control-label" ><?php echo FIELD_VEHICLE_SELECTCITY; ?><span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <select id="ecityid" name="city_select"  class="form-control error-box-class" onchange="EditLoadCategory()">
                                <option value="0">Select City</option>
                                <?php
                                foreach ($citylist as $result) {
                                    echo "<option value=" . $result->City_Id . ">" . $result->City_Name . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-sm-1" id="ecatloading" style="display: none;">
                            <img src="<?php echo base_url() ?>../../pics/loadingimg.gif" /> 
                        </div>
                    </div>
                    <br>
                    <br>
-->                    <div class="form-group" class="formex">
                        <label for="cat" class="col-sm-4 control-label" ><?php echo FIELD_CATEGORY_NAME; ?><span style="color:red;font-size: 18px">*</span></label>
                          <div class="col-sm-6">
                            <select id="ecatlists" name="fdata[cat]"  class="form-control error-box-class"> 
                                 <option value="0">Select Category</option>
                                <?php
                                 $this->load->library('mongo_db');
                                 $cursor = $this->mongo_db->get_where('Category', array('city_id' => (string) $cityIdss));    
                                 $catname = $this->mongo_db->get_one('Category', array('_id' => new MongoId($catId)));    
                                 
                                foreach ($cursor as $result) {
                                    $cur = (string) $result['_id'];
                                    
                                    echo "<option value=" . $cur . ">" . $result['cat_name'] . "</option>";
                                }
                                ?>
                            </select>
                        </div>                
                    </div>                                        
                    <br>
                    <br>
                    <div class="form-group" class="formex">
                        <div class="frmSearch">
                            <label for="group_name" class="col-sm-4 control-label"><?php echo FIELD_SERVICE_GROUP; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-8">
                                <input name="fdata[group_name]" type="text" id="egroup_name"  placeholder="service Group Name" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>
                                <div id="suggesstion-box"></div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>                    
<!--                    <div class="form-group" class="formex">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-2">
                            <input type="checkbox"  id="ecmand" name="fdata[mand]" class="">
                        </div>
                        <label for="fixed_price" class="col-sm-6 control-label">
                            CHECK IF MANDATORY
                        </label>                       
                    </div>
                    <br>
                    <br>                    -->
                    <div class="form-group" class="formex">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-2">
                            <input type="checkbox"  id="ecmult" name="fdata[mult]" class="">
                        </div>
                        <label for="fixed_price" class="col-sm-6 control-label">
                            CHECK IF MULTIPLE CAN BE SELECTED
                        </label> 
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4 error-box" id="clearerror"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="editbusiness" ><?php echo BUTTON_ADD; ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
    </button>
</div>


<div class="modal fade stick-up" id="add_model" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                    </div>
                    <h3 style="font-size: 12px; font-weight:bold;color: #0090d9; ">ADD SERVICE GROUP</h3>
                </div>
               
                <div class="modal-body">
<!--                    <div class="form-group" class="formex">
                        <label for="city_select" class="col-sm-4 control-label" ><?php echo FIELD_VEHICLE_SELECTCITY; ?><span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <select id="cityid" name="city_select"  class="form-control error-box-class" onchange="LoadCategory()" required>
                                <option value="0">Select City</option>
                                <?php
                                foreach ($citylist as $result) {
                                    echo "<option value=" . $result->City_Id . ">" . $result->City_Name . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-sm-1" id="catloading" style="display: none;">
                            <img src="<?php echo base_url() ?>../../pics/loadingimg.gif" /> 
                        </div>
                    </div>
                    <br>
                    <br>-->
<!--                    <div class="form-group" class="formex">
                        <label for="cat" class="col-sm-4 control-label" ><?php echo FIELD_CATEGORY_NAME; ?><span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <select id="catlist" name="fdata[cat]"  class="form-control error-box-class">                                                          
                            </select>
                        </div>
                                                <div class="col-sm-1" id="sloading" style="display: none;">
                                                    <img src="<?php echo base_url() ?>../../pics/loadingimg.gif" /> 
                                                </div>
                    </div>                                        
                    <br>
                    <br>-->
                    <div class="form-group" class="formex">
                        <div class="frmSearch">
                            <label for="group_name" class="col-sm-4 control-label"><?php echo FIELD_SERVICE_GROUP; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-8">
                                <input name="fdata[group_name]" type="text" id="group_name1"  placeholder="service Group Name" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>
                                <div id="suggesstion-box"></div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>                    
                    <div class="form-group" class="formex">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-2">
                            <input type="checkbox"  id="cmult1" name="fdata[mand]" class="">
                        </div>
                        <label for="fixed_price" class="col-sm-6 control-label">
                           CHECK IF MULTIPLE CAN BE SELECTED
                        </label>                       
                    </div>
<!--                    <br>
                    <br>                    
                    <div class="form-group" class="formex">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-2">
                            <input type="checkbox"  id="cmand1" name="fdata[mult]" class="">
                        </div>
                        <label for="fixed_price" class="col-sm-6 control-label">
                             CHECK IF MANDATORY
                        </label> 
                    </div>-->
                    <br/>
                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4 error-box" id="addsubcat22"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="insertsgroup" ><?php echo BUTTON_ADD; ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
    </button>
</div>
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGb3Eqs1luybPwJKIBb-jNRRI0EGTehtc&libraries=places&callback=initMap"></script>-->

<div class="modal fade stick-up" id="sub_cat_mod" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                    </div>
                    <h3 style="font-size: 12px; font-weight:bold;color: #0090d9; ">ADD SERVICE</h3>
                </div>
                
                <div class="modal-body">
<!--                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-4 control-label" ><?php echo FIELD_VEHICLE_SELECTCITY; ?><span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <select id="cityid" name="city_select"  class="form-control error-box-class" onchange="LoadCategory()">
                                <option value="0">Select City</option>
                                <?php
                                foreach ($citylist as $result) {
                                    echo "<option value=" . $result->City_Id . ">" . $result->City_Name . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-sm-1" id="catloading" style="display: none;">
                            <img src="<?php echo base_url() ?>../../pics/loadingimg.gif" /> 
                        </div>
                    </div>
                    <br>
                    <br>
                     <div class="form-group" class="formex">
                        <div class="frmSearch">
                            <label for="fname" class="col-sm-4 control-label">Currency</label>
                            <div class="col-sm-8">
                                <input name="curncy" type="text" id="curncy" style="  width: 219px;line-height: 2;" class="form-control error-box-class" readonly/>
                                <div id="suggesstion-box"></div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-4 control-label" ><?php echo FIELD_CATEGORY_NAME; ?><span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <select id="catlist" name="fdata[cat]"  class="form-control error-box-class" onchange="LoadServiceGroup()">                                                          
                            </select>
                        </div>
                        <div class="col-sm-1" id="grploading" style="display: none;">
                            <img src="<?php echo base_url() ?>../../pics/loadingimg.gif" /> 
                        </div>
                    </div>
                    <br>
                    <br>-->
                    <div class="form-group" class="formex">
                        <label for="grouplist" class="col-sm-4 control-label" ><?php echo FIELD_SELECT_GROUP; ?><span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <select id="grouplist" name="fdata[grouplist]"  class="form-control error-box-class">   
                                <option value="0">Select Service Group</option>
                                 <?php
                                 $this->load->library('mongo_db');
                                 $cursor = $this->mongo_db->get_where('Category', array('_id' => new MongoId($catId)));    
                                 
                                foreach ($cursor as $result) {
                                   
                                    foreach ($result['groups'] as $groups){
                                        $cur = (string) $groups['gid'];
                                    
                                        echo "<option value=" . $cur . ">" . $groups['group_name'] . "</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <br>
                    <br>

                    <div class="form-group" class="formex">
                        <div class="frmSearch">
                            <label for="fname" class="col-sm-4 control-label"><?php echo FIELD_SERVICE_NAME; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-8">
                                <input name="fdata[s_name]" type="text" id="s_name"  placeholder="service Name" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>
                                <div id="suggesstion-box"></div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-4 control-label"><?php echo FIELD_SERVICE_DESC; ?></label>
                        <div class="col-sm-6">
                            <input type="text"  id="s_desc" name="fdata['s_desc']"  class="form-control error-box-class" placeholder="Service Desc">
                        </div>
                    </div>
                    <br/>
                    <br/>
                    <div class="form-group" class="formex">
                        <label for="fixed_price" class="col-sm-4 control-label"> <?php echo FIELD_FIXED_PRICE; ?><span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <input type="number" min="0"  id="fixed_price" name="fdata[fixed_price]" class="form-control error-box-class" placeholder="Fxied Price">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4 error-box" id="addsubcatd"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="insertsubcat" ><?php echo BUTTON_ADD; ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
    </button>
</div>
