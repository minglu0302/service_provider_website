<?php
date_default_timezone_set('UTC');
$rupee = "$";
?>
<style>
      .header{
        height:60px !important;
    }
    .header h3{
        margin:10px 0 !important;
    }
    #companyid{
        display: none;
    }
    .exportOptions{
        display: none;
    }
    #selectedcity{
        display: none;
    }
</style>
<script type="text/javascript">
    function TypeSelected(ob)
    {
        var op = ob.value;
        if (op == "Hourly") {
            $('.hourlyp').css('display', 'block');
            $('.mhmix').css('display', 'block');
            $('.fixedp').css('display', 'none');

            $('.mileagep').css('display', 'none');
        } else if (op == "Fixed") {
            $('.hourlyp').css('display', 'none');
            $('.mhmix').css('display', 'none');
            $('.mileagep').css('display', 'none');
            $('.fixedp').css('display', 'block');
        } else if (op == "Mileage") {
            $('.hourlyp').css('display', 'none');
            $('.fixedp').css('display', 'none');
            $('.mhmix').css('display', 'block');
            $('.mileagep').css('display', 'block');
        } else {
            $('.mileagep').css('display', 'none');
            $('.mhmix').css('display', 'none');
            $('.hourlyp').css('display', 'none');
            $('.fixedp').css('display', 'none');
        }
    }
    var flag1 = '0';
    function validateForm()
    {
       

        var cityid = $("#city_id").val();
        var cat_name = $("#cat_name").val();
         $.ajax({
                        type: 'POST',
                        url: "<?php echo base_url('index.php/category') ?>/checkcategorynamebycityid",
                        data: {cid: cityid,
                                cat_name:cat_name},
                        dataType: 'JSON',
                        success: function (result)
                        {
                            console.log(result);
                            if(result=="true")
                            {
                                $("#addsubcat").text('Category is already added for this city');
                                return false;
                            }
                                
                        
                    }
                });
        
        var min_fees = $("#min_fees").val();
        var base_fees = $("#base_fees").val();
        var price_per_min = $("#price_per_min").val();
        var feetype = $('#feetype').val();
        var fixed_price = $("#fixed_price").val();
        var unsel_img = $("#unsel_img").val();
        var banner_img = $("#banner_img").val();
        var sel_img = $("#sel_img").val();
        var price_set_by = $("#price_set_by").val();
//        var pay_commision = $("#pay_commision").val();
        var can_fees = $('#can_fees').val();
        var visit_fees = $('#visit_fees').val();
        var price_mile = $('#price_mile').val();

        if (cityid == "0") {
            $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_CITY); ?>);
            return false;
        } else if (price_set_by == "0") {
            $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_PRICE_PLAN); ?>);
            return false;
        } else if (feetype == "0") {
            $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_FEES_TYPE); ?>);
            return false;
        } else if (cat_name == "" || cat_name == null) {
            $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_CATEGORY); ?>);
            return false;
        } else if (can_fees == "" || can_fees == null) {
            $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_CAN_FEES); ?>);
            return false;
        } else if (sel_img == "" || sel_img == null) {
            $("#addsubcat").text("Please upload select state icon");
            return false;
        } else if (unsel_img == "" || unsel_img == null) {
            $("#addsubcat").text("Please upload unselect state icon");
            return false;
        } else if (feetype == "Mileage") {
            if (min_fees == "" || min_fees == null) {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_MIN_FEES); ?>);
                return false;
            } else if (base_fees == "" || base_fees == null) {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_BASE_FEES); ?>);
                return false;
            } else if (price_per_min == "" || price_per_min == null) {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_PRICE_PER_MIN); ?>);
                return false;
            } else if (price_mile == "" || price_mile == null) {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_PRICE_PER_MILE); ?>);
                return false;
            }
        } else if (feetype == "Fixed") {
            if (fixed_price == "" || fixed_price == null) {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_FIXED_PRICE); ?>);
                return false;
            }
        } else if (feetype == "Hourly") {
            if (price_per_min == "" || price_per_min == null) {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_PRICE_PER_MIN); ?>);
                return false;
            } else if (visit_fees == "" || visit_fees == null) {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_VISIT_FEES); ?>);

                return false;
            }
        }
//       
        else
        {
            return true;
        }
    }

    $(document).ready(function () {
        $('#add').click(function () {
            var size = $('input[name=stickup_toggler]:checked').val()
            var modalElem = $('#NewCat');
            if (size == "mini") {
                $('#modalStickUpSmall').modal('show')
            } else {
                $('#NewCat').modal('show')
                if (size == "default") {
                    modalElem.children('.modal-dialog').removeClass('modal-lg');
                } else if (size == "full") {
                    modalElem.children('.modal-dialog').addClass('modal-lg');
                }
            }
        });

        var table = $('#tableWithSearch');
        
        var settings = {
            "sDom": "<'table-responsive't><'row'<p i>>",
            "sPaginationType": "bootstrap",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20
        };

        table.dataTable();

        $('#search-table').keyup(function () {
            table.fnFilter($(this).val());
        });

        var table = $('#big_table');
         $('#big_table_processing').show();
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url() ?>index.php/category/datatable_catogiries_services',
//            "bJQueryUI": true,
//            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
                     "aoColumns": [
                {"sWidth": "2%","sClass" : "text-center"},
                {"sWidth": "10%"},
                {"sWidth": "5%","sClass" : "text-center"},
                {"sWidth": "5%","sClass" : "text-center"}
            ],
            "oLanguage": {
                "sProcessing": "<img src='https://www.goclean-service.com/pics/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {
                $('#big_table_processing').hide();
            },
             "drawCallback": function () {
                $('#big_table_processing').hide();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };
        table.dataTable(settings);
        // search box for table
        $('#search-table').keyup(function () {
         $('#big_table_processing').show();
            table.fnFilter($(this).val());
        });

    });
</script>
<div class="content">
 <div class="jumbotron  no-margin" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;">
                <h3 class="">Page Title</h3>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg">
        <ul class="breadcrumb">
            <li>
                <a href="#" class="active">SERVICE CATEGORIES</a>
            </li>
        </ul>
        <div class="container-md-height m-b-20">
            <div class="panel panel-transparent">
                <ul class="nav nav-tabs nav-tabs-simple bg-white" role="tablist" data-init-reponsive-tabs="collapse">
                   <li class="active">
                        <a href="#" data-toggle="tab" role="tab" aria-expanded="false"><?php echo LIST_NEW_SERVICE_CAT; ?></a>
                    </li>
                   <div class="pull-right m-t-10"> <button class="btn btn-primary btn-cons action_buttons btn-botom-margin" id="add"  ><i class="fa fa-plus text-white"></i> <?php echo BUTTON_ADD_CATEGORY; ?></button></div>
                </ul>
                <div class="panel-group visible-xs" id="LaCQy-accordion"></div>
                <div class="tab-content">
                    <div class="tab-pane active" id="hlp_txt">
                        <div class="row panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title"></div>
                                <div id="big_table_processing" class="dataTables_processing" style=""><img src="<?php echo APP_SERVER_HOST ?>/pics/ajax-loader_dark.gif"></div>
                                <div class='pull-right cls110'>
                                    <div class="pull-right m-t-10" class="btn-group" >
                                       <input type="text" id="search-table" class="form-control pull-right" placeholder="<?php echo SEARCH; ?>">
                                    </div>
                                    
                                </div>
                                <br>
                                <div class="panel-body no-padding">
                                    <?php echo $this->table->generate(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    

        <div class="modal fade stick-up" id="confirmmodel" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class=" clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                            </button>
                        </div>
                    </div>
                    <br>
                    <div class="modal-body">
                        <div class="row">
                            <div class="error-box" id="errorboxdata" style="font-size: large;text-align:center"><?php echo COMPAIGNS_DISPLAY; ?></div>
                        </div>
                    </div>
                    <br>
                    <div class="modal-body">               
                        <div class="row">
                            <div class="col-sm-4" ></div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4" >
                                <button type="button" class="btn btn-primary pull-right" id="confirmed" ><?php echo BUTTON_YES; ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade stick-up" id="confirmmodels" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class=" clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                            </button>
                        </div>
                    </div>
                    <br>
                    <div class="modal-body">
                        <div class="row">
                            <div class="error-box" id="errorboxdatas" style="font-size: large;text-align:center"><?php echo COMPAIGNS_DISPLAY; ?></div>
                        </div>
                    </div>
                    <br>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-4" ></div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4" >
                                <button type="button" class="btn btn-primary pull-right" id="confirmeds" ><?php echo BUTTON_YES; ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




        <div class="modal fade stick-up" id="confirmmodelss" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class=" clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                            </button>
                        </div>
                    </div>
                    <br>
                    <div class="modal-body">
                        <div class="row">
                            <div class="error-box" id="errorboxdatass" style="font-size: large;text-align:center"><?php echo COMPAIGNS_DISPLAY; ?></div>
                        </div>
                    </div>
                    <br>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-4" ></div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4" >
                                <button type="button" class="btn btn-primary pull-right" id="confirmedss" ><?php echo BUTTON_YES; ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade stick-up" id="confirmmode" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class=" clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                            </button>
                        </div>               
                    </div>
                    <br>
                    <div class="modal-body">
                        <div class="row">
                            <div class="error-box" id="errorboxdat" style="font-size: large;text-align:center"><?php echo COMPAIGNS_DISPLAY; ?></div>
                        </div>
                    </div>
                    <br>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-4" ></div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4" >
                                <button type="button" class="btn btn-primary pull-right" id="confirme" ><?php echo BUTTON_YES; ?></button>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>


        <div class="modal fade stick-up in" id="deletemodal" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;">
            <div class="modal-dialog modal-sm">
                <div class="modal-content-wrapper">
                    <div class="modal-content">
                        <div class="modal-header clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                            </button>
                            <h5>Are You Sure To Delete This Category ? </h5>
                        </div>
                        <div class="modal-body">
                        </div>
                        <div class="modal-footer">
                            <a href="" id="deletelink"><button type="buttuon" class="btn btn-primary btn-cons  pull-left inline">Continue</button></a>
                            <button type="button" class="btn btn-default btn-cons no-margin pull-left inline" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>





        <div class="modal fade in" id="ProMod" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button id="close_nos" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">NO OF Services</h4>
                    </div>
                    <div class="modal-body prodatamod">                    
                    </div>
                    <div class="modal-footer">
                        <div style="float:right;">                   
                            <button type="button" class="btn btn-success btn-clean" id="EditButton" data="5" data-msg="edit">EDIT</button>
                            <button type="button" class="btn btn-success btn-clean"   id="delete" data="5" data-msg="vechiletype">DELETE</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>            


        <div class="modal fade stick-up" id="NewCat" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="modal-header">
                            <div class=" clearfix text-left">
                                <button type="button" id="close_nos" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                                </button>
                            </div>
                            <h3 style="font-size: 12px; font-weight:bold;color: #0090d9; "> ADD CATEGORY </h3>
                        </div>
                        <br>
                        <form action = "<?php echo base_url(); ?>index.php/category/AddNewCategory" method= "post" enctype="multipart/form-data" onsubmit="return validateForm();"> 

                            <div class="modal-body">
                                <div class="form-group" class="formex">
                                    <label for="fname" class="col-sm-4 control-label" ><?php echo FIELD_VEHICLE_SELECTCITY; ?><span style="color:red;font-size: 18px">*</span></label>
                                    <div class="col-sm-6">
                                        <select id="city_id"  class="form-control error-box-class" name="city_id" required="" style="text-transform: capitalize;">
                                            <option value="0" style="color: #881212;">Select City</option>
                                            <?php
                                            
                                            foreach ($citys as $res) {
                                                echo "<option value=" . $res->City_Id . ">" . strtolower($res->City_Name) . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="form-group" class="formex">
                                    <div class="frmSearch">
                                        <label for="price_set_by" class="col-sm-4 control-label"><?php echo FIELD_PRICE_SET_BY_NAME; ?><span style="color:red;font-size: 18px">*</span></label>
                                        <div class="col-sm-6">
                                            <select class="form-control" id='price_set_by' name="price_set_by">
                                                <option value='0'>Who Sets Pricing</option>
                                                <option value='Admin'>Admin</option>     
                                                <option value='Provider'>Provider</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="form-group" class="formex">
                                    <label for="fname" class="col-sm-4 control-label"> <?php echo FIELD_FEE_TYPE; ?><span style="color:red;font-size: 18px">*</span></label>
                                    <div class="col-sm-6">
                                        <select onchange="TypeSelected(this)" class="form-control" id='feetype' name="fee_type">
                                            <option value='0'>Pricing Plan</option>
                                            <option value="Mileage">Mileage</option>
                                            <option value='Hourly'>Hourly</option>     
                                           <option value='Fixed'>Fixed</option>
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <br>                                
                                <div class="form-group" class="formex">
                                    <div class="frmSearch">
                                        <label for="fname" class="col-sm-4 control-label"><?php echo FIELD_CATEGORY_NAME; ?><span style="color:red;font-size: 18px">*</span></label>
                                        <div class="col-sm-8">
                                            <input name="cat_name" type="text" id="cat_name"  placeholder="Category Name" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>
                                            <div id="suggesstion-box"></div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="form-group" class="formex">
                                    <label for="fname" class="col-sm-4 control-label"> <?php echo FIELD_CAN_FEES; ?><span style="color:red;font-size: 18px">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" min="0"  id="can_fees" name="can_fees" class="form-control error-box-class" placeholder="Cancellation fees">
                                    </div>
                                </div>
                                <br>
                                <br>

                               <div class="form-group" class="formex">
                                    <label style="color:#0090d9" for="fname" class="control-label">CANCELLATION FEES SETTING FOR NOW BOOKING</label>
                                    <br>
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-1">
                                        <input type="radio" id="can_after_some_min" value="0" name="can_condition" class="">
                                    </div>
                                    <label for="fixed_price" class="col-sm-9 control-label">
                                        APPLY AFTER  &nbsp; <input value="10" type="text"  id="cat_time" name="cat_time" style="width: 33px">
                                        &nbsp; MIN FROM TIME OF BOOKING
                                    </label>
                                </div>
                                <br>
                                <div class="form-group" class="">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-1">
                                        <input type="radio" id="can_after_pro_on" value="1" name="can_condition" class="">
                                    </div>
                                    <label for="can_after_pro_on" class="col-sm-9 control-label">
                                        APPLY AFTER PROVIDER IS ON THE WAY
                                    </label>
                                </div>
                                <br>
                                <div class="form-group" class="">
                                    <label style="color:#0090d9"  for="fname" class="control-label">CANCELLATION FEES SETTING FOR LATER BOOKING</label>
                                    <br>
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-1">
                                        <input type="radio" id="can_after_hr" value="2" name="can_condition" class="">
                                    </div>
                                    <label for="can_after_hr" class="col-sm-9 control-label">
                                        APPLY IF CANCELED 24hr BEFORE TIME OF BOOKING
                                    </label>
                                </div>
                                <br>
                                <br>
                                <div class="form-group" class="formex">
                                    <label for="cat_desc" class="col-sm-4 control-label"><?php echo FIELD_CATEGORY_DESC; ?></label>
                                    <div class="col-sm-6">
                                        <textarea type="text"  id="cat_desc" name="cat_desc"  class="form-control error-box-class" placeholder="Category Desc">
                                        </textarea>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="form-group" class="formex">
                                    <label for="banner_img" class="col-sm-4 control-label"><?php echo POPUP_BANNER_IMAGE; ?></label>
                                    <div class="col-sm-6">
                                        <input type="file" class="form-control" style="height: 37px;" id="banner_img" name="banner_img">
                                    </div>
                                </div>                                    
                                <br>
                                <br>
                                <div class="form-group" class="formex">
                                    <label for="selimg" class="col-sm-4 control-label"><?php echo POPUP_SEL_CAT_IMAGE; ?><span style="color:red;font-size: 18px">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="file" class="form-control" style="height: 37px;" id="sel_img" name="sel_img">
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="form-group" class="formex">
                                    <label for="unselimg" class="col-sm-4 control-label"><?php echo POPUP_UNSEL_CAT_IMAGE; ?><span style="color:red;font-size: 18px">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="file" class="form-control" style="height: 37px;" id="unsel_img" name="unsel_img">
                                    </div>
                                </div>


                                <div class="mileagep" style="display: none;"> 
                                    <br>
                                    <br>
                                    <div class="form-group" class="formex">
                                        <label for="fname" class="col-sm-4 control-label"> <?php echo FIELD_SUB_MIN_FEES; ?><span style="color:red;font-size: 18px">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" min="0"  id="min_fees" name="min_fees" class="form-control error-box-class" placeholder="Minimum Fees">
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <div class="form-group" class="formex">
                                        <label for="fname" class="col-sm-4 control-label"> <?php echo FIELD_SUB_BASE_FEES; ?><span style="color:red;font-size: 18px">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" min="0"  id="base_fees" name="base_fees" class="form-control error-box-class" placeholder="Base Fees">
                                        </div>
                                    </div>   
                                    <br>
                                    <br>


                                    <div class="form-group" class="formex">
                                        <label for="price_mile" class="col-sm-4 control-label"> <?php echo FIELD_PRICE_PER_MILE; ?><span style="color:red;font-size: 18px">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" min="0"  id="price_mile" name="price_mile" class="form-control error-box-class" placeholder="Price Mile">
                                        </div>
                                    </div>
                                    <br>
                                    <br>                                    

                                </div>

                                <div class="mhmix" style="display: none;">
                                    <br/>
                                    <br/>
                                    <div class="form-group" class="formex">
                                        <label for="fname" class="col-sm-4 control-label"> <?php echo "PRICE PER HOUR"; ?><span style="color:red;font-size: 18px">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" min="0"  id="price_per_min" name="price_min" class="form-control error-box-class" placeholder="Price Per Hour">
                                        </div>
                                    </div>
                                </div>

                                <div class="hourlyp" style="display: none;">
                                    <br>
                                    <br>
                                    <div class="form-group" class="formex">
                                        <label for="visit_fees" class="col-sm-4 control-label"> <?php echo FIELD_VISIT_FEES; ?><span style="color:red;font-size: 18px">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" min="0"  id="visit_fees" name="visit_fees" class="form-control error-box-class" placeholder="visit fees">
                                        </div>
                                    </div>
                                </div>

                                <br>
                                <br>                                    
                                <div class="form-group fixedp" class="formex" style="display: none;">
                                    <label for="fname" class="col-sm-4 control-label"> <?php echo FIELD_FIXED_PRICE; ?><span style="color:red;font-size: 18px">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" min="0"  id="fixed_price" name="fixed_price" class="form-control error-box-class" placeholder="Fixed Price">
                                    </div>
                                </div>                               

                                <input type="hidden" id="EditServiceId" value=""/>
                                <div class="row">
                                    <div class="col-sm-3" ></div>
                                    <div class="col-sm-6 error-box" id="addsubcat"></div>
                                    <div class="col-sm-3" >
                                        <button type="submit" class="btn btn-primary pull-right" id="insertsubcat1" ><?php echo BUTTON_SAVE; ?></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
            </button>
        </div>
