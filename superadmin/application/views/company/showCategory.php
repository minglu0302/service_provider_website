<?php
date_default_timezone_set('UTC');
$rupee = "$";
?>
<style>
    /*    .ui-state-default{
            font-size : 10px !important; 
        }*/
      .header{
        height:60px !important;
    }
    .header h3{
        margin:10px 0 !important;
    }
    .table-responsive{
        overflow-x: auto;
        cursor: pointer;
    }
    .ui-autocomplete{
        z-index: 5000;
    }
    #selectedcity,#companyid{
        display: none;
    }
    .ui-menu-item{cursor: pointer;background: black;color:white;border-bottom: 1px solid white;width: 200px;}
    .col-md-12.col-xlg-6 > p {
        font-size: 18px;
        letter-spacing: 1px;
        font-weight: 800;
        text-align: center;
        color: darkcyan;
    }
    .fs-14 {
        font-size: 20px !important;
        color: #054949!important;
    }
    .col-sm-8.p-r-10 {
        width: 100%;
    }
    .modal .modal-body {
        background: #ffffff;
    }
    p.pull-right.bold {
        color: cadetblue;
        font-size: 14px;
    }
</style>
<script>
    function moveUp(id){
//        alert(id);

              var row = $(id).closest('tr');
          var prev_id = row.prev('tr').find('.moveUp').attr('id')
          var curr_id = row.find('.moveUp').attr('id');
        
            $.ajax({
                url: "<?php echo base_url() ?>index.php/category/changeCatOrder",
                type: "POST",
                data: {kliye: 'interchange', curr_id: curr_id, prev_id: prev_id},
                success: function (result) {

                }
            });
            row.prev().insertAfter(row);
            $('#saveOrder').trigger('click');
//        });
       }
       function moveDown(id){
//        console.log(id);

           var row = $(id).closest('tr');
//           console.log(row.find('.moveDown').attr('id'));

//            console.log(row.next('tr').find('.moveDown').attr('id'));
//            console.log(row.prev('tr').find('.moveDown').attr('id'));
            var prev_id = row.find('.moveDown').attr('id');
          var curr_id = row.next('tr').find('.moveDown').attr('id');

            $.ajax({
                url: "<?php echo base_url() ?>index.php/category/changeCatOrder",
                type: "POST",
                data: {kliye: 'interchange', prev_id: prev_id, curr_id: curr_id},
                success: function (result) {

//                    alert("intercange done" + result);

                }
            });
            row.insertAfter(row.next());
            $('#saveOrder').trigger('click');
//        });
      }
    function TypeSelected(ob)
    {
        var op = ob.value;
        if (op == "Hourly") {
            $('.hourlyp').css('display', 'block');
            $('.mhmix').css('display', 'block');
            $('.fixedp').css('display', 'none');
            $('.mileagep').css('display', 'none');
        } else if (op == "Fixed") {
            $('.hourlyp').css('display', 'none');
            $('.mhmix').css('display', 'none');
            $('.mileagep').css('display', 'none');
            $('.fixedp').css('display', 'block');
        } else if (op == "Mileage") {
            $('.hourlyp').css('display', 'none');
            $('.fixedp').css('display', 'none');
            $('.mhmix').css('display', 'block');
            $('.mileagep').css('display', 'block');
        } else {
            $('.mileagep').css('display', 'none');
            $('.mhmix').css('display', 'none');
            $('.hourlyp').css('display', 'none');
            $('.fixedp').css('display', 'none');
        }
    }

    function validateForm()
    {

        var cityid = $("#city_id").val();
        var oldcat = $("#oldcat").val();
        
        var cat_name = $("#cat_name").val();
        if(cat_name != oldcat){
          $.ajax({
                        type: 'POST',
                        url: "<?php echo base_url('index.php/category') ?>/checkcategorynamebycityid",
                        data: {cid: <?php echo $cityid ?>,
                                cat_name:cat_name},
                        dataType: 'JSON',
                        success: function (result)
                        {
                            console.log(result);
                            if(result=="true")
                            {
                                $("#addsubcat").text('Category is already added for this city');
                                return false;
                            }
                                
                        
                    }
                });
                }
        var min_fees = $("#min_fees").val();
        var base_fees = $("#base_fees").val();
        var price_per_min = $("#price_per_min").val();
        var feetype = $('#fee_type').val();
        var fixed_price = $("#fixed_price").val();
        var unsel_img = $("#unsel_img").val();
        var sel_img = $("#sel_img").val();
        var price_set_by = $("#price_set_by").val();
        var pay_commision = $("#pay_commision").val();
        var can_fees = $('#can_fees').val();
        var visit_fees = $('#visit_fees').val();
        var price_mile = $('#price_mile').val();

        if (cityid == "0") {

            $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_CITY); ?>);
            return false;
        } else if (price_set_by == "0") {
            $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_PRICE_PLAN); ?>);
            return false;
        } else if ((sel_img == "" || sel_img == null) && $('#sel_img_hidden').val() == '')
        {
            $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_SEL_IMG); ?>);
            return false;
        } else if ((unsel_img == "" || unsel_img == null) && $('#unsel_img_hidden').val() == '')
        {
            $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_UNSEL_IMG); ?>);
            return false;
        } else if (feetype == "0") {
            $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_FEES_TYPE); ?>);
            return false;
        } else if (cat_name == "" || cat_name == null) {
            $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_CATEGORY); ?>);
            return false;
        } else if (can_fees == "" || can_fees == null) {
            $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_CAN_FEES); ?>);
            return false;
        } else if (feetype == "Mileage") {
            if (min_fees == "" || min_fees == null) {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_MIN_FEES); ?>);
                return false;
            } else if (base_fees == "" || base_fees == null) {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_BASE_FEES); ?>);
                return false;
            } else if (price_per_min == "" || price_per_min == null) {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_PRICE_PER_MIN); ?>);
                return false;
            } else if (price_mile == "" || price_mile == null) {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_PRICE_PER_MILE); ?>);
                return false;
            }
        } else if (feetype == "Fixed") {
            if (fixed_price == "" || fixed_price == null) {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_FIXED_PRICE); ?>);
                return false;
            }
        } else if (feetype == "Hourly") {
            if (price_per_min == "" || price_per_min == null) {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_PRICE_PER_MIN); ?>);
                return false;
            } else if (visit_fees == "" || visit_fees == null) {
                $("#addsubcat").text(<?php echo json_encode(POPUP_SELECT_VISIT_FEES); ?>);
                return false;
            }
        } else
        {
            return true;
        }
    }

    function validateFormAddCat()
    {


        var cat_name = $("#cat_name1").val();
         console.log("DS");
        var cat_name = $("#cat_name1").val();
          $.ajax({
                        type: 'POST',
                        url: "<?php echo base_url('index.php/category') ?>/checkcategorynamebycityid",
                        data: {cid: <?php echo $cityid ?>,
                                cat_name:cat_name},
                        dataType: 'JSON',
                        success: function (result)
                        {
                            console.log(result);
                            if(result=="true")
                            {
                                $("#addsubcat3").text('Category is already added for this city');
                                return false;
                            }
                                
                        
                    }
                });
        var min_fees = $("#min_fees1").val();
        var base_fees = $("#base_fees1").val();
        var price_per_min = $("#price_per_min1").val();
        var feetype = $('#feetype1').val();
        var fixed_price = $("#fixed_price1").val();
        var unsel_img = $("#unsel_img1").val();
        var banner_img = $("#banner_img1").val();
        var sel_img = $("#sel_img1").val();
        var price_set_by = $("#price_set_by1").val();
//        var pay_commision = $("#pay_commision").val();
        var can_fees = $('#can_fees1').val();
        var visit_fees = $('#visit_fees1').val();
        var price_mile = $('#price_mile1').val();

        if (price_set_by == "0") {
            $("#addsubcat3").text(<?php echo json_encode(POPUP_SELECT_PRICE_PLAN); ?>);
            return false;
        } else if (feetype == "0") {
            $("#addsubcat3").text(<?php echo json_encode(POPUP_SELECT_FEES_TYPE); ?>);
            return false;
        } else if (cat_name == "" || cat_name == null) {
            $("#addsubcat3").text(<?php echo json_encode(POPUP_SELECT_CATEGORY); ?>);
            return false;
        } else if (can_fees == "" || can_fees == null) {
            $("#addsubcat3").text(<?php echo json_encode(POPUP_SELECT_CAN_FEES); ?>);
            return false;
        } else if (sel_img == "" || sel_img == null) {
            $("#addsubcat3").text("Please upload select state icon");
            return false;
        } else if (unsel_img == "" || unsel_img == null) {
            $("#addsubcat3").text("Please upload unselect state icon");
            return false;
        } else if (feetype == "Mileage") {
            if (min_fees == "" || min_fees == null) {
                $("#addsubcat3").text(<?php echo json_encode(POPUP_SELECT_MIN_FEES); ?>);
                return false;
            } else if (base_fees == "" || base_fees == null) {
                $("#addsubcat3").text(<?php echo json_encode(POPUP_SELECT_BASE_FEES); ?>);
                return false;
            } else if (price_per_min == "" || price_per_min == null) {
                $("#addsubcat3").text(<?php echo json_encode(POPUP_SELECT_PRICE_PER_MIN); ?>);
                return false;
            } else if (price_mile == "" || price_mile == null) {
                $("#addsubcat3").text(<?php echo json_encode(POPUP_SELECT_PRICE_PER_MILE); ?>);
                return false;
            }
        } else if (feetype == "Fixed") {
            if (fixed_price == "" || fixed_price == null) {
                $("#addsubcat3").text(<?php echo json_encode(POPUP_SELECT_FIXED_PRICE); ?>);
                return false;
            }
        } else if (feetype == "Hourly") {
            if (price_per_min == "" || price_per_min == null) {
                $("#addsubcat3").text(<?php echo json_encode(POPUP_SELECT_PRICE_PER_MIN); ?>);
                return false;
            } else if (visit_fees == "" || visit_fees == null) {
                $("#addsubcat3").text(<?php echo json_encode(POPUP_SELECT_VISIT_FEES); ?>);

                return false;
            }
        }
//       
        else
        {
            $('#insertsubcat12').addClass("disabled");

            return true;
        }
    }

    $(document).ready(function () {

        $('.provisionen').addClass('active');
        $('.provisionen_thumb').attr('src', "<?php echo base_url(); ?>/theme/icon/comission_on copy.png");
        $('.provisionen_thumb').addClass("bg-success");
        $('#search_by_select').change(function () {
            $('#atag').attr('href', '<?php echo base_url() ?>index.php/superadmin/search_by_select/' + $('#search_by_select').val());
            $("#callone").trigger("click");
        });
        var table = $('#tableWithSearch');
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "sPaginationType": "bootstrap",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 20,
             
            "order": [[0, "desc"]]
        };
        table.dataTable(settings);
        $('#search-table').keyup(function () {
            table.fnFilter($(this).val());
        });
        $('#btnStickUpSizeToggler').click(function () {
            $("#display-data").text("");
            var size = $('input[name=stickup_toggler]:checked').val();
            var modalElem = $('#sub_cat_mod');
            if (size == "mini") {
                $('#modalStickUpSmall').modal('show')
            } else {
                $('#sub_cat_mod').modal('show')
                if (size == "default") {
                    modalElem.children('.modal-dialog').removeClass('modal-lg');
                } else if (size == "full") {
                    modalElem.children('.modal-dialog').addClass('modal-lg');
                }
            }
        });

        $('#editcitypage').click(function () {
            $("#display-data").text("");
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
            if (val.length == 0) {
                $("#display-data").text(<?php echo json_encode(POPUP_CAT_ANYONE); ?>);
            } else if (val.length > 1) {
                $("#display-data").text(<?php echo json_encode(POPUP_CAT_ONLYONE); ?>);
            } else {
                var size = $('input[name=stickup_toggler]:checked').val();
                var modalElem = $('#sub_cat_mod');
                $('#latitudeedit').val($('.checkbox:checked').parent().prev().prev().text());
                $('#longitudeedit').val($('.checkbox:checked').parent().prev().text());

                if (size == "mini") {
                    $('#modalStickUpSmall').modal('show')

                } else
                {
                    val = val.toString();
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo base_url('index.php/category') ?>/GetOneServices",
                        data: {sid: val},
                        dataType: 'JSON',
                        success: function (result)
                        {
                            console.log(result.sel_img);
                            $("#close_nos").trigger("click");
                            $('#city_id').val(result.city_id);
                            $('#cat_name').val(result.cat_name);
                            $('#oldcat').val(result.cat_name);
                            $('#cat_desc').val(result.cat_desc);
                            $('#fee_type').val(result.fee_type);
                            $('#price_set_by').val(result.price_set_by);
                            $('#pay_commision').val(result.pay_commision);
                            $('#can_fees').val(result.can_fees);
                            $('#sel_img_hidden').val(result.sel_img);
                            $('#unsel_img_hidden').val(result.unsel_img);
                            if (result.can_condition == '0') {
                                $('input:radio[name=can_condition]:nth(0)').attr('checked', true);
                            } else if (result.can_condition == '1') {
                                $('input:radio[name=can_condition]:nth(1)').attr('checked', true);
                            } else if (result.can_condition == '2') {
                                $('input:radio[name=can_condition]:nth(2)').attr('checked', true);
                            } else {
                                $('input:radio[name=can_condition]').attr('checked', false);
                            }

                            $('#CategoryId').val(val);

                            $("#selimg").attr("href", "<?php echo base_url() ?>../../pics/" + result.sel_img);


                            $("#unselimg").attr("href", "<?php echo base_url() ?>../../pics/" + result.unsel_img);

                            $("#bannerimg").attr("href", "<?php echo base_url() ?>../../pics/" + result.banner_img);
                            if (result.banner_img != '' || result.banner_img != "undefined")
                            {
                                $("#bannerimg").text("view");
                            }
                            if (result.fee_type == "Hourly")
                            {
                                $('.hourlyp').css('display', 'block');
                                $('.mhmix').css('display', 'block');
                                $('.fixedp').css('display', 'none');
                                $('.mileagep').css('display', 'none');

                                $('#visit_fees').val(result.visit_fees);
                                $('#price_per_min').val(result.price_min);
                            }
                            if (result.fee_type == "Fixed")
                            {
                                $('.hourlyp').css('display', 'none');
                                $('.mhmix').css('display', 'none');
                                $('.mileagep').css('display', 'none');
                                $('.fixedp').css('display', 'block');
                                $('#fixed_price').val(result.fixed_price);
                            }
                            if (result.fee_type == "Mileage")
                            {
                                $('.hourlyp').css('display', 'none');
                                $('.fixedp').css('display', 'none');
                                $('.mhmix').css('display', 'block');
                                $('.mileagep').css('display', 'block');

                                $('#min_fees').val(result.min_fees);
                                $('#base_fees').val(result.base_fees);
                                $('#price_per_min').val(result.price_min);
                                $('#price_mile').val(result.price_mile);

                            }
                            $('#sub_cat_mod').modal('show')
                        }
                    });
                }
            }
        });

        $('#addCategory').click(function () {

            $('#city_id1').val(<?php echo $cityid ?>);
            $('#addbygroup').val(<?php echo $cityid ?>);
            $('#NewCat').modal('show')

        });
        $('#addGroup').click(function () {
            $("#display-data").text("");

            $('#addgropModel').modal('show')


        });
        $('#insertsgroup').click(function () {
            $('.clearerror').text("");
            var data = $('.checkbox:checked').map(function () {
                return $(this).attr("data");
            }).get();

            var dataarr = data[0];//[0].split(",");

            var group_name = $('#group_name').val();
            var category = $('#catlist').val();

            var cmand = 0;
            var cmult = 0;
//            if (document.getElementById('cmand').checked)
//                cmand = 1;
            if (document.getElementById('cmult').checked)
                cmult = 1;
            if (category == "" || category == "0") {
                $("#addsubcat2").text('Please select the category');
            } else if (group_name == "" || group_name == null) {
                $("#addsubcat2").text('Please enter the group name');
            } else {
                $.ajax({
                    url: "<?php echo base_url('index.php/category') ?>/AddServiceGroup",
                    type: 'POST',
                    data: {
                        cid: category,
                        group_name: group_name,
                        cmult: cmult,
                        cmand: cmand
                    },
                    dataType: 'JSON',
                    success: function (response)
                    {
                        $('.close').trigger('click');
                        location.reload(true);

                    }
                });
            }
        });




        $("#chekdel").click(function () {
            $("#display-data").text("");
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
            if (val.length > 0)
            {
                val = val.toString();
                var size = $('input[name=stickup_toggler]:checked').val()
                var modalElem = $('#confirmmodel');
                if (size == "mini") {
                    $('#modalStickUpSmall').modal('show')
                } else {
                    $('#confirmmodel').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }
                $("#errorboxdata").text(<?php echo json_encode(CAT_DELETE); ?>);
                $("#confirmed").click(function () {
                    $.ajax({
                        url: "<?php echo base_url('index.php/category') ?>/DeleteServices",
                        type: "POST",
                        data: {val: val},
                        dataType: 'json',
                        success: function (result)
                        {
                            $(".close").trigger("click");
                            var size = $('input[name=stickup_toggler]:checked').val()
                            var modalElem = $('#confirmmodels');
                            if (size == "mini")
                            {
                                $('#modalStickUpSmall').modal('show')
                            } else
                            {
                                $('#confirmmodels').modal('show')
                                if (size == "default") {
                                    modalElem.children('.modal-dialog').removeClass('modal-lg');
                                } else if (size == "full") {
                                    modalElem.children('.modal-dialog').addClass('modal-lg');
                                }
                            }
                            if (result.flag == '1') {
                                $("#errorboxdatas").text(result.msg);

                            } else if (result.flag == '0') {
                                $("#errorboxdatas").text(result.msg);
                                $('.checkbox:checked').each(function (i) {
                                    $(this).closest('tr').remove();
                                });
                            }
                            $("#confirmeds").hide();
                        }
                    });
                });

            } else
            {
                $("#display-data").text(<?php echo json_encode(POPUP_CAT_ATLEAST); ?>);
            }
        });



    });

</script>
<style>
    #active{
        display:none;
    }
</style>
<script type="text/javascript">
    function documents(rowid) {
        $('#doc_body').html('');
        var id = $(rowid).attr('data-id');
        var imgarr = $(rowid).attr('data-val');
        var image = JSON.parse(imgarr);
        console.log(image);
        $('#myModaldocument').modal('show');


        console.log(image.bimg);
        var html = "<tr>";
        if (image.bimg) {

            html += "<td>BANNER IMAGE</td>";
            html += "<td><img src=" + '<?php echo base_url() ?>' + "../../pics/" + image.bimg + " style='border-radius: 50%;' height='100' width='100'></td>";
            html += "<td><a target=__blank href=" + '<?php echo base_url() ?>' + "../../pics/" + image.bimg + " download= " + image.bimg + "><button>download</button></a>" + "</td>";
        }
        if (image.simg) {
            html += "</tr><tr>";

            html += "<td>SELECTED STATE ICON</td>";
            html += "<td><img src=" + '<?php echo base_url() ?>' + "../../pics/" + image.simg + " style='border-radius: 50%;' height='100' width='100'></td>";
            html += "<td><a target=__blank href=" + '<?php echo base_url() ?>' + "../../pics/" + image.simg + " download= " + image.simg + "><button>download</button></a>" + "</td>";
        }

        if (image.usimg) {
            html += "</tr>";
            html += "</tr><tr>";

            html += "<td>UNSELECTED STATE ICON</td>";
            html += "<td><img src=" + '<?php echo base_url() ?>' + "../../pics/" + image.usimg + " style='border-radius: 50%;' height='100' width='100'></td>";
            html += "<td><a target=__blank href=" + '<?php echo base_url() ?>' + "../../pics/" + image.usimg + " download= " + image.usimg + "><button>download</button></a>" + "</td>";
        }
        html += "</tr>";
        $('#doc_body').append(html);


        $("#documentok").click(function () {
            $('.close').trigger('click');
        });
    }
    function feeType(rowid) {
//        $('#doc_body').html('');
        var id = $(rowid).attr('data-id');
        var imgarr = $(rowid).attr('data-val');
        var image = JSON.parse(imgarr);
        console.log(image);
        console.log(image.can_fees);
        $('#pricingPlan').modal('show');
        if (image.price_mile != "N/A")
        {
            $('.pr_pricemile').css('display', 'block');
        }
        if (image.base_fees != "N/A")
        {
            $('.pr_basefees').css('display', 'block');
        }

        if (image.fixed_price != "N/A")
        {
            $('.pr_fixedprice').css('display', 'block');
        }
        if (image.min_fees != "N/A")
        {
            $('.pr_minfees').css('display', 'block');
        }

        if (image.price_min != "N/A")
        {
            $('.pr_pricemin').css('display', 'block');
        }
        if (image.visit_fees != "N/A")
        {
            $('.pr_visitfees').css('display', 'block');
        }

        $('#canfees').text("<?php echo CURRENCY ?>"+image.can_fees);
        $('#priceset_by').text(image.price_set_by);

        $('#basefees').text("<?php echo CURRENCY ?>"+image.base_fees);

        $('#fixedprice').text("<?php echo CURRENCY ?>"+image.fixed_price);
        $('#minfees').text("<?php echo CURRENCY ?>"+image.min_fees);
        $('#pricemile').text("<?php echo CURRENCY ?>"+image.price_mile);
        $('#pricemin').text("<?php echo CURRENCY ?>"+image.price_min);

        $('#visitfees').text("<?php echo CURRENCY ?>"+image.visit_fees);
    }
    $(document).ready(function () {
//        var status = 1; //'<?php //echo $status;      ?>';
//        if (status == 1) {
//            $('#inactive').show();
//            $('#active').hide();
//            $('#btnStickUpSizeToggler').show();
//        }
        $('#big_table_processing').show();
        var table = $('#big_table');
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "aoColumns": [
                {"sWidth": "2%","sClass" : "text-center"},
                {"sWidth": "7%"},
                {"sWidth": "15%"},
                {"sWidth": "5%","sClass" : "text-center"},
                {"sWidth": "5%","sClass" : "text-center"},
                {"sWidth": "3%","sClass" : "text-center"},
                {"sWidth": "3%","sClass" : "text-center"},
                {"sWidth": "5%","sClass" : "text-center"}
            ],
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url() ?>index.php/category/datatable_show_cat/<?php echo $cityid ?>',
//                        "bJQueryUI": true,
//                        "sPaginationType": "full_numbers",
                        "iDisplayStart ": 20,
                        "oLanguage": {
                            "sProcessing": "<img src='http://107.170.66.211/roadyo_live/sadmin/theme/assets/img/ajax-loader_dark.gif'>"
                        },
                        "fnInitComplete": function () {

                            $('#big_table_processing').hide();
                        },
                                 "drawCallback": function () {
                $('#big_table_processing').hide();
            },
                        'fnServerData': function (sSource, aoData, fnCallback)
                        {

                            $.ajax
                                    ({
                                        'dataType': 'json',
                                        'type': 'POST',
                                        'url': sSource,
                                        'data': aoData,
                                        'success': fnCallback
                                    });
                        }
                    };
                    table.dataTable(settings);
                    // search box for table
                    $('#search-table').keyup(function () {
                    $('#big_table_processing').show();
                        table.fnFilter($(this).val());
                    });
                    table.dataTable(settings);

                    // search box for table
//            $('#ecityid').keyup(function () {
//                table.fnFilter($(this).val());
//            });
                    $('#ecityid').change(function () {
                        $('#big_table_processing').show();
                        table.fnFilter($(this).val());
                    });
                    $('#catlist').change(function () {
                        $('#big_table_processing').show();
                        table.fnFilter($(this).val());
                    });
                });
</script>
<script>

</script>

<style>
    /*    .dataTables_wrapper .dataTables_info{
            padding: 0;
        }*/
    /*    body {
     
      font-size: 12px !important; 
      
    }
        .ui-state-default{
            font-size : 10px !important; 
        }*/
    .exportOptions{
        display: none;
    }
</style>
<div class="content">
 <div class="jumbotron  no-margin" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;">
                <h3 class="">Page Title</h3>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg">
        <ul class="breadcrumb">
            <li>
                <a href="<?php echo base_url(); ?>index.php/category/categories_services/" class="">CATEGORY</a>
            </li>
             <?php
                foreach ($citylist as $result) {
                    if ($result->City_Id == $cityid) {
                        $cityname = $result->City_Name;
                    }
                }
                ;
                ?>
                <li><a href="#" class="active"> <?php echo $cityname; ?></a>
                </li>
        </ul>
        <div class="container-md-height m-b-20">
            <div class="panel panel-transparent">
                <ul class="nav nav-tabs nav-tabs-simple bg-white" role="tablist" data-init-reponsive-tabs="collapse">
                    <li class="active">
                        <a href="#" data-toggle="tab" role="tab" aria-expanded="false">LIST OF CATEGORY</a>
                    </li>
                    <div class="pull-right m-t-10"> 
                            <button class="btn btn-success btn-cons" id="editcitypage"><i class="fa fa-edit text-white"></i> <?php echo BUTTON_EDIT; ?></button>
                        </div>
                        <div class="pull-right m-t-10"> 
                            <button class="btn btn-danger btn-cons" id="chekdel"><i class="fa fa-trash  text-white"></i> <span><?php echo BUTTON_DELETE; ?></button>
                        </div>  
                        <div class="pull-right m-t-10"> 
                            <button class="btn btn-primary btn-cons" id="addGroup"><i class="fa fa-plus  text-white"></i> <span>Add Group</button>
                        </div> 
                        <div class="pull-right m-t-10"> 
                            <button class="btn btn-primary btn-cons" id="addCategory"><i class="fa fa-plus text-white"></i> <span>Add Category</button>
                        </div> 
                </ul>
                <div class="panel-group visible-xs" id="LaCQy-accordion"></div>
                <div class="tab-content">
                    <div class="tab-pane active" id="hlp_txt">
                        <div class="row panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title"></div>
                                <div class="error-box" id="display-data" style="text-align:center"></div>
                                <div id="big_table_processing" class="dataTables_processing" style=""><img src="<?php echo APP_SERVER_HOST ?>pics/ajax-loader_dark.gif"></div>
                                <div class='pull-right cls110'>
                                    <div class="pull-right">
                                            <input type="text" id="search-table" class="form-control pull-right" placeholder="<?php echo SEARCH; ?>"> </div>
                                    </div>
                                    <div class="pull-right" style="padding-right: 38px;">
                                        <select id="ecityid" class="form-control" id='fee_type' name="fee_type">
                                            <option value='0'>Pricing Plan</option>
											<option value=" Mileage">Mileage</option>
                                            <option value=' Hourly'>Hourly</option>     
											<option value=' Fixed'>Fixed</option>
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <div class="panel-body no-padding">
                                    <?php echo $this->table->generate(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    



    <input type="hidden" id="oldcat">

<div class="modal fade stick-up" id="sub_cat_mod" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <div class=" clearfix text-left">
                        <button type="button" id="close_nos" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                    </div>
                    <h3 style="font-size: 12px; font-weight:bold;color: #0090d9; "> EDIT CATEGORY</h3>
                </div>
                
                <form action = "<?php echo base_url(); ?>index.php/category/AddNewCategory" method= "post" enctype="multipart/form-data" onsubmit="return validateForm();"> 

                    <div class="modal-body">
                        <div class="form-group" class="formex">
                            <label for="fname" class="col-sm-4 control-label" ><?php echo FIELD_VEHICLE_SELECTCITY; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-6">
                                <select id="city_id"  class="form-control error-box-class city_id" name="city_id" required="" style="text-transform: capitalize;">
                                    <option value='0'>Select City</option>
                                    <?php
                                    foreach ($city as $res) {
                                        echo "<option value=" . $res->City_Id . ">" .strtolower($res->City_Name) . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <br>
                        <br>

                        <div class="form-group" class="formex">
                            <div class="frmSearch">
                                <label for="price_set_by" class="col-sm-4 control-label"><?php echo FIELD_PRICE_SET_BY_NAME; ?><span style="color:red;font-size: 18px">*</span></label>
                                <div class="col-sm-6">
                                    <select class="form-control" id='price_set_by' name="price_set_by">
                                        <option value='0'>Who Sets Pricing</option>
                                        <option value='Admin'>Admin</option>     
                                       <option value='Provider'>Provider</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="form-group" class="formex">
                            <label for="fname" class="col-sm-4 control-label"> <?php echo FIELD_FEE_TYPE; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-6">
                                <select onchange="TypeSelected(this)" class="form-control" id='fee_type' name="fee_type">
                                    <option value='0'>Pricing Plan</option>
                                    <option value="Mileage">Mileage</option>
                                    <option value='Hourly'>Hourly</option>     
                                    <option value='Fixed'>Fixed</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <br>

                        <div class="form-group" class="formex">
                            <div class="frmSearch">
                                <label for="fname" class="col-sm-4 control-label"><?php echo FIELD_CATEGORY_NAME; ?><span style="color:red;font-size: 18px">*</span></label>
                                <div class="col-sm-8">
                                    <input name="cat_name" type="text" id="cat_name"  placeholder="Category Name" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>
                                    <div id="suggesstion-box"></div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="form-group" class="formex">
                            <label for="fname" class="col-sm-4 control-label"> <?php echo FIELD_CAN_FEES; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" min="0"  id="can_fees" name="can_fees" class="form-control error-box-class" placeholder="Cancellation fees">
                            </div>
                        </div>
                        <br>
                        <br>
                       <div class="form-group" class="formex">
                            <label style="color:#0090d9" for="fname" class="control-label">CANCELLATION FEES SETTING FOR NOW BOOKING</label>
                            <br>
                            <div class="col-sm-2"></div>
                            <div class="col-sm-1">
                                <input type="radio" id="can_after_some_min" value="0" name="can_condition" class="">
                            </div>
                            <label for="fixed_price" class="col-sm-9 control-label">
                                APPLY AFTER  &nbsp; <input value="10" type="text"  id="cat_time" name="cat_time" style="width: 33px">
                                &nbsp; MIN FROM TIME OF BOOKING
                            </label>
                        </div>
                        <br>
                        <div class="form-group" class="">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-1">
                                <input type="radio" id="can_after_pro_on" value="1" name="can_condition" class="">
                            </div>
                            <label for="can_after_pro_on" class="col-sm-9 control-label">
                                APPLY AFTER PROVIDER IS ON THE WAY
                            </label>
                        </div>
                        <br>
                        <div class="form-group" class="">
                            <label style="color:#0090d9"  for="fname" class="control-label">CANCELLATION FEES SETTING FOR LATER BOOKING</label>
                            <br>
                            <div class="col-sm-2"></div>
                            <div class="col-sm-1">
                                <input type="radio" id="can_after_hr" value="2" name="can_condition" class="">
                            </div>
                            <label for="can_after_hr" class="col-sm-9 control-label">
                                APPLY IF CANCELED 24hr BEFORE TIME OF BOOKING
                            </label>
                        </div>
                        <br>
                        <br>
                        <div class="form-group" class="formex">
                            <label for="cat_desc" class="col-sm-4 control-label"><?php echo FIELD_CATEGORY_DESC; ?></label>
                            <div class="col-sm-6">
                                <textarea type="text"  id="cat_desc" name="cat_desc"  class="form-control error-box-class" placeholder="Category Desc">
                                </textarea>
                            </div>
                        </div>
                        <br>
                        <br>

                        <div class="form-group" class="formex">
                            <label for="banner_img" class="col-sm-4 control-label"><?php echo POPUP_BANNER_IMAGE; ?></label>
                            <div class="col-sm-6">
                                <input type="file" class="form-control" style="height: 37px;" id="banner_img" name="banner_img">


                                <a target="_blank" id="bannerimg" href=""></a> 



                            </div>
                        </div>   
                        <br>
                        <br>
                        <div class="form-group" class="formex">
                            <label for="selimg" class="col-sm-4 control-label"><?php echo POPUP_SEL_CAT_IMAGE; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-6">
                                <input type="file" class="form-control" style="height: 37px;" id="sel_img" name="sel_img">
                                <a target="_blank" id="selimg" href="">view</a> 
                            </div>
                        </div>
                        <br>
                        <br>
                        <input type="hidden" value="" id='sel_img_hidden'/>
                        <input type="hidden" value="" id='unsel_img_hidden'/>

                        <div class="form-group" class="formex">
                            <label for="unselimg" class="col-sm-4 control-label"><?php echo POPUP_UNSEL_CAT_IMAGE; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-6">
                                <input type="file" class="form-control" style="height: 37px;" id="unsel_img" name="unsel_img">
                                <a target="_blank" id="unselimg" href="">view</a> 
                            </div>
                        </div>


                        <div class="mileagep" style="display: none;"> 
                            <br>
                            <br>
                            <div class="form-group" class="formex">
                                <label for="fname" class="col-sm-4 control-label"> <?php echo FIELD_SUB_MIN_FEES; ?><span style="color:red;font-size: 18px">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" min="0"  id="min_fees" name="min_fees" class="form-control error-box-class" placeholder="Minimum Fees">
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group" class="formex">
                                <label for="fname" class="col-sm-4 control-label"> <?php echo FIELD_SUB_BASE_FEES; ?><span style="color:red;font-size: 18px">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" min="0"  id="base_fees" name="base_fees" class="form-control error-box-class" placeholder="Base Fees">
                                </div>
                            </div>   
                            <br>
                            <br>


                            <div class="form-group" class="formex">
                                <label for="price_mile" class="col-sm-4 control-label"> <?php echo FIELD_PRICE_PER_MILE; ?><span style="color:red;font-size: 18px">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" min="0"  id="price_mile" name="price_mile" class="form-control error-box-class" placeholder="Price Mile">
                                </div>
                            </div>
                            <br>
                            <br>                                    

                        </div>

                        <div class="mhmix" style="display: none;">
                            <br/>
                            <br/>
                            <div class="form-group" class="formex">
                                <label for="fname" class="col-sm-4 control-label"> <?php echo "PRICE PER HOUR"; ?><span style="color:red;font-size: 18px">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" min="0"  id="price_per_min" name="price_min" class="form-control error-box-class" placeholder="Price Per Hour">
                                </div>
                            </div>
                        </div>

                        <div class="hourlyp" style="display: none;">
                            <br>
                            <br>
                            <div class="form-group" class="formex">
                                <label for="visit_fees" class="col-sm-4 control-label"> <?php echo FIELD_VISIT_FEES; ?><span style="color:red;font-size: 18px">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" min="0"  id="visit_fees" name="visit_fees" class="form-control error-box-class" placeholder="visit fees">
                                </div>
                            </div>
                        </div>

                        <br>
                        <br>                                    
                        <div class="form-group fixedp" class="formex" style="display: none;">
                            <label for="fname" class="col-sm-4 control-label"> <?php echo FIELD_FIXED_PRICE; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" min="0"  id="fixed_price" name="fixed_price" class="form-control error-box-class" placeholder="Fixed Price">
                            </div>
                        </div>                               

                           <input type="hidden" id="EditServiceId" value=""/>
                        <input type="hidden" id="CategoryId" value="" name="CategoryId"/>
                        <div class="row">
                            <div class="col-sm-4" ></div>
                            <div class="col-sm-4 error-box" id="addsubcat"></div>
                            <div class="col-sm-4" >
                                <button type="submit" class="btn btn-primary pull-right" id="insertsubcat1" ><?php echo BUTTON_SAVE; ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
    </button>
</div>


<!--delete sub category conformatio model-->
<div class="modal fade stick-up" id="confirmmodel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class=" clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>

                </div>
            </div>
            <br>
            <div class="modal-body">
                <div class="row">
                    <div class="error-box" id="errorboxdata" style="font-size: large;text-align:center"><?php echo COMPAIGNS_DISPLAY; ?></div>
                </div>
            </div>
            <br>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4" ></div>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4" >
                        <button type="button" class="btn btn-primary pull-right" id="confirmed" ><?php echo BUTTON_YES; ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade stick-up" id="myModaldocument" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">

                <div class="modal-header">

                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>

                    </div>
                    <h3 style="font-size: 12px; font-weight:bold;color: #0090d9; "> Image Document</h3>
                </div>


                <br>
                <br>

                <div class="modal-body">

                    <div id="tableWithSearch_wrapper" class="dataTables_wrapper form-inline no-footer"><div class="table-responsivev"><table class="table table-hover demo-table-search dataTable no-footer" id="big_table" role="grid" aria-describedby="tableWithSearch_info">


                               <thead>

                                    <tr role="row">
                                        <th  rowspan="1" colspan="1" aria-sort="ascending"  style="width: 100PX;font-size: 14px">Images</th>
                                        <th  rowspan="1" colspan="1" aria-sort="ascending"  style="width: 100PX;font-size: 14px">Download</th>

                                    </tr>


                                </thead>
                                <tbody id="doc_body">

                                </tbody>
                            </table>

                            <div class="row">
                                <div class="col-sm-4" ></div>
                                <div class="col-sm-4 error-box" id="errorpass"></div>
                                <div class="col-sm-4" >
                                    <button type="button" class="btn btn-primary pull-right" id="documentok" ><?php echo BUTTON_OK; ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>


        </div>
    </div> </div>



<div class="modal fade stick-up in" id="pricingPlan" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;">
    <div class="modal-dialog" style="width:425px;">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
            </button>
            <div class="container-xs-height full-height">
                <div class="row-xs-height">
                    <div class="modal-body col-xs-height coupondisData  "><div class="col-md-12 col-xlg-6">
                            <p>PRICING MODEL </p>
                            <div class="widget-17-weather">
                                <div class="row">
                                    <div class="col-sm-8 p-r-10">

                                        <div class="row">
                                            <div class="col-md-12">
                                                <p class="pull-left"> <?php echo FIELD_PRICE_SET_BY_NAME; ?></p>
                                                <p class="pull-right bold" id="priceset_by"></p>
                                            </div>
                                        </div>
                                        <div class="pr_basefees" style="display: none;">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="pull-left"><?php echo FIELD_SUB_BASE_FEES; ?></p>
                                                    <p class="pull-right bold" id="basefees"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p class="pull-left"><?php echo FIELD_CAN_FEES; ?></p>
                                                <p class="pull-right bold" id="canfees"></p>
                                            </div>
                                        </div>
                                        <div class="pr_fixedprice" style="display: none;">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="pull-left"><?php echo FIELD_FIXED_PRICE; ?></p>
                                                    <p class="pull-right bold" id="fixedprice"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pr_minfees" style="display: none;">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="pull-left"><?php echo FIELD_SUB_MIN_FEES; ?></p>
                                                    <p class="pull-right bold" id="minfees"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pr_pricemile" style="display: none;">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="pull-left"><?php echo FIELD_PRICE_PER_MILE; ?></p>
                                                    <p class="pull-right bold" id="pricemile"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pr_pricemin" style="display: none;">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="pull-left"><?php echo "PRICE PER HOUR"; ?></p>
                                                    <p class="pull-right bold" id="pricemin"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pr_visitfees" style="display: none;"> 
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="pull-left"><?php echo FIELD_VISIT_FEES; ?></p>

                                                    <p class="pull-right bold" id="visitfees"></p>                     
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                     

                                </div>
                            </div>
                        </div></div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
    </button>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade stick-up" id="addgropModel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                    </div>
                    <h3 style="font-size: 12px; font-weight:bold;color: #0090d9; ">  ADD SERVICE GROUP</h3>
                </div>
                
                <div class="modal-body">

                    <div class="form-group" class="formex">
                        <label for="cat" class="col-sm-4 control-label" ><?php echo FIELD_CATEGORY_NAME; ?><span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <select id="catlist" name="fdata[cat]"  class="form-control error-box-class"> 
                                <option value="0">Select Category</option>
                                <?php
                                $this->load->library('mongo_db');
                                $cursor = $this->mongo_db->get_where('Category', array('city_id' => (string) $cityid));

                                foreach ($cursor as $result) {
                                    $cur = (string) $result['_id'];

                                    echo "<option value=" . $cur . ">" . $result['cat_name'] . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-sm-1" id="sloading" style="display: none;">
                            <img src="<?php echo base_url() ?>../../pics/loadingimg.gif" /> 
                        </div>
                    </div>                                        
                    <br>
                    <br>
                    <div class="form-group" class="formex">
                        <div class="frmSearch">
                            <label for="group_name" class="col-sm-4 control-label"><?php echo FIELD_SERVICE_GROUP; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-8">
                                <input name="fdata[group_name]" type="text" id="group_name"  placeholder="service Group Name" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>
                                <div id="suggesstion-box"></div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>                    
                    <div class="form-group" class="formex">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-2">
                            <input type="checkbox"  id="cmult" name="fdata[mand]" class="">
                        </div>
                        <label for="fixed_price" class="col-sm-6 control-label">
                           CHECK IF MULTIPLE CAN BE SELECTED
                        </label>                       
                    </div>
<!--                    <br>
                    <br>                    
                    <div class="form-group" class="formex">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-2">
                            <input type="checkbox"  id="cmand" name="fdata[mult]" class="">
                        </div>
                        <label for="fixed_price" class="col-sm-6 control-label">
                             CHECK IF MANDATORY
                        </label> 
                    </div>-->
                    <br/>
                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4 error-box" id="addsubcat2"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="insertsgroup" ><?php echo BUTTON_ADD; ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
    </button>
</div>
<div class="modal fade stick-up" id="NewCat" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <div class=" clearfix text-left">
                        <button type="button" id="close_nos" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                    </div>
                    <h3 style="font-size: 12px; font-weight:bold;color: #0090d9; "> ADD CATEGORY </h3>
                </div>
                 
                <form action = "<?php echo base_url(); ?>index.php/category/AddNewCategory"  method= "post" enctype="multipart/form-data" onsubmit="return validateFormAddCat();"> 

                    <div class="modal-body">
                        <div class="form-group" class="formex" style="display: none">
                            <label for="fname" class="col-sm-4 control-label" ><?php echo FIELD_VEHICLE_SELECTCITY; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-6">
                                <select id="city_id1"  class="form-control error-box-class" name="city_id" required="" style="text-transform: capitalize;">
                                    <option value="0" style="color: #881212;">Select City</option>
                                    <?php
                                    foreach ($city as $res) {
                                        echo "<option value=" . $res->City_Id . ">" .strtolower($res->City_Name) . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <br>
                        <input type="hidden" id="addbygroup" value="" name="addbygroup"/>
                        <br>
                        <div class="form-group" class="formex">
                            <div class="frmSearch">
                                <label for="price_set_by" class="col-sm-4 control-label"><?php echo FIELD_PRICE_SET_BY_NAME; ?><span style="color:red;font-size: 18px">*</span></label>
                                <div class="col-sm-6">
                                    <select class="form-control" id='price_set_by1' name="price_set_by">
                                        <option value='0'>Who Sets Pricing</option>
                                        <option value='Admin'>Admin</option>     
                                       <option value='Provider'>Provider</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="form-group" class="formex">
                            <label for="fname" class="col-sm-4 control-label"> <?php echo FIELD_FEE_TYPE; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-6">
                                <select onchange="TypeSelected(this)" class="form-control" id='feetype1' name="fee_type">
                                    <option value='0'>Pricing Plan</option>
                                    <option value="Mileage">Mileage</option>
                                    <option value='Hourly'>Hourly</option>     
                                    <option value='Fixed'>Fixed</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <br>                       
                        <div class="form-group" class="formex">
                            <div class="frmSearch">
                                <label for="fname" class="col-sm-4 control-label"><?php echo FIELD_CATEGORY_NAME; ?><span style="color:red;font-size: 18px">*</span></label>
                                <div class="col-sm-8">
                                    <input name="cat_name" type="text" id="cat_name1"  placeholder="Category Name" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>
                                    <div id="suggesstion-box"></div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="form-group" class="formex">
                            <label for="fname" class="col-sm-4 control-label"> <?php echo FIELD_CAN_FEES; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" min="0"  id="can_fees1" name="can_fees" class="form-control error-box-class" placeholder="Cancellation fees">
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="form-group" class="formex">
                            <label style="color:#0090d9" for="fname" class="control-label">CANCELLATION FEES SETTING FOR NOW BOOKING</label>
                            <br>
                            <div class="col-sm-2"></div>
                            <div class="col-sm-1">
                                <input type="radio" id="can_after_some_min" value="0" name="can_condition" class="">
                            </div>
                            <label for="fixed_price" class="col-sm-9 control-label">
                                APPLY AFTER  &nbsp; <input value="10" type="text"  id="cat_time" name="cat_time" style="width: 33px">
                                &nbsp; MIN FROM TIME OF BOOKING
                            </label>
                        </div>
                        <br>
                        <div class="form-group" class="">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-1">
                                <input type="radio" id="can_after_pro_on" value="1" name="can_condition" class="">
                            </div>
                            <label for="can_after_pro_on" class="col-sm-9 control-label">
                                APPLY AFTER PROVIDER IS ON THE WAY
                            </label>
                        </div>
                        <br>
                        <div class="form-group" class="">
                            <label style="color:#0090d9"  for="fname" class="control-label">CANCELLATION FEES SETTING FOR LATER BOOKING</label>
                            <br>
                            <div class="col-sm-2"></div>
                            <div class="col-sm-1">
                                <input type="radio" id="can_after_hr" value="2" name="can_condition" class="">
                            </div>
                            <label for="can_after_hr" class="col-sm-9 control-label">
                                APPLY IF CANCELED 24hr BEFORE TIME OF BOOKING
                            </label>
                        </div>
                        <br>
                        <br>
                        <div class="form-group" class="formex">
                            <label for="cat_desc" class="col-sm-4 control-label"><?php echo FIELD_CATEGORY_DESC; ?></label>
                            <div class="col-sm-6">
                                <textarea type="text"  id="cat_desc1" name="cat_desc"  class="form-control error-box-class" placeholder="Category Desc">
                                </textarea>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="form-group" class="formex">
                            <label for="banner_img" class="col-sm-4 control-label"><?php echo POPUP_BANNER_IMAGE; ?></label>
                            <div class="col-sm-6">
                                <input type="file" class="form-control" style="height: 37px;" id="banner_img1" name="banner_img">
                            </div>
                        </div>                                    
                        <br>
                        <br>
                        <div class="form-group" class="formex">
                            <label for="selimg" class="col-sm-4 control-label"><?php echo POPUP_SEL_CAT_IMAGE; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-6">
                                <input type="file" class="form-control" style="height: 37px;" id="sel_img1" name="sel_img">
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="form-group" class="formex">
                            <label for="unselimg" class="col-sm-4 control-label"><?php echo POPUP_UNSEL_CAT_IMAGE; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-6">
                                <input type="file" class="form-control" style="height: 37px;" id="unsel_img1" name="unsel_img">
                            </div>
                        </div>


                        <div class="mileagep" style="display: none;"> 
                            <br>
                            <br>
                            <div class="form-group" class="formex">
                                <label for="fname" class="col-sm-4 control-label"> <?php echo FIELD_SUB_MIN_FEES; ?><span style="color:red;font-size: 18px">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" min="0"  id="min_fees1" name="min_fees" class="form-control error-box-class" placeholder="Minimum Fees">
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group" class="formex">
                                <label for="fname" class="col-sm-4 control-label"> <?php echo FIELD_SUB_BASE_FEES; ?><span style="color:red;font-size: 18px">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" min="0"  id="base_fees1" name="base_fees" class="form-control error-box-class" placeholder="Base Fees">
                                </div>
                            </div>   
                            <br>
                            <br>


                            <div class="form-group" class="formex">
                                <label for="price_mile" class="col-sm-4 control-label"> <?php echo FIELD_PRICE_PER_MILE; ?><span style="color:red;font-size: 18px">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" min="0"  id="price_mile1" name="price_mile" class="form-control error-box-class" placeholder="Price Mile">
                                </div>
                            </div>
                            <br>
                            <br>                                    

                        </div>

                        <div class="mhmix" style="display: none;">
                            <br/>
                            <br/>
                            <div class="form-group" class="formex">
                                <label for="fname" class="col-sm-4 control-label"> <?php echo "PRICE PER HOUR"; ?><span style="color:red;font-size: 18px">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" min="0"  id="price_per_min1" name="price_min" class="form-control error-box-class" placeholder="Price Per Hour">
                                </div>
                            </div>
                        </div>

                        <div class="hourlyp" style="display: none;">
                            <br>
                            <br>
                            <div class="form-group" class="formex">
                                <label for="visit_fees" class="col-sm-4 control-label"> <?php echo FIELD_VISIT_FEES; ?><span style="color:red;font-size: 18px">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text"  id="visit_fees1" name="visit_fees" class="form-control error-box-class" placeholder="visit fees">
                                </div>
                            </div>
                        </div>

                        <br>
                        <br>                                    
                        <div class="form-group fixedp" class="formex" style="display: none;">
                            <label for="fname" class="col-sm-4 control-label"> <?php echo FIELD_FIXED_PRICE; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" min="0"  id="fixed_price1" name="fixed_price" class="form-control error-box-class" placeholder="Fixed Price">
                            </div>
                        </div>                               

                        <input type="hidden" id="EditServiceId" value=""/>
                        <div class="row">
                            <div class="col-sm-3" ></div>
                            <div class="col-sm-6 error-box" id="addsubcat3"></div>
                            <div class="col-sm-3" >
                                <button type="submit" class="btn btn-primary pull-right" id="insertsubcat12" ><?php echo BUTTON_SAVE; ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
    </button>
</div>
