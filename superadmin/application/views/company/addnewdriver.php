<?php
$this->load->database();
?>
<link href="<?php echo base_url(); ?>theme/build/css/intlTelInput.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>theme/build/css/demo.css" rel="stylesheet" type="text/css" />
<style>
    .add_way_point {
        font-size: 16px;
        background-image: url(http://app.bringg.com/images/860876be.add_driver_icon.png);
        background-position: center left;
        background-repeat: no-repeat;
        padding-left: 22px;
        cursor: pointer;
    }
    .form-horizontal .form-group
    {
        margin-left: 13px;
    }

    .ui-autocomplete{
        z-index: 5000;
    }
    #selectedcity,#companyid{
        display: none;
    }

    .ui-menu-item{cursor: pointer;background: black;color:white;border-bottom: 1px solid white;width: 200px;}
</style>
<script>
    function isNumberKey(evt)
    {
        $("#mobify").text("");
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 45 || charCode > 57)) {
            //    alert("Only numbers are allowed");
            $("#mobify").text(<?php echo json_encode(LIST_COMPANY_MOBIFY); ?>);
            return false;
        }
        return true;
    }
    function Freetypeselected(val)
    {
        $("#text_city").text('');
         var cid = $("#cityid").val();
      var type = val.value;
      
        if (cid != 0)
        {
            $.ajax({
                url: "getCategorybyfreetype",
                type: "POST",
                data: {cid: cid,type:type},
                dataType: "JSON",
                success: function (result) {
                    console.log(result);
                    $("#freeType").empty();
                        $.each(result,  function (index2, row2) {
                            $('#freeType').append("<div class='col-sm-2 checkbox check-primary'><input  onchange='selectMileageType(this)' type='checkbox' class='mcat' id='" + row2.cat_id + "' value='" + row2.cat_id + "'/><label for='" + row2.cat_id + "'> "+ row2.cat_name +"</label></div>");
                        });
                   
                }
            });
        } else {
//         
            $("#text_city").text("Please select the city");
            $("#feetype").val('0');
        
            $("#freeType").empty();
            
        }
    }
    
     function getproforcity(val){
    $("#feetype").val('0');
     $('#freeType').html('');
    }
//    var typearr = [];
    var ftypearr = [];
    var htypearr = [];
    var mtypearr = [];

    
    function selectMileageType(val)
    {
        $('#mcat :checkbox:not([disabled])').attr('checked', false);
        $('#fcat :checkbox:not([disabled])').attr('checked', false);
        ftypearr = [];
        mtypearr = [];

        if (val.checked)
        {
            htypearr.push(val.value);
        } else {
            var index = htypearr.indexOf(val.value);
            htypearr.splice(index, 1);
        }
        document.getElementById('fees_group').value = 2;
        document.getElementById('htypearr').value = htypearr;
    }
     

    $(document).ready(function () {

        $('.drivers').addClass('active');
        $('.Drivers').attr('src', "<?php echo base_url(); ?>/theme/icon/drivers_on.png");

        $('.datepicker-component').on('changeDate', function () {
            $(this).datepicker('hide');
        });



//        $("#datepicker1").datepicker({ minDate: 0});
        var date = new Date();
        $('.datepicker-component').datepicker({
            startDate: date
        });




        $("#firstname").keypress(function (event) {
            var inputValue = event.which;
            //if digits or not a space then don't let keypress work.
            if ((inputValue > 64 && inputValue < 91) // uppercase
                    || (inputValue > 96 && inputValue < 123) // lowercase
                    || inputValue == 32) { // space
                return;
            }
            event.preventDefault();
        });

        $("#lastname").keypress(function (event) {
            var inputValue = event.which;
            //if digits or not a space then don't let keypress work.
            if ((inputValue > 64 && inputValue < 91) // uppercase
                    || (inputValue > 96 && inputValue < 123) // lowercase
                    || inputValue == 32) { // space
                return;
            }
            event.preventDefault();
        });


        $("#file_upload").change(function ()

        {
            var iSize = ($("#file_upload")[0].files[0].size / 1024);

            if (iSize / 1024 > 1)

            {
                $("#file_driver_photo").html("your file is too large");
            } else
            {
                iSize = (Math.round(iSize * 100) / 100)
                $("#file_driver_photo").html(iSize + "kb");

            }



        });
$("#mobile").on("countrychange", function(e, countryData) {
 console.log(countryData.dialCode);
 $("#c_code").val(countryData.dialCode);
});
        $("#file_upload_l").change(function ()

        {

            var iSize = ($("#file_upload_l")[0].files[0].size / 1024);

            if (iSize / 1024 > 1)

            {
                $("#file_driver_license").html("your file is too large");
            } else
            {
                iSize = (Math.round(iSize * 100) / 100)
                $("#file_driver_license").html(iSize + "kb");

            }



        });

        $("#file_upload_p").change(function ()

        {

            var iSize = ($("#file_upload_p")[0].files[0].size / 1024);

            if (iSize / 1024 > 1)

            {
                $("#file_passbook").html("your file is too large");
            } else
            {
                iSize = (Math.round(iSize * 100) / 100)
                $("#file_passbook").html(iSize + "kb");

            }



        });


        $('.drivers').addClass('active');
        $('.driver_thumb').addClass("bg-success");

        $('#city_select').change(function () {
            $('#getvechiletype').load('<?php echo base_url() ?>index.php/superadmin/ajax_call_to_get_types/vtype', {city: $('#city_select').val()});
        });


        $('#title').change(function () {
            $('#vehiclemodel').load('<?php echo base_url() ?>index.php/superadmin/ajax_call_to_get_types/vmodel', {adv: $('#title').val()});
        });
    });

//validations for each previous tab before proceeding to the next tab
    function managebuttonstate()
    {
        $("#prevbutton").addClass("hidden");
    }

    function profiletab(litabtoremove, divtabtoremove)
    {
        var pstatus = true;

        $("#error-box").text("");

        $("#text_firstname").text("");
        $("#text_profession").text("");
        $("#text_lastnmae").text("");
        $("#driver_mobile").text("");
        $("#file_driver_photo").text("");

        var text_city = $("#cityid").val();
        var firstname = $("#firstname").val();
        var lastname = $("#lastname").val();
        var mobile = $('#mobile').val();
        var driverphoto = $('#file_upload').val();
        var htypearr1 = $('#htypearr').val();



        var password = /^(?=.*\d)(?=.*[a-zA-Z])(?!.*[\W_\x7B-\xFF]).{6,15}$/;

        var number = /^[0-9-+]+$/;

//                            var phone = /^\d{10}$/;
//                            var company = /^[-\w\s]+$/;
//                            var re = /[a-zA-Z0-9\-\_]$/;

        var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/; //^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var text = /^[a-zA-Z ]*$/;
        var alphabit = /^[a-zA-Z]+$/;


        if (text_city == "" || text_city == "0")
        {
            $("#text_city").text("Please select the city");
            pstatus = false;
        }else if (htypearr1 == "" || htypearr1 == "0")
        {
            $("#text_profession").text("Please select the service category");
            pstatus = false;
        }
        else if (firstname == "" || firstname == null)
        {
            $("#text_firstname").text(<?php echo json_encode(POPUP_DRIVER_FIRSTNAME); ?>);
            pstatus = false;
        } else if (mobile == "" || mobile == null)
        {
            $("#driver_mobile").text(<?php echo json_encode(POPUP_DRIVER_MOBILE); ?>);
            pstatus = false;
        } 
        
        if (mobile != "")
        {

//             pstatus = false;
            $.ajax({
                url: "validatePhone",
                type: "POST",
                data: {mobile: mobile},
                dataType: "JSON",
                async: false,
                success: function (result) {

                    $('#mobile').attr('data', result.msg);

                    if (result.msg == 1) {

                        pstatus = false;

                        $("#driver_mobile").text("Phone Number is already allocated !");
//                    $('#mobile').focus();

                    } else {
                        console.log("kkk");
                    }

                }

            });
        }
        if(mobile.toString().length <10){
             $("#driver_mobile").text("Phone Number is Minimum 10 digits");
            pstatus = false;
        }
        if (pstatus === false)
        {
            setTimeout(function ()
            {
                proceed(litabtoremove, divtabtoremove, 'firstlitab', 'tab1');
            }, 300);


            $("#tab1icon").removeClass("fs-14 fa fa-check");
            return false;
        }
        $("#tab1icon").addClass("fs-14 fa fa-check");
        $("#prevbutton").removeClass("hidden");
        $("#nextbutton").removeClass("hidden");
        $("#finishbutton").addClass("hidden");
        return true;
    }

    function addresstab(litabtoremove, divtabtoremove)
    {
        var astatus = true;




        if (profiletab(litabtoremove, divtabtoremove))
        {
            astatus == true;

            $("#text_password").text("");
            $("#text_zip").text("");

            var email = $("#email").val();
            var password = $("#password").val();
            var zipcode = $('#zipcode').val();
//         var driverphoto = $('#file_upload').val();
           var emails = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/; 
                          
  


            var pass = /^(?=.*\d)(?=.*[a-zA-Z])(?!.*[\W_\x7B-\xFF]).{6,15}$/;

            if (email == "" || email == null)
            {
                $("#editerrorbox").html(<?php echo json_encode(POPUP_DRIVER_DRIVER_EMAIL); ?>);
                astatus = false;
            }
//                            

             else if ((!emails.test(email)))
            {
                $("#editerrorbox").html(<?php echo json_encode(POPUP_DRIVER_DRIVER_YOUREMAIL); ?>);
                astatus = false;


            }
            else if ($("#email").attr('data') == 1)
            {
                $("#editerrorbox").html(<?php echo json_encode(POPUP_DRIVER_DRIVER_ALLOCATED); ?>);
                astatus = false;


            }

            else if (password == "" || password == null)
            {
                $("#text_email").text("");
                $("#text_password").text(<?php echo json_encode(POPUP_DRIVER_DRIVER_PASSWORD); ?>);
                astatus = false;
            }
            else if ((!pass.test(password)))
            {
                $("#text_email").text("");
                $("#text_password").text(<?php echo json_encode(POPUP_DRIVER_DRIVER_PASSWORD_VALID); ?>);
                astatus = false;
            }


//            else if (zipcode == "" || zipcode == null)
//            {
//                $("#text_password").text("");
//                $("#text_zip").text(<?php echo json_encode(POPUP_DRIVER_DRIVER_ZIPCODE); ?>);
//                astatus = false;
//            }

            if (astatus === false)
            {
                setTimeout(function ()
                {
                    proceed(litabtoremove, divtabtoremove, 'secondlitab', 'tab2');

                }, 100);

//                alert("complete Login Details tab properly")
                $("#tab2icon").removeClass("fs-14 fa fa-check");
                return false;
            }

            $("#tab2icon").addClass("fs-14 fa fa-check");
            $("#finishbutton").removeClass("hidden");
            $("#nextbutton").addClass("hidden");

            return astatus;
        }
    }

    function bonafidetab(litabtoremove, divtabtoremove)
    {
        var bstatus = true;
        if (addresstab(litabtoremove, divtabtoremove))
        {
            if (isBlank($("#file_upload_l").val()) || isBlank($("#expirationrc").val()) || isBlank($("#file_upload_p").val()))
            {
                bstatus = false;
            }

            if (bstatus === false)
            {
                setTimeout(function ()
                {
                    proceed(litabtoremove, divtabtoremove, 'thirdlitab', 'tab3');

                }, 100);

                alert("complete Driving Licence tab properly");
                $("#tab3icon").removeClass("fs-14 fa fa-check");
                return false;
            }

            $("#tab3icon").addClass("fs-14 fa fa-check");
            $("#nextbutton").addClass("hidden");
            $("#finishbutton").removeClass("hidden");

            return bstatus;

        }
    }

    function signatorytab(litabtoremove, divtabtoremove)
    {
        var bstatus = true;
        if (bonafidetab(litabtoremove, divtabtoremove))
        {
            if (isBlank($("#entitypersonname").val()) || isBlank($("#entitysignatorymobileno").val()) || isBlank($("#entitysignatoryimagefile").val()) || $("#entitydegination").val() === "null")
            {
                bstatus = false;
            }

            if (validateEmail($("#entityemail").val()) !== 2)
            {
                bstatus = false;
            }

            if (bstatus === false)
            {
                setTimeout(function ()
                {
                    proceed(litabtoremove, divtabtoremove, 'fourthlitab', 'tab4');

                }, 100);

                alert("complete Other Document tab properly");
                $("#tab4icon").removeClass("fs-14 fa fa-check");
                return false;
            }

            $("#tab4icon").addClass("fs-14 fa fa-check");
            $("#nextbutton").addClass("hidden");
            $("#finishbutton").removeClass("hidden");

            return bstatus;
        }

    }


    function proceed(litabtoremove, divtabtoremove, litabtoadd, divtabtoadd)
    {
        $("#" + litabtoremove).removeClass("active");
        $("#" + divtabtoremove).removeClass("active");

        $("#" + litabtoadd).addClass("active");
        $("#" + divtabtoadd).addClass("active");
    }

    /*-----managing direct click on tab is over -----*/

//manage next next and finish button
    function movetonext()
    {
        var currenttabstatus = $("li.active").attr('id');
        if (currenttabstatus === "firstlitab")
        {
            profiletab('secondlitab', 'tab2');
            proceed('firstlitab', 'tab1', 'secondlitab', 'tab2');
        }
        else if (currenttabstatus === "secondlitab")
        {
            addresstab('thirdlitab', 'tab3');
            proceed('secondlitab', 'tab2', 'thirdlitab', 'tab3');

        }
        else if (currenttabstatus === "thirdlitab")
        {
            bonafidetab('fourthlitab', 'tab4');
            proceed('thirdlitab', 'tab3', 'fourthlitab', 'tab4');

            $("#finishbutton").removeClass("hidden");
            $("#nextbutton").addClass("hidden");
        }
    }

    function movetoprevious()
    {
        var currenttabstatus = $("li.active").attr('id');
        if (currenttabstatus === "secondlitab")
        {
            profiletab('secondlitab', 'tab2');
            proceed('secondlitab', 'tab2', 'firstlitab', 'tab1');
            $("#prevbutton").addClass("hidden");
        }
        else if (currenttabstatus === "thirdlitab")
        {
            addresstab('thirdlitab', 'tab3');
            proceed('thirdlitab', 'tab3', 'secondlitab', 'tab2');
            $("#nextbutton").removeClass("hidden");
            $("#finishbutton").addClass("hidden");
            $("#prevbutton").removeClass("hidden");
        }
//    else if(currenttabstatus === "fourthlitab")
//    {
//        bonafidetab('fourthlitab','tab4');
//        proceed('fourthlitab','tab4','thirdlitab','tab3');
//        $("#nextbutton").removeClass("hidden");
//        $("#finishbutton").addClass("hidden");
//    }
    }
//here this function validates all the field of form while adding new subadmin you can find all related functions in RylandInsurence.js file

    function validate() {

        if (!isBlank($("#Firstname").val()))
        {
            if (!isAlphabet($("#Firstname").val()))
            {
                $("#errorbox").html("Enter only character in First name");
                return false;
            }
        } else
        {
            $("#errorbox").html("First name is blank");
            return false;
        }
    }
    function validateForm()
    {
        if (!isBlank($("#Firstname").val()))
        {
            if (!isAlphabet($("#Firstname").val()))
            {
                $("#errorbox").html("Enter only character in First name");
                return false;
            }
        } else
        {
            $("#errorbox").html("First name is blank");
            return false;
        }

        if (!isBlank($("#Lastname").val()))
        {
            if (!isAlphabet($("#Lastname").val()))
            {
                $("#errorbox").html("Enter only character in Last name");
                return false;
            }
        } else
        {
            $("#errorbox").html("Last name is blank");
            return false;
        }

        if (validateEmail($("#Email").val()) == 1)
        {

            $("#errorbox").html("Enter valid email");
            return false;
        }

        if (isBlank($("#Password").val()))
        {
            $("#errorbox").html("Password is Blank");
            return false;
        }

        if (!MatchPassword($("#Password").val(), $("#Cpassword").val()))
        {
            $("#errorbox").html("Password not matching");
            return false;
        }
        // return true;
    }

    function ValidateFromDb() {

        $.ajax({
            url: "validateEmail",
            type: "POST",
            data: {email: $('#email').val()},
            dataType: "JSON",
            success: function (result) {

                $('#email').attr('data', result.msg);

                if (result.msg == 1) {

                    $("#editerrorbox").html("Email is already allocated !");
                    $('#email').focus();
                    return false;
                } else if (result.msg == 0) {
                    $("#editerrorbox").html("");

                }
            }
        });

    }


    function submitform()
    {
        document.getElementById('sbut').disabled = true;
        $("#text_password").text("");
        $("#text_zip").text("");

        var email = $("#email").val();
        var password = $("#password").val();
        var zipcode = $('#zipcode').val();
        var vemail = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/; //^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var pass = /^(?=.*\d)(?=.*[a-zA-Z])(?!.*[\W_\x7B-\xFF]).{6,15}$/;

        if (email == "" || email == null)
        {
            $("#editerrorbox").html(<?php echo json_encode(POPUP_DRIVER_DRIVER_EMAIL); ?>);

        }
//                            

        else if (!(vemail.test(email)))
        {
            $("#editerrorbox").html(<?php echo json_encode(POPUP_DRIVER_DRIVER_YOUREMAIL); ?>);



        } else if ($("#email").attr('data') == 1)
        {
            $("#editerrorbox").html(<?php echo json_encode(POPUP_DRIVER_DRIVER_ALLOCATED); ?>);



        } else if (password == "" || password == null)
        {
            $("#text_email").text("");
            $("#text_password").text(<?php echo json_encode(POPUP_DRIVER_DRIVER_PASSWORD); ?>);

        } else if ((!pass.test(password)))
        {
            $("#text_email").text("");
            $("#text_password").text(<?php echo json_encode(POPUP_DRIVER_DRIVER_PASSWORD_VALID); ?>);

//        } else if (zipcode == "" || zipcode == null)
//        {
//            $("#text_password").text("");
//            $("#text_zip").text(<?php echo json_encode(POPUP_DRIVER_DRIVER_ZIPCODE); ?>);

        } else
            $('#addentity').submit();
//        }

    }

</script>



<script>
     $(document).ready(function () {
        
   var counttoadded = 0;
        $('#otheeducation').click(function () {


            var code = '<div class="col-sm-12" style="border: 1px solid #e6e6e6; width:98%" id="div' + counttoadded + '"> <div class="pull-right"  ><button alt="div' + counttoadded + '" class="btn btn-danger" type="button" id="temp" onclick="deleteeducation(this)"> \n\
          <i class="fa fa-trash-o"></i> </button> </div> <div class="form-group" style="width: 97%;"> \n\
        <label for="name" class="col-sm-3 control-label">DEGREE</label>\n\
    <div class="col-sm-6"> \n\
     <input type="text" class="form-control " id="position"  placeholder="Education degree" name="education[' + counttoadded + '][degree]"  aria-required="true" aria-invalid="true" maxlength="100"> \n\
          </div> </div> <div class="form-group" style="width: 97%;"> <label for="name" class="col-sm-3 control-label">Duration</label> <div class="col-sm-9"> \n\
      <div class=""> <div id="datepicker-range"> <div class="" aria-required="true">\n\
      <div class="col-sm-4"> \n\
      <select class="form-control" name="education[' + counttoadded + '][start_year]"><option value="null">Select...</option>\n\
<?php for ($i = 1950; $i < 2017; $i++) { ?>\n\
                                                                                                                          <option value="<?php echo $i ?>"><?php echo $i ?></option>\n\
<?php } ?>\n\
      </select>\n\
      </div>\n\
      <div class="col-sm-4">\n\
      <select class="form-control" name="education[' + counttoadded + '][end_year]"><option value="null">Select...</option>\n\
<?php for ($i = 1950; $i < 2017; $i++) { ?>\n\
                                                                                                                          <option value="<?php echo $i ?>"><?php echo $i ?></option>\n\
<?php } ?>\n\
      </select>\n\
      </div>\n\
      </div></div> </div> \n\
      </div></div><div class="form-group" style="width: 97%;"> <label for="name" class="col-sm-3 control-label">INSTITUTE</label> <div class="col-sm-6"> <textarea class="form-control" name="education[' + counttoadded + '][institute]"></textarea> </div> </div>';
            $('#addexperiencedyanamic').append(code);
            counttoadded++;
        });
    });
    function deleteeducation(val) {
        if (val.id == 'temp') {

            $("#" + $(val).attr('alt')).remove();
        } else {

            $.ajax({
                url: "delete_education",
                type: "POST",
                data: {idtodelete: val.id},
                dataType: "JSON",
                success: function (result) {


                    $('#div' + val.id).remove();
                }
            });
        }



    }
</script>
 
<script>
    
    var componentForm = {
        administrative_area_level_1: 'long_name',
        country: 'long_name',
        postal_code: 'short_name'
    };
    function initMap() {
        var input = (document.getElementById('proaddress'));
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.addListener('place_changed', function () {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }
            console.log(place);
            for (var component in componentForm) {
                document.getElementById(component).value = '';
            }
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }
            }


        });
    }
</script>

<div class="page-content-wrapper">
    <!-- START PAGE CONTENT -->
    <div class="content">
        <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb" style="color:#0090d9;">
                    <li><a href="Drivers/my/1" class=""><?php echo LIST_DRIVER; ?></a>
                    </li>

                    <li style="width: 100px"><a href="#" class="active"><?php echo LIST_DRIVER_ADDNEW; ?></a>
                    </li>
                </ul>


                <!-- END BREADCRUMB -->
            </div>
        <!-- START JUMBOTRON -->
        <div class="jumbotron bg-white" data-pages="parallax">
<!--            <div class="inner">
                 START BREADCRUMB 
                <ul class="breadcrumb" style="margin-left: 20px;">
                    <li><a href="Drivers/my/1" class=""><?php echo LIST_DRIVER; ?></a>
                    </li>

                    <li style="width: 100px"><a href="#" class="active"><?php echo LIST_DRIVER_ADDNEW; ?></a>
                    </li>
                </ul>
                 END BREADCRUMB 
            </div>-->



            <div class="container-fluid container-fixed-lg bg-white">

                <div id="rootwizard" class="m-t-50">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-tabs-linetriangle nav-tabs-separator nav-stack-sm" id="mytabs">
                        <li class="active" id="firstlitab" onclick="managebuttonstate()">
                            <a data-toggle="tab" href="#tab1" id="tb1"><i id="tab1icon" class=""></i> <span><?php echo LIST_DRIVER_PESIONALDETAILS; ?></span></a>
                        </li>
                        <li class="" id="secondlitab">
                            <a data-toggle="tab" href="#tab2" onclick="profiletab('secondlitab', 'tab2')" id="mtab2"><i id="tab2icon" class=""></i> <span><?php echo LIST_DRIVER_LOGINDETAILS; ?></span></a>
                        </li>
                        <li class="" id="thirdlitab">
                            <a data-toggle="tab" href="#tab3" onclick="addresstab('thirdlitab', 'tab3')"><i id="tab3icon" class=""></i> <span>OTHER DETAILS</span></a>
                        </li>
                        <!--    <li class="" id="fourthlitab">-->
                        <!--        <a data-toggle="tab" href="#tab4" onclick="bonafidetab('fourthlitab','tab4')"><i id="tab4icon" class=""></i> <span>Other Documents</span></a>-->
                        <!--    </li>-->
                    </ul>
                    <!-- Tab panes -->
                   
                    <form id="addentity" class="form-horizontal" role="form" action="<?php echo base_url(); ?>index.php/superadmin/AddNewDriverData" method="post" enctype="multipart/form-data">
                        <div class="tab-content">
                            <div class="tab-pane padding-20 slide-left active" id="tab1">
                                <div class="row row-same-height">
                                    <div class="form-group" class="formex">
                                        <label for="cityid" class="col-sm-3"><?php echo SELECT_CITY; ?><span style="color:red;font-size: 18px">*</span></label>
                                        <div class="col-sm-6">
                                            <select onchange="getproforcity(this)" id="cityid" name="cityid"  class="form-control error-box-class" style="text-transform: capitalize;">
                                                <option value="0">Select City</option>
                                                <?php
                                                foreach ($city as $res) {
                                                    echo '<option value="' . $res->City_Id . '">' .strtolower($res->City_Name) . '  </option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-3 error-box" id="text_city">
                                        </div>
                                    </div>
                                    <div class="form-group" class="formex">
                                        <label for="cityid" class="col-sm-3">SELECT FEE TYPE<span style="color:red;font-size: 18px">*</span></label>
                                        <div class="col-sm-6">
                                            <select onchange="Freetypeselected(this)" class="form-control" id='feetype' name="fee_type">
                                            <option value='0'>Pricing Plan</option>
                                           <option value="Mileage">Mileage</option>
                                            <option value='Hourly'>Hourly</option>     
                                           <option value='Fixed'>Fixed</option>
                                        </select>
                                        </div>
                                         <div class="col-sm-3 error-box" id="text_profession">
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-6" id="freeType"></div>
                                        </div>
                                       
                                         <input type="hidden" id="fees_group" name="fees_group"/>
                                        <input type="hidden" id="mtypearr" name="mtypearr"/>
                                        <input type="hidden" id="htypearr" name="htypearr"/>    
                                        <input type="hidden" id="ftypearr" name="ftypearr"/>
                                    </div>    
                                
 

                                    <div class="form-group">
                                        <label for="fname" class="col-sm-3 "><?php echo FIELD_DRIVERS_FIRSTNAME; ?><span style="color:red;font-size: 18px">*</span></label>
                                        <div class="col-sm-6">

                                            <input type="text"  id="firstname" name="firstname" required="required"class="form-control">

                                        </div>
                                        <div class="col-sm-3 error-box" id="text_firstname"></div>
                                    </div>

                                    <div class="form-group">
                                        <label for="fname" class="col-sm-3 "><?php echo FIELD_DRIVERS_LASTNAME; ?></label>
                                        <div class="col-sm-6">
                                            <input type="text" id="lastname" name="lastname" class="form-control">
                                        </div>
                                        <div class="col-sm-3 error-box" id="text_lastnmae"></div>
                                    </div>
                                    <input type="hidden" id="c_code" name="c_code">
                                    <div class="form-group">
                                        <label for="fname" class="col-sm-3 "><?php echo FIELD_DRIVERS_MOBILE; ?></label>
                                        <div class="col-sm-6">
                                            <input type="tel"  id="mobile" name="mobile" required="required"class="form-control" placeholder="1234567890" maxlength="10" onkeypress="return isNumberKey(event)">
                                        </div>
                                        <div class="col-sm-3 error-box" id="driver_mobile"></div>
                                    </div>
<!--                                        <div class="form-group">
                                        <label for="fname" class="col-sm-3 "><?php echo FIELD_DRIVERS_MOBILE; ?><span style="color:red;font-size: 18px">*</span></label>
                                        <div class="col-sm-6">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon">+</span>
                                                    <input type="hide"  id="c_code" name="c_code" required="required"class="form-control" onkeypress="return isNumberKey(event)">
                                                </div>
                                                </div>
                                                    <div class="col-sm-9">
                                                
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 error-box" id="driver_mobile"></div>
                                    </div>-->
<!--                                     <div class="form-group">
                                        <label for="fname" class="col-sm-3 ">COUNTRY CODE<span style="color:red;font-size: 18px">*</span></label>
                                        <div class="col-sm-6">

                                            <input type="text"  id="c_code" name="c_code" required="required"class="form-control" >

                                        </div>
                                        <div class="col-sm-3 error-box" id="co_code"></div>
                                    </div>

                                    <div class="form-group">
                                        <label for="fname" class="col-sm-3 "><?php echo FIELD_DRIVERS_MOBILE; ?><span style="color:red;font-size: 18px">*</span></label>
                                        <div class="col-sm-6">

                                            <input type="text"  id="mobile" name="mobile" required="required"class="form-control" onkeypress="return isNumberKey(event)">

                                        </div>
                                        <div class="col-sm-3 error-box" id="driver_mobile"></div>
                                    </div>-->
                                    <div class="form-group">
                                        <label for="address" class="col-sm-3 "><?php echo FIELD_DRIVERS_UPLOADPHOTO; ?><span style="color:red;font-size: 18px"></span></label>
                                        <div class="col-sm-6">
                                            <!--                <input type="text" class="form-control" name="entitydocname" id="entitydocname">-->
                                            <input type="file" class="form-control" style="height: 37px;" name="photos" id="file_upload">
                                        </div>
                                        <div class="col-sm-3 error-box" id="file_driver_photo"></div>
                                    </div>
                                   
                                </div>
                            </div>
                            <div class="tab-pane slide-left padding-20" id="tab2">
                                <div class="row row-same-height">

                                    <div class="form-group">
                                        <label for="fname" class="col-sm-3"><?php echo FIELD_DRIVERS_EMAIL; ?><span style="color:red;font-size: 18px">*</span></label>
                                        <div class="col-sm-6">

                                            <input type="email" data="1" id="email" name="email" required="required" onblur="ValidateFromDb()" class="form-control"
                                                   readonly  
                                                   onfocus="this.removeAttribute('readonly');">


                                        </div>
                                        <span id="editerrorbox" class="col-sm-3 " style="color: #ff0000"></span>
                                        <div class="col-sm-3 error-box" id="text_email"></div>
                                    </div>




                                    <div class="form-group">
                                        <label for="fname" class="col-sm-3 "><?php echo FIELD_DRIVERS_PASSWORD; ?><span style="color:red;font-size: 18px">*</span></label>
                                        <div class="col-sm-6">

                                            <input type="password"  id="password" name="password" required="required" class="form-control">

                                        </div>
                                        <div class="col-sm-3 error-box" id="text_password"></div>
                                    </div>

<!--                                    <div class="form-group">
                                        <label for="fname" class="col-sm-3 control-label"><?php echo FIELD_DRIVERS_ZIPCODE; ?><span style="color:red;font-size: 18px">*</span></label>
                                        <div class="col-sm-6">

                                            <input type="text"  id="zipcode" name="zipcode" required="required" class="form-control" onkeypress="return isNumberKey(event)">

                                        </div>
                                        <div class="col-sm-3 error-box" id="text_zip"></div>
                                    </div>

                                    <div class="form-group">
                                        <label for="fname" class="col-sm-3 control-label"><?php echo FIELD_DRIVERS_LICENCE_NUM; ?><span style="color:red;font-size: 18px">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control " id="position" value="<?php echo $userinfo->medical_license_num; ?>" placeholder=""   name="fdata[medical_license_num]" aria-required="true" aria-invalid="true">
                                        </div>
                                        <div class="col-sm-3 error-box" id="text_zip"></div>
                                    </div>

                                    <div class="form-group">
                                        <label for="fname" class="col-sm-3 control-label"><?php echo FIELD_DRIVERS_LICENCE_EXPIRY; ?><span style="color:red;font-size: 18px">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="date" class="form-control " id="position" name="fdata[board_certification_expiry_dt]" value="<?php echo ($userinfo->board_certification_expiry_dt == '') ? date('m/d/Y', time()) : $userinfo->board_certification_expiry_dt; ?>" placeholder=""  aria-required="true" aria-invalid="true">
                                        </div>
                                        <div class="col-sm-3 error-box" id="text_zip"></div>
                                    </div>-->

<!--                                    <div class="form-group">
                                        <div class="col-sm-6 ">
                                            <div style="padding-bottom: 12px;padding-top: 21px;">
                                                <button class="btn btn-primary btn-cons" type="button" id="board" alt="1">Upload</button>
                                                <input type="hidden" value="<?php echo $userinfo->board_certificate ?>" id="board_certificate" name="fdata[board_certificate]">
                                            </div>
                                            <div>
                                                <?php
                                                $file_formats = array("jpg", "png", "gif", "jpeg");
                                                if ($userinfo->board_certificate) {

                                                    $d_img = base_url() . '../pics/' . $userinfo->board_certificate;
                                                } else
                                                    $d_img = base_url() . 'theme/assets/img/certificatedft.png';
                                                ?>
                                                <?php if ($userinfo->board_certificate) { ?>
                                                    <a href = "<?php echo $d_img; ?>" target = "_blank" ><button class="btn btn-success btn-cons" type = "button" > View</button ></a >
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 ">
                                            <?php
                                            $ext = substr($d_img, strrpos($d_img, '.') + 1);
                                            if (!in_array($ext, $file_formats)) {
                                                $d_img = base_url() . 'theme/assets/img/certificatedft.png';
                                            }
                                            ?>
                                            <img src="<?php echo $d_img ?>" style="width: 130px;height: 108px;border-radius: 8px;" id="show_board">
                                        </div>
                                    </div>-->

                                </div>
                            </div>

                            
                             <div class="tab-pane slide-left padding-20" id="tab3">
                                <div class="row row-same-height">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="fname" class="col-sm-3 ">OFFICE ADDRESS<span style="color:red;font-size: 18px"></span></label>

                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control" style="height: 37px;" name="proaddress" id="proaddress" aria-required="true" aria-invalid="true" >
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="fname" class="col-sm-3 ">COUNTRY<span style="color:red;font-size: 18px"></span></label>

                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control" style="height: 37px;" name="country" id="country" aria-required="true" aria-invalid="true" >
                                                </div>
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="fname" class="col-sm-3 ">STATE<span style="color:red;font-size: 18px"></span></label>

                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control" style="height: 37px;" name="state" id="administrative_area_level_1" aria-required="true" aria-invalid="true">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="fname" class="col-sm-3 ">ZIP<span style="color:red;font-size: 18px"></span></label>

                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control" style="height: 37px;" name="postal_code" id="postal_code" aria-required="true" aria-invalid="true">
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="fname" class="col-sm-3 ">TAX NUMBER<span style="color:red;font-size: 18px"></span></label>

                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control" style="height: 37px;" name="tax_num" id="tax_num" aria-required="true" aria-invalid="true" >
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="fname" class="col-sm-3 ">RADIUS TO ACCEPT JOB IN THE RANGE OF (KM)<span style="color:red;font-size: 18px"></span></label>

                                                <div class="col-sm-6">
                                                   
                                                            <select class="form-control" name="radius" id="radius">
                                                            <?php
                                                            for ($i = 5; $i < 30; $i++) {
                                                                 
                                                                    ?>
                                                                        <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                                                        
                                                                        <?php
                                                                    
                                                                }
                                                                ?>
                                                            </select>
                                                </div>
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="fname" class="col-sm-3 ">LICENSE EXPIRY<span style="color:red;font-size: 18px"></span></label>

                                                <div class="col-sm-6">
                                                    <input id="expirationrc_l" name="expirationrc_l" required="required"  type="" class="form-control datepicker-component"  >
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="fname" class="col-sm-3 ">LICENSE NO<span style="color:red;font-size: 18px"></span></label>

                                                <div class="col-sm-6">
                                                    <input id="licence_no" name="licence_no" required="required"  type="" class="form-control"  >
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="fname" class="col-sm-3 ">ABOUT<span style="color:red;font-size: 18px"></span></label>
                                        <div class="col-sm-6">
                                            <textarea type="text" class="form-control " id="aboutdata"   name="aboutdata"  aria-required="true" aria-invalid="true" ></textarea>
                                        </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="fname" class="col-sm-3 no-padding">EXPERTISE<span style="color:red;font-size: 18px"></span></label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control " id="experties"  name="experties" data-Role="tagsinput"  aria-required="true" aria-invalid="true" >
                                        </div>
                                            </div>
                                        </div><br>
                                        
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="fname" class="col-sm-3 no-padding">LANGUAGES KNOWN<span style="color:red;font-size: 18px"></span></label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control " id="lanKnow" name="lanKnow" data-Role="tagsinput" aria-required="true" aria-invalid="true" >
                                        </div>
                                            </div>
                                          
                                             <div class="col-sm-6">
                                                    <label for="fname" class="col-sm-3 ">UPLOAD PRACTICE CERTIFICATE<span style="color:red;font-size: 18px"></span></label>

                                        <div class="col-sm-6">
                                            <input type="file" class="form-control" style="height: 37px;" name="certificate" id="file_upload_l" >
                                        </div>
                                        <div class="col-sm-3 error-box" id="file_driver_license"></div>
                                                </div>
                                        </div>



                                    </div>
                                  
                                     
                                   

<!--                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-3">Education Information </label>
                                                        <div class="col-sm-9">

                                                           
                                                            <div  id="addexperiencedyanamic" >



                                                            </div>
                                                            <br>
                                                            <a id="otheeducation" class="add_way_point"> <?php

    echo "Click to add Education.";
?>
                                                            </a>

                                                        </div>





                                                    </div>-->
                                                 
                                   
<!--                                    <div class="form-group">
                                        <label for="fname" class="col-sm-3 control-label"><?php echo FIELD_DRIVERS_UPLOADPASSBOOK; ?><span style="color:red;font-size: 18px">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="file" class="form-control" style="height: 37px;" name="passbook"  id="file_upload_p">
                                        </div>
                                        <div class="col-sm-3 error-box" id=file_passbook></div>
                                    </div>-->
                                    
<!--                                     <div class="form-group">
                                        <label for="fname" class="col-sm-3 control-label"><?php echo FIELD_PASSBOOK_EXDATE; ?><span style="color:red;font-size: 18px">*</span></label>
                                        <div class="col-sm-6">
                                            <input id="expirationPassbook" name="expirationPassbook" required="required"  type="" class="form-control datepicker-component"  >
                                        </div>
                                        <div class="col-sm-3 error-box" id="file_passbook_exp"></div>
                                    </div>-->

                                </div>
                            </div>

                            <div class="padding-20 bg-white">
                                <ul class="pager wizard">
                                    <li class="next" id="nextbutton">
                                        <button class="btn btn-primary btn-cons btn-animated from-left  pull-right" type="button" onclick="movetonext()">
                                            <span><?php echo BUTTON_NEXT; ?></span>
                                        </button>
                                    </li>
                                    <li class="hidden" id="finishbutton">
                                        <button class="btn btn-primary btn-cons btn-animated from-left fa fa-cog pull-right" type="button" onclick="submitform()" id="sbut">
                                            <span><?php echo BUTTON_FINISH; ?></span>
                                        </button>
                                    </li>

                                    <li class="previous hidden" id="prevbutton">
                                        <button class="btn btn-default btn-cons pull-right" type="button" onclick="movetoprevious()">
                                            <span><?php echo BUTTON_PREVIOUS; ?></span>
                                        </button>
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <input type="hidden" name="current_dt" id="time_hidden" value=""/>
                    </form> 

                </div>


            </div>
            <!-- END PANEL -->
        </div>

    </div>
    <!-- END JUMBOTRON -->

    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
        <!-- BEGIN PlACE PAGE CONTENT HERE -->

        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

</div>
<!-- END PAGE CONTENT -->
<!-- START FOOTER -->

<!-- END FOOTER -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGb3Eqs1luybPwJKIBb-jNRRI0EGTehtc&libraries=places&callback=initMap"
async defer></script>
 <script src="<?php echo base_url(); ?>theme/build/js/intlTelInput.js"></script>
  <script>
      var countryData = $.fn.intlTelInput.getCountryData();
$.each(countryData, function(i, country) {
  country.name = country.name.replace(/.+\((.+)\)/,"$1");
});
//    $("#mobile").intlTelInput({
//      // allowDropdown: false,
//      // autoHideDialCode: false,
//      // autoPlaceholder: "off",
//      // dropdownContainer: "body",
//      // excludeCountries: ["us"],
//      // formatOnDisplay: false,
//       geoIpLookup: function(callback) {
//         $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
//           var countryCode = (resp && resp.country) ? resp.country : "";
//           callback(countryCode);
//         });
//       },
//      // initialCountry: "auto",
//      // nationalMode: false,
//      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
//      // placeholderNumberType: "MOBILE",
//      // preferredCountries: ['cn', 'jp'],
//       separateDialCode: true,
//      utilsScript: "<?php echo base_url(); ?>theme/build/js/utils.js",
//      
//    });

 $("#mobile").intlTelInput({initialCountry: "auto", geoIpLookup: function (callback) {
                                                  $.get('http://ipinfo.io', function () {}, "jsonp").always(function (resp) {
                                                      var countryCode = (resp && resp.country) ? resp.country : "";
                                                      callback(countryCode);
                                                  });
                                              }, separateDialCode: true, utilsScript: "<?php echo base_url(); ?>theme/build/js/utils.js"});

     
  </script>