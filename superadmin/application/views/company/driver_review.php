<?php
date_default_timezone_set('UTC');
$rupee = "$";
//error_reporting(0);
?>

<style>
    .ui-state-default{
        font-size : 10px !important; 
    }
    .ui-autocomplete{
        z-index: 5000;
    }
    #selectedcity,#companyid{
        display: none;
    }

    .ui-menu-item{cursor: pointer;background: black;color:white;border-bottom: 1px solid white;width: 200px;}
</style>
<script>
    $(document).ready(function () {
        $("#define_page").html("Driver Review");
        $('.driver_review').addClass('active');
        $('.driver_review').attr('src', "<?php echo base_url(); ?>/theme/icon/driver review_on.png");
//        $('.driver_review_thumb').addClass("bg-success");

        $('#searchData').click(function () {


            var dateObject = $("#start").datepicker("getDate"); // get the date object
            var st = dateObject.getFullYear() + '-' + (dateObject.getMonth() + 1) + '-' + dateObject.getDate();// Y-n-j in php date() format
            var dateObject = $("#end").datepicker("getDate"); // get the date object
            var end = dateObject.getFullYear() + '-' + (dateObject.getMonth() + 1) + '-' + dateObject.getDate();// Y-n-j in php date() format

            $('#createcontrollerurl').attr('href', '<?php echo base_url() ?>index.php/superadmin/Get_dataformdate/' + st + '/' + end);

        });

        $('#search_by_select').change(function () {


            $('#atag').attr('href', '<?php echo base_url() ?>index.php/superadmin/search_by_select/' + $('#search_by_select').val());

            $("#callone").trigger("click");
        });

        var table = $('#tableWithSearch1');

        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "sPaginationType": "bootstrap",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 20,
            "order": [[0, "desc"]]
        };

        table.dataTable(settings);

        $('#search-table1').keyup(function () {
            table.fnFilter($(this).val());
        });



        $("#inactive").click(function () {
            $("#display-data").text("");
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).
                    get();
//              var appid=$('#appid').val();
//     

            if (val.length > 0) {

                //if (confirm("Are you sure you want to deactivate " + val.length + " driver review/reviews"))

                var size = $('input[name=stickup_toggler]:checked').val()
                var modalElem = $('#confirmmodel');
                if (size == "mini")
                {
                    $('#modalStickUpSmall').modal('show')
                } else
                {
                    $('#confirmmodel').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }
                $("#errorboxdata").text(<?php echo json_encode(POPUP_PASSENGERS_DEACTIVATE); ?>);

                $("#confirmed").click(function () {



                    $.ajax({
                        url: "<?php echo base_url('index.php/superadmin') ?>/inactivedriver_review",
                        type: "POST",
                        data: {val: val},
                        dataType: 'json',
                        success: function (result)
                        {

                            $('.checkbox:checked').each(function (i) {
                                $(this).closest('tr').remove();
                            });
                            $(".close").trigger('click');
                        }
                    });
                    $('.checkbox:checked').each(function (i) {
                        $(this).closest('tr').remove();
                    });
                    $(".close").trigger('click');


                });
            } else
            {
                //    alert("select atleast one passenger");
                $("#display-data").text(<?php echo json_encode(POPUP_DRIVERREVIEW_ATLEAST); ?>);
            }

        });


        $("#active").click(function () {
            $("#display-data").text("");
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();

            if (val.length > 0)
            {

                // if (confirm("Are you sure to activate " + val.length + " driver review/reviews"))


                var size = $('input[name=stickup_toggler]:checked').val()
                var modalElem = $('#confirmmodels');
                if (size == "mini")
                {
                    $('#modalStickUpSmall').modal('show')
                } else
                {
                    $('#confirmmodels').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }
                $("#errorboxdatas").text(<?php echo json_encode(POPUP_PASSENGERS_ACTIVATE); ?>);

                $("#confirmeds").click(function () {
                    {
                        $.ajax({
                            url: "<?php echo base_url('index.php/superadmin') ?>/activedriver_review",
                            type: "POST",
                            data: {val: val},
                            dataType: 'json',
                            success: function (result)
                            {

                                $('.checkbox:checked').each(function (i) {
                                    $(this).closest('tr').remove();
                                });
                                $(".close").trigger('click');
                            }
                        }); $('.checkbox:checked').each(function (i) {
                                    $(this).closest('tr').remove();
                                });
                                $(".close").trigger('click');
                    }

                });

            } else
            {
                //      alert("select atleast one passenger");
                $("#display-data").text(<?php echo json_encode(POPUP_DRIVERREVIEW_ATLEAST); ?>);
            }

        });
        $("#edit_review").click(function () {
            $("#display-data").text("");
            var did = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
            if (did != '') {
                var dataarr = did[0].split(",");

                var val = (dataarr[0]);
            }
            if (did.length == 1)
            {



                $('#reviewModel').modal('show');
                $.ajax({
                    url: "<?php echo base_url('index.php/superadmin') ?>/editGetdriver_review",
                    type: "POST",
                    data: {did: val},
                    dataType: 'json',
                    success: function (result)
                    {
                        console.log(result);
                        $('#eReview').val(result[0].review);
                        $('#eRating').val(result[0].star_rating);
                        $('#appointment_id').val(result[0].appointment_id);


                    }
                });


            } else
            {
                //      alert("select atleast one passenger");
                $("#display-data").text(<?php echo json_encode(POPUP_DRIVERREVIEW_ATLEAST); ?>);
            }

        });
        $('#updateReview').click(function () {
            var review = $('#eReview').val();
            var rating = $('#eRating').val();
            var appointment_id = $('#appointment_id').val();






            if (appointment_id == "" || appointment_id == null) {
                $("#clearerror").text("Please enter the group name");
            } else if (rating > 5)
            {
                alert("Please enter the rating below five");
                //$("#clearerror").text("Please enter the rating below five");
            } else {
                $.ajax({
                    url: "<?php echo base_url('index.php/superadmin') ?>/editdriver_review",
                    type: 'POST',
                    data: {
                        did: appointment_id,
                        review: review,
                        rating: rating

                    },
                    dataType: 'JSON',
                    success: function (response)
                    {
                        $('.close').trigger('click');
                        location.reload(true);
                    }
                });
            }
        });



    });

</script>


<script type="text/javascript">
    $(document).ready(function () {


//        alert('<?php // echo $status;        ?>');
        var status = '<?php echo $status; ?>';

        if (status == 1) {
            $('#inactive').show();
            $('#active').hide();
            $("#display-data").text("");
//               $('#big_table').find('td,th').first().remove();

        }


        $('#big_table_processing').show();

        var table = $('#big_table');

        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url() ?>index.php/superadmin/datatable_driverreview/' + status,
//            "bJQueryUI": true,
//            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "order": [[0, "desc"]],
            "oLanguage": {
                "sProcessing": "<img src='http://107.170.66.211/roadyo_live/sadmin/theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {
                //oTable.fnAdjustColumnSizing();
                $('#big_table_processing').hide();
            },
            "drawCallback": function () {
                $('#big_table_processing').hide();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };




        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function () {
            $('#big_table_processing').show();
            table.fnFilter($(this).val());
        });


        $('.whenclicked li').click(function () {
            // alert($(this).attr('id'));\
            if ($(this).attr('id') == 1) {
                $('#inactive').show();
                $('#active').hide();
                $("#display-data").text("");
//                 $('#big_table').find('td:eq(6),th:eq(6)').hide();
            } else if ($(this).attr('id') == 2) {
                $('#active').show();
                $('#inactive').hide();
                $("#display-data").text("");
//             $('#big_table').find('td:eq(6),th:eq(6)').show();


            }
        });

        $('.changeMode').click(function () {

            var table = $('#big_table');
            $('#big_table_processing').show();

            var settings = {
                "autoWidth": false,
                "sDom": "<'table-responsive't><'row'<p i>>",
                "destroy": true,
                "scrollCollapse": true,
                "iDisplayLength": 20,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": $(this).attr('data'),
//                "bJQueryUI": true,
//                "sPaginationType": "full_numbers",
                "iDisplayStart ": 20,
                "oLanguage": {
                    "sProcessing": "<img src='http://107.170.66.211/roadyo_live/sadmin/theme/assets/img/ajax-loader_dark.gif'>"
                },
                "fnInitComplete": function () {
                    //oTable.fnAdjustColumnSizing();
                    $('#big_table_processing').hide();

                },
                "drawCallback": function () {
                    $('#big_table_processing').hide();
                },
                'fnServerData': function (sSource, aoData, fnCallback)
                {
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });
                }
            };

            $('.tabs_active').removeClass('active');

            $(this).parent().addClass('active');



            table.dataTable(settings);

            // search box for table
            $('#search-table').keyup(function () {
                $('#big_table_processing').show();
                table.fnFilter($(this).val());
            });

        });

    });

    function refreshTableOnCityChange() {

        var table = $('#big_table');
        $('#big_table_processing').show();

        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": $(".whenclicked li.active").children('a').attr('data'),
//            "bJQueryUI": true,
//            "sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "oLanguage": {
                "sProcessing": "<img src='http://107.170.66.211/roadyo_live/sadmin/theme/assets/img/ajax-loader_dark.gif'>"
            },
            "fnInitComplete": function () {
                //oTable.fnAdjustColumnSizing();
                $('#big_table_processing').hide();

            },
            "drawCallback": function () {
                $('#big_table_processing').hide();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function () {
            $('#big_table_processing').show();
            table.fnFilter($(this).val());
        });
    }
</script>


<style>
    .exportOptions{
        display: none;
    }
    .header{
        height:60px !important;
    }
    .header h3{
        margin:10px 0 !important;
    }
</style>

<div class="content">
    <div class="jumbotron  no-margin" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;">
                <h3 class="">Page Title</h3>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg">
        <ul class="breadcrumb">
            <li>
                <a href="#" class="active">PROVIDER REVIEW</a>
            </li>

        </ul>
        <div class="container-md-height m-b-20">
            <div class="panel panel-transparent">
                <ul class="nav nav-tabs nav-tabs-simple bg-white whenclicked li" role="tablist" data-init-reponsive-tabs="collapse">
                    <li id= "1" class="tabs_active <?php echo ($status == 1 ? "active" : ""); ?>" style="cursor:pointer">
                        <a  class="changeMode" data="<?php echo base_url(); ?>index.php/superadmin/datatable_driverreview/1"><span><?php echo LIST_ACTIVE; ?></span></a>
                    </li>
                    <li id= "2" class="tabs_active <?php echo ($status == 2 ? "active" : ""); ?>" style="cursor:pointer">
                        <a   class="changeMode" data="<?php echo base_url(); ?>index.php/superadmin/datatable_driverreview/2"><span><?php echo LIST_INACTIVE; ?> </span></a>
                    </li>

                    <div class="pull-right m-t-10"> <button class="btn btn-warning btn-cons" id="inactive"><?php echo BUTTON_INACTIVE; ?></button></div>

                    <div class="pull-right m-t-10"> <button class="btn btn-success btn-cons" id="active"><?php echo BUTTON_ACTIVATE; ?></button></div>
                    <div class="pull-right m-t-10"> <button class="btn btn-info btn-cons" id="edit_review"><?php echo BUTTON_EDIT; ?></button></div>

                </ul>
                <div class="panel-group visible-xs" id="LaCQy-accordion"></div>
                <div class="tab-content">
                    <div class="tab-pane active" id="hlp_txt">
                        <div class="row panel panel-default">
                            <div class="panel-heading">
                                <div class="error-box" id="display-data" style="text-align:center"></div>
                                <div id="big_table_processing" class="dataTables_processing" style=""><img src="<?php echo APP_SERVER_HOST ?>pics/ajax-loader_dark.gif"></div>


                                <div class="searchbtn row clearfix pull-right" >

                                    <div class="pull-right" style="margin-right: 15px;" ><input type="text" id="search-table" class="form-control pull-right" placeholder="<?php echo SEARCH; ?>"> </div>
                                </div>
                                <div class="dltbtn">


                                </div>
                                <br>
                                <div class="panel-body no-padding">
                                    <?php echo $this->table->generate(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 




    <div class="modal fade stick-up" id="confirmmodel" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">

                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>

                    </div>

                </div>
                <br>
                <div class="modal-body">
                    <div class="row">

                        <div class="error-box" id="errorboxdata" style="font-size: large;text-align:center"><?php echo VEHICLEMODEL_DELETE; ?></div>

                    </div>
                </div>

                <br>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="confirmed" ><?php echo BUTTON_YES; ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



    <div class="modal fade stick-up" id="confirmmodels" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">

                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>

                    </div>

                </div>
                <br>
                <div class="modal-body">
                    <div class="row">

                        <div class="error-box" id="errorboxdatas" style="font-size: large;text-align:center"><?php echo VEHICLEMODEL_DELETE; ?></div>

                    </div>
                </div>

                <br>
                <input type="hidden" id="appointment_id">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="confirmeds" ><?php echo BUTTON_YES; ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade stick-up" id="reviewModel" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="modal-header">
                        <div class=" clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                            </button>
                        </div>
                        <h3 id='modalHeading'> <?php echo FIELD_GROUP_EDIT; ?></h3>
                    </div>
                    <br/>
                    <div class="modal-body">

                        <div class="form-group" class="formex">
                            <div class="frmSearch">
                                <label for="fname" class="col-sm-4 control-label"><?php echo DRIVERREVIEW_TABLE_REVIEW ?><span style="color:red;font-size: 18px">*</span></label>
                                <div class="col-sm-8">
                                    <input name="eReview" type="text" id="eReview" class="form-control"/>
                                    <div id="suggesstion-box"></div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="form-group" class="formex">
                            <label for="fname" class="col-sm-4 control-label"><?php echo DRIVERREVIEW_TABLE_RATING; ?></label>
                            <div class="col-sm-8">
                                <input name="eRating" type="text" id="eRating" class="form-control"/>
                                <div id="suggesstion-box"></div>
                            </div>

                        </div>

                        <br>
                        <br>
                        <div class="row">
                            <div class="col-sm-4" ></div>
                            <div class="col-sm-4 error-box" id="addsubcatd"></div>
                            <div class="col-sm-4" >
                                <button type="button" class="btn btn-primary pull-right" id="updateReview" ><?php echo BUTTON_YES; ?></button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
        </button>
    </div>