<?php
date_default_timezone_set('UTC');
$rupee = "$";
//error_reporting(0);
//if ($status == 5) {
//    $vehicle_status = 'New';
//    $new = "active";
//    echo '<style> .searchbtn{float: left;  margin-right: 63px;}.dltbtn{float: right;}</style>';
//} else if ($status == 2) {
//    $vehicle_status = 'Accepted';
//    $accept = "active";
//} else if ($status == 4) {
//    $vehicle_status = 'Rejected';
//    $reject = 'active';
//} else if ($status == 2) {
//    $vehicle_status = 'Free';
//    $free = 'active';
//} else if ($status == 1) {
//
//    $active = 'active';
//}
?>
<link href="<?php echo base_url(); ?>theme/build/css/intlTelInput.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>theme/build/css/demo.css" rel="stylesheet" type="text/css" />

<!--<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">-->
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<style>
    /*    .dataTables_wrapper .dataTables_info{
            padding: 0;
        }*/
    body{font-size: 11px;}

    input, select{
        border: 1px solid #CCC;
        width: 15px; 
    }
    .header{
        height:60px !important;
    }
    .header h3{
        margin:10px 0 !important;
    }
    .ui-autocomplete{
        z-index: 5000;
    }
    #selectedcity,#companyid{
        display: none;
    }
    .btncss{
        display: none;
    }
    th { font-size: 12px; }
    .ui-menu-item{cursor: pointer;background: black;color:white;border-bottom: 1px solid white;width: 200px;}
</style>
<script>

    function isNumberKey(evt)
    {
        $("#mobify").text("");
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 45 || charCode > 57)) {
            //    alert("Only numbers are allowed");
            $("#mobify").text(<?php echo json_encode(LIST_COMPANY_MOBIFY); ?>);
            return false;
        }
        return true;
    }
    function getlatlongByCity()
    {
        var cityid = $("#Cityid option:selected").text();
        var countryid = $("#countryid option:selected").text();
        console.log(cityid);
        console.log(countryid);
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': '' + cityid + ',' + countryid}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                $('#latitude').val(results[0].geometry.location.lat());
                $('#longitude').val(results[0].geometry.location.lng());

//                latitude
//                alert("location : " + results[0].geometry.location.lat() + " " + results[0].geometry.location.lng());
            } else {
                alert("Something got wrong " + status);
            }
        });
    }

    var countryArray = [];
    function LoadCities()
    {
        var country = $("#countryid option:selected").val();


        $.ajax({
            url: "<?php echo base_url('index.php/superadmin') ?>/showAvailableCities",
            type: "POST",
            data: {country: country},
            dataType: 'JSON',
            success: function (response)
            {

//                alert(JSON.stringify(response));
//                $("#selectedcity").html('');
////
//
//                var i = 0;
//                alert(response.length);
//                
//                for (i = 0; i <= 500; i++) {
//
//                    $("#selectedcity").append("<option value=" + response[i].City_Id + ">" + response[i].City_Name + "</option>");
//
//                }
                $("#Cityid").empty();
                $.each(response, function (key, value)
                {
//                    var regex = new RegExp("/^" + $('#search-box').val() + "/");
//                    if (regex.match(value.City_Name)) {
                    var city = {label: value.City_Name, id: value.City_Id};
                    countryArray.push(city);
//                    }
//                    countryArray.push(value.City_Name);
                    $("#Cityid").append("<option value=" + value.City_Name + ">" + value.City_Name + "</option>");
                });

//                $("#search-box").autocomplete({
//                    source: countryArray,
//                    select: function (event, ui) {
//
//                        $('#search-box-hidden').val(ui.item.id);
//
//
//                    }
//                });
            }
        });
    }

    function LoadCitiesbyname(cn, city)
    {
        var country = cn;
        var citys = city;


        $.ajax({
            url: "<?php echo base_url('index.php/superadmin') ?>/showAvailableCities",
            type: "POST",
            data: {country: country},
            dataType: 'JSON',
            success: function (response)
            {

                $("#eCityid").empty();
                $.each(response, function (key, value)
                {
//                    var regex = new RegExp("/^" + $('#search-box').val() + "/");
//                    if (regex.match(value.City_Name)) {
                    var city = {label: value.City_Name, id: value.City_Id};
                    countryArray.push(city);
//                    }
//                    countryArray.push(value.City_Name);
                    if (value.City_Name == citys)
                    {
                        $("#eCityid").append("<option selected value=" + value.City_Name + ">" + value.City_Name + "</option>");

                    } else {
                        $("#eCityid").append("<option value=" + value.City_Name + ">" + value.City_Name + "</option>");
                    }

                });


            }
        });
    }

    function UpdateLoadCities()
    {
        var country = $("#ecountryid option:selected").val();


        $.ajax({
            url: "<?php echo base_url('index.php/superadmin') ?>/showAvailableCities",
            type: "POST",
            data: {country: country},
            dataType: 'JSON',
            success: function (response)
            {

//                alert(JSON.stringify(response));
//                $("#selectedcity").html('');
////
//
//                var i = 0;
//                alert(response.length);
//                
//                for (i = 0; i <= 500; i++) {
//
//                    $("#selectedcity").append("<option value=" + response[i].City_Id + ">" + response[i].City_Name + "</option>");
//
//                }
                $("#eCityid").empty();
                $.each(response, function (key, value)
                {
//                    var regex = new RegExp("/^" + $('#search-box').val() + "/");
//                    if (regex.match(value.City_Name)) {
                    var city = {label: value.City_Name, id: value.City_Id};
                    countryArray.push(city);
//                    }
//                    countryArray.push(value.City_Name);
                    $("#eCityid").append("<option value=" + value.City_Name + ">" + value.City_Name + "</option>");
                });

//                $("#search-box").autocomplete({
//                    source: countryArray,
//                    select: function (event, ui) {
//
//                        $('#search-box-hidden').val(ui.item.id);
//
//
//                    }
//                });
            }
        });
    }
</script>



<script>




    $(document).ready(function () {


    $("#mobile").on("countrychange", function (e, countryData) {
            console.log(countryData.dialCode);
            $("#c_code").val(countryData.dialCode);
        });
        $("#mobile2").on("countrychange", function (e, countryData) {
            console.log(countryData.dialCode);
            $("#u_c_code").val(countryData.dialCode);
        });

        $('.cities').addClass('active');
//        $('.cities').attr('<?php echo base_url(); ?>/theme/icon/cities_on.png"');
        $('.cities_thumb').attr('src', "<?php echo base_url(); ?>/theme/icon/cities_on.png");


        $('#latitudeedit').keypress(function (event) {
            if (event.which < 45
                    || event.which > 59) {
                event.preventDefault();
            } // prevent if not number/dot

            if (event.which == 46
                    && $(this).val().indexOf('.') != -1) {
                event.preventDefault();
            } // prevent if already dot

            if (event.which == 45
                    && $(this).val().indexOf('-') != -1) {
                event.preventDefault();
            } // prevent if already -
        });

        $('#longitudeedit').keypress(function (event) {
            if (event.which < 45
                    || event.which > 59) {
                event.preventDefault();
            } // prevent if not number/dot

            if (event.which == 46
                    && $(this).val().indexOf('.') != -1) {
                event.preventDefault();
            } // prevent if already dot

            if (event.which == 45
                    && $(this).val().indexOf('-') != -1) {
                event.preventDefault();
            } // prevent if already -
        });


        $('#latitude').keypress(function (event) {
            if (event.which < 45
                    || event.which > 59) {
                event.preventDefault();
            } // prevent if not number/dot

            if (event.which == 46
                    && $(this).val().indexOf('.') != -1) {
                event.preventDefault();
            } // prevent if already dot

            if (event.which == 45
                    && $(this).val().indexOf('-') != -1) {
                event.preventDefault();
            } // prevent if already -
        });

        $('#longitude').keypress(function (event) {
            if (event.which < 45
                    || event.which > 59) {
                event.preventDefault();
            } // prevent if not number/dot

            if (event.which == 46
                    && $(this).val().indexOf('.') != -1) {
                event.preventDefault();
            } // prevent if already dot

            if (event.which == 45
                    && $(this).val().indexOf('-') != -1) {
                event.preventDefault();
            } // prevent if already -
        });



        $('.error-box-class').keypress(function () {
            $('.error-box').text('');
        });







        $('#btnStickUpSizeToggler').click(function () {
            $("#display-data").text("");
            var size = $('input[name=stickup_toggler]:checked').val()
            var modalElem = $('#myModal');
            if (size == "mini") {
                $('#modalStickUpSmall').modal('show')
            } else {
                $('#myModal').on('hidden.bs.modal', function () {
    $(this).find('form').trigger('reset');
})
                $('#pwd').val("");
                $('#myModal').modal('show')
                if (size == "default") {
                    modalElem.children('.modal-dialog').removeClass('modal-lg');
                } else if (size == "full") {
                    modalElem.children('.modal-dialog').addClass('modal-lg');
                }
            }
        });



        $('#driverresetpassword').click(function () {
            $("#display-data").text("");
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
            if (val.length == 0) {
                alert("please select any one dispatcher");
                $("#display-data").text(<?php echo json_encode(POPUP_DRIVER_EDIT); ?>);
            } else if (val.length > 1)
            {

                //     alert("please select only one to edit");
                $("#display-data").text(<?php echo json_encode(POPUP_DRIVER_ONLYEDIT); ?>);
            } else
            {
                var size = $('input[name=stickup_toggler]:checked').val()
                var modalElem = $('#myModal1_driverpass');
                if (size == "mini") {
                    $('#modalStickUpSmall').modal('show')
                } else {
                    $('#myModal1_driverpass').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }

            }
        });



        $("#editpass_msg").click(function () {
            $("errorpass").text("");
            var newpass = $(".newpass").val();
            var confirmpass = $(".confirmpass").val();
            var reg = /^\S*(?=\S*[a-zA-Z])(?=\S*[0-9])\S*$/; //^[-]?(?:[.]\d+|\d+(?:[.]\d*)?)$/;



            if (newpass == "" || newpass == null)
            {
//                alert("please enter the new password");
                $("#errorpass_driversmsg").text(<?php echo json_encode(POPUP_PASSENGERS_PASSNEW); ?>);
            } else if (!reg.test(newpass))
            {
//                alert("please enter the password with atleast one chareacter and one letter");

                $("#errorpass_driversmsg").text(<?php echo json_encode(POPUP_PASSENGERS_PASSVALID); ?>);
            } else if (confirmpass == "" || confirmpass == null)
            {
//                alert("please confirm the password");
                $("#errorpass_driversmsg").text(<?php echo json_encode(POPUP_PASSENGERS_PASSCONFIRM); ?>);
            } else if (confirmpass != newpass)
            {
//                alert("please confirm the same password");
                $("#errorpass_driversmsg").text(<?php echo json_encode(POPUP_PASSENGERS_SAMEPASSCONFIRM); ?>);
            } else
            {
                $.ajax({
                    url: "<?php echo base_url('index.php/superadmin') ?>/editdispatcherpassword",
                    type: 'POST',
                    data: {
                        newpass: newpass,
                        val: $('.checkbox:checked').val()
                    },
                    dataType: 'JSON',
                    success: function (response)
                    {
                        if (response.flag != 1) {
                            $(".close").trigger('click');
                            var size = $('input[name=stickup_toggler]:checked').val()
                            var modalElem = $('#confirmmodelss');
                            if (size == "mini")
                            {
                                $('#modalStickUpSmall').modal('show')
                            } else
                            {
                                $('#confirmmodelss').modal('show')
                                if (size == "default") {
                                    modalElem.children('.modal-dialog').removeClass('modal-lg');
                                } else if (size == "full") {
                                    modalElem.children('.modal-dialog').addClass('modal-lg');
                                }
                            }
                            $("#errorboxdatass").text(<?php echo json_encode(POPUP_DRIVERS_NEWPASSWORD); ?>);
                            $(".newpass").val('');
                            $(".confirmpass").val('');
                            $("#confirmedss").hide();

                        } else if (response.flag == 1)
                        {
                            alert("this password already exists. Enter new password");
                            $("#errorboxdatass").text(<?php echo json_encode(POPUP_DRIVERS_NEWPASSWORD_FAILED); ?>);
                        }
                        $("#confirmedss").hide();
//                        location.reload();

                    }

                });
            }

        });


//        $('#btnStickUpSizeTogglerUpdate').click(function () {
//            $("#display-data").text("");
//            var size = $('input[name=stickup_toggler]:checked').val()
//            var modalElem = $('#myModal');
//            if (size == "mini") {
//                $('#modalStickUpSmall').modal('show')
//            } else {
//                $('#myeditModal').modal('show')
//                if (size == "default") {
//                    modalElem.children('.modal-dialog').removeClass('modal-lg');
//                } else if (size == "full") {
//                    modalElem.children('.modal-dialog').addClass('modal-lg');
//                }
//            }
//        });



        $('#editcitypage').click(function () {
            $("#display-data").text("");
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).get();
            if (val.length == 0) {
                //     alert("please select any one city");
                $("#display-data").text(<?php echo json_encode(POPUP_CITY_ANYONE); ?>);
            } else if (val.length > 1) {

                //       alert("please select only one city to edit");
                $("#display-data").text(<?php echo json_encode(POPUP_CITY_ONLYONE); ?>);
            } else {

//                if (confirm("Are you sure to Edit this city"))
//                {

//                    alert('lat:' + $('.checkbox:checked').parent().siblings('td.latitude').text());
//                alert('lat:' + $('.checkbox:checked').closest('tr').find('.latitide').text());

                var size = $('input[name=stickup_toggler]:checked').val();
                var modalElem = $('#myanotherModal');
//                $('#latitudeedit').val($('.checkbox:checked').closest('tr').find('.latitide').text());
//                $('#longitudeedit').val($('.checkbox:checked').closest('tr').find('.longitude').text());
                $('#dename').val($('.checkbox:checked').parent().prev().prev().prev().prev().text());
                $('#deemail').val($('.checkbox:checked').parent().prev().prev().text());
                $('#mobile2').val($('.checkbox:checked').parent().prev().text());


                if (size == "mini") {

                    $('#modalStickUpSmall').modal('show')

                } else
                {
                    $('#myanotherModal').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }
//                }

            }
        });





        $("#insert").click(function () {

            $("#addcity").text("");

                var coun = $("#countryid").val();
                var city = $("#Cityid").val();
                var dname = $("#dname").val();
            var demail = $("#demail").val();
            var pwd = $("#pwd").val();
            var phone = $("#mobile").val();
            var ccode = $("#c_code").val();
            var reg = /[0-9]+[.[0-9]+]?/;
            var pass = /^(?=.*\d)(?=.*[a-zA-Z])(?!.*[\W_\x7B-\xFF]).{6,15}$/;
            //^[-]?(?:[.]\d+|\d+(?:[.]\d*)?)$/;
            var vemail = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/; //^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

            if (coun == "0") {
//                alert("please select country");
                $("#addcity").text("Please Select Country");
            } else if (city == "" || city == "0") {
                $("#addcity").text(<?php echo json_encode(POPUP_SELECT_CITY); ?>);
            } else if (dname == "" || dname == null)
            {
//                alert("please enter the latitude");
                $("#addcity").text('Enter Name');
            } else if (demail == "" || demail == null)
            {
//                alert("please enter the longitude");
                $("#addcity").text('Enter Email');
            } else if (!(vemail.test(demail)))
            {
                $("#addcity").text('Enter Valid Email');

            } else if (pwd == "" || pwd == null)
            {
//                alert("please enter the longitude");
                $("#addcity").text('Enter Password');
            }else if (!(pass.test(pwd)))
            {
                $("#addcity").text(<?php echo json_encode(POPUP_DRIVER_DRIVER_PASSWORD_VALID); ?>);

            } else if (phone == "" || phone == null)
            {
//                alert("please enter the longitude");
                $("#addcity").text('Enter Phone Number');
            } else

            {
                $.ajax({
                    url: "<?php echo base_url('index.php/superadmin') ?>/insertDispatcher",
                    type: 'POST',
                    data: {
                        country: coun,
                        ccode: ccode,
                        city: city,
                        dname: dname,
                        demail: demail,
                        pwd: pwd,
                        phone: phone
                    },
                    dataType: 'JSON',
                    success: function (response)
                    {
                        console.log('sdsd');

                        $(".close").trigger("click");
//                        

                        var size = $('input[name=stickup_toggler]:checked').val()
                        var modalElem = $('#confirmmodels');
                        if (size == "mini")
                        {
                            $('#mobile2').val("");
                            $('#modalStickUpSmall').modal('show');

                        } else
                        {
                            
                            $('#mobile2').val("");
                            $('#confirmmodels').modal('show')

                            if (size == "default") {
                                modalElem.children('.modal-dialog').removeClass('modal-lg');
                            } else if (size == "full") {
                                modalElem.children('.modal-dialog').addClass('modal-lg');
                            }
                        }

                        $("#confirmeds").hide();

                        if (response.flag == '0') {
                            $('#errorboxdatas').text(response.msg);
                            $("#countryid").val("");
                            $("#search-box-hidden").val("");
                            $("#latitude").val("");
                            $("#longitude").val("");

                        } else if (response.flag == '1') {
                            $('#errorboxdatas').text(response.msg);
//                            $("#errorboxdatas").text(<?php echo json_encode(POPUP_LAT_LONG_ADDED); ?>);

//                            $(".close").trigger("click");
                        }
                        //     alert(response.msg);

                        var table = $('#big_table');


                        var settings = {
                            "autoWidth": false,
                            "sDom": "<'table-responsive't><'row'<p i>>",
                            "destroy": true,
                            "scrollCollapse": true,
                            "iDisplayLength": 20,
                            "bProcessing": true,
                            "bServerSide": true,
                            "sAjaxSource": "<?php echo base_url() ?>index.php/superadmin/datatable_dispatchers",
//                            "bJQueryUI": true,
//                            "sPaginationType": "full_numbers",
//                            "iDisplayStart ": 20,
                            "oLanguage": {
                                "sProcessing": "<img src='<?php echo APP_SERVER_HOST ?>pics/ajax-loader_dark.gif'>"
                            },
                            "fnInitComplete": function () {
                                //oTable.fnAdjustColumnSizing();

                            },
                            "drawCallback": function () {
                                $('#big_table_processing').hide();
                            },
                            'fnServerData': function (sSource, aoData, fnCallback)
                            {
                                $.ajax
                                        ({
                                            'dataType': 'json',
                                            'type': 'POST',
                                            'url': sSource,
                                            'data': aoData,
                                            'success': fnCallback
                                        });
                            }
                        };

                        table.dataTable(settings);

                        // search box for table
                        $('#search-table').keyup(function () {
                            table.fnFilter($(this).val());
                        });

                    }

                });
            }

        });



        $("#editdis").click(function () {
            $("#longlat").text("");
            var val = $('.checkbox:checked').val();

//        var table = $('#big_table').DataTable();




            var dename = $("#dename").val();
            var demail = $("#deemail").val();
            var uc_code = $("#u_c_code").val();
            var mobile2 = $("#mobile2").val();
            var coun = $("#ecountryid").val();
            var city = $("#eCityid").val();
            var reg = /[0-9]+[.[0-9]+]?/;


            if (dename == "" || dename == null)
            {
//                alert("please enter the latitude");
                $("#editdiserr").text('Enter Name');
            } else if (demail == "" || demail == null)
            {
//                alert("please enter the longitude");
                $("#editdiserr").text('Ener Email');
            } else if (mobile2 == "" || mobile2 == null)
            {
//                alert("please enter the longitude");
                $("#editdiserr").text('Ener Mobile Number');
            } else
            {


                $.ajax({
                    url: "<?php echo base_url('index.php/superadmin') ?>/editdispatcherdata",
                    type: 'POST',
                    data: {
                        val: val,
                        coun: coun,
                        city: city,
                        dename: dename,
                        demail: demail,
                        uc_code: uc_code,
                        mobile2: mobile2
                    },
                    dataType: 'JSON',
                    success: function (response)
                    {
                        //  alert("completed");

                        $(".close").trigger("click");
//                        

                        var size = $('input[name=stickup_toggler]:checked').val()
                        var modalElem = $('#confirmmodels');
                        if (size == "mini")
                        {
                            $('#mobile2').val("");
                            $('#modalStickUpSmall').modal('show')
                        } else
                        {
                            $('#mobile2').val("");
                            $('#confirmmodels').modal('show')
                            if (size == "default") {
                                modalElem.children('.modal-dialog').removeClass('modal-lg');
                            } else if (size == "full") {
                                modalElem.children('.modal-dialog').addClass('modal-lg');
                            }
                        }

                        $("#errorboxdatas").text(<?php echo json_encode(POPUP_LAT_LONG_UPDATED); ?>);
                        $("#confirmeds").hide();
//                        $("#errorboxdatas").text(<?php echo json_encode(POPUP_LAT_LONG_UPDATED); ?>);
//                         $("#confirmeds").hide();



                    }

                });
            }



        });
        $('.changeMode').click(function () {

            var table = $('#big_table');
            $('#big_table_processing').show();

            var settings = {
                "autoWidth": false,
                "sDom": "<'table-responsive't><'row'<p i>>",
                "destroy": true,
                "scrollCollapse": true,
                "iDisplayLength": 20,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "<?php echo base_url() ?>index.php/superadmin/datatable_dispatchers",
//                "bJQueryUI": true,
//                "sPaginationType": "full_numbers",
//                "iDisplayStart ": 20,
                "oLanguage": {
                    "sProcessing": "<img src='<?php echo APP_SERVER_HOST ?>pics/ajax-loader_dark.gif'>"
                },
                "drawCallback": function () {
                    $('#big_table_processing').hide();
                },
                "fnInitComplete": function () {
                    //oTable.fnAdjustColumnSizing();

                },
                'fnServerData': function (sSource, aoData, fnCallback)
                {
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });
                },
            };

//            $('.tabs_active').removeClass('active');
//
//            $(this).parent().addClass('active');



            table.dataTable(settings);

            // search box for table
            $('#search-table').keyup(function () {
                $('#big_table_processing').show();
                table.fnFilter($(this).val());
            });

        });

        $("#editdispage").click(function () {
            var val = $('.checkbox:checked').val();
            if (val == '' || val == null)
            {
                alert('plese select Dispatcher');
            } else {

                $.post("<?php echo base_url() ?>index.php/superadmin/getdispatcherdetail", {
                    val: val
                },
                        function (data, status) {
                            // PARSE json data
                            var user = JSON.parse(data);
                            // Assing existing values to the modal popup fields

                            //$("#hide_code").val(user.ccode);
                            console.log(user);
                            var ccode = user.phoncode;
                            var phone = user.phonenum;
                            var phones = ccode + phone;
                            console.log(phones);
                            $("#mobile2").intlTelInput("setNumber", phones);
                            $("#hidecode").val(user.phoncode);
                            $("#dename").val(user.dpname);
                            $("#deemail").val(user.demail);
                            $("#ecountryid").val(user.ccode);
                            LoadCitiesbyname(user.ccode, user.city);
//                             $("#eCityid").val(user.city);
//                             console.log(user.city);
//                            $("#ecountryid").append("<option value=" + user.ccode + " selected>" + user.cname + "</option>");
//                            $("#eCityid").append("<option value=" + user.city + " selected>" + user.city + "</option>");
                        }
                );
                // Open modal popup
                $("#myanotherModal").modal("show");
            }
        });






        $("#chekdel").click(function () {
            $("#display-data").text("");
            var val = $('.checkbox:checked').map(function () {
                return this.value;
            }).
                    get();
            if (val.length > 0)
            {
                //  if (confirm("Are you sure to Delete " + val.length + " cities")) {
                var size = $('input[name=stickup_toggler]:checked').val()

                var modalElem = $('#confirmmodel');
                if (size == "mini") {

                    $('#modalStickUpSmall').modal('show')
                } else {

                    $('#confirmmodel').modal('show')
                    if (size == "default") {
                        modalElem.children('.modal-dialog').removeClass('modal-lg');
                    } else if (size == "full") {
                        modalElem.children('.modal-dialog').addClass('modal-lg');
                    }
                }
                $("#errorboxdata").text("Are You Sure Want to Delete Dispatcher");

                $("#confirmed").click(function () {

                    $.ajax({
                        url: "<?php echo base_url('index.php/superadmin') ?>/deletedispatcher",
                        type: "POST",
                        data: {val: val},
                        dataType: 'json',
                        success: function (result)
                        {

                            $(".close").trigger("click");

                            $('.checkbox:checked').each(function (i) {
                                $(this).closest('tr').remove();
                            });


                            var size = $('input[name=stickup_toggler]:checked').val()
                            var modalElem = $('#confirmmodels');
                            if (size == "mini")
                            {
                                $('#modalStickUpSmall').modal('show')
                            } else
                            {
                                $('#confirmmodels').modal('show')
                                if (size == "default") {
                                    modalElem.children('.modal-dialog').removeClass('modal-lg');
                                } else if (size == "full") {
                                    modalElem.children('.modal-dialog').addClass('modal-lg');
                                }
                            }
                            if (result.flag == '1') {
                                $("#errorboxdatas").text(result.msg);

                            } else if (result.flag == '0') {
                                $("#errorboxdatas").text(result.msg);
                            }
                            $("#confirmeds").hide();
                        }
                    });
                });

            } else
            {
                alert("select atleast one dispatcher");
                $("#display-data").text(<?php echo json_encode(POPUP_CITY_ATLEAST); ?>);
            }

        });


    });










</script>

<script type="text/javascript">
    $(document).ready(function () {

        var table = $('#big_table');

        $('#big_table_processing').show();
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url() ?>index.php/superadmin/datatable_dispatchers',
//            "bJQueryUI": true,
//            "sPaginationType": "full_numbers",
//            "iDisplayStart ": 20,
            "oLanguage": {
                "sProcessing": "<img src='<?php echo APP_SERVER_HOST ?>ajax-loader_dark.gif'>"
            },
            "drawCallback": function () {
                $('#big_table_processing').hide();
            },
            "fnInitComplete": function () {
                //oTable.fnAdjustColumnSizing();
                $('#big_table_processing').hide();
            },
            'fnServerData': function (sSource, aoData, fnCallback)
            {
                $.ajax
                        ({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
            }
        };



        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function () {
            $('#big_table_processing').show();
            table.fnFilter($(this).val());
        });

    });
</script>

<style>
    .exportOptions{
        display: none;
    }
</style>
<div class="content">
    <div class="jumbotron  no-margin" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner" style="transform: translateY(0px); opacity: 1;">

            </div>
        </div>
    </div>
    <div class="container-fluid container-fixed-lg">
        <ul class="breadcrumb" style="margin-top: 30px;">
            <li>
                <a href="#" class="active">DISPATCHERS</a>
            </li>
        </ul>
        <div class="container-md-height m-b-20">
            <div class="panel panel-transparent">
                <ul class="nav nav-tabs nav-tabs-simple bg-white" role="tablist" data-init-reponsive-tabs="collapse">
                    <!--                    <li class="active">
                                            <a href="#" data-toggle="tab" role="tab" aria-expanded="false">List of Cities</a>
                                        </li>-->
                    <div class="pull-right m-t-10">
                        <button class="btn btn-primary btn-cons btn-botom-margin" id="driverresetpassword">Reset Password</button>
                    </div>
                    <div class="pull-right m-t-10">
                        <button class="btn btn-primary btn-cons btn-botom-margin" id="btnStickUpSizeToggler">Add</button>
                    </div>
                </ul>
                <div class="panel-group visible-xs" id="LaCQy-accordion"></div>
                <div class="tab-content">
                    <div class="tab-pane active" id="hlp_txt">
                        <div class="row panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title"></div>
                                <div id="big_table_processing" class="dataTables_processing" style=""><img src="<?php echo APP_SERVER_HOST ?>pics/ajax-loader_dark.gif"></div>
                                <div class='pull-right cls111'>
                                    <div class="pull-right m-t-10 btn-group" >
                                        <button type="button" class="btn btn-danger" id="chekdel" style="height: 37px;"><i class="fa fa-trash-o"></i>
                                        </button>
                                    </div>
                                    <div class="pull-right m-t-10 btn-group">
                                        <button type="button" class="btn btn-success" id="editdispage" style="margin-right: 2px;height: 37px;">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                    </div>
                                    <div class="pull-right m-t-10 btn-group ">
                                        <input type="text" id="search-table" class="form-control pull-right" style="margin-right: 10px;"  placeholder="<?php echo SEARCH; ?>"/> 
                                    </div>
                                </div>
                                <br>
                                <div class="panel-body no-padding">
                                    <?php echo $this->table->generate(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>













    <div class="modal fade stick-up" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">


                    
                <div class="modal-header">

                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>

                        <h3 align="center" style="font-size: 12px; font-weight:bold;color: #0090d9; ">INSERT DISPATCHER</h3>
                    </div>

                </div>

                <div class="modal-body">
                        
                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-4 control-label" >COUNTRY<span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">

                            <select id="countryid" name="country_select"  class="form-control error-box-class" onchange="LoadCities()">
                                <option value="0">Select Country</option>
                                <?php
                                foreach ($country as $result) {

                                    echo "<option value=" . $result->Country_Id . ">" . $result->Country_Name . "</option>";
                                }
                                ?>

                            </select>
                        </div>
                    </div>

                    <br>
                    <br>
                    <div class="form-group" class="formex">
                        <div class="frmSearch">
                            <label for="fname" class="col-sm-4 control-label">CITY<span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-6">

                                <select id="Cityid" name="Cityid" class="form-control error-box-class">
                                    <option value="0">Select City</option>


                                </select>
                            </div>
                            <!--                                <div class="col-sm-8">
                                                                <input type="text" id="search-box"  placeholder="City Name" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>
                                                                <input type="hidden" id="search-box-hidden" class="form-control error-box-class" />
                                                                <div id="suggesstion-box"></div>
                                                            </div>-->
                        </div>
                    </div>
                    <!--                    <div class="form-group" class="formex">
                                            <label for="fname" class="col-sm-4 control-label">Select city</label>
                                            <div class="col-sm-6">
                    
                                                <select id="selectedcity" name="city_select"  class="form-control" >
                    
                                                </select>
                                            </div>
                                        </div>-->

                    <br>
                    <br>

                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-4 control-label"> NAME<span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <input type="text"  id="dname" name="dname"  class="form-control error-box-class" placeholder="Please enter name">
                        </div>
                    </div>

                    <br>
                    <br>

                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-4 control-label"> EMAIL<span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <input type="email"  id="demail" name="demail" class="form-control error-box-class" placeholder="Please enter emailid">
                        </div>
                    </div>

                    <br>
                    <br>

                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-4 control-label"> PASSWORD<span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <input type="password"  id="pwd" name="pwd"  class="form-control error-box-class" placeholder="Please enter name">
                        </div>
                    </div>
                    <br>
                    <br>

                    <input type="hidden" id="c_code" name="c_code">

                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-4 control-label"><?php echo FIELD_DRIVERS_MOBILE; ?></label>
                        <div class="col-sm-6">
                            <input type="tel"  id="mobile" name="mobile" required="required"class="form-control" onkeypress="return isNumberKey(event)">
                        </div>
                    </div>


                    <div class="row">

                        <div class="text-center error-box" id="addcity"></div>

                    </div>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-sm-12 " >
                            <button type="button" class="btn btn-primary pull-right" id="insert" style="margin-left: 5px"><?php echo BUTTON_ADD; ?></button>
                            <button type="button" data-dismiss="modal" class="btn btn-default btn-consc pull-right" id="cancel">Cancel</button>

                        </div>
                    </div>
                </div>

                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
        </button>
    </div>



    <div class="modal fade stick-up" id="myModal1_driverpass" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">

                    <div class="modal-header">

                        <div class=" clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                            </button>

                        </div>
                        <h3> <?php echo LIST_RESETPASSWORD_HEAD; ?></h3>
                    </div>


                    <br>
                    <br>

                    <div class="modal-body">




                        <div class="form-group" class="formex">
                            <label for="fname" class="col-sm-4 control-label"> <?php echo FIELD_NEWPASSWORD; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-6">
                                <input type="password"  id="newpass" name="latitude"  class="newpass form-control" placeholder="eg:g3Ehadd">
                            </div>
                        </div>

                        <br>
                        <br>

                        <div class="form-group" class="formex">
                            <label for="fname" class="col-sm-4 control-label"><?php echo FIELD_CONFIRMPASWORD; ?><span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-6">
                                <input type="password"  id="confirmpass" name="longitude" class="confirmpass form-control" placeholder="H3dgsk">
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-4" ></div>
                            <div class="col-sm-4 error-box" id="errorpass_driversmsg"></div>
                            <div class="col-sm-4" >
                                <button type="button" class="btn btn-primary pull-right" id="editpass_msg" ><?php echo BUTTON_SUBMIT; ?></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

    </div>





    <div class="modal fade stick-up" id="myanotherModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">

                    <div class="modal-header">

                        <div class=" clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                            </button>

                        </div>
                        <h3> Edit Dispatcher Detail </h3>
                    </div>
                    <br>
                    <br>
                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-4 control-label" >COUNTRY<span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <input type="hidden" id="hidecode" name="hidecode">
                            <select id="ecountryid" name="country_select"  class="form-control error-box-class" onchange="UpdateLoadCities()">
                                <option value="0">Select Country</option>
                                <?php
                                foreach ($country as $result) {

                                    echo "<option value=" . $result->Country_Id . ">" . $result->Country_Name . "</option>";
                                }
                                ?>

                            </select>
                        </div>
                    </div>

                    <br>
                    <br>
                    <div class="form-group" class="formex">
                        <div class="frmSearch">
                            <label for="fname" class="col-sm-4 control-label">CITY<span style="color:red;font-size: 18px">*</span></label>
                            <div class="col-sm-6">

                                <select id="eCityid" name="Cityid" class="form-control error-box-class">
                                    <option value="0">Select City</option>


                                </select>
                            </div>
                            <!--                                <div class="col-sm-8">
                                                                <input type="text" id="search-box"  placeholder="City Name" style="  width: 219px;line-height: 2;" class="form-control error-box-class"/>
                                                                <input type="hidden" id="search-box-hidden" class="form-control error-box-class" />
                                                                <div id="suggesstion-box"></div>
                                                            </div>-->
                        </div>
                    </div>
                    <!--                    <div class="form-group" class="formex">
                                            <label for="fname" class="col-sm-4 control-label">Select city</label>
                                            <div class="col-sm-6">
                    
                                                <select id="selectedcity" name="city_select"  class="form-control" >
                    
                                                </select>
                                            </div>
                                        </div>-->

                    <br>
                    <br>

                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-4 control-label"> NAME<span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <input type="text"  id="dename" name="dename"  class="form-control error-box-class" placeholder="Please enter name">
                        </div>
                    </div>

                    <br>
                    <br>

                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-4 control-label"> EMAIL<span style="color:red;font-size: 18px">*</span></label>
                        <div class="col-sm-6">
                            <input type="email"  id="deemail" name="deemail" class="form-control error-box-class" placeholder="Please enter emailid">
                        </div>
                    </div>

                    <br>
                    <br>


                    <input type="hidden" id="u_c_code" name="c_code">
                    <div class="form-group" class="formex">
                        <label for="fname" class="col-sm-4 control-label"><?php echo FIELD_DRIVERS_MOBILE; ?></label>
                        <div class="col-sm-6">
                            <input type="tel"  id="mobile2" name="mobile2" required="required"class="form-control" onkeypress="return isNumberKey(event)">
                        </div>
                    </div>


                    <div class="row">



                        <div class="text-center error-box" id="editdiserr"></div>


                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4 error-box" id="longlat" ></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right changeMode" id="editdis" ><?php echo BUTTON_SUBMIT ?></button>
                        </div>
                    </div>
                </div>
            </div>


            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



    <div class="modal fade stick-up" id="confirmmodel" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">

                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>

                    </div>

                </div>
                <br>
                <div class="modal-body">
                    <div class="row">

                        <div class="error-box" id="errorboxdata" style="font-size: large;text-align:center"><?php echo COMPAIGNS_DISPLAY; ?></div>

                    </div>
                </div>

                <br>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="confirmed" ><?php echo BUTTON_YES; ?></button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



    <div class="modal fade stick-up" id="confirmmodels" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">

                    <div class=" clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>

                    </div>

                </div>
                <br>
                <div class="modal-body">
                    <div class="row">

                        <div class="error-box" id="errorboxdatas" style="font-size: large;text-align:center"><?php echo VEHICLEMODEL_DELETE; ?></div>

                    </div>
                </div>

                <br>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-4" ></div>
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary pull-right" id="confirmeds" ><?php echo BUTTON_YES; ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>





    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGb3Eqs1luybPwJKIBb-jNRRI0EGTehtc&libraries=places"
    async defer></script>

    <script src="<?php echo base_url(); ?>theme/build/js/intlTelInput.js"></script>
    <script>
                                var countryData = $.fn.intlTelInput.getCountryData();
                                $.each(countryData, function (i, country) {
                                    country.name = country.name.replace(/.+\((.+)\)/, "$1");
                                });
                                $("#mobile").intlTelInput({
                                    // allowDropdown: false,
                                    // autoHideDialCode: false,
                                    // autoPlaceholder: "off",
                                    // dropdownContainer: "body",
                                    // excludeCountries: ["us"],
                                    // formatOnDisplay: false,
                                    geoIpLookup: function (callback) {
                                        $.get("http://ipinfo.io", function () {}, "jsonp").always(function (resp) {
                                            var countryCode = (resp && resp.country) ? resp.country : "";
                                            callback(countryCode);
                                        });
                                    },
                                    // initialCountry: "auto",
                                    // nationalMode: false,
                                    // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
                                    // placeholderNumberType: "MOBILE",
                                    // preferredCountries: ['cn', 'jp'],
                                    separateDialCode: true,
                                    utilsScript: "<?php echo base_url(); ?>theme/build/js/utils.js",
                                });

                                $("#mobile2").intlTelInput({
                                    // allowDropdown: false,
                                    // autoHideDialCode: false,
                                    // autoPlaceholder: "off",
                                    // dropdownContainer: "body",
                                    // excludeCountries: ["us"],
                                    // formatOnDisplay: false,
                                    geoIpLookup: function (callback) {
                                        $.get("http://ipinfo.io", function () {}, "jsonp").always(function (resp) {
                                            var countryCode = (resp && resp.country) ? resp.country : "";
                                            callback(countryCode);
                                        });
                                    },
                                    // initialCountry: "auto",
                                    // nationalMode: false,
                                    // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
                                    // placeholderNumberType: "MOBILE",
                                    // preferredCountries: ['cn', 'jp'],
                                    separateDialCode: true,
                                    utilsScript: "<?php echo base_url(); ?>theme/build/js/utils.js",
                                });






    </script>