<style>

    .div-table{
        display:table;         
        width:auto;         
        background-color:#eee;         
        border:1px solid  #666666;         
        border-spacing:5px;/*cellspacing:poor IE support for  this*/
    }
    .div-table-row{
        display:table-row;
        width:auto;
        clear:both;
    }
    .div-table-col{
        float:left;/*fix for  buggy browsers*/
        display:table-column;         
        width:200px;         
        background-color:#ccc;  
    }

    .select2-results {
        max-height: 192px;
    }
    td.details-control {
        background: url('<?php echo base_url() ?>theme/pages/img/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.details td.details-control {
        background: url('<?php echo base_url() ?>theme/pages/img/details_close.png') no-repeat center center;
    }
</style>
<div id="message"></div>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#big_table');
        var settings = {
            "autoWidth": false,
            "sDom": "<'table-responsive't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "iDisplayLength": 20,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url(); ?>index.php/superadmin/datatable_pro_details/<?php echo $status ?>',
                        "iDisplayStart ": 20,
                        "oLanguage": {
                            "sProcessing": "<img src='<?php echo base_url(); ?>theme/assets/img/ajax-loader_dark.gif'>"
                        },
                        "fnInitComplete": function () {
                        },
                        'fnServerData': function (sSource, aoData, fnCallback)
                        {
                            $.ajax
                                    ({
                                        'dataType': 'json',
                                        'type': 'POST',
                                        'url': sSource,
                                        'data': aoData,
                                        'success': fnCallback
                                    });
                        }
                    };
                    table.dataTable(settings);
                    $('#search-table').keyup(function () {
                        table.fnFilter($(this).val());
                    });
                    var detailRows = [];
                    table.on('draw', function () {
                        $.each(detailRows, function (i, id) {
                            $('#' + id + ' td.details-control').trigger('click');
                        });
                    });
                });
</script>
<style>
    .exportOptions{
        display: none;
    }
</style>
<div class="page-content-wrapper"style="padding-top: 20px">
    <div class="content">
        <div class="content"style="padding-top: 3px">
              <div class="inner">
                
                 <ul class="breadcrumb" style="margin-left: 20px;">
                    <li><a href="<?php echo base_url(); ?>index.php/superadmin/Drivers/my/1" class="">PROVIDER</a>
                    </li>
                    
                    <li><a href="" class="active"><?= $name ?></a>
                    </li>
                </ul>

             
                       
              
            </div>
 
            <div class="jumbotron" data-pages="parallax">
                <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                    <div class="panel panel-transparent ">
                        
                         <ul class="nav nav-tabs nav-tabs-simple bg-white" role="tablist" data-init-reponsive-tabs="collapse">
                        <li class="active">
                            <a href="#" data-toggle="tab" role="tab" aria-expanded="false">DEVICES DETAILS</a>
                        </li>    
                        </ul>
                        <div class="tab-content">
                            <div class="container-fluid container-fixed-lg bg-white no-padding">
                                <div class="panel panel-transparent">
                                    <div class="panel-heading">                                  
                                        <div class="row clearfix pull-right" >
                                            <div class="col-sm-12">
                                                <div class="searchbtn" >
                                                    <div class="pull-right"><input type="text" id="search-table" class="form-control pull-right" placeholder="Search"> </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <div id="tableWithSearch_wrapper" class="dataTables_wrapper form-inline no-footer">
                                            <div class="table-responsive">
                                                <?php
                                                $this->table->function = 'htmlspecialchars';
                                                echo $this->table->generate();
                                                ?>
                                            </div><div class="row"></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid container-fixed-lg">
        </div>
    </div>
</div>