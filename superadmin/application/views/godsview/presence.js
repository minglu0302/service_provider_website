var map;
var placelatlng;
var searchArea, searchAreaMarker, searchAreaRadius = 50000; // metres
var geocoder;
var navflag = 1;
var searchflag = 0;
var city = '', vehicleflag = '';
var infowindow;
var baseurl = 'http://iserve.ind.in/';
var godsviewurl = 'http://iserve.ind.in/superadmin/application/views/godsview/';
var serverurl = 'http://iserve.ind.in:9999/';

$('.driverlist').html(' &nbsp; &nbsp; loading..');

var car = "M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z";
var icon1 = {
    path: car,
    scale: .7,
    strokeColor: 'white',
    strokeWeight: .10,
    fillOpacity: 1,
    fillColor: '#9F0812',
    offset: '5%',
    anchor: new google.maps.Point(10, 25)
};

var icon2 = {
    path: car,
    scale: .7,
    strokeColor: 'white',
    strokeWeight: .10,
    fillOpacity: 1,
    fillColor: '#065D9D',
    offset: '5%',
    anchor: new google.maps.Point(10, 25) // orig 10,50 back of car, 10,0 front of car, 10,25 center of car
};

var icon3 = {
    path: car,
    scale: .7,
    strokeColor: 'white',
    strokeWeight: .10,
    fillOpacity: 1,
    fillColor: '#B29408',
    offset: '5%',
    anchor: new google.maps.Point(10, 25) // orig 10,50 back of car, 10,0 front of car, 10,25 center of car
};

var icon4 = {
    path: car,
    scale: .7,
    strokeColor: 'white',
    strokeWeight: .10,
    fillOpacity: 1,
    fillColor: '#998686',
    offset: '5%',
    anchor: new google.maps.Point(10, 25) // orig 10,50 back of car, 10,0 front of car, 10,25 center of car
};

var icon5 = {
    path: car,
    scale: .7,
    strokeColor: 'white',
    strokeWeight: .10,
    fillOpacity: 1,
    fillColor: '#4C7E02',
    offset: '5%',
    anchor: new google.maps.Point(10, 25) // orig 10,50 back of car, 10,0 front of car, 10,25 center of car
};

//gmap search for searching location pointing marker
function initAutocomplete() {

    //map size based on window size
    var elem = (document.compatMode === "CSS1Compat") ?
            document.documentElement :
            document.body;

    var height = elem.clientHeight;
    var restSpace = height - 90;

    document.getElementById("body").style.height = height;

    var y = document.getElementsByClassName("restSpace");
    for (var i = 0, len = y.length; i < len; i++)
        y[i].style.height = restSpace + 'px';

    document.getElementById("map").style.height = restSpace + 'px';

    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 13.028807640, lng: 77.5895080566}, // Bradford - UK as center
        zoom: 7,
        streetViewControl: false, //hiding human icon for street view
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false
    });

    //show alert msg to share you location google map
    geocoder = new google.maps.Geocoder();
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            //passing lat lng to finding drivers 50km distance
            findingmarkers(pos);
            placelatlng = pos;
            searchflag = 1;

            map.setCenter(pos);
            var marker = new google.maps.Marker({
                map: map,
            });
            marker.setPosition(pos);

            codeLatLng(pos.lat, pos.lng);
            //after 5 seconds hiding the marker
            setTimeout(function () {
                marker.setVisible(false);
            }, 5000);
        });
    }

    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function () {
        searchBox.setBounds(map.getBounds());
    });

    var markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function () {
        var places = searchBox.getPlaces();
        if (places.length == 0) {
            return;
        }
        // Clear out the old markers.
        markers.forEach(function (marker) {
            marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function (place) {
            var icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };

            //search place lat lng
            placelatlng = {
                'lat': place.geometry.location.lat(),
                'lng': place.geometry.location.lng()
            }
            city = '';

            console.log(placelatlng);
            searchflag = 1;
            //based on location finding drivers 50km distance
            findingmarkers(placelatlng);

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
                map: map,
                icon: godsviewurl + 'ICON/arrow2.png',
                title: place.name,
                animation: google.maps.Animation.DROP,
                position: place.geometry.location
            }));

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });
        map.fitBounds(bounds);
        map.setZoom(14);
    });

}

google.maps.event.addDomListener(window, 'load', initAutocomplete);

//based on lat lan finding name of city when accept share location
function codeLatLng(lat, lng) {
    var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({'latLng': latlng}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            console.log(results)
            console.log(results[0].address_components[3].long_name);
            // alert(results[0].address_components[3].long_name);
            if (results[1]) {
                //formatted address
                alert(results[0].formatted_address)
                //find country name
                for (var i = 0; i < results[0].address_components.length; i++) {
                    for (var b = 0; b < results[0].address_components[i].types.length; b++) {
                        //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                        if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                            //this is the object you are looking for
                            city1 = results[0].address_components[i];
                            break;
                        }
                    }
                }
            } else {
                alert("No results found");
            }
        } else {
            alert("Geocoder failed due to: " + status);
        }
    });
}

//based on place search updating drivers
setInterval(function () {
    if (searchflag == 1) {
        findingmarkers(placelatlng);
    }
}, 7000);

//based on search place lat lng  finding drivers from 50km distance
function findingmarkers(placelatlng) {
    searchArea = new google.maps.Circle({
        strokeColor: '#FF0000',
        strokeOpacity: 0,
        strokeWeight: 0,
        fillColor: '#FF0000',
        fillOpacity: 0,
        map: map,
        center: new google.maps.LatLng(placelatlng.lat, placelatlng.lng),
        radius: searchAreaRadius
    });

    for (var item in markerStore) {
        if (google.maps.geometry.spherical.computeDistanceBetween(markerStore[item].getPosition(), searchArea.getCenter()) <= searchArea.getRadius()) {
            console.log('=> is in searchArea');
            markerStore[item].placeflag = true;
        } else {
            console.log('=> is NOT in searchArea');
            markerStore[item].placeflag = false;
        }
    }
}


function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

//@markerStore to store the markers assigned to the users received from pubnub
var markerStore = {}, no_of_drivers = 0, flag = 12;

/**
 * Connect to socket server
 */
var socket = io.connect('http://iserve.ind.in:9999');
socket.on('connect', function () {
    console.log("Connected to socket server from godsview.");
    /*  event to request all ON THE JOB Drivers */
    socket.emit('get_online_providers', {group: false});
});

/**
 * Recieve all updates from socket server
 * (driver location update, on/off the job)
 */
socket.on("godsview_update", function (data) {
    console.log('godsview_update: ' + JSON.stringify(data.e_id));
    drawMarkers(data);
});

socket.on("godsview_remove", function (data) {
    console.log('godsview_remove: ' + JSON.stringify(data.e_id));
    removeMarker(data.e_id);
});

/*  event to get all the ON THE JOB Drivers */
socket.on("godsview_loader", function (data) {
    /*  clear the marker list */
    markerStore = {}, no_of_drivers = 0;
    /*  If success response then continue ploting the markers */
    if (data.type == 'get_all_driver' && data.err == 0) {
        (data.data).forEach(function (markerData) {
            drawMarkers(markerData);
            no_of_drivers++;
        });
        $('.no_of_online_drivers').html(no_of_drivers + '/' + data.total);
        console.log('no_of_drivers: ' + no_of_drivers);
    }
});


/* create a marker and store it in the markerStore object */
function createMarker(uuid) {
    var marker = new google.maps.Marker({
        map: map
                //icon: iconimg 
    });
    console.log("join id: ", uuid);
}

/* remove the marker from markerStore object as well from the map */
function removeMarker(uuid) {
    console.log("remove id :", uuid);
    if (markerStore[uuid]) {
        markerStore[uuid].setVisible(false);
        delete markerStore[uuid];
        if (navflag == 1) {
            availabledrivers();
        } else {
            busy_driver_list();
        }
    }

}

var position;
/* function to draw markers on the map
 @data is the data received from the pubnub
 @e_id is the email id, which is unique
 @data.t is the timestamp of the particular message received from pubnub
 @flag specifies status
 */
function drawMarkers(data) {
    if (data.lt && data.lg) {
        //Do we have this marker already?
        if (markerStore.hasOwnProperty(data.e_id)) {
//            console.log('in markerStore: ' + JSON.stringify(data.e_id));
            var z = new google.maps.LatLng(markerStore[data.e_id].lt, markerStore[data.e_id].lg);
            position = [z.lat(), z.lng()];
            var re = new google.maps.LatLng(data.lt, data.lg);
            re = [re.lat(), re.lng()];
            transition(re, data.e_id);
            markerStore[data.e_id].a = data.a;
            markerStore[data.e_id].name = data.Fname;
            markerStore[data.e_id].t = data.t;
            markerStore[data.e_id].lt = data.lt;
            markerStore[data.e_id].lg = data.lg;
            markerStore[data.e_id].image = data.ProfilePic;
            markerStore[data.e_id].appendflag = true;
        } else {
//            console.log('not in markerStore: ' + JSON.stringify(data.e_id));
            //placeflag is true means the pariticular user is under the radius
            var proimg = godsviewurl + 'ICON/driver.png';
            if (data.ProfilePic != "") {
                proimg = data.ProfilePic;
            }
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(data.lt, data.lg),
                title: data.Fname,
                map: map,
                proid: data.proid,
                t: data.t,
                name: data.Fname,
                image: proimg,
                phone: data.mobileNO,
                a: data.a,
                lt: data.lt,
                lg: data.lg,
                eid: data.e_id,
                placeflag: false,
                appendflag: false
            });

            marker.addListener('click', function () {
                get_driver_data(data.proid);
                popupmodel();
            });
            markerStore[data.e_id] = marker;
            markerStore[data.e_id].setPosition(new google.maps.LatLng(data.lt, data.lg));
            markerStore[data.e_id].setVisible(true);
            markerStore[data.e_id].appendflag = true;
        }
        showAllMarkers();
    }
}

/* function to hide the markers if data is not received for the particular user from pubnub
 @currentEpoch is the current time of the system in epoch format
 @timeDifference is the time difference in seconds between the system time and the lastUpdated marker time
 */
function offline() {
    setInterval(function () {
        for (var item in markerStore) {
            var currentEpoch = Math.floor((new Date().valueOf()) / 1000);
            var timeDifference = currentEpoch - markerStore[item].t;
            if (timeDifference >= 12) {
                markerStore[item].setVisible(false);
                markerStore[item].placeflag = false;
                clearCircle();
                markerStore[item].appendflag = false;
                if (navflag == 1) {
                    availabledrivers();
                } else {
                    busy_driver_list();
                }
            }
        }
    }, 3000);
}

function statusFour(e_id, bid, lt, lg, a) {
    if (searchflag == 1) {
        console.log('searchflag: ' + searchflag);
        if (markerStore[e_id].placeflag === true) {
            if (Number(a) == 4) {
                if (bid) {
                    if (Number(bid) == 0) {
                        markerStore[e_id].setVisible(true);
                        markerStore[e_id].setIcon(icon5);
                    } else {
                        markerStore[e_id].setVisible(false);
                    }
                } else {
                    markerStore[e_id].setVisible(true);
                    markerStore[e_id].setIcon(icon5);
                }
            } else {
                markerStore[e_id].setVisible(false);
            }
        } else {
            markerStore[e_id].setVisible(false);
        }
    } else {
        console.log('no searchflag: ');
        if (city != '') {
            console.log('city: ' + city);
            if (Number(markerStore[e_id].cityid) == Number(city)) {
                if (Number(a) == 4) {
                    if (bid) {
                        if (Number(bid) == 0) {
                            markerStore[e_id].setVisible(true);
                            markerStore[e_id].setIcon(icon5);
                        } else {
                            markerStore[e_id].setVisible(false);
                        }
                    } else {
                        markerStore[e_id].setVisible(true);
                        markerStore[e_id].setIcon(icon5);
                    }
                } else {
                    markerStore[e_id].setVisible(false);
                }
            } else {
                markerStore[e_id].setVisible(false);
            }
        } else {
            console.log('no city: ');
            if (Number(a) == 4) {
                if (bid) {
                    if (Number(bid) == 0) {
                        markerStore[e_id].setVisible(true);
                        markerStore[e_id].setIcon(icon5);
                    } else {
                        markerStore[e_id].setVisible(false);
                    }
                } else {
                    markerStore[e_id].setVisible(true);
                    markerStore[e_id].setIcon(icon5);
                }
            } else {
                markerStore[e_id].setVisible(false);
            }
        }
    }
}

function statusSix(e_id, bid, lt, lg, a) {
    if (searchflag == 1) {
        if (markerStore[e_id].placeflag === true) {
            if (Number(a) == 6) {
                markerStore[e_id].setVisible(true);
                markerStore[e_id].setIcon(icon1);
            } else {
                markerStore[e_id].setVisible(false);
            }
        } else {
            markerStore[e_id].setVisible(false);
        }
    } else {
        if (city != '') {
            if (Number(markerStore[e_id].cityid) == Number(city)) {
                if (Number(a) == 6) {
                    markerStore[e_id].setVisible(true);
                    markerStore[e_id].setIcon(icon1);
                } else {
                    markerStore[e_id].setVisible(false);
                }
            } else {
                markerStore[e_id].setVisible(false);
            }
        } else {
            if (Number(a) == 6) {
                markerStore[e_id].setVisible(true);
                markerStore[e_id].setIcon(icon1);
            } else {
                markerStore[e_id].setVisible(false);
            }
        }
    }
}

function statusSeven(e_id, bid, lt, lg, a) {
    if (searchflag == 1) {
        if (markerStore[e_id].placeflag === true) {
            if (Number(a) == 7) {
                markerStore[e_id].setVisible(true);
                markerStore[e_id].setIcon(icon2);
            } else {
                markerStore[e_id].setVisible(false);
            }
        } else {
            markerStore[e_id].setVisible(false);
        }
    } else {
        if (city != '') {
            if (Number(markerStore[e_id].cityid) == Number(city)) {
                if (Number(a) == 7) {
                    markerStore[e_id].setVisible(true);
                    markerStore[e_id].setIcon(icon2);
                } else {
                    markerStore[e_id].setVisible(false);
                }
            } else {
                markerStore[e_id].setVisible(false);
            }
        } else {
            if (Number(a) == 7) {
                markerStore[e_id].setVisible(true);
                markerStore[e_id].setIcon(icon2);
            } else {
                markerStore[e_id].setVisible(false);
            }
        }
    }
}

function statusEight(e_id, bid, lt, lg, a) {
    if (searchflag == 1) {
        if (markerStore[e_id].placeflag === true) {
            if (Number(a) == 8) {
                markerStore[e_id].setVisible(true);
                markerStore[e_id].setIcon(icon3);
            } else {
                markerStore[e_id].setVisible(false);
            }
        } else {
            markerStore[e_id].setVisible(false);
        }
    } else {
        if (city != '') {
            if (Number(markerStore[e_id].cityid) == Number(city)) {
                if (Number(a) == 8) {
                    markerStore[e_id].setVisible(true);
                    markerStore[e_id].setIcon(icon3);
                } else {
                    markerStore[e_id].setVisible(false);
                }
            } else {
                markerStore[e_id].setVisible(false);
            }
        } else {
            if (Number(a) == 8) {
                markerStore[e_id].setVisible(true);
                markerStore[e_id].setIcon(icon3);
            } else {
                markerStore[e_id].setVisible(false);
            }
        }
    }
}

function statusNine(e_id, bid, lt, lg, a) {
    if (searchflag == 1) {
        if (markerStore[e_id].placeflag === true) {
            if (Number(a) == 9) {
                markerStore[e_id].setVisible(true);
                markerStore[e_id].setIcon(icon4);
            } else {
                markerStore[e_id].setVisible(false);
            }
        } else {
            markerStore[e_id].setVisible(false);
        }
    } else {
        if (city != '') {
            if (Number(markerStore[e_id].cityid) == Number(city)) {
                if (Number(a) == 9) {
                    markerStore[e_id].setVisible(true);
                    markerStore[e_id].setIcon(icon4);
                } else {
                    markerStore[e_id].setVisible(false);
                }
            } else {
                markerStore[e_id].setVisible(false);
            }
        } else {
            if (Number(a) == 9) {
                markerStore[e_id].setVisible(true);
                markerStore[e_id].setIcon(icon4);
            } else {
                markerStore[e_id].setVisible(false);
            }
        }
    }
}

/* drivers list markers of available(free),enroute to store,arrive at store,arrive at drop, enroute to drop and all
 @ available=4, @enroute to store=6,@arrive at store=7, @arrive at drop=8,@enroute to drop=9 @all=12  
 @flag specifies the status of driver*/
function availableList() {
    flag = 4;
    clearMarkers();
    clearCircle();
}

function enroutetostore() {
    flag = 6;
    clearMarkers();
    clearCircle();
}

function arrivedatstore() {
    flag = 7;
    clearMarkers();
    clearCircle();
}

function arriveatdrop() {
    flag = 9;
    clearMarkers();
    clearCircle();
}

function enroutetodrop() {
    flag = 8;
    clearMarkers();
    clearCircle();
}

function showAllMarkers() {
    flag = 12;
    clearMarkers();
    for (var item in markerStore) {
        switch (Number(markerStore[item].a)) {
            case 6:
                showAllSix(markerStore[item]);
                break;
            case 7:
                showAllSeven(markerStore[item]);
                break;
            case 8:
                showAllEight(markerStore[item]);
                break;
            case 9:
                showAllNine(markerStore[item]);
                break;
            case 4:
                showAllFour(markerStore[item]);
                break;
        }
    }
}

function showAllSix(res) {
    if (searchflag == 1) {
        if (res.visible === true) {
            res.setIcon(icon1);
            res.setVisible(true);
        } else {
            res.setVisible(false);
        }
    } else {
        if (city != '') {
            if (Number(res.cityid) == Number(city)) {
                res.setIcon(icon1);
                res.setVisible(true);
            } else {
                res.setVisible(false);
            }
        } else {
            res.setIcon(icon1);
            res.setVisible(true);
        }
    }
}

function showAllSeven(res) {
    if (searchflag == 1) {
        if (res.placeflag === true) {
            res.setIcon(icon2);
            res.setVisible(true);
        } else {
            res.setVisible(false);
        }
    } else {
        if (city != '') {
            if (Number(res.cityid) == Number(city)) {
                res.setIcon(icon2);
                res.setVisible(true);
            } else {
                res.setVisible(false);
            }
        } else {
            res.setIcon(icon2);
            res.setVisible(true);
        }
    }
}

function showAllEight(res) {
    if (searchflag == 1) {
        if (res.placeflag === true) {
            res.setIcon(icon3);
            res.setVisible(true);
        } else {
            res.setVisible(false);
        }
    } else {
        if (city != '') {
            if (Number(res.cityid) == Number(city)) {
                res.setIcon(icon3);
                res.setVisible(true);
            } else {
                res.setVisible(false);
            }
        } else {
            res.setIcon(icon3);
            res.setVisible(true);
        }
    }
}

function showAllNine(res) {
    if (searchflag == 1) {
        if (res.placeflag === true) {
            res.setIcon(icon4);
            res.setVisible(true);
        } else {
            res.setVisible(false);
        }
    } else {
        if (city != '') {
            if (Number(res.cityid) == Number(city)) {
                res.setIcon(icon4);
                res.setVisible(true);
            } else {
                res.setVisible(false);
            }
        } else {
            res.setIcon(icon4);
            res.setVisible(true);
        }
    }
}

function showAllFour(res) {
    if (searchflag == 1) {
        if (res.placeflag === true) {
            res.setIcon(icon5);
            res.setVisible(true);
        } else {
            res.setVisible(false);
        }
    } else {
        if (city != '') {
            if (Number(res.cityid) == Number(city)) {
                res.setIcon(icon5);
                res.setVisible(true);
            } else {
                res.setVisible(false);
            }
        } else {
            res.setIcon(icon5);
            res.setVisible(true);
        }
    }
}

//clear all markers on the  map
function clearMarkers() {
    for (var item in markerStore) {
        markerStore[item].setVisible(false);
    }
}

var citydata = [];
//@citydata storing all city's object data
// var  circle, infowindow;

//soring ascending order of city name
jQuery.fn.sort = function () {
    return this.pushStack([].sort.apply(this, arguments), []);
};
function sortCityName(a, b) {
    if (a.City_Name.toUpperCase() == b.City_Name.toUpperCase()) {
        return 0;
    }
    return a.City_Name.toUpperCase() > b.City_Name.toUpperCase() ? 1 : -1;
}
;

//getting city's data from mysql 
function getcitydata() {
    console.log('getcitydata');
//    $.getJSON(serverurl + "/citydata", function (response) {
//        response = $(response).sort(sortCityName);
//        citydata = response;
//        for (var i = 0; i < response.length; i++) {
//            $('#city').append('<option value="' + response[i].City_Id + '">' + camelize(response[i].City_Name) + '</option>');
//        }
//    });
}

//converting camelcase
function camelize(inStr) {
    return inStr.replace(/\w\S*/g, function (tStr) {
        return tStr.charAt(0).toUpperCase() + tStr.substr(1).toLowerCase();
    });
}

//while loading page after 4 seconds execute function
setTimeout(function () {
    if (navflag == 1) {
        availabledrivers();
    } else {
        busy_driver_list();
    }
}, 4000);

//every 7 seconds repeating loop
//@navflag 1 means selected available tab
//@navflag 0 means selected busy tab
setInterval(function () {
    if (navflag == 1) {
        availabledrivers();
    } else {
        busy_driver_list();
    }
}, 7000);

//appending drivers data to table
function appenddata(response) {
    var img, status;
    status = Number(response.a);

    //table data appending driver status icon.
    switch (status) {
        case 4:
            img = godsviewurl + 'ICON/CIRCLE3.png';
            break;
        case 6:
            img = godsviewurl + 'ICON/CIRCLE1.png';
            break;
        case 7:
            img = godsviewurl + 'ICON/CIRCLE5.png';
            break;
        case 8:
            img = godsviewurl + 'ICON/CIRCLE4.png';
            break;
        case 9:
            img = godsviewurl + 'ICON/CIRCLE2.png';
            break;
    }
    if (response.appendflag === true) {
        $(".driverlist").append('<tr><td> <div onclick="get_driver_data(\'' + response.proid + '\');popupmodel();"><p> <span class="col-xs-height col-middle"><span class="thumbnail-wrapper d32 circular bg-success"> <img src="' + response.image + '"></span></span></p><p class="p-l-10 col-xs-height col-middle" style="width:80%"><span class="text-master" style="padding-right: 30px;"> &nbsp;<b>' + response.name + '</b>&nbsp;<br/></span></p></div></td></tr>');
    }
}

//right side bar ,its displays driver and jobs details
function popupmodel() {
    $('#myModal2').modal('show');
}

//appending available drivers based on city or vehicle type or place search to table
function availabledrivers() {
    navflag = 1;
    $('.driverlist').html('');
    for (var item in markerStore) {
        if (Number(markerStore[item].a) == 4) {
            if (city != '') {
                if (Number(markerStore[item].cityid) == Number(city)) {
                    appenddata(markerStore[item]);
                }
            } else {
                if (searchflag == 1) {
                    if (markerStore[item].placeflag === true) {
                        appenddata(markerStore[item]);
                    }
                } else {
                    appenddata(markerStore[item]);
                }
            }
        }
    }
}

//busy drivers based on city or vehicle type or radius appending drivers to the table
//@searchflag  for checking while search place from search bar
//@city for if select city from select box enable city and storing city id 
function busy_driver_list() {
    navflag = 0;
    // console.log("busy");
    $('.driverlist').html('');
    for (var item in markerStore) {
        if (markerStore[item].bid) {
            if (Number(markerStore[item].bid) != 0) {
                if (city != '') {
                    if (Number(markerStore[item].cityid) == Number(city)) {
                        appenddata(markerStore[item]);
                    }
                } else {
                    if (searchflag == 1) {
                        if (markerStore[item].placeflag === true) {
                            appenddata(markerStore[item]);
                        }
                    } else {
                        appenddata(markerStore[item]);
                    }
                }
            }
        }
    }
}

var citymarker, cityobj;
//@citymarker storing particular city marker
//@cityobj storing city object based on city id

//based on city id appending available drivers
$('#city').on('change', function () {
    // searchflag = 0;
    clearCircle();
    clearMarkers();
    $('.driverlist').html('');

    var value = $(this).val();
    console.log(value);
    if (value != '') {
        // city = value;

        //based on city lat long focusing map to particular location        
        cityobj = $.grep(citydata, function (item) {
            if (item.City_Id == value) {
                return item;
            }
        });

        if (citymarker != undefined) {
            citymarker.setMap(null);
        }

        var marker = new google.maps.Marker({
            map: map,
            position: new google.maps.LatLng(cityobj[0].City_Lat, cityobj[0].City_Long),
            icon: godsviewurl + 'ICON/arrow2.png',
            animation: google.maps.Animation.DROP,
        });
        citymarker = marker;
        map.setCenter(new google.maps.LatLng(cityobj[0].City_Lat, cityobj[0].City_Long));
        map.setZoom(14);

        placelatlng = {
            lat: cityobj[0].City_Lat,
            lng: cityobj[0].City_Long
        };
        //console.log(placelatlng);
        searchflag = 1;
        findingmarkers(placelatlng);
    } else {
        city = '';
        searchflag = 0;
    }
});

/**
 * Filter on basis of Takeaway Group
 */
$('#takeaway_group').on('change', function () {
    $('.no_of_online_drivers').html('loading..');
    driverDetailMarkers.forEach(function (driDetailMark) {
        driDetailMark.setMap(null);
    });

    clearCircle();
    clearMarkers();
    $('.driverlist').html(' &nbsp; &nbsp; loading..');

    var value = $(this).val();
    console.log(value);
    if (value != '#') {
        socket.emit('get_online_providers', {group: true, group_id: value});
    } else {
        socket.emit('get_online_providers', {group: false});
    }
});


//getting driver data and appointment data based on provider id 
function get_driver_data(proid) {
    clearCircle();
    document.getElementById("driver").innerHTML = "";
    document.getElementById("jobid").innerHTML = "";
    document.getElementById("route").innerHTML = "";
//    document.getElementById("onbookings").innerHTML = "";

    var driver_id = '', driver_mail = '';

    for (var item in markerStore) {
        if (markerStore[item].proid == proid) {
            infowindows(markerStore[item]);
            driver_id = markerStore[item].proid;
            driver_mail = markerStore[item].eid;
            console.log('Clicked On > ' + driver_mail);
            $("#driver").append('<h5 style="padding-left: 10px;color: white;">Provider Details</h5>');
            $("#driver").append('<h5><p style="margin-bottom: 0px;padding: 10px;"><b>Provider ID:</b> &nbsp;' + markerStore[item].proid + '<br><b>Provider Name:</b> &nbsp;' + markerStore[item].name + '<br><b>Email ID:</b> &nbsp;' + driver_mail + '<br><b>Phone No:</b> &nbsp;' + markerStore[item].phone + '</p></h5>');

        }
    }

    if (driver_id != '') {
        document.getElementById("onbookings").innerHTML = "";
        $("#onbookings").append('<h5 style="padding-left: 10px;color: white;">ONGOING JOBS</h5>');

        $.ajax({
            type: "GET",
            url: baseurl + "superadmin/index.php/superadmin/get_provider_jobs/" + driver_id,
            dataType: 'json',
            data: {},
            success: function (result) {
                console.log(result);
                if(result.length == 0){
                    $("#onbookings").append('<br><br><b>There is No ONGOING JOBS</b><br>');
                }
                for (var i = 0; i < result.length; i++) {
                    var status = "";
                    if (result[i].status == 5) {
                        status = "PROVIDER ON THE WAY ARRIVED";
                    } else if (result[i].status == 21) {
                        status = "PROVIDER ARRIVED";
                    } else if (result[i].status == 6) {
                        status = "JOB STARTED";
                    } else if (result[i].status == 22) {
                        status = "JOB COMPLETED INVOICE PENDING";
                    }
                    $("#onbookings").append('<h5><p style="margin-bottom: 0px;padding: 7px;">');
                    $("#onbookings").append('<b>JOB ID:</b> &nbsp;' + result[i].bid + '<br>');
                    $("#onbookings").append('<b>Customer ID:</b> &nbsp;' + result[i].customer.id + '<br>');
                    $("#onbookings").append('<b>Customer Name:</b> &nbsp;' + result[i].customer.fname + '<br>');
                    $("#onbookings").append('<b>Customer Mobile:</b> &nbsp;' + result[i].customer.mobile + '<br>');
                    $("#onbookings").append('<b>Address:</b> &nbsp;' + result[i].address1 + '<br>');
                    $("#onbookings").append('<b>Status:</b> &nbsp;' + status + '<br>');
                    $("#onbookings").append('</p></h5>');
                }
//                $("#onbookings").append('<h5><p style="margin-bottom: 0px;padding: 10px;"><b>Booking ID:</b> &nbsp;' + result.bid + '<br><b>Customer Name:</b> &nbsp;' + result.customer.name + '<br><b> Status :</b> &nbsp;' + result.status + '</p></h5>');
            },
            error: function (err) {
                console.log('Error');
            }
        });
    } else {
        console.log('Proider id is empty');
    }
}


//closing circle and infowindow
function clearCircle() {
    if (infowindow) {
        infowindow.close();
    }
}

var driverDetailMarkers = [];
//infowindow and circle for while click the driver on table list
function infowindows(response) {

    // map.setZoom(14);
    /*  set the map ceter to the driver clicked */
    map.setCenter(new google.maps.LatLng(response.lt, response.lg));
    /*  Set the zoom level for clear display */
    map.setZoom(17);

    /*  remove all the previous plotted arrow markers */
    driverDetailMarkers.forEach(function (driverMarker) {
        driverMarker.setMap(null);
    });

    /*  Add Arrow marker to the Driver clicked */
    driverDetailMarkers.push(new google.maps.Marker({
        map: map,
        icon: godsviewurl + 'ICON/arrow2.png',
        title: '',
        animation: google.maps.Animation.DROP,
        position: {lat: response.lt, lng: response.lg}
    }));

    /*  display the Driver Details popup window */
    infowindow = new google.maps.InfoWindow({
        content: '<p><img src=" ' + response.image + ' " width="34" height="34"/> ' + response.name + ' <br>Provider Id: ' + response.pro + '<br> Phone no:' + response.phone + '<p>'
    });
}

//based on click  adding css class to statusbar enroute,pickup,journey,all 
$(".btn").click(function () {
    $(".btn-default").removeClass("raised");
    $(this).addClass("raised");
});

// to resize the map window based on browser or device size
function changeSize() {
    var elem = (document.compatMode === "CSS1Compat") ?
            document.documentElement :
            document.body;
    var height = elem.clientHeight;
    var restSpace = height - 90;
    document.getElementById("body").style.height = height;
    var y = document.getElementsByClassName("restSpace");
    for (var i = 0, len = y.length; i < len; i++)
        y[i].style.height = restSpace + 'px';
    document.getElementById("map").style.height = restSpace + 'px';
}
window.onresize = changeSize;

//marker moving smoothly  
var x;
var numDeltas = 100;
var delay = 60; //milliseconds
var i = 0;
var deltaLat;
var deltaLng;
function transition(result, ids) {
    x = ids;

    i = 0;
    deltaLat = (result[0] - position[0]) / numDeltas;
    deltaLng = (result[1] - position[1]) / numDeltas;

    moveMarker();
}
function moveMarker() {
    position[0] += deltaLat;
    position[1] += deltaLng;

    var latlng1 = new google.maps.LatLng(position[0], position[1]);
    if (markerStore[x]) {
        markerStore[x].setPosition(latlng1);
    }
    if (i != numDeltas) {
        i++;
        setTimeout(moveMarker, delay);
    }
}