<!DOCTYPE html>
<html>

    <head>
        <title>GodZview</title>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"  rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css"  rel="stylesheet" >
        <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Lato:400,100,300' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,200,300' rel='stylesheet' type='text/css'>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/vis/4.15.0/vis.min.css" rel="stylesheet" type="text/css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href='http://139.59.167.195/JustDelivered/GodsView-JustDelivered/simple.css' rel='stylesheet' type='text/css'>
        <link href='http://139.59.167.195/JustDelivered/GodsView-JustDelivered/default.css' rel='stylesheet' type='text/css'>
    </head>
    <body id="body">
        <div class="conatiner-fluid">
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <h2><?php echo Appname; ?> - Gods View</h2>
                    <ul class="nav navbar-nav navbar-right" style="margin-top:1px;">
                        <li>
                            <select class="selectpicker" id="takeaway_group" style="padding: 5px;">
                                <option value="#">Select City</option>
                                <?php foreach ($city as $ci) { ?>
                                    <option value="<?php echo $ci->City_Id; ?>"><?php echo ucwords($ci->City_Name); ?></option>
                                <?php }
                                ?>
                            </select>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!--        <div id='bs-example'>
                    <nav role="navigation" class="navbar navbar-default">
                         Brand and toggle get grouped for better mobile display 
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="modal" data-target="#myModal" id="left" style='float: left !important;color: black;'>
                                <i class="fa fa-chevron-right" aria-hidden="true"></i>
                            </button>
                            <button type="button" data-target="#collapse1" data-toggle="collapse" class="navbar-toggle">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
        
                            <div id="collapse1" class="panel-collapse collapse">
                                <div class="col-xs-12">
                                    <a href="#" onclick="enroutetostore();">
                                        <h2><button class="btn btn-default" role="button" style="background-color:#9F0812">Enroute to Store</button></h2>
                                    </a>
                                </div>
                                <div class="col-xs-12">
                                    <a href="#" onclick="arrivedatstore();">
                                        <h2><button class="btn btn-default" role="button" style="background-color:#065D9D">Arrived at Store</button></h2>
                                    </a>
                                </div>
                                <div class="col-xs-12">
                                    <a href="#" onclick="enroutetodrop();">
                                        <h2> <button class="btn btn-default" role="button" style="background-color:#B29408">Enroute to drop</button></h2>
                                    </a>
                                </div>
                                <div class="col-xs-12">
                                    <a href="#" onclick="arriveatdrop();">
                                        <h2> <button class="btn btn-default" role="button" style="background-color:rgba(100, 66, 66, 0.6);">Arrive at drop</button></h2>
                                    </a>
                                </div>
                                <div class="col-xs-12">
                                    <a href="#" onclick="availableList();" class="active1">
                                        <h2><button class="btn btn-default " role="button" style="background-color:#4C7E02">Free</button></h2>
                                    </a>
                                </div>
                                <div class="col-xs-12">
                                    <a href="#" onclick="showAllMarkers();">
                                        <h2> <button class="btn btn-default raised" role="button" style="background-color:gray">All</button></h2>
                                    </a>
                                </div>
                            </div>
                        </div>
                         Collection of nav links and other content for toggling 
                        <div id="navbarCollapse" class="collapse navbar-collapse" style="margin-top: 3px;">
                            <ul class="nav navbar-nav">
                                <li>
                                    <a href="#" onclick="enroutetostore();">
                                        <h2><button class="btn btn-default" role="button" style="background-color:#9F0812">Enroute to Store</button></h2>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" onclick="arrivedatstore();">
                                        <h2><button class="btn btn-default" role="button" style="background-color:#065D9D"> Arrived at Store</button></h2>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" onclick="enroutetodrop();">
                                        <h2> <button class="btn btn-default" role="button" style="background-color:#B29408">Enroute to drop</button></h2>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" onclick="arriveatdrop();">
                                        <h2> <button class="btn btn-default" role="button" style="background-color:rgba(100, 66, 66, 0.6);">Arrive at drop</button></h2>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" onclick="availableList();" class="active1">
                                        <h2><button class="btn btn-default " role="button" style="background-color:#4C7E02"> Free</button></h2>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" onclick="showAllMarkers();">
                                        <h2> <button class="btn btn-default raised" role="button" style="background-color:gray">All</button></h2>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>-->

        <!-- Modal -->
        <div class="modal left fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document" style="margin-top: 151px;">
                <div class="modal-content">
                    <div class="modal-body" style="padding: 0px;">
                        <ul class="nav nav-tabs noborder aligncenter" role="tablist">
                            <li role="presentation" class="active widht60percent"><a href="#avilable" onclick="availabledrivers();" aria-controls="available" role="tab" data-toggle="tab">Provider On The Job</a></li>
                            <!--<li role="presentation" class="widht40percent"><a href="#busy" onclick="busy_driver_list();" aria-controls="busy" role="tab" data-toggle="tab" class="styletab2">BUSY</a></li>-->
                        </ul>
                        <table class="table table-hover search-table" id="drivertable">
                            <tbody class="driverlist"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal right fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
            <div class="modal-dialog" role="document" style="height: auto;top: 10vh;">
                <div class="modal-content" style="border-bottom: 10px solid #006873;padding: 0px;">
                    <div class="modal-body" style="padding:0px;">
                        <div id="driver"></div>
                        <div id="jobid"></div>
                        <!--<div id="slave"></div>-->
                        <div id="route" style="height: 0px; overflow: scroll;"></div>
                        <div id="onbookings" style="height: 200px; overflow: scroll;"></div>                       
                    </div>
                </div>
            </div>
        </div>
        <!-- modal -->

        <div class="row restSpace" id="mainContent">
            <div class="col-sm-3 col-md-2 nopadding restSpace" id="drivercontainer">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs noborder aligncenter" role="tablist">
                    <li role="presentation" class="active" style="width: 99%;"><a href="#avilable" aria-controls="available" role="tab" data-toggle="tab" style="align:center; background-color: #4CAF50;">Providers On The Job</a><span class="no_of_online_drivers"> loading..</span></li>
                    <!--<li role="presentation" class="widht40percent"><a href="#busy" onclick="busy_driver_list();" aria-controls="busy" role="tab" data-toggle="tab" class="styletab2">BUSY</a></li>-->
                </ul>
                <table class="table table-hover search-table" id="drivertable">
                    <tbody class="driverlist"></tbody>
                </table>
            </div>
            <div class="col-sm-9 col-md-10 nopadding restSpace">
                <input id="pac-input" class="controls" type="text" placeholder="Search Location" style="width: 15.5em;margin-left: 50px;">
                <div id="map"></div>
            </div>
        </div>
        <div style='position:fixed;bottom:0;height:3vh;width:100vw;background-color:#006873;'></div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/js/bootstrap.min.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.11.1/jquery.validate.min.js"></script>
        <script src="http://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCGb3Eqs1luybPwJKIBb-jNRRI0EGTehtc&libraries=places,geometry"></script>
        <!--<script src="http://iserve.ind.in:9999/socket.io/socket.io.js"></script>-->
        <script src="<?php echo socketpath;  ?>/socket.io/socket.io.js"></script>
        <!--<script src="http://iserve.ind.in/superadmin/application/views/godsview/presence.js"></script>-->
        <script src="<?php echo godsviewpath; ?>"></script>

        <script>
                                //driver table search
                                $(document).ready(function () {
                                    $('table.search-table').tableSearch({
                                        searchPlaceHolder: ' Search Provider'
                                    });
                                });

                                (function ($) {
                                    $.fn.tableSearch = function (options) {
                                        if (!$(this).is('table')) {
                                            return;
                                        }
                                        var tableObj = $(this),
                                                searchText = (options.searchText) ? options.searchText : '',
                                                searchPlaceHolder = (options.searchPlaceHolder) ? options.searchPlaceHolder : '',
                                                divObj = $('<div>' + searchText + '</div>'),
                                                inputObj = $('<input type="text" id="search" placeholder="' + searchPlaceHolder + '" />'),
                                                caseSensitive = (options.caseSensitive === true) ? true : false,
                                                searchFieldVal = '',
                                                pattern = '';
                                        inputObj.off('keyup').on('keyup', function () {
                                            searchFieldVal = $(this).val();
                                            pattern = (caseSensitive) ? RegExp(searchFieldVal) : RegExp(searchFieldVal, 'i');
                                            tableObj.find('tbody tr').hide().each(function () {
                                                var currentRow = $(this);
                                                currentRow.find('td').each(function () {
                                                    if (pattern.test($(this).html())) {
                                                        currentRow.show();
                                                        return false;
                                                    }
                                                });
                                            });
                                        });
                                        tableObj.before(divObj.append(inputObj));
                                        return tableObj;
                                    }
                                }(jQuery));

        </script>
    </body>

</html>