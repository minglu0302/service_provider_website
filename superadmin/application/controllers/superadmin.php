<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Superadmin extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model("superadminmodal");
        $this->load->library('session');
//        $this->load->library('excel');
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
    }

    public function index($loginerrormsg = NULL) {
        $data['loginerrormsg'] = $loginerrormsg;

        $this->load->view('company/login', $data);
    }
      public function confing_dispute() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
                
        $data['dispute'] = $this->superadminmodal->getallDispute();
		
        $data['pagename'] = "company/confing_dispute";
        $this->load->view("company", $data);
    }
    public function savedispute_confing() {
//        print_r("saf");die;
        return $this->superadminmodal->savedispute_confing();
    }

    public function setcity_session() {

        $meta = array('city_id' => $this->input->post('city'), 'company_id' => $this->input->post('company'));
        $this->session->set_userdata($meta);
    }

    public function tripDetails($param) {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $return['data'] = $this->superadminmodal->tripDetails($param);


        $return['pagename'] = "company/trip_details";
        $this->load->view("company", $return);
        //$this->superadminmodal->tripDetails();
    }

    public function AuthenticateUser() {
        $email = $this->input->post("email");
        $password = $this->input->post("password");
        if ($email && $password) {


            $status = $this->superadminmodal->ValidateSuperAdmin();


            if ($status) {
                if ($this->session->userdata('table') == 'company_info')
                    redirect(base_url() . "index.php/superadmin/Dashboard");
            } else {
                $loginerrormsg = "invalid email or password";
                $this->index($loginerrormsg);
            }
        } else
            redirect(base_url() . "index.php/superadmin");
    }

    function ForgotPassword() {
        $status = $this->superadminmodal->ForgotPassword();
        if ($status == 1)
            echo json_encode(array('succ' => 'Verification link  has been sent to your email !', 'succchk' => '1'));
        else
            echo json_encode(array('err' => 'Invalid email Id', 'errchk' => '2'));
    }

    function VerifyResetLink($vlink) {

        $this->load->database();

        $query = $this->db->query("select * from superadmin where resetData = '" . base_url() . "index.php/superadmin/VerifyResetLink/" . $vlink . "'");
        if ($query->num_rows() <= 0) {
            $data['vlink'] = $vlink;
            $this->load->view('company/sessionExp', $data);
        } else {
            $data['vlink'] = $vlink;
            $this->load->view('company/forgotpassword', $data);
        }
    }

    public function CompleteJob() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $sessionsetornot = $this->superadminmodal->issessionset();
        $data['gat_way'] = "2";
        $data['pagename'] = "company/complete_job";

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->table->clear();
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 50px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('JOB ID', 'PRO ID', 'PRO NAME', 'CUST ID', 'CUST NAME', 'DATE', 'CATEGORY', 'TYPE', 'STATUS', 'PAYMENT METHOD', 'JOB DETAILS');
//        $this->table->set_heading('Job Id', 'Provider Id', 'Pro Name', 'Cust Id', 'Cust Name', 'Date', 'Category', 'Type', 'invoice');
        $this->load->view("company", $data);
    }

    public function complete_getTransectionData() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $this->superadminmodal->complete_getTransectionData();
    }

    function varifyandsave($vlink) {

        $this->load->database();
        $val = $this->db->query("update superadmin set password  = md5('" . $this->input->post('password') . "'),resetData= '0' where resetData = '" . base_url() . "index.php/superadmin/VerifyResetLink/" . $vlink . "'");

        if ($val) {
            redirect(base_url() . "index.php/superadmin");
        }
    }

    public function uniq_val() {

        $this->superadminmodal->uniq_val_chk();
    }

    public function startpage() {

        $data['pagename'] = 'company/startpage';

        $this->load->view("company", $data);
    }

    public function Dashboard() {
        $sessionsetornot = $this->superadminmodal->issessionset();
        if ($sessionsetornot) {
            $data['todaybooking'] = $this->superadminmodal->Getdashboarddata();
            $data['pagename'] = "company/Dashboard";
            $this->load->view("company", $data);
        } else {
            redirect(base_url() . "index.php/superadmin");
        }
    }

    function datatable() {
        $this->superadminmodal->datatable();
    }

    public function Transaction() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $sessionsetornot = $this->superadminmodal->issessionset();
        $data['gat_way'] = "2";
        $data['pagename'] = "company/Transection";

        $this->load->library('Datatables');
        $this->load->library('table');
        $this->table->clear();
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 50px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('JOB ID', 'PRO ID', 'PRO NAME', 'CUST ID', 'CUST NAME', 'DATE', 'CATEGORY', 'TYPE', 'PRICE MODEL', 'DISCOUNT', 'BILLED AMOUNT', 'PRO EARNING (' . CURRENCY . ')', 'APP EARNING (' . CURRENCY . ')', 'PG COMMISSION(' . CURRENCY . ')', 'PAYMENT TXN ID', 'STATUS', 'PAYMENT METHOD', 'INVOICE');
//        $this->table->set_heading('Job Id', 'Provider Id', 'Pro Name', 'Cust Id', 'Cust Name', 'Date', 'Category', 'Type', 'invoice');
        $this->load->view("company", $data);
    }

    public function getBilledAmount() {
        $bid = $this->input->post('bid');
        $ser = $this->superadminmodal->getBilledAmount($bid);
        echo json_encode($ser);
    }

    public function showAvailableCities() {


        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $data = $this->superadminmodal->loadAvailableCity();
        echo json_encode($data);
//        return $data;
    }

    public function validateCompanyEmail() {


        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        return $this->superadminmodal->validateCompanyEmail();
    }

    function dt_passenger($status) {


        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->dt_passenger($status);
    }

    public function editdispatchers_city() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $data['editingcity'] = $this->superadminmodal->editdispatchers_city();
    }

    public function editdispatcherdata() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->editdispatcherdata();
    }

    public function datatable_cities() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $this->superadminmodal->datatable_cities();
    }

    public function datatable_dispatchers() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $this->superadminmodal->datatable_dispatchers();
    }

    public function datatable_promotion($id) {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $this->superadminmodal->datatable_promotion($id);
    }

    public function datatable_companys($status) {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->datatable_companys($status);
    }

    public function datatable_vehicletype() {


        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->datatable_vehicletype();
    }

    public function datatable_vehicles($status) {


        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $this->superadminmodal->datatable_vehicles($status);
    }

    public function datatable_driver($status) {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->datatable_driver($status);
    }

    public function datatable_dispatcher() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->datatable_dispatcher();
    }

    public function datatable_document($status = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->datatable_document($status);
    }

    public function validatePhone() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        return $this->superadminmodal->validatePhone();
    }

    public function datatable_driverreview($status) {


        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->datatable_driverreview($status);
    }

    public function datatable_proreview($status) {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->datatable_proreview($status);
    }

    public function datatable_bookings($status) {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->datatable_bookings($status);
    }

    public function datatable_compaigns($status) {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $this->superadminmodal->datatable_compaigns($status);
    }

    public function cities() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['country'] = $this->superadminmodal->get_country();
        $data['city_list'] = $this->superadminmodal->get_city();
        $this->load->library('Datatables');
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:20px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 127px;font-size: 11px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('COUNTRY', 'CITY', 'CURRENCY', 'LATITUDE', 'LONGITUDE', 'SELECT');
        $data['pagename'] = "company/cities";
        $this->load->view("company", $data);
    }

    public function dispatchers() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['country'] = $this->superadminmodal->get_country();
        $data['city_list'] = $this->superadminmodal->get_city();
        $this->load->library('Datatables');
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:20px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 127px;font-size: 11px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('SID', 'DISPATCHER NAME', 'EMAIL', 'LAST LOGIN DATE', 'CITY', 'PHONE', 'SELECT');
        $data['pagename'] = "company/dispatchers";
        $this->load->view("company", $data);
    }

    public function EditProduct($param = '') {

        $data['ProductId'] = $param;
        $data['ProductDetails'] = $this->superadminmodal->GetProductDetails($param);

        $data['AllCats'] = $this->superadminmodal->GetAllCategories();
        $data['pagename'] = "company/productdetails";
        $this->load->view("company", $data);
    }

    public function categories_services($status = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['status'] = $status;
        $data['entitylist'] = $this->superadminmodal->GetAllCategories();
        $data['entitylist_products'] = $this->superadminmodal->GetAllProducts();
        $data['city'] = $this->superadminmodal->city();
        $data['pagename'] = "company/catogiries_services";

        $this->load->view("company", $data);
    }

    function datatable_catogiries_services() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->datatable_catogiries_services();
    }

//    public function cservices() {
//        if ($this->session->userdata('table') != 'company_info') {
//            $this->Logout();
//        }
//        $this->load->library('table');
//        $data['city_list'] = $this->superadminmodal->get_city();
//        $data['citylist'] = $this->superadminmodal->get_cities();
//
//        $res = array();
//        $data['mcat'] = $this->superadminmodal->GetAllServices();
//        $allsubcat = $this->superadminmodal->get_All_SubCategory();
//        $citylist = $this->superadminmodal->get_cities();
//        foreach ($data['mcat'] as $mcat) {
//            foreach ($citylist as $city) {
//                if ($mcat['city_id'] == $city->City_Id) {
//                    $countsub = 0;
//                    foreach ($allsubcat as $scc) {
//                        if ($mcat['id'] == $scc['cat_id']) {
//                            $countsub++;
//                        }
//                    }
//                    $mcat['countsub'] = $countsub;
//                    $mcat['City_Name'] = $city->City_Name;
//                    array_push($res, $mcat);
//                }
//            }
//        }
//        $data['sub_data'] = $res;
//        $data['pagename'] = "company/services";
//        $this->load->view("company", $data);
//    }

    public function cservices() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['business'] = array();
        $this->load->library('Datatables');
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:11px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => ' <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 127px;font-size:11px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('SI NO', 'CITY NAME', 'NO OF SERVICES');
        $data['citylist'] = $this->superadminmodal->get_cities();
        $data['pagename'] = 'company/cservices';
        $this->load->view("company", $data);
    }

    public function datatable_cservices() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->datatable_cservices();
    }

    public function Get_SubCategory() {
        $res = array();
        $data['mcat'] = $this->superadminmodal->GetAllServices();
        $allsubcat = $this->superadminmodal->get_All_SubCategory();
        $citylist = $this->superadminmodal->get_cities();
        foreach ($data['mcat'] as $mcat) {
            foreach ($citylist as $city) {
                if ($mcat['city_id'] == $city->City_Id) {
                    $countsub = 0;
                    foreach ($allsubcat as $scc) {
                        if ($mcat['id'] == $scc['cat_id']) {
                            $countsub++;
                        }
                    }
                    $mcat['countsub'] = $countsub;
                    $mcat['City_Name'] = $city->City_Name;
                    array_push($res, $mcat);
                }
            }
        }
        echo json_encode($res);
    }

/*
   public function show_services($catid) {
       $data['citylist'] = $this->superadminmodal->get_cities();
       $data['mcat'] = $this->superadminmodal->GetAllServices();
       $data['sub_data'] = $this->superadminmodal->GetSubCategoryByMainCat($catid);
       $data['catdata'] = $this->superadminmodal->GetOneServices($catid);
       $data['pagename'] = "company/show_services";
       $this->load->view("company", $data);
   }
    public function show_services($cityid) {
       $data['services'] = $this->superadminmodal->GetAllServicesByCity($cityid);
       $data['pagename'] = "company/show_services1";
       $this->load->view("company", $data);
   }
*/

    public function show_services($cityid) {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['business'] = array();
        $this->load->library('Datatables');
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:11px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => ' <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 127px;font-size:11px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('SL NO', 'GROUP NAME', 'SERVICE NAME', 'DESCRIPTION', 'FIXED PRICE', 'OPTION');
        $data['cityid'] = $cityid;
        $data['citylist'] = $this->superadminmodal->get_cities();
        $data['catlist'] = $this->superadminmodal->GetAllServices();
        $data['grouplist'] = $this->superadminmodal->GetGroupByCategory();
        $data['pagename'] = 'company/show_services';
        $this->load->view("company", $data);
    }

    public function group_sort_ajax($cityid, $groupid) {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->group_sort_ajax($cityid, $groupid);
    }

    public function datatable_show_services($cityid) {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->datatable_show_services($cityid);
    }

    public function datatable_show_cat($cityid) {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->datatable_show_cat($cityid);
    }

    public function show_cat($cityid) {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['business'] = array();
        $this->load->library('Datatables');
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:11px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => ' <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 127px;font-size:11px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('SL NO', 'CATEGORY NAME', 'DESCRIPTION', 'BANNER IMAGE', 'SEL IMAGE', 'UNSEL IMAGE', 'PRICE SET BY', 'COMMISSION BY', 'PRICING MODEL', 'CAN FEES', 'MIN FEES', 'BASE FEE', 'PER/MIN', 'PER/MILE', 'VISIT FEES', 'FIXED PRICE', 'SELECT');
        $data['cityid'] = $cityid;
        $data['citylist'] = $this->superadminmodal->get_cities();
        $data['catlist'] = $this->superadminmodal->GetAllServices();
//        print_r($data); die;
        $data['grouplist'] = $this->superadminmodal->GetGroupByCategory();
        $data['pagename'] = 'company/show_cat';
        $this->load->view("company", $data);
    }

    public function GetServicesByCity() {
        $cityid = $this->input->post('cid');
        $servicelist = $this->superadminmodal->GetServicesByCity($cityid);
        echo json_encode($servicelist);
    }
    public function operator() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $this->load->library('Datatables');
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:20px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 127px;font-size: 11px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('SL NO', 'NAME', 'ADDRESS', 'MANAGER NAME', 'PHONE', 'EMAIL', 'NO OF PROVIDER', 'SELECT');
        $data['pagename'] = "company/operator";
        $this->load->view("company", $data);
    }
    public function datatable_operators() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $this->superadminmodal->datatable_operators();
    }
    public function insertoperator() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->insertoperator();
        return;
    }
    public function editoperatorpass() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['edit'] = $this->superadminmodal->editoperatorpass();
    }
    public function oprresetpassword() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->oprresetpassword();
        return;
    }

    public function editoperator() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['edit'] = $this->superadminmodal->editoperator();
    }
    public function deleteoperator() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->deleteoperator();
        return;
    }
    public function oprdetails($oprid = '') {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['operator_name'] = $this->superadminmodal->GetOperatorDetails($oprid);
        $this->load->library('Datatables');
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:20px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 127px;font-size: 11px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $data['oprid'] = $oprid;
        $this->table->set_template($tmpl);
        $this->table->set_heading('PROVIDER ID', 'FIRST NAME', 'LAST NAME', 'PROFILE PIC', 'PHONE', 'EMAIL', 'FEE TYPE', 'LAT/LONG');
        $data['pagename'] = "company/oprdetails";
        $this->load->view("company", $data);
    }
     public function datatable_oprdetails($oprid) {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $this->superadminmodal->datatable_oprdetails($oprid);
    }
    public function getCategorybyfreetype() {

        $servicelist = $this->superadminmodal->getCategorybyfreetype();
        echo json_encode($servicelist);
    }

    public function GetCategoryByCity() {
        $cityid = $this->input->post('cid');
        $servicelist = $this->superadminmodal->GetCategoryByCity($cityid);
        echo json_encode($servicelist);
    }

    public function GetCurrncy() {
        $cityid = $this->input->post('cityid');
        $servicelist = $this->superadminmodal->GetCurrncy($cityid);
        echo json_encode($servicelist);
    }

    public function GetServicesByCategory() {
        $catid = $this->input->post('cid');
        $servicelist = $this->superadminmodal->GetServicesByCategory($catid);
        echo json_encode($servicelist);
    }

    public function GetGroupByCategory() {
        $catid = $this->input->post('cid');
        $glist = $this->superadminmodal->GetGroupByCategory($catid);
        echo json_encode($glist);
    }

    public function DeleteServices() {
        $status = $this->superadminmodal->DeleteServices();
    }

    public function DeleteSubCategory() {
        $status = $this->superadminmodal->DeleteSubCategory();
    }

    public function GetOneServices() {
        $sid = $this->input->post('sid');
        $ser = $this->superadminmodal->GetOneServices($sid);
        echo json_encode($ser);
    }

    public function GetOneSubServices() {
        $sid = $this->input->post('sid');
        $sub = array();
        //$sid = "57728057e127c9d42772552f";
        $subser = $this->superadminmodal->GetOneSubServices($sid);
        foreach ($subser as $ser) {
            array_push($sub, $ser);
        }
        echo json_encode($sub);
    }

    public function productdetails() {

        $data['AllCats'] = $this->superadminmodal->GetAllCategories();
        $data['count'] = $this->superadminmodal->get_product_count();

        $data['pagename'] = "company/productdetails";
        $this->load->view("company", $data);
    }

    public function AddNewCategory() {
        $this->superadminmodal->AddNewCategory();
//        echo "Sss";
//        exit();
        redirect(base_url() . "index.php/superadmin/catogiries_services/1");
    }

//    
//    public function EditCategory()
//    {
//         $this->superadminmodal->EditCategory();
////         redirect(base_url() . "index.php/superadmin/catogiries_services/1");
//    }

    public function AddService() {
        $this->superadminmodal->AddService();
    }

    public function AddnewProduct() {

        $this->superadminmodal->AddnewProduct();
        redirect(base_url() . "index.php/superadmin/catogiries_services/3");
    }

    public function showcities() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->loadcity();
        echo json_encode($data);
    }

    public function logoutdriver() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->logoutdriver();
    }

    public function showcompanys() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->loadcompany();
        $vt = $this->input->post('vt');

        if ($vt == '1')
            $this->session->set_userdata(array('city_id' => $this->input->post('city')));
        $return = "<option value='0'>Select Company ...</option><option value='0'>None</option>";
        foreach ($data as $city) {
            $return .= "<option value='" . $city['company_id'] . "'>" . $city['companyname'] . "</option>";
        }
        echo $return;
    }

    public function insertcities() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->insert_city_available();
        return;
    }

    public function editlonglat() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['edit'] = $this->superadminmodal->editlonglat();
    }

    public function addingcountry() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        return $this->superadminmodal->addcountry();
    }

    public function addingcity() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['addingc'] = $this->superadminmodal->addcity();
    }

    public function addnewcity($status = "") {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['status'] = $status;

        $data['country'] = $this->superadminmodal->get_country();

        $data['pagename'] = 'company/addnewcity';

        $this->load->view("company", $data);
    }

    public function add_edit($status = "", $param = '', $param2 = '') {


        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $data['param'] = "";

        if ($status == 'edit') {
            $data['city_ram'] = $this->superadminmodal->city_sorted();
            $data['get_company_data'] = $this->superadminmodal->get_company_data($param);

            $data['status'] = $status;
            $data['param'] = $param;
        } elseif ($status == 'add') {
            $data['city_ram'] = $this->superadminmodal->city_sorted();
            $data['status'] = $status;
            $data['param'] = "";
        }


        $data['pagename'] = 'company/add_edit';

        $data['test'] = array('test' => 1);

        $this->load->view("company", $data);
    }

    public function activatecompany() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->activate_company();
    }

    public function delete_dispatcher() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['delete'] = $this->superadminmodal->delete_dispatcher();
    }

    public function suspendcompany() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['suspend'] = $this->superadminmodal->suspend_company();
    }

    public function deactivatecompany() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['deactivate'] = $this->superadminmodal->deactivate_company();
    }

    public function insertcompany() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['company'] = $this->superadminmodal->insert_company();
    }

    public function updatecompany($param = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $data['updatecompany'] = $this->superadminmodal->update_company($param);
    }

    public function company_s($status = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['status'] = $status;
        $data['city'] = $this->superadminmodal->city();
        $data['company'] = $this->superadminmodal->get_companyinfo($status);

        $this->load->library('Datatables');
        $this->load->library('table');

        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:20px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 127px;font-size: 11px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );

        $this->table->set_template($tmpl);

        $this->table->set_heading('COMPANY ID', 'COMPANY NAME', 'ADDRESS LINE1', 'CITY NAME', 'STATE', 'POST CODE', 'FIRST NAME', 'LAST NAME', 'EMAIL', 'MOBILE', 'SELECT');

        $data['pagename'] = "company/company_s";
        $this->load->view("company", $data);
//        $this->load->view("cities");
    }

    public function vehicle_type() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $data['vehicletype'] = $this->superadminmodal->get_vehicle_data();

        $this->load->library('Datatables');
        $this->load->library('table');

        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:20px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 127px;font-size: 11px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('TYPE ID', 'TYPE NAME', 'MAX SIZE', 'BASE FARE', 'MIN FARE', 'PRICE PER MINUTE' . ' (' . CURRENCY . ')', 'PRICE PER KILOMETER' . ' (' . CURRENCY . ')', 'TYPE DESCRIPTION', 'CITY NAME', 'CANCILATION FEE', 'SELECT');

        $data['pagename'] = "company/vehicle_type";
        $this->load->view("company", $data);
//        $this->load->view("cities");
    }

    public function delete_vehicletype() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['delete'] = $this->superadminmodal->delete_vehicletype();
    }

    public function activate_vehicle() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->activate_vehicle();
    }

    Public function reject_vehicle() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->reject_vehicle();
    }

    public function inactivedriver_review() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['driver_review'] = $this->superadminmodal->inactivedriver_review();
    }

    public function activedriver_review() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $data['driver_review'] = $this->superadminmodal->activedriver_review();
    }

    public function editdriver_review() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['driver_review'] = $this->superadminmodal->editdriver_review();
    }

    public function editGetdriver_review() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $reviewRating = $this->superadminmodal->editGetdriver_review();
    }

    public function vehicletype_addedit($status = '', $param = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $data['param'] = "";
        print($data);

        if ($status == 'edit') {
            $data['editvehicletype'] = $this->superadminmodal->edit_vehicletype($param);

            $data['status'] = $status;
            $data['param'] = $param;
        } elseif ($status == 'add') {
            $data['city'] = $this->superadminmodal->city();
            $data['status'] = $status;
            $data['param'] = "";
        }
        $data['pagename'] = "company/vehicletype_addedit";
        $data['city'] = $this->superadminmodal->get_city();
        $this->load->view("company", $data);
    }

    public function editvehicle($status = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $return['data'] = $this->superadminmodal->editvehicle($status);

        $return['vehId'] = $status;

        $return['pagename'] = "company/editvehicle";
        $this->load->view("company", $return);
    }

    public function insert_vehicletype() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->insert_vehicletype();
    }

    public function update_vehicletype($param = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['updatevehicletype'] = $this->superadminmodal->update_vehicletype($param);
    }

    public function inactivedispatchers() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->inactivedispatchers();
    }

    public function activedispatchers() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->activedispatchers();
    }

    public function editdispatchers() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $data['city'] = $this->superadminmodal->city();
        $data = $this->superadminmodal->editdispatchers();
    }

    public function insertdispatches() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->insertdispatches();
        return;
    }

    public function editpass() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->editpass();
    }

    public function deleteDriverBank() {
        $this->superadminmodal->deleteDriverBank();
    }

    public function AddBankAccountInitial() {
        $res = $this->superadminmodal->AddBankAccountInitial();
        echo json_encode($res);
    }

    public function editdriverpassword() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->editdriverpassword();
    }

    public function editdispatcherpassword() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->editdispatcherpassword();
    }

    public function editsuperpassword() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->editsuperpassword();
    }

    public function patients($status = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $data['status'] = $status;
        $data['passenger_info'] = $this->superadminmodal->get_passengerinfo($status);


        $this->load->library('Datatables');
        $this->load->library('table');

        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:20px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 35PX;font-size: 11px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('CUSTOMER ID', 'NAME', 'MOBILE', 'EMAIL', 'REGISTRATION DATE', 'APP VERSION', 'PROFILE PIC', 'OPTION', 'SELECT');

        //print_r($data);
        $data['pagename'] = "company/passengers";
        $this->load->view("company", $data);
    }

    public function EditGetCustomer() {
        $customer = $this->superadminmodal->EditGetCustomer();
        echo json_encode($customer);
    }

    public function EditCustomer() {
        $customer = $this->superadminmodal->EditCustomer();
        redirect(base_url() . "index.php/superadmin/patients/3");
    }

    public function aceptencerate() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['pagename'] = 'company/aceptencerate';

        $this->load->library('Datatables');
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 50px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );

        $this->table->set_template($tmpl);
        $this->table->set_heading('PROVIDER ID', 'PROVIDER NAME', 'PROVIDER PHONE', 'TOTAL BOOKINGS', 'TOTAL ACCEPTED', 'TOTAL REJECTED', 'TOTAL CANCELLED', "DIDN'T RESPOND", 'ACCEPTANCE RATE (%)');
        $this->load->view("company", $data);
    }

    public function DriverAcceptence_ajax() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->DriverAcceptence_ajax();
    }

    public function testmail() {


        $this->superadminmodal->testmail();
    }

    public function acceptdrivers() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['result'] = $this->superadminmodal->acceptdrivers();
        print_r($data);
        exit();
    }

    public function GetAllCatForProvider() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $proid = $this->input->post('val');
        $this->superadminmodal->GetAllCatForProvider($proid);
    }

    public function AcceptCategory() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->AcceptCategory();
    }

    public function test() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->testpush();
    }

    public function testfcmpush($args) {
        $message = "testing";
        $aplPushContent = array('alert' => $message, 'nt' => '13', 'sname' => "dsfg", 'dt' => "sdasda", 'smail' => "dsadasd", 'bid' => "1");
        $andrPushContent = array("payload" => $message, 'action' => '13', 'sname' => "sda", 'dt' => "sddas", 'smail' => "Dasda", 'bid' => "2");
        $data = $this->_sendPush(1, array(157), $message, '2', "Laxman", "SDsa", '2', $aplPushContent, $andrPushContent);

        echo "<pre>";
        print_r($data);
    }

    public function _sendPush($senderId, $recEntityArr, $message, $notifType, $sname, $datetime, $user_type, $aplContent, $andrContent, $user_device = NULL) {

        $entity_string = '';
        $aplTokenArr = array();
        $andiTokenArr = array();
        $return_arr = array();

        foreach ($recEntityArr as $entity) {

            $entity_string .= $entity . ',';
        }

        $entity_comma = rtrim($entity_string, ',');
//echo '--'.$entity_comma.'--';

        $device_check = '';
        if ($user_device != NULL)
            $device_check = " and device = '" . $user_device . "'";

        $getUserDevTypeQry = "select distinct type,push_token from user_sessions where oid in (" . $entity_comma . ") and loggedIn = '1' and user_type = '" . $user_type . "' and LENGTH(push_token) > 63" . $device_check;

        $getUserDevTypeRes = mysql_query($getUserDevTypeQry, $this->db->conn);
        if (mysql_num_rows($getUserDevTypeRes) > 0) {

            while ($tokenArr = mysql_fetch_assoc($getUserDevTypeRes)) {

                if ($tokenArr['type'] == 1)
                    $aplTokenArr[] = $tokenArr['push_token'];
                else if ($tokenArr['type'] == 2)
                    $andiTokenArr[] = $tokenArr['push_token'];
            }

            $aplTokenArr = array_values(array_filter(array_unique($aplTokenArr)));
            $andiTokenArr = array_values(array_filter(array_unique($andiTokenArr)));

            $aplResponse = $this->_sendFcmPush($andiTokenArr, $aplTokenArr, $andrContent, $aplContent, $user_type);

            foreach ($recEntityArr as $entity) {
                $ins_arr = array('notif_type' => (int) $notifType, 'sender' => (int) $senderId, 'reciever' => (int) $entity, 'message' => $message, 'notif_dt' => $datetime, 'apl' => $aplTokenArr, 'andr' => $andiTokenArr); //'aplTokens' => $aplTokenArr, 'andiTokens' => $andiTokenArr, 'andiRes' => $andiResponse,

                $newDocID = $ins_arr['_id'];
                $return_arr[] = array($entity => $newDocID);
            }
            if ($aplResponse['errorNo'] != '')
                $errNum = $aplResponse['errorNo'];
//            else if ($andiResponse['errorNo'] != '')
//                $errNum = $andiResponse['errorNo'];
            else
                $errNum = 46;

            return array('insEnt' => $return_arr, 'errNum' => $errNum, 'andiRes' => $andiResponse);
        } else {
            return array('insEnt' => $return_arr, 'errNum' => 45, 'andiRes' => $andiResponse); //means push not sent
        }
    }

    function _sendFcmPush($andiTokenArr, $aplTokenArr, $andrContent, $aplContent, $user_type) {
        $aplContent['sound'] = 'default';
        $andrContent['sound'] = 'default';

        $applPush = array(
            'to' => '',
            'collapse_key' => 'your_collapse_key',
            'priority' => 'high',
            'notification' => array(
                'title' => '',
                'body' => $aplContent['alert'],
                'sound' => 'default'
            ),
            'data' => $aplContent
        );
        $andPush = array(
            'to' => '',
            'collapse_key' => 'your_collapse_key',
            'priority' => 'high',
//            'notification' => $andrContent,
            'data' => $andrContent
        );
        $header = array('Content-Type: application/json', "Authorization: key=" . PUSH_FCM_KEY);
        foreach ($andiTokenArr as $token) {
            $andPush['to'] = $token;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, FCM_PUSH_URL);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($andPush));
            $result = curl_exec($ch);
            curl_close($ch);
            $res_dec = $result;
        }
        foreach ($aplTokenArr as $token) {
            $applPush['to'] = $token;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, FCM_PUSH_URL);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($applPush));
            $result = curl_exec($ch);
            curl_close($ch);
            $res_dec = $result;
        }

        if ($res_dec['success'] >= 1)
            return array('errorNo' => 44, 't' => $applPush, 'tok' => $aplTokenArr, 'ret' => $res_dec->result, 'res' => $res_dec);
        else
            return array('errorNo' => 46, 't' => $applPush, 'tok' => $aplTokenArr, 'res' => $res_dec);
    }

    public function rejectdrivers() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->rejectdrivers();
    }

    public function inactivepassengers() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['inactive'] = $this->superadminmodal->inactivepassengers();
    }

    public function activepassengers() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['inactive'] = $this->superadminmodal->activepassengers();
    }

    public function insertpass() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['pass'] = $this->superadminmodal->insertpass();
//        print_r($res);
    }

    public function Vehicles($status = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['company'] = $this->superadminmodal->company_data();
        $data['vehicles'] = $this->superadminmodal->Vehicles($status);


        $this->load->library('Datatables');
        $this->load->library('table');

        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:20px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 50px;font-size: 11px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);

        $this->table->set_heading('VEHICLE ID', 'VEHICLE MAKE', 'VEHICLE MODEL', 'TYPE NAME', 'COMPANY NAME', 'VEHICLE  REGISTRATION NUMBER', 'LICENSE PLATE NUMBER', 'VEHICLE INSURANCE NUMBER', 'VEHICLE COLOR', 'CITY', 'SELECT');


        $data['pagename'] = 'company/vehicles';
        $data['status'] = $status;
        $this->load->view("company", $data);
    }

    public function deletecompany() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->deletecompany();
    }

    public function datatable_drivers($for = '', $status = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->datatable_drivers($for, $status);
    }

    public function deletecountry() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->deletecountry();
    }

    public function deletepagecity() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->deletepagecity();

        print_r($data);
        exit();
    }

    public function commission() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
//        $data['business'] = $this->superadminmodal->businessdata();
        $data['business'] = array();
        $this->load->library('Datatables');
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:11px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => ' <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 127px;font-size:11px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('SI NO', 'PRO ID', 'PROVIDER NAME', 'EMAIL', ' APP COMMISSION (%)', 'SELECT');
        $data['pagename'] = 'company/commission';
        $this->load->view("company", $data);
    }

    public function datatable_commission() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->datatable_commission();
    }

    public function datatable_sgroup() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->datatable_sgroup();
    }

    public function datatable_pgateway() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->datatable_pgateway();
    }

    public function datatable_pro_rating($proid) {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->datatable_pro_rating($proid);
    }

    public function insertcommission() {
        $this->superadminmodal->insertcommission();
    }

    public function AddServiceGroup() {
        $this->superadminmodal->AddServiceGroup();
    }

    public function AddPaymentGateway() {
        $this->superadminmodal->AddPaymentGateway();
    }

    public function EditServiceGroup() {
        $this->superadminmodal->EditServiceGroup();
    }

    public function EditPaymentGateWay() {
        $this->superadminmodal->EditPaymentGateWay();
    }

    public function EditGetServiceGroup() {
        $this->superadminmodal->EditGetServiceGroup();
    }

    public function EditGetPaymentGateWay() {
        $this->superadminmodal->EditGetPaymentGateWay();
    }

    public function DeleteServiceGroup() {
        $data = $this->superadminmodal->DeleteServiceGroup();
    }

    public function DeletePaymentGateway() {
        $data = $this->superadminmodal->DeletePaymentGateway();
    }

    public function editcommission() {
        $bdata = $this->superadminmodal->editcommission();
        echo json_encode($bdata);
    }

    public function deleteCommission() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->deletecommission();
    }

    public function Drivers($for = '', $status = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $this->load->library('Datatables');
        $this->load->library('table');
        $data['status'] = $status;

        $tmpl = array('table_open' => '<table id="big_table1" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:20px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => ' <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width:30px;font-size: 11px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);

        $this->table->set_heading('PROVIDER ID', 'NAME', 'MOBILE', 'EMAIL', 'REG DATE', 'APP VERSION', 'RATING', 'FEE TYPE', 'PROFILE PIC', 'DEVICE TYPE', 'LAT/LONG', 'OPTION', 'SELECT');

        $data['pagename'] = 'company/drivers';
        $this->load->view("company", $data);
    }

    public function addnewvehicle() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $data['company'] = $this->superadminmodal->get_company();
        $data['pagename'] = 'company/addnewvehicle';

        $this->load->view("company", $data);
    }

    public function addnewdriver() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
//        $data['profession'] = $this->superadminmodal->getprofessiondata();
        $data['profession'] = $this->superadminmodal->GetAllServices();
        $data['city'] = $this->superadminmodal->city();
//        echo "<pre>";
//        print_r($data);
//        exit();
        $data['pagename'] = 'company/addnewdriver';
        $this->load->view("company", $data);
    }

    public function insertDispatcher() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->insertDispatcher();
        return;
    }

    public function getdispatcherdetail() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->getdispatcherdetail();
    }

    public function editdriver($status = '') {


        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
//        $return['profession'] = $this->superadminmodal->getprofessiondata();
        $return['data'] = $this->superadminmodal->editdriverBank($status);
        $return['ddata'] = $this->superadminmodal->editdriver($status);
        $return['driverid'] = $status;
        $return['driverData'] = $this->superadminmodal->getDriverDetail($status);
        $return['catdata'] = $this->superadminmodal->getcidbypro($status);
        $return['docdetail'] = $this->superadminmodal->getdocdetailes($status);
        $return['education'] = $this->superadminmodal->geteducationdata($status);
        $scat = array();
        foreach ($return['catdata'] as $cat) {
            if (isset($cat['catlist'])) {
                foreach ($cat['catlist'] as $cats) {
                    array_push($scat, $cats['cid']);
                }
            }
        }

        $return['scats'] = implode('_', $scat);

        $acat = array();
        $rcat = array();
        foreach ($return['catdata'] as $accept) {
            $accept['id'] = (string) $accept['_id'];
            if (isset($accept['prolist'])) {
                foreach ($accept['prolist'] as $pro) {
                    if ($pro['pid'] == $status) {
                        if ($pro['status'] == 0) {
                            array_push($rcat, $accept);
                        }
                        if ($pro['status'] == 1) {
                            array_push($acat, $accept);
                        }
                    }
                }
            }
        }

        $return['pagename'] = 'company/editdriver';

        $this->load->view("company", $return);
    }

    public function editNewVehicleData() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }


        $this->superadminmodal->editNewVehicleData();
        redirect(base_url() . "index.php/superadmin/Vehicles/5");
    }

    //* my controllers name is naveena *//

    public function transection_data_ajax() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $this->superadminmodal->getTransectionData();
    }

    public function transection_data_form_date($stdate = '', $enddate = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->transection_data_form_date($stdate, $enddate);
    }

    public function callExel($stdate = '', $enddate = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $data = $this->superadminmodal->get_all_data($stdate, $enddate);
        $this->excel->stream('Transaction_' . time() . '.xls', $data);
    }

    public function deleteDrivers() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $this->superadminmodal->deletedriver();
    }

    public function deletePatient() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->deletePatient();
    }

    public function callExel_payroll() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $value = $this->adminmodel->payroll();
        $slno = 1;
        foreach ($value as $result) {
            $data[] = array(
                'slno' => $slno,
                'Driver_Id' => $result->mas_id,
                'Driver_Name' => $result->first_name,
                'Today_earning' => $result->today_earnings,
                'Week_earning' => $result->week_earnings,
                'Month_earning' => $result->month_earnings,
                'Total_earning' => $result->total_earnings,
            );
            $slno++;
        }
        $this->excel->stream('Transaction.xls', $data);
    }

    public function bookings($status = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['status'] = $status;

        $data['pagename'] = "company/bookings";
        $this->load->library('Datatables');
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 50px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );


        $this->table->set_template($tmpl);

        $this->table->set_heading('BOOKING ID', 'BOOKING DATE', 'CUSTOMER ID', 'CUSTOMER NAME', 'PHONE', 'ADDRESS', 'BOOKING TYPE', 'JOB STATUS', 'PRO ASSIGNED', 'PRO PHONE', 'CATEGORY', 'PRO LIST');
        $this->load->view("company", $data);
    }

    public function bookings_data_ajax($status = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->getbooking_data($status);
    }

    public function onGoing_jobs($status = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $this->load->library('Datatables');
        $this->load->library('table');
        $data['status'] = $status;

        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:20px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => ' <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 35px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);

        $this->table->set_heading('BOOKING ID', 'PROVIDER ID', 'PROVIDER NAME', 'PROVIDER PHONE', 'CUSTOMER NAME', 'CUSTOMER PHONE', 'DATE TIME', 'JOB ADDRESS', 'STATUS');
        $data['ongoing_booking'] = $this->superadminmodal->get_ongoing_bookings();


        $data['pagename'] = 'company/onGoing_jobs';
        $this->load->view("company", $data);
    }

    public function cust_details($status = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $prodata = $this->superadminmodal->cust_details_get($status);

        $this->load->library('Datatables');
        $this->load->library('table');
        $data['status'] = $status;
        $data['name'] = $prodata->first_name;
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:20px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => ' <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 35px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);

        $this->table->set_heading('DEVICE ID', 'MANUFACTURER', 'DEVICE MODEL', 'DEVICE TYPE', 'DEVICE OS', 'APP VERSION', 'LAST LOGGED IN');
        $data['pagename'] = 'company/cust_details';
        $this->load->view("company", $data);
    }

    public function datatable_cust_details($custid) {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->datatable_cust_details($custid);
    }

    public function pro_details($status = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $prodata = $this->superadminmodal->pro_details_get($status);

        $this->load->library('Datatables');
        $this->load->library('table');
        $data['status'] = $status;
        $data['name'] = $prodata->first_name;
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:20px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => ' <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 35px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('DEVICE ID', 'MANUFACTURER', 'DEVICE MODEL', 'DEVICE TYPE', 'DEVICE OS', 'APP VERSION', 'LAST LOGGED IN');

        $data['pagename'] = 'company/pro_details';
        $this->load->view("company", $data);
    }

    public function datatable_pro_details($custid) {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->datatable_pro_details($custid);
    }

    public function get_provider_jobs($proid) {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->get_provider_jobs($proid);
        echo json_encode($data);
    }

    public function datatable_onGoing_jobs() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->datatable_onGoing_jobs();
    }

    public function get_appointmentDetials() {
        $this->superadminmodal->get_appointmentDetials();
    }

    public function get_appointment_details() {
        $this->superadminmodal->get_appointment_details();
    }

    //Get the all On-Going jobs by filtered city
    public function filter_AllOnGoing_jobs() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $this->superadminmodal->filter_AllOnGoing_jobs();
    }

    public function CompleteBooking() {
        $this->superadminmodal->CompleteBooking();
    }
   public function cancelBooking() {
        $this->superadminmodal->cancelBooking();
    }
    public function getBookingDetails() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $bdata = $this->superadminmodal->getBookingDetails();
        echo json_encode($bdata);
    }

    public function dispatched($status = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['status'] = $status;
        $data['city'] = $this->superadminmodal->city();
        $data['getdata'] = $this->superadminmodal->get_dispatchers_data($status);

        $this->load->library('Datatables');
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 50px;font-size:11px">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );


        $this->table->set_template($tmpl);

        $this->table->set_heading('DISPATCHER ID', 'CITY', 'EMAIL', 'DISPATCHER NAME', 'OPTION');




        $data['pagename'] = "company/dispatched";

        $this->load->view("company", $data);
    }

    public function finance($status = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['status'] = $status;
        $data['pagename'] = "company/finance";
        $this->load->view("company", $data);
    }

    public function joblogs($value = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['value'] = $value;
        $data['joblogs'] = $this->superadminmodal->get_joblogsdata($value);
        $data['pagename'] = "company/joblogs";
        $this->load->view("company", $data);
    }

    public function sessiondetails($value = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['value'] = $value;
        $data['session_details'] = $this->superadminmodal->get_sessiondetails($value);

        $data['pagename'] = "company/sessiondetails";
        $this->load->view("company", $data);
    }

    public function document($status = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $data['status'] = $status;

        $data['master'] = $this->superadminmodal->driver();

        $data['document_data'] = $this->superadminmodal->get_documentdata($status);

        $data['workname'] = $this->superadminmodal->get_workplace();


        $data['pagename'] = "company/document";
        $this->load->view("company", $data);
    }

    public function patient_rating() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $data['passenger_rating'] = $this->superadminmodal->passenger_rating();



        $this->load->library('Datatables');
        $this->load->library('table');

        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:20px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 50px;font-size: 11px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);

        $this->table->set_heading('CUSTOMER ID', 'CUSTOMER NAME', 'CUSTOMER EMAIL', 'AVG RATING');


        $data['pagename'] = "company/passenger_rating";
        $this->load->view("company", $data);
    }

    public function datatable_passengerrating() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $this->superadminmodal->datatable_passengerrating();
    }

    public function getmap_values() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $this->superadminmodal->getmap_values();
    }

    public function doctor_review($status = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['status'] = $status;
        $data['driver_review'] = $this->superadminmodal->driver_review($status);

        $this->load->library('Datatables');
        $this->load->library('table');

        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:20px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 50px;font-size: 11px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);

        $this->table->set_heading('BOOKING ID', 'BOOKING DATE AND TIME', 'PROVIDER NAME', 'CUSTOMER ID', 'REVIEW', 'RATING', 'SELECT');


        $data['pagename'] = "company/driver_review";
        $this->load->view("company", $data);
    }

    public function disputes($status = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['status'] = $status;
        $data['city'] = $this->superadminmodal->get_city();
//        $data['disputesdata'] = $this->superadminmodal->get_disputesdata($status);
//        $data['master'] = $this->superadminmodal->driver();
//        $data['slave'] = $this->superadminmodal->passenger();

        $this->load->library('Datatables');
        $this->load->library('table');

        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:20px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 50px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);

        $this->table->set_heading('DISPUTE ID', 'CUSTOMER ID', 'CUSTOMER NAME', 'PROVIDER ID', 'PROVIDER NAME', 'MESSAGE', 'DATE', 'BOOKING ID', 'SELECT');



        $data['pagename'] = "company/disputes";
        $this->load->view("company", $data);
    }

    public function datatable_disputes($status) {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $this->superadminmodal->datatable_disputes($status);
    }

    public function documentgetdata() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        echo json_encode($this->superadminmodal->documentgetdata());
        exit();
    }

    public function documentgetdatavehicles() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        echo json_encode($this->superadminmodal->documentgetdatavehicles());
        exit();
    }

    public function resolvedisputes() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->resolvedisputes();
    }

    public function delete() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $data['getvehicletype'] = $this->superadminmodal->get_vehivletype();
        $data['getcompany'] = $this->superadminmodal->get_company();
        $data['city_ram'] = $this->superadminmodal->city_sorted();
        $data['driver'] = $this->superadminmodal->get_driver();
        $data['vehiclemodal'] = $this->superadminmodal->vehiclemodal();
        $data['country'] = $this->superadminmodal->get_country();
        $data['pagename'] = "company/delete";

        $this->load->view("company", $data);
    }

    public function deactivecompaigns() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['deactivate'] = $this->superadminmodal->deactivecompaigns();
    }

    public function deletetype() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->deletetype();
    }

//    public function godsview() {
//
//        if ($this->session->userdata('table') != 'company_info') {
//            $this->Logout();
//        }
//        $data['pagename'] = "company/godsview";
//        $data['cities'] = $this->superadminmodal->get_cities();
//        $this->load->view("company", $data);
//    }

    public function godsview() {
        $this->load->library('mongo_db');
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $data['pagename'] = "company/godsview";
//        $data['takeaway_list'] = $this->mongo_db->get('TakeawayGroup');
        $data['city'] = $this->superadminmodal->city();
//        $data['cities'] = $this->superadminmodal->get_cities();
        $this->load->view("godsview/godsview", $data);
    }

    public function getDtiversArround() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->load->library('mongo_db');
        $mongo = $this->mongo_db->db;
        $query = array();
        $apptStatusVals = array(2, 3, 4);
        if ($this->input->post('type_id') != 0) {
            $query['type'] = (int) $this->input->post('type_id');
        }
        if ($this->input->post('selected') == 2)
            $query['status'] = 4;
        else if ($this->input->post('selected') == 3)
            $query['status'] = 5;
        else
            $query['status'] = 3;
        $resultArr = $mongo->selectCollection('$cmd')->findOne(array(
            'geoNear' => 'location',
            'near' => array(
                (double) $this->input->post('longitude'), (double) $this->input->post('lattitude')//77.59492814540863, 13.031087726673466
            ), 'spherical' => true, 'maxDistance' => 50000 / 6378137, 'distanceMultiplier' => 6378137,
            'query' => $query)
        );
        $driversArr = array();
        foreach ($resultArr['results'] as $res) {
            if ($this->session->userdata('table') != 'company_info') {
                $this->Logout();
            }
            $doc = $res['obj'];
            $dis = $res['dis'];
            $iconPath = "http://www.privemd.com/superadmin_v2/dashboard//theme/icon/";
            $switch = $doc['type'];
            switch ($switch) {
                case 1: $icon = $iconPath . "physician_icon.png";
                    break;
                case 2: $icon = $iconPath . "physician_extender_icon.png";
                    break;
                default : $icon = $iconPath . "prive_md_all.png";
                    break;
            }
            $driversArr[] = array('lat' => (double) $doc['location']['latitude'], 'lon' => (double) $doc['location']['longitude'], 'id' => $doc['user'], 'type_id' => $doc['type'], 'status' => (int) $doc['status'], 'icon' => $icon);
        }
        echo json_encode(array('result' => $driversArr, 'texxt' => $query));
    }

    public function getDtiverDetail() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->getDtiverDetail();
    }

    public function makeonline() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->makeonline();
    }

    public function makeOffline() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->makeOffline();
    }

    public function makeLogout() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->makeLogout();
    }

    public function refreshMap($param = '') {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->load->library('mongo_db');
        $mongo = $this->mongo_db->db;
        $query = array();
        $apptStatusVals = array(2, 3, 4);
        if ($this->input->post('type_id') != 0) {
            $query['type'] = (int) $this->input->post('type_id');
        }
        if ($this->input->post('selected') == 2)
            $query['status'] = 4;
        else if ($this->input->post('selected') == 3)
            $query['status'] = 5;
        else
            $query['status'] = 3;
        $resultArr = $mongo->selectCollection('$cmd')->findOne(array(
            'geoNear' => 'location',
            'near' => array(
                (double) $this->input->post('longitude'), (double) $this->input->post('lattitude')//77.59492814540863, 13.031087726673466
            ), 'spherical' => true, 'maxDistance' => 50000 / 6378137, 'distanceMultiplier' => 6378137,
            'query' => $query)
        );
        $driversArr = array();
        foreach ($resultArr['results'] as $res) {
            if ($this->session->userdata('table') != 'company_info') {
                $this->Logout();
            }
            $doc = $res['obj'];
            $dis = $res['dis'];
            $driversArr[] = $doc['user'];
            $iconPath = "http://www.privemd.com/superadmin_v2/dashboard//theme/icon/";
            $switch = $doc['type'];
            switch ($switch) {
                case 1: $icon = $iconPath . "physician_icon.png";
                    break;
                case 2: $icon = $iconPath . "physician_extender_icon.png";
                    break;
                default : $icon = $iconPath . "prive_md_all.png";
                    break;
            }
            $dreiverdata[$doc['user']] = array('lat' => (double) $doc['location']['latitude'], 'lon' => (double) $doc['location']['longitude'], 'id' => $doc['user'], 'type_id' => $doc['type'], 'status' => (int) $doc['status'], 'icon' => $icon);
//            $driversArr[] = array('lat' => (double) $doc['location']['latitude'], 'lon' => (double) $doc['location']['longitude'], 'id' => $doc['user'], 'type_id' => $doc['type'], 'status' => (int) $doc['status'], 'icon' => $icon);
        }
        echo json_encode(array('online' => $driversArr, 'master_data' => $dreiverdata));
    }

    public function get_vehicle_type() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }


        $this->load->library('mongo_db');

        $mongo = $this->mongo_db->db;
        $typesData = array();
        $cond = array(
            'geoNear' => 'vehicleTypes',
            'near' => array(
                (double) $this->input->post('pic_long'), (double) $this->input->post('pic_lat')
            ), 'spherical' => true, 'maxDistance' => 50000 / 6378137, 'distanceMultiplier' => 6378137);

        $resultArr1 = $mongo->selectCollection('$cmd')->findOne($cond);

        foreach ($resultArr1['results'] as $res) {
            $doc = $res['obj'];

            $types[] = (int) $doc['type'];

            $typesData[$doc['type']] = array(
                'type_id' => (int) $doc['type'],
                'type_name' => $doc['type_name'],
                'max_size' => (int) $doc['max_size'],
                'basefare' => (float) $doc['basefare'],
                'min_fare' => (float) $doc['min_fare'],
                'price_per_min' => (float) $doc['price_per_min'],
                'price_per_km' => (float) $doc['price_per_km'],
                'type_desc' => $doc['type_desc']
            );
        }

        echo json_encode($typesData);
    }

    public function vehicle_models($status = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['status'] = $status;
        $data['vehiclemake'] = $this->superadminmodal->get_vehiclemake();

        if ($status == 1) {

            $this->load->library('Datatables');
            $this->load->library('table');

            $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
                'heading_row_start' => '<tr style= "font-size:20px"role="row">',
                'heading_row_end' => '</tr>',
                'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 50px;font-size: 11px;">',
                'heading_cell_end' => '</th>',
                'row_start' => '<tr>',
                'row_end' => '</tr>',
                'cell_start' => '<td>',
                'cell_end' => '</td>',
                'row_alt_start' => '<tr>',
                'row_alt_end' => '</tr>',
                'cell_alt_start' => '<td>',
                'cell_alt_end' => '</td>',
                'table_close' => '</table>'
            );
            $this->table->set_template($tmpl);


            $this->table->set_heading('ID', 'TYPE NAME', 'SELECT');
        } else if ($status == 2) {

            $this->load->library('Datatables');
            $this->load->library('table');

            $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
                'heading_row_start' => '<tr style= "font-size:20px"role="row">',
                'heading_row_end' => '</tr>',
                'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 50px;font-size: 11px;">',
                'heading_cell_end' => '</th>',
                'row_start' => '<tr>',
                'row_end' => '</tr>',
                'cell_start' => '<td>',
                'cell_end' => '</td>',
                'row_alt_start' => '<tr>',
                'row_alt_end' => '</tr>',
                'cell_alt_start' => '<td>',
                'cell_alt_end' => '</td>',
                'table_close' => '</table>'
            );
            $this->table->set_template($tmpl);

            $this->table->set_heading('ID', 'MAKE', 'MODEL', 'SELECT');
        }


        $data['pagename'] = "company/vehicle_models";
        $this->load->view("company", $data);
    }

    function datatable_vehiclemodels($status = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }


        $this->load->library('Datatables');
        $this->load->library('table');

        if ($status == 1) {

            $this->datatables->select("id,vehicletype")
                    ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'id')
                    ->from("vehicleType"); //order by slave_id DESC ",false);
        } else if ($status == 2) {


            $this->datatables->select("vm.id,vt.vehicletype,vm.vehiclemodel")
                    ->add_column('select', '<input type="checkbox" class="checkbox" name="checkbox" value="$1"/>', 'vm.id')
                    ->from("vehiclemodel vm,vehicleType vt")
                    ->where("vm.vehicletypeid = vt.id"); //order by slave_id DESC ",false);
        }

        echo $this->datatables->generate();
    }

    public function inserttypename() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->insert_typename();
    }

    public function insertmodal() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->insert_modal();
    }

    public function deletevehicletype() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->deletevehicletype();
    }

    public function delete_company() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->delete_company();
    }

    public function deletevehiclemodal() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->deletevehiclemodal();
    }

    public function deletevehicletypemodel() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->deletevehicletypemodel();
    }

    public function deletedriver() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->deletedriver();
    }

    public function deletemodal() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->deletemodal();
    }

    public function promo_details($id = '', $page = 1) {
        $data['promo_details'] = $this->superadminmodal->get_promo_details($id, $page);
        $data['coupon_id'] = $id;
        $data['pagename'] = "company/promo_details";

        $this->load->view("company", $data);
    }

    public function referral_details($id = '', $page = 1) {

        $data['referral_details'] = $this->superadminmodal->get_referral_details($id, $page);
        $data['coupon_id'] = $id;
        $data['pagename'] = "company/referral_details";
        $this->load->view("company", $data);
    }

    public function refered($code = '', $refCode = '', $page = 1) {

        $data['refered'] = $this->superadminmodal->refered($code, $refCode, $page);
//        print_r($data);
//        exit();

        $data['coupon_id'] = $code;

        $data['pagename'] = "company/refered";

        $this->load->view("company", $data);
    }

    public function compaigns($status = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['status'] = $status;
        $data['city'] = $this->superadminmodal->get_city();
        $data['compaign'] = $this->superadminmodal->get_compaigns_data($status);

        $data['pagename'] = "company/compaigns";
        $this->load->view("company", $data);
    }

    public function compaigns_ajax($for = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        echo $this->superadminmodal->get_compaigns_data_ajax($for);
    }

    public function insertcompaigns() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        echo $this->superadminmodal->insertcampaigns();
    }

    public function updatecompaigns() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        echo $this->superadminmodal->updatecompaigns();
    }

    public function editcompaigns() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->editcompaigns();
    }

    public function cancled_booking() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['pagename'] = "company/cancled_booking";
        $this->load->view("company", $data);
    }

    public function Get_dataformdate($stdate = '', $enddate = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['transection_data'] = $this->superadminmodal->getDatafromdate($stdate, $enddate);
        $data['stdate'] = $stdate;
        $data['enddate'] = $enddate;
        $data['gat_way'] = '2';
        $data['pagename'] = "company/Transection";
        $this->load->view("company", $data);
    }

    public function Get_dataformdate_for_all_bookingspg($stdate = '', $enddate = '', $status = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->getDatafromdate_for_all_bookings($stdate, $enddate, $status);
    }

    public function search_by_select($selectdval = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->getDataSelected($selectdval);
    }

    public function profile() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $sessionsetornot = $this->superadminmodal->issessionset();
        if ($sessionsetornot) {
            $data['userinfo'] = $this->superadminmodal->getuserinfo();

            $data['pagename'] = "company/profile";
            $this->load->view("company", $data);
        } else {
            redirect(base_url() . "index.php/superadmin");
        }
    }

    public function services() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $sessionsetornot = $this->superadminmodal->issessionset();
        if ($sessionsetornot) {

            $data['service'] = $this->superadminmodal->getActiveservicedata();
            $data['pagename'] = "company/Addservice";
//             var_dump($data);
            $this->load->view("company", $data);
        } else {
            redirect(base_url() . "index.php/superadmin");
        }
    }

    public function sgroup() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['business'] = array();
        $data['citylist'] = $this->superadminmodal->get_cities();

        $this->load->library('Datatables');
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:11px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => ' <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 127px;font-size:11px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('SI NO', 'CITY', 'CATEGORY NAME', 'GROUP NAME', 'SELECT');
        $data['pagename'] = 'company/sgroup';
        $this->load->view("company", $data);
    }

    public function pgateway() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['business'] = array();
        $data['citylist'] = $this->superadminmodal->get_cities();

        $this->load->library('Datatables');
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:11px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => ' <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 127px;font-size:11px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('SI NO', 'PAYMENT GATEWAY', '% FEES', 'FIXED FEES', 'SELECT');
        $data['pagename'] = 'company/pgateway';
        $this->load->view("company", $data);
    }

    public function pro_rating($proid = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['proid'] = $proid;
        $prodata = $this->superadminmodal->pro_details_get($proid);
        $data['name'] = $prodata->first_name;
        $this->load->library('Datatables');
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:11px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => ' <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 127px;font-size:11px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('BOOKING ID', 'CUSTOMER ID', 'CUSTOMER NAME', 'REVIEW', 'RATE', 'DATE');
        $data['pagename'] = 'company/pro_rating';
        $this->load->view("company", $data);
    }

    public function dispatcher() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['business'] = array();
        $data['citylist'] = $this->superadminmodal->get_cities();

        $this->load->library('Datatables');
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:11px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => ' <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 127px;font-size:11px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('BOOKING ID', 'CUSTOMER ID', 'CUSTOMER NAME', 'PROVIDER ID', 'PROVIDER NAME', 'CATEGORY', 'DATE', 'DEVICE TYPE', 'BOOKING TYPE', 'STATUS');
        $data['pagename'] = 'company/dispatcher';
        $this->load->view("company", $data);
    }

    public function promotion_details($id) {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
//        $data['business'] = array();
        $data['id'] = $id;

        $this->load->library('Datatables');
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:11px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => ' <th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 127px;font-size:11px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('INVOICE VALUE', 'DISCOUNT', 'VALUE AFTER DISCOUNT', 'USED ON', 'BOOKING ID', 'CUSTOMER ID', 'CUSTOMER NAME', 'CUSTOMER EMAIL');
        $data['pagename'] = 'company/promotionDetail';
        $this->load->view("company", $data);
    }

    public function updateservices($tablename = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $this->superadminmodal->updateservices($tablename);
        redirect(base_url() . "index.php/superadmin/services");
    }

    function Banking() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $sessionsetornot = $this->superadminmodal->issessionset();
        if ($sessionsetornot) {
            $data['pagename'] = "company/banking";
            $this->load->view("company", $data);
        } else {
            redirect(base_url() . "index.php/superadmin");
        }
    }

    public function addservices() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $data['service'] = $this->superadminmodal->addservices();
        redirect(base_url() . "index.php/superadmin/services");
    }

    public function booking() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $sessionsetornot = $this->madmin->issessionset();
        if ($sessionsetornot) {
            $data['bookinlist'] = $this->madmin->getPassangerBooking();
            $data['pagename'] = "booking";
            $this->load->view("index", $data);
        } else {
            redirect(base_url() . "index.php/superadmin");
        }
    }

    function Logout() {

        $this->session->sess_destroy();
        redirect(base_url() . "index.php/superadmin");
    }

    function udpadedataProfile() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->updateDataProfile();

        if ($this->input->post('val')) {
            $filename = "demo.png";
            if (move_uploaded_file($_FILES['userfile']['tmp_name'], base_url() . 'files/' . $filename)) {
                echo $filename;
            }
        }
        redirect(base_url() . "index.php/superadmin/profile");
    }

    function udpadedata($IdToChange = '', $databasename = '', $db_field_id_name = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $this->madmin->updateData($IdToChange, $databasename, $db_field_id_name);
        redirect(base_url() . "index.php/superadmin/profile");
    }

    public function updateMasterBank() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $ret = $this->superadminmodal->updateMasterBank();
        $data['error'] = $ret['flag'];
        $data['error_message'] = $ret['message'];
        $data['error_array'] = $ret;
        $data['userData'] = $ret['data'];
        $data['pagename'] = "master/banking";
        $this->load->view("master", $data);
    }

    public function payroll() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['pagename'] = 'company/payroll';

        $this->load->library('Datatables');
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 50px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );

        $this->table->set_template($tmpl);

        $this->table->set_heading('PROVIDER ID', 'NAME', 'OPENING BALANCE', 'BOOKING THIS CYCLE', 'EARNING THIS CYCLE', 'CLOSING BALANCE', 'SHOW');
        $this->load->view("company", $data);
    }

    public function payroll_ajax() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->payroll();
    }

    public function payroll_data_form_date($stdate = '', $enddate = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->payroll_data_form_date($stdate, $enddate);
    }

    public function patientDetails_form_Date($stdate = '', $enddate = '', $doc_id = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->patientDetails_form_Date($stdate, $enddate, $doc_id);
    }

    public function Provider_Pay($proid = '') {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['duebalance'] = $this->superadminmodal->Provider_Pay($proid);
    }

    public function Driver_pay($id = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['duebalance'] = $this->superadminmodal->Provider_Pay($id);
        $data['driverdata'] = $this->superadminmodal->Driver_pay($id);
        $data['payrolldata'] = $this->superadminmodal->get_payrolldata($id);
        $data['totalamountpaid'] = $this->superadminmodal->Totalamountpaid($id);
        $data['mas_id'] = $id;
//        print_r($data);
//        exit();
        $data['pagename'] = 'company/driverpayment';
        $this->load->view("company", $data);
    }

    public function pay_provider($id = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->pay_provider($id);
        $data['mas_id'] = $id;
        $data['pagename'] = 'company/pay_provider';
//        echo "<pre>";
//        print_r($data);
//        exit();
        $this->load->view("company", $data);
    }

    public function pay_driver_amount($id = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $res = $this->superadminmodal->insert_payment($id);

        if ($res['error'] != '') {
            $meta = array('pay_error' => $res['error']);
            $this->session->set_userdata($meta);
        }
//        print_r($res);
//        exit();
//        print_r($res);
        redirect(base_url() . "index.php/superadmin/pay_provider/" . $id);
    }

    public function collect_driver_amount($id = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $res = $this->superadminmodal->collect_driver_amount($id);
        if ($res['error'] != '') {
            $meta = array('pay_error' => $res['error']);
            $this->session->set_userdata($meta);
        }
        redirect(base_url() . "index.php/superadmin/pay_provider/" . $id);
    }

    public function validateEmail() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        return $this->superadminmodal->validateEmail();
    }

    public function validatedispatchEmail() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        return $this->superadminmodal->validatedispatchEmail();
    }

    public function AddNewDriverData() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->AddNewDriverData();
        $this->superadminmodal->updateeducation();
        redirect(base_url() . "index.php/superadmin/Drivers/my/1");
    }

    public function editdriverdata() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $this->superadminmodal->editdriverdata();
        $this->superadminmodal->updateeducation();

        redirect(base_url() . "index.php/superadmin/Drivers/my/1");
    }

    public function AddNewVehicleData() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->AddNewVehicleData();
        redirect(base_url() . "index.php/superadmin/Vehicles/5");
    }

    public function ProRechargeList() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['gat_way'] = "2";
        $data['pagename'] = "company/ProRechargeList";
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->table->clear();

        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:20px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 127px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('SLNO', 'PROVIDER ID', 'PROVIDER NAME', 'CURRENT BALANCE', 'LAST RECHARGE DATE', 'OPERATION');
        $this->load->view("company", $data);
    }

    public function GetRechargedata_ajax($param) {

        $this->superadminmodal->GetRechargedata_ajax();
    }

    public function ProRechargeStatement($id) {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['driverId'] = $id;
        $data['driverinfo'] = $this->superadminmodal->GetProDetils($id);
        $data['pagename'] = "company/ProRechargeStatement";
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->table->clear();
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:20px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 127px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('OPENING BALANCE', 'BOOKINGID', 'COMISSION', 'COLSING BALANCE');
        $this->load->view("company", $data);
    }

    public function Recharge($id) {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['driverId'] = $id;
        $data['driverinfo'] = $this->superadminmodal->GetProDetils($id);
        $data['pagename'] = "company/ProRechargeDetails";
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->table->clear();
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:20px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 127px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('SLNO', 'RECHARGE AMOUNT', 'RECHARGE DATE', 'OPERATION');
        $this->load->view("company", $data);
    }

    public function ProRechargeDetails_ajax($param) {

        $this->superadminmodal->ProRechargeDetails($param);
    }

    public function RechargeOperation($for, $id, $masid = '') {

        $data = $this->superadminmodal->RechargeOperation($for, $id, $masid);
        if ($data == 44)
            redirect(base_url() . "index.php/superadmin/Recharge/" . $masid);
    }

    public function ProRechargeStatement_ajax($param) {
        $this->superadminmodal->ProRechargeStatement($param);
    }

    public function ProRecharge() {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['gat_way'] = "2";
        $data['pagename'] = "company/ProRechargeList";
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->table->clear();

        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr style= "font-size:20px"role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 127px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('SLNO', 'PROVIDER ID', 'PROVIDER NAME', 'CURRENT BALANCE', 'LAST RECHARGE DATE', 'OPERATION');
        $this->load->view("company", $data);
    }

    public function patientDetails($pay_id = '', $pro_id = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->cycleDetails($pay_id, $pro_id);
        $data['pagename'] = 'company/patientDetails';
        $data['pay_id'] = $pay_id;
        $data['pro_id'] = $pro_id;
        $this->load->library('Datatables');
        $this->load->library('table');
        $tmpl = array('table_open' => '<table id="big_table" border="1" cellpadding="2" cellspacing="1" class="table table-hover demo-table-search dataTable no-footer" id="tableWithSearch" role="grid" aria-describedby="tableWithSearch_info" style="margin-top: 30px;">',
            'heading_row_start' => '<tr role="row">',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th class="sorting" tabindex="0" aria-controls="tableWithSearch" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 50px;">',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );

        $this->table->set_template($tmpl);
//        $this->table->set_heading('BOOKING ID', 'PAYMENT METHOD', 'BOOKING DATE', 'TOTAL SERVICE FEES EARNING', 'OTHER EARNINGS', 'PROVIDER NET EARNING', 'CASH COLLECTED', 'CASH DUE', 'PAYMENT STATUS');
        $this->table->set_heading('BOOKING ID', 'PAYMENT METHOD', 'BOOKING DATE', 'TOTAL SERVICE FEES EARNING', 'OTHER EARNINGS', 'PROVIDER NET EARNING', 'CASH COLLECTED', 'CASH DUE');
        $this->load->view("company", $data);
    }

    public function patientDetails_ajax($pay_id = '', $pro_id) {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $this->superadminmodal->patientDetails($pay_id, $pro_id);
    }

    public function revDetails($pay_id = '', $pro_id) {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->revDetails($pay_id, $pro_id);
        $data['mas_id'] = $pro_id;
        $data['pay_id'] = $pay_id;
        $data['pagename'] = 'company/revDetails';
        $this->load->view("company", $data);
    }

    public function acptDetails($pay_id = '', $pro_id) {
        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->acptDetails($pay_id, $pro_id);
        $data['mas_id'] = $pro_id;
        $data['pay_id'] = $pay_id;
        $data['pagename'] = 'company/acptDetails';
        $this->load->view("company", $data);
    }

    public function deletecities() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['del'] = $this->superadminmodal->deletecity();
    }

    public function deletedispatcher() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data['del'] = $this->superadminmodal->deletedispatcher();
    }

    public function deleteVehicles() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $driverlist = $this->input->post('val');
        $this->load->database();
        $this->load->library('mongo_db');
        foreach ($driverlist as $result) {


            $affectedRows = 0;

            $selectCars = $this->db->query("select appointment_id from appointment where car_id = '" . $result . "'")->result_array();

            $apptIDs = array();
            foreach ($selectCars as $type) {
                $apptIDs[] = (int) $type['appointment_id'];
            }

            $masDet = $this->db->query("select mas_id from master where workplace_id= '" . $result . "'")->result_array();


            if (is_array($masDet)) {

                $db = $this->mongo_db->db;

                $location = $db->selectCollection('location');

                $location->update(array('carId' => (int) $result), array('$set' => array('type' => 0, 'carId' => 0, 'status' => 4)), array('multiple' => 1));

                $this->db->query("update master set type_id = 0 and workplace_id = 0 where mas_id = '" . $masDet[0]['mas_id'] . "'");
            }

            $varify = implode(',', $apptIDs);
            $this->db->query("delete from workplace where workplace_id = '" . $result . "'");
            $affectedRows += $this->db->affected_rows();

            $this->db->query("delete from appointment where appointment_id in ('" . $varify . "')");
            $affectedRows += $this->db->affected_rows();

            $this->db->query("delete from passenger_rating where appointment_id in ('" . $varify . "')");
            $affectedRows += $this->db->affected_rows();

            $this->db->query("delete from master_ratings where appointment_id in ('" . $varify . "')");
            $affectedRows += $this->db->affected_rows();

            $this->db->query("delete from user_sessions where user_type = 1 and oid = '" . $masDet[0]['mas_id'] . "'");
            $affectedRows += $this->db->affected_rows();
        }
        echo json_encode(array('flag' => 0, 'affectedRows' => $affectedRows, 'message' => 'Process completed.'));
    }

    /* Stripe Bank */

    public function addNewBankDetail() {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }
        $data = $this->superadminmodal->addNewBankDetail();
        echo json_encode($data);
    }

    public function defaultBank() {
        $data = $this->superadminmodal->defaultBank();
        echo json_encode($data);
    }

    public function ajax_call_to_get_types($param = '') {

        if ($this->session->userdata('table') != 'company_info') {
            $this->Logout();
        }

        $this->load->database();
        if ($param == 'vtype') {
            $get_vechile_type_display = $this->db->query("select type_id,type_name from workplace_types where city_id = '" . $_REQUEST['city'] . "' ORDER BY type_name ASC")->result();
            echo "<option value=''>Select a type</option>";
            foreach ($get_vechile_type_display as $typelist) {
                echo "<option value='" . $typelist->type_id . "' id='" . $typelist->type_id . "' >" . $typelist->type_name . "</option>";
            }
        } else if ($param == 'vmodel') {
            $loupon_sql = $this->db->query("SELECT * FROM vehiclemodel where vehicletypeid = '" . $_REQUEST['adv'] . "' ORDER BY vehiclemodel ASC")->result();
            $options = '';
            foreach ($loupon_sql as $loupon_sql_row) {
                $options .= "<option value='" . $loupon_sql_row->id . "' id='" . $loupon_sql_row->id . "'>" . $loupon_sql_row->vehiclemodel . "</option>";
            }
            echo $options;
        } else if ($param == 'companyselect') {
            $get_company = $this->db->query("select company_id,companyname from company_info where city = '" . $this->input->post('company') . "' and status = 3")->result();
            echo " <option value=''>Select a Company  </option>";
            foreach ($get_company as $row) {
                echo "<option value='" . $row->company_id . "' id='" . $row->company_id . "' >" . $row->companyname . "</option>";
            }
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */