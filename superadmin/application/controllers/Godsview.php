<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Godsview extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
                
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
    }

   public function index() {
        echo json_encode(array('flg' => 1, 'data' => 'Operation Not Permitted.'));
    }
    public function get_all_drivers() {
        $this->load->library('mongo_db');
        $time = time();
        $allusers = $this->mongo_db->get_where('location', array("online" => 1,"status" => array('$in' => [3,5])));
        
        $all = $statusfour = $statussix = $statusseven = $statuseight = array();
        
        foreach ($allusers as $value) {
            $all[] = $value;
            switch ($value['status']) {
                case 3:
                    if($value['booked'] == 0)
                      $statusfour[] = $value;
                      break;
                case 5:
                    switch($value['apptStatus']){
                        case 5:
                            $statussix[] = $value;
                            break;
                        case 21:
                            $statusseven[] = $value;
                            break;
                        case 6:
                            $statuseight[] = $value;
                            break;
                    }
                default:
                    break;
            }
        }
        
        $data = array(
            'four' => $statusfour,
            'six' => $statussix,
            'seven' => $statusseven,
            'eight' => $statuseight,
            'all' => $all
        );
        
        echo json_encode(array('flg' => 0, 'data' => $data, 'time' => $time));
    }
    
    public function get_updated_drivers($time) {
        $this->load->library('mongo_db');
        $upd_time = time();
        $newusers = $this->mongo_db->get_where('location', array("lastTs" => array('$gte' => (double)$time)));
        
        $allusers = $this->mongo_db->get_where('location', array("status" => array('$in' => [3,5])));
        
        $all = $statusfour = $statussix = $statusseven = $statuseight = array();
        
        foreach ($allusers as $value) {
            switch ($value['status']) {
                case 3:
                     if($value['booked'] == 0)
                        $statusfour[] = $value;
                        break;
                case 5:
                    switch($value['apptStatus']){
                        case 5:
                            $statussix[] = $value;
                            break;
                        case 21:
                            $statusseven[] = $value;
                            break;
                        case 6:
                            $statuseight[] = $value;
                            break;
                    }
                default:
                    break;
            }
        }
        foreach ($newusers as $value) {
            $all[] = $value;
        }
        
        $data = array(
            'four' => $statusfour,
            'six' => $statussix,
            'seven' => $statusseven,
            'eight' => $statuseight,
            'all' => $all
        );
        
        echo json_encode(array('flg' => 0, 'data' => $data, 'time' => $upd_time));
    }
    
    public function getSpecificDriver($param) {        
        if($param != ''){
            $this->load->library('mongo_db');
            $ddata = $this->mongo_db->get_one('location',array('_id' => new MongoId($param)));
            echo json_encode(array('flg' => 0, 'data' => $ddata));
        }else{
            echo json_encode(array('flg' => 1, 'data' => 'Operation Not Permitted.'));
        }
    }
    
    public function getApptDetails($param) {  
//        $this->load->database();
        if($param != ''){
            $this->load->library('mongo_db');
            $ddata = $this->mongo_db->get_one('appointments',array("status" => array('$in' =>[6,7,8]),"mas_id" => (int)$param));
//            $query = $this->db->query("select p.*, a.* "
//                    . "from appointment a,slave p where a.slave_id=p.slave_id  and a.status IN (6,7,8) and a.mas_id = ". $param)->row_array();
            echo json_encode(array('flg' => 0, 'data' => $ddata));
        }else{
            echo json_encode(array('flg' => 1, 'data' => 'Operation Not Permitted.'));
        }
    }
    function citydata() {
        $this->load->database();
        $query = $this->db->query('select * from city_available')->result();
        echo json_encode($query);
    }
 

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */