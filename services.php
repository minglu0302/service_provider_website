<?php

ini_set("log_errors", 1);
ini_set("error_log", "/tmp/php-error.log");
$date = date("Y-m-d H:i:s");
//error_log("$date: Hello! Running script /services.php" . PHP_EOL);

require_once 'Models/config.php';
require_once 'Models/API.php';
require_once 'Models/ConDBServices.php';
require_once 'Models/getErrorMsg.php';
require_once 'Models/ManageToken.php';
require_once 'Models/StripeModule.php';
require_once 'Models/StripeModuleNew.php';
require_once 'Models/sendAMail.php';
require_once "Models/Twilio/Twilio.php";
require_once 'Models/InvoiceHtml.php';
require_once 'Models/MongoClass.php';
require 'Models/aws.phar';

class MyAPI extends API {

    protected $User;
    private $db;
    private $mongo;
    private $default_profile_pic;
    private $reviewsPageSize;
    private $stripe;
    private $curr_date_time;
    private $ios_cert_path;
    private $ios_cert_pwd;
//    private $testing;
    private $promoCodeRadius = 100;
    private $distanceMetersByUnits = APP_DISTANCE_METERS;

    public function __construct($request_uri, $postData, $origin) {

        parent::__construct($request_uri, $postData);
        $this->db = new ConDBServices();
        $this->mongo = $this->db->mongo;
        $this->stripe = new StripeModule(STRIPE_SECRET_KEY);
        $this->default_profile_pic = DEFAULT_PROFILE_PIC;
        $this->reviewsPageSize = REVIEW_PAGE_SIZE;
        $this->MongoClass = new MongoClass($this->db->mongo);
    }

    /*
     * Method name: masterSignup
     * Desc: Doctor Sign up for the app step 1
     * Input: Request data
     * Output: Success flag with data array if completed successfully, else data array with error flag
     */

    protected function masterSignup($args) {
        if ($args['ent_device_type_arr'] == '' || $args['ent_fees_group'] == '' || $args['ent_first_name'] == '' || $args['ent_email'] == '' || $args['ent_password'] == '' || $args['ent_mobile'] == '' || $args['ent_dev_id'] == '' ||
                $args['ent_push_token'] == '' || $args['ent_device_type'] == '' || $args['ent_zipcode'] == '' || $args['ent_date_time'] == '' || $args['ent_city_id'] == '')
            return $this->_getStatusMessage(1, 1);
        $this->curr_date_time = urldecode($args['ent_date_time']);

        $devTypeNameArr = $this->_getDeviceTypeName($args['ent_device_type']);
        if (!$devTypeNameArr['flag'])
            return $this->_getStatusMessage(4, 4);

        $verifyEmail = $this->_verifyEmail($args['ent_email'], 'doc_id', 'doctor');
        if (is_array($verifyEmail))
            return $this->_getStatusMessage(2, 2);

        $verifyMobile = $this->_verifyMobile($args['ent_mobile'], 'doc_id', 'doctor');
        if (is_array($verifyMobile))
            return $this->_getStatusMessage(129, 2);

        $token_obj = new ManageToken($this->db);
        $location = $this->mongo->selectCollection('location');
        $location->ensureIndex(array("location" => "2d"));
        $args['ent_first_name'] = ucfirst($args['ent_first_name']);
        $args['ent_last_name'] = ucfirst($args['ent_last_name']);

        $insertMasterQry = "insert into 
                        doctor(first_name,last_name,email,password,country_code,mobile,fees_group,
                        zipcode,created_dt,last_active_dt,status,amount,medical_license_num,profile_pic,app_version,city_id) 
                        values('" . $args['ent_first_name'] . "','" . $args['ent_last_name'] . "','" . $args['ent_email'] . "',md5('" . $args['ent_password'] . "'),'" . $args['ent_country_code'] . "', '" . $args['ent_mobile'] . "','" . $args['ent_fees_group'] . "',
                            '" . $args['ent_zipcode'] . "','" . $args['ent_date_time'] . "','" . $args['ent_date_time'] . "','1','0','" . $args['ent_license_num'] . "','" . $args['ent_pro_image'] . "','" . $args['ent_app_version'] . "','" . $args['ent_city_id'] . "')";




        mysql_query($insertMasterQry, $this->db->conn);
        if (mysql_error($this->db->conn) != '')
            return $this->_getStatusMessage(3, $insertMasterQry);

        $newDoctor = mysql_insert_id($this->db->conn);
        if ($newDoctor <= 0)
            return $this->_getStatusMessage(3, 4);
        $curr_gmt_date = new MongoDate(strtotime($this->curr_date_time));

        //select all category for sending mail
        $sel_cat_name = array();
        $ProviderCategoryColl = $this->mongo->selectCollection('Category');
        $CatList = $ProviderCategoryColl->find();
        $allcatids = array();
        $allcatPrice = array();
        foreach ($CatList as $cat) {
            $cid = (string) $cat['_id'];
            $allcatids[$cid] = $cat['cat_name'];
            $allcatPrice[$cid] = $cat['price_min'];
        }

        $tarr = explode(",", $args['ent_device_type_arr']);

        $sel_cat = array();

        foreach ($tarr as $type) {
            $sel_cat[] = array('cid' => $type, 'status' => 0, 'amount' => $allcatPrice[$type]);
            $sel_cat_name[] = $allcatids[$type];
            $firstcatid = $type;
        }

        if ((!isset($args['ent_pro_image'])) || $args['ent_pro_image'] == null || $args['ent_pro_image'] == "")
            $args['ent_pro_image'] = "";

        $mongoArr = array("user" => (int) $newDoctor, "name" => $args['ent_first_name'], "lname" => $args['ent_last_name'],
            "location" => array(
                "longitude" => (float) $args['ent_longitude'],
                "latitude" => (float) $args['ent_latitude']
            ), "image" => $args['ent_pro_image'], "rating" => 0, 'status' => 1, 'email' => strtolower($args['ent_email']),
            'dt' => $curr_gmt_date->sec, 'later_status' => 1, 'mobile' => $args['ent_mobile'],
            'login' => 1, 'radius' => 10, 'devId' => $args['ent_dev_id'],
            'fee_group' => $args['ent_fees_group'],
            'accepted' => 0, 'Commission' => 10,
            'booked' => 0,
            'popup' => 0,
            'push_token' => $args['ent_push_token'],
            'device_type' => $args['ent_device_type'],
            'image' => $args['ent_pro_image'], 'catlist' => $sel_cat
        );

        $catdata = $ProviderCategoryColl->findOne(array('_id' => new MongoId($firstcatid)));

        $searchMasterQry = "select * from city where City_Id = '" . $catdata['city_id'] . "'";
        $searchMasterRes = mysql_query($searchMasterQry, $this->db->conn);
        $masterRow = mysql_fetch_assoc($searchMasterRes);
        $cityname = $masterRow['City_Name'];

        $location->insert($mongoArr);
        $mail = new sendAMail(APP_SERVER_HOST);
        $mailArr = $mail->sendMasWelcomeMail($args['ent_email'], $args['ent_first_name'], $sel_cat_name, $cityname);
        $createSessArr = $this->_checkSession($args, $newDoctor, '1', $devTypeNameArr['name']);

        if (!isset($createSessArr['Flag']))
            return $createSessArr;

        $errMsgArr = $this->_getStatusMessage(12, 5);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'], 'token' => $createSessArr['Token'], 'proid' => $newDoctor, 'expiryLocal' => $createSessArr['Expiry_local'], 'expiryGMT' => $createSessArr['Expiry_GMT'], 'flag' => $createSessArr['Flag'], 'joined' => $args['ent_date_time'],
            'serverChn' => SERVER_CHN, 'email' => $args['ent_email'], 'mail' => $mailArr['flag'], 'name' => ucfirst($args['ent_first_name']) . ' ' . ucfirst($args['ent_last_name']), 'fName' => ucfirst($args['ent_first_name']), 'lName' => ucfirst($args['ent_last_name']), 'phone' => $args['ent_mobile']);
    }

    /*
     * Method name: masterLogin
     * Desc: Doctor login on the app
     * Input: Request data
     * Output:  Success flag with data array if completed successfully, else data array with error flag
     */

    protected function masterLogin($args) {
        if ($args['ent_email'] == '' || $args['ent_password'] == '' || $args['ent_dev_id'] == '' || $args['ent_push_token'] == '' || $args['ent_device_type'] == '' || $args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, 6); //_getStatusMessage($errNo, $test_num);

        $this->curr_date_time = urldecode($args['ent_date_time']);

        $devTypeNameArr = $this->_getDeviceTypeName($args['ent_device_type']);

        if (!$devTypeNameArr['flag'])
            return $this->_getStatusMessage(5, 108);

        $searchDoctorQry = "select md5('" . $args['ent_password'] . "') as given_password,email,password,doc_id,profile_pic,first_name,created_dt,fees_group,medical_license_num,status,mobile,type_id from doctor where email = '" . $args['ent_email'] . "'  or mobile = '" . $args['ent_email'] . "'";
        $searchDoctorRes = mysql_query($searchDoctorQry, $this->db->conn);

        if (mysql_num_rows($searchDoctorRes) <= 0)
            return $this->_getStatusMessage(120, 7);

        $doctorRow = mysql_fetch_assoc($searchDoctorRes);

        if ($doctorRow['password'] !== $doctorRow['given_password'])
            return $this->_getStatusMessage(91, 18);

        if ($doctorRow['status'] == '4')
            return $this->_getStatusMessage(76, 76);

        if ($doctorRow['status'] == '1')
            return $this->_getStatusMessage(122, 76);

        $message = "You just logged out of this mobile as you just logged into another.";
        $aplPushContent = array('payload' => array('alert' => $message, 'btype' => '19'));
        $andrPushContent = array("payload" => $message, 'action' => '19');
//        $push = $this->_sendPush('0', array($doctorRow['doc_id']), $message, '19', $doctorRow['first_name'], $this->curr_date_time, '1', $aplPushContent, $andrPushContent);

        $sessDet = $this->_checkSession($args, $doctorRow['doc_id'], '1', $devTypeNameArr['name']);

        if (!isset($sessDet['Flag']))
            return $sessDet;

        if ($args['ent_app_version'] != '') {
            $updateAppVerQry = "update doctor set app_version = '" . $args['ent_app_version'] . "' where doc_id = '" . $doctorRow['doc_id'] . "'";
            mysql_query($updateAppVerQry, $this->db->conn);
        }

        $location = $this->mongo->selectCollection('location');
        if ($args['ent_latitude'] != '' && $args['ent_longitude'] != '')
            $location->update(array('user' => (int) $doctorRow['doc_id']), array('$set' => array("location" => array("longitude" => (double) $args['ent_longitude'], "latitude" => (double) $args['ent_latitude']), 'device_type' => $args['ent_device_type'], 'push_token' => $args['ent_push_token'], 'login' => 1, 'devId' => $args['ent_dev_id'], 'type' => (int) $doctorRow['type_id'])));
        else
            $location->update(array('user' => (int) $doctorRow['doc_id']), array('$set' => array('login' => 1, 'devId' => $args['ent_dev_id'], 'push_token' => $args['ent_push_token'], 'device_type' => $args['ent_device_type'], 'type' => (int) $doctorRow['type_id'])));

        $argstopass = array('proId' => $doctorRow['doc_id']);
        $this->sendDatatoSocket($argstopass, "location");

        $errMsgArr = $this->_getStatusMessage(9, 8);
        return array('errNum' => $errMsgArr['errNum'], 'proid' => $doctorRow['doc_id'],
            'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'], 'token' => $sessDet['Token'],
            'expiryLocal' => $sessDet['Expiry_local'], 'expiryGMT' => $sessDet['Expiry_GMT'],
            'profilePic' => $doctorRow['profile_pic'], 'medicalLicenseNum' => $doctorRow['medical_license_num'],
            'flag' => $sessDet['Flag'], 'joined' => $doctorRow['created_dt'], 'serverChn' => SERVER_CHN,
            'email' => $doctorRow['email'], 'name' => $doctorRow['first_name'] . ' ' . $doctorRow['last_name'],
            'fName' => $doctorRow['first_name'], 'lName' => $doctorRow['last_name'], 'phone' => $doctorRow['mobile'],
            'typeId' => $doctorRow['type_id'],
            'Languages' => $this->getLangueage(),
            'fees_group' => $doctorRow['fees_group']);
    }

    /*
     * Method name: slaveSignup
     * Desc: Patient signup
     * Input: Request data
     * Output:  Success flag with data array if completed successfully, else data array with error flag
     */

    protected function slaveSignup($args) {
        // ent_signup_type = 1  = NORMAL
        // ent_signup_type = 2  = FACEBOOK
        // ent_signup_type = 3  = GOOGEL

        $fb = $this->mongo->selectCollection('fb');
        $fb->insert(array('signupparam' => $args));

        $errMsgArr = $this->_getStatusMessage(1, 1);
        if ($args['ent_first_name'] == '') {
            return array('errMsg' => "First Name Missing", 'errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag']);
        }
        if ($args['ent_email'] == '') {
            return array('errMsg' => "Email Id Missing", 'errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag']);
        }
        if ($args['ent_latitude'] == '') {
            return array('errMsg' => "latitude Missing", 'errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag']);
        }
        if ($args['ent_longitude'] == '') {
            return array('errMsg' => "Longitude Missing", 'errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag']);
        }
        if ($args['ent_password'] == '') {
            return array('errMsg' => "Password Missing", 'errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag']);
        }
        if ($args['ent_mobile'] == '') {
            return array('errMsg' => "Mobile Missing", 'errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag']);
        }
        if ($args['ent_dev_id'] == '') {
            return array('errMsg' => "Device Id Missing", 'errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag']);
        }
        if ($args['ent_device_type'] == '') {
            return array('errMsg' => "Device Type Missing", 'errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag']);
        }
        if ($args['ent_country_code'] == '') {
            return array('errMsg' => "Country Code Missing", 'errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag']);
        }
        if ($args['ent_date_time'] == '') {
            return array('errMsg' => "Date Time Missing", 'errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag']);
        }

        $this->curr_date_time = urldecode($args['ent_date_time']);

        if ($args['ent_terms_cond'] == '0')
            return $this->_getStatusMessage(14, 14);

        if ($args['ent_pricing_cond'] == '0')
            return $this->_getStatusMessage(15, 15);

        if ($args['ent_latitude'] == '')
            $args['ent_latitude'] = '0';

        if ($args['ent_longitude'] == '')
            $args['ent_longitude'] = '0';

        $devTypeNameArr = $this->_getDeviceTypeName($args['ent_device_type']);

        if (!$devTypeNameArr['flag'])
            return $this->_getStatusMessage(4, 4);

        $verifyEmail = $this->_verifyEmail($args['ent_email'], 'patient_id', 'patient');
        if (is_array($verifyEmail))
            return $this->_getStatusMessage(2, 2);

        $insertSlaveQry = "insert into 
                        patient(first_name,last_name,profile_pic,email,password,country_code,phone,zipcode,status,
                        created_dt,last_active_dt,latitude,longitude, app_version,signup_type,fb_id) 
                        values('" . $args['ent_first_name'] . "','" . $args['ent_last_name'] . "','" . $args['ent_profile_pic'] . "','" . $args['ent_email'] . "',md5('" . $args['ent_password'] . "'), '" . $args['ent_country_code'] . "','" . $args['ent_mobile'] . "','" . $args['ent_zipcode'] . "','3',
                                '" . $this->curr_date_time . "','" . $this->curr_date_time . "','" . $args['ent_latitude'] . "','" . $args['ent_longitude'] . "','" . $args['ent_app_version'] . "' ,'" . $args['ent_signup_type'] . "','" . $args['ent_fbid'] . "')";

        mysql_query($insertSlaveQry, $this->db->conn);

        if (mysql_error($this->db->conn) != '')
            return $this->_getStatusMessage(3, $insertSlaveQry);

        $newPatient = mysql_insert_id($this->db->conn);

        // insert customenr To MongoDB
        $customer = $this->mongo->selectCollection('customer');
        $cust = array('cid' => $newPatient, 'push_token' => $args['ent_push_token'], 'device_type' => $args['ent_device_type'], 'fname' => $args['ent_first_name'], 'lname' => $args['ent_last_name'], 'email' => $args['ent_email']);
        $customer->insert($cust);

        if ($newPatient <= 0)
            return $this->_getStatusMessage(3, 3);


        $cardRes = array('errNum' => 16, 'errFlag' => 1, 'errMsg' => 'Card not added.', 'test' => 2);
        $mailArr = array();
        $mail = new sendAMail(APP_SERVER_HOST);
        $referralUsageMsg = $couponId = "";
        $couponsColl = $this->mongo->selectCollection('coupons');
        $resultArr = $this->mongo->selectCollection('$cmd')->findOne(array(
            'geoNear' => 'coupons',
            'near' => array(
                (double) $args['ent_longitude'], (double) $args['ent_latitude']
            ), 'spherical' => true, 'maxDistance' => 100000 / 6378137, 'distanceMultiplier' => 6378137,
            'query' => array('status' => 0, 'coupon_code' => 'REFERRAL'))
        );
        if (count($resultArr['results']) > 0) {

            $referralData = $resultArr['results'][0]['obj'];

            $friendDiscCode = $friendReferred = "";

            $coupon = $this->_createCoupon($couponsColl);

            if ($args['ent_referral_code'] != '') {

                $referralArr = $this->mongo->selectCollection('$cmd')->findOne(array(
                    'geoNear' => 'coupons',
                    'near' => array(
                        (double) $args['ent_longitude'], (double) $args['ent_latitude']
                    ), 'spherical' => true, 'maxDistance' => 100000 / 6378137, 'distanceMultiplier' => 6378137,
                    'query' => array('coupon_code' => (string) $args['ent_referral_code'], 'coupon_type' => 1, 'user_type' => 1, 'status' => 0))
                );

                if (count($referralArr['results']) > 0) {

                    $couponData = $referralArr['results'][0]['obj'];

                    $discountCoupon = $this->_createCoupon($couponsColl);

                    $friendReferred = $couponData['user_id'];

                    $insertArr = array(
                        "coupon_code" => $discountCoupon,
                        "coupon_type" => 3,
                        "start_date" => strtotime($this->curr_date_time),
                        "expiry_date" => strtotime($this->curr_date_time) + (10 * 30 * 24 * 60 * 60),
                        "discount_type" => $couponData['referral_discount_type'],
                        "discount" => $couponData['referral_discount'],
                        "message" => $couponData['message'],
                        "status" => 0,
                        "city_id" => $couponData['city_id'],
                        "location" => $couponData['location'],
                        "user_type" => 1,
                        "user_id" => (string) $couponData['user_id'],
                        "email" => $couponData['email'],
                        'created_ts' => strtotime($this->curr_date_time)
                    );

                    $couponsColl->insert($insertArr);

                    if ($insertArr['_id'] != '') {

                        $friendDiscCode = $this->_createCoupon($couponsColl);

                        $discCouponNewUser = array(
                            "coupon_code" => $friendDiscCode,
                            "coupon_type" => 3,
                            "start_date" => strtotime($this->curr_date_time),
                            "expiry_date" => strtotime($this->curr_date_time) + (10 * 30 * 24 * 60 * 60),
                            "discount_type" => $couponData['discount_type'],
                            "discount" => $couponData['discount'],
                            "message" => $couponData['message'],
                            "status" => 0,
                            "city_id" => $couponData['city_id'],
                            "location" => $couponData['location'],
                            "user_type" => 1,
                            "user_id" => (string) $newPatient,
                            "email" => $args['ent_email'],
                            'created_ts' => strtotime($this->curr_date_time)
                        );

                        $couponsColl->insert($discCouponNewUser);

                        $mailArr[] = $mail->discountOnFriendSignup($couponData['email'], $couponData['first_name'], array('code' => $discountCoupon, 'discountData' => $couponData, 'uname' => $args['ent_first_name']));
                        $cond = array('_id' => $referralData['_id'], 'signups.coupon_code' => $args['ent_referral_code']);
                        $args['code'] = $discountCoupon;
                        $args['discountData'] = $couponData;
                        $push = array('$push' => array('signups.$.discounts' => array('discount_code' => $discountCoupon, 'signedUpUser' => array('slave_id' => $newPatient, 'discount_code' => $friendDiscCode, 'email' => $args['ent_email'], 'referral' => $coupon, 'created_ts' => strtotime($this->curr_date_time)))));

                        $couponsColl->update($cond, $push);
                    } else {
                        $error = $this->_getStatusMessage(100, 100);
                        $referralUsageMsg = $error['errMsg'];
                    }
                } else {
                    $error = $this->_getStatusMessage(100, 2);
                    $referralUsageMsg = $error['errMsg'];
                }
            }

            $insertReferralArr = array(
                "coupon_code" => $coupon,
                "coupon_type" => 1,
                "start_date" => strtotime($this->curr_date_time),
                "expiry_date" => strtotime($this->curr_date_time) + (10 * 365 * 24 * 60 * 60),
                "discount_type" => $referralData['discount_type'],
                "discount" => $referralData['discount'],
                "referral_discount_type" => $referralData['referral_discount_type'],
                "referral_discount" => $referralData['referral_discount'],
                "message" => $referralData['message'],
                "status" => 0,
                "city_id" => $referralData['city_id'],
                "currency" => $referralData['currency'],
                "location" => $referralData['location'],
                "user_type" => 1,
                "user_id" => $newPatient,
                "email" => $args['ent_email'],
                'created_ts' => strtotime($this->curr_date_time)
            );

            $data = array('coupon_code' => $coupon, 'slave_id' => $newPatient, 'email' => $args['ent_email'], 'fname' => $args['ent_first_name'], 'created_dt' => strtotime($this->curr_date_time));

            $couponsColl->update(array('_id' => $referralData['_id']), array('$push' => array('signups' => $data)));


            $couponsColl->insert($insertReferralArr);

            if ($insertReferralArr['_id'] > 0) {
//                    $referralCode = array('code' => $coupon, 'referralData' => $referralData);
                $couponId = $coupon;

                $updateCouponQry = "update patient set coupon = '" . $coupon . "' where patient_id = '" . $newPatient . "'";
                mysql_query($updateCouponQry, $this->db->conn);
                // $mailArr[] = $mail->sendDiscountCoupon($args['ent_email'], $args['ent_first_name'], array('code' => $friendDiscCode, 'refCoupon' => $coupon, 'discountData' => $referralData));
            }
        }
        $args['ref_code'] = $couponId;
        $args['use_code'] = $friendDiscCode;
        $mailArr[] = $mail->sendSlvWelcomeMail($args['ent_email'], $args['ent_first_name'], $args);
        $createSessArr = $this->_checkSession($args, $newPatient, '2', $devTypeNameArr['name']);
        $errMsgArr = $this->_getStatusMessage(5, 5);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'],
            'errMsg' => $errMsgArr['errMsg'], 'coupon' => $couponId, 'codeErr' => $referralUsageMsg,
            'token' => $createSessArr['Token'], 'email' => $args['ent_email'], 'serverChn' => SERVER_CHN,
            "CustId" => $newPatient,
            'apiKey' => API_KEY,
            'stripe_live' => PAYMENT_GATEWAY_LIVE,
            'stripe_pub_key' => STRIPE_PUBLISHABLE_KEY,
            'gateway_name' => PAYMENT_GATEWAY_NAME,
            'currency' => CURRENCY,
            'distance' => APP_DISTANCE,
            'Languages' => $this->getLangueage());
    }

    /*
     * Method name: slaveLogin
     * Desc: Patient login on the app
     * Input: Request data
     * Output:  Success flag with data array if completed successfully, else data array with error flag
     */

    protected function testPaypal($args) {
        $paypalCOll = $this->mongo->selectCollection('paypal');
        $paypalCOll->insert(array('args' => $args));
        $errMsgArr = $this->_getStatusMessage(5, 5);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'],
            'errMsg' => $errMsgArr['errMsg']);
    }

    protected function slaveLogin($args) {

        if ($args['ent_email'] == '' || $args['ent_password'] == '' || $args['ent_dev_id'] == '' || $args['ent_push_token'] == '' || $args['ent_device_type'] == '' || $args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, 6); //_getStatusMessage($errNo, $test_num);

        $this->curr_date_time = urldecode($args['ent_date_time']);

        $devTypeNameArr = $this->_getDeviceTypeName($args['ent_device_type']);


        if (!$devTypeNameArr['flag'])
            return $this->_getStatusMessage(5, 108);


        //code for fb
//        if ($args['ent_signup_type'] == 2) {
//            $pass = MD5($args['ent_password']);
//            $searchPatientQry = "select MD5('" . $args['ent_password'] . "') as given_password,patient_id,password,profile_pic,first_name,last_name,created_dt,status,stripe_id,coupon,email from patient where fb_id = '" . $pass . "' and signup_type = '2' ";
//            $searchPatientRes = mysql_query($searchPatientQry, $this->db->conn);
//        } else {
        $searchPatientQry = "select MD5('" . $args['ent_password'] . "') as given_password,fb_id,signup_type,patient_id,password,profile_pic,first_name,last_name,created_dt,status,stripe_id,coupon,email from patient where email = '" . $args['ent_email'] . "'   or phone = '" . $args['ent_email'] . "'";
        $searchPatientRes = mysql_query($searchPatientQry, $this->db->conn);
//        }
        //end code for fb


        if (mysql_num_rows($searchPatientRes) <= 0)
            return $this->_getStatusMessage(120, 7);

        $patientRow = mysql_fetch_assoc($searchPatientRes);

        if ($args['ent_signup_type'] == 1) {
            if ($patientRow['password'] !== $patientRow['given_password'])
                return $this->_getStatusMessage(91, 18);
        }else {
            if ($args['ent_password'] !== $patientRow['fb_id'])
                return $this->_getStatusMessage(91, 12);
        }

        if ($patientRow['status'] == '1')
            return $this->_getStatusMessage(11, 18);

        if ($args['ent_app_version'] != '') {
            $updateAppVerQry = "update patient set app_version = '" . $args['ent_app_version'] . "' where patient_id = '" . $patientRow['patient_id'] . "'";
            mysql_query($updateAppVerQry, $this->db->conn);
        }


        $cardsArr = array();
        $customer = $this->mongo->selectCollection('customer');
        $customer->update(array('cid' => (int) $patientRow['patient_id']), array('$set' => array('push_token' => $args['ent_push_token'], 'device_type' => $args['ent_device_type'])));
        //start stipe card retrive
        if ($patientRow['stripe_id'] != '') {
            $getCardArr = array('stripe_id' => $patientRow['stripe_id']);
            $card = $this->stripe->apiStripe('getCustomer', $getCardArr);
            if ($card['error'])
                $cardsArr = array();
            else
                foreach ($card['sources']['data'] as $c) {
                    $cardsArr[] = array('errFlag' => 0, 'errMsg' => 'Card added.', 'id' => $c['id'], 'last4' => $c['last4'], 'type' => $c['brand'], 'exp_month' => $c['exp_month'], 'exp_year' => $c['exp_year']);
                }
        } else {
            $cardsArr = array(); //array('errNum' => 16, 'errFlag' => 1, 'errMsg' => 'No cards');
        }
        //End  stripe card retrive


        $couponId = $patientRow['coupon'];

        if ($patientRow['coupon'] == '') {

            $resultArr = $this->mongo->selectCollection('$cmd')->findOne(array(
                'geoNear' => 'coupons',
                'near' => array(
                    (double) $args['ent_longitude'], (double) $args['ent_latitude']
                ), 'spherical' => true, 'maxDistance' => 100000 / 6378137, 'distanceMultiplier' => 6378137,
                'query' => array('status' => 0, 'coupon_code' => 'REFERRAL'))
            );

            if (count($resultArr['results']) > 0) {

                $referralData = $resultArr['results'][0]['obj'];
                $couponsColl = $this->mongo->selectCollection('coupons');
                $coupon = $this->_createCoupon($couponsColl);
                $insertReferralArr = array(
                    "coupon_code" => $coupon,
                    "coupon_type" => 1,
                    "start_date" => strtotime($this->curr_date_time),
                    "expiry_date" => strtotime($this->curr_date_time) + (10 * 365 * 24 * 60 * 60),
                    "discount_type" => $referralData['discount_type'],
                    "discount" => $referralData['discount'],
                    "referral_discount_type" => $referralData['referral_discount_type'],
                    "referral_discount" => $referralData['referral_discount'],
                    "message" => $referralData['message'],
                    "status" => 0,
                    "city_id" => $referralData['city_id'],
                    "location" => $referralData['location'],
                    "user_type" => 1,
                    "user_id" => $patientRow['patient_id']
                );

                $data = array('referral_code' => $coupon, 'slave_id' => $patientRow['patient_id'], 'email' => $args['ent_email'], 'fname' => $patientRow['first_name']);

                $couponsColl->update(array('_id' => $referralData['_id']), array('$push' => array('signups' => $data)));

                $couponsColl->insert($insertReferralArr);
                if ($insertReferralArr['_id'] > 0) {
                    $couponId = $coupon;

                    $updateCouponQry = "update patient set coupon = '" . $coupon . "' where patient_id = '" . $patientRow['patient_id'] . "'";
                    mysql_query($updateCouponQry, $this->db->conn);
                }
            }
        }


        $sessDet = $this->_checkSession($args, $patientRow['patient_id'], '2', $devTypeNameArr['name']); //_checkSession($args, $oid, $user_type);

        $errMsgArr = $this->_getStatusMessage(9, 8);

        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'],
            'errMsg' => $errMsgArr['errMsg'], 'coupon' => $couponId,
            'fname' => $patientRow['first_name'], 'lname' => $patientRow['last_name'],
            'token' => $sessDet['Token'], 'email' => $patientRow['email'],
            'serverChn' => SERVER_CHN, 'profilePic' => $patientRow['profile_pic'],
            'cards' => $cardsArr,
            'apiKey' => API_KEY,
            'stripe_live' => PAYMENT_GATEWAY_LIVE,
            'stripe_pub_key' => STRIPE_PUBLISHABLE_KEY,
            'gateway_name' => PAYMENT_GATEWAY_NAME,
            'currency' => CURRENCY,
            'distance' => APP_DISTANCE,
            'Languages' => $this->getLangueage(),
            'CustId' => $patientRow['patient_id']);
    }

    protected function getVerificationCode($args) {

// 		var_dump($args);
		
        if ($args['ent_phone'] == '')
            return $this->_getStatusMessage(1, 1);
		
        $rand = rand(1000, 9999);
//        $rand = 1111;
        $client = new Services_Twilio(ACCOUNT_SID, AUTH_TOKEN);
        $message = $client->account->messages->create(array(
            'To' => $args['ent_phone'],
            'From' => ACCOUNT_NUMBER,
            'Body' => $rand . " OTP For " . APP_NAME,
        ));
		
        $location = $this->mongo->selectCollection('verification');
        $isData = $location->findOne(array('mobile' => $args['ent_phone']));
        
        if (empty($isData)) {
            $location->insert(array('mobile' => $args['ent_phone'], 'code' => (int) $rand, 'ts' => time(), 'response' => json_encode($message)));
        } else {
            $location->update(array('mobile' => $args['ent_phone']), array('$set' => array('code' => (int) $rand, 'ts' => time(),
                    'response' => json_encode($message))));
        }

        $errMsgArr = $this->_getStatusMessage(99, 5); //_getStatusMessage($errNo, $test_num);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'], 'code' => $rand);
    }

    protected function checkMobile($args) {
        if ($args['ent_phone'] == '')
            return $this->_getStatusMessage(1, 'Mobile number');

        $checkMobileQry = "select status from patient where phone = '" . $args['ent_phone'] . "'";
        $checkMobileRes = mysql_query($checkMobileQry, $this->db->conn);

        if (mysql_num_rows($checkMobileRes) > 0)
            return $this->_getStatusMessage(118, 1);
        else
            return $this->_getStatusMessage(102, 1);
    }

    protected function ProviderCheckMobile($args) {
        if ($args['ent_phone'] == '')
            return $this->_getStatusMessage(1, 'Mobile number');

        $checkMobileQry = "select status from doctor where mobile = '" . $args['ent_phone'] . "'";
        $checkMobileRes = mysql_query($checkMobileQry, $this->db->conn);

        if (mysql_num_rows($checkMobileRes) > 0)
            return $this->_getStatusMessage(118, 1);
        else
            return $this->_getStatusMessage(102, 1);
    }

    protected function verifyPhone($args) {

        if ($args['ent_phone'] == '' || $args['ent_code'] == '')
            return $this->_getStatusMessage(1, 1);


        if ($args['ent_service_type'] == 2) {
            if ($args['ent_user_type'] == 2)
                $query = "select phone as MobileNo,country_code from patient where  phone like '%" . $args['ent_phone'] . "%' limit 0,1";
            else if ($args['ent_user_type'] == 1) {
                $query = "select mobile as MobileNo,country_code from doctor where mobile like '%" . $args['ent_phone'] . "%' limit 0,1";
            }
            $searchRes = mysql_query($query, $this->db->conn);
            if (mysql_num_rows($searchRes) <= 0)
                return $this->_getStatusMessage(136, $query);
            $data = mysql_fetch_assoc($searchRes);
            $args['ent_phone'] = $data['country_code'] . $data['MobileNo'];
        }

        $location = $this->mongo->selectCollection('verification');
        if (is_array($vdata = $location->findOne(array('mobile' => $args['ent_phone'], 'code' => (int) $args['ent_code']))) || $args['ent_code'] == '1111') {
//            if ((time() - $vdata['ts']) < 900)
            return $this->_getStatusMessage(100, 1); //Mobile verified successfully
//            else
//                return $this->_getStatusMessage(104, 1); //Verification code timeout
        } else
            return $this->_getStatusMessage(101, 1); //Verification failed, please check the OTP again.
    }

    protected function reportDispute($args) {

        if ($args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, 'Date time');
        else if ($args['ent_dispute_msg'] == '')
            return $this->_getStatusMessage(1, 'Dispute message');
        else if ($args['ent_bid'] == '')
            return $this->_getStatusMessage(1, 'Booking Id');

        $this->curr_date_time = urldecode($args['ent_date_time']);
        $args['ent_bid'] = (string) $args['ent_bid'];

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '2');
//        if (is_array($returned))
//            return $returned;

        $bookingColl = $this->mongo->selectCollection('bookings');
        $apptDet = $bookingColl->findOne(array('bid' => $args['ent_bid']));

        if ($apptDet['status'] == '4')
            return $this->_getStatusMessage(41, 3);

        $bookingColl->update();
        $updateRes = $bookingColl->update(array('bid' => $args['ent_bid']), array('$set' => array('dispute_msg' => $args['ent_dispute_msg'], 'disputed' => 1,
                'dispute_status' => '1',
                'dispute_dt' => $this->curr_date_time)));

        if ($updateRes['n'] > 0) {
            $message = "Dispute reported for appointment dated " . date('d-m-Y h:i a', strtotime($apptDet['appt_date'])) . " on " . APP_NAME . "!";
            $aplPushContent = array('alert' => $message, 'nt' => '13', 'sname' => $this->User['firstName'] . ' ' . $this->User['last_name'], 'dt' => $apptDet['appt_date'], 'smail' => $this->User['email'], 'bid' => $apptDet['bid']);
            $andrPushContent = array("payload" => $message, 'action' => '13', 'sname' => $this->User['firstName'] . ' ' . $this->User['last_name'], 'dt' => $apptDet['appt_date'], 'smail' => $this->User['email'], 'bid' => $apptDet['bid']);
            $push = $this->_sendPush($this->User['entityId'], array($apptDet['provider_id']), $message, '13', $this->User['email'], $this->curr_date_time, '1', $aplPushContent, $andrPushContent);
            $errMsgArr = $this->_getStatusMessage(121, $push);
        } else {
            $errMsgArr = $this->_getStatusMessage(3, 76);
        }
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'], 't' => $insertIntoReportQry);
    }

    protected function _createCoupon($couponsColl) {

        $coupOn = $this->_generateRandomString(7);

        $find = $couponsColl->findOne(array('coupon_code' => (string) $coupOn, 'user_type' => 1, 'coupon_type' => array('$in' => array(1, 3))));

        if (is_array($find) || count($find) > 0) {
            return $this->_createCoupon($couponsColl);
        } else {
            return $coupOn;
        }
    }

    /*
     * Method name: getMasterDetails
     * Desc: Server sends the master details according to the email id that is sent by the client
     * Input: Request data
     * Output:  doctor data if available and status message according to the result
     */

    protected function getMasterDetails($args) {

        if ($args['ent_sess_token'] == '' || $args['ent_dev_id'] == '' || $args['ent_pro_id'] == '' || $args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, 1);

        $this->curr_date_time = urldecode($args['ent_date_time']);

//        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '2');
        if (is_array($returned))
            return $returned;
        date_default_timezone_set('UTC');

        //select cat
        $location = $this->mongo->selectCollection('location');
        $userData = $location->findOne(array('user' => (int) $args['ent_pro_id']));
        $selCat = array();
        $Category = $this->mongo->selectCollection('Category');
        foreach ($userData['catlist'] as $cat) {
            $catdata = $Category->findOne(array('_id' => new MongoId($cat['cid'])));
            $selCat[] = array('cat_id' => $cat['cid'], 'cat_name' => $catdata['cat_name'], 'amount' => $cat['amount']);
        }
        //select cat

        $getDetailsQry = "select doc.num_of_job_images, doc.languages, doc.email,doc.amount,doc.doc_id,doc.first_name,doc.last_name,doc.mobile,doc.about,doc.profile_pic,doc.last_active_dt,doc.expertise,";
        $getDetailsQry .= "(select avg(star_rating) from doctor_ratings where doc_id = doc.doc_id) as rating,";
        $getDetailsQry .= "(select degree from doctor_education where doc_id = doc.doc_id limit 0,1) as degree ";
        $getDetailsQry .= "from doctor doc where doc.doc_id = '" . $args['ent_pro_id'] . "'";
        $getDetailsRes = mysql_query($getDetailsQry, $this->db->conn);

        if (mysql_error($this->db->conn) != '')
            return $this->_getStatusMessage(3, $getDetailsQry);

        $num_rows = mysql_num_rows($getDetailsRes);

        if ($num_rows <= 0)
            return $this->_getStatusMessage(20, 3);

        $doc_data = mysql_fetch_assoc($getDetailsRes);

        $reviewsArr = $this->_getMasterReviews($args);

        if (!isset($reviewsArr[0]['rating']))
            $reviewsArr = array();

        $eduArr = array();

        $getEducationQry = "select edu.degree,edu.start_year,edu.end_year,edu.institute from doctor_education edu,doctor doc where doc.doc_id = edu.doc_id and doc.doc_id = '" . $args['ent_pro_id'] . "'";
        $getEducationRes = mysql_query($getEducationQry, $this->db->conn);

        while ($edu = mysql_fetch_assoc($getEducationRes)) {
            $eduArr[] = array('deg' => $edu['degree'], 'start' => $edu['start_year'], 'end' => $edu['end_year'], 'inst' => $edu['institute']);
        }

        $errMsgArr = $this->_getStatusMessage(21, 122);

        $slotArr = array();
        if ($args['ent_slot_date'] != '') {

            $exp = explode('-', $args['ent_slot_date']); //MM-DD-YYYY


            $from = mktime(0, 0, 0, $exp[0], $exp[1], $exp[2]);
            $from = $from + (60 * 60);

            $expCurrDt = explode(' ', $this->curr_date_time);

            $currentDateExp = explode('-', $expCurrDt[0]);
            $currentTimeExp = explode(':', $expCurrDt[1]);
            $currentDateTs = mktime(0, 0, 0, $currentDateExp[1], $currentDateExp[2], $currentDateExp[0]);
            $currentDtTs = mktime($currentTimeExp[0], $currentTimeExp[1], $currentTimeExp[2], $currentDateExp[1], $currentDateExp[2], $currentDateExp[0]);

            $to = $from + (60 * 60 * 24);

            if ($from == $currentDateTs)
                $from = $currentDtTs;

            $check = array('start' => array('$gte' => (int) $from), 'end' => array('$lte' => (int) $to), 'doc_id' => (int) $doc_data['doc_id'], 'booked' => array('$in' => array(1, 2)));

            $slotm = $this->mongo->selectCollection('slotbooking');
            $slotm->ensureIndex(array('loc' => '2d'));
            $slotm->ensureIndex(array('start' => 1));
            $cursor = $slotm->find($check);

            $sortedCursor = $cursor->sort(array("start" => 1));

//            $currentDtTs = $currentDtTs + (60 * 60);
            foreach ($sortedCursor as $slot) {
                if ($currentDtTs < $slot['start'])
                    $slotArr[] = array('from' => gmdate('h:i a', $slot['start']),
                        'to' => gmdate("h:i a", $slot['end']),
                        'sid' => (string) $slot['_id'],
                        'start_dt' => (string) $slot['start_dt'],
                        'slotPrice' => (float) $slot['slot_price'],
                        'stime' => $slot['start'], 'etime' => $slot['end']);
            }
        }

        $jobImagesArr = array();
        if (isset($userData['job_images']) && is_array($userData['job_images'])) {
            foreach ($userData['job_images'] as $imgdata) {
                $jobImagesArr[] = $imgdata['url'];
            }
        }

        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'],
            'errMsg' => $errMsgArr['errMsg'], 'fee' => $doc_data['amount'], 'email' => $doc_data['email'],
            'fName' => $doc_data['first_name'], 'lName' => $doc_data['last_name'],
            'deg' => $doc_data['degree'], 'mobile' => $doc_data['mobile'], 'pPic' => $doc_data['profile_pic'],
//            'about' => ($doc_data['about'] == '' ? ' ' : preg_replace("/[^a-zA-Z1-9 ]/", "", $doc_data['about'])), 'expertise' => $doc_data['expertise'],
            'about' => $doc_data['about'], 'expertise' => $doc_data['expertise'],
            'rating' => (float) $doc_data['rating'],
            'totalRev' => count($reviewsArr),
            'reviews' => $reviewsArr, 'education' => $eduArr, 'slots' => $slotArr,
            'languages' => $doc_data['languages'], 'pid' => $doc_data['doc_id'],
            'selCat' => $selCat,
            'job_images' => $jobImagesArr,
            'num_of_job_images' => $doc_data['num_of_job_images'], 'check' => $check);
    }

    protected function getAllServices($args) {

        if ($args['ent_sess_token'] == '' || $args['ent_dev_id'] == '' || $args['ent_catid'] == '')
            return $this->_getStatusMessage(1, 1);

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '2');
        if (is_array($returned))
            return $returned;

        $servicesColl = $this->mongo->selectCollection('Category');
        $servicesCurssor = $servicesColl->findOne(array('_id' => new MongoId($args['ent_catid'])));
        $groupArr = array();
        foreach ($servicesCurssor['groups'] as $groups) {
            $serviesArr = array();
            foreach ($groups['services'] as $services) {
                $services['sid'] = (string) $services['sid'];
                $services['sname'] = (string) $services['s_name'];
                $services['sdesc'] = (string) $services['s_desc'];
                unset($services['s_name']);
                unset($services['s_desc']);
                $serviesArr[] = $services;
            }
            $groups['services'] = $serviesArr;
            $groups['group_id'] = (string) $groups['gid'];
            unset($groups['gid']);
            if (count($serviesArr) > 0)
                $groupArr[] = $groups;
        }
        $errMsgArr = $this->_getStatusMessage(21, 984);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'],
            'errMsg' => $errMsgArr['errMsg'], 'data' => $groupArr);
    }

    /*
     * Method name: getMasterDetails
     * Desc: Server sends the master details according to the email id that is sent by the client
     * Input: Request data
     * Output:  doctor data if available and status message according to the result
     */

    protected function getMasterDetailsFromMap($args) {

        if ($args['ent_sess_token'] == '' || $args['ent_dev_id'] == '' || $args['ent_doc_email'] == '' || $args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, 1);

        $this->curr_date_time = urldecode($args['ent_date_time']);

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '2');

        if (is_array($returned))
            return $returned;

        $getDetailsQry = "select doc.doc_id,doc.first_name,doc.last_name,doc.mobile,doc.about,doc.profile_pic,doc.last_active_dt,doc.expertise,";
        $getDetailsQry .= "(select avg(star_rating) from doctor_ratings where doc_id = doc.doc_id) as rating,";
        $getDetailsQry .= "(select group_concat(image) from images where doc_id = doc.doc_id) as images,";
        $getDetailsQry .= "(select degree from doctor_education where doc_id = doc.doc_id limit 0,1) as degree ";
        $getDetailsQry .= "from doctor doc where doc.email = '" . $args['ent_doc_email'] . "'";
        $getDetailsRes = mysql_query($getDetailsQry, $this->db->conn);

        if (mysql_error($this->db->conn) != '')
            return $this->_getStatusMessage(3, $getDetailsQry); //_getStatusMessage($errNo, $test_num);

        $num_rows = mysql_num_rows($getDetailsRes);

        if ($num_rows <= 0)
            return $this->_getStatusMessage(20, 3); //_getStatusMessage($errNo, $test_num);

        $doc_data = mysql_fetch_assoc($getDetailsRes);

        $location = $this->mongo->selectCollection('location');

        $masterDet = $location->findOne(array('user' => (int) $doc_data['doc_id']));

        if ($masterDet['status'] != 3)
            return $this->_getStatusMessage(82, 82);

        $reviewsArr = $this->_getMasterReviews($args);

        if (!isset($reviewsArr[0]['rating']))
            $reviewsArr = array();

        $eduArr = array();

        $getEducationQry = "select edu.degree,edu.start_year,edu.end_year,edu.institute from doctor_education edu,doctor doc where doc.doc_id = edu.doc_id and doc.email = '" . $args['ent_doc_email'] . "'";
        $getEducationRes = mysql_query($getEducationQry, $this->db->conn);

        while ($edu = mysql_fetch_assoc($getEducationRes)) {
            $eduArr[] = array('deg' => $edu['degree'], 'start' => $edu['start_year'], 'end' => $edu['end_year'], 'inst' => $edu['institute']);
        }

        $errMsgArr = $this->_getStatusMessage(21, 122);

        $slotArr = array();

        if ($args['ent_slot_date'] != '') {

            $exp = explode('-', $args['ent_slot_date']); //MM-DD-YYYY

            $from = mktime(0, 0, 0, $exp[0], $exp[1], $exp[2]);

            $expCurrDt = explode(' ', $this->curr_date_time);

            $currentDateExp = explode('-', $expCurrDt[0]);
            $currentTimeExp = explode(':', $expCurrDt[1]);
            $currentDateTs = mktime(0, 0, 0, $currentDateExp[1], $currentDateExp[2], $currentDateExp[0]);
            $currentDtTs = mktime($currentTimeExp[0], $currentTimeExp[1], $currentTimeExp[2], $currentDateExp[1], $currentDateExp[2], $currentDateExp[0]);

            $to = $from + (60 * 60 * 24);

            if ($from == $currentDateTs)
                $from = $currentDtTs;

            $check = array('start' => array('$gte' => (int) $from), 'end' => array('$lte' => (int) $to), 'doc_id' => (int) $doc_data['doc_id'], 'booked' => 1);

            $slotm = $this->mongo->selectCollection('slotbooking');

            $slotm->ensureIndex(array('loc' => '2d'));
            $slotm->ensureIndex(array('start' => 1));

            $cursor = $slotm->find($check);

            $sortedCursor = $cursor->sort(array("start" => 1));

            foreach ($sortedCursor as $slot) {
                $slotArr[] = array('from' => date('h:i a', $slot['start']), 'to' => date("h:i a", $slot['end']), 'sid' => (string) $slot['_id'], 'slotPrice' => (float) $slot['slot_price']);
            }
        }

        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'],
            'fName' => $doc_data['first_name'], 'lName' => $doc_data['last_name'], 'deg' => $doc_data['degree'], 'mobile' => $doc_data['mobile'], 'pPic' => $doc_data['profile_pic'], 'about' => ($doc_data['about'] == '' ? ' ' : $doc_data['about']), 'expertise' => $doc_data['expertise'],
            'ladt' => $doc_data['last_active_dt'], 'rating' => (float) $doc_data['rating'], 'images' => explode(',', $doc_data['images']), 'totalRev' => $reviewsArr[0]['total'], 'reviews' => $reviewsArr, 'education' => $eduArr, 'slots' => $slotArr, 'check' => $check);
    }

    /*
     * Method name: updateMasterLocation
     * Desc: Update master location
     * Input: Request data
     * Output:  success if changed else error according to the result
     */

    protected function updateMasterLocation($args) {

        if ($args['ent_latitude'] == '' || $args['ent_longitude'] == '' || $args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, 1);

        $this->curr_date_time = urldecode($args['ent_date_time']);

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '2');

        if (is_array($returned))
            return $returned;

        $location = $this->mongo->selectCollection('location');

        $newdata = array('$set' => array("location" => array("longitude" => (float) $args['ent_longitude'], "latitude" => (float) $args['ent_latitude'])));
        $updated = $location->update(array("user" => (int) $this->User['entityId']), $newdata);

        $argstopass = array('proId' => $this->User['entityId']);
        $this->sendDatatoSocket($argstopass, "location");

        if ($updated)
            return $this->_getStatusMessage(23, 2);
        else
            return $this->_getStatusMessage(22, 3);
    }

    /*
     * Method name: getMasterReviews
     * Desc: Get doctor reviews by pagination
     * Input: Request data
     * Output:  success if got it else error according to the result
     */

    protected function getMasterReviews($args) {

        if ($args['ent_doc_email'] == '' || $args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, 1);

        $this->curr_date_time = urldecode($args['ent_date_time']);

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '2');

        if (is_array($returned))
            return $returned;

        $reviewsArr = $this->_getMasterReviews($args);

        if (!isset($reviewsArr[0]['rating']))
            return $reviewsArr;

        $errMsgArr = $this->_getStatusMessage(27, 122);

        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'], 'reviews' => $reviewsArr);
    }

    protected function getCityList($args) {

        if ($args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, 6);

        $this->curr_date_time = urldecode($args['ent_date_time']);

        $selectCityQry = "select * from city_available";
        $selectCityRes = mysql_query($selectCityQry, $this->db->conn);

        if (mysql_num_rows($selectCityRes) <= 0)
            return $this->_getStatusMessage(28, $selectCityQry);

        while ($city = mysql_fetch_assoc($selectCityRes)) {
            $cityArr[] = array('City_Id' => $city['City_Id'],
                'City_Name' => $city['City_Name']
////                'City_Lat' => preg_replace('/[^A-Za-z0-9 ]/', '', $city['City_Lat']),
////                'City_Long' => preg_replace('/[^A-Za-z0-9 ]/', '', $city['City_Long']));
////                'City_Name' => preg_replace('/[^A-Za-z 0-9]/','',$city['City_name']),
//                'City_Lat' => $city['City_Lat'],
//                'City_Long' => $city['City_Long']
            );
//            $cityArr[] = $city;
        }
//        print_r($cityArr);
//return $cityArr;
        $errMsgArr = $this->_getStatusMessage(106, 122);

        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'], 'city' => $cityArr);
    }

    protected function getTypeBYCityId($args) {

        if ($args['ent_city_id'] == '' || $args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, 6);
        $this->curr_date_time = urldecode($args['ent_date_time']);

        $protype = $this->mongo->selectCollection('Category');
        $cursor = $protype->find(array('city_id' => $args['ent_city_id']));
        $type = array();
        $Mileage = $Hourly = $Fixed = array();
        foreach ($cursor as $row) {
            $type[] = $row;
        }
        if (count($type) <= 0)
            return $this->_getStatusMessage(108, "no Type");
        for ($i = 0; $i < count($type); $i++) {
            $catid = (string) $type[$i]['_id'];
            if ($type[$i]['fee_type'] == "Mileage")
                $Mileage[] = array('cat_id' => $catid, 'cat_name' => $type[$i]['cat_name']);
            else if ($type[$i]['fee_type'] == "Hourly")
                $Hourly[] = array('cat_id' => $catid, 'cat_name' => $type[$i]['cat_name']);
            else if ($type[$i]['fee_type'] == "Fixed")
                $Fixed[] = array('cat_id' => $catid, 'cat_name' => $type[$i]['cat_name']);
        }
        $errMsgArr = $this->_getStatusMessage(107, 122);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'], 'Mileage' => $Mileage, 'Hourly' => $Hourly, 'Fixed' => $Fixed);
    }

    protected function get_DirectionFormMatrix($pickupLat, $pickuptLong, $dropLat, $DropLong) {
        $url = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins=' . $pickupLat . ',' . $pickuptLong . '&destinations=' . $dropLat . ',' . $DropLong;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);
        $arr = json_decode($result, true);

        return array('OrignalDatadata' => $arr,
            'distance' => $arr['rows'][0]['elements'][0]['distance']['value'],
            'duration' => $arr['rows'][0]['elements'][0]['distance']['value']);
    }

    protected function getMasterAppointmentsHome($args) {

        if ($args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, 1);
        $this->curr_date_time = urldecode($args['ent_date_time']);

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
        if (is_array($returned))
            return $returned;
//        $this->User['entityId'] = 1;

        $locationColl = $this->mongo->selectCollection('location');
        $locationRes = $locationColl->findOne(array('user' => (int) $this->User['entityId']));

        $nowBookings = $laterBookings = array();
        $bookings = $this->mongo->selectCollection('bookings');
        $bookingsCursor = $bookings->find(array('provider_id' => (string) $this->User['entityId'], 'status' => array('$in' => array(2, 5, 6, 21, 22))))->sort(array('_id' => -1));
//        $check = array('provider_id' => (string) $this->User['entityId'], 'status' => array('$in' => array(2, 5, 6, 21, 22)));
//        $bookingsCursor = $bookings->find($check)->sort(array('bid' => -1));

        $bookingsRes = array();
        foreach ($bookingsCursor as $cur) {
            $bookingsRes[] = $cur;
        }

        $slotbookingColl = $this->mongo->selectCollection('slotbooking');

        foreach ($bookingsCursor as $appnt) {
            if ($appnt['customer_pic'] == '')
                $appnt['customer_pic'] = $this->default_profile_pic;

            $amount = 0;
            $total_pro = $appnt['comp']['total_pro'];
            $misc_fees = $appnt['comp']['misc_fees'];
            $pro_disc = $appnt['comp']['pro_disc'];
            $sign_url = $appnt['comp']['sign_url'];
            $mat_fees = $appnt['comp']['mat_fees'];
            $pro_notes = $appnt['comp']['complete_note'];
            $job_start_time = date('Y-m-d H:i:s', $appnt['job_start_time']);

            $pc = $this->mongo->selectCollection('Category');
            $cdata = $pc->findOne(array('_id' => new MongoId($appnt['cat_id'])));
            $catname = $cdata['cat_name'];

            if ($cdata['fee_type'] == 'Hourly') {
                $visitAmt = $cdata['visit_fees'];
                $price_per_min = $cdata['price_min']/60;
            } else if ($cdata['fee_type'] == 'Mileage') {
                $visitAmt = 0;
                $price_per_min = 0;
            } else if ($cdata['fee_type'] == 'Fixed') {
                $visitAmt = 0;
                $price_per_min = 0;
            } else {
                $visitAmt = 0;
                $price_per_min = 0;
            }

            $services_total = 0;
            if (isset($appnt['services']) && is_array($appnt['services'])) {
                foreach ($appnt['services'] as $services) {
                    $services_total += $services['sprice'];
                }
            }
            if ($appnt['coupon_type'] == 1) {
                $forDisc = $appnt['visit_fees'] + $appnt['comp']['time_fees'] + $services_total;
                $coup_dicount = ($forDisc * $appnt['coupon_discount']) / 100;
            } else {
                $coup_dicount = $appnt['coupon_discount'];
            }

            $status = $this->GetMessageStatus($appnt['status']);

            if ($appnt['booking_type'] == 2) {

                $slotbookingRes = $slotbookingColl->findOne(array('_id' => new MongoId($appnt['slot_id'])));

                $laterBookings[] = array('bid' => $appnt['bid'], 'cid' => $appnt['customer']['id'],
                    'apntDt' => $appnt['appt_date'], 'pPic' => $appnt['customer']['pic'],
                    'email' => $appnt['customer']['email'], 'status' => $appnt['status'], 'statusMsg' => $status,
                    'fname' => $appnt['customer']['fname'], 'phone' => $appnt['customer']['country_code'] . $appnt['customer']['mobile'],
                    'apntTime' => date('h:i a', strtotime($appnt['appt_date'])),
                    'cancelAmt' => $appnt['cancel_amount'],
                    'apntDate' => date('d M Y', strtotime($appnt['appt_date'])),
                    'apptLat' => (double) $appnt['appt_lat'], 'apptLong' => (double) $appnt['appt_long'],
                    'expireTime' => '',
                    'job_timer' => $appnt['timer_start_time'], 'timer_status' => $appnt['timer_status'],
                    'job_start_time' => $job_start_time, 'visit_amount' => $visitAmt,
                    'timer' => $appnt['timer'],
                    'addrLine1' => urldecode($appnt['address1']),
                    'addrLine2' => urldecode($appnt['address2']),
                    'customer_notes' => $appnt['customer_notes'],
                    'pro_notes' => $pro_notes,
                    'services' => $appnt['services'],
                    'bookType' => $appnt['booking_type'],
                    'start_slot' => $slotbookingRes['start'],
                    'end_slot' => $slotbookingRes['end'],
                    'cat_name' => $catname, 'price_per_min' => $price_per_min,
                    'job_imgs' => $appnt['job_images'],
                    'appt_duration' => $appnt['duration'],
                    'distance_met' => $appnt['distance_met'],
                    'coupon_discount' => $coup_dicount,
                    'payment_type' => $appnt['payment_type']);
            } else {
                $nowBookings[] = array('bid' => $appnt['bid'], 'cid' => $appnt['customer']['id'],
                    'apntDt' => $appnt['appt_date'], 'pPic' => $appnt['customer']['pic'],
                    'email' => $appnt['customer']['email'], 'status' => $appnt['status'], 'statusMsg' => $status,
                    'fname' => $appnt['customer']['fname'], 'phone' => $appnt['customer']['country_code'] . $appnt['customer']['mobile'],
                    'apntTime' => date('h:i a', strtotime($appnt['appt_date'])),
                    'cancelAmt' => $appnt['cancel_amount'],
                    'apntDate' => date('d M Y', strtotime($appnt['appt_date'])),
                    'apptLat' => (double) $appnt['appt_lat'], 'apptLong' => (double) $appnt['appt_long'],
                    'expireTime' => '',
                    'job_timer' => $appnt['timer_start_time'], 'timer_status' => $appnt['timer_status'],
                    'job_start_time' => $job_start_time, 'visit_amount' => $visitAmt,
                    'timer' => $appnt['timer'],
                    'addrLine1' => urldecode($appnt['address1']),
                    'addrLine2' => urldecode($appnt['address2']),
                    'bookType' => $appnt['booking_type'],
                    'pro_notes' => $pro_notes,
                    'services' => $appnt['services'],
                    'customer_notes' => $appnt['customer_notes'],
                    'cat_name' => $catname, 'price_per_min' => $price_per_min,
                    'job_imgs' => $appnt['job_images'],
                    'appt_duration' => $appnt['duration'],
                    'coupon_discount' => $coup_dicount,
                    'distance_met' => $appnt['distance_met'],
                    'payment_type' => $appnt['payment_type']);
            }
        }

        $errMsgArr = $this->_getStatusMessage(31, 2);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'],
            'nowbooking' => $nowBookings, 'laterBookings' => $laterBookings, 'status' => $locationRes['status']);
    }

    protected function getMasterAppointmentsHistory($args) {

        if ($args['ent_appnt_dt'] == '' || $args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, 1);
        $this->curr_date_time = urldecode($args['ent_date_time']);

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
        if (is_array($returned))
            return $returned;
//         $this->User['entityId'] = "103";

        $yearmonth = $args['ent_appnt_dt'];
        $args['ent_appnt_dt'] = urldecode($args['ent_appnt_dt']);

        $args['ent_appnt_dt'] = $args['ent_appnt_dt'] . '-01';
        $endDate = date('Y-m-d', strtotime('+1 month', strtotime($args['ent_appnt_dt'])));

        $bookings = $this->mongo->selectCollection('bookings');
        $cond = new MongoRegex("/^" . $yearmonth . "/");
        $bookingCursor = $bookings->find(array('appt_date' => $cond, 'provider_id' => $this->User['entityId']))->sort(array('_id' => -1));
//        $bookingCursor = $bookings->find(array('provider_id' => $this->User['entityId']))->sort(array('_id' => -1));

        $bookingRes = array();
        foreach ($bookingCursor as $cur) {
            array_push($bookingRes, $cur);
        }

        $nowBookings = $laterBookings = $sortedApnts = array();

        foreach ($bookingRes as $appnt) {
            if ($appnt['profile_pic'] == '')
                $appnt['profile_pic'] = $this->default_profile_pic;

            $aptdate = date('Y-m-d', strtotime($appnt['appt_date']));

            $amount = 0;
            $total_pro = $appnt['comp']['total_pro'];
            $misc_fees = $appnt['comp']['misc_fees'];
            $pro_disc = $appnt['comp']['pro_disc'];
            $sign_url = $appnt['comp']['sign_url'];
            $mat_fees = $appnt['comp']['mat_fees'];
            $pro_notes = $appnt['comp']['complete_note'];
            $job_start_time = date('Y-m-d H:i:s', $appnt['job_start_time']);

            $pc = $this->mongo->selectCollection('Category');
            $cdata = $pc->findOne(array('_id' => new MongoId($appnt['cat_id'])));
            $catname = $cdata['cat_name'];

            if ($cdata['fee_type'] == 'Hourly') {
                $visitAmt = $cdata['visit_fees'];
                $price_per_min = $cdata['price_min'];
            } else if ($cdata['fee_type'] == 'Mileage') {
                $visitAmt = 0;
                $price_per_min = 0;
            } else if ($cdata['fee_type'] == 'Fixed') {
                $visitAmt = 0;
                $price_per_min = 0;
            } else {
                $visitAmt = 0;
                $price_per_min = 0;
            }

            $services_total = 0;
            if (isset($appnt['services']) && is_array($appnt['services'])) {
                foreach ($appnt['services'] as $services) {
                    $services_total += $services['sprice'];
                }
            }
            if ($appnt['coupon_type'] == 1) {
                $forDisc = $appnt['visit_fees'] + $appnt['comp']['time_fees'] + $services_total;
                $coup_dicount = ($forDisc * $appnt['coupon_discount']) / 100;
            } else {
                $coup_dicount = $appnt['coupon_discount'];
            }

            $status = $this->GetMessageStatus($appnt['status']);
            if ($appnt['disputed'] == 1) {
                $status = "Booking Disputed";
            }
            $nowBookings[$aptdate][] = array('bid' => $appnt['bid'],
                'cid' => $appnt['customer']['id'],
                'apntDt' => $appnt['appt_date'], 'pPic' => $appnt['customer']['pic'],
                'email' => $appnt['customer']['email'], 'status' => $appnt['status'], 'statusMsg' => $status,
                'fname' => $appnt['customer']['fname'], 'phone' => $appnt['country_code'] . $appnt['customer']['mobile'],
                'apntTime' => date('h:i a', strtotime($appnt['appt_date'])), 'amount' => $amount,
                'cancelAmt' => $appnt['cancel_amount'],
                'apntDate' => date('d M Y', strtotime($appnt['appt_date'])),
                'apptLat' => (double) $appnt['appt_lat'], 'apptLong' => (double) $appnt['appt_long'],
                'job_timer' => $appnt['timer_start_time'], 'timer_status' => $appnt['timer_status'],
                'job_start_time' => $job_start_time, 'cat_name' => $catname, 'total_pro' => $total_pro,
                'addrLine1' => urldecode($appnt['address1']),
                'addrLine2' => urldecode($appnt['address2']),
                'pro_notes' => $pro_notes,
                'customer_notes' => $appnt['customer_notes'],
                'bookType' => $appnt['booking_type'],
//                'distance' => number_format((float) $appnt['distance'], 2, '.', ''),
                'distance_met' => $appnt['distance_met'],
                'visit_amount' => $visitAmt,
                'services' => $appnt['services'],
                'coupon_discount' => $coup_dicount,
                'coupon_per' => $appnt['coupon_discount'],
                'coupon_type' => $appnt['coupon_type'],
                'price_per_min' => $price_per_min,
                'payment_type' => $appnt['payment_type'],
                'timer' => $appnt['timer'],
                'appt_duration' => $appnt['duration'],
                'misc_fees' => $misc_fees,
                'pro_disc' => $pro_disc,
                'mat_fees' => $mat_fees,
                'sign_url' => $sign_url);
        }
        $date = $args['ent_appnt_dt'];

        while ($date < $endDate) {
            $empty_arr = array();
            if (is_array($nowBookings[$date])) {
                $sortedApnts[] = array('date' => $date, 'appt' => $nowBookings[$date]);
            } else {
                $sortedApnts[] = array('date' => $date, 'appt' => $empty_arr);
            }
            $date = date('Y-m-d', strtotime('+1 day', strtotime($date)));
        }

        $errMsgArr = $this->_getStatusMessage(31, 2);

        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'],
            'Bookings' => $sortedApnts);
    }

    protected function __GetStatusMessageForCustomer($st) {
        if ($st == '1')
            $status = 'Job requested';
        else if ($st == '2')
            $status = 'Provider Accepted.';
        else if ($st == '3')
            $status = 'Provider Rejected.';
        else if ($st == '4')
            $status = 'customer has cancelled.';
        else if ($st == '5')
            $status = 'Provider on the way.';
        else if ($st == '21')
            $status = 'Provider Arrived at job location';
        else if ($st == '6')
            $status = 'Job started.';
        else if ($st == '22')
            $status = 'Job Completed and Invoice Pending';
        else if ($st == '7')
            $status = 'Job Completed';
        else if ($st == '8')
            $status = 'Job Timed out.';
        else if ($st == '9')
            $status = 'Job Cancelled with fee.';
        else if ($st == '10')
            $status = 'Provider has cancelled.';
        else
            $status = 'Status unavailable.';
        return $status;
    }

    protected function GetMessageStatus($st) {
        if ($st == '1')
            $status = 'Job requested';
        else if ($st == '2')
            $status = 'Provider Accepted.';
        else if ($st == '3')
            $status = 'Provider Rejected.';
        else if ($st == '4')
            $status = 'customer has cancelled.';
        else if ($st == '5')
            $status = 'Provider on the way.';
        else if ($st == '21')
            $status = 'Provider Arrived at job location';
        else if ($st == '6')
            $status = 'Job started.';
        else if ($st == '7')
            $status = 'Job Completed';
        else if ($st == '22')
            $status = 'Signature';
        else if ($st == '8')
            $status = 'Job Timed out.';
        else if ($st == '9')
            $status = 'Job Cancelled with fee.';
        else if ($st == '10')
            $status = 'Provider has cancelled.';
        else
            $status = 'Status unavailable.';
        return $status;
    }

    protected function liveBooking($args) {

        if ($args['ent_cat_id'] == '')
            return $this->_getStatusMessage(1, "Missing Ctegory Id");
        if ($args['ent_cat_name'] == '')
            return $this->_getStatusMessage(1, "Missing Ctegory Name");
        if ($args['ent_custid'] == '')
            return $this->_getStatusMessage(1, "Missing Customer Id");
        if ($args['ent_a1'] == '')
            return $this->_getStatusMessage(1, "Missing Address");
        if ($args['ent_lat'] == '')
            return $this->_getStatusMessage(1, "Missing latitude");
        if ($args['ent_long'] == '')
            return $this->_getStatusMessage(1, "Missing Longitude");
        if ($args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, "Missing DateTime");

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '2');
        if (is_array($returned))
            return $returned;
//        $this->User['entityId'] = 1;

        $this->curr_date_time = urldecode($args['ent_date_time']);
        $args['ent_appnt_dt'] = urldecode($args['ent_appnt_dt']);

        if ($args['ent_btype'] == 2) {
            if ($args['ent_slot_id'] == '' || $args['ent_proid'] == '')
                return $this->_getStatusMessage(1, 1);
            $slots = $this->mongo->selectCollection('slotbooking');
            $slots->ensureIndex(array('loc' => '2d'));
            $docData = $this->_getEntityDet($args['ent_proid'], '1');
            if (!is_array($docData))
                return $this->_getStatusMessage(37, 37);

            $slotData = $slots->findOne(array('_id' => new MongoId($args['ent_slot_id'])));
            if ($slotData['start'] == '' || ((int) $slotData['booked'] != 1 && (int) $slotData['booked'] != 2))
                return $this->_getStatusMessage(72, 72);
            else
                $args['ent_appnt_dt'] = gmdate('Y-m-d H:i:s', $slotData['start']);
        }
        //generate MOngoDB id
        $cursor = $this->mongo->selectCollection('bookings')->find(array(), array('_id'))->sort(array((string) '_id' => -1))->limit(1);
        foreach ($cursor as $data) {
            $return[] = $data;
        }
        if ($return[0]['_id'] == "")
            $apptId = 1;
        else
            $apptId = ((int) $return[0]['_id'] + 1);

        if ($args['ent_btype'] == 2) {

            //update slot as booked by patient, will not be displayed for other patients
            $slots->update(array('_id' => new MongoId($args['ent_slot_id'])), array('$set' => array('booked' => 2)));

            $message = "You booking confirmed with  " . $this->User['firstName'] . " for " . date('g:i A  \o\n jS F Y', strtotime($args['ent_appnt_dt']));
            $aplPushContent = array('alert' => $message, 'nt' => '2', 'sname' => $this->User['firstName'] . ' ' . $this->User['lastName'], 'dt' => $args['ent_appnt_dt'], 'e' => $this->User['email'], 'sound' => 'default', 'bid' => $apptId);
            $andrPushContent = array("payload" => $message, 'action' => '2', 'sname' => $this->User['firstName'] . ' ' . $this->User['lastName'], 'dt' => $args['ent_appnt_dt'], 'e' => $this->User['email'], 'bid' => $apptId);
            $pushNum['push'] = $this->_sendPush($this->User['entityId'], array($docData['doc_id']), $message, '1', $this->User['firstName'], $this->curr_date_time, '1', $aplPushContent, $andrPushContent);
        } else {
            $message = "You got an appointment request from " . $this->User['firstName'] . " for " . date('g:i A  \o\n jS F Y', strtotime($args['ent_appnt_dt']));
            $aplPushContent = array('alert' => $message, 'nt' => '1', 'sname' => $this->User['firstName'] . ' ' . $this->User['lastName'], 'dt' => $args['ent_appnt_dt'], 'e' => $this->User['email'], 'sound' => 'default', 'bid' => $apptId);
            $andrPushContent = array("payload" => $message, 'action' => '1', 'sname' => $this->User['firstName'] . ' ' . $this->User['lastName'], 'dt' => $args['ent_appnt_dt'], 'e' => $this->User['email'], 'bid' => $apptId);
            $pushNum['push'] = $this->_sendPush($this->User['entityId'], array($docData['doc_id']), $message, '1', $this->User['firstName'], $this->curr_date_time, '1', $aplPushContent, $andrPushContent);
        }


        $appointmentExpiryTs = time() + (15 * 60);
        $coupon_type = 0;
        $discount = 0;
        $cc = new MongoRegex("/^" . $args['ent_coupon'] . "/i");
        if ($args['ent_coupon'] != '') {
            $cond = array('status' => 0, 'coupon_code' => $cc, '$or' => array(array('coupon_type' => 3, 'user_id' => (string) $this->User['entityId'], 'status' => 0, 'expiry_date' => array('$gte' => time())), array('coupon_type' => 2, 'bookings.slave_id' => array('$ne' => $this->User['entityId']), 'expiry_date' => array('$gte' => time()))));
            $resultArr = $this->mongo->selectCollection('$cmd')->findOne(array(
                'geoNear' => 'coupons',
                'near' => array(
                    (double) $args['ent_long'], (double) $args['ent_lat']
                ), 'spherical' => true, 'maxDistance' => ($this->promoCodeRadius * 1000) / 6378137, 'distanceMultiplier' => 6378137,
                'query' => $cond)
            );
            if (count($resultArr['results']) <= 0)
                $args['ent_coupon'] = "";
        }
        if ($args['ent_coupon'] != '') {
            $cond = array('status' => 0, 'coupon_code' => $cc, '$or' => array(array('coupon_type' => 3, 'status' => 0, 'expiry_date' => array('$gte' => time())), array('coupon_type' => 2, 'expiry_date' => array('$gte' => time()))));
            $resultArr = $this->mongo->selectCollection('$cmd')->findOne(array(
                'geoNear' => 'coupons',
                'near' => array(
                    (double) $args['ent_long'], (double) $args['ent_lat']
                ), 'spherical' => true, 'maxDistance' => ($this->promoCodeRadius * 1000) / 6378137, 'distanceMultiplier' => 6378137,
                'query' => $cond)
            );

            if (count($resultArr['results']) <= 0)
                return $this->_getStatusMessage(96, $resultArr);

            $findOne = $resultArr['results'][0]['obj'];
            $findOne['current'] = time();
            if ($findOne['start_date'] > strtotime($this->curr_date_time) || $findOne['expiry_date'] < strtotime($this->curr_date_time))
                return $this->_getStatusMessage(96, $findOne);

            if ($findOne['discount_type'] == '2')
                $discount = $findOne['discount'];
            else
                $discount = $findOne['discount'];

            $coupon_type = $findOne['discount_type'];
        }

        $coupon_id = $findOne['_id'];

        $customer = array('id' => (string) $args['ent_custid'],
            'fname' => $this->User['firstName'],
            'lname' => $this->User['lastName'],
            'pic' => $this->User['pPic'],
            'email' => $this->User['email'],
            'mobile' => $this->User['mobile']);

        $slot_id = "";
        $provider_id = "0";
        $status = 0;
        if ($args['ent_btype'] == 2) {
            if ($args['ent_proid'] == '' || $args['ent_slot_id'] == '')
                return $this->_getStatusMessage(1, "Missing Proid and SLot id Id");
            $provider_id = $args['ent_proid'];
            $slot_id = $args['ent_slot_id'];
            $status = 2;
        }

        //start for cancel amount
        $catColl = $this->mongo->selectCollection('Category');
        $catRes = $catColl->findOne(array('_id' => new MongoId($args['ent_cat_id'])));
        //end for cancel amount

        $time = array(
            'requested_ts' => time(),
            'requested_dt' => $args['ent_date_time'],
            'accepted_dt' => "",
            'accepted_ts' => "",
            'completed_dt' => "",
            'completed_ts' => "",
            'jobend_dt' => "",
            'jobend_ts' => "",
            'raiseinvoice_ts' => "",
            'raiseinvoice_dt' => "",
            'jobstart_ts' => "",
            'jobstart_dt' => "",
            'arrived_ts' => "",
            'arrived_dt' => "",
            'ontheway_dt' => "",
            'ontheway_ts' => ""
        );

        $appt_server_ts = "";
        if ($args['ent_btype'] == 2) {
            $appnt_latr_server_diff = strtotime($args['ent_date_time']) - strtotime($args['ent_date_time']);
            $appt_server_ts = strtotime(date('Y-m-d H:i:s', time() + $appnt_latr_server_diff));
        } else {
            $appt_server_ts = strtotime(date('Y-m-d H:i:s', time()));
        }

        $bookings = $this->mongo->selectCollection('bookings');
        $mongoArr = array(
            "_id" => (int) $apptId,
            "bid" => (string) $apptId,
            'customer' => $customer,
            'provider_id' => $provider_id,
            'services' => $args['ent_services'],
            'card_token' => $args['ent_card_id'],
            'appt_created' => $this->curr_date_time,
            'appt_last_modified' => $this->curr_date_time,
            'appt_date' => $this->curr_date_time,
            'appt_server_ts' => $appt_server_ts,
            'slot_id' => $slot_id,
            'device_type' => $args['ent_dtype'], "booking_type" => $args['ent_btype'],
            'coupon_code' => ($args['ent_coupon'] ? $args['ent_coupon'] : ""),
            'coupon_discount' => $discount,
            'coupon_id' => $coupon_id,
            'coupon_type' => $coupon_type,
            'time' => $time,
            'customer_notes' => $args['ent_job_details'],
            'status' => $status, 'cat_id' => $args['ent_cat_id'],
            'cat_name' => $catRes['cat_name'],
            'job_images' => $args['ent_job_imgs'],
            'expiry_time' => $appointmentExpiryTs,
            'address1' => $args['ent_a1'],
            'address2' => ($args['ent_a2'] ? $args['ent_a2'] : ""),
            'zipcode' => ($args['ent_zipcode'] ? $args['ent_zipcode'] : ""),
            'appt_lat' => $args['ent_lat'], "appt_long" => $args['ent_long'],
            'cancel_amount' => $catRes['can_fees'],
            'payment_type' => $args['ent_pymt']
        );
        $bookings->insert($mongoArr);

        $argstopass = array('bid' => $apptId);
        $this->sendDatatoSocket($argstopass, "bookingCreated");

        if ($args['ent_btype'] == 1 || $args['ent_btype'] == 3) {
            $errMsgArr = $this->_getStatusMessage(39, 39);
            return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'],
                'errMsg' => $errMsgArr['errMsg'], 'bid' => $apptId);
        } else {
            $this->AcceptLareBookingDirect($apptId, $provider_id);
            $errMsgArr = $this->_getStatusMessage(119, 2);
            return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'],
                'errMsg' => $errMsgArr['errMsg'], 'bid' => $apptId);
        }
    }

    /*
     * Method name: getAppointmentDetails
     * Desc: Get appointment details of a given slot
     * Input: Request data
     * Output:  success if got it else error according to the result
     */

    protected function getAppointmentDetails($args) {

        if ($args['ent_bid'] == '')
            return $this->_getStatusMessage(1, 'Missing Booking Id');
        if ($args['ent_inv'] == '')
            return $this->_getStatusMessage(1, 'Missing Invoice Status');
        if ($args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, 'Missing Date Time');

        $this->curr_date_time = urldecode($args['ent_date_time']);

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '2');
        if (is_array($returned))
            return $returned;

        $args['ent_appnt_dt'] = urldecode($args['ent_appnt_dt']);

        $bookings = $this->mongo->selectCollection('bookings');
        $apptData = $bookings->findOne(array('bid' => (string) $args['ent_bid']));

        if (!$apptData)
            return $this->_getStatusMessage(62, 1);

        $location = $this->mongo->selectCollection('location');
        $userData = $location->findOne(array('user' => (int) $apptData['provider_id']));

        $catdata = $this->mongo->selectCollection('Category');
        $cdata = $catdata->findOne(array('_id' => new MongoId($apptData['cat_id'])));
        unset($cdata['groups']);

        $coupon_discount = $this->__calculateCopenDiscount($apptData);

        $fdata = array('payment_type' => $apptData['payment_type'], 'price_min' => $cdata['price_min'], 'visit_fees' => $cdata['visit_fees'],
            'coupon_per' => $apptData['coupon_discount'],
            'coupon_discount' => $coupon_discount,
            'coupon_type' => $apptData['coupon_type'],
            'pro_note' => $apptData['comp']['complete_note'],
            'misc_fees' => $apptData['comp']['misc_fees'], 'fee_type' => $apptData['fee_type'],
            'mat_fees' => $apptData['comp']['mat_fees'], 'sign_url' => $apptData['comp']['sign_url'],
            'total_pro' => $apptData['comp']['total_pro'],
            'pro_disc' => $apptData['comp']['pro_disc'],
            'time_fees' => $apptData['comp']['time_fees'], 'sub_total_cust' => $apptData['comp']['sub_total_cust']);


        $selectAvgRatQry = "select avg(star_rating) as avg from doctor_ratings where doc_id = '" . $apptData['provider_id'] . "' and status = 1";
        $selectAvgRatRes = mysql_query($selectAvgRatQry, $this->db->conn);
        $avgRow = mysql_fetch_assoc($selectAvgRatRes);

        $selrev = "select * from doctor_ratings where appointment_id = '" . $apptData['bid'] . "' and status = 1";
        $revres = mysql_query($selrev, $this->db->conn);
        $apptRevRes = mysql_fetch_assoc($revres);
        $review_submited = 0;
        if (mysql_num_rows($revres) <= 0)
            $review_submited = 0;
        else
            $review_submited = 1;

        $selrev = "select star_rating from doctor_ratings where appointment_id = '" . $apptData['bid'] . "' and status = 1 LIMIT 1";
        $revres = mysql_fetch_assoc(mysql_query($selrev, $this->db->conn));
        $star_rating = 0;
        if ($revres['star_rating'] != "")
            $star_rating = (double) $revres['star_rating'];

        $cancel_amount = 0;
        if ($apptData['status'] == '9')
            $cancel_amount = $apptData['cancel_amount'];

        $errMsgArr = $this->_getStatusMessage(21, 2);
        if ($args['ent_inv'] == '1') {
            return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'],
                'errMsg' => $errMsgArr['errMsg'],
                'bid' => $apptData['bid'],
                'visit_fees' => $cdata['visit_fees'],
                'cat_name' => $cdata['cat_name'],
                'price_min' => $cdata['price_min'],
                'cat_data' => $cdata,
                'status' => $apptData['status'],
                'apptDate' => date('m-d-Y', strtotime($apptData['appt_date'])),
                'apptTime' => date('h:i a', strtotime($apptData['appt_date'])),
                'fName' => $apptData['provider']['fname'], 'lName' => $apptData['provider']['lname'],
                'mobile' => $apptData['provider']['mobile'],
                'email' => $apptData['provider']['email'], 'addr1' => urldecode($apptData['address1']),
                'addr2' => urldecode($apptData['address2']), 'can_fees' => $cdata['can_fees'],
                'pid' => $apptData['provider_id'],
                'services' => $apptData['services'],
                'fdata' => $fdata,
                'apptLat' => $apptData['appt_lat'], 'apptLong' => $apptData['appt_long'],
                'rev_sub' => $review_submited,
                'timer' => $apptData['timer'],
                'cust_reminder' => $apptData['cust_reminder'],
                'rating' => (float) $avgRow['avg'],
                'rev' => $apptRevRes['review'],
                'star_rating' => $star_rating,
                'apptret' => $apptRevRes['star_rating'],
                'timer_status' => $apptData['timer_status'],
                'timer_start_time' => $apptData['timer_start_time'],
                'date_time' => $apptData['appt_last_modified'],
                "helpTag" => "Need Help",
                "helpLink" => "support/need_help.html",
                "cancel_reason" => $apptData['cancel_reason'],
                "cancel_amount" => $cancel_amount,
                "disputed" => $apptData['disputed'],
                "dispute_msg" => $apptData['dispute_msg'],
                'pPic' => ($apptData['provider']['pic'] == "") ? $this->default_profile_pic : $apptData['provider']['pic']
            );
        } else {
            return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'],
                'bid' => $apptData['bid'], 'devId' => $userData['devId'], 'fName' => $apptData['provider']['fname'],
                'lName' => $apptData['provider']['lname'], 'mobile' => $apptData['provider']['mobile'],
                'email' => $apptData['provider']['email'],
                'addr1' => urldecode($apptData['address1']), 'addr2' => urldecode($apptData['address2']),
                'can_fees' => $cdata['can_fees'],
                'price_min' => $cdata['price_min'],
                'status' => $apptData['status'],
                'cat_data' => $cdata,
                'services' => $apptData['services'],
                'pPic' => ($apptData['provider']['pic'] == "") ? $this->default_profile_pic : $apptData['provider']['pic'],
                'apptDate' => date('m-d-Y', strtotime($apptData['appt_date'])),
                'apptTime' => date('h:i a', strtotime($apptData['appt_date'])),
                'bookType' => $apptData['booking_type'],
                'rating' => (float) $avgRow['avg'],
                'rev' => $apptRevRes['review'],
                'star_rating' => $star_rating,
                'apptret' => $apptRevRes['star_rating'],
                'apptLat' => $apptData['appt_lat'], 'apptLong' => $apptData['appt_long'],
                'pid' => $apptData['provider_id'], 'rev_sub' => $review_submited,
                'timer' => $apptData['timer'],
                'cust_reminder' => $apptData['cust_reminder'],
                'timer_status' => $apptData['timer_status'],
                'timer_start_time' => $apptData['timer_start_time'],
                "helpTag" => "Need Help",
                "helpLink" => "support/need_help.html",
                "cancel_reason" => $apptData['cancel_reason'],
                "cancel_amount" => $cancel_amount,
                "disputed" => $apptData['disputed'],
                "dispute_msg" => $apptData['dispute_msg'],
                'date_time' => $apptData['appt_last_modified']);
        }
    }

    protected function getallMessages($args) {

        if ($args['ent_bid'] == '' || $args['ent_user_type'] == '')
            return $this->_getStatusMessage(1, 'Missing Booking Id');

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], $args['ent_user_type']);
        if (is_array($returned))
            return $returned;

        $bookings = $this->mongo->selectCollection('bookings');
        $apptData = $bookings->findOne(array('bid' => (string) $args['ent_bid']));

        if (!$apptData)
            return $this->_getStatusMessage(62, 1);

        $errMsgArr = $this->_getStatusMessage(21, 2);
        if (!isset($apptData['messages']))
            $apptData['messages'] = array();
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'], 'messages' => $apptData['messages']);
    }

    /*
     * Method name: updateSlaveReview
     * Desc: Update appointment review of an appointment
     * Input: Request data
     * Output:  success if got it else error according to the result
     */

    protected function updateSlaveReview($args) {

        if ($args['ent_bid'] == '' || $args['ent_date_time'] == '' || $args['ent_rating_num'] == '')
            return $this->_getStatusMessage(1, 1);

        $this->curr_date_time = urldecode($args['ent_date_time']);

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '2');
        if (is_array($returned))
            return $returned;

        $booking = $this->mongo->selectCollection('bookings');
        $appt = $booking->findOne(array('bid' => (string) $args['ent_bid']));
        if (!$appt)
            return $this->_getStatusMessage(62, 62);

        $insertReviewQry = "insert into doctor_ratings(doc_id,patient_id,review_dt,star_rating,review,appointment_id) values('" . $appt['provider_id'] . "','" . $appt['customer']['id'] . "','" . $this->curr_date_time . "','" . $args['ent_rating_num'] . "','" . $args['ent_review_msg'] . "','" . $appt['bid'] . "')";
        mysql_query($insertReviewQry, $this->db->conn);

        $selectAvgRatQry = "select avg(star_rating) as avg from doctor_ratings where doc_id = '" . $appt['provider_id'] . "' and status = 1";
        $selectAvgRatRes = mysql_query($selectAvgRatQry, $this->db->conn);

        $avgRow = mysql_fetch_assoc($selectAvgRatRes);

        $location = $this->mongo->selectCollection('location');

        $location->update(array('user' => (int) $appt['provider_id']), array('$set' => array('rating' => (float) $avgRow['avg'])));
        return $this->_getStatusMessage(63, 12);
    }

    protected function getAllOngoingBookings($args) {

        if ($args['ent_cust_id'] == '')
            return $this->_getStatusMessage(1, 1);

        $this->curr_date_time = urldecode($args['ent_date_time']);

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '2');
        if (is_array($returned))
            return $returned;
//        $args['ent_cust_id'] = '9';

        $bookingColl = $this->mongo->selectCollection('bookings');
        $bookingCursor = $bookingColl->find(array('customer.id' => (string) $args['ent_cust_id'], 'status' => array('$in' => array(2, 5, 21, 22, 7))));
        $bookings = array();
        foreach ($bookingCursor as $appnt) {
            if ($appnt['disputed'] != 1)
                array_push($bookings, $appnt);
        }

        $appointments = array();
        $notReviewedAppts = array();
        $selRevArr = array();
        $selRevQry = "select appointment_id from doctor_ratings dr ";
        $selRevRes = mysql_query($selRevQry, $this->db->conn);
        while ($revappt = mysql_fetch_assoc($selRevRes)) {
            $selRevArr[] = $revappt['appointment_id'];
        }

        foreach ($bookings as $appnt) {

            $status = $this->GetMessageStatus($appnt['status']);
            $appointments[] = array('apntid' => $appnt['bid'], 'status' => $appnt['status'], 'st' => $status);
            if ($appnt['status'] == '7') {
                if (!in_array($appnt['bid'], $selRevArr)) {
                    $notReviewedAppts[] = $appnt['bid'];
                }
            }
        }
        $errNum = 31;
        if (count($appointments) <= 0)
            $errNum = 30;

        $errMsgArr = $this->_getStatusMessage($errNum, 2);

        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'], 'appointments' => $appointments, 'NotReviewedAppts' => $notReviewedAppts);
    }

    /*
     * Method name: getSlaveAppointments
     * Desc: Get patient appointments
     * Input: Request data
     * Output:  success if got it else error according to the result
     */

    protected function getSlaveAppts($args) {

        if ($args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, 'Missing Date and Time');
        else if ($args['ent_page_index'] == '')
            return $this->_getStatusMessage(1, 'Missing Page index');

        $this->curr_date_time = urldecode($args['ent_date_time']);
        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '2');
        if (is_array($returned))
            return $returned;
//        $this->User['entityId'] = '4';

        $pagelimit = ($args['ent_page_index'] * 10);

        $bookingColl = $this->mongo->selectCollection('bookings');

        $bookingCursor = $bookingColl->find(array('provider_id' => array('$ne' => "0"), 'status' => array('$in' => array(3, 4, 9, 10, 7)), 'customer.id' => (string) $this->User['entityId']))->limit(10)->skip($pagelimit)->sort(array((string) '_id' => -1));
        $bookingRes = array();
        foreach ($bookingCursor as $book) {
            array_push($bookingRes, $book);
        }

//        if (count($bookingRes) == 0)
//            return $this->_getStatusMessage(65, 2);

        $past_appointments = array();
        $counter = 0;
        foreach ($bookingRes as $appnt) {

            if ($appnt['pro_pic'] == '')
                $appnt['pro_pic'] = $this->default_profile_pic;


            $aptdate = date('Y-m-d', strtotime($appnt['appt_date']));
            $status = $this->__GetStatusMessageForCustomer($appnt['status']);
            if (isset($appnt['disputed']) && $appnt['disputed'] == 1)
                $status = "Booking Disputed";

            list($date, $time) = explode(' ', $appnt['appt_date']);
            list($year, $month, $day) = explode('-', $date);
            list($hour, $minute, $second) = explode(':', $time);

            $catcol = $this->mongo->selectCollection('Category');
            $catd = $catcol->findOne(array('_id' => new MongoId($appnt['cat_id'])));

            $fdata = array('sign_url' => $appnt['sign_url'], 'misc_fees' => $appnt['misc_fees'],
                'total_pro' => $appnt['total_pro'], 'pro_disc' => $appnt['pro_disc']);

            $selrev = "select star_rating from doctor_ratings where appointment_id = '" . $appnt['bid'] . "' and status = 1 LIMIT 1";
            $revres = mysql_fetch_assoc(mysql_query($selrev, $this->db->conn));
            $star_rating = 0;
            if ($revres['star_rating'] != "")
                $star_rating = (double) $revres['star_rating'];

            $dateNumber = $year . $month . $day . $hour . $minute . $second;
//            if (($appnt['status'] == '7' || $appnt['status'] == '9' || $appnt['status'] == '10' || $appnt['status'] == '3' || $appnt['status'] == '4')) 
            {
                $counter++;
                $fdata = (object) [];
                if ($counter <= 10)
                    $past_appointments[] = array('pid' => $appnt['provider']['id'], 'apntDt' => $appnt['appt_date'], 'pPic' => $appnt['provider']['pic'], 'email' => $appnt['provider']['email'],
                        'status' => $status,
                        'cancel_reason' => $appnt['cancel_reason'], 'dispute_msg' => $appnt['dispute_msg'],
                        'disputed' => $appnt['disputed'],
                        'fname' => $appnt['provider']['fname'], 'lname' => $appnt['provider']['lname'], 'phone' => $appnt['provider']['mobile'], 'apntTime' => date('h:i a', strtotime($appnt['appt_date'])),
                        'apntDate' => date('Y-m-d', strtotime($appnt['appt_date'])), 'apptLat' => $appnt['appt_lat'], 'apptLong' => $appnt['appt_long'], 'bid' => $appnt['bid'],
                        'star_rating' => $star_rating, 'fdata' => $fdata, 'cat_name' => $catd['cat_name'], 'addrLine1' => urldecode($appnt['address1']), 'addrLine2' => urldecode($appnt['address2']), 'notes' => $appnt['extra_notes'], 'bookType' => $appnt['booking_type'], 'statCode' => $appnt['status'], 'amount' => ($appnt['status'] == '9') ? $appnt['cancel_amount'] : ($appnt['status'] == '7' ? $appnt['comp']['total_pro'] : ''), 'cancelAmount' => (double) $appnt['cancel_amount'], 'dt' => $dateNumber);
            }
        }

        $bookingCursor = $bookingColl->find(array('provider_id' => array('$ne' => "0"), 'status' => array('$in' => array(2, 5, 6, 21, 22)), 'customer.id' => (string) $this->User['entityId']))->sort(array((string) '_id' => -1));
        $bookingRes = array();
        foreach ($bookingCursor as $book) {
            array_push($bookingRes, $book);
        }

//        if (count($bookingRes) == 0)
//            return $this->_getStatusMessage(65, 2);

        $ongoing_appointments = array();
        foreach ($bookingRes as $appnt) {
            if ($appnt['profile_pic'] == '')
                $appnt['profile_pic'] = $this->default_profile_pic;

            $aptdate = date('Y-m-d', strtotime($appnt['appt_date']));
            $status = $this->__GetStatusMessageForCustomer($appnt['status']);

            list($date, $time) = explode(' ', $appnt['appt_date']);
            list($year, $month, $day) = explode('-', $date);
            list($hour, $minute, $second) = explode(':', $time);

            $catcol = $this->mongo->selectCollection('Category');
            $catd = $catcol->findOne(array('_id' => new MongoId($appnt['cat_id'])));

            $fdata = array('sign_url' => $appnt['sign_url'], 'misc_fees' => $appnt['misc_fees'],
                'total_pro' => $appnt['total_pro'], 'pro_disc' => $appnt['pro_disc']);

            $selrev = "select star_rating from doctor_ratings where appointment_id = '" . $appnt['bid'] . "' and status = 1 LIMIT 1";
            $revres = mysql_fetch_assoc(mysql_query($selrev, $this->db->conn));
            $star_rating = 0;
            if ($revres['star_rating'] != "")
                $star_rating = (double) $revres['star_rating'];

            $dateNumber = $year . $month . $day . $hour . $minute . $second;
//            if ($appnt['status'] == '2' || $appnt['status'] == '5' || $appnt['status'] == '21' || $appnt['status'] == '22' || $appnt['status'] == '6') 
            {
                $fdata = (object) [];
                $ongoing_appointments [] = array('pid' => $appnt['provider']['id'], 'apntDt' => $appnt['appt_date'], 'pPic' => $appnt['provider']['pic'], 'email' => $appnt['provider']['email'],
                    'status' => $status,
                    'cancel_reason' => $appnt['cancel_reason'], 'dispute_msg' => $appnt['dispute_msg'],
                    'disputed' => $appnt['disputed'],
                    'fname' => $appnt['provider']['fname'], 'lname' => $appnt['provider']['lname'], 'phone' => $appnt['provider']['mobile'], 'apntTime' => date('h:i a', strtotime($appnt['appt_date'])),
                    'apntDate' => date('Y-m-d', strtotime($appnt['appt_date'])), 'apptLat' => $appnt['appt_lat'], 'apptLong' => $appnt['appt_long'], 'bid' => $appnt['bid'],
                    'star_rating' => $star_rating, 'fdata' => $fdata, 'cat_name' => $catd['cat_name'], 'addrLine1' => urldecode($appnt['address1']), 'addrLine2' => urldecode($appnt['address2']),
                    'notes' => $appnt['extra_notes'], 'bookType' => $appnt['booking_type'], 'statCode' => $appnt['status'],
                    'amount' => ($appnt['status'] == '9') ? $appnt['cancel_amount'] : ($appnt['status'] == '7' ? $appnt['comp']['total_pro'] : ''), 'cancelAmount' => (double) $appnt['cancel_amount'], 'dt' => $dateNumber);
            }
        }

        $errNum = 31;
        if (count($past_appointments) <= 0 && count($ongoing_appointments) <= 0)
            $errNum = 30;

        $errMsgArr = $this->_getStatusMessage($errNum, 2);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'],
            'errMsg' => $errMsgArr['errMsg'], 'total_bookings' => $counter,
            'lastcount' => ($counter >= 10 ? 0 : 1 ), 'past_appts' => $past_appointments,
            'ongoing_appts' => $ongoing_appointments);
    }

    protected function raiseInvoice($args) {
        if ($args['ent_bid'] == '' || $args['ent_minutes'] == '')
            return $this->_getStatusMessage(1, 1);
//        $args['ent_minutes'] = 5;
        $this->curr_date_time = urldecode($args['ent_date_time']);

        $bookingColl = $this->mongo->selectCollection('bookings');
        $apptDet = $bookingColl->findOne(array('bid' => $args['ent_bid']));

        if (!$apptDet)
            return $this->_getStatusMessage(77, 1);

        $bookingColl->update(array('bid' => $args['ent_bid']), array('$set' => array('duration' => $args['ent_minutes'])));


        $proColl = $this->mongo->selectCollection('Category');
        $prodata = $proColl->findOne(array('_id' => new MongoId($apptDet['cat_id'])));

        if ($prodata['fee_type'] == 'Hourly') {
            $totalAmt = $args['ent_minutes'] * $prodata['price_min'] / 60;
            $visitAmt = $prodata['visit_fees'];
        } else if ($prodata['fee_type'] == 'Mileage') {
            $totalAmt = 0;
            $visitAmt = 0;
        } else if ($prodata['fee_type'] == 'Fixed') {
            $totalAmt = 0;
            $visitAmt = 0;
        } else {
            $totalAmt = 0;
            $visitAmt = 0;
        }

        $visit_fees = $apptDet['visit_fees'];
        $services_total = 0;
        if (isset($apptDet['services']) && is_array($apptDet['services'])) {
            foreach ($apptDet['services'] as $services) {
                $services_total += $services['sprice'];
            }
        }
        if ($apptDet['coupon_type'] == 1) {
            $forDisc = $visit_fees + $totalAmt + $services_total;
            $coup_dicount = ($forDisc * $apptDet['coupon_discount']) / 100;
        } else {
            $coup_dicount = $apptDet['coupon_discount'];
        }

        $errMsgArr = $this->_getStatusMessage(114, 2);
        $totalAmt = str_replace(",", ".", $totalAmt);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'],
            'errMsg' => $errMsgArr['errMsg'],
            'coupon_discount' => $coup_dicount,
            'coupon_per' => $apptDet['coupon_discount'],
            'coupon_type' => $apptDet['coupon_type'],
            'visitAmt' => $visitAmt, 'totalAmt' => $totalAmt);
    }

    protected function AcceptLareBookingDirect($bid, $provider_id) {

        $bid = (string) $bid;

        $bookings = $this->mongo->selectCollection('bookings');
        $bookingsRes = $bookings->findOne(array('bid' => (string) $bid));

        $argstopass = array('bid' => $bid);
        $this->sendDatatoSocket($argstopass, "laterBookingByCustomer");

        if (!$bookingsRes)
            return $this->_getStatusMessage(77, $bookingsRes);

        $patData = $this->_getEntityDet($bookingsRes['customer']['email'], '2');

        $locationColl = $this->mongo->selectCollection('location');
        $userData = $locationColl->findOne(array('user' => (int) $provider_id));

        $couponsColl = $this->mongo->selectCollection('coupons');
        $one = $couponsColl->findOne(array('coupon_code' => $bookingsRes['coupon_code']));

        if ($one['coupon_type'] == '3') {
            $couponsColl->update(array('coupon_code' => $bookingsRes['coupon_code'], 'bookings.booking_id' => (int) $bookingsRes['bid']), array('$set' => array('bookings.$.status' => 2)));
        } else {
            $couponsColl->update(array('coupon_code' => $bookingsRes['coupon_code']), array('$push' => array('usage' => array('booking_id' => $bookingsRes['bid'], 'slave_id' => (int) $patData['patient_id'], 'status' => 1, 'amount' => $bookingsRes['amount'], 'sub_total' => $bookingsRes['sub_total'], 'discount' => $bookingsRes['coupon_discount']))));
        }
        $googleDist = $this->get_DirectionFormMatrix($bookingsRes['appt_lat'], $bookingsRes['appt_long'], $userData['location']['latitude'], $userData['location']['longitude']);
        $dis_in_km = (float) ($googleDist['distance'] / $this->distanceMetersByUnits);
        $dis_in_met = $googleDist['distance'];
        $categoryData = $this->mongo->selectCollection('Category')->findOne(array('_id' => new MongoId($bookingsRes['cat_id'])));

        $provider = array('id' => $userData['user'], 'fname' => $userData['name'],
            'lname' => $userData['lname'], 'pic' => $userData['image'],
            'email' => $userData['email'], 'mobile' => $userData['mobile']);

        $dispatched[] = array('pid' => (string) $userData['user'], 'name' => $userData['name'],
            'email' => $userData['email'], 'mobile' => $userData['mobile'],
            'status_msg' => "Booking Accepted", 'status' => 1, 'res' => "2");

        $slot = $this->mongo->selectCollection('slotbooking');
        $slotdata = $slot->findOne(array('_id' => new MongoId($bookingsRes['slot_id'])));

        $bookings->update(array('bid' => $bid), array('$set' => array('provider' => $provider,
                'dispatched' => $dispatched,
                'appt_date' => $slotdata['start_dt'],
                'status' => 2, 'provider_id' => (string) $provider_id,
                'accepted_dt' => $this->curr_date_time, 'pro_commission' => $userData['Commission'],
                'distance_km' => $dis_in_km,
                'distance_met' => $dis_in_met,
                'fee_type' => $categoryData['fee_type'], 'price_min' => $categoryData['price_min'],
                'visit_fees' => $categoryData['visit_fees'])));




        $dt = explode(' ', $this->curr_date_time);
        $date = explode('-', $dt[0]);
        $days = explode(':', $dt[1]);
        $start = mktime($days[0], $days[1], $days[2], $date[1], $date[2], $date[0]);
        $check = ($bookingsRes['slot_id'] != '') ? array('_id' => new MongoId($bookingsRes['slot_id'])) : array('start' => $start, 'doc_id' => (int) $this->User['entityId']);
        $update = $slot->update($check, array('$set' => array('booked' => 3, 'status' => 3)));


        $bookings = $this->mongo->selectCollection('bookings');
        $bookingsRes = $bookings->findOne(array('bid' => (string) $bid));
        $mail = new sendAMail(APP_SERVER_HOST);
        $mailArr[] = $mail->ApptConfirmed($bookingsRes);
        $mailArr[] = $mail->NewLateBooking($bookingsRes);
    }

    protected function sendDatatoSocket($argstopass, $servicename) {
        $json = json_encode($argstopass);
        $max_exe_time = 250; // time in milliseconds
        $nodeServiceUrl = "http://localhost:9999/" . $servicename;
        $ch = curl_init($nodeServiceUrl);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, $max_exe_time);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(//<--- Added this code block
            'Content-Type: application/json',
            'Content-Length: ' . strlen($json))
        );
        curl_exec($ch);
        curl_close($ch);
    }

    /*
     * Method name: respondToAppointment
     * Desc: Respond to appointment requeted
     * Input: Request data
     * Output:  success if got it else error according to the result
     */

    protected function respondToAppointment($args) {

        if ($args['ent_proid'] == '' || $args['ent_bid'] == '' || $args['ent_appnt_dt'] == '' || $args['ent_response'] == '' || $args['ent_pat_email'] == '' || $args['ent_book_type'] == '' || $args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, 1);

        $this->curr_date_time = urldecode($args['ent_date_time']);
        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
        if (is_array($returned))
            return $returned;

        $args['ent_appnt_dt'] = urldecode($args['ent_appnt_dt']);
        $patData = $this->_getEntityDet($args['ent_pat_email'], '2');

        $bookings = $this->mongo->selectCollection('bookings');
        $bookingsRes = $bookings->findOne(array('bid' => $args['ent_bid']));

        if (!$bookingsRes)
            return $this->_getStatusMessage(77, $bookings);

        if ($bookingsRes['status'] == '2')
            return $this->_getStatusMessage(84, 3);
        if ($bookingsRes['status'] == '4' || $bookingsRes['status'] == '9')
            return $this->_getStatusMessage(41, 3);
        if ($bookingsRes['status'] == '10')
            return $this->_getStatusMessage(89, 3);
        if ($bookingsRes['status'] == '8')
            return $this->_getStatusMessage(77, 3);

        $locationColl = $this->mongo->selectCollection('location');
        $userData = $locationColl->findOne(array('user' => (int) $this->User['entityId']));
        $locationColl->update(array('user' => (int) $args['ent_proid']), array('$set' => array('popup' => 0)));

        if ($args['ent_response'] == 2)
            $resmsg = "Booking Accepted";
        if ($args['ent_response'] == 3)
            $resmsg = "Booking Rejected";
        $bookings->update(array('bid' => $args['ent_bid'], 'dispatched.pid' => (string) $args['ent_proid']), array('$set' => array('dispatched.$.res' => (string) $args['ent_response'], 'dispatched.$.status_msg' => $resmsg)));


        $provider = array('id' => $args['ent_proid'], 'fname' => $this->User['firstName'],
            'lname' => $this->User['lastName'], 'pic' => $this->User['pPic'],
            'email' => $this->User['email'], 'mobile' => $this->User['mobile']);

        // if Booking Accepted
        if ($args['ent_response'] == '2') {

            $CountOnGoingJob = $bookings->find(array('booking_type' => 1, 'provider_id' => $args['ent_proid'], 'status' => array('$in' => array(2, 5, 6, 21, 22))))->count();
            if ($CountOnGoingJob > 0 && !isset($bookingsRes['disp_status'])) {
                return $this->_getStatusMessage(85, 85);
            }

            $couponsColl = $this->mongo->selectCollection('coupons');

            if ($bookingsRes['coupon_code'] != '') {
                $data = array('booking_id' => (int) $args['ent_bid'], 'slave_id' => $bookingsRes['customer']['id'], 'email' => $bookingsRes['customer']['email']);
                $couponsColl->update(array('_id' => $bookingsRes['coupon_id']), array('$push' => array('bookings' => $data)));
            }

            $one = $couponsColl->findOne(array('coupon_code' => $bookingsRes['coupon_code']));
            if ($one['coupon_type'] == '3') {
                $couponsColl->update(array('coupon_code' => $bookingsRes['coupon_code'], 'bookings.booking_id' => (int) $bookingsRes['bid']), array('$set' => array('bookings.$.status' => 2)));
            } else {
                $couponsColl->update(array('coupon_code' => $bookingsRes['coupon_code']), array('$push' => array('usage' => array('booking_id' => $bookingsRes['bid'], 'slave_id' => (int) $patData['patient_id'], 'status' => 1, 'amount' => $bookingsRes['amount'], 'sub_total' => $bookingsRes['sub_total'], 'discount' => $bookingsRes['coupon_discount']))));
            }
            $notifType = 2;
            $message = "Your appointment with " . $this->User['firstName'] . ' ' . $this->User['lastName'] . " is confirmed for " . date('m/d/Y h:i a', strtotime($args['ent_appnt_dt'])) . " on " . APP_NAME . " " . CUSTOMER . "!";

            $googleDist = $this->get_DirectionFormMatrix($bookingsRes['appt_lat'], $bookingsRes['appt_long'], $userData['location']['latitude'], $userData['location']['longitude']);
            $dis_in_km = (float) ($googleDist['distance'] / $this->distanceMetersByUnits);
            $dis_in_met = $googleDist['distance'];

            $categoryData = $this->mongo->selectCollection('Category')->findOne(array('_id' => new MongoId($bookingsRes['cat_id'])));

            // make provider online again
            $locationColl->update(array('user' => (int) $this->User['entityId']), array('$set' => array('booked' => 1)));

            $bookings->update(array('bid' => $args['ent_bid']), array('$set' =>
                array('provider' => $provider,
                    'status' => 2, 'provider_id' => $args['ent_proid'],
                    'accepted_dt' => $this->curr_date_time,
                    'pro_commission' => $userData['Commission'],
                    'distance_km' => $dis_in_km,
                    'distance_met' => $dis_in_met,
                    'time.accepted_ts' => time(),
                    'time.accepted_dt' => $args['ent_date_time'],
                    'fee_type' => $categoryData['fee_type'],
                    'price_min' => $categoryData['price_min'],
                    'cancel_amount' => $categoryData['can_fees'],
                    'visit_fees' => $categoryData['visit_fees'])));

            $mail = new sendAMail(APP_SERVER_HOST);
            $bookingsResFormMail = $bookings->findOne(array('bid' => $args['ent_bid']));
            $mailArr['emailRes_p'] = $mail->ApptConfirmed($bookingsResFormMail);
            $mailArr['emailRes_c'] = $mail->ApptConfirmedSentToCustomer($bookingsResFormMail);
        } else {
            $notifType = 3;
            $message = "We are sorry the chosen " . PROVIDER . " will not be able to see you, please choose another " . PROVIDER . ".";
            $bookings->update(array('bid' => $args['ent_bid']), array('$set' => array('provider' => $provider, 'status' => 3, 'provider_id' => $args['ent_proid'])));
        }

        // if  its Later Booking
        if ($bookingsRes['booking_type'] == '2') {
            $slot = $this->mongo->selectCollection('slotbooking');
            $dt = explode(' ', $args['ent_appnt_dt']);
            $date = explode('-', $dt[0]);
            $days = explode(':', $dt[1]);
            $start = mktime($days[0], $days[1], $days[2], $date[1], $date[2], $date[0]);
            $check = ($bookingsRes['slot_id'] != '') ? array('_id' => new MongoId($bookingsRes['slot_id'])) : array('start' => $start, 'doc_id' => (int) $this->User['entityId']);
            if ($args['ent_response'] == '2')
                $update = $slot->update($check, array('$set' => array('booked' => 3, 'status' => 3)));
            else
                $update = $slot->update($check, array('$set' => array('booked' => 1, 'status' => 1)));
        }

        $pushNum['user'] = $userData;
        $aplPushContent = array('alert' => $message, 'sound' => 'default', 'st' => $notifType, 'chn' => $userData['chn'], 'dt' => $args['ent_appnt_dt'], 'email' => $this->User['email'], 'bid' => $bookingsRes['bid']);
        $andrPushContent = array("payload" => $message, 'st' => $notifType, 'sname' => $this->User['firstName'] . ' ' . $this->User['lastName'] . ' ' . $this->User['degree'], 'dt' => $args['ent_appnt_dt'], 'email' => $this->User['email'], 'bid' => $bookingsRes['bid']);
        if ($args['ent_response'] == '2') {
            $pushNum['push'] = $this->_sendPush($this->User['entityId'], array($patData['patient_id']), $message, '2', $this->User['firstName'], $this->curr_date_time, '2', $aplPushContent, $andrPushContent);
        }

        $argstopass = array('bid' => $args['ent_bid'], 'proid' => $args['ent_proid'], 'bstatus' => $args['ent_response']);
        $this->sendDatatoSocket($argstopass, "respondToAppointment");

        return $this->_getStatusMessage(40, 40);
    }

    /*
     * Method name: respondToAppointment
     * Desc: Respond to appointment requeted
     * Input: Request data
     * Output:  success if got it else error according to the result
     */

    protected function respondFromAdmin($args) {

        if ($args['ent_doc_id'] == '' || $args['ent_appnt_dt'] == '' || $args['ent_response'] == '' || $args['ent_pat_email'] == '' || $args['ent_book_type'] == '' || $args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, 1);


        $this->curr_date_time = urldecode($args['ent_date_time']);

        $args['ent_appnt_dt'] = urldecode($args['ent_appnt_dt']);

        $patData = $this->_getEntityDet($args['ent_pat_email'], '2');

        $getApptDetQry = "select amount,sub_total,discount,coupon_code,status,slot_id,booking_type,appointment_id from appointment where doc_id = '" . $args['ent_doc_id'] . "' and appointment_dt = '" . $args['ent_appnt_dt'] . "' and patient_id = '" . $patData['patient_id'] . "'";
        $apptDet = mysql_fetch_assoc(mysql_query($getApptDetQry, $this->db->conn));

        if (!is_array($apptDet))
            return $this->_getStatusMessage(77, $getApptDetQry);

        if ($apptDet['status'] == '2' || $apptDet['status'] == '3')
            return $this->_getStatusMessage(84, 3);

        if ($apptDet['status'] == '4' || $apptDet['status'] == '9')
            return $this->_getStatusMessage(41, 3);

        if ($apptDet['status'] == '10')
            return $this->_getStatusMessage(89, 3);

        if ($apptDet['status'] == '8')
            return $this->_getStatusMessage(77, 3);

        $oneHourBefore = date('Y-m-d H:i:s', (strtotime($args['ent_appnt_dt']) - 60 * 60));
        $oneHourAfter = date('Y-m-d H:i:s', (strtotime($args['ent_appnt_dt']) + 60 * 60));

        if ($args['ent_response'] == '2') {

            if ($args['ent_book_type'] == '1')
                $checkApptStr = "appointment_dt between '" . $oneHourBefore . "' and '" . $oneHourAfter . "' and status IN (2,5,6)";
            else
                $checkApptStr = "appointment_dt = '" . $args['ent_appnt_dt'] . "' and status IN (2,5,6)";

            $getApptDetQry = "select status from appointment where doc_id = '" . $args['ent_doc_id'] . "' and " . $checkApptStr;

            if (mysql_num_rows(mysql_query($getApptDetQry, $this->db->conn)) > 0)
                return $this->_getStatusMessage(60, 60);
        }

        $updateString = '';
        $updateResponseQry = "update appointment set status = '" . $args['ent_response'] . "',accepted_dt = '" . $this->curr_date_time . "'" . $updateString . " where doc_id = '" . $args['ent_doc_id'] . "' and patient_id = '" . $patData['patient_id'] . "' and appointment_dt = '" . $args['ent_appnt_dt'] . "' and status = 1 ORDER BY appointment_id DESC LIMIT 1";
        mysql_query($updateResponseQry, $this->db->conn);

        if (mysql_affected_rows() <= 0)
            return $this->_getStatusMessage(3, 3);

        $location = $this->mongo->selectCollection('location');
        $userData = $location->findOne(array('user' => (int) $args['ent_doc_id']));

        if ($args['ent_response'] == '2') {

            if ($apptDet['coupon_code'] != '') {

                $couponsColl = $this->mongo->selectCollection('coupons');

                $one = $couponsColl->findOne(array('coupon_code' => $apptDet['coupon_code']));

                if ($one['coupon_type'] == '3') {
                    $couponsColl->update(array('coupon_code' => $apptDet['coupon_code'], 'bookings.booking_id' => (int) $apptDet['bid']), array('$set' => array('bookings.$.status' => 2)));
                } else {
                    $couponsColl->update(array('coupon_code' => $apptDet['coupon_code']), array('$push' => array('usage' => array('booking_id' => $apptDet['bid'], 'slave_id' => (int) $patData['patient_id'], 'status' => 1, 'amount' => $apptDet['amount'], 'sub_total' => $apptDet['sub_total'], 'discount' => $apptDet['discount']))));
                }

                $couponsColl->update(array('coupon_code' => $apptDet['coupon_code']), array('$set' => array('status' => 1)));
            }
            $notifType = 2;
            $message = "Your appointment with " . $userData['name'] . ' ' . $userData['lname'] . " is confirmed for " . date('m/d/Y h:i a', strtotime($args['ent_appnt_dt'])) . " on PriveMD!";
        } else {
            $notifType = 6;
            $message = "We are sorry the chosen Provider will not be able to see you, please choose another Provoder.";
        }

        if ($args['ent_book_type'] == '2') {

            $slot = $this->mongo->selectCollection('slotbooking');

            $dt = explode(' ', $args['ent_appnt_dt']);

            $date = explode('-', $dt[0]);

            $days = explode(':', $dt[1]);

            $start = mktime($days[0], $days[1], $days[2], $date[1], $date[2], $date[0]);

            $check = ($apptDet['slot_id'] != '') ? array('_id' => new MongoId($apptDet['slot_id'])) : array('start' => $start, 'doc_id' => (int) $args['ent_doc_id']);

            if ($args['ent_response'] == '3')
                $update = $slot->update($check, array('$set' => array('booked' => 1, 'status' => 3)));
            else if ($args['ent_response'] == '2')
                $update = $slot->update($check, array('$set' => array('booked' => 3, 'status' => 4)));
            else
                $update = $slot->update($check, array('$set' => array('status' => 4)));
        }

        if ($args['ent_book_type'] == '2') {
            $pubnubContent = array('a' => 15, 'bid' => $apptDet['bid'], 'slot' => $apptDet['slot_id']);
        }

        $pushNum['user'] = $userData;
        $aplPushContent = array('alert' => $message, 'st' => $notifType, 'chn' => $userData['chn'], 'dt' => $args['ent_appnt_dt'], 'email' => $userData['email']);
        $andrPushContent = array("payload" => $message, 'st' => $notifType, 'sname' => $userData['name'] . ' ' . $userData['lname'], 'dt' => $args['ent_appnt_dt'], 'email' => $userData['email'], 'c' => $userData['chn']);
        $pushNum['push'] = $this->_sendPush($args['ent_doc_id'], array($patData['patient_id']), $message, '2', $userData['name'], $this->curr_date_time, '2', $aplPushContent, $andrPushContent);



        if ($args['ent_response'] == "2")
            $message1 = "You accepted a booking from admin.";
        else if ($args['ent_response'] == "3")
            $message1 = "You Rejected a booking from admin.";
        else
            $message1 = "Appointment";

        $aplPushContent1 = array('alert' => $message1, 'st' => '2');
        $andrPushContent1 = array("payload" => $message1, 'st' => '2');

        $pushNum['push1'] = $this->_sendPush($patData['patient_id'], array($args['ent_doc_id']), $message1, '1', $userData['name'], $this->curr_date_time, '1', $aplPushContent1, $andrPushContent1);

        $pushNum['upd'] = $update;
        return $this->_getStatusMessage(40, $pushNum);
    }

    protected function updateApptStatus($args) {

        if ($args['ent_bid'] == '' || $args['ent_appnt_dt'] == '' || $args['ent_response'] == '' || $args['ent_pat_email'] == '' || $args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, 1);

        if ($args['ent_response'] == '7') {
            if ($args['ent_signature_url'] == "" || $args['ent_total_pro'] == "") {
                return $this->_getStatusMessage(1, 1);
            }
            $args['ent_total_pro'] = str_replace(",", ".", $args['ent_total_pro']);
        }

        $this->curr_date_time = urldecode($args['ent_date_time']);
        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
        if (is_array($returned))
            return $returned;
        $args['ent_appnt_dt'] = urldecode($args['ent_appnt_dt']);

        $patData = $this->_getEntityDet($args['ent_pat_email'], '2');

        $bookingColl = $this->mongo->selectCollection('bookings');
        $apptDet = $bookingColl->findOne(array('bid' => $args['ent_bid']));

        if ($apptDet['status'] == '3')
            return $this->_getStatusMessage(88, 3);

        if ($apptDet['status'] == '4' || $apptDet['status'] == '9')
            return $this->_getStatusMessage(41, 3);

        if ($apptDet['status'] == '10')
            return $this->_getStatusMessage(89, 77);

        if ($apptDet['status'] == '8')
            return $this->_getStatusMessage(77, 77);

        $bookings = $this->mongo->selectCollection('bookings');

        if ($args['ent_response'] == '5') {
            $message = 'Provider on the way';
            $noteType = '5';
            $errNum = 57;
            $location = $this->mongo->selectCollection('location');
            $userData = $location->findOne(array('user' => (int) $this->User['entityId']));
            $aplPushContent = array('alert' => $message, 'sound' => 'default', 'bid' => $apptDet['bid'], 'st' => $noteType, 'sname' => $this->User['firstName'] . ' ' . $this->User['lastName'] . ', ' . $this->User['degree'], 'pic' => $this->User['pPic'], 'ph' => $this->User['mobile'], 'e' => $this->User['email'], 'ltg' => $apptDet['appt_lat'] . ',' . $apptDet['appt_long'], 'd' => $apptDet['appointment_dt'], 'c' => $userData['chn'], 'r' => round($userData['rating']));
            $andrPushContent = array("payload" => $message, 'bid' => $apptDet['bid'], 'st' => $noteType, 'sname' => $this->User['firstName'] . ' ' . $this->User['lastName'] . ' ' . $this->User['degree'], 'pic' => $this->User['pPic'], 'ph' => $this->User['mobile'], 'e' => $this->User['email'], 'email' => $this->User['email'], 'ltg' => $apptDet['appt_lat'] . ',' . $apptDet['appt_long'], 'd' => $apptDet['appointment_dt'], 'c' => $userData['chn'], 'dl' => $userData['location']['latitude'] . ',' . $userData['location']['longitude']);
            $bookings->update(array('bid' => $args['ent_bid']), array('$set' => array('time.ontheway_ts' => time(), 'time.ontheway_dt' => $args['ent_date_time'])));
        } else if ($args['ent_response'] == '21') {
            $message = 'Provider has arrived!';
            $noteType = '21';
            $errNum = 58;
            $aplPushContent = array('alert' => $message, 'sound' => 'default', 'bid' => $apptDet['bid'], 'st' => $noteType, 'sname' => $this->User['firstName'] . ' ' . $this->User['lastName'] . ', ' . $this->User['degree'], 'dt' => $args['ent_appnt_dt'], 'pic' => $this->User['pPic'], 'ph' => $this->User['mobile'], 'email' => $this->User['email'], 'r' => number_format($apptDet['doc_rating'], 1, '.', '')); //
            $andrPushContent = array("payload" => $message, 'bid' => $apptDet['bid'], 'st' => $noteType, 'sname' => $this->User['firstName'] . ' ' . $this->User['lastName'] . ' ' . $this->User['degree'], 'dt' => $args['ent_appnt_dt'], 'pic' => $this->User['pPic'], 'ph' => $this->User['mobile'], 'email' => $this->User['email'], 'r' => number_format($apptDet['doc_rating'], 1, '.', ''), 'd' => date("h:i a", strtotime($args['ent_appnt_dt'])));
            $bookings->update(array('bid' => $args['ent_bid']), array('$set' => array('time.arrived_ts' => time(), 'time.arrived_dt' => $args['ent_date_time'])));
        } else if ($args['ent_response'] == '6') {
            $message = 'Job Started!';
            $noteType = '6';
            $errNum = 59;
            $aplPushContent = array('alert' => $message, 'sound' => 'default', 'bid' => $apptDet['bid'], 'st' => $noteType, 'sname' => $this->User['firstName'] . ' ' . $this->User['lastName'] . ', ' . $this->User['degree'], 'dt' => $args['ent_appnt_dt'], 'pic' => $this->User['pPic'], 'ph' => $this->User['mobile'], 'e' => $this->User['email'], 'r' => number_format($apptDet['doc_rating'], 1, '.', '')); //
            $andrPushContent = array("payload" => $message, 'bid' => $apptDet['bid'], 'st' => $noteType, 'sname' => $this->User['firstName'] . ' ' . $this->User['lastName'] . ' ' . $this->User['degree'], 'dt' => $args['ent_appnt_dt'], 'pic' => $this->User['pPic'], 'ph' => $this->User['mobile'], 'smail' => $this->User['email'], 'r' => number_format($apptDet['doc_rating'], 1, '.', ''), 'd' => date("h:i a", strtotime($args['ent_appnt_dt'])));
            $bookings->update(array('bid' => $args['ent_bid']), array('$set' => array('time.jobstart_ts' => time(), 'time.jobstart_dt' => $args['ent_date_time'])));
        } else if ($args['ent_response'] == '22') {
            $message = 'Job Completed and Invoice Pending!';
            $noteType = '22';
            $errNum = 60;
            $aplPushContent = array('alert' => $message, 'sound' => 'default', 'bid' => $apptDet['bid'], 'st' => $noteType, 'sname' => $this->User['firstName'] . ' ' . $this->User['lastName'] . ', ' . $this->User['degree'], 'dt' => $args['ent_appnt_dt'], 'pic' => $this->User['pPic'], 'ph' => $this->User['mobile'], 'email' => $this->User['email'], 'r' => number_format($apptDet['doc_rating'], 1, '.', ''));
            $andrPushContent = array("payload" => $message, 'bid' => $apptDet['bid'], 'st' => $noteType, 'sname' => $this->User['firstName'] . ' ' . $this->User['lastName'] . ' ' . $this->User['degree'], 'dt' => date("h:i a", strtotime($args['ent_appnt_dt'])), 'pic' => $this->User['pPic'], 'ph' => $this->User['mobile'], 'email' => $this->User['email'], 'r' => number_format($apptDet['doc_rating'], 1, '.', ''));
            $bookings->update(array('bid' => $args['ent_bid']), array('$set' => array('invoiceraise_server' => time(), 'invoiceraise_device' => strtotime($args['ent_date_time']))));
            // update timer
            if ($args['ent_timer'] != '') {
                $bookings->update(array('bid' => $args['ent_bid']), array('$set' => array('timer' => $args['ent_timer'], 'timer_status' => 3)));
            }
            $bookings->update(array('bid' => $args['ent_bid']), array('$set' => array('timer_start' => $args['ent_timer'], 'time.jobend_ts' => time(), 'time.jobend_dt' => $args['ent_date_time'])));
        } else if ($args['ent_response'] == '7') {
            $message = 'Job completed Final Invoice Sent!';
            $noteType = '7';
            $errNum = 61;
            $aplPushContent = array('alert' => $message, 'sound' => 'default', 'bid' => $apptDet['bid'], 'st' => $noteType, 'sname' => $this->User['firstName'] . ' ' . $this->User['lastName'] . ', ' . $this->User['degree'], 'd' => $args['ent_appnt_dt'], 'email' => $this->User['email'], 'r' => number_format($apptDet['doc_rating'], 1, '.', ''));
            $andrPushContent = array("payload" => $message, 'bid' => $apptDet['bid'], 'st' => $noteType, 'sname' => $this->User['firstName'] . ' ' . $this->User['lastName'] . ' ' . $this->User['degree'], 'dt' => $args['ent_appnt_dt'], 'email' => $this->User['email'], 'r' => number_format($apptDet['doc_rating'], 1, '.', ''));

            $apptDet = $bookings->findOne(array('bid' => (string) $args['ent_bid']));

            if ($apptDet['payment_type'] == 2) {
                $chargeCustomerArr = array('stripe_id' => $patData['stripe_id'], 'amount' => (int) ((float) $args['ent_total_pro'] * 100), 'currency' => 'USD', 'description' => 'From ' . $patData['email']);
                $trStripe = $this->stripe->apiStripe('chargeCard', $chargeCustomerArr);
                $taranseferId = "";
                if ($trStripe['error']) {
                    $charge = array('errNum' => 16, 'errFlag' => 1, 'errMsg' => $trStripe['error']['message'], 'test' => 1);
                } else {
                    $taranseferId = $trStripe['id'];
                }
            } else {
                $taranseferId = "cash_000_" . $apptDet['bid'];
            }

            $services_total = 0;
            if (isset($apptDet['services']) && is_array($apptDet['services'])) {
                foreach ($apptDet['services'] as $services) {
                    $services_total += $services['sprice'];
                }
            }
            $args['ent_time_fees'] = str_replace(",", ".", $args['ent_time_fees']);
            $sub_total_cust = $args['ent_misc_fees'] + $args['ent_mat_fees'] + $args['ent_time_fees'] + $apptDet['visit_fees'] + $services_total;
            $comp = array('ctime' => $this->curr_date_time,
                'sign_url' => $args['ent_signature_url'], 'misc_fees' => $args['ent_misc_fees'],
                'total_pro' => $args['ent_total_pro'], 'pro_disc' => $args['ent_pro_disc'],
                'mat_fees' => $args['ent_mat_fees'], 'complete_note' => $args['ent_notes'],
//                'time_fees' => 5, 'sub_total_cust' => $sub_total_cust);
                'time_fees' => $args['ent_time_fees'], 'sub_total_cust' => $sub_total_cust);
            $bookings->update(array('bid' => $args['ent_bid']), array('$set' => array('comp' => $comp, 'time.completed_ts' => time(), 'time.completed_dt' => $args['ent_date_time'])));

            // make provider online again
            $locationColl = $this->mongo->selectCollection('location');
            $locationColl->update(array('user' => (int) $this->User['entityId']), array('$set' => array('booked' => 0)));
        } else {
            return $this->_getStatusMessage(56, 56);
        }

        $bookings->update(array('bid' => $args['ent_bid']), array('$set' => array('status' => (int) $args['ent_response'])));

        $apptDet['appt_date'] = date('Y-m-d H:i:s', strtotime($apptDet['appt_date']) + (int) $this->User['offset']);
        $apptDet['created_dt'] = date('Y-m-d H:i:s', strtotime($apptDet['created_dt']) + (int) $this->User['offset']);

        $location = $this->mongo->selectCollection('location');
        if ($args['ent_response'] == '7') {
            $set = array('$set' => array('status' => 3, 'apptStatus' => 0));
        } else {
            $set = array('$set' => array('status' => 5, 'apptStatus' => $args['ent_response']));
        }
        $location->update(array('user' => (int) $this->User['entityId']), $set);

        if ($args['ent_response'] == '7') {

            $updateInvoice = $this->__GenerateInvoiceDetails($args['ent_bid'], $taranseferId);
            if (!isset($updateInvoice['invoiceData']))
                return $this->_getStatusMessage(56, $updateInvoice);
            else {
                $bookings->update(array("bid" => (string) $args['ent_bid']), array('$set' => array('invoiceData' => $updateInvoice['invoiceData'])));
                $apptDet = $bookingColl->findOne(array('bid' => (string) $args['ent_bid']));
                $mail = new sendAMail(APP_SERVER_HOST);
                $push['emailRes'] = $mail->sendInvoice($apptDet);
            }

            if ($apptDet['booking_type'] == '2') {
                $slot = $this->mongo->selectCollection('slotbooking');
                $dt = explode(' ', $args['ent_appnt_dt']);
                $date = explode('-', $dt[0]);
                $days = explode(':', $dt[1]);
                $start = mktime($days[0], $days[1], $days[2], $date[1], $date[2], $date[0]);
                if ($apptDet['slot_id'] != '')
                    $check = array("_id" => new MongoId($apptDet['slot_id']));
                else
                    $check = array('start' => $start, 'doc_id' => (int) $this->User['entityId']);
                $slotUpdate = $slot->update($check, array('$set' => array('booked' => 4)));

                $location = $this->mongo->selectCollection('location');
                $userData = $location->findOne(array('user' => (int) $this->User['entityId']));
                $pubnubContent = array('a' => 16, 'bid' => $apptDet['bid'], 'slot' => $apptDet['slot_id']);
            }
        }
        $push = $this->_sendPush($this->User['entityId'], array($patData['patient_id']), $message, $noteType, $this->User['email'], $this->curr_date_time, '2', $aplPushContent, $andrPushContent);

        $testingc = $this->mongo->selectCollection('testing');
        $testingc->insert(array('testtingp' => $push));

        $pubnubContent = array('a' => 14, 'bid' => $apptDet['bid'], 's' => $args['ent_response'], 'm' => $message);
        $out = array('push' => $push, 'mail' => $invoMail, 'updateStatus' => $update, 'slot' => $slotUpdate);

        $argstopass = array('bid' => $args['ent_bid'], 'proid' => $this->User['entityId'], 'bstatus' => $args['ent_response']);
        $this->sendDatatoSocket($argstopass, "respondToAppointment");

        return $this->_getStatusMessage($errNum, $out);
    }

    protected function CanIStartJob($args) {

        if ($args['ent_bid'] == '')
            return $this->_getStatusMessage(1, 1);

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
        if (is_array($returned))
            return $returned;

        $bookingColl = $this->mongo->selectCollection('bookings');
        $apptDet = $bookingColl->findOne(array('bid' => (string) $args['ent_bid']));

        $location = $this->mongo->selectCollection('location');
        $userData = $location->findOne(array('user' => (int) $apptDet['provider_id']));
        $googleDist = $this->get_DirectionFormMatrix($apptDet['appt_lat'], $apptDet['appt_long'], $userData['location']['latitude'], $userData['location']['longitude']);

        $err = (string) $googleDist['OrignalDatadata']['rows'][0]['elements'][0]['status'];
        if ($err == "ZERO_RESULTS") {
            $errNum = 123;
        } else {
            $dis_in_met = $googleDist['distance'];
            if ($dis_in_met > 500) {
                $errNum = 123;
            } else {
                $errNum = 124;
            }
        }
        $errMsgArr = $this->_getStatusMessage($errNum, $errNum);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'], 'test' => $googleDist);
    }

    /*
     * Method name: GetFinancialData
     * Desc: get FinancialData
     * Input: Request data
     * Output:  success if got it else error according to the result
     */

    protected function GetFinancialData($args) {

        if ($args['ent_pro_id'] == '')
            return $this->_getStatusMessage(1, 1);

//        $this->curr_date_time = urldecode($args['ent_date_time']);
//        $args['ent_pro_id'] = '10';
        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
        if (is_array($returned))
            return $returned;

        $bookingColl = $this->mongo->selectCollection('bookings');

        $selectEarn = "select sum(pro_earning) as total_earn from bookings where pro_id = '" . $args['ent_pro_id'] . "'";
        $proData = mysql_fetch_assoc(mysql_query($selectEarn, $this->db->conn));

        if (!$proData)
            return $this->_getStatusMessage(62, $selectEarn);

        $selectAppt = "select * from payroll where mas_id = '" . $args['ent_pro_id'] . "' order by payroll_id";
        $payData = mysql_query($selectAppt, $this->db->conn);

        if (!$payData)
            return $this->_getStatusMessage(62, $selectAppt);

        $pastCycle = array();
        $errMsgArr = $this->_getStatusMessage(21, 1);
        $count = 1;
        $numRow = mysql_num_rows($payData);
        if ($numRow == 0) {
            $selectAppt = "select created_dt as start_date from doctor where doc_id = " . $args['ent_pro_id'];
        } else {
            $selectAppt = "select pay_date as start_date from payroll where mas_id = '" . $args['ent_pro_id'] . "' order by payroll_id desc limit 1";
        }
        $start_date = mysql_fetch_assoc(mysql_query($selectAppt, $this->db->conn));
        $start_date = $start_date['start_date'];

        $OpeningBal = "SELECT round(closing_balance,2) as OpeningBal FROM payroll WHERE mas_id = " . $args['ent_pro_id'] . " order by payroll_id desc limit 1";
        $OpeningBal = mysql_fetch_assoc(mysql_query($OpeningBal, $this->db->conn));
        $OpeningBal = $OpeningBal['OpeningBal'];
        if ($OpeningBal == "")
            $OpeningBal = 0;

        $booking_cycle = "select count(*) as booking_cycle from bookings where pro_id = " . $args['ent_pro_id'] . " and payment_status = 0";
        $booking_cycle = mysql_fetch_assoc(mysql_query($booking_cycle, $this->db->conn));
        $booking_cycle = $booking_cycle['booking_cycle'];

        $booking_earn = "select round(sum(b.pro_earning),2) as booking_earn from bookings b where b.pro_id = " . $args['ent_pro_id'] . " and b.payment_status = 0";
        $booking_earn = mysql_fetch_assoc(mysql_query($booking_earn, $this->db->conn));
        $booking_earn = $booking_earn['booking_earn'];
        if ($booking_earn == "")
            $booking_earn = 0;

        $cash_due_app = "select round(sum(b.app_earning),2) as booking_earn from bookings b where b.pro_id = " . $args['ent_pro_id'] . " and b.payment_status = 0 and payment_type = 1";
        $cash_due_app = mysql_fetch_assoc(mysql_query($cash_due_app, $this->db->conn));
        $cash_due_app = $cash_due_app['booking_earn'];
        if ($cash_due_app == "")
            $cash_due_app = 0;

        $closing_balance = (float) $OpeningBal + (float) $booking_earn - (float) $cash_due_app;
        $closing_balance = (string) $closing_balance;

        while ($apptfData = mysql_fetch_assoc($payData)) {

            $apptfData['pay_amount'] = (string) round($apptfData['pay_amount'], 2);
            $apptfData['booking_earn'] = (string) round($apptfData['booking_earning'], 2);
            $apptfData['closing_balance'] = (string) round($apptfData['closing_balance'], 2);
            $apptfData['opening_balance'] = (string) round($apptfData['opening_balance'], 2);
            $apptfData['tot_bookings'] = (string) round($apptfData['tot_bookings'], 2);
            $apptfData['tot_accepted'] = (string) round($apptfData['tot_accepted'], 2);
            $apptfData['total_reject'] = (string) round($apptfData['tot_reject'], 2);
            $apptfData['total_ignore'] = (string) round($apptfData['tot_ignore'], 2);
            $apptfData['acpt_rate'] = (string) round($apptfData['acpt_rate'], 2);
            $apptfData['rev_rate'] = (string) round($apptfData['rev_rate'], 2);
            $apptfData['tot_cancel'] = (string) round($apptfData['tot_cancel'], 2);


            if ($count == 1) {
                $dateQur = "select created_dt from doctor where doc_id = " . $args['ent_pro_id'];
                $start_date = mysql_fetch_assoc(mysql_query($dateQur, $this->db->conn));
                $start_date = $start_date['created_dt'];
            }

            $pastCycle[] = array('payroll_id' => $apptfData['payroll_id'],
                'pay_amount' => $apptfData['pay_amount'],
                'start_date' => $start_date,
                'no_of_bookings' => $apptfData['booking_cycle'],
                'pay_date' => $apptfData['pay_date'],
                'closing_balance' => $apptfData['closing_balance'],
                'booking_earn' => $apptfData['booking_earn'],
                'acpt_rate' => $apptfData['acpt_rate'],
                'rev_rate' => $apptfData['rev_rate'],
                'tot_bookings' => $apptfData['tot_bookings'],
                'tot_accepted' => $apptfData['tot_accepted'],
                'total_reject' => $apptfData['total_reject'],
                'total_ignore' => $apptfData['total_ignore'],
                'tot_cancel' => $apptfData['tot_cancel'],
                'opening_balance' => $apptfData['opening_balance'],
                'last_bid' => $apptfData['last_bid']);

            $start_date = $apptfData['pay_date'];
            $count++;
        }

        $last_ap_id = 0;

        if (count($pastCycle) > 0) {
            $last_ap_id = (int) $pastCycle[count($pastCycle) - 1]['last_bid'];
        } else {
            $last_ap_id = 0;
        }

        $totalBoookings = (string) $bookingColl->find(array('dispatched' => array('$elemMatch' => array('pid' => $args['ent_pro_id'])), '_id' => array('$gt' => $last_ap_id)))->count();
//      $totalBoookings = (string) $bookingColl->find(array('dispatched.pid' => $args['ent_pro_id'], '_id' => array('$gt' => $last_ap_id)))->count();

        $total_completed = (string) $bookingColl->find(array('provider_id' => $args['ent_pro_id'], 'status' => 7, '_id' => array('$gt' => $last_ap_id)))->count();

        $totalAccepted = (string) $bookingColl->find(array('dispatched' => array('$elemMatch' => array('pid' => $args['ent_pro_id'], 'res' => "2")), '_id' => array('$gt' => $last_ap_id)))->count();
//      $totalAccepted = (string) $bookingColl->find(array('dispatched.pid' => $args['ent_pro_id'], 'dispatched.res' => "2", '_id' => array('$gt' => $last_ap_id)))->count();

        $tot_cancel = (string) $bookingColl->find(array('dispatched' => array('$elemMatch' => array('pid' => $args['ent_pro_id'], 'res' => 10)), '_id' => array('$gt' => $last_ap_id)))->count();
//        $tot_cancel = (string) $bookingColl->find(array('dispatched.pid' => $args['ent_pro_id'], 'dispatched.res' => 10, '_id' => array('$gt' => $last_ap_id)))->count();

        $totalRejected = (string) $bookingColl->find(array('dispatched' => array('$elemMatch' => array('pid' => $args['ent_pro_id'], 'res' => "3")), '_id' => array('$gt' => $last_ap_id)))->count();
//      $totalRejected = (string) $bookingColl->find(array('dispatched.pid' => $args['ent_pro_id'], 'dispatched.res' => "3", '_id' => array('$gt' => $last_ap_id)))->count();

        $totalIgnored = (string) $bookingColl->find(array('dispatched' => array('$elemMatch' => array('pid' => $args['ent_pro_id'], 'res' => 0)), '_id' => array('$gt' => $last_ap_id)))->count();
//      $totalIgnored = (string) $bookingColl->find(array('dispatched.pid' => $args['ent_pro_id'], 'dispatched.res' => 0, '_id' => array('$gt' => $last_ap_id)))->count();        
//        $totalBoookings = (string) $bookingColl->find(array('dispatched.pid' => $args['ent_pro_id'], '_id' => array('$gt' => $last_ap_id)))->count();
//        $totalAccepted = (string) $bookingColl->find(array('dispatched.pid' => $args['ent_pro_id'], 'dispatched.res' => "2", 'status' => 7, '_id' => array('$gt' => $last_ap_id)))->count();
//        $tot_cancel = (string) $bookingColl->find(array('dispatched.pid' => $args['ent_pro_id'], 'dispatched.res' => "2", 'status' => 10, '_id' => array('$gt' => $last_ap_id)))->count();
//        $totalRejected = (string) $bookingColl->find(array('dispatched.pid' => $args['ent_pro_id'], 'dispatched.res' => "3", '_id' => array('$gt' => $last_ap_id)))->count();
//        $totalIgnored = (string) $bookingColl->find(array('dispatched.res' => array('$exists' => false), 'dispatched.pid' => $args['ent_pro_id'], '_id' => array('$gt' => $last_ap_id)))->count();
        $acpt_rate = (string) round(($totalAccepted / $totalBoookings) * 100, 2);

        $rateQur = "SELECT avg(star_rating) as rev_rate FROM doctor_ratings WHERE doc_id = " . $args['ent_pro_id'] . " and appointment_id > '" . $last_ap_id . "' ";
        $rateRes = mysql_fetch_assoc(mysql_query($rateQur, $this->db->conn));
        $rev_rate = round($rateRes['rev_rate'], 2);

        $currentCycle = array(
            'start_bid' => $last_ap_id,
            'start_date' => $start_date,
            'pay_date' => '--',
            'opening_balance' => $OpeningBal,
            'closing_balance' => $closing_balance,
            'booking_cycle' => $booking_cycle,
            'booking_earn' => $booking_earn,
            'total_bookings' => $totalBoookings,
            'total_accepted' => $totalAccepted,
            'total_completed' => $total_completed,
            'total_reject' => $totalRejected,
            'total_ignore' => $totalIgnored,
            'acpt_rate' => $acpt_rate,
            'tot_cancel' => $tot_cancel,
            'rev_rate' => $rev_rate
        );

        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'],
            'errMsg' => $errMsgArr['errMsg'], 'currentCycle' => $currentCycle, 'pastCycle' => $pastCycle);
    }

    protected function GetFinancialDataSuperadmin($args) {

        if ($args['ent_pro_id'] == '')
            return $this->_getStatusMessage(1, 1);

        $bookingColl = $this->mongo->selectCollection('bookings');

        $selectEarn = "select sum(pro_earning) as total_earn from bookings where pro_id = '" . $args['ent_pro_id'] . "'";
        $proData = mysql_fetch_assoc(mysql_query($selectEarn, $this->db->conn));

        if (!$proData)
            return $this->_getStatusMessage(62, $selectEarn);

        $selectAppt = "select * from payroll where mas_id = '" . $args['ent_pro_id'] . "' order by payroll_id desc";
        $payData = mysql_query($selectAppt, $this->db->conn);

        if (!$payData)
            return $this->_getStatusMessage(62, $selectAppt);

        $pastCycle = array();
        $errMsgArr = $this->_getStatusMessage(21, 1);
        $count = 1;
        $numRow = mysql_num_rows($payData);
        if ($numRow == 0) {
            $selectAppt = "select created_dt as start_date from doctor where doc_id = " . $args['ent_pro_id'];
        } else {
            $selectAppt = "select pay_date as start_date from payroll where mas_id = '" . $args['ent_pro_id'] . "' order by payroll_id desc limit 1";
        }
        $start_date = mysql_fetch_assoc(mysql_query($selectAppt, $this->db->conn));
        $start_date = $start_date['start_date'];

        $OpeningBal = "SELECT round(closing_balance,2) as OpeningBal FROM payroll WHERE mas_id = " . $args['ent_pro_id'] . " order by payroll_id desc limit 1";
        $OpeningBal = mysql_fetch_assoc(mysql_query($OpeningBal, $this->db->conn));
        $OpeningBal = $OpeningBal['OpeningBal'];
        if ($OpeningBal == "")
            $OpeningBal = 0;

        $booking_cycle = "select count(*) as booking_cycle from bookings where pro_id = " . $args['ent_pro_id'] . " and payment_status = 0";
        $booking_cycle = mysql_fetch_assoc(mysql_query($booking_cycle, $this->db->conn));
        $booking_cycle = $booking_cycle['booking_cycle'];

        $booking_earn = "select round(sum(b.pro_earning),2) as booking_earn from bookings b where b.pro_id = " . $args['ent_pro_id'] . " and b.payment_status = 0 and b.payment_type = 2";
        $booking_earn = mysql_fetch_assoc(mysql_query($booking_earn, $this->db->conn));
        $booking_earn = $booking_earn['booking_earn'];
        if ($booking_earn == "")
            $booking_earn = 0;

        $cash_due_app = "select round(sum(b.app_earning),2) as booking_earn from bookings b where b.pro_id = " . $args['ent_pro_id'] . " and b.payment_status = 0 and payment_type = 1 and b.app_earning > 0";
        $cash_due_app = mysql_fetch_assoc(mysql_query($cash_due_app, $this->db->conn));
        $cash_due_app = $cash_due_app['booking_earn'];
        if ($cash_due_app == "")
            $cash_due_app = 0;

        $cash_due_app1 = "select round(sum(b.app_earning),2) as booking_earn from bookings b where b.pro_id = " . $args['ent_pro_id'] . " and b.payment_status = 0 and payment_type = 1 and b.app_earning < 0";
        $cash_due_app1 = mysql_fetch_assoc(mysql_query($cash_due_app1, $this->db->conn));
        $cash_due_app1 = $cash_due_app1['booking_earn'];
        if ($cash_due_app1 == "")
            $cash_due_app1 = 0;

        $closing_balance = (float) $OpeningBal + (float) $booking_earn - (float) $cash_due_app;
        $closing_balance = (string) $closing_balance;

        while ($apptfData = mysql_fetch_assoc($payData)) {

            $apptfData['pay_amount'] = (string) round($apptfData['pay_amount'], 2);
            $apptfData['booking_earn'] = (string) round($apptfData['booking_earning'], 2);
            $apptfData['cash_due_app'] = (string) round($apptfData['cash_due_app'], 2);
            $apptfData['closing_balance'] = (string) round($apptfData['closing_balance'], 2);
            $apptfData['opening_balance'] = (string) round($apptfData['opening_balance'], 2);
            $apptfData['tot_bookings'] = (string) round($apptfData['tot_bookings'], 2);
            $apptfData['tot_accepted'] = (string) round($apptfData['tot_accepted'], 2);
            $apptfData['total_reject'] = (string) round($apptfData['tot_reject'], 2);
            $apptfData['total_ignore'] = (string) round($apptfData['tot_ignore'], 2);
            $apptfData['acpt_rate'] = (string) round($apptfData['acpt_rate'], 2);
            $apptfData['rev_rate'] = (string) round($apptfData['rev_rate'], 2);
            $apptfData['tot_cancel'] = (string) round($apptfData['tot_cancel'], 2);


            $apptfData['pro_receivable'] = (string) round($apptfData['opening_balance'] + $apptfData['booking_earn'] - $apptfData['cash_due_app'], 2);


            if ($count == 1) {
                $dateQur = "select created_dt from doctor where doc_id = " . $args['ent_pro_id'];
                $start_date = mysql_fetch_assoc(mysql_query($dateQur, $this->db->conn));
                $start_date = $start_date['created_dt'];
            }

            $pastCycle[] = array('payroll_id' => $apptfData['payroll_id'],
                'pay_amount' => $apptfData['pay_amount'],
                'start_date' => $start_date,
                'no_of_bookings' => $apptfData['booking_cycle'],
                'pay_date' => $apptfData['pay_date'],
                'closing_balance' => $apptfData['closing_balance'],
                'booking_earn' => $apptfData['booking_earn'],
                'cash_due_app' => $apptfData['cash_due_app'],
                'pro_receivable' => $apptfData['pro_receivable'],
                'acpt_rate' => $apptfData['acpt_rate'],
                'rev_rate' => $apptfData['rev_rate'],
                'tot_bookings' => $apptfData['tot_bookings'],
                'tot_accepted' => $apptfData['tot_accepted'],
                'total_reject' => $apptfData['total_reject'],
                'total_ignore' => $apptfData['total_ignore'],
                'trasaction_id' => $apptfData['trasaction_id'],
                'tot_cancel' => $apptfData['tot_cancel'],
                'opening_balance' => $apptfData['opening_balance'],
                'last_bid' => $apptfData['last_bid']);

            $start_date = $apptfData['pay_date'];
            $count++;
        }

        $last_ap_id = 0;

        if (count($pastCycle) > 0) {
            $last_ap_id = (int) $pastCycle[count($pastCycle) - 1]['last_bid'];
        } else {
            $last_ap_id = 0;
        }


        $totalBoookings = (string) $bookingColl->find(array('dispatched' => array('$elemMatch' => array('pid' => $args['ent_pro_id'])), '_id' => array('$gt' => $last_ap_id)))->count();
//      $totalBoookings = (string) $bookingColl->find(array('dispatched.pid' => $args['ent_pro_id'], '_id' => array('$gt' => $last_ap_id)))->count();

        $total_completed = (string) $bookingColl->find(array('provider_id' => $args['ent_pro_id'], 'status' => 7, '_id' => array('$gt' => $last_ap_id)))->count();

        $totalAccepted = (string) $bookingColl->find(array('dispatched' => array('$elemMatch' => array('pid' => $args['ent_pro_id'], 'res' => "2")), '_id' => array('$gt' => $last_ap_id)))->count();
//      $totalAccepted = (string) $bookingColl->find(array('dispatched.pid' => $args['ent_pro_id'], 'dispatched.res' => "2", '_id' => array('$gt' => $last_ap_id)))->count();

        $tot_cancel = (string) $bookingColl->find(array('dispatched' => array('$elemMatch' => array('pid' => $args['ent_pro_id'], 'res' => 10)), '_id' => array('$gt' => $last_ap_id)))->count();
//        $tot_cancel = (string) $bookingColl->find(array('dispatched.pid' => $args['ent_pro_id'], 'dispatched.res' => 10, '_id' => array('$gt' => $last_ap_id)))->count();

        $totalRejected = (string) $bookingColl->find(array('dispatched' => array('$elemMatch' => array('pid' => $args['ent_pro_id'], 'res' => "3")), '_id' => array('$gt' => $last_ap_id)))->count();
//      $totalRejected = (string) $bookingColl->find(array('dispatched.pid' => $args['ent_pro_id'], 'dispatched.res' => "3", '_id' => array('$gt' => $last_ap_id)))->count();

        $totalIgnored = (string) $bookingColl->find(array('dispatched' => array('$elemMatch' => array('pid' => $args['ent_pro_id'], 'res' => 0)), '_id' => array('$gt' => $last_ap_id)))->count();
//      $totalIgnored = (string) $bookingColl->find(array('dispatched.pid' => $args['ent_pro_id'], 'dispatched.res' => 0, '_id' => array('$gt' => $last_ap_id)))->count();
//        $totalBoookings = (string) $bookingColl->find(array('dispatched.pid' => $args['ent_pro_id'], '_id' => array('$gt' => $last_ap_id)))->count();
//        $totalAccepted = (string) $bookingColl->find(array('dispatched.pid' => $args['ent_pro_id'], 'dispatched.res' => "2", 'status' => 7, '_id' => array('$gt' => $last_ap_id)))->count();
//        $tot_cancel = (string) $bookingColl->find(array('dispatched.pid' => $args['ent_pro_id'], 'dispatched.res' => "2", 'status' => 10, '_id' => array('$gt' => $last_ap_id)))->count();
//        $totalRejected = (string) $bookingColl->find(array('dispatched.pid' => $args['ent_pro_id'], 'dispatched.res' => "3", '_id' => array('$gt' => $last_ap_id)))->count();
//        $totalIgnored = (string) $bookingColl->find(array('dispatched.res' => array('$exists' => false), 'dispatched.pid' => $args['ent_pro_id'], '_id' => array('$gt' => $last_ap_id)))->count();
        $acpt_rate = (string) round(($totalAccepted / $totalBoookings) * 100, 2);

        $rateQur = "SELECT avg(star_rating) as rev_rate FROM doctor_ratings WHERE doc_id = " . $args['ent_pro_id'] . " and appointment_id > '" . $last_ap_id . "' ";
        $rateRes = mysql_fetch_assoc(mysql_query($rateQur, $this->db->conn));
        $rev_rate = round($rateRes['rev_rate'], 2);

        $pro_receivable = $OpeningBal + $booking_earn - $cash_due_app - $cash_due_app1;

        if ($cash_due_app > 0)
            $cash_due_app = "-" . $cash_due_app;

        $currentCycle = array(
            'start_bid' => $last_ap_id,
            'start_date' => $start_date,
            'pay_date' => '--',
            'opening_balance' => $OpeningBal,
            'closing_balance' => $pro_receivable,
            'booking_cycle' => $booking_cycle,
            'booking_earn' => $booking_earn,
            'cash_due_app' => $cash_due_app,
            'pro_receivable' => $pro_receivable,
            'total_completed' => $total_completed,
            'total_bookings' => $totalBoookings,
            'total_accepted' => $totalAccepted,
            'total_reject' => $totalRejected,
            'total_ignore' => $totalIgnored,
            'acpt_rate' => $acpt_rate,
            'tot_cancel' => $tot_cancel,
            'rev_rate' => $rev_rate
        );

        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'],
            'errMsg' => $errMsgArr['errMsg'], 'currentCycle' => $currentCycle, 'pastCycle' => $pastCycle);
    }

    protected function __calculateCopenDiscount($apptDet) {
        $visit_fees = $apptDet['visit_fees'];
        $services_total = 0;
        if (isset($apptDet['services']) && is_array($apptDet['services'])) {
            foreach ($apptDet['services'] as $services) {
                $services_total += $services['sprice'];
            }
        }
        if ($apptDet['coupon_type'] == 1) {
            $forDisc = $visit_fees + $apptDet['comp']['time_fees'] + $services_total;
            $coup_dicount = ($forDisc * $apptDet['coupon_discount']) / 100;
        } else {
            $coup_dicount = $apptDet['coupon_discount'];
        }
        return $coup_dicount;
    }

    protected function aaa($args) {
        $bookingColl = $this->mongo->selectCollection('bookings');
        $apptDet = $bookingColl->findOne(array('bid' => '1058'));

        $visit_fees = $apptDet['visit_fees'];

        $services_total = 0;
        if (isset($apptDet['services']) && is_array($apptDet['services'])) {
            foreach ($apptDet['services'] as $services) {
                $services_total += $services['sprice'];
            }
        }

        if ($apptDet['coupon_type'] == 1) {
            $forDisc = $visit_fees + $apptDet['comp']['time_fees'] + $services_total;
            $coup_dicount = ($forDisc * $apptDet['coupon_discount']) / 100;
        } else {
            $coup_dicount = $apptDet['coupon_discount'];
        }

        $pro_pg_comm = 0;
        $app_pg_comm = 0;
        $total_pg_comm = 0;

        $app_biling = round(($visit_fees + $apptDet['comp']['time_fees'] + $services_total), 2);
        $pro_biling = round(($apptDet['comp']['misc_fees'] + $apptDet['comp']['mat_fees']) - $apptDet['comp']['pro_disc'], 2);
        $billed_amt = round($app_biling + $pro_biling, 2);

        $pro_comm = 100 - $apptDet['pro_commission'];
        $pro_commision = ($pro_biling * $pro_comm ) / 100;


        $pro_sub_total = round(($pro_commision + $apptDet['comp']['misc_fees'] + $apptDet['comp']['mat_fees']) - $apptDet['comp']['pro_disc'], 2);
        if ($apptDet['payment_type'] == "2")
            $pro_pg_comm = (float) round((($pro_biling * 2.9) / 100), 2);

        $pro_earning = (float) round($pro_sub_total - $pro_pg_comm, 2);

        $app_comm = ($app_biling * $apptDet['pro_commission']) / 100;
        $app_comm = $app_comm - $coup_dicount;

        $app_earning = (float) round($app_comm - $app_pg_comm, 2);

        return array('app_earning' => $app_earning);
    }

    protected function __GenerateInvoiceDetails($bid, $taranseferId) {

        $bookingColl = $this->mongo->selectCollection('bookings');
        $apptDet = $bookingColl->findOne(array('bid' => (string) $bid));

        $visit_fees = $apptDet['visit_fees'];

        $services_total = 0;
        if (isset($apptDet['services']) && is_array($apptDet['services'])) {
            foreach ($apptDet['services'] as $services) {
                $services_total += $services['sprice'];
            }
        }

        if ($apptDet['coupon_type'] == 1) {
            $forDisc = $visit_fees + $apptDet['comp']['time_fees'] + $services_total;
            $coup_dicount = ($forDisc * $apptDet['coupon_discount']) / 100;
        } else {
            $coup_dicount = $apptDet['coupon_discount'];
        }

        $pro_pg_comm = 0;
        $app_pg_comm = 0;
        $total_pg_comm = 0;

        //Calculate Billed Ammount
        $billed1 = $apptDet['comp']['misc_fees'] + $apptDet['comp']['mat_fees'] - $apptDet['comp']['pro_disc'];
        $billed2 = $visit_fees + $apptDet['comp']['time_fees'] + $services_total - $coup_dicount;
        $billed_amt = (float) round($billed1 + $billed2, 2);


        if ($apptDet['payment_type'] == "2") {
            //Calculate App Payment GateWay
            $app_pg1 = $visit_fees + $apptDet['comp']['time_fees'] + $services_total - $coup_dicount;
            if ($app_pg1 > 0 && $billed_amt > 0)
                $app_pg_comm = (float) round((($app_pg1 * 2.9) / 100 ) + 0.3, 2);

            //Calculate Provider Payment Gateway
            $pro_pg1 = $apptDet['comp']['misc_fees'] + $apptDet['comp']['mat_fees'] - $apptDet['comp']['pro_disc'];
            if ($pro_pg1 > 0 && $billed_amt > 0)
                $pro_pg_comm = (float) round(($pro_pg1 * 2.9) / 100, 2);

            //Calculate Total Payment Gateway
            $total_pg_comm = $pro_pg_comm + $app_pg_comm;
        }

        //Calculate Provider Earning
        $pro_biling1 = $apptDet['comp']['misc_fees'] + $apptDet['comp']['mat_fees'] - $apptDet['comp']['pro_disc'];

        $pro_biling2 = $visit_fees + $apptDet['comp']['time_fees'] + $services_total;
        $app_comm = ($pro_biling2 * $apptDet['pro_commission']) / 100;
        $pro_biling2 = $pro_biling2 - $app_comm;

        $pro_earning = (float) round($pro_biling1 + $pro_biling2 - $pro_pg_comm, 2);


        //Calculate App Earning
        $app_billing = $visit_fees + $apptDet['comp']['time_fees'] + $services_total;
        $app_earning1 = ($app_billing * $apptDet['pro_commission']) / 100;
        $app_earning = (float) round($app_earning1 - $coup_dicount - $app_pg_comm, 2);


        $invoice_id = 'RA' . str_pad($apptDet['bid'], 6, '0', STR_PAD_LEFT);
        $InvoiceData = array(
            "invoice_id" => $invoice_id,
            'billed_amount' => $billed_amt,
            'pro_billing' => $pro_biling1,
            "app_billing" => $app_billing,
//            'pro_sub_total' => $pro_biling2,
//            'app_sub_total' => $app_earning1,
            'pro_earning' => $pro_earning,
            'app_earning' => $app_earning,
            'pro_pg_comm' => $pro_pg_comm,
            'app_pg_comm' => $app_pg_comm,
            'total_pg_comm' => $total_pg_comm,
            'txn_id' => $taranseferId
        );

        $insertReviewQry = "insert into bookings(bid,pro_id,cust_id,payment_type,appointment_dt,status,billed_amount,"
                . "app_billing,pro_billing,pro_sub_total,pro_pg_comm,total_pg_comm,pro_earning,app_sub_total,app_pg_comm,app_earning,trans_id) "
                . "values('" . $apptDet['bid'] . "','" . $apptDet['provider']['id'] . "','" . $apptDet['customer']['id'] . "','" . $apptDet['payment_type'] . "',   '" . $apptDet['appt_date'] . "','7','" . $billed_amt . "','" . $app_billing . "','" . $pro_biling1 .
                "','" . $pro_biling2 . "','" . $pro_pg_comm . "','" . $total_pg_comm . "' , '" . $pro_earning . "','" . $app_earning1 . "',"
                . "'" . $app_pg_comm . "','" . $app_earning . "','" . $taranseferId . "')";

        mysql_query($insertReviewQry, $this->db->conn);
        $apptId = mysql_insert_id();
        if ($apptId < 0)
            return $this->_getStatusMessage(3, $insertReviewQry);

        return array('invoiceData' => $InvoiceData, 'invoiceMail' => $push);
    }

//    protected function __GenerateInvoiceDetails($bid, $taranseferId) {
//
//        $bookingColl = $this->mongo->selectCollection('bookings');
//        $apptDet = $bookingColl->findOne(array('bid' => (string) $bid));
//
//        $visit_fees = $apptDet['visit_fees'];
//
//        $services_total = 0;
//        if (isset($apptDet['services']) && is_array($apptDet['services'])) {
//            foreach ($apptDet['services'] as $services) {
//                $services_total += $services['sprice'];
//            }
//        }
//
//        if ($apptDet['coupon_type'] == 1) {
//            $forDisc = $visit_fees + $apptDet['comp']['time_fees'] + $services_total;
//            $coup_dicount = ($forDisc * $apptDet['coupon_discount']) / 100;
//        } else {
//            $coup_dicount = $apptDet['coupon_discount'];
//        }
//
//        $pro_pg_comm = 0;
//        $app_pg_comm = 0;
//        $total_pg_comm = 0;
//
//        $pro_biling = round(($apptDet['comp']['misc_fees'] + $apptDet['comp']['mat_fees'] - $apptDet['comp']['pro_disc']), 2);
//        
//        $app_biling = round(($visit_fees + $apptDet['comp']['time_fees'] + $services_total), 2);
//        $billed_amt = round($app_biling + $pro_biling, 2);
//
//        $pro_comm = 100 - $apptDet['pro_commission'];
//        $pro_commision = ($pro_biling * $pro_comm ) / 100;
//
//        $pro_sub_total = round(($pro_commision + $apptDet['comp']['misc_fees'] + $apptDet['comp']['mat_fees']) - $apptDet['comp']['pro_disc'], 2);
//        if ($apptDet['payment_type'] == "2")
//            $pro_pg_comm = (float) round((($pro_biling * 2.9) / 100), 2);
//
//        $pro_earning = (float) round($pro_sub_total - $pro_pg_comm, 2);
//
//
//        $app_comm = ($app_biling * $apptDet['pro_commission']) / 100;
//        $app_comm = $app_comm - $coup_dicount;
//
//        $app_sub_total = $app_biling;
//        if ($apptDet['payment_type'] == "2") {
//            $app_pg_comm = (float) round((($app_biling * 2.9) / 100), 2);
//            $app_pg_comm = $app_pg_comm + 0.3;
//        }
//
//        $app_earning = (float) round($app_comm - $app_pg_comm, 2);
//
//        if ($apptDet['payment_type'] == "2")
//            $total_pg_comm = $pro_pg_comm + $app_pg_comm;
//
//
//        $invoice_id = 'RA' . str_pad($apptDet['bid'], 6, '0', STR_PAD_LEFT);
//        $InvoiceData = array(
//            "invoice_id" => $invoice_id,
//            'billed_amount' => $billed_amt,
//            "app_billing" => $app_biling,
//            'pro_billing' => $pro_biling,
//            'pro_sub_total' => $pro_sub_total,
//            'pro_pg_comm' => $pro_pg_comm,
//            'pro_earning' => $pro_earning,
//            'app_sub_total' => $app_sub_total,
//            'app_pg_comm' => $app_pg_comm,
//            'total_pg_comm' => $total_pg_comm,
//            'app_earning' => $app_earning,
//            'txn_id' => $taranseferId
//        );
//
//        $insertReviewQry = "insert into bookings(bid,pro_id,cust_id,payment_type,appointment_dt,status,billed_amount,"
//                . "app_billing,pro_billing,pro_sub_total,pro_pg_comm,total_pg_comm,pro_earning,app_sub_total,app_pg_comm,app_earning,trans_id) "
//                . "values('" . $apptDet['bid'] . "','" . $apptDet['provider']['id'] . "','" . $apptDet['customer']['id'] . "','" . $apptDet['payment_type'] . "',   '" . $apptDet['appt_date'] . "','7','" . $billed_amt . "','" . $app_biling . "','" . $pro_biling .
//                "','" . $pro_sub_total . "','" . $pro_pg_comm . "','" . $total_pg_comm . "' , '" . $pro_earning . "','" . $app_sub_total . "',"
//                . "'" . $app_pg_comm . "','" . $app_earning . "','" . $taranseferId . "')";
//
//        $proll = $this->mongo->selectCollection('proll');
//        $proll->insert(array('InvoiceData' => $InvoiceData, 'insertReviewQry' => $insertReviewQry));
//
//        mysql_query($insertReviewQry, $this->db->conn);
//        $apptId = mysql_insert_id();
//        if ($apptId < 0)
//            return $this->_getStatusMessage(3, $insertReviewQry);
//
//        return array('invoiceData' => $InvoiceData, 'invoiceMail' => $push);
//    }

    protected function __GenerateInvoiceDetailsForCancel($bid, $taranseferId) {

        $bookingColl = $this->mongo->selectCollection('bookings');
        $apptDet = $bookingColl->findOne(array('bid' => (string) $bid));

        $pro_pg_comm = 0;
        $app_pg_comm = 0;
        $total_pg_comm = 0;

        //Calculate Billed Ammount
        $billed_amt = (float) round($apptDet['cancel_amount'], 2);

        if ($apptDet['payment_type'] == "2") {
            //Calculate App Payment GateWay
            $app_pg_comm = (float) round((($billed_amt * 2.9) / 100 ) + 0.3, 2);

            //Calculate Provider Payment Gateway
            $pro_pg_comm = 0;

            //Calculate Total Payment Gateway
            $total_pg_comm = $pro_pg_comm + $app_pg_comm;
        }

        //Calculate Provider Earning
        $app_comm = ($billed_amt * $apptDet['pro_commission']) / 100;
        $pro_earning = $billed_amt - $app_comm;

        //Calculate App Earning
        $app_earning1 = ($billed_amt * $apptDet['pro_commission']) / 100;
        $app_earning = (float) round($app_earning1 - $app_pg_comm, 2);

        $invoice_id = 'RA' . str_pad($apptDet['bid'], 6, '0', STR_PAD_LEFT);
        $InvoiceData = array(
            "invoice_id" => $invoice_id,
            'billed_amount' => $billed_amt,
            'pro_billing' => $pro_earning,
            "app_billing" => $app_earning1,
//            'pro_sub_total' => $pro_sub_total,
//            'app_sub_total' => $app_sub_total,
            'pro_earning' => $pro_earning,
            'app_earning' => $app_earning,
            'pro_pg_comm' => $pro_pg_comm,
            'app_pg_comm' => $app_pg_comm,
            'total_pg_comm' => $total_pg_comm,
            'txn_id' => $taranseferId
        );

        $insertReviewQry = "insert into bookings(bid,pro_id,cust_id,payment_type,appointment_dt,status,billed_amount,"
                . "app_billing,pro_billing,pro_sub_total,pro_pg_comm,total_pg_comm,pro_earning,app_sub_total,app_pg_comm,app_earning,trans_id) "
                . "values('" . $apptDet['bid'] . "','" . $apptDet['provider']['id'] . "','" . $apptDet['customer']['id'] . "','" . $apptDet['payment_type'] . "',   '" . $apptDet['appt_date'] . "','9','" . $billed_amt . "','" . $app_biling . "','" . $pro_biling .
                "','" . $pro_sub_total . "','" . $pro_pg_comm . "','" . $total_pg_comm . "',   '" . $pro_earning . "','" . $app_sub_total . "',"
                . "'" . $app_pg_comm . "','" . $app_earning . "','" . $taranseferId . "')";

        mysql_query($insertReviewQry, $this->db->conn);
        $apptId = mysql_insert_id();
        if ($apptId < 0)
            return $this->_getStatusMessage(3, $insertReviewQry);

        return array('invoiceData' => $InvoiceData, 'invoiceMail' => $push);
    }

    protected function UpdateApptTimer($args) {

        if ($args['ent_timer_status'] == '' || $args['ent_bid'] == '' || $args['ent_timer'] == '' || $args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, 1);

        $this->curr_date_time = urldecode($args['ent_date_time']);

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
        if (is_array($returned))
            return $returned;
//        $this->User['entityId'] = '1';

        $bookings = $this->mongo->selectCollection('bookings');
        $bookingsRes = $bookings->findOne(array('bid' => $args['ent_bid']));
        if (!$bookingsRes)
            return $this->_getStatusMessage(77, $bookings);

        if ($bookingsRes['status'] == '3')
            return $this->_getStatusMessage(88, 3);
        if ($bookingsRes['status'] == '4' || $bookingsRes['status'] == '9')
            return $this->_getStatusMessage(41, 3);
        if ($bookingsRes['status'] == '10')
            return $this->_getStatusMessage(89, 77);
        if ($bookingsRes['status'] == '8')
            return $this->_getStatusMessage(77, 77);


        if ($args['ent_timer_status'] == '1') {
            $updatedRes = $bookings->update(array('bid' => $args['ent_bid']), array('$set' => array('timer' => $args['ent_timer'], 'timer_status' => $args['ent_timer_status'], 'timer_start_time' => $this->curr_date_time)));
        } else {
            $updatedRes = $bookings->update(array('bid' => $args['ent_bid']), array('$set' => array('timer' => $args['ent_timer'], 'timer_status' => $args['ent_timer_status'])));
        }

        if ($args['ent_timer_status'] == '1') {
            $message = "Timer Started";
            $noteType = 15;
        } else if ($args['ent_timer_status'] == '2') {
            $message = "Timer Paused";
            $noteType = 16;
        } else if ($args['ent_timer_status'] == '3') {
            $message = "Timer Stopped";
            $noteType = 17;
        }

        $aplPushContent = array('alert' => $message, 'sound' => 'default', 'bid' => $bookingsRes['bid'], 'st' => $noteType, 'tim_st' => $args['ent_timer_status']);
        $andrPushContent = array("payload" => $message, 'bid' => $bookingsRes['bid'], 'st' => $noteType, 'tim_st' => $args['ent_timer_status']);
        $push = $this->_sendPush($this->User['entityId'], array($bookingsRes['customer_id']), $message, $noteType, $this->User['email'], $this->curr_date_time, '2', $aplPushContent, $andrPushContent);

        $argstopass = array('bid' => $bookingsRes['bid'], 'proid' => $this->User['entityId'], 'bstatus' => $noteType);
        $this->sendDatatoSocket($argstopass, "respondToAppointment");

        if ($updatedRes) {
            return $this->_getStatusMessage(110, 1);
        } else {
            return $this->_getStatusMessage(109, 2);
        }
    }

    protected function UpdateAppVersion($args) {

        if ($args['ent_app_version'] == '' || $args['ent_user_type'] == '')
            return $this->_getStatusMessage(1, 1);

        if ($args['ent_user_type'] == '1')
            $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
        else
            $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '2');

        if (is_array($returned))
            return $returned;

//        $this->User['entityId'] = 139;

        if ($args['ent_user_type'] == '1') {
            $updateApptVersion = "update doctor set app_version = '" . $args['ent_app_version'] . "'  where doc_id = '" . $this->User['entityId'] . "'";
        } else {
            $updateApptVersion = "update patient set app_version = '" . $args['ent_app_version'] . "' where patient_id = '" . $this->User['entityId'] . "'";
        }

        mysql_query($updateApptVersion, $this->db->conn);
        if (mysql_affected_rows() > 0) {
            return $this->_getStatusMessage(117, $updateApptVersion);
        } else {
            return $this->_getStatusMessage(116, $updateApptVersion);
        }
    }

    /*
     * Method name: getMasterProfile
     * Desc: get master profile
     * Input: Request data
     * Output:  success if got it else error according to the result
     */

//    protected function getMasterProfile($args) {
//
//        if ($args['ent_date_time'] == '')
//            return $this->_getStatusMessage(1, 1);
//
//        $this->curr_date_time = urldecode($args['ent_date_time']);
//        $docData = array();
////
//        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
//        if (is_array($returned))
//            return $returned;
//
////        $this->User['entityId'] = 25;
//
//        $explodeDateTime = explode(' ', $this->curr_date_time);
//        $explodeDate = explode('-', $explodeDateTime[0]);
//
//        $weekData = $this->week_start_end_by_date($this->curr_date_time);
//        $selectMasterProfileQry = 'select * from doctor where doc_id = "' . $this->User['entityId'] . '"';
//
//        $selectMasterProfileRes = mysql_query($selectMasterProfileQry, $this->db->conn);
//
//        if (mysql_error($this->db->conn) != '')
//            return $this->_getStatusMessage(3, mysql_error($this->db->conn));
//        if (mysql_num_fields($selectMasterProfileRes) <= 0)
//            return $this->_getStatusMessage(3, 3);
//        $docData = mysql_fetch_assoc($selectMasterProfileRes);
//
//        // select category for user
//        $location = $this->mongo->selectCollection('location');
//        $userData = $location->findOne(array('user' => (int) $this->User['entityId']));
//        $catidArr = $userData['catlist'];
//        $allcatstr = "";
//        $catArray = array();
//        $ProviderCategory = $this->mongo->selectCollection('Category');
//        foreach ($catidArr as $cat) {
//            $catdata = $ProviderCategory->findOne(array('_id' => new MongoId($cat['cid'])));
//            $catArray[] = array('cat_id' => $cat['cid'], 'cat_name' => $catdata['cat_name']);
//            if ($allcatstr == "")
//                $allcatstr = $catdata['cat_name'];
//            else
//                $allcatstr = $allcatstr . "," . $catdata['cat_name'];
//        }
//        $reviewsArr = array();
////        $selectReviewsQry = "select rev.patient_id, rev.star_rating, rev.review_dt, rev.review from doctor_ratings rev,doctor d where d.doc_id = '" . $this->User['entityId'] . "' and d.doc_id = rev.doc_id and rev.status = 1 and rev.review != '' ";
//        $selectReviewsQry = "select rev.patient_id, rev.star_rating, rev.review_dt, rev.review from doctor_ratings rev,doctor d where d.doc_id = '" . $this->User['entityId'] . "' and d.doc_id = rev.doc_id and rev.status = 1";
//        $selectReviewsRes = mysql_query($selectReviewsQry, $this->db->conn);
//        while ($review = mysql_fetch_assoc($selectReviewsRes)) {
//            $selectCustomerQry = "select first_name,last_name,profile_pic from patient";
//            $selectCustomerRes = mysql_query($selectCustomerQry, $this->db->conn);
//            $customerData = mysql_fetch_assoc($selectCustomerRes);
//            $reviewsArr[] = array('review_dt' => $review['review_dt'], 'rating' => $review['star_rating'], 'review' => $review['review'], 'fname' => $customerData['first_name'], 'lname' => $customerData['last_name'], 'img' => $customerData['profile_pic']);
//        }
//
//        $jobImagesArr = array();
//        if (isset($userData['job_images']) && is_array($userData['job_images'])) {
//            foreach ($userData['job_images'] as $imgdata) {
//                $jobImagesArr[] = $imgdata['url'];
//            }
//        }
//        $errMsgArr = $this->_getStatusMessage(21, 2);
////'type' => $docData['type_name'],
//        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'], 'fName' => $docData['first_name'], 'typeId' => $docData['type_id'],
//            'lName' => $docData['last_name'], 'email' => $docData['email'],
//            'mobile' => $docData['mobile'], 'status' => $docData['status'], 'amount' => $docData['amount'],
//            'about' => preg_replace('/[^A-Za-z0-9 \-]/', '', $docData['about']), 'zip' => $docData['zipcode'],
//            'pPic' => ($docData['profile_pic'] == '') ? $this->default_profile_pic : $docData['profile_pic'],
//            'expertise' => $docData['expertise'],
//            'licNo' => $docData['medical_license_num'],
//            'licExp' => ($docData['board_certification_expiry_dt'] == '') ? '' : date('F d, Y', strtotime($docData['board_certification_expiry_dt'])),
//            'totRats' => count($reviewsArr), 'num_of_job_images' => $docData['num_of_job_images'],
//            'avgRate' => round($docData['avgRate'], 1),
//            'rejApts' => $docData['rejApts'], 'cmpltApts' => $docData['cmpltApts'],
//            'cmpltApts' => $docData['cmpltApts'], 'todayAmt' => round($docData['today_earnings'], 2),
//            'lastBilledAmt' => round($docData['last_billed_amount'], 2),
//            'weekAmt' => round($docData['week_earnings'], 2), 'monthAmt' => round($docData['month_earnings'], 2),
//            'totalAmt' => round($docData['total_earnings'], 2),
//            'selCat' => $allcatstr,
//            'catArray' => $catArray,
//            'job_images' => $jobImagesArr,
//            'languages' => $docData['languages'], 'qry' => $selectMasterProfileQry);
//    }


    protected function testneha($args) {
        $chargeCustomerArr = array('stripe_id' => "cus_A3XZuuK7oG2ozz", 'amount' => (int) ((float) 10 * 100), 'currency' => 'USD', 'description' => 'From ');
        $trStripe = $this->stripe->apiStripe('chargeCard', $chargeCustomerArr);
        echo "<pre>";
        print_r($trStripe);
        exit();
        $taranseferId = "";
        if ($trStripe['error']) {
            return array('errNum' => 16, 'errFlag' => 1, 'errMsg' => $trStripe['error']['message'], 'test' => 1);
        } else {
            $taranseferId = $trStripe['id'];
            return $taranseferId;
        }
    }

    protected function getMasterProfile($args) {

        if ($args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, 1);

        $this->curr_date_time = urldecode($args['ent_date_time']);
        $docData = array();
//
        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
        if (is_array($returned))
            return $returned;

//        $this->User['entityId'] = 9;

        $explodeDateTime = explode(' ', $this->curr_date_time);
        $explodeDate = explode('-', $explodeDateTime[0]);

        $weekData = $this->week_start_end_by_date($this->curr_date_time);
        $selectMasterProfileQry = 'select *,(select avg(star_rating) from doctor_ratings dr where dr.doc_id = doc.doc_id) as avgRate from doctor doc where doc.doc_id = "' . $this->User['entityId'] . '"';

        $selectMasterProfileRes = mysql_query($selectMasterProfileQry, $this->db->conn);

        if (mysql_error($this->db->conn) != '')
            return $this->_getStatusMessage(3, mysql_error($this->db->conn));
        if (mysql_num_fields($selectMasterProfileRes) <= 0)
            return $this->_getStatusMessage(3, 3);
        $docData = mysql_fetch_assoc($selectMasterProfileRes);

        // select category for user
        $location = $this->mongo->selectCollection('location');
        $userData = $location->findOne(array('user' => (int) $this->User['entityId']));
        $catidArr = $userData['catlist'];
        $allcatstr = "";
        $catArray = array();
        $ProviderCategory = $this->mongo->selectCollection('Category');
        foreach ($catidArr as $cat) {
            $catdata = $ProviderCategory->findOne(array('_id' => new MongoId($cat['cid'])));
            $catArray[] = array('cat_id' => $cat['cid'], 'cat_name' => $catdata['cat_name']);
            if ($allcatstr == "")
                $allcatstr = $catdata['cat_name'];
            else
                $allcatstr = $allcatstr . "," . $catdata['cat_name'];
        }
        $reviewsArr = array();
        $selectReviewsQry = "select rev.patient_id, rev.star_rating, rev.review_dt, rev.review from doctor_ratings rev,doctor d where d.doc_id = '" . $this->User['entityId'] . "' and d.doc_id = rev.doc_id and rev.status = 1 and rev.review != '' ";
//        $selectReviewsQry = "select rev.patient_id, rev.star_rating, rev.review_dt, rev.review from doctor_ratings rev,doctor d where d.doc_id = '" . $this->User['entityId'] . "' and d.doc_id = rev.doc_id and rev.status = 1";
        $selectReviewsRes = mysql_query($selectReviewsQry, $this->db->conn);
        while ($review = mysql_fetch_assoc($selectReviewsRes)) {
            $selectCustomerQry = "select first_name,last_name,profile_pic from patient";
            $selectCustomerRes = mysql_query($selectCustomerQry, $this->db->conn);
            $customerData = mysql_fetch_assoc($selectCustomerRes);
            $reviewsArr[] = array('review_dt' => $review['review_dt'], 'rating' => $review['star_rating'], 'review' => $review['review'], 'fname' => $customerData['first_name'], 'lname' => $customerData['last_name'], 'img' => $customerData['profile_pic']);
        }

        $jobImagesArr = array();
        if (isset($userData['job_images']) && is_array($userData['job_images'])) {
            foreach ($userData['job_images'] as $imgdata) {
                $jobImagesArr[] = $imgdata['url'];
            }
        }
        $errMsgArr = $this->_getStatusMessage(21, 2);
//'type' => $docData['type_name'],
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'], 'fName' => $docData['first_name'], 'typeId' => $docData['type_id'],
            'lName' => $docData['last_name'], 'email' => $docData['email'],
            'mobile' => $docData['mobile'], 'status' => $docData['status'], 'amount' => $docData['amount'],
            'about' => preg_replace('/[^A-Za-z0-9 \-]/', '', $docData['about']), 'zip' => $docData['zipcode'],
            'pPic' => ($docData['profile_pic'] == '') ? $this->default_profile_pic : $docData['profile_pic'],
            'expertise' => $docData['expertise'],
            'licNo' => $docData['medical_license_num'],
            'licExp' => ($docData['board_certification_expiry_dt'] == '') ? '' : date('F d, Y', strtotime($docData['board_certification_expiry_dt'])),
            'totRats' => count($reviewsArr), 'num_of_job_images' => $docData['num_of_job_images'],
            'avgRate' => round($docData['avgRate'], 1),
            'rejApts' => $docData['rejApts'], 'cmpltApts' => $docData['cmpltApts'],
            'cmpltApts' => $docData['cmpltApts'], 'todayAmt' => round($docData['today_earnings'], 2),
            'lastBilledAmt' => round($docData['last_billed_amount'], 2),
            'weekAmt' => round($docData['week_earnings'], 2), 'monthAmt' => round($docData['month_earnings'], 2),
            'totalAmt' => round($docData['total_earnings'], 2),
            'selCat' => $allcatstr,
            'catArray' => $catArray,
            'job_images' => $jobImagesArr,
            'languages' => $docData['languages'], 'qry' => $selectMasterProfileQry);
    }

    /*
     * Method name: updateMasterProfile
     * Desc: update master profile
     * Input: Request data
     * Output:  success if got it else error according to the result
     */

    protected function updateMasterProfile($args) {
        //ent_about , ent_name,ent_lang,ent_expertise,ent_profile_img
        if ($args['ent_name'] == '')
            return $this->_getStatusMessage(1, 'Provider Name Missing');

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
        if (is_array($returned))
            return $returned;
//        $this->User['entityId'] = 23;

        $about = "";
        $expertise = "";
        $first_name = "";
        $languages = "";
        $profile_pic = "";
        $num_of_job_images = "";

        $about = 'about = "' . $args['ent_about'] . '"';
        $expertise = ',expertise = "' . $args['ent_expertise'] . '"';
        $first_name = ',first_name = "' . $args['ent_name'] . '"';
        $last_name = ',last_name = "' . $args['ent_last_name'] . '"';
        $languages = ',languages = "' . $args['ent_lang'] . '"';
        $profile_pic = ',profile_pic = "' . $args['ent_profile_img'] . '"';

        $updateProfileQry = "update doctor set " . $about . $expertise . $first_name . $last_name . $languages . $profile_pic . "  where doc_id = '" . $this->User['entityId'] . "'";
        $location = $this->mongo->selectCollection('location');
        $location->update(array('user' => (int) $this->User['entityId']), array('$set' => array('name' => $args['ent_name'], 'lname' => $args['ent_last_name'], 'image' => $args['ent_profile_img'])));

        if (isset($args['ent_job_img'])) {
            $jobimgArr = array();
            $args['ent_job_img'] = explode(",", $args['ent_job_img']);
            foreach ($args['ent_job_img'] as $url) {
                $jobimgArr[] = array('url' => $url);
            }
//            $location->update(array('user' => (int) $this->User['entityId']), array('$push' => array('job_images' => $jobimgArr)));
            $location->update(array('user' => (int) $this->User['entityId']), array('$set' => array('job_images' => $jobimgArr)));
        }
        mysql_query($updateProfileQry, $this->db->conn);
        if (mysql_affected_rows() >= 0) {
            $errMsgArr = $this->_getStatusMessage(23, 1);
        } else {
            $errMsgArr = $this->_getStatusMessage(22, 1);
        }
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'], 'test' => $updateProfileQry);
    }

    protected function updateMasterJobImages($args) {

        if (!isset($args['ent_job_img'])) {
            return $this->_getStatusMessage(1, "ent_job_img Tag Missing");
        }
        if (!is_array($args['ent_job_img'])) {
            return $this->_getStatusMessage(1, "ent_job_img Tag is an Array");
        }

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
        if (is_array($returned))
            return $returned;

        $location = $this->mongo->selectCollection('location');
        $jobimgArr = array();
        foreach ($args['ent_job_img'] as $url) {
            $jobimgArr[] = array('url' => $url);
        }
        $updateRes = $location->update(array('user' => (int) $this->User['entityId']), array('$set' => array('job_images' => $jobimgArr)));

        $errMsgArr = $this->_getStatusMessage(23, 1);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'], 'test' => $updateRes);
    }

    /*
     * Method name: abortAppointment
     * Desc: Passenger can Cancel an appointment requested
     * Input: Request data
     * Output:  success if got it else error according to the result
     */

    protected function abortAppointment($args) {

        if ($args['ent_bid'] == '' || $args['ent_reason'] == '' || $args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, $args);

        $this->curr_date_time = urldecode($args['ent_date_time']);
        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
        if (is_array($returned))
            return $returned;

        $args['ent_appnt_dt'] = urldecode($args['ent_appnt_dt']);

        $bookingColl = $this->mongo->selectCollection('bookings');
        $apptDet = $bookingColl->findOne(array('bid' => (string) $args['ent_bid']));

        $location = $this->mongo->selectCollection('location');
        if ($apptDet['booking_type'] == '1') {
            $location->update(array('user' => (int) $this->User['entityId']), array('$set' => array('booked' => 0)));
        }

        if (!$apptDet)
            return $this->_getStatusMessage(62, 32);

        if ($apptDet['status'] == '3')
            return $this->_getStatusMessage(44, 44);

        $docData = $this->_getEntityDet($args['ent_email'], '2');


        $status_msg = "Cancelled By Provider " . $args['ent_reason'];
        $bookingColl->update(array('bid' => (string) $args['ent_bid']), array('$set' => array('status' => 10,
                'last_modified_dt' => $this->curr_date_time,
//                'provider_id' => "0",
                'cancel_reason' => $args['ent_reason'])));

        $bookingColl->update(array('bid' => (string) $args['ent_bid'],
            'dispatched.pid' => (string) $this->User['entityId']), array('$set' => array('dispatched.$.res' => 10,
                'dispatched.$.status_msg' => $status_msg)));

        $message = "Your appointment at " . date('jS F', strtotime($apptDet['appt_date'])) . " has been cancelled";

        if ($apptDet['booking_type'] == '2') {

            $slot = $this->mongo->selectCollection('slotbooking');
            $dt = explode(' ', $apptDet['appt_date']);
            $date = explode('-', $dt[0]);
            $days = explode(':', $dt[1]);
            $start = mktime($days[0], $days[1], $days[2], $date[1], $date[2], $date[0]);

            $check = ($apptDet['slot_id'] != '') ? array('_id' => new MongoId($apptDet['slot_id'])) : array('start' => $start, 'doc_id' => (int) $this->User['entityId']);

            $update = $slot->update($check, array('$set' => array('booked' => 1, 'status' => 3)));
        }

        $aplPushContent = array('alert' => $message, 'st' => '10', 'sound' => 'default', 'r' => $args['ent_reason'], 'dt' => $args['ent_appnt_dt'], 'bid' => $apptDet['bid']);
        $andrPushContent = array('payload' => $message, 'st' => '10', 'sname' => $this->User['firstName'], 'dt' => $apptDet['appt_date'], 'email' => $this->User['email'], 'bid' => $apptDet['bid']);
        $pushNum['push'] = $this->_sendPush($this->User['entityId'], array($apptDet['customer']['id']), $message, '10', $this->User['firstName'], $this->curr_date_time, '2', $aplPushContent, $andrPushContent);

        $mail = new sendAMail(APP_SERVER_HOST);
        $apptDet = $bookingColl->findOne(array('bid' => (string) $args['ent_bid']));
        $mailArr[] = $mail->CancelledByPro_MailToCust($apptDet);
        $mailArr[] = $mail->CancelledByPro_MailToPro($apptDet);

        // make provider online again
        $location = $this->mongo->selectCollection('location');
        $location->update(array('user' => (int) $this->User['entityId']), array('$set' => array('booked' => 0)));

        if ($apptDet['booking_type'] == '2') {
            $pubnubContent = array('a' => 15, 'bid' => $apptDet['bid'], 'slot' => $apptDet['slot_id']);
        }

        $argstopass = array('bid' => $args['ent_bid'], 'proid' => $this->User['entityId'], 'bstatus' => 10);
        $this->sendDatatoSocket($argstopass, "respondToAppointment");

        return $this->_getStatusMessage(42, 3);
    }

    /*
     * Method name: abortFromAdmin
     * Desc: Passenger can Cancel an appointment requested
     * Input: Request data
     * Output:  success if got it else error according to the result
     */

    protected function abortFromAdmin($args) {

        if ($args['ent_appnt_dt'] == '' || $args['ent_doc_id'] == '' || $args['ent_slot_id'] == '' || $args['ent_email'] == '' || $args['ent_reason'] == '' || $args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, $args);

        $this->curr_date_time = urldecode($args['ent_date_time']);

//        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
//        if (is_array($returned))
//            return $returned;

        $args['ent_appnt_dt'] = urldecode($args['ent_appnt_dt']);

        $getApptDetQry = "select a.appointment_dt,a.status,a.appointment_id,a.patient_id from appointment a,patient d where a.patient_id = d.patient_id and d.email = '" . $args['ent_email'] . "' and a.doc_id = '" . $args['ent_doc_id'] . "' and a.appointment_dt = '" . $args['ent_appnt_dt'] . "'";
        $getApptDetRes = mysql_query($getApptDetQry, $this->db->conn);
        $apptDet = mysql_fetch_assoc($getApptDetRes);

        if (!is_array($apptDet))
            return $this->_getStatusMessage(62, $getApptDetQry);

        if ($apptDet['status'] == '3')
            return $this->_getStatusMessage(44, 44);

        $docData = $this->_getEntityDet($args['ent_email'], '2');

        $cancelApntQry = "update appointment set status = 10,last_modified_dt = '" . $this->curr_date_time . "',cancel_status = '" . $args['ent_reason'] . "' where doc_id = '" . $args['ent_doc_id'] . "' and patient_id = '" . $docData['patient_id'] . "' and appointment_dt = '" . $args['ent_appnt_dt'] . "'";
        mysql_query($cancelApntQry, $this->db->conn);

        if (mysql_affected_rows() <= 0)
            return $this->_getStatusMessage(3, 3);

        $message = "Your appointment at " . date('jS F', strtotime($apptDet['appointment_dt'])) . " has been cancelled";

        $this->ios_cert_path = $this->ios_uberx_slave;
        $this->ios_cert_pwd = $this->ios_slv_pwd;

        $selectDoc = "select * from doctor where doc_id = '" . $args['ent_doc_id'] . "'";
        $docDet = mysql_fetch_assoc(mysql_query($selectDoc, $this->db->conn));

        $aplPushContent = array('alert' => $message, 'nt' => '10', 't' => $apptDet['appt_type'], 'sound' => 'default', 'id' => $apptDet['bid'], 'r' => (int) $args['ent_reason']); //, 'd' => $apptDet['appointment_dt'], 'e' => $this->User['email']
        $andrPushContent = array('payload' => $message, 'action' => '10', 't' => $apptDet['appt_type'], 'sname' => $docDet['first_name'], 'dt' => $apptDet['appointment_dt'], 'e' => $docDet['email'], 'bid' => $apptDet['bid']);
        $pushNum['push'] = $this->_sendPush($args['ent_doc_id'], array($apptDet['patient_id']), $message, '10', $docDet['first_name'], $this->curr_date_time, '2', $aplPushContent, $andrPushContent, $apptDet['user_device']);

        $aplPushContent1 = array('alert' => $message, 'nt' => '3');
        $andrPushContent1 = array("payload" => $message, 'action' => '3');
        $pushNum['push1'] = $this->_sendPush($apptDet['patient_id'], array($args['ent_doc_id']), $message, '3', $docDet['first_name'], $this->curr_date_time, '1', $aplPushContent1, $andrPushContent1);

        $location = $this->mongo->selectCollection('location');

        $location->update(array('user' => (int) $args['ent_doc_id']), array('$set' => array('status' => 3)));

        $slotbooking = $this->mongo->selectCollection('slotbooking');

        $slotbooking->update(array('_id' => new MongoId($args['ent_slot_id'])), array('$set' => array('booked' => 1)));

        return $this->_getStatusMessage(42, $cancelApntQry);
    }

    /*
     * Method name: cancelAppointment
     * Desc: Patient can Cancel an appointment requested
     * Input: Request data
     * Output:  success if got it else error according to the result
     */

    protected function cancelAppointment($args) {

        if ($args['ent_bid'] == '' || $args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, 1);

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '2');
        if (is_array($returned))
            return $returned;

        $this->curr_date_time = urldecode($args['ent_date_time']);
        $args['ent_appnt_dt'] = urldecode($args['ent_appnt_dt']);
        if (is_null($args['ent_reason']))
            $args['ent_reason'] = "";

        $bookings = $this->mongo->selectCollection('bookings');
        $apptDet = $bookings->findOne(array('bid' => (string) $args['ent_bid']));

        if (!$apptDet)
            return $this->_getStatusMessage(49, 1);

        if ($apptDet['status'] == '4' || $apptDet['status'] == '9')
            return $this->_getStatusMessage(73, 73);

        if ($apptDet['status'] == '7')
            return $this->_getStatusMessage(74, 74);

        $status = 4;
        $errorNum = 42;
        $chargeAmt = 0;
        if ($apptDet['provider_id'] == "0") {
            $errorNum = 42;
            $bookings->update(array('bid' => (string) $args['ent_bid']), array('$set' => array('cancel_reason' => $args['ent_reason'], 'status' => $status,
                    'cancel_amount' => $chargeAmt, 'appt_last_modified' => $this->curr_date_time)));
            $errMsgArr = $this->_getStatusMessage($errorNum, $errorNum);
            return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg']);
        } else {
            $dt = explode(' ', $apptDet['appt_date']);
            $date = explode('-', $dt[0]);
            $days = explode(':', $dt[1]);
            $dt1 = explode(' ', $this->curr_date_time);
            $date1 = explode('-', $dt1[0]);
            $days1 = explode(':', $dt1[1]);
            $curr_dt = mktime($days1[0], $days1[1], $days1[2], $date1[1], $date1[2], $date1[0]);
            $before_24hours = mktime($days[0], $days[1], $days[2], $date[1], $date[2], $date[0]) - (24 * 60 * 60);
            if (($apptDet['booking_type'] == '1' && ($apptDet['status'] == '2' || $apptDet['status'] == '5' || $apptDet['status'] == '6')) ||
                    ($apptDet['booking_type'] == '3' && ($apptDet['status'] == '2' || $apptDet['status'] == '5' || $apptDet['status'] == '6')) ||
                    ($apptDet['booking_type'] == '2' && $curr_dt >= $before_24hours && ($apptDet['status'] == '2' || $apptDet['status'] == '5' || $apptDet['status'] == '6'))) {
                $status = 9;
                $chargeAmt = $apptDet['cancel_amount'];
                $errorNum = 43;
                $invoMail = array();
                $apptDet = $bookings->findOne(array('bid' => (string) $args['ent_bid']));
                $comp = array('ctime' => $this->curr_date_time,
                    'sign_url' => "", 'misc_fees' => 0,
                    'total_pro' => 0, 'pro_disc' => 0,
                    'mat_fees' => 0, 'complete_note' => "",
                    'time_fees' => 0, 'sub_total_cust' => 0);
                $bookings->update(array('bid' => $args['ent_bid']), array('$set' => array('comp' => $comp)));

                //Start Carge Card
                if ($apptDet['payment_type'] == 2) {
                    $patData = $this->_getEntityDet($apptDet['customer']['email'], '2');
                    $chargeCustomerArr = array('stripe_id' => $patData['stripe_id'], 'amount' => (int) ((float) $apptDet['cancel_amount'] * 100), 'currency' => 'USD', 'description' => 'From ' . $patData['email']);
                    $trStripe = $this->stripe->apiStripe('chargeCard', $chargeCustomerArr);
                    $taranseferId = "";
                    if ($trStripe['error']) {
                        $charge = array('errNum' => 16, 'errFlag' => 1, 'errMsg' => $trStripe['error']['message'], 'test' => 1);
                    } else {
                        $taranseferId = $trStripe['id'];
                    }
                } else {
                    $taranseferId = "cash_000_" . $apptDet['bid'];
                }
                //End Carg Card

                $bookings->update(array('bid' => (string) $args['ent_bid']), array('$set' => array('cancel_reason' => $args['ent_reason'])));
                $argstopass = array('bid' => $args['ent_bid']);
                $this->sendDatatoSocket($argstopass, "cancelByCustomer");

                $updateInvoice = $this->__GenerateInvoiceDetailsForCancel($args['ent_bid'], $taranseferId);
                if (!isset($updateInvoice['invoiceData']))
                    return $this->_getStatusMessage(56, $updateInvoice);
                else {
                    $bookings->update(array("bid" => (string) $args['ent_bid']), array('$set' => array('invoiceData' => $updateInvoice['invoiceData'])));
                    $apptDet = $bookings->findOne(array('bid' => (string) $args['ent_bid']));
                    $mail = new sendAMail(APP_SERVER_HOST);
                    $invoMail[] = $mail->CancelledByCust_MailToCust($apptDet);
                    $invoMail[] = $mail->CancelledByCust_MailToPro($apptDet);
                    $invoMail[] = $mail->sendInvoice($apptDet);

                    $message = $this->User['firstName'] . " has cancelled the appointment dated " . $apptDet['appt_date'];
                    $aplPushContent1 = array('alert' => $message, 'nt' => 3, 'btype' => '4', 'sname' => $this->User['firstName'], 'dt' => $args['ent_appnt_dt'], 'e' => $this->User['email'], 'sound' => 'default');
                    $aplPushContent = array('alert' => $message, 'payload' => $aplPushContent1);
                    $andrPushContent = array("payload" => $message, 'action' => '3', 'sname' => $this->User['firstName'], 'dt' => $args['ent_appnt_dt'], 'e' => $this->User['email']);
                    $push = $this->_sendPush($this->User['entityId'], array($apptDet['provider_id']), $message, '6', $this->User['firstName'], $this->curr_date_time, '1', $aplPushContent, $andrPushContent);
                    $out = array('push' => $push, 'mail' => $invoMail);
                }
            }
        }

        $bookings->update(array('bid' => (string) $args['ent_bid']), array('$set' => array('cancel_reason' => $args['ent_reason'], 'status' => $status, 'cancel_amount' => $chargeAmt, 'appt_last_modified' => $this->curr_date_time)));

        if ($apptDet['booking_type'] == '2') {
            $slot = $this->mongo->selectCollection('slotbooking');
            $dt = explode(' ', $apptDet['appt_date']);
            $date = explode('-', $dt[0]);
            $days = explode(':', $dt[1]);
            $start = mktime($days[0], $days[1], $days[2], $date[1], $date[2], $date[0]);
            $check = ($apptDet['slot_id'] != '') ? array('_id' => new MongoId($apptDet['slot_id'])) : array('start' => $start, 'doc_id' => (int) $this->User['entityId']);
            $update = $slot->update($check, array('$set' => array('booked' => 1, 'status' => 3)));
        } else {
            // make provider online again
            $locationColl = $this->mongo->selectCollection('location');
            $locationColl->update(array('user' => (int) $apptDet['provider_id']), array('$set' => array('booked' => 0)));
        }

        $errMsgArr = $this->_getStatusMessage($errorNum, $errorNum);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'], 'test' => $out);
    }

    /*
     * Method name: getProfile
     * Desc: Get slave profile data
     * Input: Request data
     * Output:  success array if got it else error according to the result
     */

    protected function getProfile($args) {

        if ($args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, 1);

        $this->curr_date_time = urldecode($args['ent_date_time']);

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '2');

        if (is_array($returned))
            return $returned;
        mysql_query("utf8", $this->db->conn);
        $selectProfileQry = "select * from patient where patient_id = '" . $this->User['entityId'] . "'";
        $profileData = mysql_fetch_assoc(mysql_query($selectProfileQry, $this->db->conn));

        if (!is_array($profileData))
            $errMsgArr = $this->_getStatusMessage(20, 20);

        $errMsgArr = $this->_getStatusMessage(33, 2);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'],
            'errMsg' => $errMsgArr['errMsg'], 'fName' => $profileData['first_name'],
            'lName' => $profileData['last_name'], 'email' => $profileData['email'],
            'country_code' => $profileData['country_code'], 'phone' => $profileData['phone'],
            'Languages' => $this->getLangueage(),
            'pPic' => ($profileData['profile_pic'] == '' ? $this->default_profile_pic : $profileData['profile_pic']));
    }

    /*
     * Method name: updateProfile
     * Desc: Update slave profile data
     * Input: Request data
     * Output:  success array if got it else error according to the result
     */

    protected function updateProfile($args) {

        if ($args['ent_first_name'] == '' || $args['ent_date_time'] == '' || $args['ent_cid'] == '')
            return $this->_getStatusMessage(1, 1);

        $this->curr_date_time = urldecode($args['ent_date_time']);

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '2');
        if (is_array($returned))
            return $returned;

        $update_name = " first_name = '" . $args['ent_first_name'] . "', ";
        $update_lname = " last_name = '" . $args['ent_last_name'] . "', ";
        $update_pp = " profile_pic = '" . $args['ent_ppic'] . "', ";
        $update_str = rtrim($update_name . $update_lname . $update_pp, ', ');
        $updateQry = "update patient set " . $update_str . ", last_active_dt = '" . $this->curr_date_time . "' where patient_id = '" . $args['ent_cid'] . "'";

        $updateRes = mysql_query($updateQry, $this->db->conn);
        if (mysql_affected_rows() > 0)
            return $this->_getStatusMessage(54, 39);
        else if ($updateRes)
            return $this->_getStatusMessage(54, 40);
        else
            return $this->_getStatusMessage(3, $updateQry);
    }

    /*
     * Method name: addCard
     * Desc: Add a card to the patient profile
     * Input: Request data
     * Output:  success array if got it else error according to the result
     */

    protected function addCard($args) {

//        $stripetest = $this->mongo->selectCollection('stripetest');
//        $stripetest->insert(array('aa' => $args));

        if ($args['ent_token'] == '' || $args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, 1);

        $this->curr_date_time = urldecode($args['ent_date_time']);

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '2');
        if (is_array($returned))
            return $returned;

        if ($this->User['stripe_id'] == '') {
            $createCustomerArr = array('token' => $args['ent_token'], 'email' => $this->User['email']);

            $customer = $this->stripe->apiStripe('createCustomer', $createCustomerArr);

            if ($customer['error'])
                return array('errNum' => 16, 'errFlag' => 1, 'errMsg' => $customer['error']['message'], 'test' => STRIPE_SECRET_KEY);

            $updateQry = "update patient set stripe_id = '" . $customer['id'] . "', last_active_dt = '" . $this->curr_date_time . "' where patient_id = '" . $this->User['entityId'] . "'";
            mysql_query($updateQry, $this->db->conn);
            if (mysql_affected_rows() <= 0)
                return $this->_getStatusMessage(51, 50);

            $getCardArr = array('stripe_id' => $customer['id']);

            $card = $this->stripe->apiStripe('getCustomer', $getCardArr);

            if ($card['error'])
                return array('errNum' => 16, 'errFlag' => 1, 'errMsg' => $card['error']['message'], 'test' => 2);

            foreach ($card['sources']['data'] as $c) {
                $cardRes[] = array('id' => $c['id'], 'last4' => $c['last4'], 'type' => $c['brand'], 'exp_month' => $c['exp_month'], 'exp_year' => $c['exp_year']);
            }

            $errMsgArr = $this->_getStatusMessage(50, 50);
            return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'], 'cards' => $cardRes, 'def' => $card['default_source']); //, 'cards' => array('id' => $customer['data']['id'], 'last4' => $customer['data']['last4'], 'type' => $customer['data']['type'], 'exp_month' => $customer['data']['exp_month'], 'exp_year' => $customer['data']['exp_year']));
        }

        $addCardArr = array('stripe_id' => $this->User['stripe_id'], 'token' => $args['ent_token']);

        $card = $this->stripe->apiStripe('addCard', $addCardArr);

        if ($card['error'])
            return array('errNum' => 16, 'errFlag' => 1, 'errMsg' => $card['error']['message'], 'test' => 3); //$card,'t'=>$addCardArr);

        $getCard = $this->stripe->apiStripe('getCustomer', $addCardArr);

        foreach ($getCard['sources']['data'] as $card) {
            $cardsArr[] = array('id' => $card['id'], 'last4' => $card['last4'], 'type' => $card['brand'], 'exp_month' => $card['exp_month'], 'exp_year' => $card['exp_year']);
        }

        $errMsgArr = $this->_getStatusMessage(50, 50);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'], 'cards' => $cardsArr, 'def' => $getCard['default_source']);
    }

    /*
     * Method name: getCards
     * Desc: Add a card to the patient profile
     * Input: Request data
     * Output:  success array if got it else error according to the result
     */

    protected function getCards($args) {

        if ($args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, 1);

        $this->curr_date_time = urldecode($args['ent_date_time']);

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '2');
        if (is_array($returned))
            return $returned;

        if ($this->User['stripe_id'] == '')
            return $this->_getStatusMessage(51, 51);

        $getCardArr = array('stripe_id' => $this->User['stripe_id']);

        $cardsArr = array();

        $card = $this->stripe->apiStripe('getCustomer', $getCardArr);

        if ($card['error'])
            return array('errNum' => 16, 'errFlag' => 1, 'errMsg' => $card['error']['message'], 'test' => 2);

        foreach ($card['sources']['data'] as $c) {
            $cardsArr[] = array('id' => $c['id'], 'last4' => $c['last4'], 'type' => $c['brand'], 'exp_month' => $c['exp_month'], 'exp_year' => $c['exp_year']);
        }

        if (count($cardsArr) > 0)
            $errNum = 52;
        else
            $errNum = 51;

        $errMsgArr = $this->_getStatusMessage($errNum, 52);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'], 'cards' => $cardsArr, 'def' => $card['default_source']);
    }

    /*
     * Method name: removeCard
     * Desc: Add a card to the patient profile
     * Input: Request data
     * Output:  success array if got it else error according to the result
     */

    protected function removeCard($args) {

        if ($args['ent_cc_id'] == '' || $args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, 1);

        $this->curr_date_time = urldecode($args['ent_date_time']);

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '2');

        if (is_array($returned))
            return $returned;

        if ($this->User['stripe_id'] == '')
            return $this->_getStatusMessage(51, 51);

        $remCardArr = array('stripe_id' => $this->User['stripe_id'], 'card_id' => $args['ent_cc_id']);

        $cardsArr = array();

        $card = $this->stripe->apiStripe('deleteCard', $remCardArr);

        if ($card->error)
            return array('errNum' => 16, 'errFlag' => 1, 'errMsg' => $card->error->message);

        foreach ($card->data as $card) {
            $cardsArr = array('id' => $card->data->id, 'last4' => $card->data->last4, 'type' => $card->data->brand, 'exp_month' => $card->data->exp_month, 'exp_year' => $card->data->exp_year);
        }

        $errMsgArr = $this->_getStatusMessage(52, 52);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'], 'cards' => $cardsArr);
    }

    /*
     * Method name: validateEmailZip
     * Desc: Validates the email and zipcode
     * Input: Token
     * Output:  gives error array if unavailable
     */

    protected function validateEmailZip($args) {

        if ($args['ent_email'] == '' || $args['ent_user_type'] == '')
            return $this->_getStatusMessage(1, 1);


        if ($args['ent_user_type'] == '1')
            $verifyEmail = $this->_verifyEmail($args['ent_email'], 'doc_id', 'doctor'); //_verifyEmail($email,$field,$table);
        else
            $verifyEmail = $this->_verifyEmail($args['ent_email'], 'patient_id', 'patient'); //_verifyEmail($email,$field,$table);

        if (is_array($verifyEmail))
            $email = array('errFlag' => 1);
        else
            $email = array('errFlag' => 0);

        if ($args['zip_code'] == '') {
            $zip = array('errFlag' => 0);
        } else {

//            $selectZipQry = "select zipcode from zipcodes where zipcode = '" . $args['zip_code'] . "'";
//            $selectZipRes = mysql_query($selectZipQry, $this->db->conn);
//            if (mysql_num_rows($selectZipRes) > 0)
            $zip = array('errFlag' => 0);
//            else
//                $zip = array('errFlag' => 1);
        }

        if ($email['errFlag'] == 0 && $zip['errFlag'] == 0)
            return $this->_getStatusMessage(47, $verifyEmail);
//        else if ($email['errFlag'] == 1 && $zip['errFlag'] == 1)
//            return $this->_getStatusMessage(46, $verifyEmail);
//        else if ($email['errFlag'] == 0 && $zip['errFlag'] == 1)
//            return $this->_getStatusMessage(46, $verifyEmail);
        else if ($email['errFlag'] == 1 && $zip['errFlag'] == 0)
            return $this->_getStatusMessage(2, $verifyEmail);
    }

    /*
     * Method name: validateEmail
     * Desc: Validates the email
     * Input: Token
     * Output:  gives error array if unavailable
     */

    protected function validateEmail($args) {

        if ($args['ent_email'] == '')
            return $this->_getStatusMessage(1, 1);

        $vmail = new verifyEmail();
        if ($vmail->check($args['ent_email'])) {
            return $this->_getStatusMessage(34, $args['ent_email']);
        } else if ($vmail->isValid($args['ent_email'])) {
            return $this->_getStatusMessage(24, $args['ent_email']);
        } else {
            return $this->_getStatusMessage(25, $args['ent_email']);
        }
    }

    /*
     * Method name: updateMasterStatus
     * Desc: Update master status
     * Input: Token
     * Output:  gives error array if failed
     */

    protected function updateMasterStatus($args) {

        if ($args['ent_date_time'] == '' || $args['ent_status'] == '')
            return $this->_getStatusMessage(1, 1);


        $this->curr_date_time = urldecode($args['ent_date_time']);

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');

        if (is_array($returned))
            return $returned;

        if ($this->User['status'] != '3')
            return $this->_getStatusMessage(10, 1);

        $location = $this->mongo->selectCollection('location');

//        $deleteAllSessionsQry = "update doctor set status = '" . $status . "' where doc_id = '" . $this->User['entityId'] . "'";
//        mysql_query($deleteAllSessionsQry, $this->db->conn);
//
//        if (mysql_affected_rows() < 0)
//            return $this->_getStatusMessage(70, $deleteAllSessionsQry);

        $update = $location->update(array('user' => (int) $this->User['entityId']), array('$set' => array('status' => (int) ($args['ent_status'] == 3 ? 3 : 4))));

        $argstopass = array('proId' => $this->User['entityId']);
        $this->sendDatatoSocket($argstopass, "location");

        return $this->_getStatusMessage(69, $update);
    }

    /*
     * Method name: checkCoupon
     * Desc: Check coupon exists or not, if exists check the expirty
     * Input: Request data
     * Output:  Success if available else error
     */

    protected function checkCoupon($args) {

        $rish = $this->mongo->selectCollection('rish');
        $rish->insert(array('b' => $args));

        if ($args['ent_coupon'] == '')
            return $this->_getStatusMessage(1, 'Coupon');
        if ($args['ent_lat'] == '' || $args['ent_long'] == '')
            return $this->_getStatusMessage(1, 'Location');

        $this->curr_date_time = urldecode($args['ent_date_time']);

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '2');

        if (is_array($returned))
            return $returned;

        $cc = new MongoRegex("/^" . $args['ent_coupon'] . "/i");

        // $cond = array('status' => 0, 'coupon_code' => $cc, '$or' => array(array('coupon_type' => 3, 'user_id' => (string) $this->User['entityId'], 'status' => 0, 'expiry_date' => array('$gte' => time())), array('coupon_type' => 2, 'bookings.slave_id' => array('$ne' => (string) $this->User['entityId']), 'expiry_date' => array('$gte' => time()))));

        if ($args['ent_coupon'] == "test") {
            $cond = array('status' => 0, 'coupon_code' => $cc, '$or' => array(array('coupon_type' => 3, 'user_id' => (string) $this->User['entityId'], 'status' => 0, 'expiry_date' => array('$gte' => time())), array('coupon_type' => 2, 'expiry_date' => array('$gte' => time()))));
        } else {
            $cond = array('status' => 0, 'coupon_code' => $cc, '$or' => array(array('coupon_type' => 3, 'user_id' => (string) $this->User['entityId'], 'status' => 0, 'expiry_date' => array('$gte' => time())), array('coupon_type' => 2, 'bookings.slave_id' => array('$ne' => (string) $this->User['entityId']), 'expiry_date' => array('$gte' => time()))));
        }

        $resultArr = $this->mongo->selectCollection('$cmd')->findOne(array(
            'geoNear' => 'coupons',
            'near' => array(
                (double) $args['ent_long'], (double) $args['ent_lat']
            ), 'spherical' => true, 'maxDistance' => ($this->promoCodeRadius * 1000) / 6378137, 'distanceMultiplier' => 6378137,
            'query' => $cond)
        );

        if (count($resultArr['results']) <= 0)
            return $this->_getStatusMessage(96, $cond);

        $findOne = $resultArr['results'][0]['obj'];

        $findOne['current'] = time();
//        if ($findOne['start_date'] > strtotime($this->curr_date_time) || $findOne['expiry_date'] < strtotime($this->curr_date_time))
//            return $this->_getStatusMessage(96, $findOne);

        $errMsgArr = $this->_getStatusMessage(98, 52);
        $dtype = "";
        if ($findOne['discount_type'] == 1) {
            $dtype = "Percent";
        } else {
            $dtype = "Fixed";
        }
        $discount = $findOne['discount'];
        if ($findOne['discount_type'] == 1) {
            return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'dtype' => $dtype, 'dis' => $discount, 'errMsg' => sprintf($errMsgArr['errMsg'], '', $findOne['discount'], ($findOne['discount_type'] == 1 ? '%' : '')));
        } else {
            return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'dtype' => $dtype, 'dis' => $discount, 'errMsg' => sprintf($errMsgArr['errMsg'], CURRENCY, $findOne['discount'], ($findOne['discount_type'] == 1 ? '%' : '')));
        }
    }

    /*
     * Method name: verifyCode
     * Desc: Check coupon exists or not, if exists check the expirty
     * Input: Request data
     * Output:  Success if available else error
     */

    protected function verifyCode($args) {


        if ($args['ent_coupon'] == '')
            return $this->_getStatusMessage(1, 'Coupon');
        else if ($args['ent_lat'] == '' || $args['ent_long'] == '')
            return $this->_getStatusMessage(1, 'Location');
        $cc = '/^"' . $args['ent_coupon'] . '"$/i';
        $resultArr = $this->mongo->selectCollection('$cmd')->findOne(array(
            'geoNear' => 'coupons',
            'near' => array(
                (double) $args['ent_long'], (double) $args['ent_lat']
            ), 'spherical' => true, 'maxDistance' => ($this->promoCodeRadius * 1000) / 6378137, 'distanceMultiplier' => 6378137,
            'query' => array('status' => 0, 'coupon_code' => $args['ent_coupon'], 'coupon_type' => 1, 'user_type' => 1))
        );
        //   /^TeST1$/i
        if (count($resultArr['results']) > 0) {
            return $this->_getStatusMessage(95, 1);
        } else {
            return $this->_getStatusMessage(94, 2);
        }
    }

    protected function GetCustomerAddress($args) {

        if ($args['ent_cust_id'] == '')
            return $this->_getStatusMessage(1, 1);

//        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
//        if (is_array($returned))
//            return $returned;

        $custid = $args['ent_cust_id'];
        $customer = $this->mongo->selectCollection('customer');
        $cursor = $customer->find(array('cid' => (int) $custid));
        $adata = array();
        foreach ($cursor as $cur) {
            if (isset($cur['addlist'])) {
                foreach ($cur['addlist'] as $val) {
                    $val['lng'] = $val['long'];
                    $adata[] = $val;
                }
            }
        }
        $errMsgArr = $this->_getStatusMessage(113, 113);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'], 'addlist' => $adata);
    }

    protected function AddCustomerAddress($args) {

        if ($args['ent_cust_id'] == '' || $args['ent_address1'] == '' || $args['ent_latitude'] == '' || $args['ent_longitude'] == '')
            return $this->_getStatusMessage(1, 1);

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '2');
        if (is_array($returned))
            return $returned;

        if (!isset($args['ent_address2']) || is_null($args['ent_address2']))
            $args['ent_address2'] = "";

        $custid = $args['ent_cust_id'];
        $addid = $custid . mt_rand();
        $data = array('aid' => $addid, 'address1' => $args['ent_address1'],
            'address2' => $args['ent_address2'], 'zipcode' => $args['ent_zipcode'],
            'tag_address' => $args['ent_tag_address'], 'lat' => $args['ent_latitude'],
            'suite_num' => $args['ent_suite_num'],
            'long' => $args['ent_longitude']);

        $customer = $this->mongo->selectCollection('customer');
        $customer->update(array('cid' => (int) $custid), array('$push' => array('addlist' => $data))
        );

        $errMsgArr = $this->_getStatusMessage(111, 111);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'], 'addressid' => $addid);
    }

    protected function DeleteCustomerAddress($args) {

        if ($args['ent_cust_id'] == '' || $args['ent_addressid'] == '')
            return $this->_getStatusMessage(1, 1);

//        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
//        if (is_array($returned))
//            return $returned;

        $custid = $args['ent_cust_id'];
        $data = array('$pull' => array('addlist' => array('aid' => (string) $args['ent_addressid'])));

        $customer = $this->mongo->selectCollection('customer');
        $customer->update(array('cid' => (int) $custid), $data);

        $cursor = $customer->find(array('cid' => (int) $custid));
        $adata = array();
        foreach ($cursor as $cur) {
            array_push($adata, $cur['addlist']);
        }
        if (!is_array($adata[0]))
            $adata[] = array();

        $errMsgArr = $this->_getStatusMessage(112, 112);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'], 'addlist' => $adata[0]);
    }

    //Start Provider Address Part
    protected function GetProviderAddress($args) {

        if ($args['ent_pro_id'] == '')
            return $this->_getStatusMessage(1, 1);

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
        if (is_array($returned))
            return $returned;

        $proid = (int) $args['ent_pro_id'];
        $provider = $this->mongo->selectCollection('masterLocations');
        $cursor = $provider->find(array('user' => (int) $proid));
        $adata = array();
        foreach ($cursor as $cur) {
            $cur['id'] = (string) $cur['_id'];
            $cur['address2'] = (string) $cur['address2'];
            unset($cur['_id']);
            array_push($adata, $cur);
        }
        $errMsgArr = $this->_getStatusMessage(113, 113);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'],
            'errMsg' => $errMsgArr['errMsg'], 'addlist' => $adata);
    }

    protected function AddProviderAddress($args) {

        if ($args['ent_pro_id'] == '' || $args['ent_address1'] == '' ||
                $args['ent_lat'] == '' || $args['ent_lng'] == '' || $args['ent_locName'] == '')
            return $this->_getStatusMessage(1, 1);

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
        if (is_array($returned))
            return $returned;

        $proid = (int) $args['ent_pro_id'];
        $data = array('user' => (int) $proid,
            'locName' => $args['ent_locName'],
            'address1' => $args['ent_address1'],
            'address2' => $args['ent_address2'],
            'lat' => (double) $args['ent_lat'],
            'lng' => (double) $args['ent_lng']);

        $provider = $this->mongo->selectCollection('masterLocations');
        $provider->insert($data);

        $errMsgArr = $this->_getStatusMessage(111, 111);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg']);
    }

    // Delete Provider Saved Address 
    protected function DeleteProviderAddress($args) {
        if ($args['ent_pro_id'] == '' || $args['ent_addressid'] == '')
            return $this->_getStatusMessage(1, 1);
        // check  User Session here
        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
        if (is_array($returned))
            return $returned;
        // select MasterLocation Collection From MongoDB
        $provider = $this->mongo->selectCollection('masterLocations');
        //Remove Address By AddressID
        $provider->remove(array('_id' => new MongoId($args['ent_addressid'])));
        // Find That Provider by Provider ID
        $cursor = $provider->find(array('user' => (int) $args['ent_pro_id']));
        $adata = array();
        // get  all other  address and return to the Provider
        foreach ($cursor as $cur) {
            $cur['id'] = (string) $cur['_id'];
            $cur['address2'] = (string) $cur['address2'];
            unset($cur['_id']);
            array_push($adata, $cur);
        }
        //get Message and return responce to the proider
        $errMsgArr = $this->_getStatusMessage(112, 112);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'],
            'errMsg' => $errMsgArr['errMsg'], 'addlist' => $adata);
    }

    //End Provider Address Part

    protected function addslot($args) {

        if ($args['ent_pro_id'] == '' || $args['ent_option'] == '' || $args['ent_start_time'] == '' ||
                $args['ent_location'] == '' || $args['ent_num_slot'] == '' || $args['ent_duration'] == '' ||
                $args['ent_price'] == '' || $args['ent_radius'] == '')
            return $this->_getStatusMessage(1, 1);

//        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
        if (is_array($returned))
            return $returned;
        date_default_timezone_set('UTC');
        $dateTime = explode(" ", $args['ent_start_time']);
        $dateArr = explode("-", $dateTime[0]);
        $startTimeArr = explode(":", $dateTime[1]);

        $slotbookingColl = $this->mongo->selectCollection('slotbooking');
        $locs = $this->mongo->selectCollection('masterLocations');
        $get_latlng = $locs->findOne(array("_id" => new MongoId($args['ent_location'])));

//        yyyy-mm-dd
        if ($args['ent_option'] == 1) {
            $from_ts = mktime($startTimeArr[0], $startTimeArr[1], 0, $dateArr[1], $dateArr[2], $dateArr[0]);
//            $to_ts = $args['ent_num_slot'] * ($from_ts + ($args['ent_duration'] * 60));
            $to_ts = $from_ts + ($args['ent_num_slot'] * ($args['ent_duration'] * 60));

            $cond = array('doc_id' => (int) $get_latlng['user'],
                'start' => array('$gte' => $from_ts),
                'end' => array('$lte' => $to_ts));
            $checkOtherSlots = $slotbookingColl->find($cond)->count();
            if ($checkOtherSlots > 0) {
                $errMsgArr = $this->_getStatusMessage(126, 126);
                return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'],
                    'errMsg' => $errMsgArr['errMsg'], 'test_cond' => $cond,
                    'test_suration' => $args['ent_duration'], 'test_numslot' => $args['ent_num_slot']);
            }
            $start = $from_ts;
            for ($i = 0; $i < $args['ent_num_slot']; $i++) {
                $next = $start + ($args['ent_duration'] * 60);
                $insertQry = array('doc_id' => (int) $args['ent_pro_id'],
                    'locId' => (string) $args['ent_location'],
                    'locName' => $get_latlng['locName'],
                    'start_dt' => date('Y-m-d H:i:s', $start),
                    'end_dt' => date('Y-m-d H:i:s', $next),
                    'start' => $start,
                    'end' => $next,
                    'loc' => array("longitude" => (double) $get_latlng['lng'],
                        'latitude' => (double) $get_latlng['lat']),
                    'booked' => 1, 'radius' => (int) $args['ent_radius'],
                    'status' => 0, 'slot_price' => $args['ent_price']);
                $slotbookingColl->insert($insertQry);
                $start = $start + ($args['ent_duration'] * 60);
            }
            $errMsgArr = $this->_getStatusMessage(125, 125);
        } else if ($args['ent_option'] == 2) {
            $repeatday = $args['ent_repeatday'];
            if ($repeatday == 1) {
                $dayarr = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');
            } else if ($repeatday == 2) {
                $dayarr = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri');
            } else if ($repeatday == 3) {
                $dayarr = array('Sat', 'Sun');
            } else {
                $dayarr = array();
            }
            $start_date = $dateTime[0];

            $end_date = $args['ent_end_date'];
            $end_date = date('Y-m-d H:i:s', strtotime($end_date . ' +1 day'));
            $begin = new DateTime($start_date);
            $end = new DateTime($end_date);
            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($begin, $interval, $end);
//            $flag = 0;
            foreach ($period as $dt) {
                $sd = $dt->format("Y-m-d");
                $day = date("D", strtotime($sd));
                if (in_array($day, $dayarr)) {
//                    $enddates = $sd;
                    $dateArr = explode('-', $sd);
//                    $enddatesarra = explode('-', $enddates);
                    $from_ts = mktime($startTimeArr[0], $startTimeArr[1], 0, $dateArr[1], $dateArr[2], $dateArr[0]);
//                    $to_ts = $args['ent_num_slot'] * ($from_ts + ($args['ent_duration'] * 60));
                    $to_ts = $from_ts + ($args['ent_num_slot'] * ($args['ent_duration'] * 60));

                    $cond = array('doc_id' => (int) $get_latlng['user'],
                        'start' => array('$gte' => $from_ts),
                        'end' => array('$lte' => $to_ts));
                    $checkOtherSlots = $slotbookingColl->find($cond)->count();
                    if ($checkOtherSlots > 0) {
                        $errMsgArr = $this->_getStatusMessage(126, 126);
                        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'],
                            'errMsg' => $errMsgArr['errMsg'], 'test' => $cond);
                    }
                    $start = $from_ts;
                    for ($i = 0; $i < $args['ent_num_slot']; $i++) {
                        $next = $start + ($args['ent_duration'] * 60);
                        $insertQry = array('doc_id' => (int) $args['ent_pro_id'],
                            'locId' => (string) $args['ent_location'],
                            'locName' => $get_latlng['locName'],
                            'start_dt' => date('Y-m-d H:i:s', $start),
                            'end_dt' => date('Y-m-d H:i:s', $next),
                            'start' => $start,
                            'end' => $next, 'loc' => array("longitude" => (double) $get_latlng['lng'],
                                'latitude' => (double) $get_latlng['lat']),
                            'booked' => 1, 'radius' => (int) $args['ent_radius'],
                            'status' => 0, 'slot_price' => $args['ent_price']);
                        $slotbookingColl->insert($insertQry);
                        $start = $start + ($args['ent_duration'] * 60);
                    }
                }
            }

            $errMsgArr = $this->_getStatusMessage(125, 125);
        } else {
            $errMsgArr = $this->_getStatusMessage(3, 3);
        }
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'],
            'errMsg' => $errMsgArr['errMsg']);
    }

    // Api Used For Removing Created Slot From Provider Application    
    protected function removeSlot($args) { // request parameters are slot ID and Provider ID
        if ($args['ent_pro_id'] == '' || $args['ent_slot_id'] == '')
            return $this->_getStatusMessage(1, 1);
        // check  user Session Valid Or Expired
        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
        if (is_array($returned))
            return $returned; //  is session not valid  return  responce from here






















































































































































































































































            
// select Slot Booking Collection here from MongoDB
        $slotbookingColl = $this->mongo->selectCollection('slotbooking');
        // Remove that Later Booking by Slot Id
        $slotbookingColl->remove(array('_id' => new MongoId($args['ent_slot_id'])));
        // Get Message For SLot Removed  by Message Id
        $errMsgArr = $this->_getStatusMessage(127, 127);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'],
            'errMsg' => $errMsgArr['errMsg']);
    }

    // Get Slot Details  By Slot Id like start_time, End_time, price radias etc
    protected function GetBookedSlotDetail($args) {
        if ($args['ent_slot_id'] == '')
            return $this->_getStatusMessage(1, 1);
        // check  user Session Here for valid or expired
        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
        if (is_array($returned))
            return $returned;

        $slotbookingColl = $this->mongo->selectCollection('bookings');
        $bookRes = $slotbookingColl->findOne(array('slot_id' => $args['ent_slot_id']));
        if (count($bookRes) == 0) {
            $data = (object) array();
            $errMsgArr = $this->_getStatusMessage(132, 132);
        } else {
            $data['cid'] = $bookRes['customer']['id'];
            $data['fname'] = $bookRes['customer']['fname'];
            $data['lname'] = $bookRes['customer']['lname'];
            $data['pic'] = $bookRes['customer']['pic'];
            $data['email'] = $bookRes['customer']['email'];
            $data['mobile'] = $bookRes['customer']['mobile'];
            $data['address'] = $bookRes['address1'];
            $data['cat_name'] = $bookRes['cat_name'];
            $data['appt_date'] = $bookRes['appt_date'];
            $data['customer_notes'] = $bookRes['customer_notes'];
            $data['job_images'] = $bookRes['job_images'];
            $data['bid'] = $bookRes['bid'];
            $data['appt_lat'] = $bookRes['appt_lat'];
            $data['appt_long'] = $bookRes['appt_long'];
            $errMsgArr = $this->_getStatusMessage(128, 128);
        }

        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'],
            'errMsg' => $errMsgArr['errMsg'], 'data' => $data);
    }

    // Start Bank Account API
    function AddBankDetails($args) {
        if ($args['routing_number'] == '')
            return $this->_getStatusMessage(1, 'routing number');
        else if ($args['account_number'] == '')
            return $this->_getStatusMessage(1, 'account number');
        else if ($args['account_holder_name'] == '')
            return $this->_getStatusMessage(1, 'account holder name');

        $dateformante = explode('/', $args['dob']);
        $data['day'] = $dateformante[1];
        $data['month'] = $dateformante[0];
        $data['year'] = $dateformante[2];
        $accountholdername = explode(" ", $args['account_holder_name']);
        $data['first_name'] = $accountholdername[0];
        $data['last_name'] = $accountholdername[1];
        $data['personal_id_number'] = $args['personal_id'];
        $data['state'] = $args['state'];
        $data['postal_code'] = $args['postal_code'];
        $data['city'] = $args['city'];
        $data['line1'] = $args['address'];
        $IdProoF = $args['IdProof'];

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
        if (is_array($returned))
            return $returned;

        $stripe = new StripeModuleNew();
        $args['mas_id'] = (int) $this->User['entityId'];
        $mas_id = (int) $this->User['entityId'];
        $location = $this->mongo->selectCollection('location');
        $master = $location->findOne(array('user' => (int) $mas_id));

        $customer = $this->stripe->apiStripe('get_token', $args);

        if ($customer['error']) {
            $errorObj = $customer['error']['det'];
            return array('errNum' => '1000', 'errFlag' => '1', 'errMsg' => $errorObj->json_body['error']['message'], 'status' => "while updateing account Info", 'fullres' => $customer);
        }
        $banktoken = $customer['id'];
        if ($banktoken != '') {
            $stripeAcc = new StripeModuleNew();
            if (is_null($master['StripeAccId']) || $master['StripeAccId'] == "") {
                $StripeRes = $stripeAcc->apiStripe('CreateAccount', array());
                if ($StripeRes['error']) {
                    $errorObj = $res['error']['det'];
                    return array('errNum' => '1001', 'errFlag' => '1', 'errMsg' => $errorObj->json_body['error']['message'], 'fullres' => $StripeRes);
                }
                $accid = $StripeRes['id'];
                $dat = array('StripeAccId' => $accid);
                $location->update(array('user' => (int) $mas_id), array('$set' => $dat));
            } else {
                $accid = $master['StripeAccId'];
            }
            if ($accid == '')
                return array('errNum' => '1001', 'errFlag' => '1', 'errMsg' => 'your stripe account unavailable please register new account');
            $data['acc_id'] = $accid;
            $res = $stripe->apiStripe('updateAccountInfo', $data);
            if ($res['error']) {
                $errorObj = $res['error']['e'];
                return array('errNum' => '1001', 'errFlag' => '1', 'errMsg' => $errorObj->jsonBody['error']['message'], 'status' => "while updateing account Info", 'fullres' => $res);
            }

//        $documentfolder = 'http://138.197.94.9/Ziride/pics/';
            $verification = $stripe->apiStripe('IdentifyiVerification', array('acc_id' => $accid, 'IdProoF' => $IdProoF));
            if ($verification['flag'] == 1) {
                return array('errNum' => '1002', 'errFlag' => '1', 'errMsg' => $verification['msg'], 'status' => "while updateing account Info", 'fullres' => $verification);
            }

            $StripeResAcc = $stripe->apiStripe('CreateAnBankAccout', array('accountId' => $accid, 'externale_accId' => $banktoken));
            $location->update(array('user' => (int) $mas_id), array('$push' => array('accountDetail' => array('status' => 'active', 'tocken' => $StripeResAcc['id']))));

            return array('errNum' => '1003', 'errFlag' => '0', 'errMsg' => "Congrats You Have added account!", 'status' => "while updateing account Info", 'userData' => $this->getMasterBankData($args));
        } else {
            return array('errNum' => '1004', 'errFlag' => '1', 'errMsg' => "BankId is missing", 'status' => "while updateing account Info");
        }
    }

    function getMasterBankData($args) {

        if ($args['ent_sess_token'] == '')
            return $this->_getStatusMessage(1, 'session token is missing');
        else if ($args['ent_dev_id'] == '')
            return $this->_getStatusMessage(1, 'device Id is missing');

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
        if (is_array($returned))
            return $returned;

        $stripe = new StripeModuleNew();
        $location = $this->mongo->selectCollection('location');
        $userType = $location->findOne(array('user' => (int) $this->User['entityId']));
        $bank_arr = array();
        $bank_arr1 = array();
        $i = 0;
        foreach ($userType['accountDetail'] as $user) {
            $getResp = array('accountId' => $userType['StripeAccId'], 'BankAccountTocken' => $user['tocken']);
            $rep = $stripe->apiStripe('RetriveABankAccount', $getResp);

            $bank_arr[$i]['accountId'] = $userType['StripeAccId'];
            $bank_arr[$i]['bank_id'] = $rep['id'];
            $bank_arr[$i]['account_holder_name'] = $rep['account_holder_name'];
            $bank_arr[$i]['bank_name'] = $rep['bank_name'];
            $bank_arr[$i]['routing_number'] = $rep['routing_number'];
            $bank_arr[$i]['country'] = $rep['country'];
            $bank_arr[$i]['currency'] = $rep['currency'];
            $bank_arr[$i]['last4'] = $rep['last4'];
            $bank_arr[$i]['default_account'] = '0';
            if ($rep['metadata']['default_for_currency'] == 'true')
                $bank_arr[$i]['default_account'] = '1';
            $i++;
        }
        if (empty($bank_arr)) {
            return array('errNum' => '1005', 'errFlag' => '1', 'errMsg' => "Not found details", 'data' => $bank_arr);
        } else {
            return array('errNum' => '1006', 'errFlag' => '0', 'errMsg' => "Got details", 'data' => $bank_arr);
        }
    }

    function deleteMasterBank($args) {

        if ($args['bank_token'] == '')
            return $this->_getStatusMessage(1, 'Bank token is missing');

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
        if (is_array($returned))
            return $returned;

        $mas_id = (int) $this->User['entityId'];
        $bankId = $args['bank_token'];
        $stripe = new StripeModuleNew();
        $location = $this->mongo->selectCollection('location');
        $userType = $location->findOne(array('user' => (int) $mas_id));

        if ($bankId != "") {
            $res = $stripe->apiStripe('DeleteABankAccount', array(
                'accountId' => $userType['StripeAccId'],
                'externale_accId' => $bankId
            ));
            if ($res['deleted']) {
                $location->update(array('user' => (int) $mas_id), array('$pull' => array('accountDetail' => array('status' => 'active', 'tocken' => $bankId))));
                $array = array('errNum' => '1007', 'errFlag' => '0', 'errMsg' => "Card Deleted Successfully.", 'status' => "while updateing account Info", 'userData' => $this->getMasterBankData($args));
            } else {
                $errorObj = $res['error']['det'];
                $array = array('errNum' => '1008', 'errFlag' => '1', 'errMsg' => $errorObj->json_body['error']['message'], 'res' => $res);
            }
            return $array;
        }
        $array = array('errNum' => '1009', 'errFlag' => '1', 'errMsg' => "There were some problem with stripe.", 'res' => $res);
        return $array;
    }

    function BankAccountDefault($args) {

        if ($args['bank_token'] == '')
            return $this->_getStatusMessage(1, 'Bank token is missing');

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
        if (is_array($returned))
            return $returned;

        $mas_id = (int) $this->User['entityId'];
        $bankId = $args['bank_token'];
        $stripe = new StripeModuleNew();
        $getBankDat = $this->getMasterBankData($args);
        if ($getBankDat['errFlag'] == '1') {
            return $getBankDat;
        } else {
            foreach ($getBankDat['data'] as $dat) {
                if ($dat['default_account'] == '1') {
                    $res = $stripe->apiStripe('BankAccountDefault', array(
                        'accountId' => $dat['accountId'],
                        'externale_accId' => $dat['bank_id'],
                        'makedefault' => 'false'
                    ));
                }
            }
        }

        $location = $this->mongo->selectCollection('location');
        $userType = $location->findOne(array('user' => (int) $mas_id));
        $res = $stripe->apiStripe('BankAccountDefault', array(
            'accountId' => $userType['StripeAccId'],
            'externale_accId' => $bankId,
            'makedefault' => 'true'
        ));

        if ($res['error']) {
            $errorObj = $res['error']['det'];
            $array = array('errNum' => '1010', 'errFlag' => '1', 'errMsg' => $errorObj->json_body['error']['message'], 'res' => $res);
        } else {
            $array = array('errNum' => '1011', 'errFlag' => '0', 'errMsg' => 'Bank account is default', 'res' => $res);
        }
        return $array;
    }

    // END Bank Account API
    // Start Bank Account API
    function AddBankDetailsFromAdmin($args) {
        if ($args['routing_number'] == '')
            return $this->_getStatusMessage(1, 'routing number');
        else if ($args['account_number'] == '')
            return $this->_getStatusMessage(1, 'account number');
        else if ($args['account_holder_name'] == '')
            return $this->_getStatusMessage(1, 'account holder name');
        else if ($args['ent_pro_id'] == '')
            return $this->_getStatusMessage(1, 'pro id missing');

        $args['account_holder_name'] = $args['account_holder_name'] . " " . $args['ent_last_name'];

        $dateformante = explode('/', $args['dob']);
        $data['day'] = $dateformante[1];
        $data['month'] = $dateformante[0];
        $data['year'] = $dateformante[2];
        $accountholdername = explode(" ", $args['account_holder_name']);
        $data['first_name'] = $accountholdername[0];
        $data['last_name'] = $accountholdername[1];
        $data['personal_id_number'] = $args['personal_id'];
        $data['state'] = $args['state'];
        $data['postal_code'] = $args['postal_code'];
        $data['city'] = $args['city'];
        $data['line1'] = $args['address'];
        $IdProoF = $args['IdProof'];

        $stripe = new StripeModuleNew();
        $args['mas_id'] = (int) $args['ent_pro_id'];
        $mas_id = (int) $args['ent_pro_id'];
        $location = $this->mongo->selectCollection('location');
        $master = $location->findOne(array('user' => (int) $mas_id));

        $customer = $this->stripe->apiStripe('get_token', $args);

        if ($customer['error']) {
            $errorObj = $customer['error']['det'];
            return array('errNum' => '1000', 'errFlag' => '1', 'errMsg' => $errorObj->json_body['error']['message'], 'status' => "while updateing account Info", 'fullres' => $customer);
        }
        $banktoken = $customer['id'];
        if ($banktoken != '') {
            $stripeAcc = new StripeModuleNew();
            if (is_null($master['StripeAccId']) || $master['StripeAccId'] == "") {
                $StripeRes = $stripeAcc->apiStripe('CreateAccount', array());
                if ($StripeRes['error']) {
                    $errorObj = $res['error']['det'];
                    return array('errNum' => '1001', 'errFlag' => '1', 'errMsg' => $errorObj->json_body['error']['message'], 'fullres' => $StripeRes);
                }
                $accid = $StripeRes['id'];
                $dat = array('StripeAccId' => $accid);
                $location->update(array('user' => (int) $mas_id), array('$set' => $dat));
            } else {
                $accid = $master['StripeAccId'];
            }

            if ($accid == '')
                return array('errNum' => '1001', 'errFlag' => '1', 'errMsg' => 'your stripe account unavailable please register new account');
            $data['acc_id'] = $accid;
            $res = $stripe->apiStripe('updateAccountInfo', $data);
            if ($res['error']) {
                $errorObj = $res['error']['e'];
                return array('errNum' => '1001', 'errFlag' => '1', 'errMsg' => $errorObj->jsonBody['error']['message'], 'status' => "while updateing account Info", 'fullres' => $res);
            }

            $verification = $stripe->apiStripe('IdentifyiVerification', array('acc_id' => $accid, 'IdProoF' => $IdProoF));
            if ($verification['flag'] == 1) {
                return array('errNum' => '1002', 'errFlag' => '1', 'errMsg' => $verification['msg'], 'status' => "while updateing account Info", 'fullres' => $verification);
            }

            $StripeResAcc = $stripe->apiStripe('CreateAnBankAccout', array('accountId' => $accid, 'externale_accId' => $banktoken));
            $location->update(array('user' => (int) $mas_id), array('$push' => array('accountDetail' => array('status' => 'active', 'tocken' => $StripeResAcc['id']))));

            return array('errNum' => '1003', 'errFlag' => '0', 'errMsg' => "Congrats You Have added account!", 'status' => "while updateing account Info", 'userData' => $this->getMasterBankData($args));
        } else {
            return array('errNum' => '1004', 'errFlag' => '1', 'errMsg' => "BankId is missing", 'status' => "while updateing account Info");
        }
    }

    function getMasterBankDataFromAdmin($args) {

        if ($args['ent_pro_id'] == '')
            return $this->_getStatusMessage(1, 'Pro id  is missing');

        $stripe = new StripeModuleNew();
        $location = $this->mongo->selectCollection('location');
        $userType = $location->findOne(array('user' => (int) $args['ent_pro_id']));
        $bank_arr = array();
        $bank_arr1 = array();
        $i = 0;
        foreach ($userType['accountDetail'] as $user) {
            $getResp = array('accountId' => $userType['StripeAccId'], 'BankAccountTocken' => $user['tocken']);
            $rep = $stripe->apiStripe('RetriveABankAccount', $getResp);

            $bank_arr[$i]['accountId'] = $userType['StripeAccId'];
            $bank_arr[$i]['bank_id'] = $rep['id'];
            $bank_arr[$i]['account_holder_name'] = $rep['account_holder_name'];
            $bank_arr[$i]['bank_name'] = $rep['bank_name'];
            $bank_arr[$i]['routing_number'] = $rep['routing_number'];
            $bank_arr[$i]['country'] = $rep['country'];
            $bank_arr[$i]['currency'] = $rep['currency'];
            $bank_arr[$i]['last4'] = $rep['last4'];
            $bank_arr[$i]['default_account'] = '0';
            if ($rep['metadata']['default_for_currency'] == 'true')
                $bank_arr[$i]['default_account'] = '1';
            $i++;
        }
        if (empty($bank_arr)) {
            return array('errNum' => '1005', 'errFlag' => '1', 'errMsg' => "Not found details", 'data' => $bank_arr);
        } else {
            return array('errNum' => '1006', 'errFlag' => '0', 'errMsg' => "Got details", 'data' => $bank_arr);
        }
    }

    function deleteMasterBankFromAdmin($args) {

        if ($args['bank_token'] == '')
            return $this->_getStatusMessage(1, 'Bank token is missing');
        else if ($args['ent_pro_id'])
            return $this->_getStatusMessage(1, 'Pro id Missing');

        $mas_id = (int) $args['ent_pro_id'];
        $bankId = $args['bank_token'];
        $stripe = new StripeModuleNew();
        $location = $this->mongo->selectCollection('location');
        $userType = $location->findOne(array('user' => (int) $mas_id));

        if ($bankId != "") {
            $res = $stripe->apiStripe('DeleteABankAccount', array(
                'accountId' => $userType['StripeAccId'],
                'externale_accId' => $bankId
            ));
            if ($res['deleted']) {
                $location->update(array('user' => (int) $mas_id), array('$pull' => array('accountDetail' => array('status' => 'active', 'tocken' => $bankId))));
                $array = array('errNum' => '1007', 'errFlag' => '0', 'errMsg' => "Card Deleted Successfully.", 'status' => "while updateing account Info", 'userData' => $this->getMasterBankData($args));
            } else {
                $errorObj = $res['error']['det'];
                $array = array('errNum' => '1008', 'errFlag' => '1', 'errMsg' => $errorObj->json_body['error']['message'], 'res' => $res);
            }
            return $array;
        }
        $array = array('errNum' => '1009', 'errFlag' => '1', 'errMsg' => "There were some problem with stripe.", 'res' => $res);
        return $array;
    }

    function BankAccountDefaultFromAdmin($args) {

        if ($args['bank_token'] == '')
            return $this->_getStatusMessage(1, 'Bank token is missing');
        else if ($args['ent_pro_id'] == '')
            return $this->_getStatusMessage(1, 'Pro id missing');

        $mas_id = (int) $args['ent_pro_id'];
        $bankId = $args['bank_token'];
        $stripe = new StripeModuleNew();
        $getBankDat = $this->getMasterBankDataFromAdmin($args);
        if ($getBankDat['errFlag'] == '1') {
            return $getBankDat;
        } else {
            foreach ($getBankDat['data'] as $dat) {
                if ($dat['default_account'] == '1') {
                    $res = $stripe->apiStripe('BankAccountDefault', array(
                        'accountId' => $dat['accountId'],
                        'externale_accId' => $dat['bank_id'],
                        'makedefault' => 'false'
                    ));
                }
            }
        }

        $location = $this->mongo->selectCollection('location');
        $userType = $location->findOne(array('user' => (int) $mas_id));
        $res = $stripe->apiStripe('BankAccountDefault', array(
            'accountId' => $userType['StripeAccId'],
            'externale_accId' => $bankId,
            'makedefault' => 'true'
        ));

        if ($res['error']) {
            $errorObj = $res['error']['det'];
            $array = array('errNum' => '1010', 'errFlag' => '1', 'errMsg' => $errorObj->json_body['error']['message'], 'res' => $res);
        } else {
            $array = array('errNum' => '1011', 'errFlag' => '0', 'errMsg' => 'Bank account is default', 'res' => $res);
        }
        return $array;
    }

    //end admin  bank API








    protected function GetCategoryDetail($args) {
        if ($args['ent_cat_id'] == '' || $args['ent_pro_id'] == "")
            return $this->_getStatusMessage(1, 1);

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
        if (is_array($returned))
            return $returned;

//        $args['ent_pro_id'] = 23;
//        $args['ent_cat_id'] = '5825da3be127c99f6e9c6e73';

        $catColl = $this->mongo->selectCollection('Category');
        $location = $this->mongo->selectCollection('location');
        $locationRes = $location->findOne(array('user' => (int) $args['ent_pro_id']));

        if (strlen($args['ent_cat_id']) == 24 && is_string($args['ent_cat_id']) && ctype_xdigit($args['ent_cat_id'])) {
            $price_set_by = 0;
            $bookRes = $catColl->findOne(array('_id' => new MongoId($args['ent_cat_id'])));

            foreach ($locationRes['catlist'] as $userCat) {
                if ($userCat['cid'] == $args['ent_cat_id']) {
                    $price_set_by = $userCat['amount'];
                }
            }
            if (count($bookRes) == 0) {
                $data = (object) array();
                $errMsgArr = $this->_getStatusMessage(134, 134);
            } else {
                $data['cat_name'] = $bookRes['cat_name'];
                $data['cat_desc'] = $bookRes['cat_desc'];
                $data['fee_type'] = $bookRes['fee_type'];
                $data['visit_fees'] = $bookRes['visit_fees'];
                $data['price_set_by'] = $bookRes['price_set_by'];
                $data['price_min'] = $price_set_by;
                $data['radius'] = $locationRes['radius'];
                $errMsgArr = $this->_getStatusMessage(133, 133);
            }
        } else {
            $data = (object) array();
            $errMsgArr = $this->_getStatusMessage(134, 134);
        }
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'],
            'errMsg' => $errMsgArr['errMsg'], 'data' => $data, 'catdata' => $data);
    }

    protected function updateApptStatusFromAdmin($args) {

        if ($args['ent_pro_id'] == '' || $args['ent_amount'] == '' || $args['ent_bid'] == '' || $args['ent_appnt_dt'] == '' || $args['ent_response'] == '' || $args['ent_pat_email'] == '' || $args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, 1);

        $this->curr_date_time = urldecode($args['ent_date_time']);
        $args['ent_appnt_dt'] = urldecode($args['ent_appnt_dt']);

        $patData = $this->_getEntityDet($args['ent_pat_email'], '2');
        $bookingColl = $this->mongo->selectCollection('bookings');
        $apptDet = $bookingColl->findOne(array('bid' => $args['ent_bid']));

        if ($apptDet['status'] == '3')
            return $this->_getStatusMessage(88, 3);

        if ($apptDet['status'] == '4' || $apptDet['status'] == '9')
            return $this->_getStatusMessage(41, 3);

        if ($apptDet['status'] == '10')
            return $this->_getStatusMessage(89, 77);

        if ($apptDet['status'] == '8')
            return $this->_getStatusMessage(77, 77);

        $bookings = $this->mongo->selectCollection('bookings');

        $message = 'Job completed Final Invoice Sent!';
        $noteType = '7';
        $errNum = 61;
        $aplPushContent = array('alert' => $message, 'sound' => 'default', 'bid' => $apptDet['bid'], 'st' => $noteType, 'sname' => $this->User['firstName'] . ' ' . $this->User['lastName'] . ', ' . $this->User['degree'], 'd' => $args['ent_appnt_dt'], 'email' => $this->User['email'], 'r' => number_format($apptDet['doc_rating'], 1, '.', ''));
        $andrPushContent = array("payload" => $message, 'bid' => $apptDet['bid'], 'st' => $noteType, 'sname' => $this->User['firstName'] . ' ' . $this->User['lastName'] . ' ' . $this->User['degree'], 'dt' => $args['ent_appnt_dt'], 'email' => $this->User['email'], 'r' => number_format($apptDet['doc_rating'], 1, '.', ''));

        $apptDet = $bookings->findOne(array('bid' => (string) $args['ent_bid']));
        if ($apptDet['payment_type'] == 2) {
            $chargeCustomerArr = array('stripe_id' => $patData['stripe_id'], 'amount' => (int) ((float) $args['ent_amount'] * 100), 'currency' => 'USD', 'description' => 'From ' . $patData['email']);
            $trStripe = $this->stripe->apiStripe('chargeCard', $chargeCustomerArr);
            $taranseferId = "";
            if ($trStripe['error']) {
                $charge = array('errNum' => 16, 'errFlag' => 1, 'errMsg' => $trStripe['error']['message'], 'test' => 1);
            } else {
                $taranseferId = $trStripe['id'];
            }
        } else {
            $taranseferId = "cash_000_" . $apptDet['bid'];
        }

        $comp = array('ctime' => $this->curr_date_time,
            'sign_url' => "", 'misc_fees' => 0,
            'total_pro' => $args['ent_amount'], 'pro_disc' => 0,
            'mat_fees' => 0, 'complete_note' => "",
            'time_fees' => 0, 'sub_total_cust' => 0);

        $bookings->update(array('bid' => $args['ent_bid']), array('$set' => array('comp' => $comp, 'job_com_server' => time(), 'job_com_device' => strtotime($args['ent_date_time']))));

        // make provider online again
        $locationColl = $this->mongo->selectCollection('location');
        $locationColl->update(array('user' => (int) $args['ent_pro_id']), array('$set' => array('booked' => 0)));

        $bookings->update(array('bid' => $args['ent_bid']), array('$set' => array('status' => (int) $args['ent_response'])));

        $apptDet['appt_date'] = date('Y-m-d H:i:s', strtotime($apptDet['appt_date']) + (int) $this->User['offset']);
        $apptDet['created_dt'] = date('Y-m-d H:i:s', strtotime($apptDet['created_dt']) + (int) $this->User['offset']);

        $location = $this->mongo->selectCollection('location');
        $set = array('$set' => array('status' => 3, 'apptStatus' => 0, 'booked' => 0));

        $location->update(array('user' => (int) $args['ent_pro_id']), $set);

        $updateInvoice = $this->__GenerateInvoiceDetails($args['ent_bid'], $taranseferId);
        if (!isset($updateInvoice['invoiceData']))
            return $this->_getStatusMessage(56, $updateInvoice);
        else {
            $bookings->update(array("bid" => (string) $args['ent_bid']), array('$set' => array('invoiceData' => $updateInvoice['invoiceData'])));
            $apptDet = $bookingColl->findOne(array('bid' => (string) $args['ent_bid']));
            $mail = new sendAMail(APP_SERVER_HOST);
            $push['emailRes'] = $mail->sendInvoice($apptDet);
        }

        if ($apptDet['booking_type'] == '2') {
            $slot = $this->mongo->selectCollection('slotbooking');
            $dt = explode(' ', $args['ent_appnt_dt']);
            $date = explode('-', $dt[0]);
            $days = explode(':', $dt[1]);
            $start = mktime($days[0], $days[1], $days[2], $date[1], $date[2], $date[0]);
            if ($apptDet['slot_id'] != '')
                $check = array("_id" => new MongoId($apptDet['slot_id']));
            else
                $check = array('start' => $start, 'doc_id' => (int) $args['ent_pro_id']);
            $slotUpdate = $slot->update($check, array('$set' => array('booked' => 4)));

            $location = $this->mongo->selectCollection('location');
            $userData = $location->findOne(array('user' => (int) $args['ent_pro_id']));
            $pubnubContent = array('a' => 16, 'bid' => $apptDet['bid'], 'slot' => $apptDet['slot_id']);
        }

        $push = $this->_sendPush($args['ent_pro_id'], array($patData['patient_id']), $message, $noteType, $this->User['email'], $this->curr_date_time, '2', $aplPushContent, $andrPushContent);

        $testingc = $this->mongo->selectCollection('testing');
        $testingc->insert(array('testtingp' => $push));

        $pubnubContent = array('a' => 14, 'bid' => $apptDet['bid'], 's' => $args['ent_response'], 'm' => $message);
        $out = array('push' => $push, 'mail' => $invoMail, 'updateStatus' => $update, 'slot' => $slotUpdate);

        $argstopass = array('bid' => $args['ent_bid'], 'proid' => $args['ent_pro_id'], 'bstatus' => $args['ent_response']);
        $this->sendDatatoSocket($argstopass, "respondToAppointment");
        $message = "Admin has completed the appointment dated " . $apptDet['appt_date'];
        $aplPushContent1 = array('alert' => $message, 'nt' => 3, 'btype' => '4', 'sname' => $this->User['firstName'], 'dt' => $args['ent_appnt_dt'], 'e' => $this->User['email'], 'sound' => 'default');
        $aplPushContent = array('alert' => $message, 'payload' => $aplPushContent1);
        $andrPushContent = array("payload" => $message, 'action' => '3', 'sname' => $this->User['firstName'], 'dt' => $args['ent_appnt_dt'], 'e' => $this->User['email']);
        $push = $this->_sendPush($args['ent_pro_id'], array($args['ent_pro_id']), $message, '6', $this->User['firstName'], $this->curr_date_time, '1', $aplPushContent, $andrPushContent);
        return array('errNum' => '200', 'errFlag' => '1',
            'errMsg' => 'success');
    }

    protected function abortAppointmentFromSuperadmin($args) {

        if ($args['ent_bid'] == '' || $args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, $args);

        $this->curr_date_time = urldecode($args['ent_date_time']);


        $args['ent_appnt_dt'] = urldecode($args['ent_appnt_dt']);

        $bookingColl = $this->mongo->selectCollection('bookings');
        $apptDet = $bookingColl->findOne(array('bid' => (string) $args['ent_bid']));

        $location = $this->mongo->selectCollection('location');
        if ($apptDet['booking_type'] == '1') {
            $location->update(array('user' => (int) $args['ent_pro_id']), array('$set' => array('booked' => 0)));
        }

        if (!$apptDet)
            return $this->_getStatusMessage(62, 32);

        if ($apptDet['status'] == '3')
            return $this->_getStatusMessage(44, 44);

        $docData = $this->_getEntityDet($args['ent_email'], '2');


        $status_msg = "Cancelled By Provider " . $args['ent_reason'];
        $bookingColl->update(array('bid' => (string) $args['ent_bid']), array('$set' => array('status' => 10,
                'last_modified_dt' => $this->curr_date_time,
                'provider_id' => "0",
                'cancel_reason' => $args['ent_reason'])));

        $bookingColl->update(array('bid' => (string) $args['ent_bid'],
            'dispatched.pid' => (string) $args['ent_pro_id']), array('$set' => array('dispatched.$.res' => 10,
                'dispatched.$.status_msg' => $status_msg)));

        $message = "Your appointment at " . date('jS F', strtotime($apptDet['appt_date'])) . " has been cancelled";

        if ($apptDet['booking_type'] == '2') {

            $slot = $this->mongo->selectCollection('slotbooking');
            $dt = explode(' ', $apptDet['appt_date']);
            $date = explode('-', $dt[0]);
            $days = explode(':', $dt[1]);
            $start = mktime($days[0], $days[1], $days[2], $date[1], $date[2], $date[0]);

            $check = ($apptDet['slot_id'] != '') ? array('_id' => new MongoId($apptDet['slot_id'])) : array('start' => $start, 'doc_id' => (int) $args['ent_pro_id']);

            $update = $slot->update($check, array('$set' => array('booked' => 1, 'status' => 3)));
        }

        $aplPushContent = array('alert' => $message, 'st' => '10', 'sound' => 'default', 'r' => $args['ent_reason'], 'dt' => $args['ent_appnt_dt'], 'bid' => $apptDet['bid']);
        $andrPushContent = array('payload' => $message, 'st' => '10', 'sname' => $this->User['firstName'], 'dt' => $apptDet['appt_date'], 'email' => $this->User['email'], 'bid' => $apptDet['bid']);
        $pushNum['push'] = $this->_sendPush($args['ent_pro_id'], array($apptDet['customer']['id']), $message, '10', $this->User['firstName'], $this->curr_date_time, '2', $aplPushContent, $andrPushContent);

        $mail = new sendAMail(APP_SERVER_HOST);
        $apptDet = $bookingColl->findOne(array('bid' => (string) $args['ent_bid']));
        $mailArr[] = $mail->CancelledByPro_MailToCust($apptDet);
        $mailArr[] = $mail->CancelledByPro_MailToPro($apptDet);

        // make provider online again
        $location = $this->mongo->selectCollection('location');
        $location->update(array('user' => (int) $args['ent_pro_id']), array('$set' => array('booked' => 0)));

        if ($apptDet['booking_type'] == '2') {
            $pubnubContent = array('a' => 15, 'bid' => $apptDet['bid'], 'slot' => $apptDet['slot_id']);
        }

        $argstopass = array('bid' => $args['ent_bid'], 'proid' => $args['ent_pro_id'], 'bstatus' => 10);
        $this->sendDatatoSocket($argstopass, "respondToAppointment");
        $argstopass = array('bid' => $args['ent_bid']);
        $this->sendDatatoSocket($argstopass, "cancelByCustomer");
        $message = "Admin has cancelled the appointment dated " . $apptDet['appt_date'];
        $aplPushContent1 = array('alert' => $message, 'nt' => 3, 'btype' => '4', 'sname' => $this->User['firstName'], 'dt' => $args['ent_appnt_dt'], 'e' => $this->User['email'], 'sound' => 'default');
        $aplPushContent = array('alert' => $message, 'payload' => $aplPushContent1);
        $andrPushContent = array("payload" => $message, 'action' => '3', 'sname' => $this->User['firstName'], 'dt' => $args['ent_appnt_dt'], 'e' => $this->User['email']);
        $push = $this->_sendPush($args['ent_pro_id'], array($args['ent_pro_id']), $message, '6', $this->User['firstName'], $this->curr_date_time, '1', $aplPushContent, $andrPushContent);

        return $this->_getStatusMessage(42, 3);
    }

    protected function UpdateCategoryDetail($args) {
        if ($args['ent_cat_id'] == '' || $args['ent_pro_id'] == "" || $args['ent_price_min'] == "" || $args['ent_radius'] == "")
            return $this->_getStatusMessage(1, 1);

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
        if (is_array($returned))
            return $returned;
//        $args['ent_pro_id'] = 

        $userColl = $this->mongo->selectCollection('location');

        $userColl->update(array('user' => (int) $args['ent_pro_id'],
            'catlist.cid' => $args['ent_cat_id']), array('$set' => array(
                'radius' => (int) $args['ent_radius'], 'catlist.$.amount' => (string) $args['ent_price_min']
            ))
        );
        $errMsgArr = $this->_getStatusMessage(135, 135);

        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'],
            'errMsg' => $errMsgArr['errMsg']);
    }

    // Maintain Schedule Booking Remider like Remove and add schedule Booking
    // upadate Reminder By Provider And Customer
    protected function UpdateBookingReminder($args) {
        if ($args['ent_bid'] == '' || $args['ent_user_type'] == "" || $args['ent_rem_id'] == "")
            return $this->_getStatusMessage(1, 1);
        // check  user session for every  API Request
        if ($args['ent_user_type'] == 1) {
            $type = '1';
        } else
            $type = '2';
        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], $type);
        if (is_array($returned)) // if session not valid then return from here only
            return $returned;
        // Select Mongodb Booking Collection
        $bookingColl = $this->mongo->selectCollection('bookings');
        //Check  User Type Like  Provider or Customer. and update reminder base on user type
        if ($args['ent_user_type'] == 1) {
            $bookingColl->update(array('bid' => (string) $args['ent_bid']), array('$set' => array(
                    'pro_reminder' => (String) $args['ent_rem_id'])));
        } else {
            $bookingColl->update(array('bid' => (string) $args['ent_bid']), array('$set' => array(
                    'cust_reminder' => (String) $args['ent_rem_id'])));
        }
        //Get Message For responce
        $errMsgArr = $this->_getStatusMessage(137, 137);
        // return  respoce Message here.
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'],
            'errMsg' => $errMsgArr['errMsg']);
    }

    // End Api Here

    protected function GetAllSlot($args) {

        if ($args['ent_pro_id'] == '')
            return $this->_getStatusMessage(1, 1);

//        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], '1');
        if (is_array($returned))
            return $returned;


        $yearmonth = $args['ent_month'];
        $args['ent_month'] = urldecode($args['ent_month']);
        $args['ent_month'] = $args['ent_month'] . '-01';
        $endDate = date('Y-m-d', strtotime('+1 month', strtotime($args['ent_month'])));

        $slotbooking = $this->mongo->selectCollection('slotbooking');
        $cond = new MongoRegex("/^" . $yearmonth . "/");
        $proid = (int) $args['ent_pro_id'];
        $cursor = $slotbooking->find(array('start_dt' => $cond, 'doc_id' => (int) $proid));


        $adata = array();

        foreach ($cursor as $cur) {

            $cur['id'] = (string) $cur['_id'];
            unset($cur['_id']);
            unset($cur['doc_id']);
            $aptdate = date('Y-m-d', strtotime($cur['start_dt']));

            $slotStartTime = explode(" ", $cur['start_dt']);
            $slotEndTime = explode(" ", $cur['end_dt']);

            $adata[$aptdate][] = array(
                'id' => (string) $cur['id'],
                'slot_price' => $cur['slot_price'],
                'locId' => $cur['locId'], 'radius' => $cur['radius'],
                'start_dt' => $slotStartTime[1], 'end_dt' => $slotEndTime[1],
                'start' => $cur['start'], 'end' => $cur['end'], 'booked' => $cur['booked']);

            array_push($adata, $cur);
        }
        $date = $args['ent_month'];
        $sortedSlots = array();
        while ($date < $endDate) {
            $empty_arr = array();
            if (is_array($adata[$date])) {
                $sortedSlots[] = array('date' => $date, 'slot' => $adata[$date]);
            } else {
                $sortedSlots[] = array('date' => $date, 'slot' => $empty_arr);
            }
            $date = date('Y-m-d', strtotime('+1 day', strtotime($date)));
        }

        $errMsgArr = $this->_getStatusMessage(128, 128);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'],
            'errMsg' => $errMsgArr['errMsg'], 'slots' => $sortedSlots);
    }

    /*
     * Method name: support 
     * Desc: Edit profile of any users
     * Input: Request data
     * Output:  Complete profile details if available, else error message
     */

    protected function support($args) {
        if ($args['ent_lan'] == '')
            return $this->_getStatusMessage(1, 'language ');

        $helpText = $this->MongoClass->get('support_txt');
        $HelpedTextArray = array();
//        $args['ent_lan'] = 0;
        $HelpedTextArray = $childes = array();

        foreach ($helpText['data'] as $res) {
            if (isset($res['name'][$args['ent_lan']])) {
                if ($res['has_scat']) {
                    foreach ($res['sub_cat'] as $subcat) {
                        if (isset($subcat['name'][$args['ent_lan']])) {
                            $childes[] = array(
                                'tag' => $subcat['name'][$args['ent_lan']],
                                'link' => APP_SERVER_HOST . 'helpText/supporttext.php?cat=' . ((string) $res['_id']) . '&lan=' . $args['ent_lan'] . '&scat=' . (string) $subcat['scat_id']
                            );
                        }
                    }
                    $HelpedTextArray [] = array(
                        'tag' => $res['name'][$args['ent_lan']],
                        'childs' => $childes
                    );
                } else {
                    $HelpedTextArray [] = array(
                        'tag' => $res['name'][$args['ent_lan']],
                        'link' => APP_SERVER_HOST . 'helpText/supporttext.php?cat=' . ((string) $res['_id']) . '&lan=' . $args['ent_lan'],
                        'childs' => array()
                    );
                }
            }
        }

        $errMsgArr = $this->_getStatusMessage(33, 2);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'], 'support' => $HelpedTextArray);
    }

    /*
     * 
     * get cancellation reason
     */

    public function getCancellationReson($args) {
        if ($args['ent_lan'] == '')
            return $this->_getStatusMessage(1, 'language ');
        else if ($args['ent_user_type'] == '')
            return $this->_getStatusMessage(1, 'User type');

//        $getLanguate = $this->MongoClass->get('can_reason', array('res_for' => $args['ent_user_type'] == 2 ? 'Passenger' : "Driver"));
//        $getLanguate = $this->MongoClass->get('can_reason', array('res_for' => 'Passenger'));
        $can_reasonColl = $this->mongo->selectCollection('can_reason');
        $lanCursor = $can_reasonColl->find(array("res_for" => $args['ent_user_type'] == 2 ? "Passenger" : "Driver"));
        $data = array();
        foreach ($lanCursor as $lan) {
            if ($lan['reasons'][$args['ent_lan']] != "")
                $data[] = $lan['reasons'][$args['ent_lan']];
        }
//        $data = array();
//        foreach ($getLanguate['data'] as $lan) {
//            $data[] = $lan['reasons'][$args['ent_lan']];
//        }
        $errMsgArr = $this->_getStatusMessage(21, 78);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'],
            'errMsg' => $errMsgArr['errMsg'], 'data' => $data
        );
    }

    /*
     * 
     * help text  
     */

    protected function helptext($args) {

        if ($args['ent_lan'] == '')
            return $this->_getStatusMessage(1, 'Date time');
        else if ($args['ent_sid'] == '') {
            return $this->_getStatusMessage(1, 'Date time');
        }
//        
        $helpText = $this->MongoClass->get('hlp_txt');

        $HelpedTextArray = array();
        $args['ent_lan'] = 0;
        $HelpedTextArray = $childes = array();

        foreach ($helpText['data'] as $res) {

            if ($res['has_scat']) {

                foreach ($res['sub_cat'] as $subcat) {
                    $childes[] = array(
                        'tag' => $subcat['name'][$args['ent_lan']],
                        'link' => APP_SERVER_HOST . 'helpText/helpText.php?cat=' . ((string) $res['_id']) . '&lan=' . $args['ent_lan'] . '&sid=' . $args['ent_sid'] . '&scat=' . (string) $subcat['scat_id']
                    );
                }
                $HelpedTextArray [] = array(
                    'tag' => $res['name'][$args['ent_lan']],
                    'childs' => $childes
                );
            } else {
                $HelpedTextArray [] = array(
                    'tag' => $res['name'][$args['ent_lan']],
                    'link' => APP_SERVER_HOST . 'helpText/helpText.php?cat=' . ((string) $res['_id']) . '&lan=' . $args['ent_lan'] . '&sid=' . $args['ent_sid'],
                    'childs' => array()
                );
            }
        }

        $errMsgArr = $this->_getStatusMessage(33, 2);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'], 'HelpedTextArray' => $HelpedTextArray);
    }

    protected function forgotPassword($args) {

        if ($args['ent_email'] == '' || $args['ent_user_type'] == '')
            return $this->_getStatusMessage(1, 1);

        if ($args['ent_user_type'] == '1' || $args['ent_user_type'] == 1) {
            $table = 'doctor';
            $uid = 'doc_id';
        } else if ($args['ent_user_type'] == '2' || $args['ent_user_type'] == 2) {
            $table = 'patient';
            $uid = 'patient_id';
        } else {
            return $this->_getStatusMessage(1, 1);
        }

        $selectUserQry = "select first_name,email, password, $uid from $table where email = '" . $args['ent_email'] . "'";
        $selectUserRes = mysql_query($selectUserQry, $this->db->conn);

        if ($args['ent_user_type'] == '1' || $args['ent_user_type'] == 1) {
            $selectUserStatusQry = "select status from $table where email = '" . $args['ent_email'] . "'";
            $selectUserStatusRes = mysql_query($selectUserStatusQry, $this->db->conn);
            $userDataStatus = mysql_fetch_assoc($selectUserStatusRes);
            if ($userDataStatus['status'] == 1) {
                return $this->_getStatusMessage(122, $selectUserStatusQry);
            }
            if ($userDataStatus['status'] == 4) {
                return $this->_getStatusMessage(11, $selectUserStatusQry);
            }
        }

        if (mysql_num_rows($selectUserRes) <= 0)
            return $this->_getStatusMessage(66, $selectUserQry);

        $userData = mysql_fetch_assoc($selectUserRes);

        $randData = $this->_generateRandomString(20) . '_' . $args['ent_user_type'];

        $mail = new sendAMail(APP_SERVER_HOST);
        $resetRes = $mail->forgotPassword($userData, $randData);

        if ($resetRes['flag'] == 0) {
            $updateResetDataQry = "update $table set resetData = '" . $randData . "', resetFlag = 1 where email = '" . $args['ent_email'] . "'";
            mysql_query($updateResetDataQry, $this->db->conn);
            //$resetRes['update'] = $updateResetDataQry;
            return $this->_getStatusMessage(67, $resetRes);
        } else {
            return $this->_getStatusMessage(68, $resetRes);
        }
    }

    protected function ForgotPasswordWithOtp($args) {
        if ($args['ent_date_time'] == '')
            return $this->_getStatusMessage(1, 'Date time');
        else if ($args['ent_mobile'] == '') {
            return $this->_getStatusMessage(1, 'mobile');
        }

        if ($args['ent_user_type'] == 2)
            $query = "select status , phone as MobileNo,country_code from patient where  phone = '" . $args['ent_mobile'] . "' limit 1";
        else if ($args['ent_user_type'] == 1) {
            $query = "select status, mobile as MobileNo,country_code from doctor where mobile = '" . $args['ent_mobile'] . "' limit 1";
        }

        $searchRes = mysql_query($query, $this->db->conn);

        if (mysql_num_rows($searchRes) <= 0)
            return $this->_getStatusMessage(136, $query);

        $data = mysql_fetch_assoc($searchRes);
        if (mysql_num_rows($searchRes) <= 0) {
            return $this->_getStatusMessage(20, $query);
        }

        if ($args['ent_user_type'] == 1) {
            if ($data['status'] == 1) {
                return $this->_getStatusMessage(122, $query);
            }
            if ($data['status'] == 4) {
                return $this->_getStatusMessage(11, $query);
            }
        }
        if ($args['ent_user_type'] == 2) {
            if ($data['status'] == 1) {
                return $this->_getStatusMessage(11, $query);
            }
        }

        $rand = $args['ent_rand'] = rand(1000, 9999);
//        $rand = 1111;
        $args['ent_mobile'] = $data['country_code'] . $data['MobileNo'];
        $resutl = $this->mobileVerification($args, 2, APP_NAME . ", Reset password code is : " . $args['ent_rand']);

        $verification = $this->mongo->selectCollection('verification');
        if (is_array($verification->findOne(array('mobile' => $args['ent_mobile']))))
            $verification->update(array('mobile' => $args['ent_mobile']), array('$set' => array('code' => (int) $rand, 'ts' => time())));
        else
            $verification->insert(array('mobile' => $args['ent_mobile'], 'code' => (int) $rand, 'ts' => time()));

        $errMsgArr = $this->_getStatusMessage(99, 5);
        return array('errNum' => $errMsgArr['errNum'], 'errFlag' => $errMsgArr['errFlag'], 'errMsg' => $errMsgArr['errMsg'], 'code' => $rand, 'query' => $query, 'args' => $args);
    }

    protected function updatePasswordForUser($args) {
        if ($args['ent_mobile'] == '') {
            return $this->_getStatusMessage(1, 'mobile');
        } else if ($args['ent_pass'] == '') {
            return $this->_getStatusMessage(1, 'password ');
        }

        if ($args['ent_user_type'] == 2) {
            $query = "select * from patient where   phone = '" . $args['ent_mobile'] . "' ";
            $Updatequery = "update patient set  password = md5('" . $args['ent_pass'] . "') where phone = '" . $args['ent_mobile'] . "' ";
        } else if ($args['ent_user_type'] == 1) {
            $query = "select * from doctor where  mobile = '" . $args['ent_mobile'] . "' ";
            $Updatequery = "update doctor set  password = md5('" . $args['ent_pass'] . "') where mobile = '" . $args['ent_mobile'] . "' ";
        }
        $searchRes = mysql_query($query, $this->db->conn);

        if (mysql_num_rows($searchRes) <= 0)
            return $this->_getStatusMessage(8);

        mysql_query($Updatequery, $this->db->conn);

        $userData = mysql_fetch_assoc($searchRes);
        $mail = new sendAMail(APP_SERVER_HOST);
        $resetRes = $mail->passwordChanged($userData);

        return $this->_getStatusMessage(131);
    }

    /*
     * Method name: logout
     * Desc: Edit profile of any users
     * Input: Request data
     * Output:  Complete profile details if available, else error message
     */

    protected function logout($args) {

        if ($args['ent_user_type'] == '' || $args['ent_date_time'] == '') {
            return $this->_getStatusMessage(1, 15);
        }

        $this->curr_date_time = urldecode($args['ent_date_time']);

        $returned = $this->_validate_token($args['ent_sess_token'], $args['ent_dev_id'], $args['ent_user_type'], '1');

        if (is_array($returned))
            return $returned;

        $logoutQry = "update user_sessions set loggedIn = '2' where oid = '" . $this->User['entityId'] . "' and sid = '" . $this->User['sid'] . "' and user_type = '" . $args['ent_user_type'] . "'";
        $logoutRes = mysql_query($logoutQry, $this->db->conn);

        if (mysql_affected_rows() >= 0) {

            if ($args['ent_user_type'] == '1') {

                $location = $this->mongo->selectCollection('location');

                $location->update(array('user' => (int) $this->User['entityId']), array('$set' => array('status' => 4, 'login' => 2, 'socket' => "")));

                $argstopass = array('proId' => $this->User['entityId']);
                $this->sendDatatoSocket($argstopass, "location");
            }
            return $this->_getStatusMessage(29, $logoutQry);
        } else {
            return $this->_getStatusMessage(3, $logoutQry);
        }
    }

    /*
     * Method name: _getMasterReviews
     * Desc: Get master reviews with pagination
     * Input: Master email, page number
     * Output:  gives review details if available else error msg
     */

    protected function _getMasterReviews($args) {

        $pageNum = (int) $args['ent_page'];
        $reviewsArr = array();

        if ($args['ent_page'] == '')
            $pageNum = 1;

        if ($pageNum == 1)
            $lowerLimit = ((($pageNum - 1) * $this->reviewsPageSize));
        else
            $lowerLimit = ((($pageNum - 1) * $this->reviewsPageSize) + 1);

        $upperLimit = $this->reviewsPageSize;

//        $selectReviewsQry = "select rev.doc_id,rev.patient_id,rev.review_dt,rev.star_rating,rev.review,(select count(*) from doctor_ratings where doc_id = rev.doc_id and review != '') as total_rows,(select first_name from patient where patient_id = rev.patient_id) as patient_name,(select profile_pic from patient where patient_id = rev.patient_id) as pat_pro_pic from doctor_ratings rev,doctor d where d.doc_id = '" . $args['ent_pro_id'] . "' and d.doc_id = rev.doc_id and rev.status = 1 and rev.review != '' order by star_rating DESC limit $lowerLimit,$upperLimit";
        $selectReviewsQry = "select rev.doc_id,rev.patient_id,rev.review_dt,rev.star_rating,rev.review,(select count(*) from doctor_ratings where doc_id = rev.doc_id and review != '') as total_rows,(select first_name from patient where patient_id = rev.patient_id) as patient_name,(select profile_pic from patient where patient_id = rev.patient_id) as pat_pro_pic from doctor_ratings rev,doctor d where d.doc_id = '" . $args['ent_pro_id'] . "' and d.doc_id = rev.doc_id and rev.status = 1 and rev.review != '' order by star_rating DESC limit $lowerLimit,$upperLimit";
        $selectReviewsRes = mysql_query($selectReviewsQry, $this->db->conn);

        if (mysql_num_rows($selectReviewsRes) <= 0)
            return $this->_getStatusMessage(28, $selectReviewsQry);


        while ($review = mysql_fetch_assoc($selectReviewsRes)) {
            if ($review['review_dt'] != '') {
                $revDtArr = explode(" ", $review['review_dt']);
            }
            if ($review['review'] != '')
                $reviewsArr[] = array('patPic' => ($review['pat_pro_pic'] == '') ? $this->default_profile_pic : $review['pat_pro_pic'], 'rating' => $review['star_rating'], 'review' => $review['review'], 'by' => $review['patient_name'], 'dt' => $revDtArr[0]);
        }

        return $reviewsArr;
    }

    protected function mobileVerification($args, $test = NULL, $message = NULL) {

        $client = new Services_Twilio(ACCOUNT_SID, AUTH_TOKEN);

        if ($test == NULL) {
            $message = "Thank you for registering with " . APP_NAME . ". Your confirmation code is " . $args['ent_rand'] . ".";
        }

        try {
            $message = $client->account->messages->create(array(
                'To' => $args['ent_mobile'],
                'From' => ACCOUNT_NUMBER,
                'Body' => $message
            ));
        } catch (Exception $e) {  //on error push userId in to error array
            $notifications = $this->mongo->selectCollection('mobilestatuerror');
            $notifications->insert(array('args' => $args, 'data' => $e->getMessage()));
        }

        return true;
    }

    /*
     * Method name: _validate_token
     * Desc: Authorizes the user with token provided
     * Input: Token
     * Output:  gives entity details if available else error msg
     */

    protected function _validate_token($ent_sess_token, $ent_dev_id, $user_type, $test = NULL) {
        if ($ent_sess_token == '' || $ent_dev_id == '') {
            return $this->_getStatusMessage(1, 101);
        } else {
            $sessDetArr = $this->_getSessDetails($ent_sess_token, $ent_dev_id, $user_type, $test);
//            print_r($sessDetArr);
            if ($sessDetArr['flag'] == '0') {
                $this->_updateActiveDateTime($sessDetArr['entityId'], $user_type);
                $this->User = $sessDetArr;
            } else if ($sessDetArr['flag'] == '3') {
                return $this->_getStatusMessage($sessDetArr['errNum'], 999);
            } else if ($sessDetArr['flag'] == '1') {
                $this->User = $sessDetArr;
                return $this->_getStatusMessage(6, 102);
            } else {
                return $this->_getStatusMessage(7, $sessDetArr);
            }
        }
    }

    /*
     * Method name: _checkEntityLogin
     * Desc: Checks the unique id with the authentication type
     * Input: Unique id and the auth type
     * Output:  entity details if true, else false
     */

    protected function _checkEntityLogin($id, $auth_type) {

        $checkFBIdQry = "select ent.Entity_Id as entId,edet.Profile_Pic_Url,ent.Create_Dt,ent.Status from entity ent,entity_details edet where ent.Entity_Id = edet.Entity_Id and ent.Unique_Identifier = '" . $id . "' and ent.authType = '" . $auth_type . "'";
        $checkFBIdRes = mysql_query($checkFBIdQry, $this->db->conn);

        if (mysql_num_rows($checkFBIdRes) == 1) {

            $userDet = mysql_fetch_assoc($checkFBIdRes);

            if ($userDet['Profile_Pic_Url'] == "")
                $userDet['Profile_Pic_Url'] = $this->default_profile_pic;

            return array('flag' => '1', 'entityId' => $userDet['entId'], 'profilePic' => $userDet['Profile_Pic_Url'], 'joined' => $userDet['Create_Dt'], 'status' => $userDet['Status'], 'test' => $checkFBIdQry);
        } else {

            return array('flag' => '0', 'test' => $checkFBIdQry);
        }
    }

    /*
     * Method name: _getDeviceTypeName
     * Desc: Returns device name using device type id
     * Input: Device type id
     * Output:  Array with Device type name if true, else false
     */

    protected function _getDeviceTypeName($devTypeId) {

        $getDeviceNameQry = "select name from dev_type where dev_id = '" . $devTypeId . "'";
        $devNameRes = mysql_query($getDeviceNameQry, $this->db->conn);
        if (mysql_num_rows($devNameRes) > 0) {

            $devNameArr = mysql_fetch_assoc($devNameRes);
            return array('flag' => true, 'name' => $devNameArr['name']);
        } else {

            return array('flag' => false);
        }
    }

    /*
     * Method name: _verifyEmail
     * Desc: Checks email for uniqueness
     * Input: Email id to be checked
     * Output:  true if available else false
     */

    protected function _verifyEmail($email, $field, $table) {

        $searchEmailQry = "select $field,status from $table where email = '" . $email . "'";
        $searchEmailRes = mysql_query($searchEmailQry, $this->db->conn);

        if (mysql_num_rows($searchEmailRes) > 0)
            return mysql_fetch_assoc($searchEmailRes);
        else
            return false;
    }

    protected function _verifyMobile($mobile, $field, $table) {

        $searcMobileQry = "select $field,status from $table where mobile = '" . $mobile . "'";
        $searchMobileRes = mysql_query($searcMobileQry, $this->db->conn);

        if (mysql_num_rows($searchMobileRes) > 0)
            return mysql_fetch_assoc($searchMobileRes);
        else
            return false;
    }

    /*
     * Method name: _getStatusMessage
     * Desc: Get details of an error from db
     * Input: Error number that need details
     * Output:  Returns an array with error details
     */

    protected function _getStatusMessage($errNo, $test_num) {

        $msg = new getErrorMsg($errNo, $this->db->conn);
        return array('errNum' => $msg->errId, 'errFlag' => $msg->errFlag, 'errMsg' => $msg->errMsg, 'test' => $test_num);
    }

    /*
     * Method name: _getSessDetails
     * Desc: retrieves a session details
     * Input: Object Id, Token and user_type
     * Output: 1 for Success and 0 for Failure
     */

    protected function _getSessDetails($token, $device_id, $user_type, $test) {

        if ($user_type == '1')
            $getDetQry = "select  us.loggedIn, us.oid, us.expiry, us.device, us.type, us.sid,doc.status,doc.first_name,doc.last_name,doc.profile_pic,doc.email,doc.stripe_id,doc.mobile,(select degree from doctor_education where doc_id = doc.doc_id limit 0,1) as degree from user_sessions us, doctor doc where us.oid = doc.doc_id and us.token = '" . $token . "' and us.device = '" . $device_id . "' and us.user_type = '" . $user_type . "'";
        else if ($user_type == '2')
            $getDetQry = "select  us.loggedIn, us.oid, us.expiry, us.device, us.type, us.sid,pat.status,pat.first_name,pat.last_name,pat.profile_pic,pat.email,pat.stripe_id,pat.phone as mobile from user_sessions us, patient pat where us.oid = pat.patient_id and us.token = '" . $token . "' and us.device = '" . $device_id . "' and us.user_type = '" . $user_type . "'";
//echo $getDetQry;
        $getDetRes = mysql_query($getDetQry, $this->db->conn);



//        $this->testing = $getDetQry;

        if (mysql_num_rows($getDetRes) > 0) {

            $sessDet = mysql_fetch_assoc($getDetRes);
//print_r($sessDet);

            if ($sessDet['profile_pic'] == "")
                $sessDet['profile_pic'] = $this->default_profile_pic;

            if ($test == NULL) {
                if ($sessDet['loggedIn'] == '2')
                    return array('flag' => '2', 'test' => $getDetQry);
                else if ($sessDet['loggedIn'] == '3')
                    return array('flag' => '3', 'errNum' => 83);

                if ($sessDet['status'] == '4')
                    return array('flag' => '3', 'errNum' => 76);
                else if ($sessDet['status'] == '1')
                    return array('flag' => '3', 'errNum' => 105);
                else if ($sessDet['status'] == '1' || $sessDet['status'] == '2')
                    return array('flag' => '3', 'errNum' => 11);
            }
            if ($sessDet['expiry'] > $this->curr_date_time) {
                return array('flag' => '0', 'sid' => $sessDet['sid'], 'status' => $sessDet['status'], 'entityId' => $sessDet['oid'], 'deviceId' => $sessDet['device'], 'deviceType' => $sessDet['type'], 'firstName' => ucfirst(strtolower($sessDet['first_name'])), 'lastName' => ucfirst(strtolower($sessDet['last_name'])), 'pPic' => $sessDet['profile_pic'], 'email' => $sessDet['email'], 'stripe_id' => $sessDet['stripe_id'], 'mobile' => $sessDet['mobile'], 'degree' => $sessDet['degree'], 't' => $getDetQry);
            } else {
                return array('flag' => '1', 'entityId' => $sessDet['oid']);
            }
        } else {
            return array('flag' => '2', 'test' => $getDetQry);
        }
    }

    /*
     * Method name: _checkSession
     * Desc: Check a session details
     * Input: Object Id, Token and user_type
     * Output: returns array of updated session details or new session details
     */

    protected function _checkSession($args, $oid, $user_type, $device_name) {

        $token_obj = new ManageToken($this->db);

        $deleteAllOtherSessionsQry = "update user_sessions set loggedIn = '3' where user_type = '" . $user_type . "' and loggedIn = '1' and ((oid = '" . $oid . "' and device != '" . $args['ent_dev_id'] . "') or (oid != '" . $oid . "' and device = '" . $args['ent_dev_id'] . "'))";
        mysql_query($deleteAllOtherSessionsQry, $this->db->conn);

//        if ($args['ent_device_type'] == '1' && trim($args['ent_push_token']) != '') {
//            $AmazonSns = new AwsPush();
//
//            $resPush = $AmazonSns->createPlatformEndpoint($args['ent_push_token'], $user_type, $args['ent_device_type']);
//            if ($resPush === false)
//                $args['ent_push_token'] = 123;
//            else
//                $args['ent_push_token'] = $resPush['EndpointArn'];
//        }

        if ($user_type == '1') {

            $location = $this->mongo->selectCollection('location');
            $location->update(array('devId' => $args['ent_dev_id']), array('$set' => array('status' => 4)), array('multiple' => true));

            $checkUserSessionQry = "select sid, token, expiry,device from user_sessions where oid = '" . $oid . "' and user_type = '" . $user_type . "' and loggedIn = '1'"; // and device != '" . $args['ent_dev_id'] . "'

            $checkUserSessionRes = mysql_query($checkUserSessionQry, $this->db->conn);

            $num = mysql_num_rows($checkUserSessionRes);

            $res = mysql_fetch_assoc($checkUserSessionRes);

            if ($num == 1 && $res['device'] == $args['ent_dev_id']) {
                return $token_obj->updateSessToken($oid, $args['ent_dev_id'], $args['ent_push_token'], $user_type, $this->curr_date_time, $args['ent_app_version'], $args['ent_dev_os'], $args['ent_dev_model'], $args['ent_manf']);
            } else if ($num >= 1 && $res['device'] != $args['ent_dev_id']) {
                return $token_obj->createSessToken($oid, $device_name, $args['ent_dev_id'], $args['ent_push_token'], $user_type, $this->curr_date_time, $args['ent_app_version'], $args['ent_dev_os'], $args['ent_dev_model'], $args['ent_manf']);
            } else {
                return $token_obj->createSessToken($oid, $device_name, $args['ent_dev_id'], $args['ent_push_token'], $user_type, $this->curr_date_time, $args['ent_app_version'], $args['ent_dev_os'], $args['ent_dev_model'], $args['ent_manf']);
            }
        } else {

            $checkUserSessionQry = "select sid, token, expiry from user_sessions where oid = '" . $oid . "' and device = '" . $args['ent_dev_id'] . "' and user_type = '" . $user_type . "'";
            $checkUserSessionRes = mysql_query($checkUserSessionQry, $this->db->conn);

            if (mysql_num_rows($checkUserSessionRes) == 1)
                return $token_obj->updateSessToken($oid, $args['ent_dev_id'], $args['ent_push_token'], $user_type, $this->curr_date_time, $args['ent_app_version'], $args['ent_dev_os'], $args['ent_dev_model'], $args['ent_manf']);
            else
                return $token_obj->createSessToken($oid, $device_name, $args['ent_dev_id'], $args['ent_push_token'], $user_type, $this->curr_date_time, $args['ent_app_version'], $args['ent_dev_os'], $args['ent_dev_model'], $args['ent_manf']);
        }
    }

    /*
     * Method name: _getEntityDet
     * Desc: Gives facebook id for entity id 
     * Input: Request data, entity_id
     * Output: entity details for success or error array
     */

    protected function _getEntityDet($eid, $userType) {

        if ($userType == '1')
            $getEntityDetQry = "select profile_pic,first_name,last_name,doc_id,mobile,email,badge,type_id,amount from doctor where email = '" . $eid . "' or doc_id = '" . $eid . "'";
        else
            $getEntityDetQry = "select profile_pic,first_name,patient_id,last_name,email,stripe_id from patient where email = '" . $eid . "'";

        $getEntityDetRes = mysql_query($getEntityDetQry, $this->db->conn);

        if (mysql_num_rows($getEntityDetRes) > 0) {

            $det = mysql_fetch_assoc($getEntityDetRes);

            if ($det['profile_pic'] == '')
                $det['profile_pic'] = $this->default_profile_pic;

            return $det;
        } else {
            return false;
        }
    }

    /*
     * Method name: _sendPush
     * Desc: Divides the tokens according to device type and sends a push accordingly
     * Input: Request data, entity_id
     * Output: 1 - success, 0 - failure
     */

    protected function pushFromAdmin($args) {
        if ($args['ent_message'] == "" || $args['ent_notify'] == "" || $args['ent_usertype'] == "" || $args['ent_date_time'] == "")
            return $this->_getStatusMessage(1, 1);
        if (!is_array($args['ent_id']))
            return $this->_getStatusMessage(1, 1);

        $aplPushContent = array('alert' => $args['ent_message'], 'sound' => 'default', 'st' => $args['ent_notify']);
        $andrPushContent = array("payload" => $args['ent_message'], 'st' => $args['ent_notify']);

        $push_res = $this->_sendPush(1, $args['ent_id'], $args['ent_message'], '6', $args['ent_message'], $args['ent_date_time'], $args['ent_usertype'], $aplPushContent, $andrPushContent);
        return array('push_res' => $push_res);
    }

    protected function _sendPush($senderId, $recEntityArr, $message, $notifType, $sname, $datetime, $user_type, $aplContent, $andrContent, $user_device = NULL) {

        $entity_string = '';
        $aplTokenArr = array();
        $andiTokenArr = array();
        $return_arr = array();

        foreach ($recEntityArr as $entity) {

            $entity_string .= $entity . ',';
        }

        $entity_comma = rtrim($entity_string, ',');
//echo '--'.$entity_comma.'--';

        $device_check = '';
        if ($user_device != NULL)
            $device_check = " and device = '" . $user_device . "'";

        $getUserDevTypeQry = "select distinct type,push_token from user_sessions where oid in (" . $entity_comma . ") and loggedIn = '1' and user_type = '" . $user_type . "' and LENGTH(push_token) > 63" . $device_check;

        $getUserDevTypeRes = mysql_query($getUserDevTypeQry, $this->db->conn);
        if (mysql_num_rows($getUserDevTypeRes) > 0) {

            while ($tokenArr = mysql_fetch_assoc($getUserDevTypeRes)) {

                if ($tokenArr['type'] == 1)
                    $aplTokenArr[] = $tokenArr['push_token'];
                else if ($tokenArr['type'] == 2)
                    $andiTokenArr[] = $tokenArr['push_token'];
            }

            $aplTokenArr = array_values(array_filter(array_unique($aplTokenArr)));
            $andiTokenArr = array_values(array_filter(array_unique($andiTokenArr)));

            $aplResponse = $this->_sendFcmPush($andiTokenArr, $aplTokenArr, $andrContent, $aplContent, $user_type);

            foreach ($recEntityArr as $entity) {
                $ins_arr = array('notif_type' => (int) $notifType, 'sender' => (int) $senderId, 'reciever' => (int) $entity, 'message' => $message, 'notif_dt' => $datetime, 'apl' => $aplTokenArr, 'andr' => $andiTokenArr); //'aplTokens' => $aplTokenArr, 'andiTokens' => $andiTokenArr, 'andiRes' => $andiResponse,

                $newDocID = $ins_arr['_id'];
                $return_arr[] = array($entity => $newDocID);
            }
            if ($aplResponse['errorNo'] != '')
                $errNum = $aplResponse['errorNo'];
//            else if ($andiResponse['errorNo'] != '')
//                $errNum = $andiResponse['errorNo'];
            else
                $errNum = 46;

            return array('insEnt' => $return_arr, 'errNum' => $errNum, 'andiRes' => $andiResponse);
        } else {
            return array('insEnt' => $return_arr, 'errNum' => 45, 'andiRes' => $andiResponse); //means push not sent
        }
    }

    protected function testfcmpush($args) {
//        $args['ent_ios_token'] = "cG4Z2VQXSt4:APA91bHBxJb1j7Iyum9zcx02iK0-aPK3Oqe_U71QF282KF5hxsev8O5LyDBXN8zV8lFl0XJ0pGMc0QFfqejfFSrL6rQQT2KBIRUIfEdoakjFwS2gqdA8An_a69zZxR-pEb_jhGojzM5O";
//        $args['ent_and_token'] = "";
        $aplPushContent = array('alert' => "test push from server", 'nt' => '8', 'n' => "Hello Dear");
        $andrPushContent = array("payload" => "test push from server", 'action' => '8', 'sname' => "Hello Dear");
        $aplTokenArr = array($args['ent_ios_token']);
        $andiTokenArr = array($args['ent_and_token']);
        return $this->_sendFcmPush($andiTokenArr, $aplTokenArr, $andrPushContent, $aplPushContent, 1);
    }

    function _sendFcmPush($andiTokenArr, $aplTokenArr, $andrContent, $aplContent, $user_type) {
        $aplContent['sound'] = 'default';
        $andrContent['sound'] = 'default';

        $applPush = array(
            'to' => '',
            'collapse_key' => 'your_collapse_key',
            'priority' => 'high',
            'notification' => array(
                'title' => '',
                'body' => $aplContent['alert'],
                'sound' => 'default'
            ),
            'data' => $aplContent
        );
        $andPush = array(
            'to' => '',
            'collapse_key' => 'your_collapse_key',
            'priority' => 'high',
//            'notification' => $andrContent,
            'data' => $andrContent
        );
        $header = array('Content-Type: application/json', "Authorization: key=" . PUSH_FCM_KEY);
        foreach ($andiTokenArr as $token) {
            $andPush['to'] = $token;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, FCM_PUSH_URL);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($andPush));
            $result = curl_exec($ch);
            curl_close($ch);
            $res_dec = $result;
        }
        foreach ($aplTokenArr as $token) {
            $applPush['to'] = $token;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, FCM_PUSH_URL);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($applPush));
            $result = curl_exec($ch);
            curl_close($ch);
            $res_dec = $result;
        }

        if ($res_dec['success'] >= 1)
            return array('errorNo' => 44, 't' => $applPush, 'tok' => $aplTokenArr, 'ret' => $res_dec->result, 'res' => $res_dec);
        else
            return array('errorNo' => 46, 't' => $applPush, 'tok' => $aplTokenArr, 'res' => $res_dec);
    }

    /*
     * method name: generateRandomString
     * Desc: Generates a random string according to the length of the characters passed
     * Input: length of the string
     * Output: Random string
     */

    protected function _generateRandomString($length) {

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $randomString;
    }

    protected function _updateActiveDateTime($entId, $user_type) {

        if ($user_type == '1')
            $updateQry = "update doctor set last_active_dt = '" . $this->curr_date_time . "' where doc_id = '" . $entId . "'";
        else if ($user_type == '2')
            $updateQry = "update patient set last_active_dt = '" . $this->curr_date_time . "' where patient_id = '" . $entId . "'";

        mysql_query($updateQry, $this->db->conn);

        if (mysql_affected_rows() > 0)
            return true;
        else
            return false;
    }

    /*
     * Method name: _check_zip
     * Desc: Authorizes the user with token provided
     * Input: Zipcode
     * Output:  gives entity details if available else error msg
     */

    protected function _check_zip($zip_code) {

        $selectZipQry = "select zipcode from zipcodes where zipcode = '" . $zip_code . "'";
        $selectZipRes = mysql_query($selectZipQry, $this->db->conn);
        if (mysql_num_rows($selectZipRes) > 0)
            return true;
        else
            return false;
    }

    protected function expireAppointments($args) {

        $expireTime = time() - (60 * 60);
        $selectAppointmentsQry = "select a.appointment_id,a.doc_id,a.patient_id,d.first_name as doc_fname,d.last_name as doc_lname,p.first_name as pat_fname,p.last_name as pat_lname from appointment a,doctor d,patient p where a.doc_id = d.doc_id and a.patient_id = p.patient_id and a.status != 8 and a.server_ts <= '" . $expireTime . "' order by a.appointment_id ";
        $selectAppointmentsRes = mysql_query($selectAppointmentsQry);

        $updateQry = "update appointment set status = 8 where appointment_id in (";
        $idsComma = $push = array();

        while ($appt = mysql_fetch_assoc($selectAppointmentsRes)) {

            $idsComma[] = $appt['appointment_id'];

            $message = "Your request for appointment with " . $appt['doc_fname'] . " " . $appt['doc_lname'] . " got expired.";

            $aplPushContent = array('alert' => $message, 'nt' => '8', 'n' => $appt['doc_fname']);
            $andrPushContent = array("payload" => $message, 'action' => '8', 'sname' => $appt['doc_fname']);

            $push[] = $this->_sendPush($appt['doc_id'], array($appt['patient_id']), $message, '6', $appt['doc_fname'], $this->curr_date_time, '2', $aplPushContent, $andrPushContent);
        }

        $updateQry .= implode(',', array_filter($idsComma)) . ")";

        mysql_query($updateQry, $this->db->conn);

        return array('affected' => mysql_affected_rows(), 'push' => $push, 'qry' => $selectAppointmentsQry);
    }

    private function week_start_end_by_date($date, $format = 'Y-m-d') {

//Is $date timestamp or date?
        if (is_numeric($date) AND strlen($date) == 10) {
            $time = $date;
        } else {
            $time = strtotime($date);
        }

        $week['week'] = date('W', $time);
        $week['year'] = date('o', $time);
        $week['year_week'] = date('oW', $time);
        $first_day_of_week_timestamp = strtotime($week['year'] . "W" . str_pad($week['week'], 2, "0", STR_PAD_LEFT));
        $week['first_day_of_week'] = date($format, $first_day_of_week_timestamp);
        $week['first_day_of_week_timestamp'] = $first_day_of_week_timestamp;
        $last_day_of_week_timestamp = strtotime($week['first_day_of_week'] . " +6 days");
        $week['last_day_of_week'] = date($format, $last_day_of_week_timestamp);
        $week['last_day_of_week_timestamp'] = $last_day_of_week_timestamp;

        return $week;
    }
    
    public function test_mail(){
	    $toMail = "tonylu0302@gmail.com";
	    $toName = "tony lu";
	    $mail = new sendAMail(APP_SERVER_HOST);
        $mailArr = $mail->testMail($toMail, $toName);
        var_dump($mailArr);
    }
    

    public function getLangueage() {

        $getLanguate = $this->MongoClass->get('lang_hlp');
//        print_r($getLanguate);
        $data = array();

        $data[] = (object) array("lan_id" => 0, "lan_name" => "English");
        foreach ($getLanguate['data'] as $lan) {
//            $data[] = array($lan['lan_id'] => $lan['lan_name']);
//            if ($lan['lan_id'][$lan['lan_name']] != "")
//                $data[] = $lan['lan_id'][$lan['lan_name']];
            unset($lan['_id']);
            $data[] = $lan;
        }
        return $data;
    }

    protected function truncateDB12321($args) {

        $num = 0;

        $qry2 = "truncate table doctor";
        mysql_query($qry2, $this->db->conn);
        $num += mysql_affected_rows();

        $qry12 = "truncate table patient";
        mysql_query($qry12, $this->db->conn);
        $num += mysql_affected_rows();

        $qry21 = "truncate table user_sessions";
        mysql_query($qry21, $this->db->conn);
        $num += mysql_affected_rows();

        $qry22 = "truncate table doctor_ratings";
        mysql_query($qry22, $this->db->conn);
        $num += mysql_affected_rows();

        $qry23 = "truncate table appointment";
        mysql_query($qry23, $this->db->conn);
        $num += mysql_affected_rows();

        $qry24 = "truncate table doctor_education";
        mysql_query($qry24, $this->db->conn);
        $num += mysql_affected_rows();

        $qry25 = "truncate table images";
        mysql_query($qry25, $this->db->conn);
        $num += mysql_affected_rows();

        $qry26 = "truncate table workplace";
        mysql_query($qry26, $this->db->conn);
        $num += mysql_affected_rows();

        $location = $this->mongo->selectCollection('location');
        $response[] = $location->drop();

        $slot = $this->mongo->selectCollection('slotbooking');
        $response[] = $slot->drop();

        $masterLoc = $this->mongo->selectCollection('masterLocations');
        $response[] = $masterLoc->drop();

        $location->ensureIndex(array("location" => "2d"));

        $cursor = $location->find();

        $data = array();

        foreach ($cursor as $doc) {
            $data[] = $doc;
        }

        $dir = 'pics/';
        $leave_files = array('aa_default_profile_pic.gif');

        $image = 0;

        foreach (glob("$dir/* ") as $file) {
            if (is_dir($file)) {
                foreach (glob("$dir/$file/*") as $file1) {
                    if (!in_array(basename($file1), $leave_files)) {
                        unlink($file);
                        $image++;
                    }
                }
            } else if (!in_array(basename($file), $leave_files)) {
                unlink($file);
                $image++;
            }
        }


        $dir1 = 'pics/mdpi/';


        foreach (glob("$dir1/*") as $file) {
            if (is_dir($file)) {
                foreach (glob("$dir1/$file/*") as $file1) {
                    if (!in_array(basename($file1), $leave_files)) {
                        unlink($file);
                        $image++;
                    }
                }
            } else if (!in_array(basename($file), $leave_files)) {
                unlink($file);
                $image++;
            }
        }

        $dir2 = 'pics/hdpi/';

        foreach (glob("$dir2/*") as $file) {
            if (is_dir($file)) {
                foreach (glob("$dir2/$file/*") as $file1) {
                    if (!in_array(basename($file1), $leave_files)) {
                        unlink($file);
                        $image++;
                    }
                }
            } else if (!in_array(basename($file), $leave_files)) {
                unlink($file);
                $image++;
            }
        }

        $dir3 = 'pics/xhdpi/';

        foreach (glob("$dir3/*") as $file) {
            if (is_dir($file)) {
                foreach (glob("$dir3/$file/*") as $file1) {
                    if (!in_array(basename($file1), $leave_files)) {
                        unlink($file);
                        $image++;
                    }
                }
            } else if (!in_array(basename($file), $leave_files)) {
                unlink($file);
                $image++;
            }
        }

        $dir4 = 'pics/xxhdpi/';

        foreach (glob("$dir4/*") as $file) {
            if (is_dir($file)) {
                foreach (glob("$dir4/$file/*") as $file1) {
                    if (!in_array(basename($file1), $leave_files)) {
                        unlink($file);
                        $image++;
                    }
                }
            } else if (!in_array(basename($file), $leave_files)) {
                unlink($file);
                $image++;
            }
        }

        $dir5 = 'invoice/';

        foreach (glob("$dir5/*") as $file) {
            unlink($file);
            $image++;
        }

        $dir6 = 'certificate/';

        foreach (glob("$dir6/*") as $file) {
            unlink($file);
            $image++;
        }

        return array('mongodb' => $response, 'data' => $data, 'rows' => $num, 'images' => $image);
    }

}

if (!array_key_exists('HTTP_ORIGIN', $_SERVER)) {

    $_SERVER['HTTP_ORIGIN'] = $_SERVER['SERVER_NAME'];
}
try {
   /*
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
       Header('Status: 404 Not Found');

       $uri_404 = 'http://'
               . $_SERVER['HTTP_HOST']
               . ($_SERVER['HTTP_PORT'] ? (':' . $_SERVER['HTTP_PORT']) : '')
               . '/services.php';
       $curl_req = curl_init($uri_404);
       curl_setopt($curl_req, CURLOPT_MUTE, true);
       $body = curl_exec($curl_req);
       curl_close($curl_req);
   } else {
       $API = new MyAPI($_SERVER['REQUEST_URI'], $_REQUEST, $_SERVER['HTTP_ORIGIN']);
       echo $API->processAPI();
   }
*/
    $API = new MyAPI($_SERVER['REQUEST_URI'], $_REQUEST, $_SERVER['HTTP_ORIGIN']);
    echo $API->processAPI();
} catch (Exception $e) {

    echo json_encode(Array('error' => $e->getMessage()));
}
?>
