<?php
 /* Template Name: confirm phone number */
 ?>
 <?php get_header(); ?> 
 <link href="<?php echo get_stylesheet_directory_uri();?>/assets/css/otp.css" rel="stylesheet" />
 <style type="text/css">
     #loader_verify {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 0.5s linear infinite;
  animation: spin 0.5s linear infinite;
}
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
.parent_verify {
    position: fixed;
    width: 100%;
    height: 100%;
    background: rgba(0,0,0,0.5);
    z-index: 999;
}

.sub_parent_verify {
    background: white;
    width: 150px;
    padding: 10px;
    border-radius: 10px;
    position: absolute;
    left: calc(50% - 75px);
    top: calc(50% - 75px);
}
 </style>
 <div class="parent_verify" style="display: none;">
 <div class="sub_parent_verify">
<div id="loader_verify" >

</div>
    <span>Verifying Phone...</span>
</div></div>
<!-- <div id="loader" class="loder-hide"></div> -->
 <section  class="ng-scope not-activated ">
   
    <section class="content-section ng-scope" id="bgimg" >
        <div class="page">
            <div class="login-page login-page--center login-page--margin-120">
                <img src="../wp-content/uploads/2017/11/Gyapom-website-home-logo.png" class="imgctr">
                <h3 class="login-page__header login-page__header--sm">Confirm your number</h3>
                
                
                <form name="activationForm" class="not-activated__form ng-pristine ng-valid" >
                    <div form-errors="" class="ng-isolate-scope"></div>
                    <div class="form-group">
                        <div class="control">
                            <div class="control__label control__label--activation">Verify the code</div>
                            <div name="validationCode" class="control__field control__field--activation ng-pristine ng-untouched ng-valid ng-empty" validation-key="validationCode">
                                <span  class="ng-scope">
                                    <div class="not-activated__input-container">
                                        <input type="text" id="verification1" maxlength='1'  placeholder="-" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                    </div>
                                    <span class="not-activated__divider ng-scope"></span>
                                </span>
                                <span  class="ng-scope">
                                    <div class="not-activated__input-container">
                                        <input type="text" id="verification2" maxlength='1'  placeholder="-" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                    </div>
                                    <span class="not-activated__divider ng-scope"></span>
                                </span>
                                <span  class="ng-scope">
                                    <div class="not-activated__input-container">
                                        <input type="text" id="verification3" maxlength='1' placeholder="-" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                    </div>
                                    <span class="not-activated__divider ng-scope" ></span>
                                </span>
                                <span  class="ng-scope">
                                    <div class="not-activated__input-container">
                                        <input type="text" id="verification4" maxlength='1'  placeholder="-" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                    </div>
                                    <span class="not-activated__divider ng-scope" ></span>
                                </span>
                                
                            </div>
                            <div form-errors="" name="validationCode" theme="alert--label-only" class="ng-isolate-scope"></div>
                        </div>
                    </div>

                       <span class="not-activated__submit-btn verify-code" id="verify-code" style="display: none;">
                        Verify
                       </span>
                </form>
                <h2 id="emailSuccess"></h2>
                <h2 id="emailError"></h2>
                <section class="not-activated__bottom-content">
                    Haven't received the message?
                    <!--<button type="button" class="not-activated__btn" >Change number</button>
                    or-->
                    <button type="button" class="not-activated__btn" id="verify-resend" >Resend</button>
                </section>
            </div>
        </div>
    </section>
    <div class="not-activated__spacer ng-scope" style="display: none;"></div>
    <section class="not-activated__mobile-bottom-content ng-scope">
        <div class="not-activated__cell">
            <div>Haven't received a message yet?</div>
            <!--<button type="button" class="not-activated__mobile-btn">
                Change number
            </button>-->
            <button type="button" class="not-activated__mobile-btn">
                Resend
            </button>
        </div>
    </section><!-- end ngIf: !notActivatedCtrl.isShowChangeNumber -->
    
</section>

 <?php get_footer(); ?>
 <script type="text/javascript">
     jQuery(document).ready(function($){
        $('#verification1').focus();
     });
 </script>
 <?php include_once( dirname(__FILE__) . '/template/api-functions/otppassword.php'); ?>

 