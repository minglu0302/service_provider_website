<?php

/*
Template Name:  cleaning on the way
*/
get_header('frontpage');	
?>
<link href="<?php echo get_stylesheet_directory_uri();?>/assets/css/cleaning.css" rel="stylesheet" />
<link href="<?php echo get_stylesheet_directory_uri();?>/css/api.css" rel="stylesheet" />


<div class="page_heading_container">
  <div class="cleaning_template">     
		<div class="col-md-12">
		  <div class="page_heading_content"></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
<div class="page-container">
  <div class="cleaning--default">     
    <div class="page-content">
      <div class="col-md-12">
        <div class="fullwidth">           
          <?php if (have_posts()) : the_post(); ?>
				  <?php the_content(); ?>	
			    <?php endif; ?>	  
				</div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</div>

<div class="js-progress-container">
  <div class="build-progress" data-pie-id="269">
    <div class="build-progress__container container--wide">
      <div class="build-progress__step js-step" data-page="form" data-state="current">
        <i class="fa fa-pencil"></i>
        <span>1.</span>
        <span>Fill Out Task Details</span>
      </div>
      <div class="build-progress__step js-step" data-page="recommendations" data-state="future">
        <i class="ss-smile"></i>
        <span>2.</span>
        <span>View Taskers &amp; Prices</span>
      </div>

      <div class="build-progress__step js-step" data-page="confirm" data-state="future">
        <i class="ss-checkmark"></i>
        <span>3.</span>
        <span>Confirm &amp; Book</span>
      </div>
    </div>
  </div>
</div>

<div class="js-trust-banner-container">
  <div class="build-trust-banner box-no-padding" data-pie-id="297">
    <div class="build-trust-banner__container container--wide">
      <img alt="Trust badge" class="build-trust-banner__badge trust" src="http://www.goclean-service.com/wp-content/uploads/2017/12/logo-stripe.png">
      <div class="build-trust-banner__title">
        <strong>Trust &amp; Safety Guarantee:</strong> $1MM insurance guarantee on every task.
      </div>
      <a class="build-trust-banner__info u-hidden--sm u-hidden--md u-hidden--lscp js-tt-trigger" data-tt-contenttemplate="build.trustBannerTooltip" data-tt-placement="b" href="#">
        <i class="fa fa-info"></i>
      </a>
    </div>
  </div>
</div>

<div class="js-child-container">
  <div class="page-loader"></div>
    <div data-pie-id="469">
      <div class="view--build-main view--body">
        <div class="container--wide build-container row">
          <div class="js-subheader-container col-12 col-lg-10 col--centered">
            <div data-pie-id="496">
              <div class="build-subheader build-subheader--form">
                <div class="build-subheader__title">General Cleaning

                  <a class="build-subheader__mod-link js-change-template new1popup " href="#">Change</a>
                </div>
                <div class="supplemental_infos">
                  <div class="panel build-subheader__panel--spaced">
                    <div class="media--object">
                      <div class="media--figure">
                        <img alt="Cleaning kits@2x" height="80" src="http://www.goclean-service.com/wp-content/uploads/2017/12/cleaning_kits@2x-abcfdfa210b1d1bb2827b2010ca18fa0.jpg" width="100">
                      </div>
                      <div class="media--content">
                        <div class="build-subheader__supplemental">
                          <div class="build-subheader__supplemental-info-title">What to Expect</div>
                          <div class="build-subheader__supplemental-info-copy">A highly-rated Tasker will arrive with supplies and a mop and leave your home sparkling clean, including your kitchen, bathrooms, bedrooms and common areas</div>
                        </div>
                        <a class="js-show-supplemental expect_popup" href="#">Read more about what to expect</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="js-items-container col-12 col-lg-10 col--centered">
            <div class="build-group panel js-error-target build-form-input is-focused" data-pie-id="534" data-key="" data-field-name="job_size_group" data-field-type="group" data-state="empty" data-editing="true">
              <h3 class="build-group__title">Task Details</h3>
              <div></div>
              <div class="build-group__summary js-summary" style="display: none;"></div>
              <div class="build-group__items-container js-form-container" style="height: auto; display: block;">
              <div class="js-items-container">
                <div data-pie-id="585" class="js-error-target build-form-input" data-key="job_size" data-field-name="job_size" data-field-type="radio">

                  <h4 class="build-form-input__title">How big is your cleaning task?</h4>

                    <ul class="build-input-list">

                      <li>
                        <input id="job_size_small" name="job_size" type="radio" value="small">
                        <label for="job_size_small">

                        <strong>Small - Est. 1-2 hrs.</strong>
                        <span>Studio or 1 bedroom apartment</span>

                        </label>
                      </li>

                      <li>
                        <input id="job_size_medium" name="job_size" type="radio" value="medium">
                        <label for="job_size_medium">

                        <strong>Medium - Est. 2-3 hrs.</strong>
                        <span>2 bedroom apartment and larger</span>

                        </label>
                      </li>

                      <li>
                        <input id="job_size_large" name="job_size" type="radio" value="large">
                        <label for="job_size_large">

                        <strong>Large - Est. 3+ hrs.</strong>
                        <span>3+ bedroom apartment or house</span>

                        </label>
                      </li>

                    </ul>
                  </div>
                </div>
                <div class="build-group__cta">
                  <button class="btn btn-primary js-group-cta click_open blue_vision" >Continue</button>
                </div>
              </div>
              <i class="build-group__status-icon"></i>
            </div>
            <div class="build-group panel js-error-target build-form-input" data-pie-id="610" data-key="" data-field-name="location_group" data-field-type="group" data-state="empty" data-editing="false">
              <h3 class="build-group__title">Your Task Location</h3>
              <div></div>
              <div class="build-group__summary js-summary"></div>
              <div class="build-group__items-container js-form-container display_open">
                <div class="js-items-container">
                  <div data-pie-id="701" class="js-error-target build-form-input" data-key="location.freeform location.address2 location.lat location.lng location.virtual" data-field-name="location" data-field-type="address">
                    <div class="row row--guttered">
                      <div class="col-12 col-md-8 col-lg-10">
                        <input class="js-autocomplete" name="location.freeform" id="address" placeholder="Enter street address" type="text" value="" autocomplete="off">
                      </div>
                      <div class="col-12 col-md-4 col-lg-2">
                          <input class="ignored-error" id="address-700-address2" name="location.address2" placeholder="Unit or Apt #" type="text" value="">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="build-group__cta">
                  <button class="btn btn-primary js-group-cta click_open2 blue_vision" type="">Continue</button>
                </div>
              </div>
              <i class="build-group__status-icon"></i>
            </div>
            <div class="build-group panel js-error-target build-form-input" data-pie-id="711" data-key="" data-field-name="description_group" data-field-type="group" data-state="empty" data-editing="false">
              <h3 class="build-group__title">Additional Details</h3>
              <div>If you need two or more Taskers, please post additional tasks for each Tasker needed.</div>
              <div class="build-group__summary js-summary"></div>
              <div class="build-group__items-container js-form-container  display_open2">
                <div class="js-items-container">
                  <div data-pie-id="762" class="js-error-target build-form-input" data-key="description" data-field-name="description" data-field-type="text">
                    <textarea name="description" placeholder="Provide any additional instructions for your Tasker if you like. For example, if you have any pets, where to park, or how to get in."></textarea>
                  </div>
                </div>
                <div class="build-group__cta">
                  <a href="http://iserve.ind.in/view-taskers-prices/">
                    <button class="btn btn-primary js-group-cta blue_vision" type="">Continue</button>
                  </a>
                  <div class="build-group__cta-subtext">Next: See available Taskers</div>
                </div>
            </div>
            <i class="build-group__status-icon"></i>
          </div>
        </div>

        <!--popup code for confirm Navigation-->

        <div class="lightbox--container" data-pie-id="733">
          <div class="lightbox--background js-lightbox-bg js-dismiss gone_class"></div>
          <div class="lightbox--internal">
            <div class="lightbox--content-wrapper">
              <div class="lightbox--title js-lightbox-title">Confirm Navigation</div>
              <div class="lightbox--content js-lightbox-content">
                <div class="page-loader"></div>
                <div data-pie-id="726" class="confirm-lightbox">
                  <div class="row confirm-lightbox--content-row">
                    <div class="col-12 centy">
                      <span>Changing your task type will erase everything entered on this page. Continue?</span>
                    </div>
                  </div>
                  <div class="row row--guttered confirm-lightbox--button-row row--thin">
                    <div class="col-12 centy">
                      <button class="js-confirm btn btn-primary close">OK</button>
                      <button class="js-deny btn close">Cancel</button>
                    </div>
                  </div>
                </div>
              </div>
              <a href="#" class="lightbox--dismiss js-lightbox-dismiss js-dismiss close">
                <i class="fa fa-times"></i>
              </a>
            </div>
          </div>
        </div>
        <!--popup code for confirm Navigation-->

        <!--popup code2  for confirm Navigation-->

        <div class="lightbox--container1">
          <div class="lightbox--background js-lightbox-bg js-dismiss popup2 gone_close"></div>
          <div class="lightbox--internal">
            <div class="lightbox--content-wrapper">
              <div class="lightbox--title js-lightbox-title is-hidden"></div>
                <div class="lightbox--content js-lightbox-content">
                  <div class="page-loader"></div>
                  <div class="supplemental-info__modal-body">
                    <h3 class="u-align--center">What to Expect</h3>
                    <div class="row supplemental-info__offering-row flex-container flex-vertical-centering">
                      <img alt="Cleaning kits transparent@2x" class="supplemental-info__offering-img" height="165" src="https://www.goclean-service.com/wp-content/uploads/2017/12/cleaning_kits_transparent@2x-1288481a9d70c3d0cd9fd37e2b850037.png" width="165">
                    </div>
                    <div class="row">
                      <div class="col-12 col-lg-4">
                        <div class="supplemental-info__section">
                          <div class="supplemental-info__section-title">
                              Bedrooms &amp; Common Areas
                          </div>
                          <ul class="list--bulleted">

                            <li>Surfaces dusted and wiped</li>

                            <li>Floors cleaned (vacuumed if provided)</li>

                            <li>Basic organization</li>

                            <li>All trash removed and bags replaced</li>

                          </ul>
                        </div>
                      </div>

                      <div class="col-12 col-lg-4">
                        <div class="supplemental-info__section">
                          <div class="supplemental-info__section-title">
                          Bathrooms
                          </div>
                          <ul class="list--bulleted">

                          <li>Showers and tubs scrubbed</li>

                          <li>Toilets scrubbed and wiped</li>

                          <li>Sinks, counters, and mirrors wiped</li>

                          <li>Floors cleaned</li>

                          </ul>
                        </div>
                      </div>

                      <div class="col-12 col-lg-4">
                        <div class="supplemental-info__section">
                          <div class="supplemental-info__section-title">
                          Kitchen
                          </div>
                          <ul class="list--bulleted">

                          <li>Dishes washed and dishwasher emptied</li>

                          <li>Counters, stovetop, and fridge exterior wiped</li>

                          <li>Microwave wiped (in/out)</li>

                          <li>Floors cleaned</li>

                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <a href="#" class="lightbox--dismiss js-lightbox-dismiss js-dismiss gone_close">
                  <i class="fa fa-times"></i>
                </a>
              </div>
            </div>
          </div>
          <!--popup code 2 for confirm Navigation-->
        </div>
      </div>
    </div>
  </div>

          <!--script for map autocomplete-->
          
<?php include_once( dirname(__FILE__) . '/template/api-functions/cleaning.php'); ?>





      

