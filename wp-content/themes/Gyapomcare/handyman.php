<?php

/*
Template Name: handy man
*/
get_header('frontpage');	
?>
<link href="<?php echo get_stylesheet_directory_uri();?>/assets/css/handy.css" rel="stylesheet" />
<link href="<?php echo get_stylesheet_directory_uri();?>/css/api.css" rel="stylesheet" />
<link href="<?php echo get_stylesheet_directory_uri();?>/css/handy.css" rel="stylesheet" />
<link href="<?php echo get_stylesheet_directory_uri();?>/css/marketing.css" rel="stylesheet" />
<link href="<?php echo get_stylesheet_directory_uri();?>/css/support_handyman.css" rel="stylesheet" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script></script>

  <div class="page_heading_container">
    <div class="task_template">   
    	<div class="col-md-12">
    	  <div class="page_heading_content"></div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
  <div class="page-container">
    <div class="task--default">
      <div class="page-content">
        <div class="col-md-12">
          <div class="fullwidth">           
            <?php if (have_posts()) : the_post(); ?>
    				<?php the_content(); ?>	
    			  <?php endif; ?>	  
				  </div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
  </div>
  <div class="hero mg-hero-container">
    <img class="hero__partner-logo" src="">
    <div class="mg-hero-text">
      <h1 class="mg-hero-title">Handyman</h1>
      <p class="hero-subtitle">Taskers can help with handyman tasks around your home.</p>
    </div>
  </div>

<div class="view--body">
<div class="container--wide">
<div class="row">
<div class="col-12 col-desk-9 mktg-templates-container">
<div class="panel panel--spaced panel--floating panel--isolated mktg-template-item">
<div class="media--object media--object--mobile-stacked media--object--stacked">
<div class="media--figure mktg-template-item--img-container">
<img class="mktg-template-item--img" src="http://www.goclean-service.com/wp-content/uploads/2017/01/25w.jpg">
</div>
<div class="media--content">
<h2 class="mktg-template-item--title"><a class="mktg-template-item--title-link" href="/m/handyman/general-handyman">General Handyman</a></h2>
<p class="mktg-template-item--subtitle">Jacks (and Jills) of all trades can handle most of your minor home repairs.</p>
<a class="btn btn-secondary btn-small js-template-cta" data-id="2042" data-name="General Handyman" href="#">Select Task</a>
</div>
</div>
</div>
<div class="panel panel--spaced panel--floating panel--isolated mktg-template-item">
<div class="media--object media--object--mobile-stacked media--object--stacked">
<div class="media--figure mktg-template-item--img-container">
<img class="mktg-template-item--img" src="http://www.goclean-service.com/wp-content/uploads/2017/01/25w.jpg">
</div>
<div class="media--content">
<h2 class="mktg-template-item--title"><a class="mktg-template-item--title-link" href="/m/handyman/assemble-furniture">Assemble Furniture</a></h2>
<p class="mktg-template-item--subtitle">Have a new desk or bookcase to put together? Taskers can assemble any of your furniture - quickly and professionally.</p>
<a class="btn btn-secondary btn-small js-template-cta" data-id="2030" data-name="Assemble Furniture" href="#">Select Task</a>
</div>
</div>
</div>
<div class="panel panel--spaced panel--floating panel--isolated mktg-template-item">
<div class="media--object media--object--mobile-stacked media--object--stacked">
<div class="media--figure mktg-template-item--img-container">
<img class="mktg-template-item--img" src="http://www.goclean-service.com/wp-content/uploads/2017/01/25w.jpg">
</div>
<div class="media--content">
<h2 class="mktg-template-item--title"><a class="mktg-template-item--title-link" href="/m/handyman/wall-mount-tv">TV Mounting</a></h2>
<p class="mktg-template-item--subtitle">We can properly mount your TV on the wall and leave you happily clicking the remote.</p>
<a class="btn btn-secondary btn-small js-template-cta" data-id="2044" data-name="TV Mounting" href="#">Select Task</a>
</div>
</div>
</div>
<div class="panel panel--spaced panel--floating panel--isolated mktg-template-item">
<div class="media--object media--object--mobile-stacked media--object--stacked">
<div class="media--figure mktg-template-item--img-container">
<img class="mktg-template-item--img" src="http://www.goclean-service.com/wp-content/uploads/2017/01/25w.jpg">
</div>
<div class="media--content">
<h2 class="mktg-template-item--title"><a class="mktg-template-item--title-link" href="/m/handyman/furniture-shopping-assembly">Furniture Shopping &amp; Assembly</a></h2>
<p class="mktg-template-item--subtitle">We'll pick up, deliver, and assemble any furniture you need.</p>
<a class="btn btn-secondary btn-small js-template-cta" data-id="2033" data-name="Furniture Shopping &amp; Assembly" href="#">Select Task</a>
</div>
</div>
</div>
<div class="panel panel--spaced panel--floating panel--isolated mktg-template-item">
<div class="media--object media--object--mobile-stacked media--object--stacked">
<div class="media--figure mktg-template-item--img-container">
<img class="mktg-template-item--img" src="http://www.goclean-service.com/wp-content/uploads/2017/01/25w.jpg">
</div>
<div class="media--content">
<h2 class="mktg-template-item--title"><a class="mktg-template-item--title-link" href="/m/handyman/heavy-lifting">Heavy Lifting</a></h2>
<p class="mktg-template-item--subtitle">Need to move something bulky or heavy? Professional Taskers are up to the challenge.</p>
<a class="btn btn-secondary btn-small js-template-cta" data-id="2036" data-name="Heavy Lifting" href="#">Select Task</a>
</div>
</div>
</div>
<div class="panel panel--spaced panel--floating panel--isolated mktg-template-item">
<div class="media--object media--object--mobile-stacked media--object--stacked">
<div class="media--figure mktg-template-item--img-container">
<img class="mktg-template-item--img" src="http://www.goclean-service.com/wp-content/uploads/2017/01/25w.jpg">
</div>
<div class="media--content">
<h2 class="mktg-template-item--title"><a class="mktg-template-item--title-link" href="/m/handyman/painting">Painting</a></h2>
<p class="mktg-template-item--subtitle">Whether it's an entire house, a room, or a wall, we can get it painted to your satisfaction.</p>
<a class="btn btn-secondary btn-small js-template-cta" data-id="2054" data-name="Painting" href="#">Select Task</a>
</div>
</div>
</div>
<div class="panel panel--spaced panel--floating panel--isolated mktg-template-item">
<div class="media--object media--object--mobile-stacked media--object--stacked">
<div class="media--figure mktg-template-item--img-container">
<img class="mktg-template-item--img" src="http://www.goclean-service.com/wp-content/uploads/2017/01/25w.jpg">
</div>
<div class="media--content">
<h2 class="mktg-template-item--title"><a class="mktg-template-item--title-link" href="/m/handyman/plumbing">Plumbing</a></h2>
<p class="mktg-template-item--subtitle">Hire expert Taskers to handle your plumbing problem.</p>
<a class="btn btn-secondary btn-small js-template-cta" data-id="2055" data-name="Plumbing" href="#">Select Task</a>
</div>
</div>
</div>
<div class="panel panel--spaced panel--floating panel--isolated mktg-template-item">
<div class="media--object media--object--mobile-stacked media--object--stacked">
<div class="media--figure mktg-template-item--img-container">
<img class="mktg-template-item--img" src="http://www.goclean-service.com/wp-content/uploads/2017/01/25w.jpg">
</div>
<div class="media--content">
<h2 class="mktg-template-item--title"><a class="mktg-template-item--title-link" href="/m/handyman/yard-work-removal">Yard Work &amp; Removal</a></h2>
<p class="mktg-template-item--subtitle">We can clean up your yard and remove any yard waste or rubbish.</p>
<a class="btn btn-secondary btn-small js-template-cta" data-id="2061" data-name="Yard Work &amp; Removal" href="#">Select Task</a>
</div>
</div>
</div>
<div class="panel panel--spaced panel--floating panel--isolated mktg-template-item">
<div class="media--object media--object--mobile-stacked media--object--stacked">
<div class="media--figure mktg-template-item--img-container">
<img class="mktg-template-item--img" src="http://www.goclean-service.com/wp-content/uploads/2017/01/25w.jpg">
</div>
<div class="media--content">
<h2 class="mktg-template-item--title"><a class="mktg-template-item--title-link" href="/m/handyman/hang-pictures">Hang Pictures</a></h2>
<p class="mktg-template-item--subtitle">Need help hanging all those pictures? Taskers will hang pictures and art, ensuring they are level and securely mounted.</p>
<a class="btn btn-secondary btn-small js-template-cta" data-id="2046" data-name="Hang Pictures" href="#">Select Task</a>
</div>
</div>
</div>
<div class="panel panel--spaced panel--floating panel--isolated mktg-template-item">
<div class="media--object media--object--mobile-stacked media--object--stacked">
<div class="media--figure mktg-template-item--img-container">
<img class="mktg-template-item--img" src="http://www.goclean-service.com/wp-content/uploads/2017/01/25w.jpg">
</div>
<div class="media--content">
<h2 class="mktg-template-item--title"><a class="mktg-template-item--title-link" href="/m/handyman/shelf-mounting">Mounting &amp; Installation</a></h2>
<p class="mktg-template-item--subtitle">Mounting &amp; Installation</p>
<a class="btn btn-secondary btn-small js-template-cta" data-id="2276" data-name="Mounting &amp; Installation" href="#">Select Task</a>
</div>
</div>
</div>
<div class="panel panel--spaced panel--floating panel--isolated mktg-template-item">
<div class="media--object media--object--mobile-stacked media--object--stacked">
<div class="media--figure mktg-template-item--img-container">
<img class="mktg-template-item--img" src="http://www.goclean-service.com/wp-content/uploads/2017/01/25w.jpg">
</div>
<div class="media--content">
<h2 class="mktg-template-item--title"><a class="mktg-template-item--title-link" href="/m/handyman/light-installation">Light Installation</a></h2>
<p class="mktg-template-item--subtitle">From replacing light bulbs to installing light fixtures, capable Taskers can shed some light on your space.</p>
<a class="btn btn-secondary btn-small js-template-cta" data-id="2043" data-name="Light Installation" href="#">Select Task</a>
</div>
</div>
</div>
<div class="panel panel--spaced panel--floating panel--isolated mktg-template-item">
<div class="media--object media--object--mobile-stacked media--object--stacked">
<div class="media--figure mktg-template-item--img-container">
<img class="mktg-template-item--img" src="http://www.goclean-service.com/wp-content/uploads/2017/01/25w.jpg">
</div>
<div class="media--content">
<h2 class="mktg-template-item--title"><a class="mktg-template-item--title-link" href="/m/handyman/need-electrical-work-done">Electrical Work</a></h2>
<p class="mktg-template-item--subtitle">Professional Taskers can handle electrical work for you.</p>
<a class="btn btn-secondary btn-small js-template-cta" data-id="2064" data-name="Electrical Work" href="#">Select Task</a>
</div>
</div>
</div>
<div class="panel panel--spaced panel--floating panel--isolated mktg-template-item">
<div class="media--object media--object--mobile-stacked media--object--stacked">
<div class="media--figure mktg-template-item--img-container">
<img class="mktg-template-item--img" src="http://www.goclean-service.com/wp-content/uploads/2017/01/25w.jpg">
</div>
<div class="media--content">
<h2 class="mktg-template-item--title"><a class="mktg-template-item--title-link" href="/m/handyman/carpentry-construction">Carpentry &amp; Construction</a></h2>
<p class="mktg-template-item--subtitle">Need something built? Expert Taskers can help with carpentry and construction work.</p>
<a class="btn btn-secondary btn-small js-template-cta" data-id="2208" data-name="Carpentry &amp; Construction" href="#">Select Task</a>
</div>
</div>
</div>

</div>
<div class="col-12 col-desk-3">
<div class="mg-sidebar--panel">
<h4 class="mg-sidebar--title">How Iserve Works</h4>
<ul class="mg-sidebar--list">
<li class="mg-sidebar--education-item" data-index="1">
<h4 class="u-regular">Select your Task</h4>
<p>Choose from one of the tasks recommended, or create your own custom task.</p>
</li>
<li class="mg-sidebar--education-item" data-index="2">
<h4 class="u-regular">Describe your Task</h4>
<p>Describe your tasks so that our qualified Taskers can get a clear idea of the details.</p>
</li>
<li class="mg-sidebar--education-item" data-index="3">
<h4 class="u-regular">Choose a Tasker</h4>
<p>Select from a variety of taskers with specific specialities and price points.</p>
</li>
</ul>
</div>

<div class="mg-sidebar--panel">
<h4 class="mg-sidebar--title">Hear What People Are Saying</h4>
<ul class="mg-sidebar--list">
<li class="mg-sidebar--quote">
<div class="media--object media--object--stacked">
<div class="media--figure">
<img class="mg-quote--img1" src="http://www.goclean-service.com/wp-content/uploads/2017/01/25w.jpg">
<div class="u-hidden--md u-hidden--lg muted u-regular mg-quote--name">Wendell A.</div>
</div>
<div class="media--content">
<p class="mg-quote--text">Alfonso was great! He knew the best way to mount the wire in the drywall and I feel confident that the curtain wire he reinstalled is secure and won't come crashing down again! He was a pleasure to have around and I would hire him again. Thanks, Alfonso!</p>
<p class="u-hidden--sm muted u-regular mg-quote--name">– Wendell A.</p>
</div>
</div>
</li>
<li class="mg-sidebar--quote">
<div class="media--object media--object--stacked">
<div class="media--figure">
<img class="mg-quote--img1" src="http://www.goclean-service.com/wp-content/uploads/2017/01/25w.jpg">
<div class="u-hidden--md u-hidden--lg muted u-regular mg-quote--name">Marcia B.</div>
</div>
<div class="media--content">
<p class="mg-quote--text">Absolutely wonderful job by Jasper installing our baby gate; he was here early, got right to work, and was done in less than an hour. Money well spent!</p>
<p class="u-hidden--sm muted u-regular mg-quote--name">– Marcia B.</p>
</div>
</div>
</li>
<li class="mg-sidebar--quote">
<div class="media--object media--object--stacked">
<div class="media--figure">
<img class="mg-quote--img1" src="http://www.goclean-service.com/wp-content/uploads/2017/01/25w.jpg">
<div class="u-hidden--md u-hidden--lg muted u-regular mg-quote--name">Sacha W.</div>
</div>
<div class="media--content">
<p class="mg-quote--text">Aubrey came equipped with a plethora of tools and supplies, and knew exactly what to use to get the job done. She worked with me to make sure I got exactly what I wanted (we were hanging invisible book shelves, as well as an accessory rack).</p>
<p class="u-hidden--sm muted u-regular mg-quote--name">– Sacha W.</p>
</div>
</div>
</li>
</ul>
</div>

</div>
</div>
</div>
</div>


<div class="mktg-nav-container">
<div class="container--wide">
<div class="row">
<div class="col-12 col-lg-9 col--centered">
<h3 class="mktg-nav--title u-light">Search Other Tasks</h3>
<div class="mktg-nav">
<a class="mktg-nav--link" href="/m/handyman-direct">
<i class="mktg-nav--icon ss-hammer ss-icon"></i>
Hang your shelves
</a>
<a class="mktg-nav--link" href="/m/moving-help-direct">
<i class="mktg-nav--icon ss-icon ss-truck"></i>
Haul your stuff
</a>
<a class="mktg-nav--link" href="/m/cleaning-direct">
<i class="mktg-nav--icon ss-icon ss-spray"></i>
Clean your house
</a>
<a class="mktg-nav--link" href="/m/delivery-shopping-direct">
<i class="mktg-nav--icon ss-bag ss-icon"></i>
Run your errands
</a>
<a class="mktg-nav--link" href="/m/furntiture-assembly-direct">
<i class="mktg-nav--icon ss-furniture ss-icon"></i>
Assemble your IKEA
</a>
<a class="mktg-nav--link" href="/m/lift-and-shift-furniture-direct">
<i class="mktg-nav--icon ss-furniture ss-icon"></i>
Carry your couch
</a>
</div>
</div>
</div>
</div>
</div>

   <?php get_footer(); ?>