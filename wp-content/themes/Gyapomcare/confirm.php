<?php

/*
Template Name:   confirm page
*/
get_header('frontpage');	
 if(!$_SESSION['first_name']){  
    header("Location: ". $object_name['webUrl'] . "book-a-service/");
}
?>

<link href="<?php echo get_stylesheet_directory_uri();?>/css/api.css" rel="stylesheet" />
<link href="<?php echo get_stylesheet_directory_uri();?>/assets/css/confirm.css" rel="stylesheet" />
<div id="card_load"  style="display:none">
<span class="loader_card"><span class="loader-inner"></span></span>
</div>
<div class="page_heading_container">
    <div class="task_template">   
		<div class="col-md-12">
		    <div class="page_heading_content"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="page-container">
    <div class="task--default">   
            <div class="page-content">
                <div class="col-md-12">
                    <div class="fullwidth">           
                        <?php if (have_posts()) : the_post(); ?>
            				<?php the_content(); ?>	
            			<?php endif; ?>	  
				    </div>
                </div>
                <div class="clear"></div>
            </div>
    </div>
</div>
<div class="js-progress-container">
    <div class="build-progress" data-pie-id="269">
        <div class="build-progress__container container--wide">
            <div class="build-progress__step js-step" data-page="form" data-state="previous">
                <i class="ss-pencil"></i>
                <span>1.</span>
                <span>Fill Out Task Details</span>
            </div>

            <div class="build-progress__step js-step" data-page="recommendations" data-state="previous">
                <i class="ss-smile"></i>
                <span>2.</span>
                <span>View Taskers &amp; Prices</span>
            </div>

            <div class="build-progress__step js-step" data-page="confirm" data-state="current">
                <i class="fa fa-check"></i>
                <span>3.</span>
                <span>Confirm &amp; Book</span>
            </div>

        </div>
    </div>
</div>

<div class="js-trust-banner-container">
    <div class="build-trust-banner box-no-padding" data-pie-id="297">
        <div class="build-trust-banner__container container--wide">
            <img alt="Trust badge" class="build-trust-banner__badge trust" src="../wp-content/uploads/2017/11/lock_icon.png">
            <div class="build-trust-banner__title">
                <strong>Trust &amp; Safety Guarantee</strong>
            </div>
            <a class="build-trust-banner__info u-hidden--sm u-hidden--md u-hidden--lscp js-tt-trigger" data-tt-contenttemplate="build.trustBannerTooltip" data-tt-placement="b" href="#">
               
            </a>
        </div>
    </div>
</div>

<div class="js-child-container">
    <div class="page-loader"></div>
    <div data-pie-id="1485">
        <div class="view--build-main view--body confirmation__main-body">
            <div class="container--medium confirmation--main build-container confirmation__build-container">
                <div class="panel confimationB-container">
                    <div class="js-job-header">
                        <div data-pie-id="1548">
                            <div class="confirmation--job-info confirmation--job-header row">
                                <div class="col-12">
                                    <div class="confirmation__job-info-header confirmation--section confirmation--section">
                                        <div class="confirmation__job-info-title">
                                            <div class="h2"></div>
                                        </div>
                                        <div class="confirmation__rate-plus-savings">
                                            <div class="confirmation__hourly-rate">
                                                <span class="confirmation__formatted-hourly-rate taskerrate"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="js-payment-info">
                        <div class="confirmation--section confirmation__bordered-top" data-pie-id="1563">

                            <div class="button_system">
                                <div class="cash_payment">
                                <button class="cash_button coloractive">CASH</button>
                                </div>
                                <div class="card_payment">
                                <button class="card_button">CARD</button>
                                </div>

                            </div>                            
													 
                            <div class="cardpay cardpayments">
							<label class="confirmation__section-label" style="text-align:center;padding:15px">You are charged only after your task is completed.</label>
							<div id="show_all_cards"> 
                                </div>
                             <p id="new_card_title">ADD NEW CARD</p>	
							<!--- CARD LOADING ANIMATION-->
							<!--

Follow me on
Dribbble: https://dribbble.com/supahfunk
Twitter: https://twitter.com/supahfunk
Codepen: http://codepen.io/supah/

This example is just for fun.
I realized it for the dailyui challenge
https://dailyui.co/

Fork it if you want, it's free, but I apreciate credits or a retweet

Enjoy :)

-->

<div class="checkout">
  <div class="credit-card-box">
    <div class="flip">
      <div class="front">
        <div class="chip"></div>
        <div class="logo">
          <svg version="1.1" id="visa" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
               width="47.834px" height="47.834px" viewBox="0 0 47.834 47.834" style="enable-background:new 0 0 47.834 47.834;">
            <g>
              <g>
                <path d="M44.688,16.814h-3.004c-0.933,0-1.627,0.254-2.037,1.184l-5.773,13.074h4.083c0,0,0.666-1.758,0.817-2.143
                         c0.447,0,4.414,0.006,4.979,0.006c0.116,0.498,0.474,2.137,0.474,2.137h3.607L44.688,16.814z M39.893,26.01
                         c0.32-0.819,1.549-3.987,1.549-3.987c-0.021,0.039,0.317-0.825,0.518-1.362l0.262,1.23c0,0,0.745,3.406,0.901,4.119H39.893z
                         M34.146,26.404c-0.028,2.963-2.684,4.875-6.771,4.875c-1.743-0.018-3.422-0.361-4.332-0.76l0.547-3.193l0.501,0.228
                         c1.277,0.532,2.104,0.747,3.661,0.747c1.117,0,2.313-0.438,2.325-1.393c0.007-0.625-0.501-1.07-2.016-1.77
                         c-1.476-0.683-3.43-1.827-3.405-3.876c0.021-2.773,2.729-4.708,6.571-4.708c1.506,0,2.713,0.31,3.483,0.599l-0.526,3.092
                         l-0.351-0.165c-0.716-0.288-1.638-0.566-2.91-0.546c-1.522,0-2.228,0.634-2.228,1.227c-0.008,0.668,0.824,1.108,2.184,1.77
                         C33.126,23.546,34.163,24.783,34.146,26.404z M0,16.962l0.05-0.286h6.028c0.813,0.031,1.468,0.29,1.694,1.159l1.311,6.304
                         C7.795,20.842,4.691,18.099,0,16.962z M17.581,16.812l-6.123,14.239l-4.114,0.007L3.862,19.161
                         c2.503,1.602,4.635,4.144,5.386,5.914l0.406,1.469l3.808-9.729L17.581,16.812L17.581,16.812z M19.153,16.8h3.89L20.61,31.066
                         h-3.888L19.153,16.8z"/>
              </g>
            </g>
          </svg>
        </div>
        <div class="number"></div>
        <div class="card-holder">
          <label>Card holder</label>
          <div></div>
        </div>
        <div class="card-expiration-date">
          <label>Expires</label>
          <div></div>
        </div>
      </div>
      <div class="back">
        <div class="strip"></div>
        <div class="logo">
          <svg version="1.1" id="visa" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
               width="47.834px" height="47.834px" viewBox="0 0 47.834 47.834" style="enable-background:new 0 0 47.834 47.834;">
            <g>
              <g>
                <path d="M44.688,16.814h-3.004c-0.933,0-1.627,0.254-2.037,1.184l-5.773,13.074h4.083c0,0,0.666-1.758,0.817-2.143
                         c0.447,0,4.414,0.006,4.979,0.006c0.116,0.498,0.474,2.137,0.474,2.137h3.607L44.688,16.814z M39.893,26.01
                         c0.32-0.819,1.549-3.987,1.549-3.987c-0.021,0.039,0.317-0.825,0.518-1.362l0.262,1.23c0,0,0.745,3.406,0.901,4.119H39.893z
                         M34.146,26.404c-0.028,2.963-2.684,4.875-6.771,4.875c-1.743-0.018-3.422-0.361-4.332-0.76l0.547-3.193l0.501,0.228
                         c1.277,0.532,2.104,0.747,3.661,0.747c1.117,0,2.313-0.438,2.325-1.393c0.007-0.625-0.501-1.07-2.016-1.77
                         c-1.476-0.683-3.43-1.827-3.405-3.876c0.021-2.773,2.729-4.708,6.571-4.708c1.506,0,2.713,0.31,3.483,0.599l-0.526,3.092
                         l-0.351-0.165c-0.716-0.288-1.638-0.566-2.91-0.546c-1.522,0-2.228,0.634-2.228,1.227c-0.008,0.668,0.824,1.108,2.184,1.77
                         C33.126,23.546,34.163,24.783,34.146,26.404z M0,16.962l0.05-0.286h6.028c0.813,0.031,1.468,0.29,1.694,1.159l1.311,6.304
                         C7.795,20.842,4.691,18.099,0,16.962z M17.581,16.812l-6.123,14.239l-4.114,0.007L3.862,19.161
                         c2.503,1.602,4.635,4.144,5.386,5.914l0.406,1.469l3.808-9.729L17.581,16.812L17.581,16.812z M19.153,16.8h3.89L20.61,31.066
                         h-3.888L19.153,16.8z"/>
              </g>
            </g>
          </svg>

        </div>
        <div class="ccv">
          <label>CCV</label>
          <div></div>
        </div>
      </div>
    </div>
  </div>
  <form class="form" autocomplete="off" novalidate id="credit_card_form">
    <fieldset>
      <label for="card-number">Card Number* (* is mandatory to be filled)</label>
      <input type="text" id="card-number" class="validation_class input-cart-number restrict_values" maxlength="4" name="num_field" id="creditCard_card_number"/>
      <input type="text" id="card-number-1" class="validation_class input-cart-number restrict_values" maxlength="4" name="num_field2" id="creditCard_card_number2"/>
      <input type="text" id="card-number-2" class="validation_class input-cart-number restrict_values" maxlength="4" name="num_field3" id="creditCard_card_number3"/>
      <input type="text" id="card-number-3" class="validation_class input-cart-number restrict_values" maxlength="4" name="num_field4" id="creditCard_card_number4"/> 
    </fieldset>
    <fieldset>
      <label for="card-holder">Card holder*</label>
      <input type="text" id="card-holder" name="cardholder_name" class="validation_class"/>
    </fieldset>
    <fieldset class="fieldset-expiration">
      <label for="card-expiration-month">Expiration date*</label>
      <div class="select validation_class">
        <select id="card-expiration-month" name="creditCard_expiration_month" >
          <option disabled="" selected="" style="display:none" value="">MM</option>
          <option>01</option>
          <option>02</option>
          <option>03</option>
          <option>04</option>
          <option>05</option>
          <option>06</option>
          <option>07</option>
          <option>08</option>
          <option>09</option>
          <option>10</option>
          <option>11</option>
          <option>12</option>
        </select>
      </div>
      <div class="select validation_class">     
        <select id="card-expiration-year" name="creditCard_expiration_year"  id="creditCard_expiration_year">
          <option disabled="" selected="" style="display:none" value="">YYYY</option>
          <option>2016</option>
          <option>2017</option>
          <option>2018</option>
          <option>2019</option>
          <option>2020</option>
          <option>2021</option>
          <option>2022</option>
          <option>2023</option>
          <option>2024</option>
          <option>2025</option>
        </select>
      </div>
    </fieldset>
    <fieldset class="fieldset-ccv">
      <label for="card-ccv">CCV*</label>
      <input type="text" id="card-ccv" maxlength="3" name="creditCard_number" class="validation_class restrict_values"/>
    </fieldset>
    <button class="btn" type="submit" id="add_card_button"><i class="fa fa-lock"></i> Save</button> 
	<div id="addcard" style="display:none">addcard</div>
	<p id="card_error_success"></p>
  </form>
</div>
							<!--LOADING ANIMATION -- ENDS HERE>
                                 <!-- add card  
							
                                <div class="card_full_div">
                                    <span class="card_type_block">Visa</span>
                                    <span class="card_number_full">**** **** ****4242 -  Exp: 4/2024</span>
                                    <input onchange="cardChange(&quot;card_1A5zD7IOwCmEkZU9kUH3FBg7&quot;)" type="radio" name="card_selection" id="card_1A5zD7IOwCmEkZU9kUH3FBg7" class="card_type_id_radio">
                                    <span class="font_mes" style="float: right;"><i class="fa fa-trash delete_move" aria-hidden="true"></i></span>
                                </div>--> 
  						
                              <!--  <div class="confirmation--payment-form ">
                                    <div class="confirmation--payment-cancel-container">
                                        <a class="confirmation--payment-cancel js-cancel-container js-toggle-edit is-hidden" href="#" tabindex="-1">Cancel</a>
                                    </div>
                                    <div class="row row--guttered">
                                        <div class="col-12">
                                            <div class="row row--guttered row--thin">
                                                <div class="col-12">
                                                    <label class="confirmation__section-label">You are charged only after your task is completed.</label>
                                                </div>
												  
												<form  method="post">  
                                                <div class="col-12 col-lg-4 js-error-target" data-key="creditCard.card_number">
                                                    <label class="confirmation__field-label" for="creditCard.card_number">Credit Card</label>
                                                    <input class="confirmation--form-height" id="creditCard_card_number" name="field" placeholder="#### #### #### ####" type="text">
                                                </div>
                                                <div class="col-12 col-lg-3 js-error-target" data-key="creditCard.expiration_month creditCard.expiration_year">
                                                    <label class="confirmation__field-label" for="creditCard.expiration_month">Expiration</label>
                                                    <div class="row row--thin">
                                                        <div class="col-5 month-field">
                                                            <select  name="creditCard_expiration_month">
                                                                <option disabled="" selected="" style="display:none" value="">MM</option>
                                                                <option value="01">01</option>
                                                                <option value="02">02</option>
                                                                <option value="03">03</option>
                                                                <option value="04">04</option>
                                                                <option value="05">05</option>
                                                                <option value="06">06</option>
                                                                <option value="07">07</option>
                                                                <option value="08">08</option>
                                                                <option value="09">09</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div>   
                                                        <div class="col-7">
                                                            <select name="creditCard_expiration_year">
                                                                <option disabled="" selected="" style="display:none" value="">YYYY</option>
                                                                <option value="2017">2017</option>
                                                                <option value="2018">2018</option>
                                                                <option value="2019">2019</option>
                                                                <option value="2020">2020</option>
                                                                <option value="2021">2021</option>
                                                                <option value="2022">2022</option>
                                                                <option value="2023">2023</option>
                                                                <option value="2024">2024</option>
                                                                <option value="2025">2025</option>
                                                                <option value="2026">2026</option>
                                                                <option value="2027">2027</option>
                                                                <option value="2028">2028</option>
                                                                <option value="2029">2029</option>
                                                                <option value="2030">2030</option>
                                                                <option value="2031">2031</option>
                                                                <option value="2032">2032</option>
                                                                <option value="2033">2033</option>
                                                                <option value="2034">2034</option>
                                                                <option value="2035">2035</option>
                                                                <option value="2036">2036</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-lg-2 js-error-target" data-key="creditCard.cvv">
                                                    <label class="confirmation__field-label" for="creditCard.cvv">CVV Code</label>
                                                    <input class="confirmation--form-height" id="creditCard_cvv" name="creditCard_number" type="text">
                                                </div>
                                                <div class="col-12 col-lg-3 js-error-target" data-key="creditCard.postal_code">
                                                    <button type="submit" id="add_card_button" >Pay</button>
                                                </div>
												<button onclick="submit()" style="display: none;">sdf</button>
												</form>
												<p id="card_error_success"></p>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="js-contact-info">
                        <div class="confirmation--contact-section" data-pie-id="1552">
                            <!--<div class="row row--guttered">
                                <div class="col-12 col-lg-4 js-error-target" data-key="job.phone_number">
                                    <label class="confirmationB__field-label" for="job.phone_number">Mobile Phone</label>
                                    <input id="job.phone_number" name="job.phone_number" type="text">
                                </div>
                            </div>-->
                        </div>
                    </div>
                    <div class="js-job-info"><div data-pie-id="1592">

                        <div class="confirmation--job-info row">
                            <div class="col-12">
                                <div class="row row--thin">
                                    <div class="confirmation--section confirmation--section confirmation__bordered-top col-12">

                                        <div class="row row-wide-gutter">
                                            <div class="col-12 col-lg-6">
                                                <label class="confirmation__section-label">Date &amp; Time</label>
                                                <div id="selected_time" class="confirmation__section-value confirmation--job-info-time"></div>
                                            </div>
                                            <div class="col-12 col-lg-6">
                                                <div class="confirmation__job-info-invitee media--object js-invitee-row">

                                                    <div class="media--figure u-hidden--sm u-hidden--md">
                                                        <img class="avatar-new-36 confirmation__invitee-avatar" src="../wp-content/uploads/2017/04/user.png" id="taskerimg" src="">
                                                    </div>
                                                    <div class="media--content">
                                                        <label class="confirmation__section-label Provider_category">Tasker</label>
                                                        <span class="confirmation__section-value taskername Provider_name">Quick Assign</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 confirmation__location">
                                                <label class="confirmation__section-label">Task Location</label>
                                                <div class="confirmation__section-value taskerlocation"></div>
                                            </div>
                                        </div>

                                        <div class="row row--thin">
                                            <div class="col-12 confirmation--job-description">
                                                <label class="confirmation__section-label">Task Description</label>
                                                <div class="confirmation__section-value js-description-container"><p class="tasker_description">testing content from iserve</p></div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--<div class="col-12 u-align--center">
                        <a class="link-secondary js-edit" href="/tasks/a/hire?uuid=ddb060ec-1c58-4280-ba4c-a73a2b4c1fc4" tabindex="-1">
                            <i class="fa fa-cog"></i>
                            <span>Modify Task</span>
                        </a>
                    </div>-->
                </div>
                <div class="confirmation--section confirmation--section floating-cta">
                <div class="col-12 col-lg-4 col--centered button_moving">
									<p id="card_select_error"></p>
                                    <button class="btn btn-primary confirm-book-btn btn-sm-block js-submit continue_blue" id="bookingstart">Confirm &amp; Book</button>
                                </div>
                    <!--<div class="js-floating-cta">
                        <div data-pie-id="1619" style="height: 122px;">
                            <div class="row confirmation--cta-container floating-cta is-stuck">
                                <div class="col-12 col-lg-4 col--centered">
                                    <button class="btn btn-primary confirm-book-btn btn-sm-block js-submit continue_blue">Confirm &amp; Book</button>
                                </div>
                                <a class="js-reveal-promoCodeField hidden-when-stuck u-align--center js-scroll-promo-code " href="#">Have a Promo Code?</a>
                            </div>
                        </div>
                    </div>-->
                    <div class="js-promo-code">
                        <div data-pie-id="1600">
                            <div class="row row--guttered">
                                <div class="col-12 col-lg-6 col--centered js-error-target" data-key="job.promotion_code">
                                    <div class="u-align--center">
                                        <a class="js-reveal-promoCodeField promoCodetext" >Have a Promo Code?</a>
                                    </div>
                                    <div class="col-12 col-lg-6 col--centered js-error-target" data-key="job.promotion_code">
                                        <div class="u-align--center">
                                            <a class="js-reveal-promoCodeField is-hidden promoCodetext" >Have a Promo Code?</a>
                                        </div>
                                        <input class="js-promo-code-field" id="promotion_codes" name="promotion_codes" placeholder="Insert Code" tabindex="-1" type="text">
                                        
                                        
                                        <div class="confirmation__promo-description js-description-container ">
<!--                                            <i class="ss-icon ss-checkmark"></i>-->
                                           
                                        </div>
                                        
                                        
                                    </div>

                                    <input class="js-promo-code-field is-hidden" id="promotion_codes" name="promotion_codes" placeholder="Insert Code" tabindex="-1" type="text">
                                        <div class="text-center">
                                            <p><span class="js-description"></span></p> 
                                            <p><span class="js-errors"></span></p> 
                                        </div>
                                    
                                        <div id="page_loader_parent" style="display:none">
                                            <div id="spinner" class="active">
                                                <span id="first" class="ball"></span>
                                                <span id="second" class="ball"></span>
                                                <span id="third" class="ball"></span>
                                            </div>
                                        </div>
                                    
                                        <div class="apply_button">
                                            <button id="PromoCode" class="Apply_bu">Apply</button> 
                                        </div>
                                    <div class="confirmation__promo-description js-description-container is-hidden">
                                        <i class="ss-icon ss-checkmark"></i>
                                        <span class="js-description"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row confirmation__cta-help">
                <div class="col-11 col--centered">
         

                    <p class="muted"> <b>You are charged only after your task is completed ,</b> Tasks have a one hour minimum . If you cancel a ASAP booking 5 minutes after a provider has accepted a booking , you will be charged a cancellation fee and if you cancel a scheduled booking inside 24 hours from the time of the booking you will be charged a cancellation fee.”
</p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<!--popup for cancel a task-->



<div class="lightbox--container cancel_card" data-pie-id="" style="display: block;">
   <div class="lightbox--background js-lightbox-bg js-dismiss gone_class"></div>
	<div class="lightbox--internal">
		  <div class="lightbox--content-wrapper">
			    <div class="lightbox--title js-lightbox-title">Delete this Card</div>
			    <div class="lightbox--content js-lightbox-content">
				    <div class="page-loader"></div>
				    <div data-pie-id="726" class="confirm-lightbox">
				        <div class="row confirm-lightbox--content-row">
				            <div class="col-12 centy">
				                <span>Are you sure ? You want to delete this card  </span>
				            </div>
				        </div>
				        <div class="row row--guttered confirm-lightbox--button-row row--thin">
				            <div class="col-12 centy">
				                <button class="js-confirm btn btn-primary close" id="delete_card" style="width:48%;display:inline-block;float:left">OK</button>  
				                <button class="js-deny btn close" style="width:43%;display:inline-block;float:left;color:#000">Cancel</button>
				            </div>
				        </div>
				    </div>
			    </div>
			    <a href="#" class="lightbox--dismiss js-lightbox-dismiss js-dismiss close">
			        <i class="fa fa-times"></i>
			    </a>
		  </div>
	</div>
</div>


<!--popup for cancel a task-->

<!--popup for booking confirmation a task-->

<div class="lightbox--container booking_confirmation" data-pie-id="" style="display: none;">
   <div class="lightbox--background js-lightbox-bg js-dismiss close"></div>
	<div class="lightbox--internal confirm-box">
		  <div class="lightbox--content-wrapper">
			    <div class="lightbox--title js-lightbox-title" style="green">Booking Confirmed</div>
			    <div class="lightbox--content js-lightbox-content">
				    <div class="page-loader"></div>
				    <div data-pie-id="726" class="confirm-lightbox">
				        <div class="row confirm-lightbox--content-row">
				            <div class="col-12 centy">
				                <span>Click ok to view the booking </span>
				            </div>
				        </div>
				        <div class="row row--guttered confirm-lightbox--button-row row--thin">
				            <div class="col-12 centy">
				                <a href="/dashboard/"><button class="js-confirm btn btn-primary close" style="width:100%;display:inline-block;float:left">OK</button></a> 
				            </div>
				        </div>
				    </div>
			    </div>
			    <a href="#" class="lightbox--dismiss js-lightbox-dismiss js-dismiss close">
			        <i class="fa fa-times"></i>
			    </a>
		  </div>
	</div>
</div>

<style type="text/css">
    .js-description{
        right: 1em;
        font-size: 1.125rem;
        color: #3b8e20;
        line-height: 44px;
    }
    .js-errors{
        right: 1em;
        font-size: 1.125rem;
        color: #ce0617;
        line-height: 44px;
    }
    #promotion_codes{
        text-align: center;
    }
    
    .confirmation__invitee-avatar{
        width: 60px;
        height:60px;
        border-radius: 50%;
    }
    .cardpayments{
        display: none;
    }
    
    .colordeactive{
       background-color: #464646!important;    
    }
    button.cash_button{
      border: 0;   
    }
    
    button.card_button {
    border: 0;
}
.confirm-lightbox--button-row button {
    font-size: 14px !important;
}
    
    /*--------------------------spinner--------------------------------*/
#spinner.active {
    display: block;
}
div#page_loader_parent {
    position: fixed;
    width: 100%;
    height: 100%;
    z-index: 99;
    background: rgba(0,0,0,0.7);
    top: 0px;
    left: 0px;
    border-radius: 8px
}
#spinner {
    display: none;
    position: absolute;
    height: 60px;
    width: 60px;
    top: 40%;
    left: 48%;
    z-index: 1;
}
.ball {
    position: absolute;
    display: block;
    background-color: #feffff;
    left: 24px;
    width: 12px;
    height: 12px;
    border-radius: 6px;
}
div#spinner:after {
    content: 'verifying promocode...';
    color: #fff;
    position: absolute;
    bottom: -36px;
    width: 171px;
    left: -40px;
}
#first {
    -webkit-animation-timing-function: cubic-bezier(0.5, 0.3, 0.9, 0.9);
    -webkit-animation-name: rotate; 
    -webkit-animation-duration: 2s; 
    -webkit-animation-iteration-count: infinite;
    -webkit-transform-origin: 6px 30px;
    -moz-animation-timing-function: cubic-bezier(0.5, 0.3, 0.9, 0.9);
    -moz-animation-name: rotate; 
    -moz-animation-duration: 2s; 
    -moz-animation-iteration-count: infinite;
    -moz-transform-origin: 6px 30px;

}
#second {
    -webkit-animation-timing-function: cubic-bezier(0.5, 0.5, 0.9, 0.9);
    -webkit-animation-name: rotate; 
    -webkit-animation-duration: 2s; 
    -webkit-animation-iteration-count: infinite;
    -webkit-transform-origin: 6px 30px;
	  -moz-animation-timing-function: cubic-bezier(0.5, 0.5, 0.9, 0.9);
    -moz-animation-name: rotate; 
    -moz-animation-duration: 2s; 
    -moz-animation-iteration-count: infinite;
    -moz-transform-origin: 6px 30px;
}
#third {
    -webkit-animation-timing-function: cubic-bezier(0.5, 0.7, 0.9, 0.9);
    -webkit-animation-name: rotate; 
    -webkit-animation-duration: 2s; 
    -webkit-animation-iteration-count: infinite;
    -webkit-transform-origin: 6px 30px;
	  -moz-animation-timing-function: cubic-bezier(0.5, 0.7, 0.9, 0.9);
    -moz-animation-name: rotate; 
    -moz-animation-duration: 2s; 
    -moz-animation-iteration-count: infinite;
    -moz-transform-origin: 6px 30px;
}
@-webkit-keyframes rotate {
  0% {
    -webkit-transform: rotate(0deg) scale(1);
  }
  100% { 
    -webkit-transform: rotate(1440deg) scale(1); 
  }
}​

@-moz-keyframes rotate {
  0% {
    -moz-transform: rotate(0deg) scale(1);
  }
  100% { 
    -moz-transform: rotate(1440deg) scale(1); 
  }
}
/*PAGE LOADER CSS ENDING--------------*/    
</style>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
$(".cancel_card").hide();

$(".apply_button").hide();
$(document).on('click', '.delete_move', function(){ 
  $(".cancel_card").show();  
});
$(".gone_class").click(function(){
  $(".cancel_card").hide();  
});
$(".close").click(function(){
  $(".cancel_card").hide();  
});
$(".promoCodetext").click(function(){
  $(".apply_button").show();  
});
$("input#creditCard_card_number").keydown(function(event){
        var $this = $(this);
		console.log($this.val().length);
          if ( event.keyCode < 106 && event.keyCode > 95 && $this.val().length<19) {
          if ((($this.val().length+1) % 5)==0){
            $this.val($this.val() + " ");
        }
        }
		else if(event.keyCode < 90 && event.keyCode>65 && $this.val().length>19 ){
               $this.val($this.val() + " ");
              
		}
  
    });
	

</script>


<?php include_once( dirname(__FILE__) . '/template/api-functions/confirm.php'); ?>


