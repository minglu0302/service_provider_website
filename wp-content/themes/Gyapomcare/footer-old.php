<footer class="footer-container ng-scope loader-active">
<div class="footer-not-authorized ng-scope">
        <div class="footer-not-authorized-column">
            <a href="<?php echo site_url(); ?>/privacy-policy/" class="footer-not-authorized__link">Privacy</a>
            <a href="<?php echo site_url(); ?>/terms-and-condition/" class="footer-not-authorized__link">Terms &amp; Condition</a>
        </div>
        <div class="footer-not-authorized-column">
            <a href="<?php echo site_url(); ?>"><div class="footer-not-authorized__icon"></div></a>
        </div>
        <div class="footer-not-authorized-column">
            <a href="#" class="footer-not-authorized__social-link">
                <span class="icon icon--inst"></span>
            </a>
            <a href="#" class="footer-not-authorized__social-link">
                <span class="icon icon--twitter"></span>
            </a>
            <a href="#" class="footer-not-authorized__social-link">
                <span class="icon icon--facebook"></span>
            </a>
            <span class="footer-not-authorized__social">Copyright © 2016 Addison Lee</span>
        </div>
    </div>
</footer>
</div>
<style>
    
    label.valid {
    background: url('<?php echo get_stylesheet_directory_uri();?>/images/tick1.png') 50% 50%  no-repeat;
    display: block;
    width: 42px;
    height: 21px;
    position: absolute;
    right: 0px;
    top: 24px;
}
</style>
</body>

<script type="text/javascript">
   
   $(".loder-show").hide();
  $(".loder-hide").show();
    setTimeout(function(){
    $(".footer-container").removeClass("loader-active");
      $(".loder-hide").hide();
      $(".loder-show").show();
    },1500);
    
</script>

   <?php wp_footer(); ?>
</html>


