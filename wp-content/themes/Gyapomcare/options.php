<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 * 
 */

function optionsframework_option_name() {

	// This gets the theme name from the stylesheet (lowercase and without spaces)
	$themename = get_theme_data(STYLESHEETPATH . '/style.css');
	$themename = $themename['Name'];
	$themename = preg_replace("/\W/", "", strtolower($themename) );
	
	$optionsframework_settings = get_option('optionsframework');
	$optionsframework_settings['id'] = $themename;
	update_option('optionsframework', $optionsframework_settings);
	
	// echo $themename;
}



function  logo_show(){
     return of_get_option('logo_show');
}  
add_shortcode('logo', ' logo_show');

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the "id" fields, make sure to use all lowercase and no spaces.
 *  
 */

function optionsframework_options() {
	
	/*$agents = array();

	//$agents[0] = "Cathi & Ben Walter vs. Leslie Arnold"; //additional custom code added, so be carefull don't know what the effect are. enable would make associate widget goes blank, you must created the associate first.
	foreach(get_posts('post_type='.VR_Custom_Post_Types::ASSOCIATE_POST_TYPE.'&orderby=name&numberposts=-1') as $agent) {
		$agents[$agent->ID] = $agent->post_title;
	}
	
	$terms = get_terms('videos');

	$categories = array();
	
foreach ( $terms as $term ) { 

 $categories[get_term_link( $term, 'videos' )] = $term->name; 

  } */	
	if(class_exists('RGFormsModel')){
		$all_gform = RGFormsModel::get_forms( null, 'title' );
		
		foreach($all_gform as $all_form){
			$form_option_arr[$all_form->id] = $all_form->title;
		}
	}
	$options = array();
/**
 * begin options list
 * 
 *  
 */
 
/*begin logo*/
$options[] = array( "name" => "Theme logo",
						"type" => "heading");
						
$options[] = array( "name" => "Header Logo",
						"desc" => "",
						"id" => "header_logo",
						"std" => "",
						"type" => "upload");
//footer function

$options[] = array( "name" => "Footer",
						"type" => "heading");
// Facebook

$options[] = array( "name" => "Facebook url",
						"desc" => "",
						"id" => "facebookurl",
						"std" => "",
						"type" => "text");

$options[] = array( "name" => " Facebook Image",
						"desc" => "",
						"id" => "facebook_image",
						"std" => "",
						"type" => "upload");
// Twitter

 $options[] = array( "name" => "Twitter url",
						"desc" => "",
						"id" => "twitterurl",
						"std" => "",
						"type" => "text");

 $options[] = array( "name" => " Twitter Image",
						"desc" => "",
						"id" => "twitter_image",
						"std" => "",
						"type" => "upload");
 // Instagram

 $options[] = array( "name" => "Instagram url",
						"desc" => "",
						"id" => "instagramurl",
						"std" => "",
						"type" => "text");

 $options[] = array( "name" => " Instagram Image",
						"desc" => "",
						"id" => "instagram_image",
						"std" => "",
						"type" => "upload");
 // Slider Functions
    
    $options[] = array( "name" => "Tasker Slider",
						"type" => "heading");
    
    $options[] = array( "name" => "SliderOne",
						"type" => "info");				
    $options[] = array( "name" => "Image",
						"desc" => "",
						"id" => "sliderone_image",
						"std" => "",
						"type" => "upload");
    $options[] = array( "name" => "Name",
						"desc" => "",
						"id" => "sliderone_name",
						"std" => "",
						"type" => "text");
    
    $options[] = array( "name" => "SliderTwo",
						"type" => "info");				
    $options[] = array( "name" => "Image",
						"desc" => "",
						"id" => "slidertwo_image",
						"std" => "",
						"type" => "upload");
    $options[] = array( "name" => "Name",
						"desc" => "",
						"id" => "slidertwo_name",
						"std" => "",
						"type" => "text");
    
    $options[] = array( "name" => "Sliderthree",
						"type" => "info");				
    $options[] = array( "name" => "Image",
						"desc" => "",
						"id" => "sliderthree_image",
						"std" => "",
						"type" => "upload");
    $options[] = array( "name" => "Name",
						"desc" => "",
						"id" => "sliderthree_name",
						"std" => "",
						"type" => "text");
    
    $options[] = array( "name" => "SliderFour",
						"type" => "info");				
    $options[] = array( "name" => "Image",
						"desc" => "",
						"id" => "sliderfour_image",
						"std" => "",
						"type" => "upload");
    $options[] = array( "name" => "Name",
						"desc" => "",
						"id" => "sliderfour_name",
						"std" => "",
						"type" => "text");
    
    $options[] = array( "name" => "SliderFive",
						"type" => "info");				
    $options[] = array( "name" => "Image",
						"desc" => "",
						"id" => "sliderfive_image",
						"std" => "",
						"type" => "upload");
    $options[] = array( "name" => "Name",
						"desc" => "",
						"id" => "sliderfive_name",
						"std" => "",
						"type" => "text");



			
						
	return $options;
}