<?php 
/*
	Template Name: Blog
*/
?>
<link href="<?php echo get_stylesheet_directory_uri();?>/assets/css/blog.css" rel="stylesheet" />

<?php get_header(); ?>

	<div class="page-container">
    <div class="full_landing">
        <div class="page-content">
            <div class="col-md-12">
                <div class="fullwidth">           
                    <?php if (have_posts()) : the_post(); ?>
        				<?php the_content(); ?>	
        			<?php endif; ?>	  
				</div>
            </div>
           <div class="clear"></div>
        </div>
    </div>
</div>

<?php get_footer(); ?>