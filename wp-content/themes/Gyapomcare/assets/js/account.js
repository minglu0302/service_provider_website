jQuery(document).ready(function($){
             
    var toSendPassword;
	var Usertoken = localStorage.getItem('iserve_token');
    
 
    var fingerprintr = localStorage.getItem('time_stamp');
  
        function d2(n) {
            if(n<9) return "0"+n;
            return n;
        }
        today = new Date();
      
        var current_time = today.getFullYear() + "-" + d2(parseInt(today.getMonth()+1)) + "-" + d2(today.getDate()) + " " + d2(today.getHours()) + ":" + d2(today.getMinutes()) + ":" + d2(today.getSeconds());
        var datalog1 ={
                "ent_sess_token":localStorage.getItem('iserve_token'),
                "ent_dev_id":  fingerprintr,
                "ent_date_time":current_time,
        };
      
        var changedPassword = "";
        $('.width100').focus(function(){
            $(".passerror").text('');
        });
    
        $("#ChangePas").click(function(){ 
            var password = $(".passwords").val();
            var confirm_password = $(".passwords_confirm").val();
            var oldPassword =$(".current_password").val();
    
            if(password != confirm_password ||password==''|| confirm_password=='') {
                if(password==''|| confirm_password==''){
                $('.passerror').text("Please fill the both the fields");  
                }
                else
                $('.passerror').text("Passwords Don't Match");
              } else {
                $('.passerror').text(''); 
                changedPassword = confirm_password; 
                
                passwordData ={
                    "ent_old_password":oldPassword,
                    "ent_new_password":changedPassword
              }
                 toSendPassword = JSON.stringify(passwordData);
                  
                  
                send_password(toSendPassword);
              }    
        });
           
        function send_password(toSendPassword){
            console.log("toSendPassword",toSendPassword);
         var token = localStorage.getItem('iserve_token');   
            
            $.ajax({
                type: 'PUT',
                url: object_name.apiUrl+'setNewPassword/'+token, 
                dataType: 'json',
                data:toSendPassword,
                headers: {'accept': 'application/json', 'content-type': 'application/json'},
                success: function(res) {
                    console.log(res);  
                   if(res.errFlag==1)
                   {
                    if(res.errNum==28){
                     $(".passsuccess").text(res.errMsg);
                        close_all(); 
                    }else{
                        $(".passerror").text(res.errMsg);
                    }
                        
                   }
                   else{
                     close_all();   
                   }
                }
            });   
        }    
	        
            
	         

       // Logout API

        $(".logout_button").click(function(){
        	    var acceptValue = 'application/json'; 
                var contentType = 'application/json'; 
		        $.ajax({
		            type: 'GET',
        			url: object_name.apiUrl+'logout/'+Usertoken,
        			dataType: 'json',
        			headers: {'accept': acceptValue, 'content-type': contentType},
		            success: function(response) {
		              console.log(response);
		                if(response.errFlag == 1){ 
		                    console.log(response);
                            
		                }
		                else{ 
		                     close_all();   
		                    }
		            }
		        });
	    });

        // UserUpdate API
        $("#UserUpdate").click(function(){
        	   var acceptValue = 'application/json'; 
               var contentType = 'application/json'; 
	           var FirstNames = $("#first_name").val();
	           var LastNames = $("#last_name").val();
	           var ac_mobile = $("#acc_number").val();

		        var userupdates ={
		                "ent_sess_token": localStorage.getItem('iserve_token'),
		                "ent_first_name": FirstNames ,
		                "ent_last_name": LastNames,
		                "ent_mobile": ac_mobile,
		        };
		         userupdates = JSON.stringify(userupdates);
		        $.ajax({
		        	type: 'PUT',
            		url: object_name.apiUrl+'profile',
           			dataType: 'json',
            		headers: {'accept': acceptValue, 'content-type': contentType},
            		data: userupdates,
		            success: function(response) {
		              console.log(response);
		                if(response.errFlag == 1){ 
		                    console.log(response);
		                    
                            
		                }
		                else{ 
		                     close_all();   
		                    }
		            }
		        });
	    });
    
    function close_all(){

                          localStorage.removeItem('addressloc');  
                          localStorage.removeItem('booking_type');
                          localStorage.removeItem('cat_id');
                          localStorage.removeItem('cat_lat');
                          localStorage.removeItem('cat_lng');
                          localStorage.removeItem('catname');
                          localStorage.removeItem('cusid');
                          localStorage.removeItem('iserve_token');
                          localStorage.removeItem('payment_type');
                          localStorage.removeItem('time_stamp');
                         
                          window.location.href = "../wp-content/themes/Gyapomcare/getinfo.php"; 
;   
    }
    
    

	    $(".new_edit").hide();
        $(".showing_edit").click(function(){
	        $(".new_edit").show();
	        $(".avatar_section").hide();
        
        });  

});


