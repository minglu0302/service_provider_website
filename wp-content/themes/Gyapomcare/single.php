<style>
    .vc_grid.vc_grid-owl-theme .vc_grid-owl-dots {
        display: none;
        margin-top: 0px;
        text-align: center;
        -webkit-tap-highlight-color: transparent;
        padding-bottom: 50px;
    }
    .whole_section {
        padding-left: 10%;
        padding-right: 10%;
    }
    div#respond {
        padding-left: 10%;
        padding-right: 10%;
    }
    .vc-gitem-zone-height-mode-auto:before {
        content: "";
        display: block;
        padding-top: 80%;
    }
    .vc_grid.vc_grid-owl-theme .vc_grid-owl-dots {
        display: none;
        margin-top: 20px;
        text-align: center;
        -webkit-tap-highlight-color: transparent;
        margin-bottom: 30px;
    }
    div#div-comment-5 {
        padding-left: 10%;
    }
    h3#comments {
        padding-left: 10%;
    }
    img.attachment-post-thumbnail.size-post-thumbnail.wp-post-image {
        width: 100%;
        padding-top: 2%;
    }
    input {
        border: 1px solid black;
        margin-bottom: .5em;
        padding: 1% !important;
        cursor: pointer;
    }
</style>
<?php


get_header(); ?>


		<!-- Start the Loop. -->
 <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

 	<div class="whole_section">

 	<h2><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>

      <?php the_post_thumbnail(); ?>
 	<!-- Display the date (November 16th, 2009 format) and a link to other posts by this posts author. -->

 	<small><?php the_time('F jS, Y'); ?> by <?php the_author_posts_link(); ?></small>


 	<!-- Display the Post's content in a div box. -->

 	<div class="entry">
 		<?php the_content(); ?>
 	</div>


 	<!-- Display a comma separated list of the Post's Categories. -->

 	<p class="postmetadata"><?php _e( 'Posted in' ); ?> <?php the_category( ', ' ); ?></p>
 	</div> <!-- closes the first div box -->
           <?php comments_template(); ?>

 	<!-- Stop The Loop (but note the "else:" - see next line). -->

 <?php endwhile; else : ?>


 	<!-- The very first "if" tested to see if there were any Posts to -->
 	<!-- display.  This "else" part tells what do if there weren't any. -->
 	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>


 	<!-- REALLY stop The Loop. -->

</div>
 <?php endif; ?>
		



<?php get_footer(); ?>
