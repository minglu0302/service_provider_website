<?php

/*
Template Name: How its work on the way
*/
?>
<?php get_header('frontpage');    ?>
<!-- style & scripts -->

<link href="<?php echo get_stylesheet_directory_uri();?>/assets/css/how-it-works.css" rel="stylesheet" />
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>   
<style type="text/css">
    .slick-slide img {
        display: inline-block;
        text-align: center !important;
    }
    .carousel-inner {
         overflow: inherit !important;
    }
</style>
<!--  Header container-->

<div class="page_heading_container">
    <div class="Howitswork_template">   
        <div class="col-md-12">
            <div class="page_heading_content">
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="page-container">
    <div class="howitswork_default">
        <div class="page-content">
            <div class="col-md-12">
                <div class="fullwidth">           
                    <?php if (have_posts()) : the_post(); ?>
                        <?php the_content(); ?> 
                    <?php endif; ?>   
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>

<?php get_footer(); ?>

<script type="text/javascript">
    $('.slider').slick({
   slidesToShow: 1,
slidesToScroll: 1,
dots: true,
infinite: false,
cssEase: 'linear'
});
</script>