<?php

/*
Template Name:terms&condition
*/
get_header('frontpage');	
?>



<style>
.privacy_policy_sector {
    padding-bottom: 20px;
    font: 400 18px/30px 'Roboto', sans-serif;
    color: #999999;
    letter-spacing: 1px;
    font-family: steagal;
}
h3 {
    font-size: 1.4rem;
    letter-spacing: 1.5px;
}
</style>





<div class="page_heading_container">
  <div class="container">
        <div class="row">
		<div class="col-md-12">
		<div class="page_heading_content">
		
		</div>
</div>
</div>
<div class="clear"></div>
</div>
</div>
<div class="page-container">
    <div class="container">
        <div class="row">
            <div class="page-content">
             <div class="col-md-12">
              <div class="fullwidth">           
            <?php if (have_posts()) : the_post(); ?>
				<?php the_content(); ?>	
			<?php endif; ?>	  
				</div>
			</div>
        </div>
        <div class="clear"></div>
    </div>
</div>
