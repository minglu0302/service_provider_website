
<?php

/*
Template Name:   history Iserve
*/
session_start();

if(!$_SESSION['first_name']){
 header("Location:". $object_name['webUrl'] . "book-a-service/");    
}

get_header('frontpage');	

?>


<link href="<?php echo get_stylesheet_directory_uri();?>/assets/css/history.css" rel="stylesheet" />   
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
	<script type="text/javascript" src="//cdn.rawgit.com/niklasvh/html2canvas/0.5.0-alpha2/dist/html2canvas.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
<link href="<?php echo get_stylesheet_directory_uri();?>/css/account.css" rel="stylesheet" />
<link href="<?php echo get_stylesheet_directory_uri();?>/css/application.css" rel="stylesheet" />
<meta name="viewport" content="width=device-width">
     <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCn0dk0FzLNwlseL973kfz9EKRFFyy5a5M&libraries=places&callback=initMap"
        async defer></script>
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
      <style type="text/css">#ko_twocolumnBlock_6 .textsmallintenseStyle a, #ko_twocolumnBlock_6 .textsmallintenseStyle a:link, #ko_twocolumnBlock_6 .textsmallintenseStyle a:visited, #ko_twocolumnBlock_6 .textsmallintenseStyle a:hover {color: #fff;color: ;text-decoration: none;text-decoration: none;font-weight: bold;}
         #ko_twocolumnBlock_6 .textsmalllightStyle a:visited, #ko_twocolumnBlock_6 .textsmalllightStyle a:hover {color: #3f3d33;text-decoration: none;text-decoration: none;font-weight: bold;}
      </style>

<style>



</style>
<div id="my-toast-location" style="position: fixed; top; 0; right: 20px;"></div>  



<div class="overlay" style="display:none">   
  <div class="wrap"> 
    <div class="center">
      <div class="document-loader">
        <span class="heading short"></span>
        <span class="line short"></span>
        <span class="line"></span>
        <span class="line"></span>
        <span class="line"></span>
        <span class="line short"></span>
        <span class="line"></span>
        <span class="line"></span>
        <span class="line short"></span>
        <span class="heading"></span>
        <span class="line"></span>
        <span class="line"></span>
        <span class="line short"></span>
        <span class="line"></span>
        <span class="line"></span>
        <span class="line"></span>
        <span class="line"></span>
        <span class="line short"></span>
      </div>
      <p>Loading Bookings...</p>  
    </div>
  </div>
</div>
<div class="container--wide" style="margin-top: 20px;">
	<div class="row">
		<div class="col-12 col-lg-8 col--centered">
			<div>
				<div class="media--object media--object--centered">
					<div class="media--figure">
						<img class="avatar-new-50" src="http://www.goclean-service.com/wp-content/uploads/2017/12/default_avatar.jpg" alt="">
					</div>
					<div class="media--content">
						<h2 class=""><span class="UserNames">Welcome to Goclean Service</span></h2>
					</div>
				</div><!-- react-empty: 21 -->
			</div>
		</div>
		<div class="col-12 col-lg-8 col--centered task_history">
			<button class="current_tasks addcft">CURRENT TASKS</button>
			<button class="pasttasks">PAST TASKS</button>
		</div>
        <!-- current tasker list -->
        <div class="current_tasker">
          	<div class="col-12 col-lg-8 col--centered padding_sector">

			</div>
			<div class="showing_deatils_block">  
			<div class="showing_book_info">
			<span id="show_current">
			Showing : 
			<span class="show_now_book"></span>
			</span>
            <span class="show_now_slash">/</span>
			</span>
			<span class="show_now_full"></span>
			</span>
            <span class="show_now_entries">Bookings</span>
			</span>	
			</div>
			<div>
             <button id="load_more">Load More</button>			
			</div>
        </div>
		</div>
        <!-- Past current task -->
        <div class="past_tasker" style="display: none;">
			<div class="col-12 col-lg-8 col--centered calender no_padding">

				<div style="cursor:pointer;" class="container">
				    <div class='col-md-3 padding_zero'>
				        <div class="form-group">
				            <div class='input-group date' id='datetimepicker6'>
				                <input type='text' class="form-control"  id="start_date" placeholder="start date" />
				                <span class="input-group-addon">
				                    <span class="glyphicon glyphicon-calendar"></span>
				                </span>
				            </div>
				        </div>
				    </div>
				    <div class='col-md-3 padding_zero'>
				        <div class="form-group">
				            <div class='width_80 input-group date' id='datetimepicker7'>
				                <input type='text' class="form-control" id="end_date" placeholder="end date"/>
				                <span class="input-group-addon">
				                    <span class="glyphicon glyphicon-calendar"></span>
				                </span>
								</div>
								<div class="width_20">
								<span class="input-group-addon" id="search_bookings">
				                    <span class="glyphicon glyphicon-search"></span>
				                </span>   
								</div>
				            
				        </div>
				    </div>
                    
				    <div class="col-md-3 col-xs-12  padding_zero ">
				    	<input class="searching_content" type="text" name="search" placeholder="Search By Provider Name" id="search_booking_name"><span class="fa fa-search" id="provider_by_name_search"></span>  
				    </div>
				</div>
			</div>



			<div class="col-12 col-lg-8 col--centered padding_sector1">



			</div>
			<div class="col-12 col-lg-8 col--centered showing_deatils_block2" style="padding:0px">   
			<div class="showing_book_info2">
			<span id="show_current2">
			Showing : 
			<span class="show_now_book2"></span>
			</span>
            <span class="show_now_slash2">/</span>
			</span>
			<span class="show_now_full2"></span>
			</span>
            <span class="show_now_entries2">Bookings</span>
			</span>	
			</div>
			<div>
             <button id="load_more2">Load More</button>			
			</div>
          </div>
		</div>
	</div>

</div>


<!--popup invoice-->
<div id="editor"></div>
<div id="invoice_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
      
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <img class="logo_iserve_Popup" src="../wp-content/uploads/2017/11/Gyapom-website-home-logo.png">
      </div>
      <div class="modal-body" id="content">                   
        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="ko_twocolumnBlock_6"> 
         <tbody>
            <tr class="row-a">
               <td bgcolor="#f9f9f9" align="center" class="section-padding" style="padding-top: 0px; padding-left: 0px; padding-bottom: 0px; padding-right: 0px;">
                  <table border="0" cellpadding="0" cellspacing="0" width="600" class="responsive-table">
                     <tbody>
                        <tr>
                           <td>
                              <!-- TWO COLUMNS -->
                               
							  <table cellpadding="0" cellspacing="0" border="0" width="600" align="left" class="responsive-table" style="background:#fff;padding:2%;border: 1px solid #e6e3e3;">
								<tbody><tr><td style="font-size: 23px; letter-spacing:1px; padding: 10px;font-family: fantasy; font-weight: 700;width:50%" id="invoice_total_amt">$ 100.66</td>
								    <td class="mobile_using" style="float: right; color: #b2b2b2; margin-top: 20px; margin-right: 10px;font-family: sans-serif;letter-spacing: 1px;font-size: 13px;">Thanks for using Gyapomcare, </td>
									
								</tr>
							  </tbody></table>
							  
                              <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                 <tbody>
                                    <tr>
                                       <td valign="top" style="padding: 2%;background:#fff;border: 1px solid #e6e3e3; border-top: 0px;" class="mobile-wrapper">
                                          <!-- LEFT COLUMN -->
                                          <table cellpadding="5" cellspacing="0" border="0" width="46%" align="left" class="responsive-table" style="background:#fff;border:1px solid #f9f9f9">
                                             <tbody>
                                               
												
                       
												<tr class="bottom_border">
												  <td>
												    <p class="left_col_head">JOB ID</p>
													<span class="left_col_field align_pro" id="invoice_jobid" style="text-align: center;"></span>
												  </td>
												  <td>
												    <p class="left_col_head"></p>
													<span class="left_col_field"></span>
												  </td>
												  <td style="text-align:center">
												    <p class="left_col_head" style="text-align: center;">DATE AND TIME</p>
													<span class="left_col_field align_pro" id="invoice_booking_date" style="text-align: center;"></span>
												  </td>
												</tr>
												<tr class="bottom_border">
												  <td colspan="3">
												    <p class="left_col_head">JOB LOCATION</p>
													<span class="left_col_field" id="invoice_job_lcoation">
													</span>
												  </td>
												</tr>
                                      	<tr class="bottom_border">
												  <td colspan="3">
												    <p class="left_col_head">PROVIDER DETAILS</p>
													<span><img id="invoice_pro_img" src="" style="margin-bottom:15px;float:left;margin-right:5px"></span><span class="left_col_field" style="margin-left:0px" id="invoice_pro_name">
													Edward Francis<br>
													<span id="invoice_pro_phone"></span>
													</span>
												  </td>
												</tr>											
												  
												<tr>
												  <td colspan="3">
												    <p class="left_col_head" style="text-align:center">Issued on behalf of <span id="invoice_pro_name2"></span></p>
												  </td>
												</tr>
	<tr class="bottom_border">
												  <td colspan="3">
												    <p class="left_col_head21">JOB DESCRIPTION</p>
												    <p id="invoice_job_desc" style="font-size:12px"></p>
												  </td>
												</tr>
												<tr>
												  <td colspan="3">
												    <p class="left_col_head22" style="text-align:left">PROVIDER DESCRIPTION</p>
													<p id="invoice_pro_desc_" style="font-size:12px"></p>
												  </td>
												</tr>
												
											  <tr>

                                           
                                             </tbody>
                                          </table>
                                          <!-- RIGHT COLUMN -->
                                          <table cellpadding="5" cellspacing="0" border="0" width="46%" align="right" class="responsive-table">
                                             <tbody>
                                 
											    <td class="rt_col_head" colspan="3">RECEIPT</td>	  </tr>                                         
												                                                     
                                               	
                                                
                                               

												<tr class="">
												  <td class="rt_col_head2 ">
												   Job Type
												  </td>
												  <td class="align-rt">
												    <span id="invoice_job_type"></span>
												  </td>
                                                </tr>


																									<tr class="">
												  <td class="rt_col_head2 ">
												  Services
												  </td>
												  <td class="align-rt">
												  <span id="invoice_services"></span>
												  </td>

                                                <tr class="delivery1">
                                                <td class="rt_col_head2">
												 Services Price
												  </td>
												  <td class="align-rt">
												    <span id="invoice_services_price"></span>
												  </td>
                                                </tr>
																								   <tr class="delivery1">
                                                <td class="rt_col_head2">
												 Booking Type
												  </td>
												  <td class="align-rt">
												    <span id="invoice_booking_type"></span>                    
												  </td>
                                                </tr>

																								   <tr class="delivery1">
                                                <td class="rt_col_head2">
												Hourly Fee
												  </td>
												  <td class="align-rt">
												 <span id="invoice_hourly_fee"></span>
												  </td>
                                                </tr>
																								   <tr class="delivery1">
                                                <td class="rt_col_head2">
												Misc Fee
												  </td>
												  <td class="align-rt">
												 <span id="invoice_misc_fee"></span>
												  </td>
                                                </tr>
												
												<tr class="delivery1">
                                                <td class="rt_col_head2">
												Mat. Fee
												  </td>
												  <td class="align-rt">
												  <span id="invoice_mat_fee"></span>
												  </td>
                                                </tr>
																								   <tr class="delivery1">
                                                <td class="rt_col_head2">
												 Provider Discount
												  </td>
												  <td class="align-rt">
												    <span id="invoice_pro_discount"></span>
												  </td>
                                                </tr>
																								   <tr class="delivery1">
                                                <td class="rt_col_head2">
												App Discount
												  </td>
												  <td class="align-rt">
												    <span id="invoice_app_discount"></span>
												  </td>
                                                </tr>
												

												<tr class="bottom_border">
												  <td class="bold_txt" style="font-size:16px">
												     Total
												  </td>
												  <td class="bold_txt" style="text-align:right">
												     <span id="invoice_total"></span>
												  </td>
                                                </tr>
												<tr>
												  <td class="rt_col_head2" style="font-size: 10px;line-height: 9px;margin-bottom: 0px;">CHARGED</td>
                                                </tr>
												<tr class="bottom_border">
												  <td class="bold_txt" style="font-size:17px">
												    <span><img id="invoice_pay_img" src="<?php echo get_stylesheet_directory_uri();?>/images/cash.png" style=" float: left;  padding-top: 2px; margin-right: 5px;"></span> <span  id="invoice_pay_type">Cash</span>
												  </td>
												  <td class="bold_txt" style="font-size:15px;text-align:right">
												     <span id="invoice_total_amt"></span>
												  </td>
                                                </tr>



                                                <tr style="font-size: 13px;width: 100%;">
												   <td colspan="3">
												    <p class="left_col_head">CUSTOMER SIGNATURE:<img src="" id="sign_img" alt="signature_img"></p>    
												   </td>
												</tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
         </tbody>   
      </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default close_popup" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-default close_popup getpdf" id="cmd" >Download </button>


      </div>
    </div>

  </div>
</div>



<!--popup invoice -->


<?php wp_footer(); ?>

<?php include_once( dirname(__FILE__) . '/template/api-functions/history.php'); ?>