  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
<style>
.active, .dot:hover {
    background-color: #fff; 
}
.removeClass.active {
    color: #5aa1e3;
}
span.glyphicon.glyphicon-chevron-left {
    display: none;
}
span.glyphicon.glyphicon-chevron-right {
    display: none;
}

img.image_Fixed {
    width: 312px;
}
.background_Slidetaskers {
    /* position: relative; */
    height: 472px;
    display: inline-block !important;
    float: left;
    width: 250px;
}
.carousel-inner {
    display: inline-block !important;
    width: 300px;
    position: absolute;
    left: -3%;
    margin-top: 5%;
}
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 100%;
      margin: auto;
  }
  </style>
</head>
<body>

<div class="moning_screen">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active">

</li>
      <li data-target="#myCarousel" data-slide-to="1">

</li>
      <li data-target="#myCarousel" data-slide-to="2">

</li>
      <li data-target="#myCarousel" data-slide-to="3">

</li>
<li data-target="#myCarousel" data-slide-to="4">

</li>
    </ol>

    <!-- Wrapper for slides -->
<div class="background_Slidetaskers">
<img class="image_Fixed" src="<?php echo get_stylesheet_directory_uri();?>/images/mockup.png">
</div>
    <div class="carousel-inner" role="listbox">
      <div class="item active" id="move1">
        <img class="hello_class show1" src="<?php echo of_get_option('sliderone_image');?>" alt="Chania" width="460" height="345">
      </div>

      <div class="item" id="move2">
        <img class="hello_class show2" src="<?php echo of_get_option('slidertwo_image');?>" alt="" width="460" height="345">
      </div>
    
      <div class="item" id="move3">
        <img class="hello_class show3" src="<?php echo of_get_option('sliderthree_image');?>" alt="" width="460" height="345">
      </div>

      <div class="item" id="move4">
        <img class="hello_class show4" src="<?php echo of_get_option('sliderfour_image');?>" alt="" width="460" height="345">
      </div>
<div class="item" id="move5">
        <img class="hello_class show5" src="<?php echo of_get_option('sliderfive_image');?>" alt="" width="460" height="345">
      </div> 
   
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
<div class="new-tasker-hiw__container new-tasker-hiw__points-container">
<div class="js-hiw-point new-tasker-hiw__point removeClass move1 active" id="move1" ><span class="pre_shadow"></span><?php echo of_get_option('sliderone_name');?></div>
<div class="js-hiw-point new-tasker-hiw__point removeClass  move2" id="move2"><span class="pre_shadow"></span><?php echo of_get_option('slidertwo_name');?></div>
<div class="js-hiw-point new-tasker-hiw__point removeClass  move3" id="move3"><span class="pre_shadow"></span><?php echo of_get_option('sliderthree_name');?></div>
<div class="js-hiw-point new-tasker-hiw__point removeClass  move4" id="move4"><span class="pre_shadow"></span><?php echo of_get_option('sliderfour_name');?></div>
<div class="js-hiw-point new-tasker-hiw__point removeClass  is-active move5" id="move5"><span class="pre_shadow"></span><?php echo of_get_option('sliderfive_name');?></div>
</div>
</div>



<script>
$('#myCarousel').on('slide.bs.carousel', function (ev) {
  var id = ev.relatedTarget.id;
  $(".removeClass").removeClass("active");
  if(id == "move1"){
    $(".move1").addClass("active");   
  }else if(id == "move2"){
   $(".move2").addClass("active");   
 }else if(id == "move3"){
   $(".move3").addClass("active");  
 }else if(id == "move4"){
   $(".move4").addClass("active");   
 }
else if(id == "move5"){
   $(".move5").addClass("active");   
 }
})

$(".removeClass").click(function(){
var id = $(this).attr('id');
console.log(id);
$(".removeClass").removeClass("active");
$(".item").removeClass("active");
 if(id == "move1"){
    $("#move1").addClass("active"); 
    $(".move1").addClass("active");
  }else if(id == "move2"){
   $("#move2").addClass("active");
   $(".move2").addClass("active");   
 }else if(id == "move3"){
   $("#move3").addClass("active");
   $(".move3").addClass("active");  
 }else if(id == "move4"){
   $("#move4").addClass("active"); 
   $(".move4").addClass("active");  
 }
else if(id == "move5"){
   $("#move5").addClass("active");
   $(".move5").addClass("active");   
 }
});
</script>