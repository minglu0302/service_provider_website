
 <script type="text/javascript">
     var sinMobile;
     var code_type;
      
    code_type=localStorage.getItem('code_type');
       
  $(".not-activated__content").hide();
   $('#verification1').focus();
   $('#verification1').keyup(function() {
        if(this.value.length == 1) {
            $('#verification2').focus();
        }
    });
   $('#verification2').keyup(function() {
        if(this.value.length == 1) {
            $('#verification3').focus();
        }else{
           $('#verification1').focus();
           console.log("ff");
        }
    });
   $('#verification3').keyup(function() {
        if(this.value.length == 1) {
            $('#verification4').focus();
        }else{
           $('#verification2').focus();
        }
   });
   $('#verification4').keyup(function() {
        if(this.value.length == 1) {
            $('#verification5').focus();
        }else{
           $('#verification3').focus();
        }
    });

    var acceptValue = 'application/json'; 
    var contentType = 'application/json'; 

    function d2(n) {
        if(n<9) return "0"+n;
        return n;
    }
    today = new Date();
    var current_time = today.getFullYear() + "-" + d2(parseInt(today.getMonth()+1)) + "-" + d2(today.getDate()) + " " + d2(today.getHours()) + ":" + d2(today.getMinutes()) + ":" + d2(today.getSeconds());
    var fingerprint = new Fingerprint().get();  
    var fullfingerprint = (fingerprint+localStorage.getItem("emails")).toString();
    localStorage.setItem('time_stamp',fullfingerprint);


   
  
    $('#verification4').keyup(function(){
         if($('#verification1').val()==""){
                $('#verification1').focus();
         }
         else if($('#verification2').val()=="")
         {
                 $('#verification2').focus();
         }
         else if($('#verification3').val()=="")
         { 
                $('#verification3').focus();
         }
         else if($('#verification4').val()!="")
         {
           console.log('clicked');
           $("#verify-code").trigger('click');           
         }
    });
    $("#verify-code").click(function(){
        var val1 = $('#verification1').val();
        var val2 = $('#verification2').val();
        var val3 = $('#verification3').val();
        var val4 = $('#verification4').val();
         var codeM = val1+""+val2+""+val3+""+val4;
		 var singup_type ;
		 var signup_id;
		 if(localStorage.getItem('ent_signup_type_id'))
	    {
		singup_type = 2;
		signup_id = localStorage.getItem('ent_signup_type_id').toString();
		
	    }
	    else{
            $("#loader_verify").show();
		singup_type = 1 ;
		signup_id =null;
	   }
	      sinMobile = localStorage.getItem("phones");
          sinMobile_code = localStorage.getItem('countrycodesin');
          $(".parent_verify").show();
          
          code_type=localStorage.getItem('code_type');  
        
            
         if (code_type!=2){
              code_type=1;
        };
             
        
         // verifyPhone Opt API
         $.ajax({
            type: 'GET',
            url: object_name.apiUrl+'verifyCode/'+sinMobile+"/"+sinMobile_code+"/"+codeM+"/"+code_type,
            dataType: 'json',
            headers: {'accept': acceptValue, 'content-type': contentType},
            success: function(response) {
                console.log(response);

                if(response.errFlag == 1){ 
                    console.log(response); 
                    $("#emailError").text(response.errMsg);
                    $("#emailSuccess").text("");                    
                  $(".parent_verify").hide();                                         
                }else if(response.errNum==79){
                         window.location.href=response.data;
                }else{      
                // Sign in API   
                     var date_time = moment().format('YYYY-MM-DD HH:MM:SS');				
                    var data2 = {
                        "ent_first_name":localStorage.getItem("firstName"),
                        "ent_last_name":localStorage.getItem("last_name"),
                        "ent_email":localStorage.getItem("emailsin"),
                        "ent_password":localStorage.getItem("password"),
                        "ent_country_code":localStorage.getItem("countrycodesin"),
                        "ent_mobile":localStorage.getItem("phones"),
                        "ent_signup_type":singup_type,
						"ent_fb_id":signup_id,
						"ent_register_date":date_time
                    };
                    data2 = JSON.stringify(data2);
                    $.ajax({
                        type: 'POST',
                        url: object_name.apiUrl+'signup',
                        dataType: 'json',
                        headers: {'accept': acceptValue, 'content-type': contentType},
                        data:data2,
                        success: function(response) {
                            if(response.errNum == 409){ 
                                $("#emailError").text(response.errMsg);
                                $("#emailSuccess").text("");
                                $(".parent_verify").hide();  

                            }
                            else{
                                $("#emailError").hide();
                                console.log(response);
                                console.log(response.data.id);
                                localStorage.setItem('iserve_token',response.data.token);
								localStorage.removeItem('ent_signup_type_id');
                                var local_token_signup = localStorage.getItem('iserve_token');
                                localStorage.setItem('cusid',response.data.id);
                                window.location.href = '<?php echo site_url(); ?>/dashboard/?token='+local_token_signup;
                                 $(".parent_verify").hide();
                            }

                        },
                        error: function(error) {
                            console.log(error);
                                                                           
                        }
                    });
                        //  console.log(response);
                }
                  
            }
        });
    });
       
     // verifyPhone resend Opt API
     $("#verify-resend").click(function(){
       
        var sinMobile = localStorage.getItem("countryphone");
        var acceptValue = 'application/json'; 
        var contentType = 'application/json';         
        $.ajax({
            type: 'GET',
            url: object_name.apiUrl+'VerificationCode/'+sinMobile,
            dataType: 'json',
            headers: {'accept': acceptValue, 'content-type': contentType},
            success: function(response) {
                console.log("verification"+response);
                if(response.errFlag == 1){ 
                    console.log(response);
                    
                    $(".parent_verify").hide();

                }
                else{
                  
                    console.log(response);                    
                    $("#emailError").text("");
                    $("#emailSuccess").text(response.errMsg);
                    $(".parent_verify").hide();
                                 
                }
                              
            }
        });
                    
    });
 </script>

 