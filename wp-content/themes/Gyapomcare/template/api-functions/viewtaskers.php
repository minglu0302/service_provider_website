

<script type="text/javascript">
    
jQuery(document).ready(function($){
    $('#loader').hide();
    
    var pid = "";
    var dateTime ="";
    var slot_Id ="";
	var sort = 0;
    var slot_date_time ="";
    var date_time_slot_toSend ="";
    
    
    var getTimeSlot;
    var query = window.location.search.substring(1);
   query = query.replace("uid=", "");
   var booking_type = 1;
   localStorage.setItem('booking_type', booking_type);
  function d2(n) {
                if(n<9) return "0"+n;
                return n;
        }
        today = new Date(); 
        var current_time = today.getFullYear() + "-" + d2(parseInt(today.getMonth()+1)) + "-" + d2(today.getDate()) + " " + d2(today.getHours()) + ":" + d2(today.getMinutes()) + ":" + d2(today.getSeconds());
// calendar functions

   $('.right_btn_date').click(function(){

        $(".btn-hide").addClass('slick-prev');
        $(".btn-hide").show();
        $(".left_btn_date").removeClass('is-disabled');
        $(".slick-next").trigger('click');  
        if($('.slick-next').hasClass('slick-disabled')){
                $(".right_btn_date").addClass('is-disabled');
            
        }  
      
   });

   $('.left_btn_date').click(function(){
        $(".slick-prev").trigger('click');
        $(".right_btn_date").removeClass('is-disabled');
        if($('.slick-prev').hasClass('slick-disabled')){
            $(".left_btn_date").addClass('is-disabled');
            
        }   
   });
   

   $('#aioConceptName').find(":selected").text();

    var Usertoken = localStorage.getItem('iserve_token');
    var query = window.location.search.substring(1);
    query = query.replace("uid=", "");

    Date.prototype.addDays = function(days) {
        var dat = new Date(this.valueOf())
        dat.setDate(dat.getDate() + days);
        return dat;

    }

    function getDates(startDate, stopDate) {
        var dateArray = new Array();
        var currentDate = startDate;
        while (currentDate <= stopDate) {
            dateArray.push(currentDate)
            currentDate = currentDate.addDays(1);
        }
        return dateArray;
    }

		
    var weekday = new Array(7);
            weekday[0] =  "Sunday";
            weekday[1] = "Monday";
            weekday[2] = "Tuesday";
            weekday[3] = "Wednesday";
            weekday[4] = "Thursday";
            weekday[5] = "Friday";
            weekday[6] = "Saturday";
            var monthNames = ["Jan", "Feb", "March", "April", "May", "June",
      "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    ];

    var dateArray = getDates(new Date(), (new Date()).addDays(90));
	var option_placeholder;
	var active_class ="active";
    for (i = 0; i < dateArray.length; i ++ ) {
		          if(i>0){
					  active_class = "";
				  }
                 option_placeholder = moment(dateArray[i]).format('YYYY-MM-DD');
                 $(".datelists").append("<div class='recommendations__schedule-filter-day js-datetime-window-day frdactive' ><input class='is-hidden'  name='job.schedule.date' type='radio' value="+ dateArray[i].getDate() +"><label class='recommendations__schedule-filter-day__inner lableactive "+active_class+"' data-attribute='"+option_placeholder+"'><strong>"+weekday[dateArray[i].getDay()]+"</strong><span>"+ dateArray[i].getDate() + " " + monthNames[dateArray[i].getMonth()]+"</span></label></div>")
                 console.log("datelist",option_placeholder);

    }
	    var timeArray = [];  // ONPAGE LOAD TIMES WHEN ARE AFTER 1 HOUR FROM ASAP 
        d = new Date();
        h = d.getHours();
        m = d.getMinutes();
        meridiem = ['AM','PM'];
		var dt_tm_nw_book = $('.lableactive.active').attr('data-attribute');
        for (var i = h+1,timelt_cont= 0; i < 24; ++i,timelt_cont++) {
            //for (var j = i==h ? Math.ceil(m/30) : 0; j < 2; ++j) {
                //timeArray.push(i%12 + ':' + (j*30||'00') + ' ' + meridiem[i/12|0]);
          //  }
			timeArray.push(i%12 + ':' + '00' + ' ' + meridiem[i/12|0]); 
				if(i<10)              
			{
				i = "0"+i                                           //APPENDS "0" WHEN TIME IS BELOW 10
			}
            $(".timelist").append("<option value='"+dt_tm_nw_book+' '+i+':00'+':00'+"'>"+timeArray[timelt_cont]+"</option>");	
        }
// calendar slider

    $('.slider').slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        dots:false ,
        infinite: false,
        cssEase: 'linear'
    });
    
    
    var change_bool = true;
	var callProviders_bool = true;
	var booking_date_time;
	var see_later_bookings = false;
	 var sort ;
    
    
	$(document).on('change', '.timelist', function() {
	 change_bool = false;
     var today_bookings = $(this).find('option:selected').val();
     if(today_bookings == 0){
		 callProviders_bool = false;
	 }
	 $("label.lableactive.active").trigger('click');
    });
    
    
            
    function showtimeslots(pid,date){
       $(".parent_verify").show();
         $('.slot_lists').empty();
        $.ajax({
                type: 'GET',
                url: object_name.apiUrl+'slots/'+Usertoken+'/'+pid+'/'+date,
                dataType: 'json',
                headers: {'accept': 'application/json', 'content-type': 'application/json'},
                success: function(response) { 
                console.log(response);
                    if(response.errFlag == 1){ 
                        console.log(response);
                    }
                    else{
                        for(var list_inst=0;list_inst<response.data.length;list_inst++){
                            slot_date_time =response.data[list_inst].start_dt;
                            var start_date_timing = moment(response.data[list_inst].start_dt).format('hh:mm a');
                            var end_date_time = moment(response.data[list_inst].end_dt).format('hh:mm a');
                            var slot_price = response.data[list_inst].slot_price
                            
                        //  $('.slot_lists').append('<div class="slot_buttons" data-value="'+slot_date_time+'" data-attribute="'+response.data[list_inst]._id+'">'+start_date_timing+'-'+end_date_time+'<span class="popup_pricing_slot">$ '+slot_price+'</span></div>');  
                        $('.slot_lists').append('<div class="slot_buttons" data-value="'+slot_date_time+'" data-attribute="'+response.data[list_inst]._id+'">'+start_date_timing+'-'+end_date_time+'<span class="popup_pricing_slot">£ '+slot_price+'</span></div>'); 
                        }


                        
                        $(".slot_Container").show(); 
                     $(".parent_verify").hide();
                    }
                }
        });                                         
    };     
       
    
	$(document).on('click', '.provider_select', function() { 
	var slot = $('.timelist :selected').val();
	var id = $(this).attr('data-attribute');    
        pid=id;
        dateTime=slot;
        showtimeslots(pid,dateTime);
    });
    
    $(document).on('click', '.slot_buttons', function() { 
        date_time_slot_toSend =$(this).attr('data-value');
        $(".slot_error").empty();
        if ($(".slot_buttons").hasClass("active_slot")) {
              $(".slot_buttons").removeClass("active_slot");
            }
        $(this).addClass("active_slot");
        slot_Id =$(this).attr('data-attribute');
        // window.location.href =  object_name.webUrl+'confirm/?pid='+pid+'&slot='+dateTime+'&slot_id='+slot_id;
    });
    
    
     $(document).on('click', '.go_for_book span', function() { 
       if(slot_Id!=""){
          window.location.href =  object_name.webUrl+'confirm/?pid='+pid+'&slot='+date_time_slot_toSend+'&slot_id='+slot_Id;
          }
         else{
             $(".slot_error").text("Please select a slot")
         }
     });
    $(document).on('click', '.close_booking', function() {   
        slot_Id="";
        $(".slot_error").empty();
        $(".slot_Container").hide();
    });
   
   $('#tomorrows').click(function(){
	   see_later_bookings = true;
	   $("label.lableactive.active").trigger('click');
   });
   
   $('#selectDropdown').change(function() {
    sort = $(this).find('option:selected').val();
	
	change_bool = false;
	 $("label.lableactive.active").trigger('click');
});
    $(".slick-prev").addClass('btn-hide');
    $(".btn-hide").hide();
    $(".btn-hide").removeClass('slick-prev');
	
    $(".lableactive").click(function(){
		var valDate = $(this).parent().children().val();
		var option_value =$(this).attr('data-attribute');
		var currentdate = new Date().getDate();
	    var timeArray = [];
        d = new Date();
        h = d.getHours();
        m = d.getMinutes();
        meridiem = ['AM','PM'];
		if(change_bool==true){                             // APPENDS DATE AND TIME IN DROPDOWN BUTTON BASED ON BUTTON CLICK IF THE LABEL SHOWING DATE IS CLICKED IT WONT CHANGE ....
		if(valDate == currentdate){                        // IF CURRENT DATE IS SELECTED IT WILL REMAINING TIME SLOTS INTERVALS OF ONE HOUR
		$(".timelist").empty();$(".timelist").append("<option value=''>ASAP</option>");
        for (var i = h+1,timelt_cont=0; i < 24; ++i,timelt_cont++) {
			timeArray.push(i%12 + ':' + '00' + ' ' + meridiem[i/12|0]);
			if(i<10)              
			{
				i = "0"+i                                           //APPENDS "0" WHEN TIME IS BELOW 10
			}
            $(".timelist").append("<option value='"+option_value+' '+i+':00'+':00'+"'>"+timeArray[timelt_cont]+"</option>");			
        }
		}
		else{
			$(".timelist").empty();                                  // IF LATER DATE IS SELECTED IT WILL ALL TIME SLOTS INTERVALS OF ONE HOUR FROM 5 AM -  11PM
			for (var i = 5,timelt_cont =0; i < 24; ++i,timelt_cont++) {
			timeArray.push(i%12 + ':' + '00' + ' ' + meridiem[i/12|0]); 
			if(i<10)
			{
				i = "0"+i
			}
			$(".timelist").append("<option value='"+option_value+' '+i+':00'+':00'+"'>"+timeArray[timelt_cont]+"</option>");
        }
		}
	}
	if(see_later_bookings == true){
		booking_date_time =  $('.timelist option:nth-child(2)').val();
		$('.timelist option:nth-child(2)').attr('selected', 'selected');
	}
	else{
		booking_date_time = $('.timelist').find(":selected").val();
        getTimeSlot = booking_date_time;
        localStorage.setItem("Timeslot", getTimeSlot);
	}
		
		console.log("valDate",valDate);
    $(".parent_verify").show();
    $(".js-datetime-window-day").removeClass("frdactive");
    $(".lableactive").removeClass("active");

    if( booking_date_time!=0 && callProviders_bool){

        var booking_type = 2;
        localStorage.setItem('booking_type', booking_type);
        var acceptValue = 'application/json'; 
        var contentType = 'application/json'; 
        $("#providerlist").empty();       
        $("#providerlist").show();
        $("#selectdropdown_parent").show();
        $("#quick1").hide();
        $("#quick2").hide();
        $("#quick3").hide();
		console.log("valDate",valDate);
      $(".hiding_progress").hide();
        var cat_lat = localStorage.getItem('cat_lat');
        var cat_lng = localStorage.getItem('cat_lng');
         console.log("object_name.apiUrl+'tasker/'+Usertoken+'/'+cat_lat+'/'+cat_lng+'/'+query+'/'+sort",
                     object_name.apiUrl+'tasker/'+Usertoken+'/'+cat_lat+'/'+cat_lng+'/'+query+'/'+sort +'/'+booking_date_time);
                     console.log(object_name.apiUrl+'taskerForBookLater/'+Usertoken+'/'+cat_lat+'/'+cat_lng+'/'+query+'/'+sort +'/'+booking_date_time);
        $.ajax({
                type: 'GET',
                url: object_name.apiUrl+'taskerForBookLater/'+Usertoken+'/'+cat_lat+'/'+cat_lng+'/'+query+'/'+sort +'/'+booking_date_time,
                dataType: 'json',
                headers: {'accept': acceptValue, 'content-type': contentType},
                success: function(response) {
						 see_later_bookings = false;
						 callProviders_bool = true;       
						 change_bool = true;	
                    if(response.errNum == 39){ 
					    // console.log(url);
                       $(".parent_verify").hide();
                       $("#providerlist").append("<div class='recommendations__result recommendations__result--tasker is-enabled customer_enabled taskerlist_s'><p id='no_provider_ava_text'><span><img src='../wp-content/uploads/2017/05/sad-2.png' alt='no provider' height='100' width='100' id='no_provider_ava'></span><br>No Taskers available at the selected time</p></div>");
                       $('#selectDropdown').attr('disabled', true); 
					   change_bool = true;	
                    }
                    else{
		  
                         $(".parent_verify").hide();
                         console.log("taskerForBookLater",response); 
                          response=response.data;
                          var pros = response.length;
						  if(pros>1){
							  $('#selectDropdown').attr('disabled', false); 
						  }
						  else{
							  $('#selectDropdown').attr('disabled', true); 
						  }

                           var pros_cont= 0;
                           for(;pros_cont<pros;pros_cont++) 
                         {
                           var taskslist;
                           if(response[pros_cont].tot_comp>1)
						   {
							   taskslist = response[pros_cont].tot_comp +" Tasks Completed";
						   }
						   else{
							   taskslist = response[pros_cont].tot_comp +" Task Completed";
						   }
						   var rating;
						   if(response[pros_cont].avg_rev!=0){
							   rating = (response[pros_cont].avg_rev * 10)*2;
							   rating = rating + "%";
						   }
						   else{
							   rating = "0%"
						   }
						   var about_us;
						   if(response[pros_cont].aboutUs==null)
						   {
							   response[pros_cont].aboutUs = "Not Available";
						   }
						   var image;
						   if(response[pros_cont].image==''||response[pros_cont].image==null)
						   {   
							   response[pros_cont].image = '../wp-content/uploads/2017/05/default_profile.png';
						   };
						   if(response[pros_cont].last_review.cust_pic==''||response[pros_cont].last_review.cust_pic==null){
                              response[pros_cont].last_review.cust_pic= '../wp-content/uploads/2017/05/default_profile.png';
                           };
                           if(response[pros_cont].last_review.review==''||response[pros_cont].last_review.review==null){
                                response[pros_cont].last_review.review='Reviews are not available';
                              }  ;
                            if(response[pros_cont].last_review.star_rating==''||response[pros_cont].last_review.star_rating==null){
                                response[pros_cont].last_review.star_rating='Na';
                              };
                            
						   var class_append ;
						   if(response[pros_cont].tot_comp==0){
							   class_append = "display_non";
						   }
						   else{
							   class_append = "";
						   }
						   var date_time = moment(response[pros_cont].last_review.review_dt).format("dddd, MM Do YYYY, h:mm:ss a")	;
						       response[pros_cont].last_review.review_dt = date_time;
							if(response[pros_cont].last_review.cust_fname==undefined){
								response[pros_cont].last_review.cust_fname = "Anonymous";
							}
						   var default_img = object_name.webUrl+'/wp-content/uploads/2017/05/default_profile.png';
                             
						    
                        
                           //alert("dsf");
                           $("#providerlist").append("<div class='recommendations__result recommendations__result--tasker is-enabled customer_enabled taskerlist_s'><div class='recommendations__result-figure'><div class='chrome_48_fix'><img class='recommendations__avatar js-link-reviews' onerror='this.onerror=null;"+default_img+"' src="+ response[pros_cont].image +"></div><div class='recommendations__result-actions'><a class='recommendations__hire-btn btn btn-primary btn-large js-btn-hire customer provider_select'  data-attribute='"+response[pros_cont].pro_id+"' alt = '"+response[pros_cont].pro_id+"'>Select &amp; Continue</a><a class='recommendations__review-link js-link-reviews popup_review' id="+response[pros_cont].pro_id+" Click='getbid()' href='#'>Reviews &amp; Profile</a></div></div><div class='recommendations__result-content '><div class='recommendations__result-name js-link-reviews'>"+ response[pros_cont].name +"</div><div class='js-recommendation-item-price'><div data-pie-id='9945'><div class='recommendations__result-price-container'><div class='recommendations__result-price recommendations__pill'><strong class='js-hourly-rate'>£"+ response[pros_cont].hourl_rate +"</strong><strong>/hr</strong></div><div class='recommendations__recurring-savings'></div></div></div></div><ul class='recommendations__result-info'><li class='recommendations__result-info--invoices'><i class='fa fa-check'></i><span>"+ taskslist +"</span></li><li class='recommendations__result-info--rating'><i class='fa fa-thumbs-o-up'></i><span>"+ response[pros_cont].total_reviews +" Reviews, Avg. Rating: <div class='star-ratings-css'> <div class='star-ratings-css-top' style='width:"+rating+" '><span>★</span><span>★</span><span>★</span><span>★</span><span>★</span></div>	<div class='star-ratings-css-bottom'><span>★</span><span>★</span><span>★</span><span>★</span><span>★</span></div></div><span class='avg_rating'>("+ response[pros_cont].avg_rev +")</span></span></li></ul><div class='recommendations__blurb-title'>How I can help:</div><div class='recommendations__blurb'><p class=''>"+ response[pros_cont].aboutUs +"</p></div><div class='recommendations__review media--object "+class_append+"'><div class='media--figure'><img class='recommendations__no-category-review__avatar avatar-new-72' src="+   response[pros_cont].last_review.cust_pic +"></div><div class='media--content'><div class='recommendations__review-content review_txt'>"+ response[pros_cont].last_review.review +"</div><div class='avg_rating_cust'>Rating :<span class='cust_rating_points'>"+response[pros_cont].last_review.star_rating+"★</span></div><div class='recommendations__review-content'>"+ response[pros_cont].last_review.cust_fname +"</div><div class='u-muted'>‐ "+ response[pros_cont].last_review.review_dt +"</div></div></div><div class='recommendations__result-actions'><a class='recommendations__hire-btn btn btn-primary btn-large js-btn-hire provider_select' data-attribute='"+response[pros_cont].pro_id+"'>Select &amp; Continue</a><a class='recommendations__review-link js-link-reviews popup_review' id="+response[pros_cont].pro_id+" Click='getbid()' href='#' >Reviews &amp; Profile</a></div></div></div>");
                             var urlid = response[pros_cont].user; 
                            
//                                if(response[pros_cont].last_review.cust_pic=="undefined"||response[pros_cont].last_review.cust_pic==""||response[pros_cont].last_review.cust_pic=   ="null"){
//                                    $(".recommendations__no-category-review__avatar avatar-new-72").attr('src',default_img);
//                                }  
                             
                             
                             //******************** handling the broken image with default image *************************//
                             $('img').each(function(){
                                 $(this).attr('src', $(this).attr('src'));
                             });

                             $("img").error(function(){
                                 $(this).attr('src', default_img); 
                             });
                            //********************* handling the broken image with default image *************************//
                         } 
                         
                        }
                }
        }); 
        
   
        
    }
        
 
    else{
		callProviders_bool = true;       
		change_bool = true;
        var booking_type = 1;
        localStorage.setItem('booking_type', booking_type);
         $(".parent_verify").hide();
        $('#selectDropdown').attr('disabled', true); 
        $("#providerlist").hide();
        $(".hiding_progress").show();
        $("#quick1").show();
        $("#quick2").show();
        $("#quick3").show();
     }
    console.log(valDate, currentdate);
    $(this).addClass("active");
  });
    
    
 
  
    
    
  // Quick Booking

   $("#LiveBookings").click(function(){
    window.location.href =  object_name.webUrl+'confirm/?proid='+'0'+'/'+'ASAP';               
 });



});
</script>


   