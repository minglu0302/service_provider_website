<?php

/*
Template Name:  Customer profile
*/
get_header('frontpage');	
 if(!$_SESSION['first_name']){  
      header("Location: ". $object_name['webUrl'] . "book-a-service/");
}
?>

<link href="<?php echo get_stylesheet_directory_uri();?>/css/api.css" rel="stylesheet" />
<link href="<?php echo get_stylesheet_directory_uri();?>/assets/css/customerpro.css" rel="stylesheet" />
<!--css for full profile-->
<style>
.profile__header {
    padding: 2rem 0;
    background-repeat: no-repeat;
    background-size: cover;
    text-align: center;
    color: white;
    background: url(../wp-content/uploads/2017/05/fullprofile.png);
}
    .rating{
        float: right;
    }
.size_re {
    margin-bottom: 16px;
    font-size: 18px;
    line-height: 36px;
}
a.profile__subnav-link.about:active {
    border-bottom: 3px solid !important;
}
@media (max-width: 956px){
h2, h3 {
    font-size: 20px !important; 
}
}
</style>

<div id="page_loader_parent" style="display:none">
    <div id="spinner" class="active">
        <span id="first" class="ball"></span>
        <span id="second" class="ball"></span>
        <span id="third" class="ball"></span>
    </div>
</div>

<div class="profile">
  <div class="profile__build-header js-build-header">
    <div data-pie-id="3813" style="height: auto;">
      <div class="build-subheader build-subheader--profile js-subheader-panel">
        <div class="container--wide">
          <div class="row row--guttered row--thin">
            <div class="col-12 col-lg-8">
              <h2 class="build-subheader__title provider_catname">
              Assemble Furniture
              </h2>
          </div>
           
            <div class="col-12 col-lg-4 u-align--right">
            <a  id="sendto" class="btn btn-primary profile__build-hire-btn js-hire customer provider_select" href="#">Select & Continue </a>
<!--<span class="muted provider_catcost"></span>-->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  
<div class="slot_Container">  
     
<div class="lightbox--background js-lightbox-bg js-dismiss close_booking"></div>
    <div class="lightbox--internal">
        <div id="page_loader_parent" style="display:none">
                            <div id="spinner" class="active">
                                <span id="first" class="ball"></span>
                                <span id="second" class="ball"></span>
                                <span id="third" class="ball"></span>
                            </div>
                        </div>
        <div class="lightbox--content-wrapper">
            <div class="lightbox--title js-lightbox-title text-center">Available Slots</div>
            <div class="lightbox--content js-lightbox-content">
                <div data-pie-id="4279" class="">
                        <div class="reviews-section">
                            <div class="js-child-container reviews-list slot_lists">
                                 
                            </div>
                        </div>
                        
                       
                </div>
                
                <div class="book_slot">
                  <div class="slot_error"></div>
                   
                    
                   <div class="cancel_book">
                      <span class="close_booking">Cancel</span>
                    </div>
                    <div class="go_for_book">
                      <span> Book </span>
                    </div>
                   
                </div>
            </div>
            
            <a href="#" class="lightbox--dismiss js-lightbox-dismiss js-dismiss">
              <i class="fa fa-times close_booking"></i>
            </a>
        </div>
    </div>
</div>




  <div class="profile__header">
    <div class="container">
      <div>
      <img class="profile__headshot-img profile_photop" src="../wp-content/uploads/2017/05/default_profile.png"> 

      </div>
      <h1 class="profile__greeting"><strong>Hello,</strong> <span class="provider_namep">I’m Jake A.</span></h1>

      <div class="profile__status"><strong>Last online:</strong> <span class="lastSeen"></span> </div>

      <div class="col-md-12 profile__overview">
          
        <div class="col-md-4 profile__overview-section">
          <i class="fa fa-thumbs-o-up hand"></i>
          <div class="profile__overview-details">
            <h4 class="profile__overview-primary">Average rating <span class="profile_rate"> </span></h4>
          </div>
        </div>
          
        <div class="col-md-4 profile__overview-section">
          <i class="fa fa-check hand"></i>
          <div class="profile__overview-details">
            <h4 class="profile__overview-primary">I’ve done <strong class="total_cop"></strong>.</h4>
            <div class="profile__overview-secondary">I’ve been a Tasker <strong>since <span class="month"></span> <span class="year"> </span></strong>.</div>
          </div>
        </div>
          
        <div class="col-md-4 profile__overview-section">
          <i class="fa fa-map-marker hand"></i>
          <div class="profile__overview-details">
            <h4 class="profile__overview-primary">
            
              <span class="location">
              
          
          
          </span></h4>
          </div>
        </div>
          
      </div>
        
    </div>
  </div>
  <div class="profile__subnav">
    <div class="container">
      <ul class="profile__subnav-list js-subnav">

         <li class="profile__subnav-item about_us">
          <a class="profile__subnav-link about is-focused" data-page="about" data-replace-state="1" >About</a>
        </li>

        <li class="profile__subnav-item review_open">
          <a class="profile__subnav-link review" id="review_pp" data-page="reviews" data-replace-state="1" >Reviews</a>
            
        </li>

       
      </ul>
    </div>
  </div>



<div class="profile__subview js-subview aboutview_open">
  <div data-pie-id="3946">
    <div class="profile__bio">
      <div class="container">
        <div class="row row--gutter">
          <div class="col-12">
         
            <div class="profile__bio-section">
                 <div class="expertise">
                    <h2>Expertise:</h2>
                    <span class="Expertise"> </span>
                  </div>   
              
                  <div class="aboutSelf">
                       <h2>About Myself:</h2>

                          <p class="size_re ">
                              <span class="aboutMyself">  </span>
                            </p>
                   </div>
                   <div class="Knownlanguages">
                                <h2>Languages Known:</h2>
                                <span class="Languages">
                                </span>
                    </div>
              <div class="profile__bio-questions"> 

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


<!--review section-->


  <div class="profile__subview js-subview profile_full alter">
  <div data-pie-id="3834">    
    <div class="container">
      <div class="row">
        <div class="col-12">
          <label for="review_filter">Filter by:</label>
          <select class="js-filter" id="review_filter" onchange="changeFunc(value);">
          <option value="0">ALL</option>
          <option value="5">Excellent</option>   
          <option value="4">Postive</option>
          <option value="3">Average</option>
          <option value="2">Poor</option>
          <option value="1">Negative</option>
          </select>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <div class="profile__reviews-list js-items-container" id="providerreview_profile">
            <!-- review list --> 
          </div>
        <div class="pagination-container"></div>
      </div>


    </div>
  </div>

</div>
</div>





  </div>
</div>

<?php include_once( dirname(__FILE__) . '/template/api-functions/customer-profile.php'); ?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/assets/js/customerProfile.js"></script>