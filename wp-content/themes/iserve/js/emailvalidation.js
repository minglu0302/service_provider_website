
$.validator.setDefaults({
  debug: true,
  success: "valid"
});

$( "#myformvalid" ).validate({
  rules: {
    emails: {
      required: true,
      email: true
    },
    password: {
           required: true,
            minlength: 5
    },
    password_again: {
      required: true,
      minlength: 5,
      equalTo: "#password"
    },

    firstname: {
      required: true
      
    },
    lastname: {
      required: true
      
    },
    mobilenumber: {
      required: true,
       minlength: 5
      
    }
  },
  messages: {
    emails: "Please enter email address",
    password: "Password is required",
    password_again:"Sorry, your passwords don't match. Please re-enter your passwords",
    firstname: "Please enter a first name",
    lastname: "Please enter a last name",
    mobilenumber: "Please re-enter mobile number with a minimum of 5 numbers"


   }
});
 