


<?php

/*
Template Name:   view taskers and price
*/
get_header('frontpage');    
 if(!$_SESSION['first_name']){
    header("Location:http://iserve.ind.in/book-a-service");
}
?>

<link href="<?php echo get_stylesheet_directory_uri();?>/assets/css/viewtaskers.css" rel="stylesheet" />
<link href="<?php echo get_stylesheet_directory_uri();?>/css/api.css" rel="stylesheet" />


<div class="page_heading_container">
    <div class="task_template">       
        <div class="col-md-12">
           <div class="page_heading_content">
           </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="page-container">
    <div class="task--default">    
        <div class="page-content">
            <div class="col-md-12">
                <div class="fullwidth">           
                    <?php if (have_posts()) : the_post(); ?>
                        <?php the_content(); ?> 
                    <?php endif; ?>   
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<style type="text/css">
     #loader_verify {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 0.5s linear infinite;
  animation: spin 0.5s linear infinite;
  margin-bottom:10px
}
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
.parent_verify {
    position: fixed;
    width: 100%;    
	top: 0px;
    height: 100%;
    background: rgba(0,0,0,0.5);
    z-index: 999;
}

.sub_parent_verify {
    background: white;
        width: 141px;
    padding: 10px;
    border-radius: 10px;
    position: absolute;
    left: calc(50% - 75px);
    top: calc(50% - 75px);
	    text-align: center;
    font-size: 15px;
}
img#no_provider_ava {
    margin-bottom: 20px;
}
#no_provider_ava_text{
    text-align: center;
    display: inline-block;
    width: 100%;
    font-size: 19px;
    margin-top: 14px;
    color: #b1b1b1;
    font-weight: 600;
}
select#selectDropdown[disabled]{
	cursor:not-allowed
}
    
    
/*--------------------------spinner--------------------------------*/
#spinner.active {
    display: block;
}
div#page_loader_parent {
    position: fixed;
    width: 100%;
    height: 100%;
    z-index: 99;
    background: rgba(0,0,0,0.7);
    top: 0px;
    left: 0px;
    border-radius: 8px
}
#spinner {
    display: none;
    position: absolute;
    height: 60px;
    width: 60px;
    top: 40%;
    left: 48%;
    z-index: 1;
}
.ball {
    position: absolute;
    display: block;
    background-color: #feffff;
    left: 24px;
    width: 12px;
    height: 12px;
    border-radius: 6px;
}
div#spinner:after {
    content: 'loading Reviews...';
    color: #fff;
    position: absolute;
    bottom: -36px;
    width: 150px;
    left: -16px;
}
#first {
    -webkit-animation-timing-function: cubic-bezier(0.5, 0.3, 0.9, 0.9);
    -webkit-animation-name: rotate; 
    -webkit-animation-duration: 2s; 
    -webkit-animation-iteration-count: infinite;
    -webkit-transform-origin: 6px 30px;
    -moz-animation-timing-function: cubic-bezier(0.5, 0.3, 0.9, 0.9);
    -moz-animation-name: rotate; 
    -moz-animation-duration: 2s; 
    -moz-animation-iteration-count: infinite;
    -moz-transform-origin: 6px 30px;

}
#second {
    -webkit-animation-timing-function: cubic-bezier(0.5, 0.5, 0.9, 0.9);
    -webkit-animation-name: rotate; 
    -webkit-animation-duration: 2s; 
    -webkit-animation-iteration-count: infinite;
    -webkit-transform-origin: 6px 30px;
	  -moz-animation-timing-function: cubic-bezier(0.5, 0.5, 0.9, 0.9);
    -moz-animation-name: rotate; 
    -moz-animation-duration: 2s; 
    -moz-animation-iteration-count: infinite;
    -moz-transform-origin: 6px 30px;
}
#third {
    -webkit-animation-timing-function: cubic-bezier(0.5, 0.7, 0.9, 0.9);
    -webkit-animation-name: rotate; 
    -webkit-animation-duration: 2s; 
    -webkit-animation-iteration-count: infinite;
    -webkit-transform-origin: 6px 30px;
	  -moz-animation-timing-function: cubic-bezier(0.5, 0.7, 0.9, 0.9);
    -moz-animation-name: rotate; 
    -moz-animation-duration: 2s; 
    -moz-animation-iteration-count: infinite;
    -moz-transform-origin: 6px 30px;
}
@-webkit-keyframes rotate {
  0% {
    -webkit-transform: rotate(0deg) scale(1);
  }
  100% { 
    -webkit-transform: rotate(1440deg) scale(1); 
  }
}​

@-moz-keyframes rotate {
  0% {
    -moz-transform: rotate(0deg) scale(1);
  }
  100% { 
    -moz-transform: rotate(1440deg) scale(1); 
  }
}
/*PAGE LOADER CSS ENDING--------------*/    
 </style>
<!-- templae codes -->
<div class="parent_verify" style="display: none;">
 <div class="sub_parent_verify">
    <div id="loader_verify" ></div>
	<span>Fetching taskers...</span>
 </div>
</div>
<div class="js-progress-container">
    <div class="build-progress" data-pie-id="269">
        <div class="build-progress__container container--wide">
            <div class="build-progress__step js-step" data-page="form" data-state="previous">
                <i class="ss-pencil"></i>
                <span>1.</span>
                <span>Fill Out Task Details</span>
            </div>
            <div class="build-progress__step js-step" data-page="recommendations" data-state="current">
                <i class="fa fa-smile-o"></i>
                <span>2.</span>
                <span>View Taskers &amp; Prices</span>
            </div>
            <div class="build-progress__step js-step" data-page="confirm" data-state="future">
                <i class="ss-checkmark"></i>
                <span>3.</span>
                <span>Confirm &amp; Book</span>
            </div>
        </div>

   </div>
</div>

<div class="js-trust-banner-container">
    <div class="build-trust-banner box-no-padding" data-pie-id="297">
        <div class="build-trust-banner__container container--wide">
            <img alt="Trust badge" class="build-trust-banner__badge trust" src="http://iserve.ind.in/wp-content/uploads/2017/03/trust_badge.png">
                <div class="build-trust-banner__title">
                    <strong>Trust &amp; Safety Guarantee</strong>
                </div>
                <a class="build-trust-banner__info u-hidden--sm u-hidden--md u-hidden--lscp js-tt-trigger" data-tt-contenttemplate="build.trustBannerTooltip" data-tt-placement="b" href="#">
                   
                </a>
       </div>

    </div>
</div>

<div class="view--body view--build-main recommendations">
    <div class="container--wide build-container">
        <div class="js-subheader-container">
            <div data-pie-id="1982">
                <div class="build-subheader build-subheader--form">
                    <div class="build-subheader__title catnames">
                            General Handyman
                    </div>
                    <div class="supplemental_infos"></div>
                </div>
            </div>
        </div>
        <div class="recommendations__layout js-recommendations-container">
            <div class="recommendations__filters js-filters-container">
                <div data-pie-id="2106" style="height: auto;">
                    <span class="recommendations__date-picker-title u-hidden--lg">Date &amp; Time</span>
                    <a class="recommendations__mobile-filter-trigger btn btn-primary js-modal-trigger" href="#">
                        <span class="js-filter-summary"><strong>Today,</strong> Mar 01: 6:30pm</span>
                        &nbsp;&nbsp;
                        <i class="ss-icon ss-caret-down-light"></i>
                    </a>
                   <div class="recommendations__filter-items panel panel--spaced js-child-container">
                        <div data-pie-id="2131" class="recommendations__filter" id="selectdropdown_parent">
                            <h4 class="recommendations__filter-title">Sorted By:</h4>
                            <select class="u-capitalize" name="currentSort" id="selectDropdown" disabled>
                                <!-- <option value="recommended">recommended</option> -->
								<option value="0">Select Filter</option>
                                <option value="2">price (lowest to highest)</option>
                                <option value="1">price (highest to lowest)</option>
                                <option value="4">highest rating</option>
                                <option value="3">most reviews</option>
                            </select>
                        </div>
                        <div data-pie-id="2159" class="recommendations__filter">
                            <h4 class="recommendations__filter-title">
                                <i class="fa fa-clock-o "></i>
                                <span>Task Date &amp; Time:</span>
                            </h4>
                           <div class="recommendations__schedule-filter">
                                <div class="build--datetime-windows js-dates-container">

                                    <!--  <input id="datepicker-example7-start" type="text"> -->
                                    <div class="build--datetime-windows-viewport js-dates-list datelists slider" style="left: 20px;">
                                    </div>
                                    
                                   <a class="build--datetime-windows-control js-move left_btn_date is-disabled" data-direction="left">
                                       <i class="fa fa-caret-left"></i>
                                   </a>
                                    <a class="build--datetime-windows-control js-move right_btn_date" data-direction="right">
                                       <i class="fa fa-caret-right"></i>
                                    </a> 
                                </div>
                                <div class="recommendations__schedule-filter-slot">
                                    <select name="job.schedule.offset_seconds" class="timelist">
                                        <option data-duration="0" value="0">ASAP</option>
                                        
                                    </select>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                    <a class="recommendations__mobile-filter-dismiss btn btn-primary js-dismiss" href="#">See Available Taskers</a>
                </div>
            </div>
            <div class="recommendations__results js-results-container">
                <div data-pie-id="1997">
                    <div class="recommendations__results-list js-list-container">
                        <div class="recommendations__result recommendations__result--broadcast is-enabled hiding_progress" data-user-id="broadcast" data-pie-id="2350">
                            <div class="recommendations__result-figure">
                                <div class="chrome_48_fix">
                                    <img class="recommendations__avatar" src="http://iserve.ind.in/wp-content/uploads/2017/04/user.png">
                                </div>
                            </div>
                            <div class="recommendations__result-content" id="quick1">
                                <div class="recommendations__result-name">Quick Assign</div>
                                <div class="js-recommendation-item-price">
                                    <div data-pie-id="2379">
                                        <div class="recommendations__result-price-container">
                                            <div class="recommendations__result-price recommendations__pill">
                                                <!-- <strong class="js-hourly-rate">$78</strong><strong>/hr</strong> -->
                                            </div>
                                            <div class="recommendations__recurring-savings">

                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="recommendations__blurb-title">How Quick Assign will work:</div>
                                <div class="recommendations__blurb">We will notify qualified Taskers in your area about your task based on the information you’ve provided.</div>
                                <a class="recommendations__hire-btn btn btn-primary btn-large js-btn-hire continue_option" id="LiveBookings">Select &amp; Continue</a>
                                <!-- <a class="recommendations__hire-btn btn btn-primary btn-large js-btn-hire continue_option" href="http://iserve.ind.in/confirm/">Select &amp; Continue</a> -->
                            </div>

                       </div>
                       <div data-pie-id="2077">
                            <div class="recommendations__status-warning" id="quick2">If you prefer to select your Tasker directly, please choose a different day for your task.</div>
                            <div class="recommendations__status" id="quick3">
                                <div class="btn btn-secondary btn--md js-tomorrow tasker_avail" id="tomorrows">See Available Taskers</div>
                            </div>
                            <div id="providerlist">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

 <!--modal popup for review-->
<div class="lightbox--container" data-pie-id="4284"><div class="lightbox--background js-lightbox-bg js-dismiss class_gone "></div>
    <div class="lightbox--internal">
        <div class="lightbox--content-wrapper">
            <div class="lightbox--title js-lightbox-title pro_name">Reviews for .</div>
            <div class="lightbox--content js-lightbox-content">
                <div class="page-loader"></div>
                <div data-pie-id="4279" class="">
                    <div class="recommendation-reviews-lightbox">
                        <div class="reviews-section">
                            <div class="reviews-section--header">A little about me:</div>
                            <p></p><p class="proabouts">This is my history</p><p></p>
                        </div>
                        <div class="reviews-section  rev_body">
                            <div class="reviews-section--header recommendation-reviews-list--title">Reviews:</div>
                            <div class="js-child-container reviews-list">
                  
                            </div>
                        </div>
                        
                        <div id="page_loader_parent" style="display:none">
                            <div id="spinner" class="active">
                                <span id="first" class="ball"></span>
                                <span id="second" class="ball"></span>
                                <span id="third" class="ball"></span>
                            </div>
                        </div>
                        
<!--
                        <ul id="pagin">
                            <li><span><a class="current" href="#">1</a></span></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                        </ul>
-->
                        <div class="u-align--right js-pagination-container pagination-container">
                            <div class="pagination">
                                <span class="current">1</span>
                                <a class="pagination-number" data-page="2" href="/">2</a>
                                <a class="pagination-number" data-page="3" href="">3</a>
                                <a class="pagination-caret" data-page="2" href="">
                                    <i class="fa fa-chevron-right"></i>
                                </a>
                            </div>
                        </div>
                        <div class="u-align--left">
                            <a class="recommendation-reviews-list--profile-link js-profile-link providerprofile_p">See full profile</a>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" class="lightbox--dismiss js-lightbox-dismiss js-dismiss">
              <i class="fa fa-times class_gone"></i>
            </a>
        </div>
    </div>
</div>


<div class="slot_Container">   
<div class="lightbox--background js-lightbox-bg js-dismiss close_booking"></div>
    <div class="lightbox--internal">
        <div class="lightbox--content-wrapper">
            <div class="lightbox--title js-lightbox-title text-center">Available Slots</div>
            <div class="lightbox--content js-lightbox-content">
                <div data-pie-id="4279" class="">
                        <div class="reviews-section">
                            <div class="js-child-container reviews-list slot_lists">
                                 
                            </div>
                        </div>
                        
                        <div id="page_loader_parent" style="display:none">
                            <div id="spinner" class="active">
                                <span id="first" class="ball"></span>
                                <span id="second" class="ball"></span>
                                <span id="third" class="ball"></span>
                            </div>
                        </div>
                </div>
                
                <div class="book_slot">
                  <div class="slot_error"></div>
                   
                    
                   <div class="cancel_book">
                      <span class="close_booking">Cancel</span>
                    </div>
                    <div class="go_for_book">
                      <span> Book </span>
                    </div>
                   
                </div>
            </div>
            
            <a href="#" class="lightbox--dismiss js-lightbox-dismiss js-dismiss">
              <i class="fa fa-times close_booking"></i>
            </a>
        </div>
    </div>
</div>



<!--<div id='loader'><img src='<?php bloginfo('template_url')?>/images/loading3.gif'></div>-->
<!--<style type="text/css">
ul#pagin li a.current {
    color: #242a30 !important;
}
ul#pagin {
    float: right;
    letter-spacing: 10px;
}
#pagin li {
  display: inline-block;
}
    .loadersmall {
    position: absolute;
    border: 5px solid #f3f3f3;
    -webkit-animation: spin 1s linear infinite;
    animation: spin 1s linear infinite;
    border-top: 5px solid #555;
    border-radius: 50%;
    width: 50px;
    height: 50px;
    left: calc(50% - 45px);
    top: calc(50% - 0px);
}
</style>-->

<!--end of review popup-->
<script type="text/javascript" src="http://cdn.jsdelivr.net/jquery.slick/1.3.15/slick.min.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/assets/js/viewtasker.js"></script>

<?php include_once( dirname(__FILE__) . '/template/api-functions/viewtaskers.php'); ?>


