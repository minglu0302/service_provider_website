<?php 

session_start();  
if(isset($_GET['data'])){
    $token = $_GET['data'];
    $_SESSION["token"]  = $token;
    header("Location: http://iserve.ind.in/change-password/");
}else if(!isset($_SESSION["token"])){
    header("Location: http://iserve.ind.in");
}
$token = $_SESSION["token"];
?>

<?php
 /* Template Name: change password */
 ?>
 
 <link href="<?php echo get_stylesheet_directory_uri();?>/assets/css/changepassword.css" rel="stylesheet" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

 <style>
body{
 background: #ededed;
}
div#main_div {
    width: 320px;
    margin: auto;
    background: #ededed;
    padding: 180px 16px;
}
     fieldset{
         padding: 40px;
     }
button.pure-button.pure-button-primary {
       background: #00adff;
    border: 0px;
    width: 100%;
    padding: 20px;
    color: #fff;
    border-radius: 4px;
    margin: 11px 0px;
    cursor: pointer;
    font-size: 18px;
}input {
   margin: 10px 0px;
    padding: 20px 30px;
    border-radius: 4px;
}
p#error {
    color: red;
	text-align:center;
}
     p#success{
         color: green;
         text-align:center;
     }     
     
</style>

<body>
<div id="main_div">
<div class="change_password">   
<form class="pure-form">
    <fieldset>
        <legend>Change Password</legend>

        <input type="password" placeholder="Password" id="password" required>
        <input type="password" placeholder="Confirm Password" id="confirm_password" required>

        <p id="error"></p>
       
    </fieldset>		
</form>
     <button type="submit" id="submit" class="pure-button pure-button-primary">Confirm</button>   
</div>    
<p id="success"></p>

<div class="ok" style="display:none">
  <button  id="ok_button" class="pure-button pure-button-primary">ok</button>  
</div>    

</div>

<script>
$(document).ready(function(){

    
var sti = document.URL;
//var pieces = sti.split(/[?&]+/);
//var securenumber = pieces[1].split(/[=]+/);
var password = document.getElementById("password"), 
confirm_password = document.getElementById("confirm_password");
var changedPassword = "";
    
function validatePassword(){
  if(password.value != confirm_password.value) {
    $('#error').text("Passwords Don't Match");
  } else {
    changedPassword = confirm_password.value; 
    send_password();
  }
}

$("#submit").click(function(e){
 validatePassword();
});

$(".ok button").click(function(e){
     window.location.href ="http://iserve.ind.in/book-a-service/";
});    
    
$('input').focus(function(){
$("#error").text('');
});
    
//$("#routeTo").click(function(e){
// alert();
//});      

function send_password(){
  
var token = "<?php print($token); ?>"; 
        $.ajax({
            type: 'GET',
            url: 'http://iserve.ind.in:7002/slave/forgetPassword/'+changedPassword+'/'+token, 
            dataType: 'json',
            headers: {'accept': 'application/json', 'content-type': 'application/json'},
            success: function(res) {
               if(res.errFlag==1&&res.errNum==24)
			   {
                 $(".change_password").hide("slow");   
			     $(".ok").show(); 
                  $('#success').text(res.errMsg);  
			   }else if(res.errFlag==1){
                   $('#error').text(res.errMsg);
               }
			   else{
                 window.location.href ="http://iserve.ind.in/book-a-service/";	    
			   }
            }
        });
     
  
}
    

    
   
  
});
</script>
    

</body>
</html>



