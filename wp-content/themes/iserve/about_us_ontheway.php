<?php

/*
Template Name: About us on the way
*/

 get_header();	?>
<link href="<?php echo get_stylesheet_directory_uri();?>/css/application.css" rel="stylesheet" />
<link href="<?php echo get_stylesheet_directory_uri();?>/css/aboutus.css" rel="stylesheet" />
<link href="<?php echo get_stylesheet_directory_uri();?>/assets/css/aboutus1.css" rel="stylesheet" /> 

<div class="page_heading_container">
    <div class="aboutus_template"> 
		<div class="col-md-12">
		     <div class="page_heading_content"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="page-container">
    <div class="aboutus_default">       
        <div class="page-content">
            <div class="col-md-12">
                <div class="fullwidth">           
                    <?php if (have_posts()) : the_post(); ?>
        				<?php the_content(); ?>	
        			<?php endif; ?>	  
				</div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<?php get_footer(); ?>

<script>
$(".scroll2 ").click(function(){
    
$(".show2").show();
});


if ($(window).width() < 768) {  
   
   $('.slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    infinite: false,
    cssEase: 'linear'
}); 

}
else{
    
$('.slider').slick({
   slidesToShow: 3,
slidesToScroll: 3,
dots: true,
infinite: false,
cssEase: 'linear'
});

}  
 
 

</script>



