<?php

/*
Template Name:   Help on the way
*/
get_header('frontpage');	
?>
<link href="<?php echo get_stylesheet_directory_uri();?>/assets/css/help.css" rel="stylesheet" />
<link href="<?php echo get_stylesheet_directory_uri();?>/css/help.css" rel="stylesheet" />
<link href="<?php echo get_stylesheet_directory_uri();?>/css/help1.css" rel="stylesheet" />

<div class="page_heading_container">
    <div class="help_template">
	   <div class="col-md-12">
		    <div class="page_heading_content"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="page-container">
    <div class="help--default">
        <div class="page-content">
            <div class="col-md-12">
                <div class="fullwidth">           
                    <?php if (have_posts()) : the_post(); ?>
        				<?php the_content(); ?>	
        			<?php endif; ?>	  
				</div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<?php get_footer(); ?>