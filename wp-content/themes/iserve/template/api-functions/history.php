
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.1/socket.io.js"></script>
<script type="text/javascript">




   jQuery(document).ready(function($) { 
        var socket = io('http://iserve.ind.in:9999');
		   socket.emit('track', {
                        pid: 336
                    });
        socket.on('track', function (data) { 
			console.log('track');
            console.log(data);
        });	
		socket.on('LiveBookingResponce', function (data) {
			console.log('LiveBookingResponce');
            console.log("bookiingrespn",data);
			var bid = data._id;
			var popup_type;
			switch (data.status) {
			case 5:
				popup_type = "#6633FF";
				break;
			case 21:
				popup_type = "orange";
				break;
			case 22:
				popup_type = "#00FF33";
				break;
			case 6:
				popup_type = "#00CCFF";
				break;
			case 7:
				popup_type = "#00CC00";
				break;
			case 2:
				popup_type = "#FF9933";
				break;
			case 1:
				popup_type = "#00CCFF";
			case 3:
				popup_type = "#CC0000";
				   
		}
		
		$('.booking_status_msg_'+data._id).text(data.msg);
		if(data.status==7){
			//$('.booking_bid_div'+data._id).remove();
			$('.booking_bid_div'+data._id).addClass('remove_animate');
		}
		$('#my-toast-location').toastee({
			type: "success",
			header: data.msg,
			message: 'Bid:' + data._id +"\n"+"Provider-Name:"+data.provider.email,    
			width: 250,
            height: 100,
			background: popup_type,
			fadeout: 5000
		});
        });	
        $('#datetimepicker6').datetimepicker({format: 'YYYY-MM-DD'});
        $('#datetimepicker7').datetimepicker({
            useCurrent: false, format: 'YYYY-MM-DD' //Important! See issue #1075
        });
      /*   $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        }); */
    });

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      function initMap() {
        var origin_place_id = null;
        var destination_place_id = null;
        var travel_mode = 'WALKING';
        var map = new google.maps.Map(document.getElementById('map'), {
          mapTypeControl: false,
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13
        });
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        directionsDisplay.setMap(map);

        var origin_input = document.getElementById('origin-input');
        var destination_input = document.getElementById('destination-input');
        var modes = document.getElementById('mode-selector');

        map.controls[google.maps.ControlPosition.TOP_LEFT].push(origin_input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(destination_input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(modes);

        var origin_autocomplete = new google.maps.places.Autocomplete(origin_input);
        origin_autocomplete.bindTo('bounds', map);
        var destination_autocomplete =
            new google.maps.places.Autocomplete(destination_input);
        destination_autocomplete.bindTo('bounds', map);

        // Sets a listener on a radio button to change the filter type on Places
        // Autocomplete.
        function setupClickListener(id, mode) {
          var radioButton = document.getElementById(id);
          radioButton.addEventListener('click', function() {
            travel_mode = mode;
          });
        }
        setupClickListener('changemode-walking', 'WALKING');
        setupClickListener('changemode-transit', 'TRANSIT');
        setupClickListener('changemode-driving', 'DRIVING');

        function expandViewportToFitPlace(map, place) {
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
          }
        }

        origin_autocomplete.addListener('place_changed', function() {
          var place = origin_autocomplete.getPlace();
          if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
          }
          expandViewportToFitPlace(map, place);

          // If the place has a geometry, store its place ID and route if we have
          // the other place ID
          origin_place_id = place.place_id;
          route(origin_place_id, destination_place_id, travel_mode,
                directionsService, directionsDisplay);
        });

        destination_autocomplete.addListener('place_changed', function() {
          var place = destination_autocomplete.getPlace();
          if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
          }
          expandViewportToFitPlace(map, place);

          // If the place has a geometry, store its place ID and route if we have
          // the other place ID
          destination_place_id = place.place_id;
          route(origin_place_id, destination_place_id, travel_mode,
                directionsService, directionsDisplay);
        });

        function route(origin_place_id, destination_place_id, travel_mode,
                       directionsService, directionsDisplay) {
          if (!origin_place_id || !destination_place_id) {
            return;
          }
          directionsService.route({
            origin: {'placeId': origin_place_id},
            destination: {'placeId': destination_place_id},
            travelMode: travel_mode
          }, function(response, status) {
            if (status === 'OK') {
              directionsDisplay.setDirections(response);
            } else {
              window.alert('Directions request failed due to ' + status);
            }
          });
        }
      }


   // $(".container").hide();
    $(".invoice_section").hide();
    $(".pasttasks").click(function()
    {
    	$(".pasttasks").addClass("addcft1");
    	$(".current_tasks").removeClass("addcft");
      
      $(".current_tasker").slideUp( "slow", function() {
                    
      });
      $(".container, .past_tasker").slideDown( "slow", function() {
               // Animation complete.
              
      });

      $(".invoice_section").show();

    });

    $(".current_tasks").click(function()
    {
      $(".past_tasker, .container").slideUp( "slow", function() {
                    
      });
    	$(".pasttasks").removeClass("addcft1");
    	$(".current_tasks").addClass("addcft");
        $(".current_tasker").slideDown( "slow", function() {
               // Animation complete.
              
        });
        $(".invoice_section").hide();
    });
    var ent_start_date ="";
	var ent_end_date="";
	var provider_name="";
	var loading_booking_length_past = 0;
	var past_book_length = 1;
    $("#search_bookings").click(function(event){
		event.stopPropagation();
		var start_date = $("#start_date").val();
		var end_date = $("#end_date").val();
	    if(end_date==''||start_date=='')
	    {
		  if(start_date==''){
			  $("#start_date").addClass('has-error');
		  }
	     else{
			 $("#start_date").removeClass('has-error');
			 $("#end_date").addClass('has-error');
		 }
	    }
		else{
			$(".form-control").removeClass('has-error');
			ent_start_date =start_date;
			ent_end_date = end_date;
			$('.past_count').remove();
			$('.padding_sector1').empty();
			loading_booking_length_past = 0;
			past_book_length =1;
			provider_name="";
			loadPastbookings();
		}
	});
	
		$(document).on('click', '.generate_invoice', function(){ 
		var bid = $(this).attr('data-attribute');
    	getInvoice(bid);
});

	$(document).on('click', '.hide_details', function(){ 
    	var m = $(this).attr('data-attribute');
		$('#toggle_'+m).hide( "slow", function() {});
        $("#toggle_show"+m).show();
        $("#toggle_hide"+m).hide();
       console.log($(this).parent().siblings());
}); 
	$(document).on('click', '.hide_details1', function(){ 
    	var m = $(this).attr('data-attribute');
		$('#toggle_'+m).show( "slow", function() {});
        $("#toggle_show"+m).hide();
        $("#toggle_hide"+m).show();
        console.log($(this).parent().siblings());
});
var loading_booking_length = 0;
var present_book_lng =1;
	$('.ddddd').on('click', function() {	

});   
	        var Usertoken = localStorage.getItem('iserve_token');
			var page_index = 0;
			var acceptValue = 'application/json'; 
            var contentType = 'application/json'; 
			function call_current_bookings(){
				$(".overlay").show();  
		    $.ajax({
	            type: 'GET',
              	url: object_name.apiUrl+'currentbookings/'+Usertoken+'/'+present_book_lng,
              	dataType: 'json',
              	headers: {'accept': acceptValue, 'content-type': contentType},
	            success: function(response) {
					console.log("currentbookings",response);
					
					if(response.errFlag==0){
				    loading_booking_length = response.data.current_bookings.length + loading_booking_length;
					$('.show_now_book').text(loading_booking_length);
					$('.show_now_full').text(response.data.penCount);
					$('.current_tasks').prepend('<div class="pencount_current">'+response.data.penCount+'</div>');
                    if(response.data.current_bookings.length!=0){
					$(".showing_deatils_block").show();	
                    if(loading_booking_length==response.data.penCount){
                        $("#load_more").attr('disabled',true);
                    }
					for(var book_inc=0;book_inc<response.data.current_bookings.length;book_inc++)
					{
                        console.log(loading_booking_length,response.data.penCount);
                    
					var data = response.data.current_bookings[book_inc];
					console.log(data.pro_fname); 
                    var display_assign;
					if(response.data.pro_fname==undefined){
                        console.log("inloop");
						display_assign="display_not_assigned"
					}
                    else{
                        display_assign='';
                    }
					var appt_date = moment(data.appt_date).format('dddd, MMMM Do, YYYY h:mm:ss A');
					 if(data.desc==null){
						 data.desc="Not available";
					 }
					 
	            	  $('.padding_sector').append('<div class="struture_whole booking_bid_div'+data.bid+'" id="booking_blocks"><div class="displayhide" ><div class="general_setting"><span class="generalhandling_view">' + data.cat_name + '</span> <span class="generalhandling_view2"> Booking ID : <span class="bid_num">' + data.bid + '</span></span><span class="booking_status2"> <span class="booking_sta_code booking_status_msg_'+data.bid+'">'+data.statusText+'</span></span></div><div class="whole_ee_serwct" id="toggle_'+data.bid+'"><div class="booking_status '+display_assign+'"><span class="location_photo_tasker1"> You have booked+data.pro_fname++data.pro_lname</span></div><div class="datepicker_section"><div class="time_packed"><span class="date_selection">'+appt_date+'</span></div></div><div class="location_min"><span class="location_date"><img src="http://iserve.ind.in/wp-content/uploads/2017/03/map-marker.png">  Location </span><br><span class="city_name">'+data.address1+'</span></div><div class="taskername '+display_assign+'"><span class="tasker_name">Tasker</span><br><span class="tasker_name1">+pro_fname++pro_lname</span></div><div class="Description_section"><p style="color: #b4b8bd;">Description</p><p>'+data.desc+'</p></div></div></div><div class="hide_details" id="toggle_hide'+data.bid+'" data-attribute="'+data.bid+'"><span class="sitsh">Hide Details <i class="fa fa-chevron-up" aria-hidden="true"></i> </span></div><div class="hide_details1" id="toggle_show'+data.bid+'" style="display: none;"  data-attribute="'+data.bid+'"><span class="sitsh">Show Details  <i class="fa fa-chevron-down" aria-hidden="true"></i> </span></div></div>');
					  if(book_inc==response.data.current_bookings.length-1)
					  {
						
					  }
					 }
					}
					else{
						$(".showing_deatils_block").hide();
						$('.padding_sector').append('<div class="no_booking_parent"><span><img src="http://iserve.ind.in/wp-content/uploads/2017/05/sad-2.png" height="150" width="150" ></span><br><span id="no_bookings">No Bookings available</span></div>');
					}
					$(".overlay").hide();  
					}
	            }
	    });}
		
	
        $("#provider_by_name_search").click(function(){
			if($("#search_booking_name").val()!='')
			{
				$("#search_booking_name").removeClass('has-error');
				provider_name = $("#search_booking_name").val();	
				$('.past_count').remove();
     			$('.padding_sector1').empty();
	     		loading_booking_length_past = 0;
		    	past_book_length =1;
				loadPastbookings(); 
			}
			else{
				$("#search_booking_name").addClass('has-error');
			}
		});
		$('#load_more2').click(function(){
			past_book_length++;        
			$('.past_count').remove();
			loadPastbookings();
		});
		call_current_bookings();
$(".UserNames").click(function(){

});
		 
		$('#load_more').click(function(){

			present_book_lng++;    
			$(".pencount_current").remove();
			call_current_bookings();
			//$('.past_count').remove();
			//loadPastbookings();
		});
		<!--past bookings-->
		 function loadPastbookings(){
			 $(".overlay").show();  
			 $.ajax({
	            type: 'GET',
              	url: object_name.apiUrl+'pastbookings/'+Usertoken+'/'+past_book_length+'?ent_start_date='+ent_start_date+'&ent_end_date='+ent_end_date+'&ent_provider_name='+provider_name,
              	dataType: 'json',
              	headers: {'accept': acceptValue, 'content-type': contentType},
	            success: function(response) {
					console.log("pastbookings",response);
					if(response.data.past_bookings!=0){
						$('#search_booking_name').attr('disabled',false);
						$("#load_more2").attr('disabled',false);
						loading_booking_length_past = response.data.past_bookings.length + loading_booking_length_past;
					    if(loading_booking_length_past==response.data.penCount)
						{
							$("#load_more2").attr('disabled',true);
						}

					$('.show_now_book2').text(loading_booking_length_past);
					$('.show_now_full2').text(response.data.penCount);
					 
						if(response.data.penCount==undefined){       
							response.data.penCount = 0;
						$('.pasttasks').prepend('<div class="pencount_current past_count">'+response.data.penCount+'</div>')      
						}
						else{
						$('.pasttasks').prepend('<div class="pencount_current past_count">'+response.data.penCount+'</div>')	
						}
						if(response.data.past_bookings.length!=undefined){
					for(var book_inc=0;book_inc<response.data.past_bookings.length;book_inc++)
					{	
					var data = response.data.past_bookings[book_inc];
					//data.address1 = get_place(data.latitude,data.longitude);
					console.log("data.address1",data.address1);
					if(data.pro_fname==undefined){
						data.pro_fname ="-NA-"
						data.pro_lname =""
					}
					var appt_date = moment(data.appt_date).format('dddd, MMMM Do, YYYY h:mm:ss A');
					 if(data.desc==null||data.desc==''){
						 data.desc="Not available";
					 }
					 var addinvoice =""
					 if(data.statusCode==7)
					 {
						 addinvoice = "show_invoice";
					 }
					 else{
						 addinvoice = "hide_invoice";
					 }
	            	  $('.padding_sector1').append('<div class="struture_whole" id="booking_blocks"><div class="displayhide" ><div class="general_setting"><span class="generalhandling_view">' + data.cat_name + '</span> <span class="generalhandling_view2"> Booking ID : <span class="bid_num">' + data.bid + '</span></span><span class="booking_status2"> <span class="booking_sta_code">'+data.statusText+'</span></span></div><div class="whole_ee_serwct" id="toggle_'+data.bid+'"><div class="Invoice_section '+addinvoice+'"><button class="invoice_section generate_invoice" data-toggle="modal" data-target="#myModal" style="display: inline-block;"  data-attribute="'+data.bid+'">VIEW INVOICE</button></div><div class="booking_status"><span class="location_photo_tasker1"> You have booked '+data.pro_fname+' '+data.pro_lname+'</span></div><div class="datepicker_section"><div class="time_packed"><span class="date_selection">'+appt_date+'</span></div></div><div class="location_min"><span class="location_date"><img src="http://iserve.ind.in/wp-content/uploads/2017/03/map-marker.png">  Location </span><br><span class="city_name">'+data.address1+'</span></div><div class="taskername"><span class="tasker_name">Tasker</span><br><span class="tasker_name1">'+data.pro_fname+' '+data.pro_lname+'</span></div><div class="Description_section"><p style="color: #b4b8bd;">Description</p><p>'+data.desc+'</p></div></div></div><div class="hide_details" id="toggle_hide'+data.bid+'" data-attribute="'+data.bid+'"><span class="sitsh">Hide Details <i class="fa fa-chevron-up" aria-hidden="true"></i> </span></div><div class="hide_details1" id="toggle_show'+data.bid+'" style="display: none;"  data-attribute="'+data.bid+'"><span class="sitsh">Show Details  <i class="fa fa-chevron-down" aria-hidden="true"></i> </span></div></div>');
					}
						$(".overlay").hide();
						}
					}
					else{
						$('.padding_sector1').append('<div class="no_booking_parent"><span><img src="http://iserve.ind.in/wp-content/uploads/2017/05/sad-2.png" height="150" width="150" ></span><br><span id="no_bookings">No Bookings available</span></div>');
					    $('.show_now_book2').text(loading_booking_length_past);
					    $('.show_now_full2').text(response.data.penCount);
						$("#load_more2").attr('disabled',true);
						$('.pasttasks').prepend('<div class="pencount_current past_count">'+response.data.penCount+'</div>')
						$('#search_booking_name').attr('disabled',true);
						$(".overlay").hide();
					}
	            }
	    });
		 }
		 loadPastbookings(); 
		 
		 <!-- GET INVOICE  -->
         function getInvoice(bid){
			 $(".overlay").show();
			 	    $.ajax({
	            type: 'GET',
              	url: object_name.apiUrl+'invoice/'+bid+'/'+Usertoken,
              	dataType: 'json',
              	headers: {'accept': acceptValue, 'content-type': contentType},
	            success: function(response) {
					console.log(response);
	            	 $("#invoice_jobid").text(response.jobId);
					 $("#invoice_booking_date").text(response.booking_date);
					 get_lat_longs(response.location.latitude,response.location.longitude);
				     for(var key in response.comp) {
						 console.log(response.comp[key]);
						 
						if(response.comp[key]=='')     
						{response.comp[key]=0;
						}
					}
					 if(response.paymentType==2){
						 $('#invoice_pay_type').text('Cash');
						 $('#invoice_pay_img').attr('src',"<?php echo get_stylesheet_directory_uri();?>/images/cash.png");
					 }
					 else{
						 $('#invoice_pay_type').text('Card');
						 $('#invoice_pay_img').attr('src',"<?php echo get_stylesheet_directory_uri();?>/images/credit-card.png");
					 }
					 $("#invoice_pro_desc").text(response.comp.pro_disc);
                     $("#invoice_pro_name,#invoice_pro_name2").text(response.providerDetails.fname+' '+response.providerDetails.lname);
					 $("#invoice_job_type").text(response.job_type); 
					 $("#invoice_mat_fee").text('$'+response.comp.mat_fees);
					 $("#invoice_misc_fee").text('$'+response.comp.misc_fees);
					 $("#invoice_hourly_fee").text('$'+response.hourly_fee);
					 $("#invoice_pro_discount").text('$'+response.comp.pro_disc);  
					 $("#invoice_app_discount").text('$'+response.coupon_discount);
                     $("#sign_img").attr('src',response.comp.sign_url);
					 if(response.job_description!=''){
					 $("#invoice_job_desc").text(response.job_description); 
					 }
					 else{
					 $("#invoice_job_desc").text("Not Available");	 
					 }
					 	 var price = 0 ;
					 if(response.services.length>0){
					
					 for(var inst=0;inst<response.services.length;inst++)
					 {
						 $('#invoice_services').append(response.services[inst].sname);
						 price = price + parseFloat(response.services[inst].sprice);
					 }
					 $("#invoice_services_price").text('$'+price);
					 }
					 else{
						 $('#invoice_services').append('--');
						 $("#invoice_services_price").text('$'+price);
					 }
					  var total = parseFloat(price)+parseFloat(response.comp.mat_fees)+parseFloat(response.comp.misc_fees)+parseFloat(response.hourly_fee) - (parseFloat(response.coupon_discount)+parseFloat(response.comp.pro_disc));
						//total = parseFloat(price)+parseFloat(response.comp.mat_fees)+parseFloat(response.comp.misc_fees)+parseFloat(response.hourly_fee);
					$("#invoice_total,#invoice_total_amt,#invoice_total_amt").text('$'+total);
					 if(response.booking_type==1)
					 {
						 $("#invoice_booking_type").text('Now');
					 }
					 else{
						 $("#invoice_booking_type").text('Later');
					 }
					 $("#invoice_pro_desc_").text(response.pro_desc);
					 $('#invoice_modal').modal('show');
					 $(".overlay").hide();
	            }
	    });
		 }		 
		 
		 <!--GET PLACE USING LAT LONG TO GENERATE INVOICE USING REVERSE GEOCODING-->
		 var geocoder = new google.maps.Geocoder;
		 function get_lat_longs(lt,lng){  
        var latlng = {lat: parseFloat(lt), lng: parseFloat(lng)};
        geocoder.geocode({'location': latlng}, function(results, status) {
          if (status === 'OK') {
            if (results[1]) {
				
				$("#invoice_job_lcoation").text(results[1].formatted_address);
            } else {
              window.alert('No results found');
            }
          } else {
            window.alert('Geocoder failed due to: ' + status);
          }
        });
      
		 }
		 
		 	 var geocoder = new google.maps.Geocoder;
		 function get_place(lt,lng){  
		 console.log(lt,lng);
        var latlng = {lat: parseFloat(lt), lng: parseFloat(lng)};
        geocoder.geocode({'location': latlng}, function(results, status) {
			console.log(results);
          if (status === 'OK') {
            if (results[1]) {
				var add = results[1].formatted_address;
				return add;
            } else {
              window.alert('No results found');
            }
          } else {
            window.alert('Geocoder failed due to: ' + status);
          }
        });
      
		 }
		 
		 <!--DOWNLOAD AS PDF FOR INVOICE-->
		
	var 
	form = $('#content'),
	cache_width = form.width(),
	a4  =[ 595.28,  841.89];  // for a4 size paper width and height

$('#cmd').on('click',function(){
	$(".overlay").show();
	$('body').scrollTop(0);
	createPDF();
});
//create pdf
function createPDF(){
	getCanvas().then(function(canvas){
		var 
		img = canvas.toDataURL("image/png"),
		doc = new jsPDF({
          unit:'px', 
          format:'a4'
        });     
        doc.addImage(img, 'JPEG', 20, 20);
        doc.save('Invoice-iserve.pdf');
        form.width(cache_width);
		 $(".overlay").hide();
	});
}

// create canvas object
function getCanvas(){
	form.width((a4[0]*1.33333) -80).css('max-width','none');
	return html2canvas(form,{
    	imageTimeout:2000,
		useCORS:false,
    	removeContainer:false
    });	
}
		
		
</script>

		 




					
						
						
							
							

							
						
					
					
				
				