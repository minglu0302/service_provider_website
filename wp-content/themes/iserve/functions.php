<?php

function wpt_theme_styles() {
    wp_enqueue_style('styles_css', get_template_directory_uri().'/css/styles.css', array(), null, 'all');
    wp_enqueue_style('appli_css', get_template_directory_uri().'/css/application.css', array(), null, 'all');
    wp_enqueue_style('home_css', get_template_directory_uri().'/css/hamepage.css', array(), null, 'all');
    //wp_enqueue_style('theme_css', get_template_directory_uri().'/css/theme.css', array(), null, 'all');
    wp_enqueue_style('theme_style_css', get_template_directory_uri().'/style.css', array(), null, 'all'); 
    wp_enqueue_style('emailvalidation_css', get_template_directory_uri().'/css/calendar.css', array(), null, 'all');
    wp_enqueue_style('emailvalidation1_css', get_template_directory_uri().'/css/datess.css', array(), null, 'all');
    wp_enqueue_style('calendar', get_template_directory_uri().'/public/css/style.css', array(), null, 'all');
    wp_enqueue_style('calendar1', get_template_directory_uri().'/public/css/bootstrap.css', array(), null, 'all');
    wp_enqueue_style('googlefont_css', 'http://fonts.googleapis.com/css?family=Asap:400,700,400italic,700italic', array(), null, 'all'); 
    wp_enqueue_style('main_css', 'https://jqueryvalidation.org/files/demo/site-demos.css', array(), null, 'all'); 
}
add_action('wp_enqueue_scripts', 'wpt_theme_styles');
function ju_enqueue(){ 
     wp_enqueue_script('oxfam_js_cookie', 'https://code.jquery.com/jquery-1.11.1.min.js', array('jquery'), null, false);
	 wp_enqueue_script('oxfam_js_cookie0', 'http://cdn.pubnub.com/pubnub-3.16.5.min.js', array('jquery'), null, false);
    wp_enqueue_script('oxfam_js_cookie1', 'https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js', array('jquery'), null, false);
    wp_enqueue_script('oxfam_js_cookie2', 'https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js', array('jquery'), null, false);
    wp_enqueue_script('oxfam_js_cookie3',  get_template_directory_uri().'/js/emailvalidation.js', array('jquery'), null, false);
    wp_enqueue_script('oxfam_js_cookie2', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'), null, false);
    wp_enqueue_script('oxfam_js_cookie4',  get_template_directory_uri().'/js/scripts.js', array('jquery'), null, false);
    wp_enqueue_script('oxfam_js_cookie5',  get_template_directory_uri().'/js/jquery.simple-dtpicker.js', array('jquery'), null, false);
    wp_enqueue_script('calendar3',  get_template_directory_uri().'/public/javascript/jquery-1.12.0.js', array('jquery'), null, false);
     wp_enqueue_script('calendar4',  get_template_directory_uri().'/public/javascript/zebra_datepicker.js', array('jquery'), null, false);
      wp_enqueue_script('calendar5',  get_template_directory_uri().'/public/javascript/core.js', array('jquery'), null, false);
}
add_action('wp_enqueue_scripts', 'ju_enqueue');

add_action( 'wp_default_scripts', function( $scripts ) {
    if ( ! empty( $scripts->registered['jquery'] ) ) {
        $jquery_dependencies = $scripts->registered['jquery']->deps;
        $scripts->registered['jquery']->deps = array_diff( $jquery_dependencies, array( 'jquery-migrate' ) );
    }
} );
  function register_primary_menu() {
    register_nav_menu('primary', __('Primary Menu'));
}
add_action('init', 'register_primary_menu');  

 function register_mobile_menu() {
    register_nav_menu('mobile', __('Mobile Menu'));
}
add_action('init', 'register_mobile_menu');  

function register_second_menu() {
    register_nav_menu('second', __('Second Menu'));
}
add_action('init', 'register_second_menu'); 

function add_menuclass($ulclass) {
   return preg_replace('/<a /', '<a class="header__navigation-link"', $ulclass);
}
add_filter('wp_nav_menu','add_menuclass');

// Add Api url's
  wp_register_script( 'my-script', 'myscript_url' );
  wp_enqueue_script( 'my-script' );

  $translation_array = array( 
      'apiUrl' => 'http://iserve.ind.in:7002/slave/',
      'masterapiUrl' => 'http://iserve.ind.in:7002/master/',   
      'channel' => 'iserve_channel',
      'webUrl' => 'http://iserve.ind.in/'
  );
//after wp_enqueue_script
  
wp_localize_script( 'my-script', 'object_name', $translation_array );

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'solor_ind ';
    }
    return $classes;
}

include_once( dirname(__FILE__) . '/widget/all-widgets.php');