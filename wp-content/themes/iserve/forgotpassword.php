<?php  
/*
Template Name: FORGOT PASSWORD
*/
get_header();	
	
?>
  <link href="<?php echo get_stylesheet_directory_uri();?>/assets/css/login.css" rel="stylesheet" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php  echo get_stylesheet_directory_uri(); ?>/css/intlTelInput.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="<?php  echo get_stylesheet_directory_uri(); ?>/js/intlTelInput.js"></script>
  <script src="<?php  echo get_stylesheet_directory_uri(); ?>/js/utils.js"></script>
<div class="page_heading_container">
    <div class="Howitswork_template">
        <div class="col-md-12">
	        <div class="page_heading_content"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="page-container">
    <div class="howitswork_default">   
       <div class="page-content">
            <div class="col-md-12">
                <div class="fullwidth">           
                    <?php if (have_posts()) : the_post(); ?>
        		    <?php the_content(); ?>	
        		    <?php endif; ?>	  
		        </div>	
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>



<link href="http://cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.min.css" rel="stylesheet"/>
<script src="http://cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.jquery.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri();?>/js/intlTelInput.js"></script>

<script>

$(".mobile_hide_pp").hide();
$(".mail_pp").hide();
$(".rest_option").hide();



$(".phone_number_reset").click(function(){
    $(".mobile_hide_pp").show();
    $(".mail_pp").hide();
     $(".rest_option").show();
    if($(".btn").hasClass("forgotemail")){
        $(".btn").removeClass("forgotemail");
        
    }
    $(".btn").addClass("forgotphone");
    $(".btn").attr("id","emailPhone");
});
    
$(".e-mail_reset").click(function(){
    $(".mobile_hide_pp").hide();
    $(".mail_pp").show();
    $(".rest_option").show();
    if($(".btn").hasClass("forgotphone")){
        $(".btn").removeClass("forgotphone");
    }
    $(".btn").addClass("forgotemail");
    $(".btn").attr("id","emailforg");
});
 
    
    
    
 $("#phone").intlTelInput({
       nationalMode: false,
       initialCountry: "auto",
       geoIpLookup: function(callback) {
    $.get('http://ipinfo.io', function() {}, "jsonp").always(function(resp) {
      var countryCode = (resp && resp.country) ? resp.country : "";
      callback(countryCode);
    });  },
      utilsScript: ""
    });   
    
    
    
    
    
    
 $("#emailforg").bind("keyup change", function(e){
        $(".rightcolorf").hide();
       $(".u-full-width").removeClass("error");
       var emailforg = $("#emailforg").val();
        var email_regex = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
      if(!emailforg.match(email_regex) || emailforg.length == 0){
            $("#emailforg").addClass("error");           
       $(".rightcolorf").hide();        
      }else{
           $(this).next('i').show();
      }  
  });   
 
    
 $("#phone").bind("keyup change", function(e){
    var phone = $("#phone").val();        
    var patt = /^\s*(?:\+?(\d{1,3}))?[- (]*(\d{3})[- )]*(\d{3})[- ]*(\d{4})(?: *[x/#]{1}(\d+))?\s*$/;
    if(!phone.match(patt) || phone.length == 0){
            $("#phone").addClass("error");           
       $(".rightcolorf").hide();        
      }else{
           $(this).next('i').show();
      } 
 });
    
    
// Forgot password API
    
 $('.rest_option').on("click", ".forgotphone", function(){    
//$(".forgotemail").click(function(e){
 
    var acceptValue = 'application/json'; 
    var contentType = 'application/json'; 
    $(".u-full-width").removeClass("error");
     
    var phone = $("#phone").val();
    var country_code = $(".selected-dial-code").text(); 
    var phone_code =country_code;
     
     localStorage.setItem('countryphone',phone);
      localStorage.setItem('countryphone_code',country_code);
     localStorage.setItem('code_type',2); 
     
     var patt = /^\s*(?:\+?(\d{1,3}))?[- (]*(\d{3})[- )]*(\d{3})[- ]*(\d{4})(?: *[x/#]{1}(\d+))?\s*$/;
    if(!phone.match(patt) || phone.length == 0){
       $("#phone").addClass("error");           
    }
    else{  
      $.ajax({
        type: 'GET',
        url: object_name.apiUrl+'resetWithPhone/'+country_code+"/"+phone,
        dataType: 'json',
        headers: {'accept': acceptValue, 'content-type': contentType},
        success: function(response) {
          console.log("verification"+response);
          if(response.errFlag == 1){ 
              $(".forerror").text(response.errMsg);
              $(".forsuccess").text("");
                             
          }
          else{
              console.log(response);
                             
              $(".forerror").text("");
              $(".forsuccess").text(response.errMsg);
              window.location.href="/confirm-phone-number/";
          }
                                      
        }
      });     
    
        }
});
  
      
   
 $('.rest_option').on("click", ".forgotemail", function(){    
//$(".forgotemail").click(function(e){
 
    var acceptValue = 'application/json'; 
    var contentType = 'application/json'; 
    $(".u-full-width").removeClass("error");

    var emailforg = $("#emailforg").val();
    var email_regex = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if(!emailforg.match(email_regex) || emailforg.length == 0){

       $("#emailforg").addClass("error");           
    }
    else{  
      $.ajax({
        type: 'GET',
        url: object_name.apiUrl+'resetWithPhone/'+emailforg,
        dataType: 'json',
        headers: {'accept': acceptValue, 'content-type': contentType},
        success: function(response) {
          console.log("verification"+response);
          if(response.errFlag == 1){ 
              $(".forerror").text(response.errMsg);
              $(".forsuccess").text("");
                             
          }
          else{
              console.log(response);
                             
              $(".forerror").text("");
              $(".forsuccess").text(response.errMsg);
          }
                                      
        }
      });     
    
        }
});    
    
    
    
    
    
    
    

    
    
    
    
    
    
    
    
</script>

<style type="text/css">
input#emailforg {
    margin-top: 15px;
}
  .rightcolorlogemail{
    display: none;
    position: absolute;
    right: 10px;
    top: 30px;
}
.rightcolorlogpass{
    display: none;
    position: absolute;
    right: 10px;
    top: 30px;
}
.rightcolorsiginname{
    display: none;
    position: absolute;
    right: 10px;
    top: 30px;
}
.rightcolorsiginemail{
    display: none;
    position: absolute;
    right: 10px;
    top: 30px;
}
.rightcolorsiginnumb{
    position: absolute;
    right: 10px;
    top: 30px;
}
.rightcolorsiginpass{
    display: none;
    position: absolute;
    right: 10px;
    top: 30px;
}
.rightcolorf {
    display: none;
    position: absolute;
    right: 8px;
    top: 5px;
}
div#signup_in_signin button {
    background: #33b786;
    border: 0px;
    padding: 10px;
    width: 150px;
    color: #fff;
    border-radius: 5px;
    margin-top: 10px
}
div#signup_in_signin {
    text-align: center;
}
.errordatas{
    margin: 10px;
}
div#login_in_user {
    text-align: center;
}
div#login_in_user button {
    background: #33b786;
    border: 0px;
    padding: 10px;
    width: 150px;
    color: #fff;
    border-radius: 5px;
    margin-top: 10px;
}

#fountainG{
    position:relative;
    width:234px;
    height:28px;
    margin:auto;
}

.fountainG{
    position:absolute;
    top:0;
    background-color:rgb(0,0,0);
    width:28px;
    height:28px;
    animation-name:bounce_fountainG;
        -o-animation-name:bounce_fountainG;
        -ms-animation-name:bounce_fountainG;
        -webkit-animation-name:bounce_fountainG;
        -moz-animation-name:bounce_fountainG;
    animation-duration:1.5s;
        -o-animation-duration:1.5s;
        -ms-animation-duration:1.5s;
        -webkit-animation-duration:1.5s;
        -moz-animation-duration:1.5s;
    animation-iteration-count:infinite;
        -o-animation-iteration-count:infinite;
        -ms-animation-iteration-count:infinite;
        -webkit-animation-iteration-count:infinite;
        -moz-animation-iteration-count:infinite;
    animation-direction:normal;
        -o-animation-direction:normal;
        -ms-animation-direction:normal;
        -webkit-animation-direction:normal;
        -moz-animation-direction:normal;
    transform:scale(.3);
        -o-transform:scale(.3);
        -ms-transform:scale(.3);
        -webkit-transform:scale(.3);
        -moz-transform:scale(.3);
    border-radius:19px;
        -o-border-radius:19px;
        -ms-border-radius:19px;
        -webkit-border-radius:19px;
        -moz-border-radius:19px;
}

#fountainG_1{
    left:0;
    animation-delay:0.6s;
        -o-animation-delay:0.6s;
        -ms-animation-delay:0.6s;
        -webkit-animation-delay:0.6s;
        -moz-animation-delay:0.6s;
}

#fountainG_2{
    left:29px;
    animation-delay:0.75s;
        -o-animation-delay:0.75s;
        -ms-animation-delay:0.75s;
        -webkit-animation-delay:0.75s;
        -moz-animation-delay:0.75s;
}

#fountainG_3{
    left:58px;
    animation-delay:0.9s;
        -o-animation-delay:0.9s;
        -ms-animation-delay:0.9s;
        -webkit-animation-delay:0.9s;
        -moz-animation-delay:0.9s;
}

#fountainG_4{
    left:88px;
    animation-delay:1.05s;
        -o-animation-delay:1.05s;
        -ms-animation-delay:1.05s;
        -webkit-animation-delay:1.05s;
        -moz-animation-delay:1.05s;
}

#fountainG_5{
    left:117px;
    animation-delay:1.2s;
        -o-animation-delay:1.2s;
        -ms-animation-delay:1.2s;
        -webkit-animation-delay:1.2s;
        -moz-animation-delay:1.2s;
}

#fountainG_6{
    left:146px;
    animation-delay:1.35s;
        -o-animation-delay:1.35s;
        -ms-animation-delay:1.35s;
        -webkit-animation-delay:1.35s;
        -moz-animation-delay:1.35s;
}

#fountainG_7{
    left:175px;
    animation-delay:1.5s;
        -o-animation-delay:1.5s;
        -ms-animation-delay:1.5s;
        -webkit-animation-delay:1.5s;
        -moz-animation-delay:1.5s;
}

#fountainG_8{
    left:205px;
    animation-delay:1.64s;
        -o-animation-delay:1.64s;
        -ms-animation-delay:1.64s;
        -webkit-animation-delay:1.64s;
        -moz-animation-delay:1.64s;
}



@keyframes bounce_fountainG{
    0%{
    transform:scale(1);
        background-color:rgb(0,0,0);
    }

    100%{
    transform:scale(.3);
        background-color:rgb(255,255,255);
    }
}

@-o-keyframes bounce_fountainG{
    0%{
    -o-transform:scale(1);
        background-color:rgb(0,0,0);
    }

    100%{
    -o-transform:scale(.3);
        background-color:rgb(255,255,255);
    }
}

@-ms-keyframes bounce_fountainG{
    0%{
    -ms-transform:scale(1);
        background-color:rgb(0,0,0);
    }

    100%{
    -ms-transform:scale(.3);
        background-color:rgb(255,255,255);
    }
}

@-webkit-keyframes bounce_fountainG{
    0%{
    -webkit-transform:scale(1);
        background-color:rgb(0,0,0);
    }

    100%{
    -webkit-transform:scale(.3);
        background-color:rgb(255,255,255);
    }
}

@-moz-keyframes bounce_fountainG{
    0%{
    -moz-transform:scale(1);
        background-color:rgb(0,0,0);
    }

    100%{
    -moz-transform:scale(.3);
        background-color:rgb(255,255,255);
    }
}
</style>