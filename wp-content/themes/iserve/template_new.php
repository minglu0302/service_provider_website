<?php

/*
Template Name:TEMPLATE_NEW
*/
get_header();	
?>
<script type="text/javascript">
jQuery(document).ready(function($) { 
	 $(".slider_img1, .slider_img2, .slider_img3,.slider_img4,.slider_img5").hide();
});
</script>
<style>

.sticky {
    padding: 10px;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    background-color: rgba(255, 255, 255, 0.98);
    box-shadow: 0 2px 2px #efefef;
    z-index: 9999;
}
.vc_custom_1492696506865 {
    background-repeat: no-repeat;
    background-size: 100% 100% !important;
    height: 100vh !important;
    margin: 3px auto 0;
    position: relative;
}
.col-md-12{
padding:0;}
.vc_column-inner {
    padding-top: 0px !important;
}
</style>
<?php 
  function my_shortcode() {
    ob_start();
    ?>
   <?php include_once( dirname(__FILE__) . '/template/homeslidershort.php'); ?>
     <?php
    return ob_get_clean();
  }
  add_shortcode( 'mobslider', 'my_shortcode' );
?>

<link href="<?php echo get_stylesheet_directory_uri();?>/assets/css/home.css" rel="stylesheet" /> 
<div class="page_heading_container">
    <div class=" template"> 
	    <div class="col-md-12 ">
		  <div class="page_heading_content">
		  </div>
        </div>

        <div class="clear"></div>
    </div>
</div>
<div class="page-container">
    <div class="full_landing">
        <div class="page-content">
            <div class="col-md-12">
                <div class="fullwidth">           
                    <?php if (have_posts()) : the_post(); ?>
        				<?php the_content(); ?>	
        			<?php endif; ?>	  
				</div>
            </div>
           <div class="clear"></div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/assets/js/homepage.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/3.1.1/jquery.waypoints.min.js"></script>

<script>
$(document).ready(function(){
$('.section-sticky').waypoint(function(direction) {
        if (direction == "down") {
            $('.changedemo').addClass('sticky');
        } else {
            $('.changedemo').removeClass('sticky');
           $('.close-sticky').removeClass('sticky');
        }
    },{
      offset: '0px;'
    });
});
</script>
<script>
$(document).ready(function(){
$('.close-sticky').waypoint(function(direction) {
        if (direction == "down") {
            $('.changedemo').removeClass('sticky');
        } 
    },{
      offset: '0px;'
    });
});
</script>

<script>
$(document).ready(function(){
$('.close-sticky').waypoint(function(direction) {
        if (direction == "up") {
            $('.changedemo').addClass('sticky');
        } 
    },{
      offset: '0px;'
    });
});
</script>

