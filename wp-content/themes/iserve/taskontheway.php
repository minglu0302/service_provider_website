<?php

/*
Template Name:  Task api on the way
*/
get_header('frontpage');	
?>
<link href="<?php echo get_stylesheet_directory_uri();?>/assets/css/tasker.css" rel="stylesheet" />
<link href="<?php echo get_stylesheet_directory_uri();?>/css/api.css" rel="stylesheet" />
<style>


</style>

<div class="page_heading_container">
    <div class="task_template">
		<div class="col-md-12">
		    <div class="page_heading_content">
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="page-container">
    <div class="task--default">
            <div class="page-content">
                <div class="col-md-12">
                    <div class="fullwidth">           
			            <?php if (have_posts()) : the_post(); ?>
							<?php the_content(); ?>	
						<?php endif; ?>	  
				    </div>
                </div>
                <div class="clear"></div>
            </div>
    </div>
</div>

<!--header part menu-->

<div id="map"></div>
<div data-pie-id="227" data-page-state="form">
	<div class="js-progress-container">
	    <div class="build-progress" data-pie-id="269">
			<div class="build-progress__container container--wide">

				<div class="build-progress__step js-step" data-page="form" data-state="current">
					<i class="fa fa-pencil"></i>
					<span>1.</span>
					<span>Fill Out Task Details</span>
				</div>
				<div class="build-progress__step js-step" data-page="recommendations" data-state="future">
					<i class="ss-smile"></i>
					<span>2.</span>
					<span>View Taskers &amp; Prices</span>
				</div>
				<div class="build-progress__step js-step" data-page="confirm" data-state="future">
					<i class="ss-checkmark"></i>
					<span>3.</span>
					<span>Confirm &amp; Book</span>
				</div>

			</div>

		</div>
	</div>														
<!--end of header part menu-->

<div class="js-trust-banner-container">
    <div class="build-trust-banner box-no-padding" data-pie-id="297">
		<div class="build-trust-banner__container container--wide">
		    <img alt="Trust badge" class="build-trust-banner__badge trust" src="http://iserve.ind.in/wp-content/uploads/2017/03/trust_badge.png">
		    <div class="build-trust-banner__title">
		        <strong>Trust &amp; Safety Guarantee</strong>
		    </div>
			<a class="build-trust-banner__info u-hidden--sm u-hidden--md u-hidden--lscp js-tt-trigger" data-tt-contenttemplate="build.trustBannerTooltip" data-tt-placement="b" href="#">
			
			</a>
		</div>

	</div>
</div>
<div class="js-child-container">
    <div class="page-loader"></div>
    <div data-pie-id="440">
        <div class="view--build-main view--body">
            <div class="container--wide build-container row">
                <div class="js-subheader-container col-12 col-lg-10 col--centered">
                    <div data-pie-id="467">
                        <div class="build-subheader build-subheader--form">
                            <div class="build-subheader__title catnames">
                                Iserve

                                <a class="build-subheader__mod-link js-change-template popup_confirm" href="#">Change</a>

                            </div>
							<div class="supplemental_infos">

							</div>
                        </div>

                    </div>
                </div>
                <div class="js-items-container col-12 col-lg-10 col--centered">
                    <div class="build-group panel js-error-target build-form-input is-focused" data-pie-id="505" data-key="" data-field-name="location_group" data-field-type="group" data-state="empty" data-editing="true">
                        <h3 class="build-group__title">Your Task Location</h3>
                        <div class="build-group__summary js-summary catSummary" style="display: none;">
                        	<ul>
                        	    <li><i class="fa fa-map-marker"></i>
                        	        <span class="addressval">Beverly Hills, CA, United States
	                        	        <span class="build__location-validation u-pull--desk-right u-pull--lscp-right">Good news! Iserve is available in your area
	                        	        </span>
                        	        </span>
                        	    </li>
                        	</ul>
                        </div>
                       <div class="build-group__items-container js-form-container catSummary1" style="height: auto; display: block;">
                            <div class="js-items-container">
                                <div data-pie-id="596" class="js-error-target build-form-input" data-key="location.freeform location.address2 location.lat location.lng location.virtual" data-field-name="location" data-field-type="address">

                                    <div class="row row--guttered">
                                        <div class="col-12 col-md-8 col-lg-10">
                                            <input class="js-autocomplete" name="location.freeform" id="address" placeholder="Enter street address" type="text" value="" autocomplete="off">
                                            <div class="adderror" style="display: none;">Shoot! This task is outside of our coverage area.</div>
                                        </div>
										<div class="col-12 col-md-4 col-lg-2">
										    <input class="ignored-error" id="address-595-address2" name="location.address2" placeholder="Unit or Apt #" type="text" value="">
										</div>
                                    </div>

                                </div>
                            </div>
							<div class="build-group__cta">
							    <button class="btn btn-primary js-group-cta continue_blue open_pop" id="catDetails" type="submit">Continue</button>
							</div>
                        </div>
						<i class="build-group__status-icon"></i>
                   </div>
                   <div class="build-group panel js-error-target build-form-input" data-pie-id="606" data-key="" data-field-name="description_group" data-field-type="group" data-state="empty" data-editing="false">
                        <h3 class="build-group__title">Tell Us About Your Task</h3>
                        <div class=""moretaskers>If you need two or more Taskers, please post additional tasks for each Tasker needed.</div>
                        <div class="build-group__summary js-summary"></div>
                        <div data-pie-id="686" class="js-error-target build-form-input display_Replace" data-key="description" data-field-name="description" data-field-type="text">
							<textarea name="description" class="cusdescription" placeholder="EXAMPLE: We have a few lightbulbs that need replacing. We’ve got the lightbulbs, but we’d need you to provide the ladder."></textarea>
                        </div>
						<div class="build-group__cta next_page">
						    
						        <button class="btn btn-primary js-group-cta continue_blue" id="continueSend" type="submit">Continue</button>
						    
							<!--<div class="build-group__cta-subtext">
							     Next: See available Taskers
							</div>-->
						</div>
                    </div>
                    <i class="build-group__status-icon"></i>
                </div>
            </div>
        </div>
    </div>

</div>
</div>

<!--popup code for confirm Navigation-->
<div class="lightbox--container" data-pie-id="733">
   <div class="lightbox--background js-lightbox-bg js-dismiss gone_class"></div>
	<div class="lightbox--internal">
		  <div class="lightbox--content-wrapper">
			    <div class="lightbox--title js-lightbox-title">Confirm Navigation</div>
			    <div class="lightbox--content js-lightbox-content">
				    <div class="page-loader"></div>
				    <div data-pie-id="726" class="confirm-lightbox">
				        <div class="row confirm-lightbox--content-row">
				            <div class="col-12 centy">
				                <span>Changing your task type will erase everything entered on this page. Continue?</span>
				            </div>
				        </div>
				        <div class="row row--guttered confirm-lightbox--button-row row--thin">
				            <div class="col-12 centy">
				                <button class="js-confirm btn btn-primary close">OK</button>
				                <button class="js-deny btn close">Cancel</button>
				            </div>
				        </div>
				    </div>
			    </div>
			    <a href="#" class="lightbox--dismiss js-lightbox-dismiss js-dismiss close">
			        <i class="fa fa-times"></i>
			    </a>
		  </div>
	</div>
</div>

<!--popup code for confirm Navigation-->


<!--script for map autocomplete-->


<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script>

<?php include_once( dirname(__FILE__) . '/template/api-functions/tasker.php'); ?>
