<?php

/*
Template Name: footer_onway
*/
	
?>
<footer>
<div class="row social-row">
<div class="container--wide">
<span class="footer-title social-title">Follow us! We're friendly: </span>
<a class="social-link" href="https://www.facebook.com/TaskRabbit" target="_blank"><img alt="Facebook" src="https://d31ebqhycylygn.cloudfront.net/v3/assets/web/icons/facebook-white-c3a6136eef265690fc96b6d0d4ab06e4.svg"></a>
|
<a class="social-link" href="https://twitter.com/taskrabbit" target="_blank"><img alt="Twitter" src="https://d31ebqhycylygn.cloudfront.net/v3/assets/web/icons/twitter-white-3079b06bb5d6e9db5d69a649abc70a32.svg"></a>
|
<a class="social-link" href="https://www.instagram.com/taskrabbit/" target="_blank"><img alt="Instagram" src="https://d31ebqhycylygn.cloudfront.net/v3/assets/web/icons/instagram-white-8371c5be5bffe9b46f865c33738d4187.svg"></a>
</div>
</div>
<div class="row footer-row">
<div class="container--wide footer-content">
<div class="footer-column">
<span class="footer-title">Discover</span>
<div class="links">
<a class="box-no-padding" href="/how-it-works">How TaskRabbit Works</a>
<a class="box-no-padding" href="/become-a-tasker">Become a Tasker</a>
<a class="box-no-padding" href="/taskrabbit-elite">The TaskRabbit Elite</a>
<a class="box-no-padding" href="/account/gift-cards">Buy a Gift Card</a>
<a class="box-no-padding" href="https://taskrabbit.zendesk.com/home" target="_blank">Help</a>
</div>
</div>
<div class="footer-column">
<span class="footer-title">Company</span>
<div class="links">
<a class="box-no-padding" href="/about">About Us</a>
<a class="box-no-padding" href="/careers">Careers</a>
<a class="box-no-padding" href="/press">Press</a>
<a class="box-no-padding" href="http://blog.taskrabbit.com/" target="_blank">Blog</a>
<a class="box-no-padding inline-links" href="/terms">Terms</a>
<span class="amp box-no-padding">&amp;</span>
<a class="box-no-padding inline-links" href="/privacy">Privacy</a>
</div>
</div>
<div class="footer-column">
<div class="mobile-apps">
<div class="footer-title">Download our app</div>
<p class="apps-message">Tackle your to-do list wherever you are with our mobile app.</p>
<a class="box-no-padding appstore-badge" href="https://itunes.apple.com/us/app/goclean-service/id1331343919?mt=8" target="_blank">
<img alt="Download on the App Store" height="45" src="https://d31ebqhycylygn.cloudfront.net/v3/assets/web/icons/appstore_badge-b38296811f274beb5cf9e8811fd0ea73.svg">
</a>
<a class="box-no-padding appstore-badge" href="https://play.google.com/store/apps/details?id=com.gocleanservice.customer&hl=en" target="_blank">
<img alt="Get it on Google Play" height="45" src="https://d31ebqhycylygn.cloudfront.net/v3/assets/web/icons/google_play_badge-f22ef50734324ff62305edcefa657fc0.svg">
</a>
</div>
</div>
</div>
</div>
</footer>