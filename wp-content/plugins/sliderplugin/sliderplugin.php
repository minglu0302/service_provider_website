<?php
/*
Plugin Name: SliderPLugin
Plugin URI: http://rachelmccollin.co.uk
Description: This plugin provides a shortcode to list posts, with parameters. It also registers a couple of post types and tacxonomies to work with.
Version: 1.0
Author: santhosh
Author URI: http://appscrip.com/
License: GPLv2
*/
// register custom post type to work with
add_action( 'init', 'rmcc_create_post_type' );
function rmcc_create_post_type() {  // clothes custom post type
    // set up labels
    $labels = array(
        'name' => 'About Slider',
        'singular_name' => 'Slider Item',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Slider Item',
        'edit_item' => 'Edit Slider Item',
        'new_item' => 'New Slider Item',
        'all_items' => 'All Slider',
        'view_item' => 'View Slider Item',
        'search_items' => 'Search Slider',
        'not_found' =>  'No Slider Found',
        'not_found_in_trash' => 'No Slider found in Trash',
        'parent_item_colon' => '',
        'menu_name' => 'Slider',
    );
    register_post_type(
        'abslider',
        array(
            'labels' => $labels,
            'has_archive' => true,
            'public' => true,
            'hierarchical' => true,
            'supports' => array( 'title', 'editor', 'excerpt', 'custom-fields', 'thumbnail','page-attributes' ),
            'taxonomies' => array( 'post_tag', 'category' ),
            'exclude_from_search' => true,
            'capability_type' => 'post',
        )
    );
}
 
// create shortcode to list all clothes which come in blue
add_shortcode( 'list-posts-basic', 'rmcc_post_listing_shortcode1' );
function rmcc_post_listing_shortcode1( $atts ) {
    ob_start();
    $query = new WP_Query( array(
        'post_type' => 'abslider',
        'posts_per_page' => -1,
        'order' => 'ASC',
        'orderby' => 'title',
    ) );
    ?>
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
    <?php 
    if ( $query->have_posts() ) { ?>
         <section class="static-section about-stats">
            <div class="container tr-carousel tr-carousel--about u-visible--sm u-visible--md">
                <div class="row row--thin tr-carousel__slide js-carousel-slide slider" data-slide="1" style="opacity: 1;">
			        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
			        	<div class="col-xs-12 col-lg-4 u-align--center stat-outer-wrap">
		                    <div class="stat-container">
		                        <img alt="Bag" class="stat-icon" src="<?php the_post_thumbnail_url(); ?>">
		                        <div class="stat-factoid"><?php the_title(); ?></div>
		                        <div class="stat-sub"><?php the_content(); ?></div>
		                    </div>
		                </div>
			        <?php endwhile;
			        wp_reset_postdata(); ?>
			    </div> 
            </div>
        </section> 
    <?php $myvariable = ob_get_clean();
    return $myvariable;
    }
}
?>
