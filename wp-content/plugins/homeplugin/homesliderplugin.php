<?php
/*
Plugin Name: Home Slider PLugin
Plugin URI: http://appscrip.com/
Description: This plugin provides a shortcode to list posts, with parameters. It also registers a couple of post types and tacxonomies to work with.
Version: 1.0
Author: santhosh
Author URI: http://appscrip.com/
License: GPLv2
*/
// register custom post type to work with
add_action( 'init', 'rmcc_create_post_types3' );
function rmcc_create_post_types3() {  // clothes custom post type
    // set up labels
    $labels = array(
        'name' => 'Home Slider',
        'singular_name' => 'Home Slider Item',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Home Slider Item',
        'edit_item' => 'Edit Home Slider Item',
        'new_item' => 'New Home Slider Item',
        'all_items' => 'All Home Slider',
        'view_item' => 'View Home Slider Item',
        'search_items' => 'Search Home Slider',
        'not_found' =>  'No Home Slider Found',
        'not_found_in_trash' => 'No Work Slider found in Trash',
        'parent_item_colon' => '',
        'menu_name' => 'Home Slider',
    );
    register_post_type(
        'homelider',
        array(
            'labels' => $labels,
            'has_archive' => true,
            'public' => true,
            'hierarchical' => true,
            'supports' => array( 'title', 'editor', 'excerpt', 'custom-fields', 'thumbnail','page-attributes' ),
            'taxonomies' => array( 'post_tag', 'category' ),
            'exclude_from_search' => true,
            'capability_type' => 'post',
        )
    );
}
 
// create shortcode to list all clothes which come in blue
add_shortcode( 'list-posts-basichs', 'rmcc_post_listing_shortcode3' );
function rmcc_post_listing_shortcode3( $atts ) {
    ob_start();
    $query = new WP_Query( array(
        'post_type' => 'homelider',
        'posts_per_page' => -1,
        'order' => 'ASC',
        'orderby' => 'title',
    ) );
    ?>

                               
                                
    <?php 
    if ( $query->have_posts() ) {  ?>
    <div class="slideshow-container">
        <div class="background_image_section">
            <img src="http://www.goclean-service.com/wp-content/uploads/2017/12/mobile_mockup.png">
        </div>
    	<?php while ( $query->have_posts() ) : $query->the_post(); ?>
            <div class="mySlides fade">
                <div class="numbertext"></div>
                <img class="mob_size" src="<?php the_post_thumbnail_url(); ?>" style="width:250px">
                <div class="text"> 
                    <div style="text-transform:uppercase;">  <?php the_title(); ?>  </div>
                        <?php remove_filter( 'the_content', 'wpautop' );?>
                    <p class="under_button"> <?php the_content(); ?></p>
                </div>
            </div>  
                    
        <?php endwhile;
		 wp_reset_postdata(); ?>  
        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next" onclick="plusSlides(1)">&#10095;</a>
    </div>
      

    <?php $myvariable = ob_get_clean();
    return $myvariable;
    }
}
?>
