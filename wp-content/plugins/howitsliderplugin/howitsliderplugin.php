<?php
/*
Plugin Name: How it Slider PLugin
Plugin URI: http://appscrip.com/
Description: This plugin provides a shortcode to list posts [list-posts-basicw], with parameters. It also registers a couple of post types and tacxonomies to work with .
Version: 1.0
Author: 3Embed
Author URI: http://appscrip.com/
License: GPLv2
*/
// register custom post type to work with
add_action( 'init', 'rmcc_create_post_types' );
function rmcc_create_post_types() {  // clothes custom post type
    // set up labels
    $labels = array(
        'name' => 'How its Work Slider',
        'singular_name' => 'Work Slider Item',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Work Slider Item',
        'edit_item' => 'Edit Work Slider Item',
        'new_item' => 'New Work Slider Item',
        'all_items' => 'All Work Slider',
        'view_item' => 'View Work Slider Item',
        'search_items' => 'Search Work Slider',
        'not_found' =>  'No Work Slider Found',
        'not_found_in_trash' => 'No Work Slider found in Trash',
        'parent_item_colon' => '',
        'menu_name' => 'How It Work Slider',
    );
    register_post_type(
        'hwslider',
        array(
            'labels' => $labels,
            'has_archive' => true,
            'public' => true,
            'hierarchical' => true,
            'supports' => array( 'title', 'editor', 'excerpt', 'custom-fields', 'thumbnail','page-attributes' ),
            'taxonomies' => array( 'post_tag', 'category' ),
            'exclude_from_search' => true,
            'capability_type' => 'post',
        )
    );
}
 
// create shortcode to list all clothes which come in blue
add_shortcode( 'list-posts-basicw', 'rmcc_post_listing_shortcode2' );
function rmcc_post_listing_shortcode2( $atts ) {
    ob_start();
    $query = new WP_Query( array(
        'post_type' => 'hwslider',
        'posts_per_page' => -1,
        'order' => 'ASC',
        'orderby' => 'title',
    ) );
    ?>
    <?php 
    if ( $query->have_posts() ) { ?>
         <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->

                <!-- Wrapper for slides -->
                <div class="carousel-inner slider" role="listbox">
                   
    			    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                        <div class="item ">
                            <div class="image_On">
    			                <img class="scrolling_in sliderhtcimg" src=" <?php the_post_thumbnail_url(); ?>" alt="Chania" width="50" height="50">
    		                </div>      
    		              <div class="title_slide_ON"><?php the_title(); ?></div>
                          <?php remove_filter( 'the_content', 'wpautop' );?>
                             <p class="title_slider_text"><?php the_content(); ?></p>
                        </div>
    			    <?php endwhile;
			            wp_reset_postdata(); ?>
			       
                </div>
        </div>

    <?php $myvariable = ob_get_clean();
    return $myvariable;
    }
}
?>
