<?php
/*
Plugin Name: Testimonial Pro
Description: This plugin will enable Testimonial in your WordPress site.
Plugin URI: http://shapedplugin.com/plugin/testimonial-pro
Author: ShapedPlugin
Author URI: http://shapedplugin.com
Version: 1.9
*/


/* Define */
define( 'SP_TP_URL', plugins_url( '/' ) . plugin_basename( dirname( __FILE__ ) ) . '/' );
define( 'SP_TP_PATH', plugin_dir_path( __FILE__ ) );


if ( file_exists( SP_TP_PATH . 'inc/functions.php' ) ) {
	require_once( SP_TP_PATH . "inc/functions.php" );
}
/* Including files */
if ( file_exists( SP_TP_PATH . 'inc/scripts.php' ) ) {
	require_once( SP_TP_PATH . "inc/scripts.php" );
}