<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly


// Testimonial Pro shortcode
function sp_testimonial_pro_shortcode( $atts ) {
	extract( shortcode_atts( array(
		'items'                => '3',
		'items_desktop'        => '3',
		'items_desktop_small'  => '3',
		'items_tablet'         => '2',
		'items_mobile'         => '1',
		'total_items'          => '25',
		'color'                => '#53b5d8',
		'pagination'           => 'true',
		'nav'                  => 'true',
		'auto_play'            => 'true',
		'auto_height'          => 'false',
		'stop_on_hover'        => 'true',
		'rating'               => 'true',
		'category'             => '',
		'order_by'             => 'date',
		'order'                => 'DESC',
		'theme'                => 'theme-one',
		'image_style'          => 'two',
		'read_more'            => 'false',
		'read_more_limit'      => '120',
		'read_more_text'       => 'Read More',
		'read_more_close_text' => 'Close',
		'width'                => '100',
		'height'               => '100',
		'crop'                 => 'true',
		'slides_to_scroll'     => '1',
		'speed'                => '300',
		'auto_play_speed'      => '3000',
		'rtl'                  => 'false',
	), $atts, 'testimonial-pro' ) );


	if ( isset( $category ) && $category != '' ) {
		$args = array(
			'post_type'       => 'spt_testimonial',
			'orderby'         => $order_by,
			'order'           => $order,
			'testimonial_cat' => $category,
			'posts_per_page'  => $total_items,
		);
	} else {
		$args = array(
			'post_type'      => 'spt_testimonial',
			'orderby'        => $order_by,
			'order'          => $order,
			'posts_per_page' => $total_items,
		);
	}

	$que = new WP_Query( $args );

	// Pagination Query
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	if ( isset( $category ) && $category != '' ) {
		$args = array(
			'post_type'       => 'spt_testimonial',
			'orderby'         => $order_by,
			'order'           => $order,
			'testimonial_cat' => $category,
			'posts_per_page'  => $total_items,
			'paged'           => $paged
		);
	} else {
		$args = array(
			'post_type'      => 'spt_testimonial',
			'orderby'        => $order_by,
			'order'          => $order,
			'posts_per_page' => $total_items,
			'paged'          => $paged
		);
	}

	$que_page = new WP_Query( $args, $paged );


	$id = uniqid();

	$outline = '';

	$outline .= '<style type="text/css">
				#sp-testimonial-pro' . $id . ' .testimonial-pro .read-more a.more-link{
					color: ' . $color . ';
				}
				</style>';


	$outline .= '
	    <script type="text/javascript">
	            jQuery(document).ready(function() {
				jQuery("#sp-testimonial-pro' . $id . '").slick({
			        infinite: true,
			        slidesToShow: ' . $items . ',
			        arrows: ' . $nav . ',
			        prevArrow: "<div class=\'slick-prev\'><i class=\'fa fa-angle-left\'></i></div>",
	                nextArrow: "<div class=\'slick-next\'><i class=\'fa fa-angle-right\'></i></div>",
			        dots: ' . $pagination . ',
			        pauseOnHover: ' . $stop_on_hover . ',
			        autoplay: ' . $auto_play . ',
			        adaptiveHeight: ' . $auto_height . ',
			        
			        slidesToScroll: ' . $slides_to_scroll . ',
			        speed: ' . $speed . ',
			        autoplaySpeed: ' . $auto_play_speed . ',
			        rtl: ' . $rtl . ',
		            
		            
		            responsive: [
						    {
						      breakpoint: 1199,
						      settings: {
						        slidesToShow: ' . $items_desktop . '
						      }
						    },
						    {
						      breakpoint: 979,
						      settings: {
						        slidesToShow: ' . $items_desktop_small . '
						      }
						    },
						    {
						      breakpoint: 767,
						      settings: {
						        slidesToShow: ' . $items_tablet . '
						      }
						    },
						    {
						      breakpoint: 479,
						      settings: {
						        slidesToShow: ' . $items_mobile . '
						      }
						    }
						  ]
		        });

		    });
	    </script>';

	if ( $read_more == 'true' && $theme !== 'theme-six' || $read_more == 'true' && $theme !== 'theme-seven' ) {
		$outline .= '
	    <script type="text/javascript">
	    jQuery(document).ready(function() {
			jQuery("#sp-testimonial-pro' . $id . ' div.tp-client-testimonial").expander({
				slicePoint: ' . $read_more_limit . ',
				widow: 2,
				userCollapseText: "' . $read_more_close_text . '",
				expandText: "' . $read_more_text . '"
			});

	    });
	    </script>';
	}


	if ( $theme == 'theme-six' ) {

		$outline .= '<style type="text/css">
				#sp-tp' . $id . '.testimonial-pro-six-area .tp-pagination li span.current,
				#sp-tp' . $id . '.testimonial-pro-six-area .tp-pagination li a:hover{
					background-color: ' . $color . ';
				}
				</style>';

		if ( $read_more == 'true' && $theme == 'theme-six' ) {
			$outline .= '
		    <script type="text/javascript">
		    jQuery(document).ready(function() {
				jQuery("#sp-tp' . $id . ' div.tp-client-testimonial").expander({
					slicePoint: ' . $read_more_limit . ',
					widow: 2,
					userCollapseText: "' . $read_more_close_text . '",
					expandText: "' . $read_more_text . '"
				});

		    });
		    </script>';
		}

		$outline .= '<div id="sp-tp' . $id . '" class="testimonial-pro-six-area masonry_testimonial tp-container ' . $theme . '">';
		if ( $que_page->have_posts() ): while ( $que_page->have_posts() ) : $que_page->the_post();

			$allowedtags_add = array(
				'a'      => array(
					'href'  => array(),
					'title' => array(),
				),
				'b'      => array(),
				'br'     => array(),
				'em'     => array(),
				'i'      => array(),
				'strong' => array(),
				'span'   => array(
					'class' => true,
					'id'    => true,
				),
				'p'      => array(
					'class' => true,
					'id'    => true,
				),
				'h5'     => array(
					'class' => true,
					'id'    => true,
				),
				'h4'     => array(
					'class' => true,
					'id'    => true,
				),
				'h3'     => array(
					'class' => true,
					'id'    => true,
				)
			);
			$tps_designation = get_post_meta( get_the_ID(), 'tp_designation', true );
			$tp_designation  = wp_kses( $tps_designation, $allowedtags_add );
			$tp_rating       = esc_html( get_post_meta( get_the_ID(), 'tp_rating', true ) );
			$tp_five_star    = '<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star" aria-hidden="true"></i>
						';
			$tp_four_star    = '
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star-o" aria-hidden="true"></i>
						';
			$tp_three_star   = '
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star-o" aria-hidden="true"></i>
						<i class="fa fa-star-o" aria-hidden="true"></i>
						';
			$tp_two_star     = '
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star-o" aria-hidden="true"></i>
						<i class="fa fa-star-o" aria-hidden="true"></i>
						<i class="fa fa-star-o" aria-hidden="true"></i>
						';
			$tp_one_star     = '
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star-o" aria-hidden="true"></i>
						<i class="fa fa-star-o" aria-hidden="true"></i>
						<i class="fa fa-star-o" aria-hidden="true"></i>
						<i class="fa fa-star-o" aria-hidden="true"></i>
						';


			$outline .= '<div class="tp-col-lg-' . $items . ' tp-col-md-' . $items_desktop . ' tp-col-sm-' . $items_tablet . ' tp-col-xs-' . $items_mobile . ' testimonial-pro">';

			$outline .= '<div class="tp-client-testimonial">';
			$outline .= get_the_content();
			$outline .= '</div>';

			$tp_thumb   = get_post_thumbnail_id();
			$tp_img_url = wp_get_attachment_url( $tp_thumb, 'full' );
			$tp_image   = aq_resize( $tp_img_url, $width, $height, $crop );

			if ( has_post_thumbnail( $que_page->post->ID ) ) {
				$outline .= '<div class="tp-client-image tp-image-style-' . $image_style . '">';
				if ( $tp_image ) {
					$outline .= '<img src="' . $tp_image . '" alt="' . get_the_title() . '">';
				} else {
					$outline .= '<img src="' . $tp_img_url . '" alt="' . get_the_title() . '">';
				}
				$outline .= '</div>';
			}
			$outline .= '<h2 class="tp-client-name">';
			$outline .= get_the_title();
			$outline .= '</h2>';
			if ( $tp_designation ) {
				$outline .= '<h6 class="tp-client-designation">';
				$outline .= $tp_designation;
				$outline .= '</h6>';
			}

			if ( $rating == 'true' && $tp_rating ) {
				$outline .= '<div class="tp-client-rating">';
				if ( $tp_rating == 'five_star' ) {
					$outline .= $tp_five_star;
				} elseif ( $tp_rating == 'four_star' ) {
					$outline .= $tp_four_star;
				} elseif ( $tp_rating == 'three_star' ) {
					$outline .= $tp_three_star;
				} elseif ( $tp_rating == 'two_star' ) {
					$outline .= $tp_two_star;
				} elseif ( $tp_rating == 'one_star' ) {
					$outline .= $tp_one_star;
				}
				$outline .= '</div>';
			}

			$outline .= '</div>'; //testimonial pro


		endwhile;
			wp_reset_postdata();
			// Custom query loop pagination
			$outline .= '<div class="tp-col-lg-1 sp-clear">';
			if ( $que_page->max_num_pages > 1 ) {
				$big   = 999999999; // need an unlikely integer
				$items = paginate_links( array(
					'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format'    => '?paged=%#%',
					'prev_next' => true,
					'current'   => max( 1, get_query_var( 'paged' ) ),
					'total'     => $que_page->max_num_pages,
					'type'      => 'array',
					'prev_text' => '<i class="fa fa-angle-left"></i>',
					'next_text' => '<i class="fa fa-angle-right"></i>'
				) );

				$pagination = "<ul class=\"tp-pagination\">\n\t<li>";
				$pagination .= join( "</li>\n\t<li>", $items );
				$pagination . "</li>\n</ul>\n";

				$outline .= $pagination;
			}

			$outline .= '</div>';


		endif;
		$outline .= '</div>'; //tp-container

	} elseif ( $theme == 'theme-seven' ) {

		$outline = '';

		$outline .= '<style type="text/css">
				#sp-tp-filter' . $id . '.testimonial-pro-seven-area .sp-testimonial-filter li a:hover,
				#sp-tp-filter' . $id . '.testimonial-pro-seven-area .sp-testimonial-filter li a.active{
					background-color: ' . $color . ';
				}
				</style>';

		if ( $read_more == 'true' && $theme == 'theme-seven' ) {
			$outline .= '
		    <script type="text/javascript">
		    jQuery(document).ready(function() {
				jQuery("#sp-tp-filter' . $id . ' div.tp-client-testimonial").expander({
					slicePoint: ' . $read_more_limit . ',
					widow: 2,
					userCollapseText: "' . $read_more_close_text . '",
					expandText: "' . $read_more_text . '"
				});

		    });
		    </script>';
		}

		$outline .= '<div id="sp-tp-filter' . $id . '" class="testimonial-pro-seven-area tp-container" >';

		if ( ! is_tax() ) {

			$terms = get_terms( "testimonial_cat" );

			$count = count( $terms );

			if ( $count > 0 ) {

				$outline .= '<div class="text-center">';
				$outline .= '<ul class="sp-testimonial-filter">';

				$outline .= '<li>
											<a class="active" href="#" data-filter="*">' . esc_html__( 'All', 'testimonial-pro' )
				            . '</a>
										</li>';

				foreach ( $terms as $term ) {

					$outline .= '<li><a href="#" data-filter=".testimonial_cat-' . $term->slug . '">' . $term->name . '</a></li>';

				}

				$outline .= '</ul>';
				$outline .= '</div>';

			}
		}


		$outline .= '<div class="sp-isotope-testimonial-items">';


		$args = array(
			'post_type'      => 'spt_testimonial',
			'orderby'        => $order_by,
			'order'          => $order,
			'posts_per_page' => $total_items
		);
		$loop = new WP_Query( $args );
		?>
		<?php
		if ( $loop->have_posts() ) {
			while ( $loop->have_posts() ) : $loop->the_post();


				$allowedtags_add = array(
					'a'      => array(
						'href'  => array(),
						'title' => array(),
					),
					'b'      => array(),
					'br'     => array(),
					'em'     => array(),
					'i'      => array(),
					'strong' => array(),
					'span'   => array(
						'class' => true,
						'id'    => true,
					),
					'p'      => array(
						'class' => true,
						'id'    => true,
					),
					'h5'     => array(
						'class' => true,
						'id'    => true,
					),
					'h4'     => array(
						'class' => true,
						'id'    => true,
					),
					'h3'     => array(
						'class' => true,
						'id'    => true,
					)
				);
				$tps_designation = get_post_meta( get_the_ID(), 'tp_designation', true );
				$tp_designation  = wp_kses( $tps_designation, $allowedtags_add );
				$tp_rating       = esc_html( get_post_meta( get_the_ID(), 'tp_rating', true ) );
				$tp_five_star    = '<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star" aria-hidden="true"></i>
						';
				$tp_four_star    = '
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star-o" aria-hidden="true"></i>
						';
				$tp_three_star   = '
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star-o" aria-hidden="true"></i>
						<i class="fa fa-star-o" aria-hidden="true"></i>
						';
				$tp_two_star     = '
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star-o" aria-hidden="true"></i>
						<i class="fa fa-star-o" aria-hidden="true"></i>
						<i class="fa fa-star-o" aria-hidden="true"></i>
						';
				$tp_one_star     = '
						<i class="fa fa-star" aria-hidden="true"></i>
						<i class="fa fa-star-o" aria-hidden="true"></i>
						<i class="fa fa-star-o" aria-hidden="true"></i>
						<i class="fa fa-star-o" aria-hidden="true"></i>
						<i class="fa fa-star-o" aria-hidden="true"></i>
						';


				$outline .= '<div class=" ' . join( ' ', get_post_class( 'tp-col-lg-' . $items . ' tp-col-md-' . $items_desktop . ' tp-col-sm-' . $items_tablet . ' tp-col-xs-' . $items_mobile . ' testimonial-pro' ) ) . '">';

				$outline .= '<div class="tp-client-testimonial">';
				$outline .= get_the_content();
				$outline .= '</div>';

				$tp_thumb   = get_post_thumbnail_id();
				$tp_img_url = wp_get_attachment_url( $tp_thumb, 'full' );
				$tp_image   = aq_resize( $tp_img_url, $width, $height, $crop );

				if ( has_post_thumbnail( $loop->post->ID ) ) {
					$outline .= '<div class="tp-client-image tp-image-style-' . $image_style . '">';
					if ( $tp_image ) {
						$outline .= '<img src="' . $tp_image . '" alt="' . get_the_title() . '">';
					} else {
						$outline .= '<img src="' . $tp_img_url . '" alt="' . get_the_title() . '">';
					}
					$outline .= '</div>';
				}
				$outline .= '<h2 class="tp-client-name">';
				$outline .= get_the_title();
				$outline .= '</h2>';
				if ( $tp_designation ) {
					$outline .= '<h6 class="tp-client-designation">';
					$outline .= $tp_designation;
					$outline .= '</h6>';
				}

				if ( $rating == 'true' && $tp_rating ) {
					$outline .= '<div class="tp-client-rating">';
					if ( $tp_rating == 'five_star' ) {
						$outline .= $tp_five_star;
					} elseif ( $tp_rating == 'four_star' ) {
						$outline .= $tp_four_star;
					} elseif ( $tp_rating == 'three_star' ) {
						$outline .= $tp_three_star;
					} elseif ( $tp_rating == 'two_star' ) {
						$outline .= $tp_two_star;
					} elseif ( $tp_rating == 'one_star' ) {
						$outline .= $tp_one_star;
					}
					$outline .= '</div>';
				}

				$outline .= '</div>'; //testimonial pro


			endwhile;

		}


		$outline .= '</div>';
		$outline .= '</div>';


	} else {
		$outline .= '<style type="text/css">
				#sp-testimonial-pro' . $id . '.sp-testimonial-section .slick-next:hover,
				#sp-testimonial-pro' . $id . '.sp-testimonial-section .slick-prev:hover{
					color: ' . $color . ';
				}
				#sp-testimonial-pro' . $id . '.sp-testimonial-section ul.slick-dots li.slick-active button{
					background-color: ' . $color . ';
				}
				</style>';
		$outline .= '<div ';
		if ( $nav == 'false' ) {
			$outline .= 'style="padding: 0;"';
		}
		$outline .= 'id="sp-testimonial-pro' . $id . '" class="sp-testimonial-section ' . $theme . '">';


		if ( $que->have_posts() ) {
			while ( $que->have_posts() ) : $que->the_post();

				$allowedtags_add = array(
					'a'      => array(
						'href'  => array(),
						'title' => array(),
					),
					'b'      => array(),
					'br'     => array(),
					'em'     => array(),
					'i'      => array(),
					'strong' => array(),
					'span'   => array(
						'class' => true,
						'id'    => true,
					),
					'p'      => array(
						'class' => true,
						'id'    => true,
					),
					'h5'     => array(
						'class' => true,
						'id'    => true,
					),
					'h4'     => array(
						'class' => true,
						'id'    => true,
					),
					'h3'     => array(
						'class' => true,
						'id'    => true,
					)
				);
				$tps_designation = get_post_meta( get_the_ID(), 'tp_designation', true );
				$tp_designation  = wp_kses( $tps_designation, $allowedtags_add );
				$tp_rating       = esc_html( get_post_meta( get_the_ID(), 'tp_rating', true ) );
				$tp_five_star    = '<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star" aria-hidden="true"></i>
					';
				$tp_four_star    = '
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star-o" aria-hidden="true"></i>
					';
				$tp_three_star   = '
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star-o" aria-hidden="true"></i>
					<i class="fa fa-star-o" aria-hidden="true"></i>
					';
				$tp_two_star     = '
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star-o" aria-hidden="true"></i>
					<i class="fa fa-star-o" aria-hidden="true"></i>
					<i class="fa fa-star-o" aria-hidden="true"></i>
					';
				$tp_one_star     = '
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star-o" aria-hidden="true"></i>
					<i class="fa fa-star-o" aria-hidden="true"></i>
					<i class="fa fa-star-o" aria-hidden="true"></i>
					<i class="fa fa-star-o" aria-hidden="true"></i>
					';

				if ( $theme == 'theme-two' || $theme == 'theme-three' || $theme == 'theme-four' ) {

					$outline .= '<div class="testimonial-pro">';

					$outline .= '<div class="tp-client-testimonial">';
					$outline .= get_the_content();
					$outline .= '</div>';

					$tp_thumb   = get_post_thumbnail_id();
					$tp_img_url = wp_get_attachment_url( $tp_thumb, 'full' );
					$tp_image   = aq_resize( $tp_img_url, $width, $height, $crop );

					if ( has_post_thumbnail( $que->post->ID ) ) {
						$outline .= '<div class="tp-client-image tp-image-style-' . $image_style . '">';
						if ( $tp_image ) {
							$outline .= '<img src="' . $tp_image . '" alt="' . get_the_title() . '">';
						} else {
							$outline .= '<img src="' . $tp_img_url . '" alt="' . get_the_title() . '">';
						}
						$outline .= '</div>';
					}

					$outline .= '<h2 class="tp-client-name">';
					$outline .= get_the_title();
					$outline .= '</h2>';
					if ( $tp_designation ) {
						$outline .= '<h6 class="tp-client-designation">';
						$outline .= $tp_designation;
						$outline .= '</h6>';
					}

					if ( $rating == 'true' && $tp_rating ) {
						$outline .= '<div class="tp-client-rating">';
						if ( $tp_rating == 'five_star' ) {
							$outline .= $tp_five_star;
						} elseif ( $tp_rating == 'four_star' ) {
							$outline .= $tp_four_star;
						} elseif ( $tp_rating == 'three_star' ) {
							$outline .= $tp_three_star;
						} elseif ( $tp_rating == 'two_star' ) {
							$outline .= $tp_two_star;
						} elseif ( $tp_rating == 'one_star' ) {
							$outline .= $tp_one_star;
						}
						$outline .= '</div>';
					}

					$outline .= '</div>'; //testimonial pro

				} else {
					$outline .= '<div class="testimonial-pro text-center">';

					$tp_thumb   = get_post_thumbnail_id();
					$tp_img_url = wp_get_attachment_url( $tp_thumb, 'full' );
					$tp_image   = aq_resize( $tp_img_url, $width, $height, $crop );

					if ( has_post_thumbnail( $que->post->ID ) ) {
						$outline .= '<div class="tp-client-image tp-image-style-' . $image_style . '">';
						if ( $tp_image ) {
							$outline .= '<img src="' . $tp_image . '" alt="' . get_the_title() . '">';
						} else {
							$outline .= '<img src="' . $tp_img_url . '" alt="' . get_the_title() . '">';
						}
						$outline .= '</div>';
					}
					$outline .= '<div class="tp-client-testimonial"';
					if ( $theme == 'theme-five' && has_post_thumbnail( $que->post->ID ) ) {
						$outline .= 'style="padding-top: 44px;"';
					}

					$outline .= '>';
					$outline .= get_the_content();
					$outline .= '</div>';
					$outline .= '<h2 class="tp-client-name">';
					$outline .= get_the_title();
					$outline .= '</h2>';
					if ( $tp_designation ) {
						$outline .= '<h6 class="tp-client-designation">';
						$outline .= $tp_designation;
						$outline .= '</h6>';
					}

					if ( $rating == 'true' && $tp_rating ) {
						$outline .= '<div class="tp-client-rating">';
						if ( $tp_rating == 'five_star' ) {
							$outline .= $tp_five_star;
						} elseif ( $tp_rating == 'four_star' ) {
							$outline .= $tp_four_star;
						} elseif ( $tp_rating == 'three_star' ) {
							$outline .= $tp_three_star;
						} elseif ( $tp_rating == 'two_star' ) {
							$outline .= $tp_two_star;
						} elseif ( $tp_rating == 'one_star' ) {
							$outline .= $tp_one_star;
						}
						$outline .= '</div>';
					}

					$outline .= '</div>'; //testimonial pro
				}

			endwhile;
		} else {
			$outline .= '<h2 class="sp-not-found-any-testimonial">' . esc_html__( 'No testimonials found', 'testimonial-pro' ) . '</h2>';
		}
		$outline .= '</div>';


		wp_reset_query();
	}


	return $outline;


}

add_shortcode( 'testimonial-pro', 'sp_testimonial_pro_shortcode' );


// Testimonial Pro form shortcode
function sp_testimonial_pro_form_shortcode( $atts ) {
	extract( shortcode_atts( array(
		'designation_required' => 'false',
		'photo_required'       => 'false',
		'rating'               => 'true',
		'category'             => 'true',
		'photo'                => 'true',
		'designation'          => 'true',
		'text_name'            => 'Your Name',
		'text_email'           => 'Your Email',
		'text_designation'     => 'Designation or Company Name',
		'text_photo'           => 'Upload Your Photo',
		'text_category'        => 'Select Category',
		'text_testimonial'     => 'Write Your Testimonial',
		'text_success_massage' => 'Thank you for submitting a new testimonial!',
		'text_rating'          => 'Select Star Rating',
		'text_no_star'         => 'No Rating',
		'text_one_star'        => '1 Star',
		'text_two_star'        => '2 Stars',
		'text_three_star'      => '3 Stars',
		'text_four_star'       => '4 Stars',
		'text_five_star'       => '5 Stars',
		'text_submit'          => 'Submit Testimonial',
		'color'                => '#52b3d9'
	), $atts, 'testimonial-pro-form' ) );


	$outline = '';

	$outline .= "<style type='text/css'>
				.tp-fronted-form input[type='submit']{
					background: " . $color . ";
    				border-color: " . $color . ";
				}
				</style>";

	// Get the value of a settings field
	global $testimonial_pro_default_options;
	$settings = get_option( 'testimonial_pro_default_options', $testimonial_pro_default_options );

	// testimonial submit form
	if ( 'POST' == $_SERVER['REQUEST_METHOD'] && ! empty( $_POST['action'] ) && $_POST['action'] == "testimonial_form" ) {

		// Do some minor form validation to make sure there is content
		if ( isset ( $_POST['tp_your_name'] ) ) {
			$tp_your_name = wp_strip_all_tags( $_POST['tp_your_name'] );
		} else {
			$tp_your_name = '';
		}

		if ( isset ( $_POST['tp_your_testimonial'] ) ) {
			$tp_your_testimonial = $_POST['tp_your_testimonial'];
		} else {
			$tp_your_testimonial = '';
		}

		if ( isset ( $_POST['tp_client_rating'] ) ) {
			$tp_client_rating = $_POST['tp_client_rating'];
		} else {
			$tp_client_rating = '';
		}

		if ( isset ( $_POST['testimonial-cat'] ) ) {
			$testimonial_cat = $_POST['testimonial-cat'];
		} else {
			$testimonial_cat = '';
		}

		if ( isset ( $_POST['tp_designation'] ) ) {
			$tp_designation = $_POST['tp_designation'];
		} else {
			$tp_designation = '';
		}


		// ADD THE FORM INPUT TO $testimonial_form ARRAY
		$testimonial_form = array(
			'post_title'   => $tp_your_name,
			'post_content' => $tp_your_testimonial,
			'post_status'  => 'pending',
			'post_type'    => 'spt_testimonial',
			'tax_input'    => array(
				'testimonial_cat' => array( $testimonial_cat )
			),
			'meta_input'   => array(
				'tp_designation' => $tp_designation,
				'tp_rating'      => $tp_client_rating,
				'tp_email'       => ( $_POST['tp_email'] )
			)
		);


		//Save The Testimonial
		$pid = wp_insert_post( $testimonial_form );
		wp_set_object_terms( $pid, intval( $testimonial_cat ), 'testimonial_cat', true );


		// Client Photo
		if ( ! function_exists( 'wp_generate_attachment_metadata' ) ) {
			require_once( ABSPATH . "wp-admin" . '/includes/image.php' );
			require_once( ABSPATH . "wp-admin" . '/includes/file.php' );
			require_once( ABSPATH . "wp-admin" . '/includes/media.php' );
		}
		if ( $_FILES ) {
			foreach ( $_FILES as $file => $array ) {
				if ( $_FILES[ $file ]['error'] !== UPLOAD_ERR_OK ) {

				} else {
					$attach_id = media_handle_upload( $file, $pid );

					if ( $attach_id > 0 ) {
						//set post image
						update_post_meta( $pid, '_thumbnail_id', $attach_id );
					}
				}
			}
		}


		//Thanks
		if ( $pid ) {
			$outline .= "<p class='tp-success-massage'>" . esc_html( $text_success_massage ) . "</p>";
		}


	} // END THE IF STATEMENT THAT STARTED THE WHOLE FORM

	//POST THE POST
	do_action( 'wp_insert_post', 'wp_insert_post', true );


	$outline .= '
	<!-- Frontend Submission Form -->
	<div class="tp-fronted-form">
		<form id="testimonial_form" name="testimonial_form" method="post" action="" enctype="multipart/form-data">';
	$current_user = wp_get_current_user();
	$outline .= '
		<!-- Name -->
			<fieldset class="tp-name-set">
				<label for="tp_your_name">' . esc_html( $text_name ) . '</label>
				<input type="text" id="tp_your_name" value="' . $current_user->display_name . '" name="tp_your_name" class="required""/>
			</fieldset>';

	$outline .= '
		<!-- Email -->
			<fieldset class="tp-email-set">
				<label for="tp_email">' . esc_html( $text_email ) . '</label>
				<input type="email" id="tp_email" value="'.$current_user->user_email.'" name="tp_email" class="required"/>
			</fieldset>';


	if ( $designation == 'true' ) {
		$outline .= '<!-- Designation -->
			<fieldset class="tp-designation-set">';

		$outline .= '<label for="tp_designation">' . esc_html( $text_designation ) . '</label>
				<textarea cols="60" rows="2" name="tp_designation" id="tp_designation" class="';
		if ( $designation_required == 'true' ) {
			$outline .= 'required';
		}
		$outline .= '"></textarea>
			</fieldset>';
	}


	if ( $photo == 'true' ) {
		$outline .= '<!-- Photo -->
			<fieldset class="tp-photo-set">
				<label for="tp_your_photo">' . esc_html( $text_photo ) . '</label>
				<input type="file" name="tp_your_photo" id="tp_your_photo" class="';
		if ( $photo_required == 'true' ) {
			$outline .= 'required';
		}
		$outline .= '" accept="image/jpeg,image/jpg,image/png,">
			</fieldset>';
	}


	if ( $category == 'true' ) {
		$outline .= '<!-- Category -->
			<fieldset class="tp-category-set">
				<label for="testimonial-cat">' . esc_html( $text_category ) . '</label>';

		$tp_categorys = wp_dropdown_categories( array(
			'name'       => 'testimonial-cat',
			'id'         => 'testimonial-cat',
			'taxonomy'   => 'testimonial_cat',
			'hide_empty' => 0,
			'echo'       => 0,
			'selected'   => isset( $_POST['testimonial-cat'] ) ? sanitize_text_field( $_POST['testimonial-cat'] ) : false,
		) );

		$outline .= $tp_categorys;

		$outline .= '</fieldset>';
	}


	$outline .= '<!-- Your Testimonial -->
			<fieldset class="tp-testimonial-set">
				<label for="tp_your_testimonial">' . esc_html( $text_testimonial ) . '</label>
				<textarea id="tp_your_testimonial" class="required" name="tp_your_testimonial"></textarea>
			</fieldset>';


	if ( $rating == 'true' ) {
		$outline .= '<!-- Rating -->
			<fieldset class="tp-rating-set">';
		$rating = esc_html( get_post_meta( get_the_ID(), 'tp_rating', true ) );

		$outline .= '<label for="tp_rating">' . esc_html( $text_rating ) . '</label>';


		$options = array(
			'no_star'    => esc_html( $text_no_star ),
			'one_star'   => esc_html( $text_one_star ),
			'two_star'   => esc_html( $text_two_star ),
			'three_star' => esc_html( $text_three_star ),
			'four_star'  => esc_html( $text_four_star ),
			'five_star'  => esc_html( $text_five_star ),
		);
		if ( isset( $rating ) ) {
			$order_by = $rating;
		}

		$outline .= '<select id="tp_client_rating" name="tp_client_rating">';

		foreach ( $options as $key => $value ) {

			if ( $order_by === $key ) {
				$outline .= '<option value="' . $key . '" selected="selected">' . $value . '</option>';
			} else {
				$outline .= '<option value="' . $key . '">' . $value . '</option>';
			}
		}

		$outline .= '</select>
			</fieldset>';
	}

	if ( $settings['enable_captcha'] == 'yes' && $settings['public_key'] && $settings['secret_key'] ) {
		$outline .= '<div class="tp-verify-human"><div class="g-recaptcha" data-sitekey="' . $settings['public_key'] . '"></div></div>';
	}

	$outline .= '<fieldset class="tp-submit-set">
				<input type="submit" value="' . esc_html( $text_submit ) . '" id="submit" name="submit"/>
			</fieldset>

			<input type="hidden" name="action" value="testimonial_form"/>';
	wp_nonce_field( 'new-testimonial' );
	$outline .= '</form>
	</div> <!-- END tp-fronted-form -->';


	return $outline;

}

add_shortcode( 'testimonial-pro-form', 'sp_testimonial_pro_form_shortcode' );