<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

// Get the value of a settings field
global $testimonial_pro_default_options;
$settings = get_option( 'testimonial_pro_default_options', $testimonial_pro_default_options );
$testimonial_pro_secret_key = $settings['secret_key'];


/**
 * Scripts and styles
 */
class SP_TP_Scripts{

	/**
	 * Script version number
	 */
	protected $version;

	/**
	 * Initialize the class
	 */
	public function __construct() {
		$this->version = '20170304';

		add_action( 'wp_enqueue_scripts', array( $this, 'sp_tp_front_scripts' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'sp_tp_admin_scripts' ) );
	}

	/**
	 * Front Scripts
	 */
	public function sp_tp_front_scripts() {
		// CSS Files
		wp_enqueue_style( 'slick', SP_TP_URL . 'assets/css/slick.css', false, $this->version );
		wp_enqueue_style( 'font-awesome', SP_TP_URL . 'assets/css/font-awesome.min.css', false, $this->version );
		wp_enqueue_style( 'testimonial-pro-style', SP_TP_URL . 'assets/css/style.css', false, $this->version );
		wp_enqueue_style( 'testimonial-pro-responsive', SP_TP_URL . 'assets/css/responsive.css', false, $this->version );

		//JS Files
		wp_enqueue_script( 'slick-min-js', SP_TP_URL . 'assets/js/slick.min.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'isotope-js', SP_TP_URL . 'assets/js/jquery.isotope.min.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'expander-js', SP_TP_URL . 'assets/js/jquery.expander.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'validate-js', SP_TP_URL . 'assets/js/jquery.validate.min.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'recaptcha-js', '//www.google.com/recaptcha/api.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'jquery-masonry', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'testimonial-pro-main-js', SP_TP_URL . 'assets/js/main.js', array( 'jquery' ), $this->version, true );
	}

	/**
	 * Admin Scripts
	 */
	public function sp_tp_admin_scripts() {
		wp_enqueue_style( 'tp-admin-style', SP_TP_URL . 'assets/css/admin-style.css', false, $this->version );
	}


}
new SP_TP_Scripts();


/*** Plugin ReCaptcha Scripts ***/
if($settings['enable_captcha'] == 'yes' && $settings['public_key'] && $settings['secret_key'] ) {
	function sp_tp_recaptcha_scripts() { ?>
		<script type="text/javascript">
			jQuery(".tp-fronted-form #submit").click(function (e) {
				var data_2;
				jQuery.ajax({
					type: "POST",
					url: "<?php echo esc_url(SP_TP_URL); ?>inc/recaptcha/human-verify.php",
					data: jQuery('#testimonial_form').serialize(),
					async: false,
					success: function (data) {
						if (data.nocaptcha === "true") {
							data_2 = 1;
						} else if (data.spam === "true") {
							data_2 = 1;
						} else {
							data_2 = 0;
						}
					}
				});
				if (data_2 != 0) {
					e.preventDefault();
					if (data_2 == 1) {
						alert("Please check the captcha");
					} else {
						alert("Please Don't spam");
					}
				} else {
					jQuery("#testimonial_form").submit
				}
			});
		</script>
	<?php }

	add_action( 'wp_footer', 'sp_tp_recaptcha_scripts' );
}