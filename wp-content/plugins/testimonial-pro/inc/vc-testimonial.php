<?php

//Visual Composer
if ( class_exists( 'WPBakeryVisualComposerAbstract' ) ) {
	vc_map( array(
		"name"        => esc_html__( "Testimonial Pro", 'testimonial-pro' ),
		"base"        => "testimonial-pro",
		'icon'        => 'icon-testimonial-pro',
		"class"       => "",
		"description" => esc_html__( "This will enable Testimonials in your page/post.", 'testimonial-pro' ),
		"category"    => esc_html__( 'ShapedPlugin', 'testimonial-pro' ),
		"params"      => array(

			array(
				"type"        => "textfield",
				"heading"     => esc_html__( "Category Name", 'testimonial-pro' ),
				"description" => esc_html__( "Type category name. For multiple categories, use comma (,) among the category names.", "testimonial-pro" ),
				"param_name"  => "category",
				"value"       => "",
			),
			array(
				"type"        => "textfield",
				"heading"     => esc_html__( "Number of Columns", 'testimonial-pro' ),
				"description" => esc_html__( "Set number of columns.", "testimonial-pro" ),
				"param_name"  => "items",
				"value"       => "3",
			),
			array(
				"type"        => "textfield",
				"heading"     => esc_html__( "Number of Columns in Desktop", 'testimonial-pro' ),
				"description" => esc_html__( "Set number of columns in Desktop view.", "testimonial-pro" ),
				"param_name"  => "items_desktop",
				"value"       => "3",
				"group"       => "Responsive"
			),
			array(
				"type"        => "textfield",
				"heading"     => esc_html__( "Number of Columns in Small Desktop", 'testimonial-pro' ),
				"description" => esc_html__( "Set number of columns in Small Desktop view.", "testimonial-pro" ),
				"param_name"  => "items_desktop_small",
				"value"       => "3",
				"group"       => "Responsive"
			),
			array(
				"type"        => "textfield",
				"heading"     => esc_html__( "Number of Columns in Tablet", 'testimonial-pro' ),
				"description" => esc_html__( "Set number of columns in tablet view.", "testimonial-pro" ),
				"param_name"  => "items_tablet",
				"value"       => "2",
				"group"       => "Responsive"
			),
			array(
				"type"        => "textfield",
				"heading"     => esc_html__( "Number of Columns in Mobile", 'testimonial-pro' ),
				"description" => esc_html__( "Set number of columns in mobile view.", "testimonial-pro" ),
				"param_name"  => "items_mobile",
				"value"       => "1",
				"group"       => "Responsive"
			),
			array(
				"type"        => "textfield",
				"heading"     => esc_html__( "Number of Testimonials", 'testimonial-pro' ),
				"description" => esc_html__( "Set number of testimonials.", "testimonial-pro" ),
				"param_name"  => "total_items",
				"value"       => "10",
			),
			array(
				"type"        => "textfield",
				"heading"     => esc_html__( "Slides to Scroll", 'testimonial-pro' ),
				"description" => esc_html__( "Set slides to scroll testimonials.", "testimonial-pro" ),
				"param_name"  => "slides_to_scroll",
				"value"       => "1",
			),
			array(
				"type"        => "textfield",
				"heading"     => esc_html__( "Slide Speed", 'testimonial-pro' ),
				"description" => esc_html__( "Set testimonial slide speed.", "testimonial-pro" ),
				"param_name"  => "speed",
				"value"       => "300",
			),
			array(
				"type"        => "dropdown",
				"heading"     => esc_html__( "OderBy", 'testimonial-pro' ),
				"description" => esc_html__( "Select an OderBy option.", "testimonial-pro" ),
				"param_name"  => "order_by",
				"value"       => array(
					'Date'     => 'date',
					'Title'    => 'title',
					'Modified' => 'modified',
					'Author'   => 'author',
					'Random'   => 'rand'
				),
			),
			array(
				"type"        => "dropdown",
				"heading"     => esc_html__( "Order", 'testimonial-pro' ),
				"description" => esc_html__( "Select an order option.", "testimonial-pro" ),
				"param_name"  => "order",
				"value"       => array(
					'DESC' => 'DESC',
					'ASC'  => 'ASC'
				),
			),
			array(
				"type"        => "checkbox",
				"class"       => "",
				"heading"     => esc_html__( "Disable Rating", "testimonial-pro" ),
				"param_name"  => "rating",
				"value"       => array( esc_html__( 'Disable', 'testimonial-pro' ) => 'enable' ),
				"description" => esc_html__( "If you want disable rating check this.", "testimonial-pro" ),
			),
			array(
				"type"        => "dropdown",
				"heading"     => esc_html__( "Navigation", 'testimonial-pro' ),
				"description" => esc_html__( "Select navigation show / hide option.", "testimonial-pro" ),
				"param_name"  => "nav",
				"value"       => array(
					'Show' => 'true',
					'Hide' => 'false'
				),
			),
			array(
				"type"        => "dropdown",
				"heading"     => esc_html__( "Pagination", 'testimonial-pro' ),
				"description" => esc_html__( "Select pagination show / hide option.", "testimonial-pro" ),
				"param_name"  => "pagination",
				"value"       => array(
					'Show' => 'true',
					'Hide' => 'false'
				),
			),
			array(
				"type"        => "dropdown",
				"heading"     => esc_html__( "AutoPlay", 'testimonial-pro' ),
				"description" => esc_html__( "Select AutoPlay option.", "testimonial-pro" ),
				"param_name"  => "auto_play",
				"value"       => array(
					'Yes' => 'true',
					'No'  => 'false'
				),
			),
			array(
				"type"        => "textfield",
				"heading"     => esc_html__( "Auto Play Speed", 'testimonial-pro' ),
				"description" => esc_html__( "Set testimonial auto play speed.", "testimonial-pro" ),
				"param_name"  => "auto_play_speed",
				"value"       => "3000",
			),
			array(
				"type"        => "dropdown",
				"heading"     => esc_html__( "AutoHeight", 'testimonial-pro' ),
				"description" => esc_html__( "Select AutoHeight option.", "testimonial-pro" ),
				"param_name"  => "auto_height",
				"value"       => array(
					'No'  => 'false',
					'Yes' => 'true'
				),
			),
			array(
				"type"        => "dropdown",
				"heading"     => esc_html__( "Stop On Hover", 'testimonial-pro' ),
				"description" => esc_html__( "Select stop on hover option.", "testimonial-pro" ),
				"param_name"  => "stop_on_hover",
				"value"       => array(
					'Yes' => 'true',
					'No'  => 'false'
				),
			),
			array(
				"type"        => "dropdown",
				"heading"     => esc_html__( "Theme", 'testimonial-pro' ),
				"description" => esc_html__( "Select a theme.", "testimonial-pro" ),
				"param_name"  => "theme",
				"value"       => array(
					'Theme One'   => 'theme-one',
					'Theme Two'   => 'theme-two',
					'Theme Three' => 'theme-three',
					'Theme Four'  => 'theme-four',
					'Theme Five'  => 'theme-five',
					'Theme Six'   => 'theme-six',
					'Theme Seven' => 'theme-seven',
				),
				"group"       => "Style"
			),
			array(
				"type"        => "dropdown",
				"heading"     => esc_html__( "Image Style", 'testimonial-pro' ),
				"description" => esc_html__( "Select a style.", "testimonial-pro" ),
				"param_name"  => "image_style",
				"value"       => array(
					'One'   => 'one',
					'Two'   => 'two',
					'Three' => 'three',
					'Four'  => 'four',
				),
				"group"       => "Style"
			),
			array(
				"type"        => "colorpicker",
				"heading"     => esc_html__( "Brand Color", 'testimonial-pro' ),
				"description" => esc_html__( "Set a brand color.", "testimonial-pro" ),
				"param_name"  => "color",
				"value"       => "#52b3d9",
				"group"       => "Style"
			),
			array(
				"type"        => "dropdown",
				"heading"     => esc_html__( "RTL", 'testimonial-pro' ),
				"description" => esc_html__( "Select rtl option.", "testimonial-pro" ),
				"param_name"  => "rtl",
				"value"       => array(
					'No'  => 'false',
					'Yes' => 'true'
				),
			),
			array(
				"type"        => "dropdown",
				"heading"     => esc_html__( "Read More", 'testimonial-pro' ),
				"description" => esc_html__( "Select ReadMore option.", "testimonial-pro" ),
				"param_name"  => "read_more",
				"value"       => array(
					'No'  => 'false',
					'Yes' => 'true'
				),
			),
			array(
				"type"        => "textfield",
				"heading"     => esc_html__( "Read More text limit", 'testimonial-pro' ),
				"description" => esc_html__( "Set number of text.", "testimonial-pro" ),
				"param_name"  => "read_more_limit",
				"value"       => "120",
			),
			array(
				"type"        => "textfield",
				"heading"     => esc_html__( "Read More text", 'testimonial-pro' ),
				"description" => esc_html__( "Set read more text.", "testimonial-pro" ),
				"param_name"  => "read_more_text",
				"value"       => "Read More",
			),
			array(
				"type"        => "textfield",
				"heading"     => esc_html__( "Read More close text", 'testimonial-pro' ),
				"description" => esc_html__( "Set read more close text.", "testimonial-pro" ),
				"param_name"  => "read_more_close_text",
				"value"       => "Close",
			),

		)

	) );
}