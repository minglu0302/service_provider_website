<?php

function sp_testimonial_pro_meta_box() {
	add_meta_box( 'sp_testimonial_pro_meta_box_section',
		__( 'Testimonial Options', 'testimonial-pro' ),
		'display_sp_testimonial_pro_meta_box',
		'spt_testimonial', 'normal', 'high'
	);
}

add_action( 'admin_init', 'sp_testimonial_pro_meta_box' );


function display_sp_testimonial_pro_meta_box( $client_designation ) {
	//

	$allowedtags_add = array(
		'a'      => array(
			'href'  => array(),
			'title' => array(),
		),
		'b'      => array(),
		'br'     => array(),
		'em'     => array(),
		'i'      => array(),
		'strong' => array(),
		'span'   => array(
			'class' => true,
			'id'    => true,
		),
		'p'      => array(
			'class' => true,
			'id'    => true,
		),
		'h5'     => array(
			'class' => true,
			'id'    => true,
		),
		'h4'     => array(
			'class' => true,
			'id'    => true,
		),
		'h3'     => array(
			'class' => true,
			'id'    => true,
		)
	);
	$tps_designation = get_post_meta( $client_designation->ID, 'tp_designation', true );
	$tp_designation  = wp_kses( $tps_designation, $allowedtags_add );
	$tp_rating       = esc_html( get_post_meta( $client_designation->ID, 'tp_rating', true ) );
	?>
	<div class="sp-meta-box-framework">

		<div class="sp-mb-element sp-mb-field-text">
			<div class="sp-mb-title">
				<label for="tp_client_designation"><?php _e( 'Designation:', 'testimonial-pro' ) ?></label>

				<p class="sp-mb-desc"><?php _e( 'Type client designation here.', 'testimonial-pro' ) ?></p>
			</div>
			<div class="sp-mb-field-set">
				<textarea cols="60" rows="5" id="tp_client_designation"
				          name="tp_client_designation"><?php echo esc_html( $tp_designation ); ?></textarea>
			</div>
			<div class="sp-clear"></div>
		</div>

		<div class="sp-mb-element sp-mb-field-text">
			<div class="sp-mb-title">
				<label for="tp_client_rating"><?php _e( 'Rating Star:', 'testimonial-pro' ) ?></label>

				<p class="sp-mb-desc"><?php _e( 'Select rating star.', 'testimonial-pro' ) ?></p>
			</div>
			<div class="sp-mb-field-set">
				<?php
				$options = array(
					'no_star'    => __( 'No Rating', 'testimonial-pro' ),
					'one_star'   => __( '1 Star', 'testimonial-pro' ),
					'two_star'   => __( '2 Stars', 'testimonial-pro' ),
					'three_star' => __( '3 Stars', 'testimonial-pro' ),
					'four_star'  => __( '4 Stars', 'testimonial-pro' ),
					'five_star'  => __( '5 Stars', 'testimonial-pro' )
				);
				if ( isset( $tp_rating ) ) {
					$order_by = $tp_rating;
				}
				?>
				<select class="sp-mb-select" id="tp_client_rating" name="tp_client_rating">
					<?php
					$op = '<option value="%s"%s>%s</option>';

					foreach ( $options as $key => $value ) {

						if ( $order_by === $key ) {
							printf( $op, $key, ' selected="selected"', $value );
						} else {
							printf( $op, $key, '', $value );
						}
					}
					?>
				</select>

			</div>
			<div class="sp-clear"></div>
		</div>

	</div>

	<?php
}


function add_sp_testimonial_pro_fields( $client_designation_id, $client_designation ) {

	if ( $client_designation->post_type == 'spt_testimonial' ) {
		// Store data in post meta table if present in post data
		if ( isset( $_POST['tp_client_designation'] ) ) {
			update_post_meta( $client_designation_id, 'tp_designation', $_POST['tp_client_designation'] );
		}
		if ( isset( $_POST['tp_client_rating'] ) ) {
			update_post_meta( $client_designation_id, 'tp_rating', $_POST['tp_client_rating'] );
		}
	}

}

add_action( 'save_post', 'add_sp_testimonial_pro_fields', 10, 2 );