<?php

// Sdd SubMenu Page
function sp_testimonial_pro_settings_menu() {
	add_submenu_page(
		'edit.php?post_type=spt_testimonial',
		'Testimonial Pro Settings',
		'Settings',
		'manage_options',
		'testimonial-pro-settings',
		'sp_testimonial_pro_settings_submenu_page_callback' );
}

add_action( 'admin_menu', 'sp_testimonial_pro_settings_menu' );


// Register settings and call sanitation functions
function sp_testimonial_pro_register_setting() {
	register_setting( 'testimonial_pro_fields_options', 'testimonial_pro_default_options', 'testimonial_pro_validate_options' );
}

add_action( 'admin_init', 'sp_testimonial_pro_register_setting' );


// Default options values
$testimonial_pro_default_options = array(
	'enable_captcha' => 'yes',
	'public_key'     => '',
	'secret_key'     => '',
);

// Options validate
function testimonial_pro_validate_options( $input ) {

	// We strip all tags from the text field, to avoid vulnerablilties like XSS
	$input['enable_captcha'] = wp_filter_post_kses( $input['enable_captcha'] );
	$input['public_key']     = wp_filter_post_kses( $input['public_key'] );
	$input['secret_key']     = wp_filter_post_kses( $input['secret_key'] );

	return $input;
}

// Function to generate options page
function sp_testimonial_pro_settings_submenu_page_callback() {

	global $testimonial_pro_default_options;

	?>

	<div class="wrap">

		<h2>Captcha</h2>


		<?php if ( isset( $_GET['settings-updated'] ) ) { ?>
			<div id="message" class="updated fade">
				<p><strong><?php _e( 'Settings saved.' ) ?></strong></p>
			</div>
		<?php } ?>

		<form method="post" action="options.php">

			<?php $settings = get_option( 'testimonial_pro_default_options', $testimonial_pro_default_options ); ?>

			<?php settings_fields( 'testimonial_pro_fields_options' );
			/* This function outputs some hidden fields required by the form,
			including a nonce, a unique number used to ensure the form has been submitted from the admin page
			and not somewhere else, very important for security */ ?>

			<div class="warp">


				<div class="settings_field">
					<table class="form-table">

						<tr valign="top">
							<th scope="row">
								<label for="enable_captcha">Enable Captcha?</label>
							</th>
							<td>
								<?php
								$options = array(
									'yes' => __( 'Yes', 'shapedplugin' ),
									'no'  => __( 'No', 'shapedplugin' )
								);
								if ( isset( $settings['enable_captcha'] ) ) {
									$target_by = $settings['enable_captcha'];
								}
								?>
								<select id="enable_captcha" name="testimonial_pro_default_options[enable_captcha]">
									<?php
									$op = '<option value="%s"%s>%s</option>';

									foreach ( $options as $key => $value ) {

										if ( $target_by === $key ) {
											printf( $op, $key, ' selected="selected"', $value );
										} else {
											printf( $op, $key, '', $value );
										}
									}
									?>
								</select>

								<p class="spf-desc">If you want to enable captcha on testimonial form, select yes.</p>
							</td>
						</tr>

						<tr valign="top">
							<th scope="row">
								<label for="public_key">ReCaptcha public key</label>
							</th>
							<td>
								<input style="width: 300px;" id="public_key"
								       name="testimonial_pro_default_options[public_key]"
								       value="<?php echo stripslashes( $settings['public_key'] ); ?>" type="text"/>

								<p class="spf-desc">Give your ReCaptcha public key here. Don't have one? <a
										target="_blank" href="https://www.google.com/recaptcha/admin">Get it here.</a>
								</p>
							</td>
						</tr>

						<tr valign="top">
							<th scope="row">
								<label for="secret_key">ReCaptcha secret key</label>
							</th>
							<td>
								<input style="width: 300px;" id="secret_key"
								       name="testimonial_pro_default_options[secret_key]"
								       value="<?php echo stripslashes( $settings['secret_key'] ); ?>" type="text"/>

								<p class="spf-desc">Give your ReCaptcha secret key here.</p>
							</td>
						</tr>

					</table>
				</div>


				<div class="submit_area">
					<input class="button button-primary" type="submit" value="Save Options"/>
				</div>


			</div>
		</form>
	</div>
	<?php
}