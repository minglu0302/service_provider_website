<?php
// Testimonial Pro Widget

add_action( 'widgets_init', 'sp_testimonial_pro_widget' );

function sp_testimonial_pro_widget() {
	register_widget( 'sp_testimonial_pro_widget_content' );
}

class sp_testimonial_pro_widget_content extends WP_Widget {

	function sp_testimonial_pro_widget_content() {
		parent::__construct( 'sp_testimonial_pro_widget_content', esc_html__( 'Testimonial Pro', 'testimonial-pro' ),
			array(
				'description' => esc_html__( 'This widget is to display testimonial.', 'testimonial-pro' )
			)
		);
	}


	/*-------------------------------------------------------
	 *				Front-end display of widget
	 *-------------------------------------------------------*/

	function widget( $args, $instance ) {
		extract( $args );

		$title = apply_filters( 'widget_title', $instance['title'] );

		$id                   = $instance['id'];
		$total_items          = $instance['total_items'];
		$color                = $instance['color'];
		$pagination           = $instance['pagination'];
		$nav                  = $instance['nav'];
		$auto_play            = $instance['auto_play'];
		$auto_height          = $instance['auto_height'];
		$stop_on_hover        = $instance['stop_on_hover'];
		$rating               = $instance['rating'];
		$category             = $instance['category'];
		$order_by             = $instance['order_by'];
		$order                = $instance['order'];
		$theme                = $instance['theme'];
		$image_style          = $instance['image_style'];
		$read_more            = $instance['read_more'];
		$read_more_limit      = $instance['read_more_limit'];
		$read_more_text       = $instance['read_more_text'];
		$read_more_close_text = $instance['read_more_close_text'];
		$rtl                  = $instance['rtl'];
		$slides_to_scroll     = $instance['slides_to_scroll'];
		$speed                = $instance['speed'];
		$auto_play_speed      = $instance['auto_play_speed'];


		echo $before_widget;

		$output = '';

		if ( $title ) {
			echo $before_title . $title . $after_title;
		}

		$output .= '<div class="sp-tp-widget">';
		$output .= do_shortcode( "[testimonial-pro items='1' items_desktop='1' items_desktop_small='1'
		items_tablet='1'	items_mobile='1' slides_to_scroll='" . $slides_to_scroll . "' auto_play_speed='" . $auto_play_speed . "' speed='"
		                         . $speed . "' rtl='" . $rtl . "' read_more='" . $read_more . "' id='" . $id . "' read_more_limit='" . $read_more_limit . "' read_more_text='" . $read_more_text . "'
		read_more_close_text='" . $read_more_close_text . "'
		total_items='" .
		                         $total_items . "'
		color='"
		                         . $color . "' pagination='" . $pagination . "' nav='" . $nav . "' auto_play='" . $auto_play . "' auto_height='" . $auto_height . "' stop_on_hover='" . $stop_on_hover . "' rating='" . $rating . "' category='" . $category . "' order_by='" . $order_by . "' order='" . $order . "' theme='" . $theme . "' image_style='" . $image_style . "' ]" );
		$output .= '</div>';

		echo $output;

		echo $after_widget;
	}


	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title']                = strip_tags( $new_instance['title'] );
		$instance['id']                   = strip_tags( $new_instance['id'] );
		$instance['total_items']          = strip_tags( $new_instance['total_items'] );
		$instance['color']                = strip_tags( $new_instance['color'] );
		$instance['pagination']           = strip_tags( $new_instance['pagination'] );
		$instance['nav']                  = strip_tags( $new_instance['nav'] );
		$instance['auto_play']            = strip_tags( $new_instance['auto_play'] );
		$instance['auto_height']          = strip_tags( $new_instance['auto_height'] );
		$instance['stop_on_hover']        = strip_tags( $new_instance['stop_on_hover'] );
		$instance['rating']               = strip_tags( $new_instance['rating'] );
		$instance['category']             = strip_tags( $new_instance['category'] );
		$instance['order_by']             = strip_tags( $new_instance['order_by'] );
		$instance['order']                = strip_tags( $new_instance['order'] );
		$instance['theme']                = strip_tags( $new_instance['theme'] );
		$instance['image_style']          = strip_tags( $new_instance['image_style'] );
		$instance['read_more']            = strip_tags( $new_instance['read_more'] );
		$instance['read_more_limit']      = strip_tags( $new_instance['read_more_limit'] );
		$instance['read_more_text']       = strip_tags( $new_instance['read_more_text'] );
		$instance['read_more_close_text'] = strip_tags( $new_instance['read_more_close_text'] );
		$instance['rtl']                  = strip_tags( $new_instance['rtl'] );
		$instance['slides_to_scroll']     = strip_tags( $new_instance['slides_to_scroll'] );
		$instance['speed']                = strip_tags( $new_instance['speed'] );
		$instance['auto_play_speed']      = strip_tags( $new_instance['auto_play_speed'] );


		return $instance;
	}


	function form( $instance ) {
		$defaults = array(
			'title'                => '',
			'id'                   => '123',
			'total_items'          => '4',
			'color'                => '#52b3d9',
			'pagination'           => 'true',
			'nav'                  => 'true',
			'auto_play'            => 'true',
			'auto_height'          => 'false',
			'stop_on_hover'        => 'true',
			'rating'               => 'true',
			'category'             => '',
			'order_by'             => 'date',
			'order'                => 'DESC',
			'theme'                => 'theme_two',
			'image_style'          => 'two',
			'read_more'            => 'false',
			'read_more_limit'      => '120',
			'read_more_text'       => 'Read More',
			'read_more_close_text' => 'Close',
			'rtl'                  => 'false',
			'slides_to_scroll'     => '1',
			'speed'                => '300',
			'auto_play_speed'      => '3000',
		);
		$instance = wp_parse_args( (array) $instance, $defaults );
		?>
        <p>
            <label
                    for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Widget Title:', 'testimonial-pro' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
                   name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
                   value="<?php echo $instance['title']; ?>" style="width:100%;"/>
        </p>

        <p>
            <label
                    for="<?php echo esc_attr( $this->get_field_id( 'id' ) ); ?>"><?php esc_html_e( 'Unique ID:', 'testimonial-pro' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'id' ) ); ?>"
                   name="<?php echo esc_attr( $this->get_field_name( 'id' ) ); ?>"
                   value="<?php echo $instance['id']; ?>" type="number" style="width: 60px;margin-left: 8px;"/>
        </p>

        <p>
            <label
                    for="<?php echo $this->get_field_id( 'theme' ); ?>"><?php esc_html_e( 'Theme:', 'testimonial-pro' ); ?></label>
			<?php
			$options = array(
				'theme-one'   => esc_html__( 'Theme One', 'testimonial-pro' ),
				'theme-two'   => esc_html__( 'Theme Two', 'testimonial-pro' ),
				'theme-three' => esc_html__( 'Theme Three', 'testimonial-pro' ),
				'theme-four'  => esc_html__( 'Theme Four', 'testimonial-pro' ),
				'theme-five'  => esc_html__( 'Theme Five', 'testimonial-pro' ),
				'theme-six'   => esc_html__( 'Theme Six', 'testimonial-pro' ),
				'theme-seven' => esc_html__( 'Theme Seven', 'testimonial-pro' )
			);
			if ( isset( $instance['theme'] ) ) {
				$order_by = $instance['theme'];
			}
			?>
            <select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'theme' ) ); ?>" name="<?php echo
			esc_attr( $this->get_field_name( 'theme' ) ); ?>">
				<?php
				$op = '<option value="%s"%s>%s</option>';

				foreach ( $options as $key => $value ) {

					if ( $order_by === $key ) {
						printf( $op, $key, ' selected="selected"', $value );
					} else {
						printf( $op, $key, '', $value );
					}
				}
				?>
            </select>
        </p>

        <p style="line-height: 40px;">
            <label
                    for="<?php echo esc_attr( $this->get_field_id( 'color' ) ); ?>"><?php _e( 'Brand Color:', 'testimonial-pro' ); ?></label>
            <input type="text" class="sp-tp-brand-color<?php echo $instance['id']; ?>"
                   id="<?php echo esc_attr( $this->get_field_id( 'color' ) ); ?>"
                   name="<?php echo esc_attr( $this->get_field_name( 'color' ) ); ?>"
                   value="<?php echo esc_attr( $instance['color'] ); ?>" style="margin-left: 8px;"/>
        </p>

        <p>
            <label
                    for="<?php echo $this->get_field_id( 'image_style' ); ?>"><?php esc_html_e( 'Image Style:', 'testimonial-pro' ); ?></label>
			<?php
			$options = array(
				'two'   => esc_html__( 'Two', 'testimonial-pro' ),
				'one'   => esc_html__( 'One', 'testimonial-pro' ),
				'three' => esc_html__( 'Three', 'testimonial-pro' ),
				'four'  => esc_html__( 'Four', 'testimonial-pro' )
			);
			if ( isset( $instance['image_style'] ) ) {
				$order_by = $instance['image_style'];
			} ?>
            <select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'image_style' ) ); ?>"
                    name="<?php echo esc_attr( $this->get_field_name( 'image_style' ) ); ?>">
				<?php
				$op = '<option value="%s"%s>%s</option>';

				foreach ( $options as $key => $value ) {

					if ( $order_by === $key ) {
						printf( $op, $key, ' selected="selected"', $value );
					} else {
						printf( $op, $key, '', $value );
					}
				}
				?>
            </select>
        </p>

        <p>
            <label
                    for="<?php echo esc_attr( $this->get_field_id( 'category' ) ); ?>"><?php esc_html_e( 'Category Nome:',
					'testimonial-pro' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'category' ) ); ?>"
                   name="<?php echo esc_attr( $this->get_field_name( 'category' ) ); ?>"
                   value="<?php echo $instance['category']; ?>" style="width:100%;"/>
        </p>

        <p>
            <label
                    for="<?php echo esc_attr( $this->get_field_id( 'total_items' ) ); ?>"><?php esc_html_e( 'Number of Testimonials:', 'testimonial-pro' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'total_items' ) ); ?>"
                   name="<?php echo esc_attr( $this->get_field_name( 'total_items' ) ); ?>"
                   value="<?php echo esc_attr( $instance['total_items'] ); ?>" type="number"
                   style="width: 60px;margin-left: 8px;"/>
        </p>

        <p>
            <label
                    for="<?php echo esc_attr( $this->get_field_id( 'slides_to_scroll' ) ); ?>"><?php esc_html_e( 'Slides to Scroll:', 'testimonial-pro' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'slides_to_scroll' ) ); ?>"
                   name="<?php echo esc_attr( $this->get_field_name( 'slides_to_scroll' ) ); ?>"
                   value="<?php echo esc_attr( $instance['slides_to_scroll'] ); ?>" type="number"
                   style="width: 60px;margin-left: 8px;"/>
        </p>

        <p>
            <label
                    for="<?php echo esc_attr( $this->get_field_id( 'speed' ) ); ?>"><?php esc_html_e( 'Speed:', 'testimonial-pro' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'speed' ) ); ?>"
                   name="<?php echo esc_attr( $this->get_field_name( 'speed' ) ); ?>"
                   value="<?php echo esc_attr( $instance['speed'] ); ?>" type="number"
                   style="width: 60px;margin-left: 8px;"/>
        </p>

        <p>
            <label
                    for="<?php echo esc_attr( $this->get_field_id( 'auto_play_speed' ) ); ?>"><?php esc_html_e( 'Auto Play Speed:', 'testimonial-pro' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'auto_play_speed' ) ); ?>"
                   name="<?php echo esc_attr( $this->get_field_name( 'auto_play_speed' ) ); ?>"
                   value="<?php echo esc_attr( $instance['auto_play_speed'] ); ?>" type="number"
                   style="width: 60px;margin-left: 8px;"/>
        </p>

        <p>
            <label
                    for="<?php echo $this->get_field_id( 'auto_play' ); ?>"><?php esc_html_e( 'Auto Play:', 'testimonial-pro' ); ?></label>
			<?php
			$options = array(
				'true'  => esc_html__( 'Yes', 'testimonial-pro' ),
				'false' => esc_html__( 'No', 'testimonial-pro' )
			);
			if ( isset( $instance['auto_play'] ) ) {
				$order_by = $instance['auto_play'];
			} ?>
            <select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'auto_play' ) ); ?>"
                    name="<?php echo esc_attr( $this->get_field_name( 'auto_play' ) ); ?>">
				<?php
				$op = '<option value="%s"%s>%s</option>';

				foreach ( $options as $key => $value ) {

					if ( $order_by === $key ) {
						printf( $op, $key, ' selected="selected"', $value );
					} else {
						printf( $op, $key, '', $value );
					}
				}
				?>
            </select>
        </p>

        <p>
            <label
                    for="<?php echo $this->get_field_id( 'auto_height' ); ?>"><?php esc_html_e( 'Auto Height:', 'testimonial-pro' );
				?></label>
			<?php
			$options = array(
				'false' => esc_html__( 'No', 'testimonial-pro' ),
				'true'  => esc_html__( 'Yes', 'testimonial-pro' )
			);
			if ( isset( $instance['auto_height'] ) ) {
				$order_by = $instance['auto_height'];
			} ?>
            <select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'auto_height' ) ); ?>"
                    name="<?php echo esc_attr( $this->get_field_name( 'auto_height' ) ); ?>">
				<?php
				$op = '<option value="%s"%s>%s</option>';

				foreach ( $options as $key => $value ) {

					if ( $order_by === $key ) {
						printf( $op, $key, ' selected="selected"', $value );
					} else {
						printf( $op, $key, '', $value );
					}
				}
				?>
            </select>
        </p>

        <p>
            <label
                    for="<?php echo $this->get_field_id( 'nav' ); ?>"><?php esc_html_e( 'Navigation:', 'testimonial-pro' );
				?></label>
			<?php
			$options = array(
				'true'  => esc_html__( 'Show', 'testimonial-pro' ),
				'false' => esc_html__( 'Hide', 'testimonial-pro' )
			);
			if ( isset( $instance['nav'] ) ) {
				$order_by = $instance['nav'];
			} ?>
            <select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'nav' ) ); ?>"
                    name="<?php echo esc_attr( $this->get_field_name( 'nav' ) ); ?>">
				<?php
				$op = '<option value="%s"%s>%s</option>';

				foreach ( $options as $key => $value ) {

					if ( $order_by === $key ) {
						printf( $op, $key, ' selected="selected"', $value );
					} else {
						printf( $op, $key, '', $value );
					}
				}
				?>
            </select>
        </p>

        <p>
            <label
                    for="<?php echo $this->get_field_id( 'pagination' ); ?>"><?php esc_html_e( 'Pagination:', 'testimonial-pro' );
				?></label>
			<?php
			$options = array(
				'true'  => esc_html__( 'Show', 'testimonial-pro' ),
				'false' => esc_html__( 'Hide', 'testimonial-pro' )
			);
			if ( isset( $instance['pagination'] ) ) {
				$order_by = $instance['pagination'];
			} ?>
            <select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'pagination' ) ); ?>"
                    name="<?php echo esc_attr( $this->get_field_name( 'pagination' ) ); ?>">
				<?php
				$op = '<option value="%s"%s>%s</option>';

				foreach ( $options as $key => $value ) {

					if ( $order_by === $key ) {
						printf( $op, $key, ' selected="selected"', $value );
					} else {
						printf( $op, $key, '', $value );
					}
				}
				?>
            </select>
        </p>

        <p>
            <label
                    for="<?php echo $this->get_field_id( 'stop_on_hover' ); ?>"><?php esc_html_e( 'Stop On Hover:', 'testimonial-pro' ); ?></label>
			<?php
			$options = array(
				'true'  => esc_html__( 'Yes', 'testimonial-pro' ),
				'false' => esc_html__( 'No', 'testimonial-pro' )
			);
			if ( isset( $instance['stop_on_hover'] ) ) {
				$order_by = $instance['stop_on_hover'];
			} ?>
            <select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'stop_on_hover' ) ); ?>"
                    name="<?php echo esc_attr( $this->get_field_name( 'stop_on_hover' ) ); ?>">
				<?php
				$op = '<option value="%s"%s>%s</option>';

				foreach ( $options as $key => $value ) {

					if ( $order_by === $key ) {
						printf( $op, $key, ' selected="selected"', $value );
					} else {
						printf( $op, $key, '', $value );
					}
				}
				?>
            </select>
        </p>

        <p>
            <label
                    for="<?php echo $this->get_field_id( 'rating' ); ?>"><?php esc_html_e( 'Rating:', 'testimonial-pro' ); ?></label>
			<?php
			$options = array(
				'true'  => esc_html__( 'Show', 'testimonial-pro' ),
				'false' => esc_html__( 'Hide', 'testimonial-pro' )
			);
			if ( isset( $instance['rating'] ) ) {
				$order_by = $instance['rating'];
			} ?>
            <select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'rating' ) ); ?>"
                    name="<?php echo esc_attr( $this->get_field_name( 'rating' ) ); ?>">
				<?php
				$op = '<option value="%s"%s>%s</option>';

				foreach ( $options as $key => $value ) {

					if ( $order_by === $key ) {
						printf( $op, $key, ' selected="selected"', $value );
					} else {
						printf( $op, $key, '', $value );
					}
				}
				?>
            </select>
        </p>

        <p>
            <label
                    for="<?php echo $this->get_field_id( 'order_by' ); ?>"><?php esc_html_e( 'Order By:', 'testimonial-pro' ); ?></label>
			<?php
			$options = array(
				'date'     => esc_html__( 'Date', 'testimonial-pro' ),
				'modified' => esc_html__( 'Modified', 'testimonial-pro' ),
				'author'   => esc_html__( 'Author', 'testimonial-pro' ),
				'rand'     => esc_html__( 'Random', 'testimonial-pro' )
			);
			if ( isset( $instance['order_by'] ) ) {
				$order_by = $instance['order_by'];
			}
			?>
            <select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'order_by' ) ); ?>" name="<?php echo
			esc_attr( $this->get_field_name( 'order_by' ) ); ?>">
				<?php
				$op = '<option value="%s"%s>%s</option>';

				foreach ( $options as $key => $value ) {

					if ( $order_by === $key ) {
						printf( $op, $key, ' selected="selected"', $value );
					} else {
						printf( $op, $key, '', $value );
					}
				}
				?>
            </select>
        </p>

        <p>
            <label
                    for="<?php echo $this->get_field_id( 'order' ); ?>"><?php _e( 'Order:', 'testimonial-pro' ); ?></label>
			<?php
			$options = array(
				'DESC' => esc_html__( 'DESC', 'testimonial-pro' ),
				'ASC'  => esc_html__( 'ASC', 'testimonial-pro' )
			);
			if ( isset( $instance['order'] ) ) {
				$order_by = $instance['order'];
			}
			?>
            <select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'order' ) ); ?>" name="<?php echo
			esc_attr( $this->get_field_name( 'order' ) ); ?>">
				<?php
				$op = '<option value="%s"%s>%s</option>';

				foreach ( $options as $key => $value ) {

					if ( $order_by === $key ) {
						printf( $op, $key, ' selected="selected"', $value );
					} else {
						printf( $op, $key, '', $value );
					}
				}
				?>
            </select>
        </p>

        <p>
            <label
                    for="<?php echo $this->get_field_id( 'rtl' ); ?>"><?php esc_html_e( 'RTL:', 'testimonial-pro' );
				?></label>
			<?php
			$options = array(
				'false' => esc_html__( 'No', 'testimonial-pro' ),
				'true'  => esc_html__( 'Yes', 'testimonial-pro' )

			);
			if ( isset( $instance['rtl'] ) ) {
				$order_by = $instance['rtl'];
			} ?>
            <select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'rtl' ) ); ?>"
                    name="<?php echo esc_attr( $this->get_field_name( 'rtl' ) ); ?>">
				<?php
				$op = '<option value="%s"%s>%s</option>';

				foreach ( $options as $key => $value ) {

					if ( $order_by === $key ) {
						printf( $op, $key, ' selected="selected"', $value );
					} else {
						printf( $op, $key, '', $value );
					}
				}
				?>
            </select>
        </p>

        <p>
            <label
                    for="<?php echo $this->get_field_id( 'read_more' ); ?>"><?php esc_html_e( 'Read More:', 'testimonial-pro' ); ?></label>
			<?php
			$options = array(
				'true'  => esc_html__( 'Yes', 'testimonial-pro' ),
				'false' => esc_html__( 'No', 'testimonial-pro' )
			);
			if ( isset( $instance['read_more'] ) ) {
				$order_by = $instance['read_more'];
			} ?>
            <select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'read_more' ) ); ?>"
                    name="<?php echo esc_attr( $this->get_field_name( 'read_more' ) ); ?>">
				<?php
				$op = '<option value="%s"%s>%s</option>';

				foreach ( $options as $key => $value ) {

					if ( $order_by === $key ) {
						printf( $op, $key, ' selected="selected"', $value );
					} else {
						printf( $op, $key, '', $value );
					}
				}
				?>
            </select>
        </p>

        <p>
            <label
                    for="<?php echo esc_attr( $this->get_field_id( 'read_more_limit' ) ); ?>"><?php esc_html_e( 'Read
				More text limit:', 'testimonial-pro' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'read_more_limit' ) ); ?>"
                   name="<?php echo esc_attr( $this->get_field_name( 'read_more_limit' ) ); ?>"
                   value="<?php echo $instance['read_more_limit']; ?>" type="number"
                   style="width: 60px;margin-left: 8px;"/>
        </p>

        <p>
            <label
                    for="<?php echo esc_attr( $this->get_field_id( 'read_more_text' ) ); ?>"><?php esc_html_e( 'Read More
				 text:',
					'testimonial-pro' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'read_more_text' ) ); ?>"
                   name="<?php echo esc_attr( $this->get_field_name( 'read_more_text' ) ); ?>"
                   value="<?php echo $instance['read_more_text']; ?>" style="width:100%;"/>
        </p>

        <p>
            <label
                    for="<?php echo esc_attr( $this->get_field_id( 'read_more_close_text' ) ); ?>"><?php esc_html_e(
					'Read More close text:',
					'testimonial-pro' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'read_more_close_text' ) ); ?>"
                   name="<?php echo esc_attr( $this->get_field_name( 'read_more_close_text' ) ); ?>"
                   value="<?php echo $instance['read_more_close_text']; ?>" style="width:100%;"/>
        </p>


        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $('.sp-tp-brand-color<?php echo $instance['id']; ?>').wpColorPicker();
            })
        </script>


		<?php
	}
}