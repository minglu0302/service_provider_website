<?php

// Load Text Domain
function testimonial_pro_load_text_domain() {
	load_plugin_textdomain( 'testimonial-pro', false, plugin_basename( dirname( __FILE__ ) ) . '/languages' );
}
add_action( 'plugins_loaded', 'testimonial_pro_load_text_domain' );

// Post thumbnails
add_theme_support( 'post-thumbnails' );
add_image_size( 'tp-client-image-size', 100, 100, true );


function testimonial_pro_register_post_type() {

	$labels = array(
		'name'               => esc_html__( 'Testimonials', 'testimonial-pro' ),
		'singular_name'      => esc_html__( 'Testimonial', 'testimonial-pro' ),
		'add_new'            => esc_html_x( 'Add New Testimonial', 'testimonial-pro', 'testimonial-pro' ),
		'add_new_item'       => esc_html__( 'Add New Testimonial', 'testimonial-pro' ),
		'edit_item'          => esc_html__( 'Edit Testimonial', 'testimonial-pro' ),
		'new_item'           => esc_html__( 'New Testimonial', 'testimonial-pro' ),
		'view_item'          => esc_html__( 'View Testimonial', 'testimonial-pro' ),
		'search_items'       => esc_html__( 'Search Testimonials', 'testimonial-pro' ),
		'not_found'          => esc_html__( 'No Testimonials found', 'testimonial-pro' ),
		'not_found_in_trash' => esc_html__( 'No Testimonials found in Trash', 'testimonial-pro' ),
		'parent_item_colon'  => esc_html__( 'Parent Testimonial:', 'testimonial-pro' ),
		'menu_name'          => esc_html__( 'Testimonials', 'testimonial-pro' ),
	);

	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => 'description',
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-format-quote',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => false,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'capability_type'     => 'post',
		'supports'            => array(
			'title',
			'editor',
			'thumbnail',
			'page-attributes'
		)
	);

	register_post_type( 'spt_testimonial', $args );
}

add_action( 'init', 'testimonial_pro_register_post_type' );



// Register Custom Taxonomies
function sp_testimonial_pro_create_taxonomy()
{
	$labels = array(
		'name'              => esc_html_x( 'Categories', 'testimonial-pro' ),
		'singular_name'     => esc_html_x( 'Category', 'testimonial-pro' ),
		'search_items'      => esc_html__( 'Search Category', 'testimonial-pro' ),
		'all_items'         => esc_html__( 'All Categories', 'testimonial-pro' ),
		'parent_item'       => esc_html__( 'Parent Category', 'testimonial-pro' ),
		'parent_item_colon' => esc_html__( 'Parent Category:', 'testimonial-pro' ),
		'edit_item'         => esc_html__( 'Edit Category', 'testimonial-pro' ),
		'update_item'       => esc_html__( 'Update Category', 'testimonial-pro' ),
		'add_new_item'      => esc_html__( 'Add New Category', 'testimonial-pro' ),
		'new_item_name'     => esc_html__( 'New Category Name', 'testimonial-pro' ),
		'menu_name'         => esc_html__( 'Categories', 'testimonial-pro' )
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true
	);

	register_taxonomy('testimonial_cat',array('spt_testimonial'),$args);

}

add_action('init','sp_testimonial_pro_create_taxonomy');


// Change title placeholder
function sp_testimonial_pro_change_default_title($title) {
	$screen = get_current_screen();
	if('spt_testimonial' == $screen->post_type) {
		$title = 'Type client name here';
	}
	return $title;
}
add_filter('enter_title_here','sp_testimonial_pro_change_default_title');



// Load WP Color Picker
add_action( 'load-widgets.php', 'sp_tp_widget_color_picker' );

function sp_tp_widget_color_picker() {
	wp_enqueue_style( 'wp-color-picker' );
	wp_enqueue_script( 'wp-color-picker' );
}



/**
 * Submit testimonial send mail.
 */
function tp_submit_send_mail( $post_id, $post ) {

	$testimonial = 'spt_testimonial';

	if ( $testimonial != $post->post_type ) {
		return;
	}


	$tp_name = get_the_title( $post_id );
	$testimonial_content = get_post_field('post_content', $post_id);
	$testimonial_cat = get_the_terms( $post_id, 'testimonial_cat' );;
	$site_name = get_bloginfo('name');
	$site_description = get_bloginfo('description');
	$tp_admin_email = get_option( 'admin_email' );


	$allowedtags_add = array(
		'a'      => array(
			'href'  => array(),
			'title' => array(),
		),
		'b'      => array(),
		'br'     => array(),
		'em'     => array(),
		'i'      => array(),
		'strong' => array(),
		'span'   => array(
			'class' => true,
			'id'    => true,
		),
		'p'      => array(
			'class' => true,
			'id'    => true,
		),
		'h5'     => array(
			'class' => true,
			'id'    => true,
		),
		'h4'     => array(
			'class' => true,
			'id'    => true,
		),
		'h3'     => array(
			'class' => true,
			'id'    => true,
		)
	);
	$tps_designation = get_post_meta( $post_id, 'tp_designation', true );
	$tp_designation  = wp_kses( $tps_designation, $allowedtags_add );
	$tp_rating = esc_html( get_post_meta( $post_id, 'tp_rating', true ) );
	$tp_email =  get_post_meta( $post_id, 'tp_email', true );


	$tp_subject = 'A New Testimonial is Pending!';


	$message = "";

	$message .= '<div style="background-color: #f6f6f6;font-family: Helvetica Neue,Helvetica,Arial,sans-serif;">';
	$message .= '<div style="width: 100%;margin: 0;padding: 70px 0 70px 0;">';
	$message .= '<div style="background-color: #ffffff;border: 1px solid #e9e9e9;border-radius: 2px!important;padding: 20px 20px 10px 20px;width: 520px;margin: 0 auto;">';
	$message .= '<h1 style="color: #000000;margin: 0;padding: 28px 24px;font-size: 32px;font-weight: 500;text-align: center;">New Testimonial!</h1>';
	$message .= '<div style="padding: 40px;">';
	if(isset($tp_name)){
	$message .= "<strong>Name: </strong>";
	$message .= $tp_name;
	$message .= "<br><br>";
	}
	if(isset($tp_designation)){
	$message .= "<strong>Designation/Company: </strong>";
	$message .= $tp_designation;
	$message .= "<br><br>";
	}
	if(isset($testimonial_cat[0]->name)){
	$message .= "<strong>Category: </strong>";
	$message .= $testimonial_cat[0]->name;
	$message .= "<br><br>";
	}
	if(isset($testimonial_content)){
	$message .= "<strong>Testimonial: </strong>";
	$message .= $testimonial_content;
	$message .= "<br><br>";
	}
	if($tp_rating){
		$message .= "<strong>Rating: </strong>";

	if($tp_rating == 'no_star'){
		$message .= "No Rating";
	}
	elseif($tp_rating == 'one_star'){
		$message .= "1 Star";
	}
	elseif($tp_rating == 'two_star'){
		$message .= "2 Stars";
	}
	elseif($tp_rating == 'three_star'){
		$message .= "3 Stars";
	}
	elseif($tp_rating == 'four_star'){
		$message .= "4 Stars";
	}
	elseif($tp_rating == 'five_star'){
		$message .= "5 Stars";
	}
	$message .= "<br><br>";
	}
	$message .= '</div>';

	$message .= '<div style="border-top:1px solid #efefef;padding:20px 0;clear:both;text-align:center"><small style="font-size:11px">'. $site_name .' - '. $site_description .'</small></div>';

	$message .= '</div>';
	$message .= '</div>';
	$message .= '</div>';

	if($tp_email){
		$tp_message_header = array('Content-Type: text/html; charset=UTF-8', 'From: "'. $tp_name .'" < '. $tp_email .' >');
	}else{
		$tp_message_header = array('Content-Type: text/html; charset=UTF-8', 'From: "'. $tp_name .'" < '. $tp_admin_email .' >');
	}

	// Send email to admin.
	wp_mail( $tp_admin_email, $tp_subject, $message, $tp_message_header );
}
add_action( 'pending_spt_testimonial', 'tp_submit_send_mail', 10, 3 );


/* Including files */
if(file_exists( SP_TP_PATH . 'inc/options/meta-box.php')){
	require_once(SP_TP_PATH . "inc/options/meta-box.php");
}

if(file_exists( SP_TP_PATH . 'inc/options/settings.php')){
	require_once(SP_TP_PATH . "inc/options/settings.php");
}

if(file_exists( SP_TP_PATH . 'inc/shortcodes.php')){
	require_once(SP_TP_PATH . "inc/shortcodes.php");
}
if (function_exists('vc_map')) {
	require_once(SP_TP_PATH . "inc/vc-testimonial.php");
}
if(file_exists( SP_TP_PATH . 'inc/widget.php')){
	require_once(SP_TP_PATH . "inc/widget.php");
}
if(file_exists( SP_TP_PATH . 'inc/resizer.php')){
	require_once(SP_TP_PATH . "inc/resizer.php");
}


/**
 * ShortCode Support in Widget
 */
add_filter( 'widget_text', 'do_shortcode' );


/**
 * Update post type
 */

global $wpdb;
$old_post_types = array('testimonial-free' => 'spt_testimonial');
foreach ($old_post_types as $old_type=>$type) {
	$wpdb->query( $wpdb->prepare( "UPDATE {$wpdb->posts} SET post_type = REPLACE(post_type, %s, %s) 
                     WHERE post_type LIKE %s", $old_type, $type, $old_type ) );
	$wpdb->query( $wpdb->prepare( "UPDATE {$wpdb->posts} SET guid = REPLACE(guid, %s, %s) 
                     WHERE guid LIKE %s", "post_type={$old_type}", "post_type={$type}", "%post_type={$type}%" ) );
	$wpdb->query( $wpdb->prepare( "UPDATE {$wpdb->posts} SET guid = REPLACE(guid, %s, %s) 
                     WHERE guid LIKE %s", "/{$old_type}/", "/{$type}/", "%/{$old_type}/%" ) );
}

$sp_old_post_types = array('testimonial-pro' => 'spt_testimonial');
foreach ($sp_old_post_types as $sp_old_type=>$type) {
	$wpdb->query( $wpdb->prepare( "UPDATE {$wpdb->posts} SET post_type = REPLACE(post_type, %s, %s) 
                     WHERE post_type LIKE %s", $sp_old_type, $type, $sp_old_type ) );
	$wpdb->query( $wpdb->prepare( "UPDATE {$wpdb->posts} SET guid = REPLACE(guid, %s, %s) 
                     WHERE guid LIKE %s", "post_type={$sp_old_type}", "post_type={$type}", "%post_type={$type}%" ) );
	$wpdb->query( $wpdb->prepare( "UPDATE {$wpdb->posts} SET guid = REPLACE(guid, %s, %s) 
                     WHERE guid LIKE %s", "/{$sp_old_type}/", "/{$type}/", "%/{$sp_old_type}/%" ) );
}