jQuery(function ($) {

    'use strict';

    /* === Masonery === */
    (function () {
        var masonry = $('.masonry_testimonial');
        $(window).load(function () {
            masonry.masonry();//Masonry
        });
    }());


    /* === Testimonial Submit Form === */
    (function () {
        jQuery("#testimonial_form").validate();
    }());


    // ----------------------------------------------
    //  Isotope Filter
    // ----------------------------------------------
    (function () {
        var winDow = $(window);
        var $container=$('.sp-isotope-testimonial-items');
        var $filter=$('.sp-testimonial-filter');

        try{
            $container.imagesLoaded( function(){
                $container.show();
                $container.isotope({
                    filter:'*',
                    layoutMode:'masonry',
                    animationOptions:{
                        duration:750,
                        easing:'linear'
                    }
                });
            });
        } catch(err) {
        }

        winDow.bind('resize', function(){
            var selector = $filter.find('a.active').attr('data-filter');

            try {
                $container.isotope({
                    filter	: selector,
                    animationOptions: {
                        duration: 750,
                        easing	: 'linear',
                        queue	: false,
                    }
                });
            } catch(err) {
            }
            return false;
        });

        $filter.find('a').click(function(){
            var selector = $(this).attr('data-filter');

            try {
                $container.isotope({
                    filter	: selector,
                    animationOptions: {
                        duration: 750,
                        easing	: 'linear',
                        queue	: false,
                    }
                });
            } catch(err) {

            }
            return false;
        });


        var filterItemA	= $('.sp-testimonial-filter a');

        filterItemA.on('click', function(){
            var $this = $(this);
            if ( !$this.hasClass('active')) {
                filterItemA.removeClass('active');
                $this.addClass('active');
            }
        });
    }());

});