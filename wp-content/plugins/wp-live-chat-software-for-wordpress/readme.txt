=== LiveChat - WP live chat plugin for WordPress ===
Contributors: LiveChat
Tags: live support, live chat, live chat software, help desk, help desk software, online ticketing system, ticketing system, online support, ecommerce chat, sales, customer help, customer support, livechat, customer service software, chat plugin, chat, customer service chat, live chat button, live chat plugin, live chat support, live chat tool, live chat widget, live support button, live chat solution, online live chat, chat plugin, online chat, wordpress live chat
Stable tag: 3.4.2
Requires at least: 3.4
Tested up to: 4.9

Live chat and help desk software plugin for WordPress. Add LiveChat (live chat and help desk software) to your WordPress. 

== Description ==

Looking to add a live chat plugin to your WordPress website? LiveChat is a live chat app that enables its users to easily connect and communicate with their on-site visitors and customers. Chat operators can utilize LiveChat to instantly help customers, answer queries, and make the customer experience better. From answering support queries to onboarding new customers, LiveChat chat plugin can be used to facilitate every aspect of running a business.

LiveChat comes with a comprehensive set of features, including proactive chat invitations - so chat operators can automatically send personalized messages to appear based on specified conditions, such as the number of visited pages or time spent on the website. Other features include real time monitoring, file sharing, and advanced analytics. It has a clean design and is highly customizable, allowing to make the live chat window fit your brand.  

In addition to live chat support, LiveChat also offers a built-in ticketing system to provide 24/7 customer service to customers. That being said, whenever there are no chat operators available, the chat widget is replaced by a ticket form so that customers can still contact you with their questions. 

From email marketing to social, LiveChat integrates seamlessly with a range of popular third party apps and services. These include help desk software Zendesk, Freshdesk, customer relationship management apps (CRM) HubSpot, Salesforce and social media platforms Facebook or Twitter. 

https://www.youtube.com/watch?v=Yf0NkRSkFRE

== Live chat benefits ==

From overnight shipping to instant access to your product or service, customers want help instantly. Out of all the customer support channels available — offline or online — live chat is by far the quickest and simplest.

With live chat for WordPress, customers have a direct and free option to ask for help. They can use it with minimal disruption to their day, which turns out to be very convenient.

Using live chat and WordPress together, enables customers to ask questions about the product they want to buy and you can answer them in real-time, which simplifies the buying process. 

Moreover, using live chat to communicate with the customer helps to understand their needs. You can use that knowledge to recommend similar products and services to increase the average order value.

Just by offering live chat – WordPress users are one step ahead of the companies that do not. Competitive advantage can be obtained by managing products more effectively, and having good relationships with customers.

== What is live chat for WordPress? ==

If you run a website, and you are serious about your business then using live chat is no brainer. Live chat plugin for WordPress adds a live chat widget to the bottom of your website that allows visitors to chat to you in real time. Live chat software is perfect for businesses of all sizes, enabling them to close more sales and improve customer satisfaction scores.

== Usage ==

With the LiveChat chat plugin for WordPress, you can add live chat to every page of your website, including the checkout in no time. To answer chats, chat operators have to be logged into one of our apps - web based or desktop. There are also applications for mobile devices - iPhone, iPad and Android. 

== Requirements ==

While LiveChat plugin for WordPress is free, you need a subscription of LiveChat to use it. For an overview of available plans and their cost navigate to [LiveChat pricing](https://www.livechatinc.com/pricing). There's a 30-day trial to test all of the features. No credit card, no commitments.

[Sign up for our LiveChat here.](https://www.livechatinc.com/signup/?utm_source=wordpress.org&utm_medium=integration&utm_campaign=WordpressIntegration)

== Frequently Asked Questions ==

= Do I need a LiveChat account to use this live chat plugin? =
Yes - you can create your live chat account [here](https://www.livechatinc.com/signup/?utm_source=wordpress.org&utm_medium=integration&utm_campaign=WordpressIntegration).

= Do you offer a free trial? =
Yes, you can test all of the features of our live chat software for 30 days. Sign up for free here.

[Sign up for free here](https://www.livechatinc.com/signup/?utm_source=wordpress.org&utm_medium=integration&utm_campaign=WordpressIntegration).

= Are there any limitations during the trial period? =
No, during the 30-day trial, by default, you will be able to test all of the features in the Team plan.

= Is it required to install any additional software to use LiveChat? = 
Just this live chat plugin; LiveChat works as SaaS and doesn't require any extra service to work.

= Do you provide live chat support? =
YES! You can live chat with us anytime - we are available 24/7/365!

= I am logged in. How do I start? =
The first thing you need to do is change the live chat status to ACCEPT CHATS. That will allow you to start answering incoming chats from customer. You can also refer to our [Agent Handbook](https://www.livechatinc.com/resources/customer-service/agents-handbook-guide-satisfied-customers/?utm_source=wordpress.org&utm_medium=integration&utm_campaign=WordpressIntegration) for more details.

= Do I need to be logged in to be available for chats? =
Yes, you need to stay online to allow customers to contact you. If no one is available, your live chat window will be shown as a ticket form - click [here](https://www.livechatinc.com/kb/support-tickets-in-livechat/?utm_source=wordpress.org&utm_medium=integration&utm_campaign=WordpressIntegration) to learn more about tickets in LiveChat.

= Is there a limit to the number of chats I can take? =
No, you can handle an unlimited number of live chats with any number of customers.

= Is it possible to track on-site visitors through this live chat plugin? =
YES! With LiveChat – live chat plugin for WordPress, you can see your visitor’s important information, such as their location, the page they are currently viewing.

= Is the chat window customizable? =
Yes, each part of the chat window can be customized – company logo, live chat theme and more.

= Can I proactively reach out to my website visitors and offer help? =
It is possible to send [proactive chats](https://www.livechatinc.com/kb/why-should-i-set-up-chat-greetings/?utm_source=wordpress.org&utm_medium=integration&utm_campaign=WordpressIntegration) to your website visitors based on “live chat triggers”, such as the number of visited pages or time spent on the website.

= Does your live chat software support other languages? =
Short answer: LiveChat supports 45 languages. Long answer: English, Spanish, French, Chinese, Portuguese, Indonesian, Thai, Vietnamese (…). You can learn more about the supported languages [here](https://www.livechatinc.com/kb/how-to-modify-chat-window-language/?utm_source=wordpress.org&utm_medium=integration&utm_campaign=WordpressIntegration).

= How do I add more live chat agents? =
Just head to AGENTS > ADD AN AGENT to start adding new live chat agents. 

= What can I customize in the live chat dashboard? = 
You can customize a range of features, like the [live chat surveys](https://www.livechatinc.com/kb/chat-surveys/?utm_source=wordpress.org&utm_medium=integration&utm_campaign=WordpressIntegration), [live chat theme](https://www.livechatinc.com/kb/chat-window-customization/?utm_source=wordpress.org&utm_medium=integration&utm_campaign=WordpressIntegration), [live chat buttons](https://www.livechatinc.com/kb/chat-button-guide/?utm_source=wordpress.org&utm_medium=integration&utm_campaign=WordpressIntegration), [live chat greetings](https://www.livechatinc.com/kb/why-should-i-set-up-chat-greetings/?utm_source=wordpress.org&utm_medium=integration&utm_campaign=WordpressIntegration), [live chat canned responses](https://www.livechatinc.com/kb/canned-responses/?utm_source=wordpress.org&utm_medium=integration&utm_campaign=WordpressIntegration), [live chat tags](https://www.livechatinc.com/kb/tagging-chats-and-tickets/?utm_source=wordpress.org&utm_medium=integration&utm_campaign=WordpressIntegration) and and some other advanced live chat settings.

= How does the live chat app work after hours? =
When you go offline, your live chat widget is replaced with a ticket form. It means that visitors can still leave you questions as tickets. To manage tickets you need to log into your live chat app. 

= How to personalize my live chat avatar? =
To edit the live chat avatar, simply navigate to the AGENTS section, pick your account and click EDIT. Apart from changing the picture, you can edit the name and title for your avatar.

= Does LiveChat support multiple websites? =
Yes, you can use our live chat to monitor multiple websites using a single interface. Additionally, you can customize the look of the live chat widget per website. 

= Is it possible to change the position of the live chat widget? =
Definitely - you can move your live chat from the right side of the screen to the left using [CSS styles](https://www.livechatinc.com/kb/customize-your-chat-window-with-css/?utm_source=wordpress.org&utm_medium=integration&utm_campaign=WordpressIntegration).

= Can I have my customers include their contact details when they are asking for support? =
Sure, you can use the pre-chat survey at the beginning of each live chat to collect relevant information, such as an email address or phone number. 

= How can I get notified of incoming live chats? =
There are sound and visual notifications available to make sure that no chat or message goes unnoticed. 

= Can I live chat with customers on my mobile? =
With our [mobile live chat apps](https://www.livechatinc.com/kb/stay-mobile-with-livechat/?utm_source=wordpress.org&utm_medium=integration&utm_campaign=WordpressIntegration) for iPhone, iPad and Android you can chat everywhere: in a car, at home or while shopping. 

= How to view chat transcripts? =
Each live chat is saved as a chat transcript for future reference, and available to view from the Archives section.

= Can I add live chat to Facebook? = 
It is possible to place a live chat option on your Facebook fan page that will open a whole new channel for your customers to use - read more about the live chat integration with Facebook [here](https://www.livechatinc.com/kb/adding-live-chat-to-facebook/?utm_source=wordpress.org&utm_medium=integration&utm_campaign=WordpressIntegration).

= Can live chat agents help one another when chatting with customers? = 
Sure thing - they can use use chat transfers to hand over their chats to other live chat agents. Additionally, each ongoing chat can be supervised in real time for coaching purposes. Read more on the transfers and supervision features [here](https://www.livechatinc.com/kb/in-chat-cooperation-transfers-and-supervision/?utm_source=wordpress.org&utm_medium=integration&utm_campaign=WordpressIntegration).

= Do you offer any tips on how to live chat… I could check? =
Sure - head over to our [website](https://www.livechatinc.com/resources/?utm_source=wordpress.org&utm_medium=integration&utm_campaign=WordpressIntegration) for best practices and tips on how to your company can benefit from live chat support.

= I love LiveChat. How can I promote it? = 
Feel free to leave us a review. Your feedback is really important to us, as it helps others discover our live chat software. 

= How do I learn more about LiveChat and your live chat plugin? = 
Navigate to our [website](https://www.livechatinc.com/?utm_source=wordpress.org&utm_medium=integration&utm_campaign=WordpressIntegration) and start a live chat with us!


== Installation ==

1. Upload `wp-live-chat-software-for-wordpress` directory to WordPress plugins directory (`/wp-content/plugins/`).
2. Activate the live chat plugin through the `Plugins` menu in WordPress.
3. Click the `LiveChat` menu on the left.
4. Follow the instructions.

For more detailed instructions, go to the [live chat plugin page](https://www.livechatinc.com/kb/wordpress-livechat-on-your-website/?utm_source=wordpress.org&utm_medium=integration&utm_campaign=WordpressIntegration).

== Screenshots ==

1. Live chat applications are available for web browser, Windows Mac and iPhone, iPad and Android devices.
2. LiveChat is designed specifically for real-time customer service using live chat and help desk tickets.
3. Visitors list and client details display valuable information useful during chats with customers.
4. Live chat dashboard - a real-time glimpse of your customer service to display on a big screen.
5. Live chat window can be customized. Social media icons can be added for social engagement.
6. LiveChat offers plenty of reports and customer service analytics.
7. Additional information from pre-chat and post-chat surveys serves as extra knowledge about customers.
8. Help desk software in LiveChat allows collecting tickets from chats, emails and ticket forms.


== Changelog ==

= 3.4.2 =
* Fixed get_plugin_url function, updated logo - added HiRes retina image

= 3.4.1 =
* Updated readme.txt file, checked compatibility to WordPress 4.8.1, small plugin js/css fixes.

= 3.4 =
* Updated readme.txt file, checked compatibility to WordPress 4.7.3

= 3.3.1 =
* Updated readme.txt file

= 3.3 =
* get_currentuser info function deprecated in WordPress 4.5, replaced to wp_get_current_user function

= 3.2.14 =
* Updated readme.txt file, checked compatibility to WordPress 4.4.1

= 3.2.13 =
* Removed __lc.group from live chat tracking code, updated readme.txt file

= 3.2.12 =
* Updated readme.txt file, checked compatibility to WordPress 4.3

= 3.2.11 =
* Moved live chat screenshots to assets directory

= 3.2.10 =
* Updated readme.txt file, updated live chat screenshots

= 3.2.9 =
* Updated readme.txt file, checked compatibility to WordPress 4.2.2

= 3.2.8 =
* Live chat plugin fully supports the newest version of WordPress.

= 3.2.7 =
* Live chat plugin compatible with the newest LiveChat version

= 3.2.5 =
* Updated trunk

= 3.2.4 =
* Updated url for live chat licence number

= 3.2.3 =
* Removed language parameter from live chat tracking code

= 3.2.2 =
* Fixed CSS styles conflicts with other plugins

= 3.2.1 =
* Fixed live chat plugin "Settings" link
* Changed message after successful installation

= 3.2.0 =
* Compatibility with new live chat window
* Removed "click-to-chat button" configurator (no longer used)

= 3.1.0 =
* Major update in the live chat tracking code

= 3.0.0 =
* Rewritten whole live chat plugin code
* Fixed creating live chat licenses
* Improved look and feel

= 2.1.8 =
* Updated live chat desktop application download link

= 2.1.7 =
* Fixed `Skill` parameter
* Updated monitoring and live chat button codes (for improved performance)
* Updated skills tutorial link

= 2.1.6 =
* Added LiveChat icon in menu bar

= 2.1.5 =
* Fixed creating new live chat licences
* Rebranded LIVECHAT Contact Center to LiveChat
* Updated live chat application download link
* Renamed "Groups" to "Skills"

= 2.1.4 =
* Fixed readme.txt file causing the `The plugin does not have a valid header.` problem

= 2.1.3 =
* Added timezone detection when creating new live chat license

= 2.1.2 =
* Added "Company", "Phone" and "Website" fields in  live chat license registration form
* Fixed license registration bug
* Updated Control Panel URL

= 2.1.1 =
* Fixed monitoring and live chat button code installation

= 2.1.0 =
* Added "Settings" link to live chat plugin settings page
* Added "Download application" button
* Added Control Panel to live chat plugin
* Added Chat button installation help for themes with no live chat widgets support

= 2.0.0 =
* Rewritten whole live chat plugin from scratch
* Ability to create new license from WordPress live chat plugin settings
* Updated live chat monitoring code speed
* Added live chat notification messages

= 1.0.0 =
* First live chat plugin version