<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

session_start();
require_once 'Models/ConDBServices.php';
require_once 'Models/config.php';
require_once 'Models/sendAMail.php';

if (!isset($_REQUEST['data']) && !isset($_SESSION['resetData']))
    header('location: http://www.google.com');
if (isset($_REQUEST['data'])) {
     $db = new ConDBServices();
    $dataArr = explode('_', $_SESSION['resetData']);

    if ($dataArr[1] == '1') {
        $table = 'doctor';
        $uid = 'doc_id';
    } else if ($dataArr[1] == '2') {
        $table = 'patient';
        $uid = 'patient_id';
    } else {
        header('location: http://www.google.com');
    }
    $_SESSION['resetData'] = $_REQUEST['data'];
    $selectDataQry = "select $uid,email,first_name from $table where resetData = '" . $_SESSION['resetData'] . "' and resetFlag = 1";
    $selectDataRes = mysql_query($selectDataQry, $db->conn);

    if (mysql_num_rows($selectDataRes) <= 0) {

        $ht = '<style>#formL {
    display: none;
}</style><div style="text-align: center;">
     <img src="http://iserve.ind.in/superadmin/theme/assets/img/demo/homelogo.png" alt="logo" data-src-retina="http://iserve.ind.in/superadmin/theme/assets/img/demo/homelogo.png">
      
     <h3 style="color: #1fa7b2;">Sorry! The link to reset your password has expired. Please submit the ‘forgot password’ request again</h3>
   
    
</div>';
        echo $ht;
    }
}
function checkPassword($pwd) {

    $errors = array();

    if (strlen($pwd) < 5) {
        $errors[] = "Password too short, 5 characters least!";
    }

    if (!preg_match("#[0-9]+#", $pwd)) {
        $errors[] = "Password must include at least one number!";
    }

    if (!preg_match("#[A-Z]+#", $pwd)) {
        $errors[] = "Password must include at least one upper case letter!";
    }

    return $errors;
}

$dataArr = explode('_', $_SESSION['resetData']);

if ($dataArr[1] == '1') {
    $table = 'doctor';
    $uid = 'doc_id';
} else if ($dataArr[1] == '2') {
    $table = 'patient';
    $uid = 'patient_id';
} else {
    header('location: http://www.google.com');
}

$message = "";

if (isset($_REQUEST['pass']) && isset($_REQUEST['conf_pass'])) {

    if ($_REQUEST['pass'] == '' || $_REQUEST['conf_pass'] == '') {
        $message = 'Please enter both fields.';
    } else if ($_REQUEST['pass'] != $_REQUEST['conf_pass']) {
        $message = 'Passwords does not match, please try again.';
    } else {

        $strength = checkPassword($_REQUEST['pass']);

        if (count($strength) > 0) {
            foreach ($strength as $err) {
                $message = "Your password is too short, it must be 5 at least characters long.<br>The password must also include at least one number."; //$err.'<br>';
            }
        } else {


            $db = new ConDBServices();

            $selectDataQry = "select $uid,email,first_name from $table where resetData = '" . $_SESSION['resetData'] . "' and resetFlag = 1";
            $selectDataRes = mysql_query($selectDataQry, $db->conn);

            if (mysql_num_rows($selectDataRes) <= 0) {
               
                 $ht ='<style>#formL {
    display: none;
}</style><div style="text-align: center;">
     <img src="http://iserve.ind.in/superadmin/theme/assets/img/demo/homelogo.png" alt="logo" data-src-retina="http://iserve.ind.in/superadmin/theme/assets/img/demo/homelogo.png">
      
     <h3 style="color: #1fa7b2;">link expire</h3>
   
    
</div>';
    echo $ht;
     $message = 'Nice try, use forget password option on the mobile App!.';
            } else {
                $userData = mysql_fetch_assoc($selectDataRes);

                $updateDataQry = "update $table set password = md5('" . $_REQUEST['pass'] . "'), resetData = null, resetFlag = null where $uid = '" . $userData[$uid] . "'";
                mysql_query($updateDataQry, $db->conn);

                if (mysql_affected_rows() > 0) {

                    $deleteAllSessionsQry = "update user_sessions set loggedIn = 2 where oid = '" . $userData[$uid] . "' and user_type = '" . $dataArr[1] . "'";
                    mysql_query($deleteAllSessionsQry, $db->conn);

                    $mail = new sendAMail(APP_SERVER_HOST);
                    $resetRes = $mail->passwordChanged($userData);
                    $da ='<style>#login {
    display: none;
}</style> ';
                    echo $da;
                    $message = 'Password changed successfully.';
                } else {
                    $message = 'Failed to change password.';
                }
            }
        }
    }
}
?>
<div style="text-align: center;" id="formL">
     <img src="http://iserve.ind.in/superadmin/theme/assets/img/demo/homelogo.png" alt="logo" data-src-retina="http://iserve.ind.in/superadmin/theme/assets/img/demo/homelogo.png">
      <?php echo '<h5 style="color:red;">' . $message . '</h5>'; ?>
     <div id="login">
     <h3 style="color: #1fa7b2;">Change the password here: </h3>
   
    <form action="" method="post">
        
        <lable>Password:</lable><input type="password" name="pass" placeholder="New password"><br><br>
        <lable>Confirm :</lable><input type="password" name="conf_pass" placeholder="Confirm password"><br>
        <input type="submit" name="submit" /><br>
       
    </form>
      </div>
</div>

<style>
    #login {
    padding-top: 50px
}
#login .form-wrap {
    width: 30%;
    margin: 0 auto;
}
#login h1 {
    color: #1aa5b1;
    font-size: 18px;
    text-align: center;
    font-weight: bold;
    padding-bottom: 20px;
}
#login .form-group {
    margin-bottom: 25px;
}
#login .checkbox {
    margin-bottom: 20px;
    position: relative;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    -o-user-select: none;
    user-select: none;
}
#login .checkbox.show:before {
    content: '\e013';
    color: #1fa67b;
    font-size: 17px;
    margin: 1px 0 0 3px;
    position: absolute;
    pointer-events: none;
    font-family: 'Glyphicons Halflings';
}
#login .checkbox .character-checkbox {
    width: 25px;
    height: 25px;
    cursor: pointer;
    border-radius: 3px;
    border: 1px solid #ccc;
    vertical-align: middle;
    display: inline-block;
}
#login .checkbox .label {
    color: #6d6d6d;
    font-size: 13px;
    font-weight: normal;
}
#login .btn.btn-custom {
    font-size: 14px;
	margin-bottom: 20px;
}
#login .forget {
    font-size: 13px;
	text-align: center;
	display: block;
}

/*    --------------------------------------------------
	:: Inputs & Buttons
	-------------------------------------------------- */
.form-control {
    color: #212121;
}
.btn-custom {
    color: #fff;
	background-color: #1fa67b;
}
.btn-custom:hover,
.btn-custom:focus {
    color: #fff;
}

/*    --------------------------------------------------
    :: Footer
	-------------------------------------------------- */
#footer {
    color: #6d6d6d;
    font-size: 12px;
    text-align: center;
}
#footer p {
    margin-bottom: 0;
}
#footer a {
    color: inherit;
}
</style>
<!--<div style="text-align: center;">
    <h3>Change the password here: </h3>
    <?php echo '<h5 style="color:red;">' . $message . '</h5>'; ?>
    <form action="" method="post">
        <lable>Passwords:</lable><input type="password" name="pass" /><br><br>
        <lable>Confirm :</lable><input type="password" name="conf_pass" /><br>
        <input type="submit" name="submit" /><br>
    </form>
</div>-->
<!--<section id="login">
    <div class="container">
    	<div class="row">
    	    <div class="col-xs-12">
        	    <div class="form-wrap">
                        <img src="http://yaaro.in/superadmin/theme/assets/img/demo/homelogo.png" alt="logo" data-src-retina="http://yaaro.in/superadmin/theme/assets/img/demo/homelogo.png" style="margin-left: 20%">
                <h1>Change the password here</h1>
                <?php echo '<h5 style="color:red;">' . $message . '</h5>'; ?>
                  <form action="" method="post">
                        <div class="form-group">
                            <label for="email" class="sr-only">Passwords</label>
                            <input type="password" name="pass" i  class="form-control" placeholder="New Password">
                        </div>
                        <div class="form-group">
                            <label for="key" class="sr-only">Confirm</label>
                            <input type="password" name="conf_pass"   class="form-control" placeholder="Confrim password">
                        </div>
                         
                        <input type="submit" name="submit" /><br>
                    </form>
                   
                     
        	    </div>
    		</div>  /.col-xs-12 
    	</div>  /.row 
    </div>  /.container 
</section>-->